package com.telefonica.total.rest;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telefonica.total.common.util.RulesValueProp;

@RestController
@RequestMapping("/cache")
public class CacheManagerRest {

    @Autowired
    private CacheManager manager;

    @Autowired
    private ObjectMapper objMap;

    @Autowired
    private RulesValueProp prop;

    @DeleteMapping("/clear/all")
    public ResponseEntity<Object> clearAllCaches(@RequestBody String json) throws IOException {

	if (this.validateUsrPass(json)) {
	    for (String cacheName : manager.getCacheNames()) {
		clearCacheFromCacheName(cacheName);
	    }
	    return new ResponseEntity<>(HttpStatus.OK);
	} else {
	    return new ResponseEntity<>(HttpStatus.METHOD_NOT_ALLOWED);
	}

    }

    @DeleteMapping("/clear/{cacheName}")
    public ResponseEntity<Object> clearCache(@PathVariable("cacheName") String cacheName, @RequestBody String json) throws IOException {
	if (this.validateUsrPass(json)) {
	    return clearCacheFromCacheName(cacheName) ? new ResponseEntity<>(HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
	} else {
	    return new ResponseEntity<>(HttpStatus.METHOD_NOT_ALLOWED);
	}
    }

    private Boolean clearCacheFromCacheName(final String cacheName) {
	final Cache cache = manager.getCache(cacheName);
	if (cacheExists(cache)) {
	    cache.clear();
	    return true;
	}
	return false;
    }

    private Boolean cacheExists(final Cache cache) {
	return cache != null;
    }

    private Boolean validateUsrPass(String json) throws IOException {
	JsonNode jsonNode = objMap.readTree(json);
	String user = jsonNode.get("user").textValue();
	String password = jsonNode.get("password").textValue();

	if (prop.getUSER().equals(user) && prop.getPASSWORD().equals(password)) {
	    return Boolean.TRUE;
	} else {
	    return Boolean.FALSE;
	}
    }

}
