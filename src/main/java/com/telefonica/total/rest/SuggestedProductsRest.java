package com.telefonica.total.rest;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.telefonica.total.common.controller.BaseMTController;
import com.telefonica.total.exception.ApiFault;
import com.telefonica.total.pojo.req.ReqData;
import com.telefonica.total.pojo.resp.RespData;
import com.telefonica.total.service.ISuggestedProduct;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = "/query", consumes = "application/json")
@RestController
@RequestMapping("/query")
public class SuggestedProductsRest extends BaseMTController {

    @Autowired
    private ISuggestedProduct suggested;

    @ApiOperation(protocols = "http", value = "Método que se encarga de la logica de negocio del motor MT", notes = "Esta operacion devolvera los productos sugeridos a los clientes MT tanto planes como equipos", response = RespData.class, nickname = "getSuggestProduct", responseContainer = "List")
    @ApiResponses(value = { @ApiResponse(code = 400, message = "Bad Request", response = ApiFault.class),
	    @ApiResponse(code = 500, message = "Internal Server Error", response = ApiFault.class),
	    @ApiResponse(code = 204, message = "No Content", response = ApiFault.class) })
    @PostMapping("/suggested-products")
    public ResponseEntity<RespData> getSuggestProduct(@Valid @RequestBody ReqData request, BindingResult bindingResult,
	    HttpServletRequest httpReq) throws MethodArgumentNotValidException {
	this.evaluateValidation(bindingResult, httpReq, request);

	RespData response = suggested.getSuggestedOffers(request);

	return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
