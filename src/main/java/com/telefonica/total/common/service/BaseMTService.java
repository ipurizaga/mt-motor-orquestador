package com.telefonica.total.common.service;

import com.telefonica.total.pojo.req.ReqData;
import com.telefonica.total.pojo.resp.RespData;

public interface BaseMTService {

    /***
     * Método que se encarga de realizar la logica de todo el flujo de planes del
     * motor de MT.
     * 
     * @param request
     * @return
     */
    RespData executeLogicOperations(ReqData request);

}
