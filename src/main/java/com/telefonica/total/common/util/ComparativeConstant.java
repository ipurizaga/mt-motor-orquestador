package com.telefonica.total.common.util;

public class ComparativeConstant {
    
    public static final String	ORIGEN  = "Origen";
    public static final String	DESTINO = "Destino";
    public static final String	GAP     = "Gap";
    
    public static final String	INTERNET_FIJO          = "InternetFijo";
    public static final String	INTERNET_VELOCIDAD     = "InternetVelocidad";
    public static final String	TELEVISION_TENENCIA    = "Television";
    public static final String	BLOQUEHD_TENENCIA      = "BloqueHD";
    public static final String	DATOS_MOVILES_TENENCIA = "DatosMovil%s";
    public static final String	DATOS_MOVILES_DATOS    = "DatosMovil%sGB";
    public static final String	DATOS_MOVILES_DATOS_TOTAL    = "DatosMovilTotalGB";
    public static final String	DATOS_MOVILES_ROAM     = "DatosMovil%sRoam";
    public static final String	DATOS_MOVILES_ROAM_TOTAL     = "DatosMovilTotalRoam";
    public static final String	CANTIDAD_LINEAS        = "CantLineas";
    public static final String	DATOS_MOVIL_ILIM        = "DatosMovilIlim";
    
    private ComparativeConstant() {
	
    }
}
