package com.telefonica.total.common.util;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.model.ParamMovTotal;
import com.telefonica.total.repository.ParamMovTotalRepo;

/**
 * 
 * @Author: Jonathan Reyna Rossel.
 * @Datecreation: 20 jul. 2020 14:00:00
 * @FileName: ValidationUtil.java
 * @AuthorCompany: Everis
 * @version: 0.1
 * @Description: clase para validar las priorizaciones de las ofertas MT.
 */
public class ValidationUtil {	
	/**
	 * 
	 * @param filteredProductList
	 * @param downselList
	 * @param productType
	 * @param typeTotalization
	 * @param arpa
	 * 
	 * Funcion creada para las priorizaciones de ofertas con cobertura HFC
	 */
	public static void PriorizationOfferHfc(List<ProductFixedDTO> filteredProductList, List<ProductFixedDTO> downselList, String productType, Integer typeTotalization, Double arpa, List<ParamMovTotal> listPmt) {
		ParamMovTotal pmt = new ParamMovTotal();
		for (ProductFixedDTO productFixedDTO : filteredProductList) {
		    if (SuscriptionType.TRIO.getCode().equals(productType)) {
		    	if(typeTotalization==1) {
		    		//if (productFixedDTO.getJump() >= -30 && productFixedDTO.getGap_velocity()>=0) {
		    		pmt = parametro(listPmt, Constant.UPOHTT);
		    		if (productFixedDTO.getJump() >= Double.parseDouble(pmt.getValor()) && productFixedDTO.getGap_velocity()>=Double.parseDouble(pmt.getCol1())) {
						downselList.add(productFixedDTO);
					}
		    	}
		    	else if(typeTotalization==2) {
		    		//if (arpa < 325) {
		    			pmt = parametro(listPmt, Constant.UMBFIJO);
		    		if (arpa < Double.parseDouble(pmt.getValor())) {
		    			//if (productFixedDTO.getJump() >= 0 && productFixedDTO.getGap_velocity()>= -20) {
		    			pmt = parametro(listPmt, Constant.UPOHTF);
		    			if (productFixedDTO.getJump() >= Double.parseDouble(pmt.getValor()) && productFixedDTO.getGap_velocity()>= Double.parseDouble(pmt.getCol1())) {
							downselList.add(productFixedDTO);
						}
		    		}
		    		else {
		    			pmt = parametro(listPmt, Constant.UPOHTF);
		    			if (productFixedDTO.getJump() >= Double.parseDouble(pmt.getCol2()) && productFixedDTO.getGap_velocity()>= Double.parseDouble(pmt.getCol3())) {
							downselList.add(productFixedDTO);
						}
		    		}
		    	}
			} else if (SuscriptionType.DUO.getCode().equals(productType)) {
				if (typeTotalization==1) {
					pmt = parametro(listPmt, Constant.UPOHDT);
					if (productFixedDTO.getJump() >= Double.parseDouble(pmt.getValor()) && productFixedDTO.getGap_velocity()>= Double.parseDouble(pmt.getCol1())) {
						downselList.add(productFixedDTO);
					}
				}
				else if(typeTotalization==2) {
					pmt = parametro(listPmt, Constant.UPOHDF);
					if (productFixedDTO.getJump() >= Double.parseDouble(pmt.getValor()) && productFixedDTO.getGap_velocity()>= Double.parseDouble(pmt.getCol1())) {
						downselList.add(productFixedDTO);
					}
				}
				
			} else if (SuscriptionType.DUOINTERNET.getCode().equals(productType)) {
				if (typeTotalization==1) {
					pmt = parametro(listPmt, Constant.UPOHDBT);
					if (productFixedDTO.getJump() >= Double.parseDouble(pmt.getValor()) && productFixedDTO.getGap_velocity()>= Double.parseDouble(pmt.getCol1())) {
						downselList.add(productFixedDTO);
					}
				}
				else if(typeTotalization==2) {
					pmt = parametro(listPmt, Constant.UPOHDBF);
					if (productFixedDTO.getJump() >= Double.parseDouble(pmt.getValor()) && productFixedDTO.getGap_velocity()>= Double.parseDouble(pmt.getCol1())) {
						downselList.add(productFixedDTO);
					}
				}
			} 
			else if (SuscriptionType.DUOCABLE.getCode().equals(productType)) {
				if (typeTotalization==1) {
					pmt = parametro(listPmt, Constant.UPOHDTT);
					if (productFixedDTO.getJump() >= Double.parseDouble(pmt.getValor()) && productFixedDTO.getGap_velocity()>= Double.parseDouble(pmt.getCol1())) {
						downselList.add(productFixedDTO);
					}
				}
				else if(typeTotalization==2) {
					pmt = parametro(listPmt, Constant.UPOHDTF);
					if (productFixedDTO.getJump() >= Double.parseDouble(pmt.getValor()) && productFixedDTO.getGap_velocity()>= Double.parseDouble(pmt.getCol1())) {
						downselList.add(productFixedDTO);
					}
				}
			}
			else {
				pmt = parametro(listPmt, Constant.UPOHMLT);
				if (productFixedDTO.getJump() >= Double.parseDouble(pmt.getValor()) && productFixedDTO.getGap_velocity()>= Double.parseDouble(pmt.getCol1())) {
					downselList.add(productFixedDTO);
				}
			}
		}
		/**
		 * Validando si aplico a alguna priorización principal,
		 * sino se llama la función de priorización por default
		 */
		if(downselList.size()==0) {
			PriorizationOfferDefault(filteredProductList,downselList,arpa,listPmt);
		}
	}
	
	/**
	 * 
	 * @param filteredProductList
	 * @param downselList
	 * @param productType
	 * @param typeTotalization
	 * @param arpa
	 * 
	 * Función creada para las priorizaciones de ofertas con cobertura FTTH
	 */
	public static void PriorizationOfferFtth(List<ProductFixedDTO> filteredProductList, List<ProductFixedDTO> downselList, String productType, Integer typeTotalization, Double arpa, List<ParamMovTotal> listPmt) {
		ParamMovTotal rango = new ParamMovTotal();
		ParamMovTotal umbral = new ParamMovTotal();
		
		umbral = parametro(listPmt, Constant.UMBFIJOFTTH);
		for (ProductFixedDTO productFixedDTO : filteredProductList) {		
			if (SuscriptionType.TRIO.getCode().equals(productType)) {			
				rango = parametro(listPmt, Constant.UPFT);
	    		if (arpa <= Double.parseDouble(umbral.getValor())) {
		    		if (productFixedDTO.getJump() >= Double.parseDouble(rango.getValor()) && productFixedDTO.getGap_velocity()>=Double.parseDouble(rango.getCol1())) {
						downselList.add(productFixedDTO);
					}
	    		}
	    		else if(arpa > Double.parseDouble(umbral.getValor()) && arpa <= Double.parseDouble(umbral.getCol1())) {
	    			if (productFixedDTO.getJump() >= Double.parseDouble(rango.getCol2()) && productFixedDTO.getGap_velocity()>=Double.parseDouble(rango.getCol3())) {
						downselList.add(productFixedDTO);
					}
	    		}
	    		else if(arpa > Double.parseDouble(umbral.getCol1())) {
	    			if (productFixedDTO.getJump() >= Double.parseDouble(rango.getCol4()) && productFixedDTO.getGap_velocity()>=Double.parseDouble(rango.getCol5())) {
						downselList.add(productFixedDTO);
					}
	    		}
				
			}
			
			else if (SuscriptionType.DUO.getCode().equals(productType)) {		
				rango = parametro(listPmt, Constant.UPFD);
	    		if (arpa <= Double.parseDouble(umbral.getValor())) {
		    		if (productFixedDTO.getJump() >= Double.parseDouble(rango.getValor()) && productFixedDTO.getGap_velocity()>=Double.parseDouble(rango.getCol1())) {
						downselList.add(productFixedDTO);
					}
	    		}
	    		else if(arpa > Double.parseDouble(umbral.getValor()) && arpa <= Double.parseDouble(umbral.getCol1())) {
	    			if (productFixedDTO.getJump() >= Double.parseDouble(rango.getCol2()) && productFixedDTO.getGap_velocity()>=Double.parseDouble(rango.getCol3())) {
						downselList.add(productFixedDTO);
					}
	    		}
	    		else if(arpa > Double.parseDouble(umbral.getCol1())) {
	    			if (productFixedDTO.getJump() >= Double.parseDouble(rango.getCol4()) && productFixedDTO.getGap_velocity()>=Double.parseDouble(rango.getCol5())) {
						downselList.add(productFixedDTO);
					}
	    		}
				
			}
			
			else if (SuscriptionType.DUOINTERNET.getCode().equals(productType)) {	
				rango = parametro(listPmt, Constant.UPFDB);
	    		if (arpa <= Double.parseDouble(umbral.getValor())) {
		    		if (productFixedDTO.getJump() >= Double.parseDouble(rango.getValor()) && productFixedDTO.getGap_velocity()>=Double.parseDouble(rango.getCol1())) {
						downselList.add(productFixedDTO);
					}
	    		}
	    		else if(arpa > Double.parseDouble(umbral.getValor()) && arpa <= Double.parseDouble(umbral.getCol1())) {
	    			if (productFixedDTO.getJump() >= Double.parseDouble(rango.getCol2()) && productFixedDTO.getGap_velocity()>=Double.parseDouble(rango.getCol3())) {
						downselList.add(productFixedDTO);
					}
	    		}
	    		else if(arpa > Double.parseDouble(umbral.getCol1())) {
	    			if (productFixedDTO.getJump() >= Double.parseDouble(rango.getCol4()) && productFixedDTO.getGap_velocity()>=Double.parseDouble(rango.getCol5())) {
						downselList.add(productFixedDTO);
					}
	    		}
				
			}
			
			else if (SuscriptionType.DUOCABLE.getCode().equals(productType)) {
				rango = parametro(listPmt, Constant.UPFDT);
	    		if (arpa <= Double.parseDouble(umbral.getValor())) {
		    		if (productFixedDTO.getJump() >= Double.parseDouble(rango.getValor()) && productFixedDTO.getGap_velocity()>=Double.parseDouble(rango.getCol1())) {
						downselList.add(productFixedDTO);
					}
	    		}
	    		else if(arpa > Double.parseDouble(umbral.getValor()) && arpa <= Double.parseDouble(umbral.getCol1())) {
	    			if (productFixedDTO.getJump() >= Double.parseDouble(rango.getCol2()) && productFixedDTO.getGap_velocity()>=Double.parseDouble(rango.getCol3())) {
						downselList.add(productFixedDTO);
					}
	    		}
	    		else if(arpa > Double.parseDouble(umbral.getCol1())) {
	    			if (productFixedDTO.getJump() >= Double.parseDouble(rango.getCol4()) && productFixedDTO.getGap_velocity()>=Double.parseDouble(rango.getCol5())) {
						downselList.add(productFixedDTO);
					}
	    		}
				
			}
			
			else {
				rango = parametro(listPmt, Constant.UPOFMLF);
	    		if (arpa <= Double.parseDouble(umbral.getValor())) {
		    		if (productFixedDTO.getJump() >= Double.parseDouble(rango.getValor()) && productFixedDTO.getGap_velocity()>=Double.parseDouble(rango.getCol1())) {
						downselList.add(productFixedDTO);
					}
	    		}
	    		else if(arpa > Double.parseDouble(umbral.getValor()) && arpa <= Double.parseDouble(umbral.getCol1())) {
	    			if (productFixedDTO.getJump() >= Double.parseDouble(rango.getCol2()) && productFixedDTO.getGap_velocity()>=Double.parseDouble(rango.getCol3())) {
						downselList.add(productFixedDTO);
					}
	    		}
	    		else if(arpa > Double.parseDouble(umbral.getCol1())) {
	    			if (productFixedDTO.getJump() >= Double.parseDouble(rango.getCol4()) && productFixedDTO.getGap_velocity()>=Double.parseDouble(rango.getCol5())) {
						downselList.add(productFixedDTO);
					}
	    		}
				
			}
		}
		
		
		
		/**
		 * Validando si aplico a alguna priorización principal,
		 * sino se llama la función de priorización por default
		 */
		if(downselList.size()==0) {
			PriorizationOfferDefault(filteredProductList,downselList,arpa,listPmt);
		}
	}
	
	/**
	 * 
	 * @param filteredProductList
	 * @param downselList
	 * @param productType
	 * @param typeTotalization
	 * @param arpa
	 * 
	 * Función creada para las priorizaciones de ofertas Móviles
	 */
	public static void PriorizationOfferMobile(List<ProductFixedDTO> filteredProductList, List<ProductFixedDTO> downselList, String productType, Integer typeTotalization, Double arpa, List<ParamMovTotal> listPmt) {
		ParamMovTotal pmt = new ParamMovTotal();
		pmt = parametro(listPmt, Constant.UPOFM);
		for (ProductFixedDTO productFixedDTO : filteredProductList) {			
				if (productFixedDTO.getJump() >Double.parseDouble(pmt.getValor()) && productFixedDTO.getGap_velocity()>Double.parseDouble(pmt.getCol1())) {
					downselList.add(productFixedDTO);
				}

		}
		/**
		 * Validando si aplico a alguna priorización principal,
		 * sino se llama la función de priorización por default
		 */
		if(downselList.size()==0) {
			PriorizationOfferDefault(filteredProductList,downselList,arpa,listPmt);
		}
	}
	
	/**
	 * 
	 * @param filteredProductList
	 * @param downselList
	 * @param productType
	 * @param typeTotalization
	 * @param arpa
	 * 
	 * Función creada para las priorizaciones de ofertas Móviles
	 */
	public static void PriorizationOfferDefault(List<ProductFixedDTO> filteredProductList, List<ProductFixedDTO> downselList, Double arpa, List<ParamMovTotal> listPmt) {
		double arpaInicial = 0;
		double arpaFinal = 0;
		int cantOfer =  filteredProductList.size();
		boolean validaArpaFinal = false;
		Collections.sort(filteredProductList, ProductFixedDTO.rentComparator);
		
		for (int i=0; i<cantOfer; i++) {
			if(i+1 >= cantOfer) break;
			if(arpa>arpaInicial && arpa<=filteredProductList.get(i+1).getPackageRent()) {
				arpaFinal = filteredProductList.get(i).getPackageRent();
				validaArpaFinal = true;
				break;
			}
			else {
				arpaInicial = filteredProductList.get(i).getPackageRent();
			}
		}
		
		if(!validaArpaFinal) {
			arpaFinal = filteredProductList.get(cantOfer-1).getPackageRent();
		}
		
		
		for (ProductFixedDTO productFixedDto : filteredProductList) {
			if(productFixedDto.getPackageRent()>=arpaFinal) {
				downselList.add(productFixedDto);
			}
		}
		
		if(downselList.size()==0) {
			for (int i=filteredProductList.size()-1; i>=0; i--) {
				if(filteredProductList.get(i).getPackageRent()<arpaFinal) {
					downselList.add(filteredProductList.get(i));
					break;
				}
			}
		}

	}
	
	private static ParamMovTotal parametro(List<ParamMovTotal> lstPmtr, String code) {
		ParamMovTotal parametro = new ParamMovTotal();
		for(ParamMovTotal pmt: lstPmtr) {
		    if(!(pmt.getCodValor()==null)) {
			if(pmt.getCodValor().equals(code)) {
				parametro = pmt;
				break;
			}
		    }
		}
		
		return parametro;
	}

}
