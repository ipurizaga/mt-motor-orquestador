package com.telefonica.total.common.util;

public final class Constant {

    public static final String	  FIX			 = "fijo";
    public static final String	  MOBILE		 = "movil";
    public static final String	  TRUE			 = "true";
    public static final String	  FALSE			 = "false";
    public static final String	  SEPARATORCOMM		 = ",";
    public static final String	  PIPE			 = "|";
    public static final String	  PPF			 = "ppf";
    public static final Double	  CERO			 = 0D;
    public static final Integer	  CERO_INT		 = 0;
    public static final String	  SP_NAME_LSTPLANDEVICE	 = "listPlanDevice";
    public static final String	  FU_NAME_LSTPLANFINANCING	 = "getPlanFinancing";
    public static final String	  FU_NAME_UPFRONT_COUNTED	 = "getUpFrontCounted";
    public static final String	  FU_NAME_UPFRONT_FINANCED	 = "getUpFrontFinanced";
    public static final String	  CURRENT_OFFER_TYPE	 = "Producto Actual";
    public static final String	  PRODUCT_FIXED_FOX	 = "FOX";
    public static final String	  PRODUCT_FIXED_HBO	 = "HBO";
    public static final String	  PRODUCT_FIXED_HD	 = "HD";
    public static final String	  PRODUCT_ESTELLAR	 = "EST";
    public static final String	  PRODUCT_MOVIECITY	 = "MC";
    public static final String	  PRODUCT_GOLDPREM	 = "GP";
    public static final String	  PRODUCT_HTPACK	 = "HP";
    public static final Character YES			 = 'Y';
    public static final Character NOT			 = 'N';
    public static final String	  BUSINESS_ERROR	 = "Business Error";
    public static final String	  TECHNICAL_ERROR	 = "Technical Error";
    public static final String	  APP_MSG_BUSINESS_ERROR = "BadRequest Error";
    public static final String	  CAUSE_BUSINESS_ERROR	 = "Excepción ó salida del flujo de negocio.";
    public static final String	  CAUSE_SERVER_ERROR	 = "Excepción inesperada.";
    public static final String	  CAUSE_VALIDATION_ERROR = "Solicitud Incorrecta";
    public static final String	  GENERIC_SERVER_FAULT	 = "Generic Server Fault";
    public static final String	  EXCEPTION_CATEGORY	 = "SVC";
    public static final String	  EXCEPTION_SEVERITY	 = "E";
    public static final String	  GVS_POS1		 = "POS1";
    public static final Character ZERO			 = '0';
    public static final Character ONE			 = '1';
    public static final String	  ONES			 = "1";
    public static final Character TWO			 = '2';
    public static final Integer	  ONE_INT		 = 1;
    public static final Integer	  TWO_INT		 = 2;
    public static final Double	  CONSUMPTION_PERCENTAGE = 0.5;
    public static final Integer	  DEFAULT_PERMANENCY	 = 12;
    public static final String	  TRUE_MAYUS		 = "TRUE";

    /** Velocidades de Internet **/
    public static final Integer	VELOCIDAD_30MB	= 30000;
    public static final Integer	VELOCIDAD_40MB	= 40000;
    public static final Integer	VELOCIDAD_60MB	= 60000;
    public static final Integer	VELOCIDAD_100MB	= 100000;
    public static final Integer	VELOCIDAD_120MB	= 120000;
    public static final Integer	VELOCIDAD_200MB	= 200000;

    /** Codigo de evaluacion producto MT **/
    public static final Integer	ALTA_NUEVA     = 1;
    public static final Integer	GESTION_PLANTA = 2;
    public static final Integer	AVERIA_TIPO1   = 3;
    public static final Integer	AVERIA_TIPO2   = 4;
    public static final Integer	AVERIA_TIPO3   = 5;

    /** Codigos de errores funcionales **/
    public static final String GENERIC = "0000";
    public static final String BE_1050 = "1050";
    public static final String BE_1051 = "1051";
    public static final String BE_1052 = "1052";
    public static final String BE_1053 = "1053";
    public static final String BE_1054 = "1054";
    public static final String BE_1055 = "1055";
    public static final String BE_1056 = "1056";

    public static final String BE_1057 = "1057";
    public static final String BE_1058 = "1058";
    public static final String BE_1059 = "1059";
    public static final String BE_1060 = "1060";
    public static final String BE_1061 = "1061";
    public static final String BE_1062 = "1062";
    public static final String BE_1063 = "1063";
    public static final String BE_1064 = "1064";
    public static final String BE_1065 = "1065";
    public static final String BE_1066 = "1066";
    public static final String BE_1067 = "1067";
    
    public static final String BE_1068 = "1068";
    public static final String BE_1069 = "1069";
    public static final String BE_1070 = "1070";
    public static final String BE_1071 = "1071";
    public static final String BE_1073 = "1073";
    public static final String BE_1074 = "1074";
    public static final String BE_1075 = "1075";
    
    public static final String BE_1080 = "1080";
    public static final String BE_1081 = "1081";
    
    /** Codigos errores funcionales Stored Procedure **/
    public static final String BE_2021 = "2021";
    public static final String BE_2022 = "2022";
    public static final String BE_2023 = "2023";
    public static final String BE_2024 = "2024";
    public static final String BE_2025 = "2025";
    public static final String BE_2026 = "2026";
    public static final String BE_2027 = "2027";
    public static final String BE_2028 = "2028";
    public static final String BE_2029 = "2029";
    public static final String BE_2030 = "2030";

    /** Gestión de logs y entornos **/
    public static final String NEW_LINE		      = System.getProperty("line.separator");
    public static final String URL_LABEL	      = "[URL]: ";
    public static final String REQUEST_METHOD_LABEL   = "[Request Method]: ";
    public static final String CONTROLLER_LABEL	      = "[Controller]: ";
    public static final String CLASS_LOG_LABEL	      = "[Class]: ";
    public static final String METHOD_LOG_LABEL	      = "[Method]: ";
    public static final String PARAMETERS_LOG_LABEL   = "(...)";
    public static final String INPUT_PARAMETERS_LABEL = "[Input Params]: ";
    public static final String INPUT_PARAMETER_LABEL  = "[Input]: ";
    public static final String OUTPUT_LABEL	      = "[Output]: ";
    public static final String SEPARATOR	      = "===================================================================================================================================================================================";
    public static final String EXCEPTION_WAS_THROWN   = "-> An exception was thrown by method: ";
    public static final String PARSE_JSON_RESPONSE    = "[It couldn't parse to JSON the method's response (Please check the log)]";
    public static final String DEV_ENVIRONMENT	      = "dev";
    public static final String PROD_ENVIRONMENT	      = "prod";

    public static final String DEFAULT_NUMBER_FIXED    = "9999999";
    public static final String DEFAULT_NUMBER_MOBILE   = "000000000";
    public static final String DEFAULT_DOCUMENT_NUMBER = "00000000";

    public static final String CAMPANIA_DEFAULT1 = "MOVISTAR TOTAL";
    
    public static final String GROUP_PARAM_PS = "PS";
    public static final String GROUP_PARAM_DESC_TEMP_CAMPANIA = "DESC_TEMP_CAMPANIA";

    /** date format **/
    public static final String TIME = "HH:mm:ss";
    public static final String DATE = "dd/MM/yy";
    
    /** Flujos priorizaciones gestion planta **/
    public static final Integer FLUJO_REGULAR = 1;
    public static final Integer FLUJO_MIGRACION_PARRILLA = 2;
    public static final Integer FLUJO_INEXISTENTE = 3;
    
    public static final String TIPO_PAQUETE_MT    = "4Play";
    public static final String TIPO_MT    = "mtm";
    public static final String TIPO_PAQUETE_FIJO  = "Trio";
    public static final String TIPO_PAQUETE_MOVIL = "Movil";
    public static final String TIPO_LMA = "LMA";
    
    /** PO_CODEs **/
    public static final String POCODE_LMA = "LINEAMOVILADICIONAL";
    
    /** Tipo Operacion **/
    public static final String TIPO_OPERACION_PORTABILITY = "Portability";
    
    /**  Para los parametros del PLAN EQUIPO MONTOS **/
    public static final String CAMPO_DNR_ORP_PRD_TP = "DNR_ORP_PRD_TP";

    /**  Para los parametros de Instalacion **/
    public static final String NOMBRE_COSTO = "COSTO";
    public static final String NOMBRE_CUOTA = "CUOTA";
    
    /**  Filtros para parrilla **/
    public static final String PARRILLA_1 = "P1";
    public static final String PARRILLA_2 = "P2";
    public static final String PARRILLA_3 = "P3";

    /**  Para los parametros de INSTALACION_CUOTAS e INSTALACION_COSTO **/
    public static final Integer INSTALACION_CUOTAS = 6;
    public static final Double INSTALACION_COSTO = 90D;
        
    /*TYPE SUSCRIBER*/
    public static final String TYPE_LMA = "lma";
    public static final String TYPE_MTM = "mtm";
    public static final String TYPE_MTF = "mtf";
    
    public static final String Upsell = "Upsell";
    public static final String Downsell = "Downsell";
    
    public static final String UP = "UP";
    public static final String DOWN = "DOWN";
    
    /*Operation*/    
    public static final String OPERATION_ALTA = "provide";
    public static final String OPERATION_PORTABILITY = "portability";
    public static final String OPERATION_REPLACEOFFER = "replaceOffer";
    
    /** Para los parámetros de UMBRAL de TECNOLOGIA **/
    public static final String UMBTECNO = "um_tecn";
    
    /** Para los parámetros de UMBRAL de PRIORIDADES **/
    //UMBRAL FIJO
    public static final String UMBFIJO = "um_fijo";
    public static final String UMBFIJOFTTH = "um_fijo_ftth";
    
    //HFC
    public static final String UPOHTT = "um_hfc_trio_tot";
    public static final String UPOHTF = "um_hfc_trio_fij";
    public static final String UPOHDT = "um_hfc_duo_tot";
    public static final String UPOHDF = "um_hfc_duo_fij";
    public static final String UPOHDBT = "um_hfc_duoba_tot";
    public static final String UPOHDBF = "um_hfc_duoba_fij";
    public static final String UPOHDTT = "um_hfc_duotv_tot";
    public static final String UPOHDTF = "um_hfc_duotv_fij";
    public static final String UPOHMBT = "um_hfc_monoba_tot";
    public static final String UPOHMBF = "um_hfc_monoba_fij";
    public static final String UPOHMTT = "um_hfc_monotv_tot";
    public static final String UPOHMFT = "um_hfc_monotv_fij";
    public static final String UPOHMLT = "um_hfc_monolin_tot";
    public static final String UPOHMLF = "um_hfc_monolin_fij";
    public static final String UPOHM = "um_hfc_movil";

    //FTTH
    public static final String UPOFTT = "um_ftth_trio_tot";
    public static final String UPOFTF = "um_ftth_trio_fij";
    public static final String UPOFDT = "um_ftth_duo_tot";
    public static final String UPOFDF = "um_ftth_duo_fij";
    public static final String UPOFDBT = "um_ftth_duoba_tot";
    public static final String UPOFDBF = "um_ftth_duoba_fij";
    public static final String UPOFDTT = "um_ftth_duotv_tot";
    public static final String UPOFDTF = "um_ftth_duotv_fij";
    public static final String UPOFMBT = "um_ftth_monoba_tot";
    public static final String UPOFMBF = "um_ftth_monoba_fij";
    public static final String UPOFMTT = "um_ftth_monotv_tot";
    public static final String UPOFMFT = "um_ftth_monotv_fij";
    public static final String UPOFMLT = "um_ftth_monolin_tot";
    public static final String UPOFMLF = "um_ftth_monolin_fij";
    public static final String UPOFM = "um_ftth_movil";
    //NEW OVERLAY
    public static final String UPFT = "um_ftth_trio";
    public static final String UPFD = "um_ftth_duo";
    public static final String UPFDB = "um_ftth_duoba";
    public static final String UPFDT = "um_ftth_duotv";
    
    public static final String ACTUAL_TECHNOLOGY = "actual_technology";
    
    public static final String PACK_BENEFIT = "paq_benefit";
    
    /** DESCUENTO TEMPORAL CAMPAÑA **/
    public static final String DESC_TEMP_CAMP = "DESC_TEMP_CAMPANIA";
    public static final String ARPA_VELOCIDAD = "ARPA_VELOCIDAD";
    public static final String UPFRONT = "upfront";
    public static final String PRODUCTO_PLAYA = "producto_playa";
    public static final String DEFAULT = "defecto";
    public static final String PRIORITY = "prioridad";
 
    private Constant() {
    }
}
