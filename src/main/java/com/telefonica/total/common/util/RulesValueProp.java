package com.telefonica.total.common.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.stereotype.Service;

import lombok.Getter;

@Getter
@PropertySources({ @PropertySource("classpath:rules-value.properties") })
@ConfigurationProperties
@Service
public class RulesValueProp {

    @Value(value = "${device.postpagofacil}")
    public String PPF;

    @Value(value = "${fixedrules.internet-cobertura-radioenlace-uno}")
    public Character INTERNET_COVERAGE_RADIO_LINK_ONE;

    @Value(value = "${fixedrules.linea-presente-cero}")
    public Character PRESENT_LINE_CERO;

    @Value(value = "${fixedrules.linea-presente-uno}")
    public Character PRESENT_LINE_ONE;

    @Value(value = "${fixedrules.comportamiento-pago-covergente-cero}")
    public Integer BEHAVIOR_CONVERGENT_PAYMENT_CERO;

    @Value(value = "${fixedrules.paquete-tipo-4play}")
    public String TYPE_PACKAGE_4PLAY;

    @Value(value = "${fixedrules.scoring-fijo-rechazar}")
    public String FIXED_SCORING_REJECT;

    @Value(value = "${fixedrules.internet-tecnologia-ftth}")
    public String INTERNET_TECNOLOGIA_FTTH;

    @Value(value = "${fixedrules.internet-tecnologia-hfc}")
    public String INTERNET_TECNOLOGIA_HFC;

    @Value(value = "${fixedrules.internet-tecnologia-adsl}")
    public String INTERNET_TECNOLOGIA_ADSL;

    @Value(value = "${fixedrules.internet-cobertura-cero}")
    public String INTERNET_COVERAGE_CERO;

    @Value(value = "${fixedrules.internet-tecnologia-cero}")
    public String INTERNET_TENCOLOGY_CERO;

    @Value(value = "${fixedrules.cable-presente-uno}")
    public Character CABLE_PRESENT_ONE;

    @Value(value = "${fixedrules.producto-salto-cero}")
    public Integer PRODUCT_JUMP_CERO;

    @Value(value = "${fixedrules.producto-caracteristica}")
    public String PRODUCT_CHARACTERISTIC;
    
    @Value(value = "${fixedrules.netflix-periodo}")
    public String NETFLIX_PERIOD;
    
    @Value(value = "${fixedrules.producto-indicador-oferta}")
    public String PRODUCT_IND_OFFER;
    
    @Value(value = "${fixedrules.producto-indicador-oferta-down}")
    public String PRODUCT_IND_OFFER_DOWN;
    
    @Value(value = "${fixedrules.producto-indicador-oferta-up}")
    public String PRODUCT_IND_OFFER_UP;

    @Value(value = "${fixedrules.producto-migracion-tecnologica}")
    public String PRODUCT_MIGRATION_TECHNOLOGY;
    
    @Value(value = "${fixedrules.producto-caracteristica-downsell}")
    public String PRODUCT_CHARACTERISTIC_DOWNSELL;
    
    @Value(value = "${fixedrules.producto-caracteristica-upsell}")
    public String PRODUCT_CHARACTERISTIC_UPSELL;

    @Value(value = "${fixedrules.producto-caracteristica-menor-velocidad}")
    public String PRODUCT_CHARACTERISTIC_MINOR_VELOCITY;

    @Value(value = "${fixedrules.producto-caracteristica-menor-consumo}")
    public String PRODUCT_CHARACTERISTIC_MINOR_CONSUMPTION;
    
    @Value(value = "${fixedrules.producto-caracteristica-producto-actual}")
    public String PRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT;
    
    @Value(value = "${fixedrules.producto-caracteristica-producto-playa}")
    public String PRODUCT_CHARACTERISTIC_BEACH_PRODUCT;
    
    @Value(value = "${fixedrules.producto-caracteristica-deshabilitada}")
    public String PRODUCT_CHARACTERISTIC_DISABLED;
    
    @Value(value = "${fixedrules.producto-caracteristica-oferta-espejo}")
    public String PRODUCT_CHARACTERISTIC_MIRROR_OFFER;

    @Value(value = "${fixedrules.modo-negociacion.venta}")
    public String NEGOTIATION_MODE_SALE;

    @Value(value = "${fixedrules.modo-negociacion.retencion}")
    public String NEGOTIATION_MODE_RETENTION;

    @Value(value = "${fixedrules.flag-regla-deco-uno}")
    public Character FLAG_RULE_DECO_ONE;

    @Value(value = "${fixedrules.flag-regla-deco-cero}")
    public Character FLAG_RULE_DECO_CERO;

    @Value(value = "${fixedrules.flag-regla-modem-uno}")
    public Character FLAG_RULE_MODEM_ONE;

    @Value(value = "${fixedrules.flag-regla-modem-cero}")
    public Character FLAG_RULE_MODEM_CERO;

    @Value(value = "${fixedrules.flag-cantidad-deco-seis}")
    public Integer DECO_QUANTITY_SIX;

    @Value(value = "${fixedrules.deco.equipamiento.2hd}")
    public String DECO_EQUIPMENT_2HD;

    @Value(value = "${fixedrules.deco.equipamiento.1hd}")
    public String DECO_EQUIPMENT_1HD;

    @Value(value = "${fixedrules.deco.equipamiento.smarthd}")
    public String DECO_EQUIPMENT_SMART_HD;

    @Value(value = "${fixedrules.deco.equipamiento.smart}")
    public String DECO_EQUIPMENT_SMART;

    @Value(value = "${fixedrules.modem.equipamiento.adsl}")
    public String MODEM_EQUIPMENT_ADSL;

    @Value(value = "${fixedrules.modem.equipamiento.smartwifi}")
    public String MODEM_EQUIPMENT_SMART_WIFI;
    
    @Value(value = "${fixedrules.modem.equipamiento.repetidor-smart-wifi}")
    public String REAPEATER_SMART_WIFI;

    @Value(value = "${fixedrules.modem.equipamiento.docsis2}")
    public String MODEM_EQUIPMENT_DOCSIS2;

    @Value(value = "${fixedrules.modem.equipamiento.docsis3}")
    public String MODEM_EQUIPMENT_DOCSIS3;

    @Value(value = "${fixedrules.modem.equipamiento.dualband}")
    public String MODEM_EQUIPMENT_DUALBAND;

    @Value(value = "${fixedrules.modem.equipamiento.gpon}")
    public String MODEM_EQUIPMENT_GPON;

    @Value(value = "${fixedrules.present-rent.adicionalhd}")
    public String RENT_ADITIONAL_HD;

    @Value(value = "${fixedrules.present-rent.adicionalhbo}")
    public String RENT_ADITIONAL_HBO;

    @Value(value = "${fixedrules.present-rent.adicionalfox}")
    public String RENT_ADITIONAL_FOX;
    
    @Value(value = "${fixedrules.present-rent.adicionalestelar}")
    public String RENT_ADITIONAL_ESTELLAR;
    
    @Value(value = "${fixedrules.present-rent.adicionalgoldprem}")
    public String RENT_ADITIONAL_GOLDPREM;
    
    @Value(value = "${fixedrules.present-rent.adicionalhtpack}")
    public String RENT_ADITIONAL_HTPACK;
    
    @Value(value = "${fixedrules.present-rent.adicionalmultidestino}")
    public String RENT_ADITIONAL_MULTIDESTINO;
    
    @Value(value = "${fixedrules.present-rent.adicionalmantenimiento}")
    public String RENT_ADITIONAL_MANTENIMIENTO;

    @Value(value = "${fixedrules.present-rent.adicionaldecosmart}")
    public String RENT_ADITIONAL_DECO_SMART;

    @Value(value = "${fixedrules.present-rent.puntoadicionalhd}")
    public String RENT_ADITIONAL_POINT_HD;

    @Value(value = "${fixedrules.present-rent.adicionalultrawifi}")
    public String RENT_ADITIONAL_ULTRA_WIFI;

    @Value(value = "${fixedrules.present-rent.adicionalseguridadtotal}")
    public String RENT_ADITIONAL_TOTAL_SECURITY;

    @Value(value = "${fixedrules.present-rent.adicionalotros}")
    public String RENT_ADITIONAL_OTHERS;

    @Value(value = "${fixedrules.bloque-mantener.hd}")
    public String BLOCK_HD_KEEP;

    @Value(value = "${fixedrules.bloque-mantener.hbo}")
    public String BLOCK_HBO_KEEP;

    @Value(value = "${fixedrules.bloque-mantener.fox}")
    public String BLOCK_FOX_KEEP;
    
    @Value(value = "${fixedrules.bloque-mantener.estelar}")
    public String BLOCK_EST_KEEP;
    
    @Value(value = "${fixedrules.bloque-mantener.goldprem}")
    public String BLOCK_GOLDPREM_KEEP;
    
    @Value(value = "${fixedrules.bloque-mantener.htpack}")
    public String BLOCK_HTPACK_KEEP;

    @Value(value = "${fixedrules.renta-mantener.rentadecosmart}")
    public String RENT_DECO_SMART_KEEP;

    @Value(value = "${fixedrules.renta-mantener.rentapuntoadicionalhd}")
    public String RENT_ADDITIONAL_POINT_KEEP;

    @Value(value = "${fixedrules.renta-mantener.rentaultrawifi}")
    public String RENT_ULTRA_WIFI_KEEP;

    @Value(value = "${fixedrules.renta-mantener.rentaseguriodadtotal}")
    public String RENT_TOTAL_SECURITY_KEEP;
    
    @Value(value = "${fixedrules.renta-mantener.rentamultidestino}")
    public String RENT_MULTIDESTINO_KEEP;
    
    @Value(value = "${fixedrules.renta-mantener.rentamantenimiento}")
    public String RENT_MANTENIMIENTO_KEEP;

    @Value(value = "${fixedrules.renta-mantener.rentaotros}")
    public String RENT_OTHERS_KEEP;

    @Value(value = "${fixedrules.ppfIndicator.true}")
    public Character PPF_INDICATOR_TRUE;

    @Value(value = "${fixedrules.ppfIndicator.false}")
    public Character PPF_INDICATOR_FALSE;

    @Value(value = "${fixedrules.ppfIndicator.true}")
    public Integer TITULAR_TRUE;

    @Value(value = "${fixedrules.ppfIndicator.false}")
    public Integer TITULAR_FALSE;

    @Value(value = "${fixedrules.producto-velocidad-fija-limite}")
    public Integer LIMIT_FIXED_VELOCITY;

    @Value(value = "${fixedrules.producto-consumo-mobil-limite}")
    public Integer LIMIT_MOBILE_CONSUME;

    @Value(value = "${fixedrules.rent199.arpalimit.value}")
    public Double RENTA199_ARPA_LIMIT;
    
    @Value(value = "${fixedrules.rent199.packageRentlimit.value}")
    public Double RENTA199_PACKAGERENT_LIMIT;

    @Value(value = "${flush.cache.user}")
    public String USER;

    @Value(value = "${flush.cache.password}")
    public String PASSWORD;

}
