package com.telefonica.total.common.util;

import java.util.ArrayList;
import java.util.List;

import com.telefonica.total.enums.ProductType;
import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.exception.BusinessException;
import com.telefonica.total.generic.CommOperCollection;
import com.telefonica.total.model.CatalogCalPreSimcard;
import com.telefonica.total.model.CatalogFouthDigitPPF;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.Subscription;

public class UtilCollections {

    private UtilCollections() {
    }

    public static CommercialOperation getMobileForQueryDevice(List<CommercialOperation> suscriptions) {
	CommOperCollection<CommercialOperation> lstCommOper = new CommOperCollection<>();
	lstCommOper.addAll(suscriptions);
	CommercialOperation commOpera = lstCommOper.filterProvideMobile(ProductType.MOBILE);
	if (commOpera != null) {
	    return commOpera;
	} else {
	    throw new BusinessException("No existe ningun número en request");
	}
    }

    public static CommOperCollection<CommercialOperation> getCollectionByType(List<CommercialOperation> suscriptions,
	    String suscriptionType) {
	CommOperCollection<CommercialOperation> lstCommOper = new CommOperCollection<>(suscriptions);
	lstCommOper.filterByProduct(SuscriptionType.getSuscriptionType(suscriptionType));
	if (!lstCommOper.isEmpty()) {
	    return lstCommOper;
	} else {
	    throw new BusinessException("No existe ningun número en request");
	}
    }

    /**
     * metodo que busca el objeto del 4to digito ppf segun el 4 digito del score
     * 
     * @param lstCatalogFourth
     * @param fourthDigit
     * @return
     */
    public static CatalogFouthDigitPPF getFourthDigit(List<CatalogFouthDigitPPF> lstCatalogFourth, String fourthDigit) {
	for (CatalogFouthDigitPPF catalogFourth : lstCatalogFourth) {
	    if (fourthDigit.equalsIgnoreCase(catalogFourth.getFourthDigitScore())) {
		return catalogFourth;
	    }
	}
	return null;
    }

    /**
     * metodo que busca el precio de simcard segun parametros
     * 
     * @param preSimcardLst
     * @param customerType
     * @param product
     * @param operation
     * @param saleMode
     * @param flagRisk
     * @return
     */
    public static CatalogCalPreSimcard getSimcardPrice(List<CatalogCalPreSimcard> preSimcardLst, String customerType, String product,
	    String operation, String saleMode, String flagRisk) {
	for (CatalogCalPreSimcard catalogCalPreSimcard : preSimcardLst) {
	    if (catalogCalPreSimcard.getClientType().equalsIgnoreCase(customerType)
		    && catalogCalPreSimcard.getProductType().equalsIgnoreCase(product)
		    && (catalogCalPreSimcard.getCommercialOperation() == null
			    || catalogCalPreSimcard.getCommercialOperation().equalsIgnoreCase(operation))
		    && catalogCalPreSimcard.getMobilSaleModality().equalsIgnoreCase(saleMode)
		    && catalogCalPreSimcard.getFlagRisk().toString().equals(flagRisk)) {
		return catalogCalPreSimcard;
	    }
	}
	return null;
    }

    /**
     * metodo que busca el suscriber fijo
     * 
     * @param commercial
     * @return
     */
    public static Subscription getFixedPhone(List<CommercialOperation> commercialOpers) {
	for (CommercialOperation commercial : commercialOpers) {
	    if (ProductType.FIXED.getCodeDesc().equals(commercial.getProduct())) {
		return commercial.getSubscriber();
	    }
	}
	return null;
    }

    /**
     * metodo que busca los suscriber moviles
     * 
     * @param commercialOpers
     * @return
     */
    public static List<Subscription> getMobilePhones(List<CommercialOperation> commercialOpers) {
	List<Subscription> mobilePhones = new ArrayList<>();
	for (CommercialOperation commercial : commercialOpers) {
	    if (ProductType.MOBILE.getCodeDesc().equals(commercial.getProduct())) {
		mobilePhones.add(commercial.getSubscriber());
	    }
	}
	return mobilePhones;
    }

    /***
     * Método que permite verificar si es alta o portabilidad, si es asi devolvera
     * true , en caso de otras oeraciones devolvera false
     * 
     * @param commOperList
     * @param type
     * @return
     */
    public static boolean verifyProvideOrPortability(List<CommercialOperation> commOperList, SuscriptionType type) {
	CommOperCollection<CommercialOperation> opeComProviPort = new CommOperCollection<>(commOperList);
	opeComProviPort.filterByProduct(type);
	if (type.getCode().equals(Constant.MOBILE)) {
	    opeComProviPort.filterProvidePorta();
	} else if (type.getCode().equals(Constant.FIX)) {
	    opeComProviPort.filterByOperationType(type);
	}
	return opeComProviPort.isEmpty() ? Boolean.TRUE : Boolean.FALSE;
    }
    
    
    /***
     * Método que permite verificar si es alta o portabilidad, si es asi devolvera
     * true , en caso de otras oeraciones devolvera false
     * 
     * @param commOperList
     * @param type
     * @return
     */
    public static boolean verifyCaplOrPortability(List<CommercialOperation> commOperList, SuscriptionType type) {
	CommOperCollection<CommercialOperation> opeComCaplPort = new CommOperCollection<>(commOperList);
	opeComCaplPort.filterByProduct(type);
	opeComCaplPort.filterCaplPorta();
	return opeComCaplPort.isEmpty() ? Boolean.TRUE : Boolean.FALSE;
    }
    
    /***
     * Método que permite obtener los números móviles que sean de tipo CAEQ/CAPL de las operaciones comerciales
     * @param commOperList
     * @return
     */
    public static List<String> obtainMobileCaeqCaplNumbers(List<CommercialOperation> commOperList){
	List<String> caplNumbers = new ArrayList<>();
	CommOperCollection<CommercialOperation> operations = new CommOperCollection<>(commOperList);
    	operations.filterByProduct(SuscriptionType.MOBILE);
	operations.filterCAPL();
	if(!operations.isEmpty()) {
	    for(CommercialOperation operation : operations) {
		caplNumbers.add(operation.getSubscriber().getServiceNumber());
	    }
	}
	return caplNumbers;
    }
}
