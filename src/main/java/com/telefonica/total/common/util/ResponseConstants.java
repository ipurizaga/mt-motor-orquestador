package com.telefonica.total.common.util;

public final class ResponseConstants {

    public static final String RENTAADICIONAL_HD		  = "RentaAdicionalHD";
    public static final String RENTAADICIONAL_FOX		  = "RentaAdicionalFOX";
    public static final String RENTAADICIONAL_HBO		  = "RentaAdicionalHBO";
    public static final String RENTAADICIONAL_DECOSMART		  = "RentaAdicionalDecoSmart";
    public static final String RENTAADICIONAL_PUNTOADICIONALHD	  = "RentaAdicionalPuntoAdicionalHD";
    public static final String RENTAADICIONAL_ULTRAWIFI		  = "RentaAdicionalUltraWifi";
    public static final String RENTAADICIONAL_SEGURIDADTOTAL	  = "RentaAdicionalSeguridadTotal";
    public static final String RENTAOTROS_ADICIONALES		  = "RentaOtrosAdicionales";
    public static final String RENTATOTAL_ADICIONALESACTUALES	  = "RentaTotalAdicionalesActuales";
    public static final String FIJO_CICLO_ACTUAL		  = "Fijo_ciclo";
    public static final String PF_INTERNET_VELOCIDAD		  = "PF_Internet_Velocidad";
    public static final String PF_INTERNET_VELOCIDAD_GAP	  = "PFInternetGapVelocity";
    public static final String PF_DATOS_MOVILES_GAP	  	  = "DatosMovil1GBGap";
    public static final String PF_INTERNET_TECNOLOGIA		  = "PF_Internet_Tecnologia";
    public static final String PF_CABLE_TIPO			  = "PF_Cable_Tipo";
    public static final String PF_ARPA			          = "PF_Arpa";
    public static final String PF_CABLE_CANTIDADDECOSSMART	  = "PF_Cable_CantidadDecosSmart";
    public static final String PF_CABLE_CANTIDADDECOSHD		  = "PF_Cable_CantidadDecosHD";
    public static final String PF_INTERNET_ULTRAWIFICANTIDAD	  = "PF_Internet_UltraWifiCantidad";
    public static final String PF_PAQUETE_RENTA			  = "PF_Paquete_Renta";
    public static final String PF_DESCUENTOPERMANENTE		  = "PF_DescuentoPermanente";
    public static final String PF_INTERNET_MODEM		  = "PF_InternetModem";
    public static final String PM_MOVILX_RENTA			  = "PM_Movil%s_Renta";
    public static final String PM_MOVILX_DESCPERMANENTE		  = "PM_Movil%s_DescPermanente";
    public static final String PM_MOVILX_CANTDATOS		  = "PM_Movil%s_CantDatos";
    public static final String PM_MOVILX_CONSUMO_MB		  = "PM_Movil%s_ConsumoMB";
    public static final String PM_MOVILX_APPS			  = "PM_Movil%s_Apps";
    public static final String PM_MOVIL_BONODUPLICA_FLAG	  = "PM_Movil%s_BonoDuplicaFlag";
    public static final String PM_MOVIL_BONODUPLICA_VENC	  = "PM_Movil%s_BonoDuplicaVenc";
    public static final String PM_MOVIL_BONODUPLICA_CANT	  = "PM_Movil%s_CantDatos_BonoDuplica";
    public static final String PM_MOVIL_BONODUPLICA_FECHA_FIN	  = "PM_Movil%s_FechaFin_BonoDuplica";
    public static final String PM_MOVIL_BONODUPLICA_DATOS_TOTAL	  = "PM_Movil%s_CantDatosTotal";
    public static final String PM_MOVIL_DESCUENTOPROM_RENTA	  = "PM_Movil%s_DescuentoProm";
    public static final String PM_MOVIL_DESCUENTOPROM_RENTA_FINAL = "PM_Movil%s_RentaFinal";
    public static final String PM_MOVIL_DESCUENTOPROM_FECHA_FIN	  = "PM_Movil%s_FechaFin_DescProm";

    public static final String PM_MOVIL_DESCUENTOPROM	   = "PM_Movil%s_DescuentoProm";
    public static final String PM_MOVIL_DESCUENTOPROM_VENC = "PM_Movil%s_DescuentoPromVenc";
    public static final String RENTA_ACTUAL		   = "RentaActual";
    public static final String MOVIL_FINANCIAMIENTO	   = "Movil%s_Financiamiento";
    public static final String MOVIL_CUOTAFINANCIAMIENTO   = "Movil%s_CuotaFinanciamiento";
    public static final String MOVIL_FECHAINICIOFINANC	   = "Movil%s_FechaInicioFinanc";
    public static final String MOVIL_FECHAFINFINANC	   = "Movil%s_FechaFinFinanc";

    public static final String PAQUETE_BENEFICIO	   = "PaqueteBeneficio";
    public static final String PAQUETE_BENEFICIO_EXPERTO_WIFI	   = "Experto Wifi";
    
    public static final String EQUIPAMIENTO_CANTIDADDECOS	  = "eqDecoQuantity";
    public static final String EQUIPAMIENTO_CANTIDADREPETIDORWIFI = "eqUltraWifiQuantity";
    
    /* PO - BO LMA*/        
    public static final String PO_ID	  = "Movil2_PO";
    public static final String BO_ID	  = "Movil2_BO";
    
    /* Billing Offer Information */
    public static final String FIJO_LINEA	  = "Fijo_Linea";
    public static final String FIJO_INTERNET	  = "Fijo_Internet";
    public static final String FIJO_TELEVISION	  = "Fijo_Television";
    public static final String MOVIL1_MINUTOS	  = "Movil1_Minutos";
    public static final String MOVIL2_MINUTOS_OTROS_OPERADORES	  = "Movil2_Minutos_Otros_Operadores";
    public static final String MOVIL1_DATOS	  = "Movil1_Datos";
    public static final String MOVIL1_APPS	  = "Movil1_Apps";
    public static final String MOVIL2_MINUTOS	  = "Movil2_Minutos";
    public static final String MOVIL2_DATOS	  = "Movil2_Datos";
    public static final String MOVIL2_APPS	  = "Movil2_Apps";
    public static final String MOVIL1_ROAMING	  = "Movil1_Roaming";
    public static final String MOVIL2_ROAMING	  = "Movil2_Roaming";
    public static final String MOVIL1_ROAMINGCOVERAGE	  = "Movil1_RoamingCobertura";
    public static final String MOVIL2_ROAMINGCOVERAGE	  = "Movil2_RoamingCobertura";
    public static final String MOVIL1_GIGAPASS	  = "Movil1_PasaGigas";
    public static final String MOVIL2_GIGAPASS	  = "Movil2_PasaGigas";
    public static final String PAQUETE_RENTA_FIJA = "Fijo_RentaPaquete";
    public static final String RENTAPAQUETE	  = "RentaPaquete";
    public static final String PRECIOREGULAR	  = "PrecioRegular";
    public static final String DESCUENTO	  = "Descuento";
    public static final String NOMBRE		  = "Nombre";
    public static final String PS		  = "PS";
    public static final String COSTOHOGARSOLO	  = "CostoHogarSolo";
    public static final String COSTOMOVIL1_SOLO	  = "CostoMovil1_Solo";
    public static final String COSTOMOVIL2_SOLO	  = "CostoMovil2_Solo";
    public static final String MOVISTAR_PRIX	  = "MPrix";
    public static final String MOVISTAR_MUSIC	  = "MMusica";
    public static final String MOVISTAR_PLAY	  = "MPlay";
    public static final String NETFLIX		  = "Netflix";
    public static final String MOVIL_BONO_DUPLICA = "Movil%s_BonoDuplica";
    public static final String LINEA_MINUTOS	  = "Linea_Minutos";
    public static final String CABLE_CANALES	  = "Cable_Canales";
    public static final String MOVIL1_SMS	  = "Movil1_SMS";
    public static final String MOVIL2_SMS	  = "Movil2_SMS";
    public static final String MOVIL1_WAINT	  = "Movil1_WAInt";
    public static final String MOVIL2_WAINT	  = "Movil2_WAInt";
    public static final String MOVIL1_RENTA	  = "Movil1_RentaPaquete";
    public static final String MOVIL2_RENTA	  = "Movil2_RentaPaquete";
    public static final String PENALIDAD_PLAN_RANK_MOVIL = "Penalidad Plan Rank_Movil%s";
    public static final String PARRILLA_PERTENENCIA = "Parrilla";

    public static final String RENTATOTAL		   = "RentaTotal";
    public static final String FIJO_EQUIPAMIENTO_MODEM	   = "Fijo_Equipamiento_Modem";
    public static final String FIJO_EQUIPAMIENTO_DECO	   = "Fijo_Equipamiento_Deco";
    public static final String RENTAPORADICIONALES	   = "RentaPorAdicionales";
    public static final String SALTO			   = "Salto";
    public static final String DESC_EMPLEADO_MONTO	   = "DescuentoEmpleado_Monto";
    public static final String DESC_EMPLEADO_RENTA_PAQUETE = "DescuentoEmpleado_RentaPaquete";

    public static final String PRIORIDAD			  = "Prioridad";
    public static final String CARACTERISTICA			  = "caracteristica";
    public static final String AD_DECOSMART			  = "AD_DecoSmart";
    public static final String AD_PUNTOADICIONALHD		  = "AD_PuntoAdicionalHD";
    public static final String AD_ULTRAWIFI			  = "AD_UltraWifi";
    public static final String AD_SEGURIDADTOTAL		  = "AD_SeguridadTotal";
    public static final String AD_OTROSADICIONALES		  = "AD_OtrosAdicionales";
    public static final String AD_RENTAADICIONALESMANTENER	  = "AD_RentaAdicionalesMantener";
    public static final String AD_HD_BLOQUE			  = "AD_HD_Bloque";
    public static final String AD_HBO_BLOQUE			  = "AD_HBO_Bloque";
    public static final String AD_FOX_BLOQUE			  = "AD_FOX_Bloque";
    public static final String AD_ESTELAR_BLOQUE		  = "AD_EST_Bloque";
    public static final String AD_GOLDPREM_BLOQUE		  = "AD_GOLPREM_Bloque";
    public static final String AD_HTPACK_BLOQUE		          = "AD_HTPACK_Bloque";
    public static final String AD_RENTABLOQUESADICIONALESMANTENER = "AD_RentaBloquesAdicionalesMantener";
    public static final String COSTOTOTALPRODUCTOSSEPARADOS	  = "CostoTotalProductosSeparados";
    public static final String INTERNET_TECNOLOGIADESTINO	  = "Internet_TecnologiaDestino";
    public static final String CANTIDAD_LINEAS			  = "Cantidad_Lineas";
    public static final String DATOS_MOVIL_ILIM			  = "Ilimitado";
    public static final String PLAN_OFFER_ARQUETIPO		  = "planOfferArq";
    public static final String ARQUETIPO_LITE			  = "LITE";
    public static final String ARQUETIPO_FULL			  = "FULL";
    public static final String ARQUETIPO_PRO			  = "PRO";

    /** solo equipos **/
    public static final String POSTPAIDEASY_MOTHNSDELAY_DISCOUNT = "PostpagoFacilMonthstoDelayDiscount";
    public static final String ADDITIONALCOST_PPF		 = "CostoAdicionalPorPPF";
    public static final String PENALITY				 = "penalidad";
    public static final String SIMCARD_PRICE			 = "precioSimcard";
    public static final String POSTPAIDEASY_DISCOUNT_DURATION	 = "PostpagoFacilDiscountDuration";
    public static final String POSTPAIDEASY_DISCOUNT_PERCENTAGE	 = "PostpagoFacilDiscountPercentage";

    public static final String FINANCIAMIENTO = "financiamiento";

    public static final String DESCUENTO_PS	  = "DescuentoTemporal_PS";
    public static final String DESCUENTO_NOMBRE	  = "DescuentoTemporal_Nombre";
    public static final String DESCUENTO_MONTO	  = "DescuentoTemporal_Monto";
    public static final String DESCUENTO_DURACION = "DescuentoTemporal_Duracion";
    public static final String DESCUENTO_CAMPANIA = "DescuentoTemporal_Campania";
    public static final String PROMO_PS	  = "VelocidadTemporal_PS";
    public static final String PROMO_NOMBRE	  = "VelocidadTemporal_Nombre";
    public static final String PROMO_MONTO	  = "VelocidadTemporal_Monto";
    public static final String PROMO_DURACION = "VelocidadTemporal_Duracion";
    public static final String PROMO_CAMPANIA = "VelocidadTemporal_Campania";
    public static final String CODIGO_AUTOMATIZADOR = "PF_CODIGO_AUTOMATIZADOR";
    public static final String MONTO_DEVOLUCION_UPFRONT = "montoDevolucionUpFront";
    public static final String PERIODO_UPFRONT = "periodoUpFront";
    public static final String RENTA_PROMOCIONAL_UPFRONT = "rentaPromocionalUpFront";
    public static final String DESCUENTO_PROMOCIONAL_UPFRONT = "descuentoPromocionalUpFront";
    
    /** Flag desposicionado **/
    public static final String FLAG_DESPOSICIONADO = "Flag_Desposicionado";
    
    /**  Para los parametros de INSTALACION_CUOTAS e INSTALACION_COSTO **/
    public static final String INSTALACION_CUOTAS = "InstalacionCuotas";
    public static final String INSTALACION_COSTO = "InstalacionCosto";

    private ResponseConstants() {
    }
}
