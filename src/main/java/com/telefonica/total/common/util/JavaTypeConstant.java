package com.telefonica.total.common.util;

public final class JavaTypeConstant {

    public static final String INTEGER = "JAVALANGINTEGER";
    public static final String DOUBLE = "JAVALANGDOUBLE";
    public static final String DATE = "JAVASQLDATE";
    public static final String CHARACTER = "JAVALANGCHARACTER";
    public static final String STRING = "JAVALANGSTRING";
    

    private JavaTypeConstant() {
    }

}
