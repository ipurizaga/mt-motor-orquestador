package com.telefonica.total.common.util;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.LinkedList;
import java.util.Collections;

import org.apache.commons.collections4.CollectionUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.PartialRequestDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.enums.OperationComm;
import com.telefonica.total.enums.OperationDevice;
import com.telefonica.total.enums.ProductType;
import com.telefonica.total.enums.QueryType;
import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.exception.BusinessException;
import com.telefonica.total.model.CatalogBillingProduct;
import com.telefonica.total.model.MasterParkMobil;
import com.telefonica.total.model.ParamMovTotal;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.CommercialOperationInfo;
import com.telefonica.total.pojo.req.Customer;
import com.telefonica.total.pojo.req.Device;
import com.telefonica.total.pojo.req.Information;
import com.telefonica.total.pojo.req.Offer;
import com.telefonica.total.pojo.req.SaleCondition;
import com.telefonica.total.repository.ParamMovTotalRepo;

import lombok.AllArgsConstructor;

import com.telefonica.total.business.offersRules.productsRules.IProductsRules;
import com.telefonica.total.common.util.ValidationUtil;

public class TotalUtil {
    
    
    
    public TotalUtil() {
    }

    /**
     * 
     * @param offers
     * @return se genera el array de planes - request
     */
    public static String generateArrayPlan(List<Offer> offers) {
	if (offers == null || offers.isEmpty()) {
	    throw new BusinessException(Constant.BE_2022);
	}

	StringBuilder sb = new StringBuilder();
	for (Offer offer : offers) {
	    sb.append(offer.getBoCode()).append(Constant.SEPARATORCOMM).append(offer.getPoCode()).append(Constant.SEPARATORCOMM)
		    .append(getConditionSale(offer)).append(Constant.PIPE);
	}
	String result = sb.toString();
	return result.substring(0, result.length() - 1);
    }

    /**
     * 
     * @param devices
     * @return se genera el array de device - request
     */
    public static String generateArrayDevice(List<Device> devices) {
	StringBuilder sb = new StringBuilder();
	for (Device device : devices) {
	    sb.append(device.getSapId()).append(Constant.PIPE);
	}
	String result = sb.toString();
	return result.substring(0, result.length() - 1);
    }

    /**
     * 
     * @param sales
     * @param value
     * @return se obtiene la condicion de venta ppf
     */
    public static String getConditionSale(Offer offer) {
	if (null == offer.getSaleConditions()) {
	    return Constant.FALSE;
	}
	for (SaleCondition sale : offer.getSaleConditions()) {
	    if (Constant.PPF.equalsIgnoreCase(sale.getCondition().getCode())
		    && Constant.TRUE.equalsIgnoreCase(sale.getCondition().getValue())) {
		return Constant.TRUE;
	    }
	}
	return Constant.FALSE;
    }

    public static int getNumericValueOf(Object o) {
	if (o == null) {
	    return 0;
	} else if (o instanceof Character) {
	    return Integer.parseInt(o.toString());
	} else if (o instanceof Boolean) {
	    if ((boolean) o) {
		return 1;
	    } else {
		return 0;
	    }
	} else if (o instanceof Integer) {
	    return ((Integer) o).intValue();
	} else if (o instanceof String) {
	    Integer i = Integer.parseInt(o.toString());
	    return ((Integer) i).intValue();
	}else {
	    if (o.toString().equals("1")) {
		return 1;
	    } else {
		return 0;
	    }
	}
    }

    public static void clearAndSetFixedList(PartialRequestDTO partial, List<ProductFixedDTO> list) {
	partial.getResultantProductFixedList().clear();
	partial.getResultantProductFixedList().addAll(list);
    }

    public static String getDateFormat(Date date, String exp) {
	SimpleDateFormat sdf = new SimpleDateFormat(exp);
	return sdf.format(date);
    }

    public static String addMonthsReturnStringFromSqlDate(java.sql.Date fecha, int numberOfMonths) {
	DateTime date = new DateTime(fecha.getTime()).plusMonths(numberOfMonths);
	return getDateFormat(date.toDate(), Constant.DATE);
    }

    /**
     * Método que evalua que tipo de flujo debe de seguir la consulta que se ha
     * realizado. <br/>
     * <b>Retorno esperado:</b> <br/>
     * 1 - Alta Nueva <br/>
     * 2 - Gestión de Planta <br/>
     * 3 - Averia (Tipo 1) <br/>
     * 4 - Averia (Tipo 2)
     * 
     * @param lstCommOpers
     * @return int
     */
    public static Integer evaluateMT(List<CommercialOperation> lstCommOpers, QueryType query, ProductType product) {

	if (query == QueryType.ONLYDEVICES) {
	    return Constant.ALTA_NUEVA;
	}
	
	if (product == ProductType.MOBILE) {
	    return -1;
	}

	int countMobileMt = 0;
	int countFixMt = 0;
	int countMTMV = 0;
	int countMTFV = 0;
	String[] arrayStr = generateSubscArrTypes(lstCommOpers);

	for (int i = 0; i < arrayStr.length; i++) {
	    if (SuscriptionType.MTFV.getCode().equals(arrayStr[i])) {
		countMTFV++;
	    }
	    if (SuscriptionType.MTMV.getCode().equals(arrayStr[i])) {
		countMTMV++;
	    }
	    if (SuscriptionType.MTM.getCode().equals(arrayStr[i])) {
		countMobileMt++;
	    }
	    if (SuscriptionType.MTF.getCode().equals(arrayStr[i])) {
		countFixMt++;
	    }
	}

	if (countFixMt == 1 && countMobileMt >= 1) {
	    return Constant.GESTION_PLANTA;
	} else if (countFixMt == 1 && countMobileMt == 0 && countMTMV == 0 && countMTFV == 0) {
	    return Constant.AVERIA_TIPO1;
	} else if (countFixMt == 0 && countMobileMt >= 1 && countMTMV == 0 && countMTFV == 0) {
	    return Constant.AVERIA_TIPO2;
	} else if ((countFixMt == 1 && countMTMV >= 1) || (countMTFV == 1 && countMobileMt >= 1) || (countMTFV == 1 && countMTMV >= 1)) {
	    return Constant.AVERIA_TIPO3;
	}
	return 1;
    }

    /**
     * Método que genera el array de tipos de susbcripción
     * 
     * @param lstCommOpers
     * @return
     */
    private static String[] generateSubscArrTypes(List<CommercialOperation> lstCommOpers) {
	StringBuilder types = new StringBuilder();
	for (CommercialOperation commercial : lstCommOpers) {
	    if (!OperationComm.PROVIDE.getCodeDesc().equals(commercial.getOperation()) && commercial.getSubscriber() != null) {
		types.append(commercial.getSubscriber().getType()).append(Constant.SEPARATORCOMM);
	    }
	}
	String strTypes = types.toString();
	return strTypes.split(Constant.SEPARATORCOMM);
    }

    public static MasterParkMobil obtainDefaultMobileInfo() {
	MasterParkMobil parq = new MasterParkMobil();
	Field[] fields = parq.getClass().getDeclaredFields();
	for (Field field : fields) {
	    String typeName = field.getType().getName().toUpperCase().replace(".", "");
	    field.setAccessible(true);
	    if (!field.getName().equals("serialVersionUID")) {
		try {
		    if (field.getName().equals("document")) {
			field.set(parq, Constant.DEFAULT_DOCUMENT_NUMBER);
			continue;
		    }
		    if (field.getName().equals("mobileNumber")) {
			field.set(parq, Constant.DEFAULT_NUMBER_MOBILE);
			continue;
		    }
		    switch (typeName) {
		    case JavaTypeConstant.INTEGER:
			field.set(parq, Integer.parseInt("0"));
			break;
		    case JavaTypeConstant.DOUBLE:
			field.set(parq, Double.parseDouble("0"));
			break;
		    case JavaTypeConstant.DATE:
			field.set(parq, null);
			break;
		    case JavaTypeConstant.CHARACTER:
			field.set(parq, Character.valueOf('0'));
			break;
		    case JavaTypeConstant.STRING:
			field.set(parq, "0");
			break;
		    default:
			field.set(parq, null);
			break;
		    }
		} catch (Exception ex) {
		   // System.out.println(ex.getMessage());
		}
	    }
	}
	return parq;
    }

    public static Date getActualDate() {
	Calendar calendar = Calendar.getInstance();
	calendar.set(Calendar.HOUR_OF_DAY, 0);
	calendar.set(Calendar.MINUTE, 0);
	calendar.set(Calendar.SECOND, 0);
	calendar.set(Calendar.MILLISECOND, 0);
	return calendar.getTime();
    }

    public static boolean isCurrentDateBetweenDates(Date dateIni, Date dateEnd) {
	boolean result = false;
	Date currentDate = getActualDate();
	if ((currentDate.compareTo(dateIni) == 0 || currentDate.compareTo(dateIni) > 0)
		&& (currentDate.compareTo(dateEnd) == 0 || currentDate.compareTo(dateEnd) < 0)) {
	    result = true;
	}
	return result;
    }

    public static ProductFixedDTO getEquivalentOfferMT(List<ProductFixedDTO> productsMtList, Integer boId, Integer psId) {
	ProductFixedDTO product = null;
	for (ProductFixedDTO products : productsMtList) {
	    if (products.getEnabled().equals(Constant.ZERO) && products.getPackageMobilCode().equals(boId.toString())
		    && products.getPackagePs().toString().equals(psId.toString())) {
		product = products;
		break;
	    }
	}
	return product;
    }

    public static ProductFixedDTO getActualDisabledOfferMT(List<ProductFixedDTO> productsMtList, FixedDTO fixedParkDto) {
	ProductFixedDTO product = null;
	for (ProductFixedDTO products : productsMtList) {
	    if (products.getEnabled().equals(Constant.ZERO)
		    && products.getPackageMobilCode().equals(fixedParkDto.getBoIdRequest().toString())
		    && products.getPoPackage().equals(fixedParkDto.getPoIdRequest().toString())
		    && products.getPackagePs().toString().equals(fixedParkDto.getPsRequest())) {
		product = products;
		break;
	    }
	}
	return product;
    }

    public static ProductFixedDTO getActualMirrorOfferMT(List<ProductFixedDTO> filteredProductList, List<ProductFixedDTO> productsMtList,
	    FixedDTO fixedParkDto) {
    	List<String> lastMirror = new LinkedList<>();
    	boolean noMirror = false;
    	ProductFixedDTO product = null;
    	product = getActualDisabledOfferMT(productsMtList, fixedParkDto);

    	if (product != null) {
    		for (ProductFixedDTO productMirror : filteredProductList) {
    			lastMirror.add(productMirror.getMirrorOffer());
    			if (product.getMirrorOffer().equals(productMirror.getMirrorOffer())) {
    				product = productMirror;
    				product.setCoverageRquest(fixedParkDto.getCoverage());
    				product.setCob_hfc(fixedParkDto.getHfcCobInternet());
    				product.setTypeF(fixedParkDto.getTypeF());
    				product.setTypeM1(fixedParkDto.getTypeM1());
    				product.setTypeM2(fixedParkDto.getTypeM2());
    				product.setDeviceOperationM1(fixedParkDto.getDeviceOperationM1());
    				product.setDeviceOperationM2(fixedParkDto.getDeviceOperationM2());
    				product.setQuantityMobile(fixedParkDto.getQuantityMobile());
    				noMirror = false;
    				break;
    			}
    			else {
    				noMirror = true;
    			}
    		}	    
    	}
    	
    	Collections.sort(lastMirror);
    	
    	if (noMirror) {
        	for (ProductFixedDTO productMirror : filteredProductList) {        	    
        		if (productMirror.getMirrorOffer().equals(lastMirror.get(lastMirror.size()-1))) {
        		    product = productMirror;      
        		    product.setCoverageRquest(fixedParkDto.getCoverage());
        		    product.setCob_hfc(fixedParkDto.getHfcCobInternet());
        		    product.setTypeF(fixedParkDto.getTypeF());
        		    product.setTypeM1(fixedParkDto.getTypeM1());
        		    product.setTypeM2(fixedParkDto.getTypeM2());
        		    product.setDeviceOperationM1(fixedParkDto.getDeviceOperationM1());
        		    product.setDeviceOperationM2(fixedParkDto.getDeviceOperationM2());
        		    product.setQuantityMobile(fixedParkDto.getQuantityMobile());
        		    break;
        		}		
        	}
    	}

	return product;
    }

    public static CatalogBillingProduct getCatalogBillingProductBoId(List<CatalogBillingProduct> catBillProdList, Integer boId) {
	CatalogBillingProduct catBoPo = null;
	if (boId != null) {
	    for (CatalogBillingProduct catalogBillingProduct : catBillProdList) {
		if (catalogBillingProduct.getBoId().equals(boId.toString())) {
		    catBoPo = catalogBillingProduct;
		    break;
		}
	    }
	}

	return catBoPo;
    }

    public static boolean isOfferMTActualProduct(ProductFixedDTO product, String flagActualProduct) {
	boolean isActualProduct = false;
	if (product.getCharacteristics() != null && CollectionUtils.isNotEmpty(product.getCharacteristics())) {
	    for (Information information : product.getCharacteristics()) {
		if (information.getValue().equals(flagActualProduct)) {
		    isActualProduct = true;
		    break;
		}
	    }
	}
	return isActualProduct;
    }

    public static List<ProductFixedDTO> getProductsMTByPS(List<ProductFixedDTO> filteredProductList, String psCode) {
	List<ProductFixedDTO> totalFoundProducts = new ArrayList<>();
	for (ProductFixedDTO productFixedDTO : filteredProductList) {
		int ps = Integer.parseInt(psCode);
	    if (productFixedDTO.getPackagePs()==ps) {
		totalFoundProducts.add(productFixedDTO);
	    }
	}
	return totalFoundProducts;
    }

    public static ProductFixedDTO getActualMirrorOfferMTByBo(List<ProductFixedDTO> filteredProductList,
	    List<ProductFixedDTO> productsMtList, String boId) {
	ProductFixedDTO product = getOfferMTByBo(productsMtList, boId);
	if (product != null) {
	    product = getMirrorOfferMT(filteredProductList, product.getMirrorOffer());
	}
	return product;
    }

    public static ProductFixedDTO getOfferMTByBo(List<ProductFixedDTO> productsMtList, String boId) {
	ProductFixedDTO product = null;
	for (ProductFixedDTO productFixedDTO : productsMtList) {
	    if (productFixedDTO.getPackageMobilCode().equals(boId)) {
		product = productFixedDTO;
		break;
	    }
	}
	return product;
    }

    //AGREGANDO METODO PARA ENVIAR OFERTA CUANDO NO TIENES ESPEJO
    public static ProductFixedDTO getMirrorOfferMT(List<ProductFixedDTO> productsMtList, String mirrorOffer) {
	ProductFixedDTO product = null;
	List<String> lastMirror = new LinkedList<>();
	//String lastMirror = mirrorOffer;
	for (ProductFixedDTO productFixedDTO : productsMtList) {
		//lastMirror = mirrorOffer;
		lastMirror.add(productFixedDTO.getMirrorOffer());
	    if (productFixedDTO.getMirrorOffer().equals(mirrorOffer)) {
		product = productFixedDTO;
		break;
	    }
	}
	
	Collections.sort(lastMirror);
	
	if (product == null) {
    	for (ProductFixedDTO productFixedDTO : productsMtList) {
    	    
    		if (productFixedDTO.getMirrorOffer().equals(lastMirror.get(lastMirror.size()-1))) {
    		    product = productFixedDTO;
    		    
    		    break;
    		}		
    	}
	}
	
	/**if(product == null) {
	    product = productsMtList.get(productsMtList.size()-1);
	}**/
	
	return product;
	
    }
    
    /**
	 * Se añade al método obtainOfferMTWithPositiveJump las variables : 
	 * typeTotalization (Verifica si es Totalizado o Solo Fijo),
	 * arpa (el arpa actual del cliente)
	 * productType (Verifica el tipo de producto fijo actual : TRIO, DUO, DUO BA, etc.  )
	 * 
	 * Se añaden nuevas validaciones para las priorizaciones requeridas comprobando producto fijo actual, arpa y
	 * gap_velocity
	 * 
	 * Se añade la variable coverage, para evaluar la priorizacion de acuerdo al tipo de cobertura
	 * ya sea HFC o FTTH
	 **/	
    public static void obtainOfferMTWithPositiveJump(List<ProductFixedDTO> filteredProductList, List<ProductFixedDTO> downselList, String productType, Integer typeTotalization, Double arpa, String coverage, List<ParamMovTotal> lstParam, FixedDTO fixedParkDto) {  	

    	String actualTechnology = fixedParkDto.getTechnologyInternet() != null ? fixedParkDto.getTechnologyInternet() : null;
    	
    	if(actualTechnology != null) {
    	Character hfcCobInternet = fixedParkDto.getHfcCobInternet() != null? fixedParkDto.getHfcCobInternet() : '0';
        	if(actualTechnology.equals("HFC") && hfcCobInternet.toString().equals("2")) {
        		coverage = "FTTH";
        	}
    	}

    	
    	if(coverage.equals("HFC")) {
    	    
    		ValidationUtil.PriorizationOfferHfc(filteredProductList, downselList, productType, typeTotalization, arpa, lstParam);
    	}
    	else if(coverage.equals("FTTH")) {
    		ValidationUtil.PriorizationOfferFtth(filteredProductList, downselList, productType, typeTotalization, arpa, lstParam);
    	}
    	else if(coverage.equals("MOBILE")) {
    		ValidationUtil.PriorizationOfferMobile(filteredProductList, downselList, productType, typeTotalization, arpa, lstParam);
    	}
    	else {
    		throw new BusinessException("1069");
    	}
    }

    public static Integer divideByThousand(Integer origin) {
	if (origin != null) {
	    return origin / 1000;
	} else {
	    return 0;
	}
    }

    public static List<Information> compareComponent(Integer origin, Integer destiny, String name) {
	return setcompareComponent(origin, destiny, (destiny - origin), name);
    }

    public static List<Information> compareComponentD(Double origin, Double destiny, String name) {
	return setcompareComponent(origin, destiny, (destiny - origin), name);
    }

    public static List<Information> setcompareComponent(Object origin, Object destiny, Object gap, String name) {
	List<Information> comparisonList = new ArrayList<>();
	comparisonList.add(Information.builder().code(name + ComparativeConstant.ORIGEN).value(origin.toString()).build());
	comparisonList.add(Information.builder().code(name + ComparativeConstant.DESTINO).value(destiny.toString()).build());
	comparisonList.add(Information.builder().code(name + ComparativeConstant.GAP).value(String.valueOf(gap)).build());

	return comparisonList;
    }

    public static PartialRequestDTO availableOffersSa(FixedDTO fixedParkDto, List<ProductFixedDTO> priorizationList,
	    List<MobileDTO> mobileDevices) {
	PartialRequestDTO partial = new PartialRequestDTO();
	partial.setCustomerInformation(fixedParkDto);
	partial.setMobileCustomerInformation(mobileDevices);
	partial.setResultantProductFixedList(priorizationList);
	partial.setActualRent(calculateActualRent(mobileDevices));
	return partial;
    }
    
    public static PartialRequestDTO availableOffersLma(CommercialOperationInfo commercialInfo, FixedDTO fixedParkDto, List<ProductFixedDTO> priorizationList,
	    List<MobileDTO> mobileDevices, Offer offer) {
	PartialRequestDTO partial = new PartialRequestDTO();
	partial.setCustomerInformation(fixedParkDto);
	partial.setMobileCustomerInformation(mobileDevices);
	partial.setResultantProductFixedList(priorizationList);
	partial.setActualRent(calculateActualRent(mobileDevices));
	//Seteando campos para filtrar ofertas en catalog_billoff_prodoff por: po_code, dealer_code, department y store_id
	partial.setPo_code(offer.getPoCode());
	partial.setSaleChannel(commercialInfo.getSalesChannel());
	partial.setStoreDepartment(commercialInfo.getDepartment());
	partial.setDealerCode(commercialInfo.getDealerCode());
	partial.setStoreId(commercialInfo.getStoreId());
	return partial;
    }

    public static Double calculateActualRent(List<MobileDTO> mobileDevices) {
	double rentMobile = 0D;
	for (MobileDTO mobile : mobileDevices) {
	    rentMobile += mobile.getMobileRentMonoProduct() - Math.abs(mobile.getPermanentDisccount());
	}
	return rentMobile;
    }
    
    /***
     * Método que se encarga de validar si la lista tiene productos MT
     * 
     * @param filteredProductList
     * @param exceptionParameter
     */
	/* Se añade validacion extra por si acaso la lista de productos esta vacía o es nula */
    public static void availableOffersMT(List<ProductFixedDTO> filteredProductList, String exceptionParameter) {
    	if (filteredProductList==null || filteredProductList.size()==0) {
	    throw new BusinessException(exceptionParameter);
	}
    }
    
    /***
     * Método que se encarga de remover el producto indicado segun renta.
     * 
     * @param filteredProductList
     * @param rent
     */
    public static void removeRuleByRentFromProductOffers(List<ProductFixedDTO> filteredProductList, int rent) {
	List<ProductFixedDTO> removeList = new ArrayList<>();
	removeRule(filteredProductList, removeList, rent);
	filteredProductList.removeAll(removeList);
    }

    /***
     * Método que se encarga de remover el producto indicado segun renta.
     * 
     * @param filteredProductList
     * @param rent
     */
    public static void removeOfferMtByRentFromNoPriorizited(List<ProductFixedDTO> priorizationList, List<ProductFixedDTO> filteredProductList,
	    int rent) {
	List<ProductFixedDTO> removeList = new ArrayList<>();
	removeRule(priorizationList, removeList, rent);
	if (removeList.isEmpty()) {
	    removeRuleByRentFromProductOffers(filteredProductList, rent);
	}
    }
    
    private static void removeRule(List<ProductFixedDTO> productList, List<ProductFixedDTO> removeList, int rent) {
	for (ProductFixedDTO product : productList) {
	    
	    boolean vProduct = true;
	    
	    vProduct = validateCurrentProduct(product);
	    
	    if(!vProduct) {
	    
        	    if (product.getPackageRent() == rent) {
        		removeList.add(product);
        	    }
	    
	    }
	}
    }
    
    public static boolean validateCurrentProduct(ProductFixedDTO product) {
    
	boolean existeActual = false;
	
    	
    	for (Information info : product.getCharacteristics()) {
    	    
    	    if(info.getValue().equals("Producto_Actual")) {
    		
    		existeActual = true;
    		
    		break;
    		
    	    }
    	    
    	}
    	
        return existeActual;
    
    }
    
    //Función para validar el MÓVIL 2 cuando no tiene valores 
    public static void valMobile2(List<ProductFixedDTO> enabledOffers) {
    	for(ProductFixedDTO eo : enabledOffers) {
    		
    		if (eo.getMobil1QuantityData() == null) {
    			eo.setMobil1QuantityData(0);
    		}
    		if (eo.getMobil1Roaming() == null) {
    			eo.setMobil1Roaming(0.0);
    		}
    		if (eo.getMobil1Rent() == null) {
    			eo.setMobil1Rent(0.0);
    		}
    		if (eo.getMobil1Name() == null) {
    			eo.setMobil1Name("");
    		}
    		if (eo.getMobil2CostAlone() == null) {
    			eo.setMobil2CostAlone(0.0);
    		}
    		if (eo.getMobil1GigaPass() == null) {
    			eo.setMobil1GigaPass(0);
    		}
    	}
    }
    
  //Función agregar  costo y cuotas de Instalaciòn
    public static void updateCostFeeInstall(List<ProductFixedDTO> enabledOffers, List<ParamMovTotal> listParam, Integer velocityInternet, Integer typeUpdate) {
	
	ParamMovTotal paraMMovTotal = new ParamMovTotal();
    	for(ProductFixedDTO eo : enabledOffers) {  
    		switch(typeUpdate) {
    		 case 1:
    			 setCostFeeInstall(listParam,eo,paraMMovTotal);
    		 case 2:
    			 if(eo.getInternetSpeed() != velocityInternet) {
    			    setCostFeeInstall(listParam,eo,paraMMovTotal);
    			 }
    		}    	    	
    	}
    }
    
    public static void updateCostFeeInstallByInternetTechnologyDestiny(String technologyInternet, List<ProductFixedDTO> enabledOffers, List<ParamMovTotal> listParam, Integer velocityInternet, Integer typeUpdate) {
	
	ParamMovTotal paraMMovTotal = new ParamMovTotal();
    	for(ProductFixedDTO eo : enabledOffers) {  
    	    if(!technologyInternet.equals(eo.getInternetDestinyTechnology())) {
    		switch(typeUpdate) {
    		 case 1:
    			 setCostFeeInstall(listParam,eo,paraMMovTotal);
    		 case 2:
    			 if(eo.getInternetSpeed() != velocityInternet) {
    			    setCostFeeInstall(listParam,eo,paraMMovTotal);
    			 }
    		}
    	    }
    	}
    }
    
    /**
     * Función para establecer el costo de instalación y cuota de instalación
     * por cada oferta respectiva
     * @param listParam
     * @param eo
     * @param paraMMovTotal
     */
    public static void setCostFeeInstall(List<ParamMovTotal> listParam, ProductFixedDTO eo, ParamMovTotal paraMMovTotal) {
    	if(listParam.size() > 0) {
    		if (eo.getInstalacionCosto() == null) {
    		    	paraMMovTotal = getValueCostFee(listParam,Constant.NOMBRE_COSTO);
    			eo.setInstalacionCosto(Double.valueOf(paraMMovTotal.getValor()));
    		}
    		if (eo.getInstalacionCuotas() == null) {
    		    	paraMMovTotal = getValueCostFee(listParam,Constant.NOMBRE_CUOTA);
    			eo.setInstalacionCuotas(Integer.parseInt(paraMMovTotal.getValor()));
    		}
	    	}else {
        	    	if (eo.getInstalacionCosto() == null) {
    		    	//paraMMovTotal = getValueCostFee(listParam,Constant.NOMBRE_COSTO);
    			eo.setInstalacionCosto(null);
    		}
    		if (eo.getInstalacionCuotas() == null) {
    		    	//paraMMovTotal = getValueCostFee(listParam,Constant.NOMBRE_CUOTA);
    			eo.setInstalacionCuotas(null);
    		}
	    	}
    }
    
    public static ParamMovTotal getValueCostFee(List<ParamMovTotal> listParam, String groupParam) {
	
	ParamMovTotal paraMMovTotal = new ParamMovTotal();
    	for(ParamMovTotal pmt : listParam) {   		
    		if (pmt.getGrupoParam().equals(groupParam)) {
    		    	paraMMovTotal = pmt;
    		    	break;
    		}
    		
    	}
    	
    	return paraMMovTotal;
    }
  
  /* Funcion para separar ofertas por Tecnología 
public static void productsPerTechnology(List<ProductFixedDTO> listProducts, List<ProductFixedDTO> finalProducts, String technology) {
for (ProductFixedDTO product : listProducts) {
if(product.getInternetDestinyTechnology()!=null && product.getInternetDestinyTechnology().equals(technology))
finalProducts.add(product);
}
}
 public static void clearPriority(List<ProductFixedDTO> listProducts) {
for (ProductFixedDTO product : listProducts) {
product.setPriority(null);
}*/
  
  
  
  
  
  
  
    
  //Validar en que casos se agrega  costo y cuotas de Instalaciòn
    public static void validateCostFeeInstall(FixedDTO fixedParkDto, CommercialOperationInfo commercialInfo, List<ProductFixedDTO> enabledOffers, List<ParamMovTotal> listParam) {
    	
    	Integer velocityInternet = fixedParkDto.getSpeedInternet() != null ? fixedParkDto.getSpeedInternet() : 0;
    	
    	//Si es Alta Fija
    	if(fixedParkDto.getTechnologyInternet()==null) {
    		updateCostFeeInstall(enabledOffers, listParam, velocityInternet, 1);
    	}
    	//Si la Tecnología actual de internet es diferente de la tecnología de cobertura
    	//else if(!fixedParkDto.getTechnologyInternet().equals(commercialInfo.getCoverage())) {
    	/*else if(!fixedParkDto.getTechnologyInternet().equals(commercialInfo.getCoverage())) {
        		updateCostFeeInstall(enabledOffers, listParam, velocityInternet, 2);
        }*/
    	//Si ya tiene producto fijo
        else {
            if(commercialInfo.getCommercialOpers().get(0).getSubscriber()!=null) {
        	    //Si el campo Type de Subscriber es nulo o está vacío
        	    	if(commercialInfo.getCommercialOpers().get(0).getSubscriber().getType()==null || commercialInfo.getCommercialOpers().get(0).getSubscriber().getType().equals("")) {
        	    	    throw new BusinessException(Constant.BE_1074);
        	    	}
        	    //Si los productos actuales son diferentes a un TRÍO
        	    	else if(/*commercialInfo.getCommercialOpers().get(0).getSubscriber().getType().equals(SuscriptionType.DUOINTERNET.getCode())
        			|| commercialInfo.getCommercialOpers().get(0).getSubscriber().getType().equals(SuscriptionType.DUO.getCode())
        			|| commercialInfo.getCommercialOpers().get(0).getSubscriber().getType().equals(SuscriptionType.DUOCABLE.getCode())
        			|| commercialInfo.getCommercialOpers().get(0).getSubscriber().getType().equals(SuscriptionType.FIXED.getCode())*/
        			commercialInfo.getCommercialOpers().get(0).getSubscriber().getType().equals(SuscriptionType.MTF.getCode())
        	    		|| commercialInfo.getCommercialOpers().get(0).getSubscriber().getType().equals(SuscriptionType.TRIO.getCode())) {
        	    	    updateCostFeeInstallByInternetTechnologyDestiny(fixedParkDto.getTechnologyInternet(), enabledOffers, listParam, velocityInternet, 1);
        		}else if(!commercialInfo.getCommercialOpers().get(0).getSubscriber().getType().equals(SuscriptionType.TRIO.getCode())) {
        		    updateCostFeeInstall(enabledOffers, listParam, velocityInternet, 1);
        		}
            }else if(commercialInfo.getCommercialOpers().get(0).getSubscriber()==null) { 
        	updateCostFeeInstall(enabledOffers, listParam, velocityInternet, 1);
            }
        
        }
        /*else {
            updateCostFeeInstallByInternetTechnologyDestiny(fixedParkDto.getTechnologyInternet(), enabledOffers, listParam, velocityInternet, 1);
        }*/
    	
     }
    
    
    public static void priorizationsMigration(List<ProductFixedDTO> productsWithoutActual,
	    List<ProductFixedDTO> actualProducts, FixedDTO fixedParkDto, ParamMovTotal paramMovSelected) {
	int iterator = 0;
	
	boolean prioridad = false;
	
	
	
	for (ProductFixedDTO fixProd : productsWithoutActual) {
	    fixProd.setPriority(null);
	}
	
	String coverage = "";
	
	for (ProductFixedDTO fixProd : productsWithoutActual) {
	    if (iterator > 0) {
		break;
	    }
	    
	    
	    //System.out.println("TECNOLOGIA INTERNET ==> "+fixedParkDto.getTechnologyInternet());
	    //System.out.println("COB HFC ==> "+actualProducts.get(0).getCob_hfc());
	    //System.out.println("COVERAGE_REQUEST ==> "+actualProducts.get(0).getCoverageRquest());
	    
	    if(fixedParkDto.getTechnologyInternet() == null) {
		fixedParkDto.setTechnologyInternet("");
	    }
	    
	    if(fixedParkDto.getTechnologyInternet().equals("HFC") && !((actualProducts.get(0).getCob_hfc() == null? "": actualProducts.get(0).getCob_hfc()).toString().equals("2")) && actualProducts.get(0).getCoverageRquest().equals("HFC")) {
		
		coverage = "1";
		
		validationPriorizationsMigration(fixProd,actualProducts.get(0));
		
		if(fixProd.getPriority()!=null) {
		
        		if(fixProd.getPriority().equals("1")) {
        		    	iterator++;
        			prioridad = true;
        		}
        		
		}
		
	    }else if (fixedParkDto.getTechnologyInternet().equals("FTTH") && ((actualProducts.get(0).getCob_hfc() == null? "": actualProducts.get(0).getCob_hfc()).toString().equals("2"))) {
		
		coverage = "2";
		
		validationPriorizationsMigration(fixProd,actualProducts.get(0));
		
		if(fixProd.getPriority()!=null) {
		
        		if(fixProd.getPriority().equals("1")) {
        		    	iterator++;
        			prioridad = true;
        		}
        		
		}
		
	    }else if (fixedParkDto.getTechnologyInternet().equals("HFC") && ((actualProducts.get(0).getCob_hfc() == null? "": actualProducts.get(0).getCob_hfc()).toString().equals("2"))) {
    		
		//if(TotalUtil.getNumericValueOf(fixProd.getInternetSpeed()) >= Constant.VELOCIDAD_100MB) {
		if(TotalUtil.getNumericValueOf(fixProd.getInternetSpeed()) >= TotalUtil.getNumericValueOf(paramMovSelected.getValor())) {
		    	coverage = "3";
		
        		validationPriorizationsMigration(fixProd,actualProducts.get(0));
        		
        		if(fixProd.getPriority()!=null) {
        		
                		if(fixProd.getPriority().equals("1")) {
                		    	iterator++;
                			prioridad = true;
                		}
                		
        		}
    		
		}
    		
	    }else if (fixedParkDto.getTechnologyInternet().equals("HFC") && !((actualProducts.get(0).getCob_hfc() == null? "": actualProducts.get(0).getCob_hfc()).toString().equals("2")) && actualProducts.get(0).getCoverageRquest().equals("FTTH")) {
		
		//if(actualProducts.get(0).getInternetTechnology().equals("HFC")) {
		    
		    //if(fixProd.getInternetTechnology().equals("FTTH")) {
		    if(TotalUtil.getNumericValueOf(fixProd.getInternetSpeed()) >= TotalUtil.getNumericValueOf(paramMovSelected.getValor())) {
			coverage = "4";
			
			validationPriorizationsMigration(fixProd,actualProducts.get(0));
			
			if(fixProd.getPriority()!=null) {
			
	        		if(fixProd.getPriority().equals("1")) {
	        		    	iterator++;
	        			prioridad = true;
	        		}
	        		
			}
			
		    }
		    
		/*}else {
		    coverage = "2";
			
			validationPriorizationsMigration(fixProd,actualProducts.get(0));
			
			if(fixProd.getPriority()!=null) {
			
	        		if(fixProd.getPriority().equals("1")) {
	        		    	iterator++;
	        			prioridad = true;
	        		}
	        		
			}
		}*/
		
		
		
	    }else {
		
		coverage = "0";
		
		if (fixProd.getPackageRent() >= actualProducts.get(0).getPackageRent() ) {
			fixProd.setPriority(String.valueOf(1));
			
			iterator++;
			prioridad = true;
			
		}
	    }
	    
	    /*else if (fixProd.getPackageRent() > actualProducts.get(0).getPackageRent()) {
		fixProd.setPriority(String.valueOf(iterator + 1));
		priorizatedProducts.add(fixProd);
		iterator++;
	    }*/
	}
	
	Double paqueteRenta = 0.0;
	    Integer velocidadInternet = 0;
	    
	    boolean prioridad2 = false;
	    
	     if(!prioridad) {
		 
		 for (ProductFixedDTO fixProdPriority : productsWithoutActual) {
		     
		     if (fixProdPriority.getPackageRent() > paqueteRenta &&  fixProdPriority.getInternetSpeed() > velocidadInternet) {
			 
			 paqueteRenta = fixProdPriority.getPackageRent();
			 velocidadInternet = fixProdPriority.getInternetSpeed();
			 
		     }
		     
		 }
		 
		 for (ProductFixedDTO fixProdPriority : productsWithoutActual) {
		     
		     if (fixProdPriority.getPackageRent() == paqueteRenta &&  fixProdPriority.getInternetSpeed() == velocidadInternet) {
			 
			 if (!(paqueteRenta == actualProducts.get(0).getPackageRent() &&  velocidadInternet == actualProducts.get(0).getInternetSpeed())) {
        			 	
			     
			     //if(!(fixProdPriority.getEnabled().equals(Constant.ONE))) {
			     if(!(actualProducts.get(0).getEnabled().equals(Constant.ONE))) {
			     
        			     if((coverage.equals("1")) || (!fixProdPriority.getMerchandisingType().equals("P3"))) {
        			     
        				 fixProdPriority.setPriority(String.valueOf(1));
        			     
        			     }
			     
			     }
        				
			 }
			 
		     }
		     
		 }
		 
	     }  	  
    }
    
    private static void validationPriorizationsMigration(ProductFixedDTO offer,
	    ProductFixedDTO offertActual) {
	if(offertActual.getTypeF() != null) {
		if(offertActual.getTypeF().equals(Constant.TYPE_MTF)) {
		    	if(offertActual.getQuantityMobile() == 1) {
        		    	if (offer.getPackageRent() > offertActual.getPackageRent() &&  offer.getInternetSpeed() > offertActual.getInternetSpeed()) {
        		    	offer.setPriority(String.valueOf(1));
				}
		    	}else {
		    
    			if(offertActual.getTypeM1()!=null && offertActual.getTypeM2()==null) {
    			    
    			    if((offertActual.getTypeM1().equals(Constant.TYPE_MTM)) && 
    				    (offertActual.getDeviceOperationM2().equals(OperationDevice.ACTIVATION.getCodeDesc()) ||
    					offertActual.getDeviceOperationM2().equals(OperationDevice.PORTABILITY.getCodeDesc()) )) {
    				if (offer.getPackageRent() > offertActual.getPackageRent() &&  offer.getInternetSpeed() > offertActual.getInternetSpeed()) {
    				    	offer.setPriority(String.valueOf(1));
    				}
    			    }else if((offertActual.getTypeM1().equals(Constant.TYPE_MTM))) {
    				
    				
                    		if (offer.getPackageRent() > offertActual.getPackageRent() &&  offer.getInternetSpeed() > offertActual.getInternetSpeed()) {
                    		    	offer.setPriority(String.valueOf(1));
                    			
                    		}	
    			    }
    			    
    			    
    			    
    			}
    			    
    			if(offertActual.getTypeM1()!=null && offertActual.getTypeM2()!=null) {
    			    
    			    
    			    if(offertActual.getTypeM1().equals(Constant.TYPE_MTM) && offertActual.getTypeM2().equals(Constant.MOBILE) 
    				    && ((offertActual.getDeviceOperationM2().equals(OperationDevice.PORTABILITY.getCodeDesc())) || (offertActual.getDeviceOperationM2().equals(OperationDevice.CAEQ.getCodeDesc())))) {
				if (offer.getPackageRent() > offertActual.getPackageRent() &&  offer.getInternetSpeed() > offertActual.getInternetSpeed()) {
				    offer.setPriority(String.valueOf(1));
				}
    			    }
    			    
    			    if(offertActual.getTypeM1().equals(Constant.TYPE_MTM) && offertActual.getTypeM2().equals(Constant.TYPE_MTM)) {
        				if ((offer.getPackageRent()+45.0) > offertActual.getPackageRent() &&  offer.getInternetSpeed() > offertActual.getInternetSpeed()) {
        				    offer.setPriority(String.valueOf(1));
        				}
    			    }
    			    
    			    if(offertActual.getTypeM1().equals(Constant.TYPE_MTM) && offertActual.getTypeM2().equals(Constant.TYPE_LMA)) {
    				if(offertActual.getDeviceOperationM2().equals(OperationDevice.CAEQ.getCodeDesc())) {
            				if ((offer.getPackageRent()+45.0) > offertActual.getPackageRent() &&  offer.getInternetSpeed() > offertActual.getInternetSpeed()) {
  	    	          			offer.setPriority(String.valueOf(1));
            				}
    				
    				}
    			    }
    			    
    			    if(offertActual.getTypeM1().equals(Constant.TYPE_LMA) && offertActual.getTypeM2().equals(Constant.TYPE_MTM)) {
    				if(offertActual.getDeviceOperationM2().equals(OperationDevice.CAEQ.getCodeDesc())) {
            				if ((offer.getPackageRent()+45.0) > offertActual.getPackageRent() &&  offer.getInternetSpeed() > offertActual.getInternetSpeed()) {
            				    	offer.setPriority(String.valueOf(1));
            				}
    				}
    			    }	
    			}
		    	}
		 }else {
		     if (offer.getPackageRent() >= offertActual.getPackageRent() ) {
			 	offer.setPriority(String.valueOf(1));
				
		     }
		 }
	}

    }
    
    private static void validationPriorizationsMigrationOnlyFTTH(ProductFixedDTO offer,
	    ProductFixedDTO offertActual) {
	if(offertActual.getTypeF() != null) {
		if(offertActual.getTypeF().equals(Constant.TYPE_MTF)) {
		    	if(offertActual.getQuantityMobile() == 1) {
        		    	if (offer.getPackageRent() > offertActual.getPackageRent() &&  offer.getInternetSpeed() > offertActual.getInternetSpeed()) {
        		    	offer.setPriority(String.valueOf(1));
				}
		    	}else {
		    
    			if(offertActual.getTypeM1()!=null && offertActual.getTypeM2()==null) {
    			    
    			    if((offertActual.getTypeM1().equals(Constant.TYPE_MTM)) && 
    				    (offertActual.getDeviceOperationM2().equals(OperationDevice.ACTIVATION.getCodeDesc()) ||
    					offertActual.getDeviceOperationM2().equals(OperationDevice.PORTABILITY.getCodeDesc()) )) {
    				if (offer.getPackageRent() > offertActual.getPackageRent() &&  offer.getInternetSpeed() > offertActual.getInternetSpeed()) {
    				    	offer.setPriority(String.valueOf(1));
    				}
    			    }else if((offertActual.getTypeM1().equals(Constant.TYPE_MTM))) {
    				
    				
                    		if (offer.getPackageRent() > offertActual.getPackageRent() &&  offer.getInternetSpeed() > offertActual.getInternetSpeed()) {
                    		    	offer.setPriority(String.valueOf(1));
                    			
                    		}	
    			    }
    			    
    			    
    			    
    			}
    			    
    			if(offertActual.getTypeM1()!=null && offertActual.getTypeM2()!=null) {
    			    if(offertActual.getTypeM1().equals(Constant.TYPE_MTM) && offertActual.getTypeM2().equals(Constant.TYPE_MTM)) {
        				if (offer.getPackageRent() > offertActual.getPackageRent() &&  offer.getInternetSpeed() > offertActual.getInternetSpeed()) {
        				    offer.setPriority(String.valueOf(1));
        				}
    			    }
    			    
    			    if(offertActual.getTypeM1().equals(Constant.TYPE_MTM) && offertActual.getTypeM2().equals(Constant.TYPE_LMA)) {
    				if(offertActual.getDeviceOperationM2().equals(OperationDevice.CAEQ.getCodeDesc())) {
            				if (offer.getPackageRent() > offertActual.getPackageRent() &&  offer.getInternetSpeed() > offertActual.getInternetSpeed()) {
            				    	offer.setPriority(String.valueOf(1));
            				}
    				
    				}
    			    }
    			    
    			    if(offertActual.getTypeM1().equals(Constant.TYPE_LMA) && offertActual.getTypeM2().equals(Constant.TYPE_MTM)) {
    				if(offertActual.getDeviceOperationM2().equals(OperationDevice.CAEQ.getCodeDesc())) {
            				if (offer.getPackageRent() > offertActual.getPackageRent() &&  offer.getInternetSpeed() > offertActual.getInternetSpeed()) {
            				    	offer.setPriority(String.valueOf(1));
            				}
    				}
    			    }	
    			}
		    	}
		 }else {
		     if (offer.getPackageRent() >= offertActual.getPackageRent() ) {
			 	offer.setPriority(String.valueOf(1));
				
		     }
		 }
	}

    }
    
    public static void orderListPriorization(List<ProductFixedDTO> productsWithoutActual,
	    List<ProductFixedDTO> actualProducts) {
	
	List<ProductFixedDTO> listUpsell = new ArrayList<ProductFixedDTO>();
	List<ProductFixedDTO> listDownsell = new ArrayList<ProductFixedDTO>();
	
	for (ProductFixedDTO fixProd : productsWithoutActual) {
	    
	    if (fixProd.getPackageRent() > actualProducts.get(0).getPackageRent() &&  fixProd.getInternetSpeed() > actualProducts.get(0).getInternetSpeed()) {
				
		for (Information information : fixProd.getCharacteristics()) {
			 if (information.getValue().equals(Constant.Upsell)) {
			     
			     break;
			 }else if (information.getValue().equals(Constant.Downsell)) {
			     information.setValue(Constant.Upsell);
			     
			 }
		}
		
		listUpsell.add(fixProd);
		
	    //}else if(fixProd.getPackageRent() < actualProducts.get(0).getPackageRent() &&  fixProd.getInternetSpeed() < actualProducts.get(0).getInternetSpeed()) {
	    }else {
		for (Information information : fixProd.getCharacteristics()) {
			 if (information.getValue().equals(Constant.Downsell)) {
			     break;
			 }else if (information.getValue().equals(Constant.Upsell)) {
			     information.setValue(Constant.Downsell);
			     
			 }
		}
		
		listDownsell.add(fixProd);
	    }
	    
	}
	
	Collections.sort(listUpsell, ProductFixedDTO.minorToMajorRentComparator);
	
	Collections.sort(listDownsell, ProductFixedDTO.majorToMinorRentComparator);
	
	productsWithoutActual.clear();
	
	for (ProductFixedDTO productFixedDTOUpsell : listUpsell) {
	    productsWithoutActual.add(productFixedDTOUpsell);
	}
	
	for (ProductFixedDTO productFixedDTODownsell : listDownsell) {
	    productsWithoutActual.add(productFixedDTODownsell);
	}
	
	/*for (ProductFixedDTO fixProdFinal : productsWithoutActual) {
	    
	    String valor = "";
	    
	    for (Information information : fixProdFinal.getCharacteristics()) {
		 if (information.getValue().equals(Constant.Downsell)) {
		     valor = information.getValue();
		 }else if (information.getValue().equals(Constant.Upsell)) {
		     valor = information.getValue();		     
		 }
	    }
	    
	    for (Information information12 : fixProdFinal.getCharacteristics()) {
		
		if(valor.equals(Constant.Downsell)) {
		    
		    if (information12.getValue().equals(Constant.UP)) {
			information12.setValue(Constant.DOWN);
		    }
		    
		}else if(valor.equals(Constant.Upsell)){
		    if (information12.getValue().equals(Constant.DOWN)) {
			information12.setValue(Constant.UP);
		    }
		}
		
	    }
	    
	}*/
	
    }
    /**
     * Funcion para separar ofertas por Tecnología
     * 
     * @param listProducts
     * @param finalProducts
     * @param technology
     */
    public static void productsPerTechnology(List<ProductFixedDTO> listProducts, List<ProductFixedDTO> finalProducts, String technology) {
 	   for (ProductFixedDTO product : listProducts) {
 			if(product.getInternetDestinyTechnology()!=null && product.getInternetDestinyTechnology().equals(technology))
 				finalProducts.add(product);
 		}
    }
    
    public static void clearPriority(List<ProductFixedDTO> listProducts) {
    	for (ProductFixedDTO product : listProducts) {
  				product.setPriority(null);
 		}
    }
    
    /**
     * Funcion para validacion de Campañas Temporales
     * Evalua si es producto playas o upfron, de acuerdo a ello envia variable de validacion y 
     * cambia el campaignValue
     * 
     * @param AdditionalOperationInformation
     * @return
     */
    

    
    public static Information validationUpfront(List<Information> AdditionalOperationInformation, List<ParamMovTotal> campaniasDescTemp) {
    	
    	Information respValUpFrn = new Information();
    	ParamMovTotal campaignDefault = campaniaDefault(campaniasDescTemp);
    	ParamMovTotal campaignPriority = campaniaPrioridad(campaniasDescTemp);
    	Integer cantidadCampanias = AdditionalOperationInformation.size();
    	String campaignCode = "";
    	String campaignValue = "";
    	

    	if(cantidadCampanias > 1) {
    		respValUpFrn = multiplesCampanias(campaniasDescTemp, AdditionalOperationInformation);
    	}
    	else if(cantidadCampanias == 1) {
    		campaignCode = AdditionalOperationInformation.get(0).getCode();
    		campaignValue = AdditionalOperationInformation.get(0).getValue();
    		respValUpFrn = validaCampania(campaniasDescTemp, campaignCode, campaignValue);
    	}
    	else {
    		respValUpFrn.setCode(campaignDefault.getCol1());
    		respValUpFrn.setValue(campaignDefault.getValor());
    	}
    	
    	return respValUpFrn;
    }
    
    public static Information multiplesCampanias(List<ParamMovTotal> campaniasDescTemp, List<Information> campaniasDescTempRequest) {
    	boolean validaPriority = false;
    	Information campania = new Information("","");
    	ParamMovTotal campaignDefault = campaniaDefault(campaniasDescTemp);
    	ParamMovTotal campaignPriority = campaniaPrioridad(campaniasDescTemp);
    	for(Information campaniaDescTempRequest : campaniasDescTempRequest) {
    		if(campaniaDescTempRequest.getCode().equals(campaignPriority.getCodValor()) && campaniaDescTempRequest.getValue().equals(campaignPriority.getValor())) {
    			//campania.setValue(campaniaDescTempRequest.getValue());
    			campania.setCode(campaignPriority.getCol1());
    			validaPriority = true;
    			break;
    		}
    	}
    	
    	if(validaPriority) {
        	for(Information campaniaDescTempRequest : campaniasDescTempRequest) {
        		if(!(campaniaDescTempRequest.getCode().equals(campaignPriority.getCodValor()) && campaniaDescTempRequest.getValue().equals(campaignPriority.getValor()))) {
        			campania.setValue(campaniaDescTempRequest.getValue());
        		}
        	}
    	}
    	else {
    		for(Information campaniaDescTempRequest : campaniasDescTempRequest) {
        		if(!(campaniaDescTempRequest.getCode().equals(campaignPriority.getCodValor()) && campaniaDescTempRequest.getValue().equals(campaignPriority.getValor()))) {
        			campania.setValue(campaniaDescTempRequest.getValue());
        			campania.setCode(campaignDefault.getCol1());
        		}
        	}
    	}
    	
    	return campania;

    }
    
    public static Information validaCampania(List<ParamMovTotal> campaniasDescTemp, String campaniaCode, String campaniaValue) {
    	Information campania = new Information("","");
    	ParamMovTotal campaignPriority = campaniaPrioridad(campaniasDescTemp);
    	ParamMovTotal campaignDefault = campaniaDefault(campaniasDescTemp);
    	for(ParamMovTotal campaniaDescTemp : campaniasDescTemp) {
    		if(campaniaDescTemp.getCodValor().equals(campaniaCode) && campaniaDescTemp.getValor().equals(campaniaValue)) {
    			if(campaignPriority.getCodValor().equals(campaniaCode)) {
    				campania.setCode(campaignPriority.getCol1());
        			campania.setValue(campaignDefault.getValor());
        			break;
    			}
    			else {
        			campania.setCode(campaniaDescTemp.getCol1());
        			campania.setValue(campaniaValue);
        			break;
    			}
    		}
    	}
    	
    	if(campania.getCode().equals("")) {
            campania.setCode(campaignDefault.getCol1());
            campania.setValue(campaignDefault.getValor());
    	}
    	
    	return campania;
    }

    public static String campaniaCode(List<ParamMovTotal> campaniasDescTemp, List<Information> campaniasDescTempRequest) {
    	String campaniaCode = "";
    	ParamMovTotal campaignPiority = campaniaPrioridad(campaniasDescTemp);
    	for(Information campaniaDescTempRequest : campaniasDescTempRequest) {
    		if(campaniaDescTempRequest.getCode().equals(campaignPiority.getCodValor()) && campaniaDescTempRequest.getValue().equals(campaignPiority.getValor())) {
    			campaniaCode = campaniaDescTempRequest.getCode();
    		}
    	}
    	
    	return campaniaCode;
    }
    
    public static ParamMovTotal campaniaPrioridad(List<ParamMovTotal> campaniasDescTemp) {
    	ParamMovTotal campaignPiority = new ParamMovTotal();
    	for(ParamMovTotal campaniaDescTemp : campaniasDescTemp) {
    		if(campaniaDescTemp.getCol2()!=null) {
        		if(campaniaDescTemp.getCol2().equals(Constant.PRIORITY)) {
        			campaignPiority = campaniaDescTemp;
        			break;
        		}
    		}
    	}
    	return campaignPiority;
    }
    
    public static ParamMovTotal campaniaDefault(List<ParamMovTotal> campaniasDescTemp) {
    	ParamMovTotal campaignPiority = new ParamMovTotal();
    	for(ParamMovTotal campaniaDescTemp : campaniasDescTemp) {
    		if(campaniaDescTemp.getCol2()!=null) {
        		if(campaniaDescTemp.getCol2().equals(Constant.DEFAULT)) {
        			campaignPiority = campaniaDescTemp;
        			break;
        		}
    		}
    	}
    	return campaignPiority;
    }
    
    public static String getValuePlayas(CommercialOperationInfo commerInfo) {
	
	
	String pp = "0";
	
	if((commerInfo.getAdditionalOperationInformation()!= null)) {
	
        	for (Information info : commerInfo.getAdditionalOperationInformation()) {
        	    
        	    if(info.getCode().equals(Constant.PRODUCTO_PLAYA)) {
        		pp = info.getValue();
        	    }
        	    
        	}
	
	}
	
	return pp;
	
    }
}
