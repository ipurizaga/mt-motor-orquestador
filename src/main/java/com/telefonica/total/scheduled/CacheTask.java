package com.telefonica.total.scheduled;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.java.Log;

@Component
@Log
public class CacheTask {

    @Autowired
    private CacheManager manager;

    @Scheduled(cron = "0 0 6 * * ?")	
    public void morningClearAllCaches() {
	for (String cacheName : manager.getCacheNames()) {
	    clearCacheFromCacheName(cacheName);
	}
    }
    
    
    private Boolean clearCacheFromCacheName(final String cacheName) {
	final Cache cache = manager.getCache(cacheName);
	if (cacheExists(cache)) {
	    cache.clear();
	    log.info("Cache borrada en "+ new Date());
	    return true;
	}
	return false;
    }
    
    private Boolean cacheExists(final Cache cache) {
	return cache != null;
    }


}
