package com.telefonica.total.generic;

import java.util.ArrayList;

import com.telefonica.total.dto.ProductOfferDTO;


public class ProdOffDtoCollection<T extends ProductOfferDTO> extends ArrayList<ProductOfferDTO
> {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

}
