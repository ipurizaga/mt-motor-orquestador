package com.telefonica.total.generic;

import java.util.ArrayList;
import java.util.List;

import com.telefonica.total.model.CatalogDevice;

public class DeviceCatalogCollection<T extends CatalogDevice> extends ArrayList<CatalogDevice> {

    private static final long serialVersionUID = 1L;

    public List<String> filterByClassification() {
	List<String> deviceClassif = new ArrayList<>();
	for (CatalogDevice catalogDevice : this) {
	    deviceClassif.add(catalogDevice.getDeviceClassification());
	}
	return deviceClassif;
    }

}
