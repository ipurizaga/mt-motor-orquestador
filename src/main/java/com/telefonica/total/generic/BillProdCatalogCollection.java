package com.telefonica.total.generic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.telefonica.total.dto.BillingProductDTO;
import com.telefonica.total.model.CatalogBillingProduct;

public class BillProdCatalogCollection<T extends CatalogBillingProduct> extends ArrayList<BillingProductDTO> {

    private static final long serialVersionUID = 1L;
    
    public BillProdCatalogCollection() {
	super();
    }
    
    public BillProdCatalogCollection(List<BillingProductDTO> responseList) {
	super.addAll(responseList);
    }
    
    public void filtrar( String scoreDigits ) {
	BillProdCatalogCollection<BillingProductDTO> list = new BillProdCatalogCollection<>();
	for(BillingProductDTO billingProductDto : this) {
	   if(Integer.parseInt(billingProductDto.getBoPlanCreditScore()) <= Integer.parseInt(scoreDigits)) {
	       list.add(billingProductDto);
	   }
	}
    }
    
    public BillProdCatalogCollection<BillingProductDTO> filterByDevClassif(String... devicesClass) {
	BillProdCatalogCollection<BillingProductDTO> filterProds = new BillProdCatalogCollection<>();

	for (String device : Arrays.asList(devicesClass)) {
	    for (BillingProductDTO billProd : this) {
		if (billProd.getPoDeviceClassification().equals(device)) {
		    filterProds.add(billProd);
		}
	    }
	}
	return filterProds;
    }
    
    

}
