package com.telefonica.total.generic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.enums.OperationComm;
import com.telefonica.total.enums.ProductType;
import com.telefonica.total.enums.SaleModalityMobile;
import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.pojo.req.CommercialOperation;

public class CommOperCollection<T extends CommercialOperation> extends ArrayList<CommercialOperation> {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public CommOperCollection() {
	super();
    }

    public CommOperCollection(List<CommercialOperation> commOperList) {
	super.addAll(commOperList);
    }

    public void filterByOperationType(SuscriptionType type) {
	CommOperCollection<CommercialOperation> filterLst = new CommOperCollection<>();
	if (type.getCode().equals(Constant.MOBILE) || type.getCode().equals(Constant.FIX)) {

	    for (CommercialOperation subscription : this) {
		if (OperationComm.PORTABILITY.getCodeDesc().equalsIgnoreCase(subscription.getOperation())
			|| OperationComm.PROVIDE.getCodeDesc().equalsIgnoreCase(subscription.getOperation())) {
		    filterLst.add(subscription);
		}
	    }
	}
	if (CollectionUtils.isNotEmpty(filterLst)) {
	    super.removeAll(filterLst);
	}

    }

    public void filterProvidePorta() {
	CommOperCollection<CommercialOperation> filterLst = new CommOperCollection<>();
	for (CommercialOperation subscription : this) {
	    if (!OperationComm.PORTABILITY.getCodeDesc().equalsIgnoreCase(subscription.getOperation())
		    || !OperationComm.PROVIDE.getCodeDesc().equalsIgnoreCase(subscription.getOperation())) {
		filterLst.add(subscription);
	    }
	}
	if (CollectionUtils.isNotEmpty(filterLst)) {
	    super.removeAll(filterLst);
	}
    
    }
    
    public void filterCaplPorta() {
	CommOperCollection<CommercialOperation> filterLst = new CommOperCollection<>();
	for (CommercialOperation subscription : this) {
	    if (!OperationComm.PORTABILITY.getCodeDesc().equalsIgnoreCase(subscription.getOperation())
		    || !OperationComm.CAPL.getCodeDesc().equalsIgnoreCase(subscription.getOperation())) {
		filterLst.add(subscription);
	    }
	}
	if (CollectionUtils.isNotEmpty(filterLst)) {
	    super.removeAll(filterLst);
	}
    
    }
    
    public void filterCAPL() {
	CommOperCollection<CommercialOperation> filterLst = new CommOperCollection<>();
	for (CommercialOperation subscription : this) {
	    if (OperationComm.CAPL.getCodeDesc().equalsIgnoreCase(subscription.getOperation())  ) {
		filterLst.add(subscription);
	    }
	}
	if (CollectionUtils.isNotEmpty(filterLst)) {
	    super.clear();
	    super.addAll(filterLst);
	} else {
	    super.clear();
	}
    }
    
    public void obtainProvideAndPortaOperations() {
	CommOperCollection<CommercialOperation> filterLst = new CommOperCollection<>();
	for (CommercialOperation subscription : this) {
	    if (OperationComm.PROVIDE.getCodeDesc().equalsIgnoreCase(subscription.getOperation()) 
		    || OperationComm.PORTABILITY.getCodeDesc().equalsIgnoreCase(subscription.getOperation())) {
		filterLst.add(subscription);
	    }
	}
	if (CollectionUtils.isNotEmpty(filterLst)) {
	    super.clear();
	    super.addAll(filterLst);
	} else {
	    super.clear();
	}
    }

    public void filterByProduct(SuscriptionType product) {
	CommOperCollection<CommercialOperation> filterLst = new CommOperCollection<>();
	for (CommercialOperation subscription : this) {
	    if (product.getCode().equals(subscription.getProduct())) {
		filterLst.add(subscription);
	    }
	}
	if (CollectionUtils.isNotEmpty(filterLst)) {
	    super.clear();
	    super.addAll(filterLst);
	}
    }
    
    public void filterBySubscriberType(SuscriptionType type) {
	CommOperCollection<CommercialOperation> filterLst = new CommOperCollection<>();
	for (CommercialOperation subscription : this) {
	    if (type.getCode().equals(subscription.getSubscriber().getType())) {
		filterLst.add(subscription);
	    }
	}
	if (CollectionUtils.isNotEmpty(filterLst)) {
	    super.clear();
	    super.addAll(filterLst);
	}
    }

    public void filterBySalesMode(SaleModalityMobile type) {
	CommOperCollection<CommercialOperation> filterLst = new CommOperCollection<>();
	for (CommercialOperation subscription : this) {
	    if (subscription.getSaleMode() == null || !type.getCode().equals(subscription.getSaleMode())) {
		filterLst.add(subscription);
	    }
	}
	if (CollectionUtils.isNotEmpty(filterLst)) {
	    super.removeAll(filterLst);
	}
    }

    public CommercialOperation filterOneMobileType(SuscriptionType type) {
	for (CommercialOperation subscription : this) {
	    if (type.getCode().equals(subscription.getSubscriber().getType())) {
		return subscription;
	    }
	}
	return null;
    }

    public CommercialOperation filterProvideMobile(ProductType product) {
	for (CommercialOperation subscription : this) {
	    if (product.getCodeDesc().equals(subscription.getProduct())) {
		return subscription;
	    }
	}
	return null;
    }

    public String obtainThreeFirtsDigitFromScore() {
	Map<Integer, String> var = new HashMap<>();
	List<Integer> scoreList = new ArrayList<>();
	for (CommercialOperation comm : this) {
	    String creditScore = comm.getCreditData().getCreditScore();
	    scoreList.add(Integer.parseInt(creditScore));
	    var.put(Integer.parseInt(creditScore), creditScore);
	}
	int min = Collections.min(scoreList);
	return var.get(min).substring(0, 3);
    }
    
    

}
