package com.telefonica.total.generic;

import java.util.ArrayList;

import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.pojo.req.Subscription;

public class SubscriptionCollection<T extends Subscription> extends ArrayList<T> {

    private static final long serialVersionUID = 1L;

    public SubscriptionCollection<Subscription> filterByType(SuscriptionType type) {
	SubscriptionCollection<Subscription> filterLst = new SubscriptionCollection<Subscription>();
	for (Subscription subscription : this) {
	    if (subscription.getType().equals(type.getCode())) {
		filterLst.add(subscription);
	    }
	}
	return filterLst;
    }

}
