package com.telefonica.total.generic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

import com.telefonica.total.dto.CatalogDiscountTempDTO;
import com.telefonica.total.model.CatalogDiscountTemp;
import com.telefonica.total.pojo.req.CommercialOperation;

public class CatalogDiscountCollection<T extends CatalogDiscountTemp> extends ArrayList<CatalogDiscountTempDTO> {

    private static final long serialVersionUID = 1L;
    
    public CatalogDiscountCollection() {
	super();
    }
    
    public CatalogDiscountCollection(List<CatalogDiscountTempDTO> responseList) {
	super.addAll(responseList);
    }
    
    public void filterByType(String type) {
	CatalogDiscountCollection<CatalogDiscountTempDTO> filterLst = new CatalogDiscountCollection<>();
	for (CatalogDiscountTempDTO subscription : this) {
	    if (type.equals(subscription.getDiscountType())) {
		filterLst.add(subscription);
	    }
	}
	if (CollectionUtils.isNotEmpty(filterLst)) {
	    super.clear();
	    super.addAll(filterLst);
	}
    }
    
    

}
