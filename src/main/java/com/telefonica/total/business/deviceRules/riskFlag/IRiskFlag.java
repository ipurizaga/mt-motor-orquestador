package com.telefonica.total.business.deviceRules.riskFlag;

import java.util.List;

import com.telefonica.total.model.CatalogFouthDigitPPF;
import com.telefonica.total.pojo.req.CommercialOperation;

public interface IRiskFlag {

    /***
     * Método que se encarga de realizar el calculo para obtener el flag de riesgo
     * segun el 4to digito del score.
     * 
     * @param scores
     * @return
     */
    CatalogFouthDigitPPF execute(List<CommercialOperation> scores);

}
