package com.telefonica.total.business.deviceRules.priceSimCard;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.ResponseConstants;
import com.telefonica.total.common.util.UtilCollections;
import com.telefonica.total.dto.ProductDtoMappper;
import com.telefonica.total.dto.ProductOfferDTO;
import com.telefonica.total.exception.BusinessException;
import com.telefonica.total.model.CatalogCalPreSimcard;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.Information;
import com.telefonica.total.pojo.req.ReqData;
import com.telefonica.total.repository.CatalogCPreSimcardRepo;

@Service
public class PriceSimCard implements IPriceSimCard {

    @Autowired
    private CatalogCPreSimcardRepo catCPreSimcardRepo;
    
    @Autowired
    private ProductDtoMappper	   productMap;

    @Override
    public List<ProductOfferDTO> execute(List<ProductOfferDTO> products, ReqData request, String flagRisk) {
	ProductOfferDTO prodOff = null;
	Information info = null;
	List<ProductOfferDTO> productsDto = new ArrayList<>();
	CommercialOperation commOpera = UtilCollections.getMobileForQueryDevice(request.getCommOperationInfo().getCommercialOpers());
	List<CatalogCalPreSimcard> preSimcardLst = catCPreSimcardRepo.findAll();
	CatalogCalPreSimcard catalogSim = null;
	for (ProductOfferDTO catOffer : products) {
	    catalogSim = UtilCollections.getSimcardPrice(preSimcardLst, request.getCustomer().getCustomerType(),
		    catOffer.getPoProduct(), commOpera.getOperation(), commOpera.getSaleMode(), flagRisk);
	    //Se añade validación y mensaje de error, cuando no hay Precio de simcard válido
	    if (catalogSim == null) {
		throw new BusinessException("2032");
	    }

	    prodOff = productMap.dtoToProduct(catOffer);
	    info = new Information();
	    info.setCode(ResponseConstants.SIMCARD_PRICE);
	    info.setValue(String.valueOf(catalogSim.getBoPrice()));
	    prodOff.getBillingOffersInfo().add(info);
	    productsDto.add(prodOff);
	}
	return productsDto;
    }

}
