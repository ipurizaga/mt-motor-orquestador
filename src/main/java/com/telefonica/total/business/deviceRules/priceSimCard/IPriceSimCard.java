package com.telefonica.total.business.deviceRules.priceSimCard;

import java.util.List;

import com.telefonica.total.dto.ProductOfferDTO;
import com.telefonica.total.pojo.req.ReqData;

public interface IPriceSimCard {
    
    /***
     * Método que se encaga de obtener el precio de la SImCard.
     * 
     * @param products
     * @param request
     * @param flagRisk
     * @return
     */
    List<ProductOfferDTO> execute(List<ProductOfferDTO> products, ReqData request, String flagRisk);

}
