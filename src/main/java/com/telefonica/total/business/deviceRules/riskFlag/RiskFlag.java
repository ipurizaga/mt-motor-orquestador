package com.telefonica.total.business.deviceRules.riskFlag;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.UtilCollections;
import com.telefonica.total.enums.ProductType;
import com.telefonica.total.model.CatalogFouthDigitPPF;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.repository.Catalog4DigitPPFRepo;

@Service
public class RiskFlag implements IRiskFlag {

    @Autowired
    private Catalog4DigitPPFRepo catalog4DigitPPFRepo;

    @Override
    public CatalogFouthDigitPPF execute(List<CommercialOperation> scores) {
	List<CatalogFouthDigitPPF> lst = catalog4DigitPPFRepo.findAll();
	String fourth = StringUtils.EMPTY;
	for (CommercialOperation commOpera : scores) {
	    if (ProductType.MOBILE.getCodeDesc().equals(commOpera.getProduct())) {
		int size = commOpera.getCreditData().getCreditScore().length();
		fourth = commOpera.getCreditData().getCreditScore().substring(size - 1);
	    }
	}
	return UtilCollections.getFourthDigit(lst, fourth);
    }

}
