package com.telefonica.total.business;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.model.MasterParkMobil;
import com.telefonica.total.pojo.req.CommercialOperation;

/**
 * 
 * @Author: jomapozo.
 * @Datecreation: 2 nov. 2018 10:50:32
 * @FileName: IMastersParksRules.java
 * @AuthorCompany: Telefonica
 * @version: 0.1
 * @Description: Rules for masters tables MASTER_PARQ_FIJO and
 *               MASTER_PARQ_MOVIL;
 */

public interface IMastersParksRules {
    /***
     * Método que se encarga de la obtencion de los datos del usuario del parque
     * movil.
     * 
     * @param subscriptions
     * @return
     */
    List<MobileDTO> getMasterMobile(List<CommercialOperation> subscriptions);

    /***
     * Método que se encarga de la obtencion de los datos del usuario del parque
     * fijo.
     * 
     * @param commercialOperations
     * @return
     */
    FixedDTO getMasterFixed(List<CommercialOperation> commercialOperations);

    /***
     * Método que se encarga de obtener los registros de los datos de usuario de
     * parque movil segun el dni del cliente.
     * 
     * @param document
     * @return
     */
    List<MasterParkMobil> getMasterMobileByDocument(String document);

}
