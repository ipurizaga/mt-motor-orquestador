package com.telefonica.total.business;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.ResponseConstants;
import com.telefonica.total.common.util.RulesValueProp;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.common.util.UtilCollections;
import com.telefonica.total.dto.FinancingSaleCondition;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.PartialRequestDTO;
import com.telefonica.total.dto.PostPaidEasyDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.model.ParamMovTotal;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.CommercialOperationInfo;
import com.telefonica.total.pojo.req.Customer;
import com.telefonica.total.pojo.req.Information;
import com.telefonica.total.pojo.req.Offer;
import com.telefonica.total.pojo.req.SaleCondition;
import com.telefonica.total.pojo.resp.CurrentProductOffer;
import com.telefonica.total.pojo.resp.GeneralSaleCondition;
import com.telefonica.total.pojo.resp.ProductOffer;
import com.telefonica.total.pojo.resp.RespData;
import com.telefonica.total.repository.ParamMovTotalRepo;
import com.telefonica.total.enums.OperationComm;
import com.telefonica.total.enums.OperationDevice;
import com.telefonica.total.enums.ProductType;
import com.telefonica.total.jdbc.PlanFinancingEntity;

@Service
public class GenerateOnlyPlansResponse {

    @Autowired
    private RulesValueProp rulesValue;
    
    @Autowired
    private ParamMovTotalRepo paramMovTotalRepo;

    /**
     * MÃ©todo encargado de armar la estructura de datos a ser enviada como respuesta
     * por el servicio al consultar la opciÃ³n solo planes de MT.
     * 
     * @param partial
     * @return
     */
    public RespData sendResponse(List<PartialRequestDTO> partialList, CommercialOperationInfo commercialOperInfo, List<PlanFinancingEntity> planFinanLst1, List<PlanFinancingEntity> planFinanLst2, Integer quantityFinancing, String sfinance) {

	RespData response = new RespData();
	for (PartialRequestDTO partial : partialList) {
	    /* Send Current Product Information */
	    FixedDTO fixedPark = partial.getCustomerInformation();
	    List<Information> currentProductInfo = new ArrayList<>();
	    actualProductsInformationResponse(fixedPark, partial, currentProductInfo);
	    response.setCurrentProductInfo(currentProductInfo);

	    /* Send Offers */
	    List<ProductOffer> productOffers = new ArrayList<>();
	    ProductOffer prodOffer = null;
	    int contar= 0;
	    
	    ProductFixedDTO p = new ProductFixedDTO();
	    
	    boolean existe = false;
	    
	    for (ProductFixedDTO product : partial.getResultantProductFixedList()) {
		if(product.getCurrentProduct()!=null) {
        		if(product.getCurrentProduct().equals(Constant.CURRENT_OFFER_TYPE)) {
        		    p = product;
        		    existe = true;
        		}
		}else if(product.getCharacteristics()!=null) {
		    
		    for (Information info : product.getCharacteristics()) {
			if(info.getValue().equals(rulesValue.getPRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT())) {
			    p = product;
			    existe = true;
			}
		    }
		}
	    }
	    
	    if(!existe) {
		p = null;
	    }
	    
	    CommercialOperationInfo commerInfo=commercialOperInfo;
	    
	    List<ProductOffer> productOffers1 = new ArrayList<ProductOffer>();
	    
	    for (ProductFixedDTO product : partial.getResultantProductFixedList()) {
		
		//if(customer.getCustomerType().equals(product.getBillingProductDto().getPoCustomerType()) && offer.getPoCode().equals(product.getBillingProductDto().getPoCode())) {
		
		contar++;
		prodOffer = new ProductOffer();
		/* Obtain product offers MT */
		List<Information> billingOffersInfo = new ArrayList<>();

		//produtOffersMTResponse(fixedPark, product, billingOffersInfo, 1, partial.getMobileCustomerInformation());
		

		produtOffersMTResponse(fixedPark, product, billingOffersInfo, 1, partial.getMobileCustomerInformation(), contar, p, commerInfo);
		/** ValidaciÃ³n extra para que se muestre solo cÃ¡scara en el response si es paquete_tipo = movil y 
		*   su pocode no coincide con ofertas habilitadas en la catalog_product_fijo
		**/
		if(product.getPackageType()!=null) {
			if(!product.getPackageType().equals(Constant.TIPO_PAQUETE_MOVIL)) {
				
				prodOffer.setBillingOffersInfo(billingOffersInfo);
			}
			else {
				//if(partial.getPo_code().equals(Constant.POCODE_LMA)) {
				if(product.getMobil0Minutes()!=0 && product.getMobil0QuantityData()!=0) {
					prodOffer.setBillingOffersInfo(billingOffersInfo);
				}
			}
		}
		//prodOffer.setBillingOffersInfo(billingOffersInfo);

		
		/* Obtain Current offers to keep */
		List<CurrentProductOffer> currentOffersToKeep = new ArrayList<>();
		/** ValidaciÃ³n extra para que se muestre solo cÃ¡scara en el response si es paquete_tipo = movil y 
		*   su pocode no coincide con ofertas habilitadas en la catalog_product_fijo
		**/
		if(product.getPackageType()!=null) {
			if(!product.getPackageType().equals(Constant.TIPO_PAQUETE_MOVIL)) {
				
				currentOffersToKeepResponse(product, currentOffersToKeep);
			}
			else {
				//if(partial.getPo_code().equals(Constant.POCODE_LMA)) {
				if(product.getMobil0Minutes()!=0 && product.getMobil0QuantityData()!=0) {
					currentOffersToKeepResponse(product, currentOffersToKeep);
				}
			}
		}
		prodOffer.setCurrentOffersToKeep(currentOffersToKeep);
		
		/* Obtain Sales Conditions */
		Integer cantFinan = 0;
		
		if(planFinanLst1.size()>0) cantFinan++;
		else if(planFinanLst2.size()>0) cantFinan++;
		
		
		List<SaleCondition> saleConditionList = new ArrayList<>();
		salesConditionsResponse(product, saleConditionList, cantFinan);
		/** ValidaciÃ³n extra para que se muestre solo cÃ¡scara en el response si es paquete_tipo = movil y 
		*   su pocode no coincide con ofertas habilitadas en la catalog_product_fijo
		**/
		if(product.getPackageType()!=null) {
			if(!product.getPackageType().equals(Constant.TIPO_PAQUETE_MOVIL)) {
				
				prodOffer.setSaleConditions(saleConditionList);
			}
			else {
				if(product.getMobil0Minutes()!=0 && product.getMobil0QuantityData()!=0) {
				//if(partial.getPo_code().equals(Constant.POCODE_LMA)) {
					prodOffer.setSaleConditions(saleConditionList);
				}
			}
			
		}
		/* Obtain Product Offer Details */
		prodOffer.setPoId(product.getBillingProductDto().getPoId());
		prodOffer.setPoCode(product.getBillingProductDto().getPoCode());
		prodOffer.setPoNameEs(product.getBillingProductDto().getPoNameEsp());
		prodOffer.setBoId(product.getBillingProductDto().getBoId());
		prodOffer.setBoCode(product.getBillingProductDto().getBoCode());
		prodOffer.setBoNameEs(product.getBillingProductDto().getBoNameEsp());
		prodOffer.setPrice(product.getBillingProductDto().getBoPrice());
		if (product.getPriority() != null) {
		    prodOffer.setPriority(Integer.parseInt(product.getPriority()));
		}
		/* Add to product Offer list */
		productOffers.add(prodOffer);
		
		//}
	    }	 
	    
	   
	    
	    //productOffers.clear();
	    
	    Set<ProductOffer> hashSet = new HashSet<ProductOffer>(productOffers);
	    productOffers.clear();
	    productOffers.addAll(hashSet);
	    
	    List<GeneralSaleCondition> generalSaleConditionList = new ArrayList<GeneralSaleCondition>();
	    // llamando a la función generalSalesConditionsResponse, para poder ver los planes de financiamiento
	    generalSalesConditionsResponse(planFinanLst1, planFinanLst2, generalSaleConditionList, quantityFinancing, sfinance, commercialOperInfo);
	    
	    response.setGeneralSaleCondition(generalSaleConditionList);
	    
	    response.setProductOffers(productOffers);
	    break;
	}
	

	/* segunda iteraccion para los casos de combinatorias */
//	if (partialList.size() == 3) {
//	    partialList.remove(0);
//	    for (int i = 0; i < partialList.size(); i++) {
//		/* Send Offers */
//		List<ProductOffer> productOffers = new ArrayList<>();
//		ProductOffer prodOffer = null;
//		for (ProductFixedDTO product : partialList.get(i).getResultantProductFixedList()) {
//		    prodOffer = new ProductOffer();
//		    FixedDTO fixedPark = partialList.get(i).getCustomerInformation();
//		    if(product.getPackageRent() == 199 || product.getPackageRent() == 229) {
//			/* Obtain product offers MT */
//			List<Information> billingOffersInfo = new ArrayList<>();
//			produtOffersMTResponse(fixedPark, product, billingOffersInfo,0);
//			billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL_EVALUADO)
//				.value(String.format(ResponseConstants.MOVIL_NUMERO, i)).build());
//			prodOffer.setBillingOffersInfo(billingOffersInfo);
//			
//			/* Obtain Current offers to keep */
//			List<CurrentProductOffer> currentOffersToKeep = new ArrayList<>();
//			currentOffersToKeepResponse(product, currentOffersToKeep);
//			
//			prodOffer.setCurrentOffersToKeep(currentOffersToKeep);
//				
//			/* Obtain Sales Conditions */
//			List<SaleCondition> saleConditionList = new ArrayList<>();
//			salesConditionsResponse(product, saleConditionList);
//			prodOffer.setSaleConditions(saleConditionList);
//			
//			/* Obtain Product Offer Details */
//			prodOffer.setPoId(product.getBillingProductDto().getPoId());
//			prodOffer.setPoCode(product.getBillingProductDto().getPoCode());
//			prodOffer.setPoNameEs(product.getBillingProductDto().getPoNameEsp());
//			prodOffer.setBoId(product.getBillingProductDto().getBoId());
//			prodOffer.setBoCode(product.getBillingProductDto().getBoCode());
//			prodOffer.setBoNameEs(product.getBillingProductDto().getBoNameEsp());
//			prodOffer.setPrice(product.getBillingProductDto().getBoPrice());
//				
//			/* Add to product Offer list */
//			productOffers.add(prodOffer);
//		    }
//		    
//		}
//		response.getProductOffers().addAll(productOffers);
//	    }
//	}
	return response;
    }
    
    public RespData sendResponseLMA(List<PartialRequestDTO> partialList, CommercialOperationInfo commercialOperInfo, List<PlanFinancingEntity> planFinanLst1, List<PlanFinancingEntity> planFinanLst2, Integer quantityFinancing, String sfinance, Customer customer, Offer offer) {

	RespData response = new RespData();
	for (PartialRequestDTO partial : partialList) {
	    /* Send Current Product Information */
	    FixedDTO fixedPark = partial.getCustomerInformation();
	    List<Information> currentProductInfo = new ArrayList<>();
	    actualProductsInformationResponse(fixedPark, partial, currentProductInfo);
	    response.setCurrentProductInfo(currentProductInfo);

	    /* Send Offers */
	    List<ProductOffer> productOffers = new ArrayList<>();
	    ProductOffer prodOffer = null;
	    int contar= 0;
	    
	    ProductFixedDTO p = new ProductFixedDTO();
	    
	    boolean existe = false;
	    
	    for (ProductFixedDTO product : partial.getResultantProductFixedList()) {
		if(product.getCurrentProduct()!=null) {
        		if(product.getCurrentProduct().equals(Constant.CURRENT_OFFER_TYPE)) {
        		    p = product;
        		    existe = true;
        		}
		}else if(product.getCharacteristics()!=null) {
		    
		    for (Information info : product.getCharacteristics()) {
			if(info.getValue().equals(rulesValue.getPRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT())) {
			    p = product;
			    existe = true;
			}
		    }
		}
	    }
	    
	    if(!existe) {
		p = null;
	    }
	    
	    CommercialOperationInfo commerInfo=commercialOperInfo;
	    
	    List<ProductOffer> productOffers1 = new ArrayList<ProductOffer>();
	    
	    List<ProductFixedDTO> listaProductFixedList = new ArrayList<ProductFixedDTO>();
	    
	    List<Double> creditLimits = new ArrayList<>();
	    
	    List<CommercialOperation> filteredMobilelist = UtilCollections.getCollectionByType(commercialOperInfo.getCommercialOpers(),
		    Constant.MOBILE);
	    
	    for (CommercialOperation commercialOperation : filteredMobilelist) {
		creditLimits.add(commercialOperation.getCreditData().getCreditLimit());
	    }
	    
	    double clientCreditLimit = Collections.max(creditLimits);
	    
	    for (ProductFixedDTO product : partial.getResultantProductFixedList()) {
		
		if (clientCreditLimit > (Double.valueOf(product.getBillingProductDto().getBoPlankRank().intValue()))) {
		    listaProductFixedList.add(product);
		}
		
	    }
	    
	    partial.getResultantProductFixedList().clear();
	    
	    partial.getResultantProductFixedList().addAll(listaProductFixedList);
	    
	    for (ProductFixedDTO product : partial.getResultantProductFixedList()) {
		
		if(customer.getCustomerType().equals(product.getBillingProductDto().getPoCustomerType()) && offer.getPoCode().equals(product.getBillingProductDto().getPoCode())) {
		
		contar++;
		prodOffer = new ProductOffer();
		/* Obtain product offers MT */
		List<Information> billingOffersInfo = new ArrayList<>();

		//produtOffersMTResponse(fixedPark, product, billingOffersInfo, 1, partial.getMobileCustomerInformation());
		

		produtOffersMTResponseLAM(fixedPark, product, billingOffersInfo, 1, partial.getMobileCustomerInformation(), contar, p, commerInfo);
		/** ValidaciÃ³n extra para que se muestre solo cÃ¡scara en el response si es paquete_tipo = movil y 
		*   su pocode no coincide con ofertas habilitadas en la catalog_product_fijo
		**/
		if(product.getPackageType()!=null) {
			if(!product.getPackageType().equals(Constant.TIPO_PAQUETE_MOVIL)) {
				
				prodOffer.setBillingOffersInfo(billingOffersInfo);
			}
			else {
				//if(partial.getPo_code().equals(Constant.POCODE_LMA)) {
				if(product.getMobil0Minutes()!=0 && product.getMobil0QuantityData()!=0) {
					prodOffer.setBillingOffersInfo(billingOffersInfo);
				}
			}
		}
		//prodOffer.setBillingOffersInfo(billingOffersInfo);

		
		/* Obtain Current offers to keep */
		List<CurrentProductOffer> currentOffersToKeep = new ArrayList<>();
		/** ValidaciÃ³n extra para que se muestre solo cÃ¡scara en el response si es paquete_tipo = movil y 
		*   su pocode no coincide con ofertas habilitadas en la catalog_product_fijo
		**/
		if(product.getPackageType()!=null) {
			if(!product.getPackageType().equals(Constant.TIPO_PAQUETE_MOVIL)) {
				
				currentOffersToKeepResponse(product, currentOffersToKeep);
			}
			else {
				//if(partial.getPo_code().equals(Constant.POCODE_LMA)) {
				if(product.getMobil0Minutes()!=0 && product.getMobil0QuantityData()!=0) {
					currentOffersToKeepResponse(product, currentOffersToKeep);
				}
			}
		}
		prodOffer.setCurrentOffersToKeep(currentOffersToKeep);
		
		/* Obtain Sales Conditions */
		Integer cantFinan = 0;
		
		if(planFinanLst1.size()>0) cantFinan++;
		else if(planFinanLst2.size()>0) cantFinan++;
		
		
		List<SaleCondition> saleConditionList = new ArrayList<>();
		salesConditionsResponse(product, saleConditionList, cantFinan);
		/** ValidaciÃ³n extra para que se muestre solo cÃ¡scara en el response si es paquete_tipo = movil y 
		*   su pocode no coincide con ofertas habilitadas en la catalog_product_fijo
		**/
		if(product.getPackageType()!=null) {
			if(!product.getPackageType().equals(Constant.TIPO_PAQUETE_MOVIL)) {
				
				prodOffer.setSaleConditions(saleConditionList);
			}
			else {
				if(product.getMobil0Minutes()!=0 && product.getMobil0QuantityData()!=0) {
				//if(partial.getPo_code().equals(Constant.POCODE_LMA)) {
					prodOffer.setSaleConditions(saleConditionList);
				}
			}
			
		}
		/* Obtain Product Offer Details */
		prodOffer.setPoId(product.getBillingProductDto().getPoId());
		prodOffer.setPoCode(product.getBillingProductDto().getPoCode());
		prodOffer.setPoNameEs(product.getBillingProductDto().getPoNameEsp());
		prodOffer.setBoId(product.getBillingProductDto().getBoId());
		prodOffer.setBoCode(product.getBillingProductDto().getBoCode());
		prodOffer.setBoNameEs(product.getBillingProductDto().getBoNameEsp());
		prodOffer.setPrice(product.getBillingProductDto().getBoPrice());
		if (product.getPriority() != null) {
		    prodOffer.setPriority(Integer.parseInt(product.getPriority()));
		}
		/* Add to product Offer list */
		productOffers.add(prodOffer);
		
		}
	    }	 
	    
	   
	    
	    //productOffers.clear();
	    
	    Set<ProductOffer> hashSet = new HashSet<ProductOffer>(productOffers);
	    productOffers.clear();
	    productOffers.addAll(hashSet);
	    
	    
	    
	    List<GeneralSaleCondition> generalSaleConditionList = new ArrayList<GeneralSaleCondition>();
	    // llamando a la función generalSalesConditionsResponse, para poder ver los planes de financiamiento
	    generalSalesConditionsResponse(planFinanLst1, planFinanLst2, generalSaleConditionList, quantityFinancing, sfinance, commercialOperInfo);
	    
	    response.setGeneralSaleCondition(generalSaleConditionList);
	    
	    response.setProductOffers(productOffers);
	    break;
	}
	

	/* segunda iteraccion para los casos de combinatorias */
//	if (partialList.size() == 3) {
//	    partialList.remove(0);
//	    for (int i = 0; i < partialList.size(); i++) {
//		/* Send Offers */
//		List<ProductOffer> productOffers = new ArrayList<>();
//		ProductOffer prodOffer = null;
//		for (ProductFixedDTO product : partialList.get(i).getResultantProductFixedList()) {
//		    prodOffer = new ProductOffer();
//		    FixedDTO fixedPark = partialList.get(i).getCustomerInformation();
//		    if(product.getPackageRent() == 199 || product.getPackageRent() == 229) {
//			/* Obtain product offers MT */
//			List<Information> billingOffersInfo = new ArrayList<>();
//			produtOffersMTResponse(fixedPark, product, billingOffersInfo,0);
//			billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL_EVALUADO)
//				.value(String.format(ResponseConstants.MOVIL_NUMERO, i)).build());
//			prodOffer.setBillingOffersInfo(billingOffersInfo);
//			
//			/* Obtain Current offers to keep */
//			List<CurrentProductOffer> currentOffersToKeep = new ArrayList<>();
//			currentOffersToKeepResponse(product, currentOffersToKeep);
//			
//			prodOffer.setCurrentOffersToKeep(currentOffersToKeep);
//				
//			/* Obtain Sales Conditions */
//			List<SaleCondition> saleConditionList = new ArrayList<>();
//			salesConditionsResponse(product, saleConditionList);
//			prodOffer.setSaleConditions(saleConditionList);
//			
//			/* Obtain Product Offer Details */
//			prodOffer.setPoId(product.getBillingProductDto().getPoId());
//			prodOffer.setPoCode(product.getBillingProductDto().getPoCode());
//			prodOffer.setPoNameEs(product.getBillingProductDto().getPoNameEsp());
//			prodOffer.setBoId(product.getBillingProductDto().getBoId());
//			prodOffer.setBoCode(product.getBillingProductDto().getBoCode());
//			prodOffer.setBoNameEs(product.getBillingProductDto().getBoNameEsp());
//			prodOffer.setPrice(product.getBillingProductDto().getBoPrice());
//				
//			/* Add to product Offer list */
//			productOffers.add(prodOffer);
//		    }
//		    
//		}
//		response.getProductOffers().addAll(productOffers);
//	    }
//	}
	return response;
    }
    
    public RespData sendResponseMT(List<PartialRequestDTO> partialList, CommercialOperationInfo commercialInfo, List<PlanFinancingEntity> planFinanLst1, List<PlanFinancingEntity> planFinanLst2, Integer quantityFinancing, String sfinance, String parrilla, String modemEquipmentOfferCopy) {

	RespData response = new RespData();
	for (PartialRequestDTO partial : partialList) {
	    /* Send Current Product Information */
	    FixedDTO fixedPark = partial.getCustomerInformation();
	    List<Information> currentProductInfo = new ArrayList<>();
	    actualProductsInformationResponse(fixedPark, partial, currentProductInfo);
	    response.setCurrentProductInfo(currentProductInfo);

	    /* Send Offers */
	    List<ProductOffer> productOffers = new ArrayList<>();
	    ProductOffer prodOffer = null;
	    int contar= 0;
	    
	    ProductFixedDTO p = new ProductFixedDTO();
	    
	    boolean existe = false;
	    
	    for (ProductFixedDTO product : partial.getResultantProductFixedList()) {
		if(product.getCurrentProduct()!=null) {
        		if(product.getCurrentProduct().equals(Constant.CURRENT_OFFER_TYPE)) {
        		    p = product;
        		    existe = true;
        		}
		}else if(product.getCharacteristics()!=null) {
		    
		    for (Information info : product.getCharacteristics()) {
			if(info.getValue().equals(rulesValue.getPRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT())) {
			    product.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
			    p = product;
			    existe = true;
			}
		    }
		}
	    }
	    
	    if(!existe) {
		p = null;
	    }
	    
	    int cantidad = partial.getResultantProductFixedList().size();
	    
	    boolean validarFinal = false;
	    
	    int contando= 0;
	    
	    Character cobIntHfc = fixedPark.getHfcCobInternet() != null ? fixedPark.getHfcCobInternet() : '0';
	    
	    for (ProductFixedDTO productAnt : partial.getResultantProductFixedList()) {
		contando++;
		if(contando == cantidad) {
		
		
        		for (Information information : productAnt.getCharacteristics()) {
                		if(information.getValue().equals(rulesValue.getPRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT())) {
                		    if(productAnt.getInternetDestinyTechnology().equals("FTTH")) {
                			
                			validarFinal = true;
                			
                		    }else if(productAnt.getInternetDestinyTechnology().equals("HFC") &&
                			    (commercialInfo.getCoverage().toLowerCase().equals("fiber")?"FTTH":commercialInfo.getCoverage()).equals("HFC") &&
                			    cobIntHfc == '1') {
                			validarFinal = true;
                		    }
                		}
        		
        		}
		
		}
		
	    }
	    
	    if(validarFinal) {
		partial.getResultantProductFixedList().remove(cantidad-1);
	    }
	    
	    ParamMovTotal pmt = new ParamMovTotal();
	    
	    List<ParamMovTotal> listParamMov = this.paramMovTotalRepo.findAll();
	    
	    for (ParamMovTotal paramMovTotal : listParamMov) {
		    
		    if(paramMovTotal.getGrupoParam().equals("MIGRACION_TECNOLOGICA")) {
			
			pmt = new ParamMovTotal();
			
			pmt = paramMovTotal;
			
			break;
			
		    }
		    
	    }
	    
	    boolean existeMigracion = true;
	    
	    if(parrilla.equals("P3")) {
	    
        	    
        	    if(!validarFinal) {
        		if(pmt.getValor().equals("0")) {
        		    
        		    ProductFixedDTO productSelected = null;
        		    
        		    boolean existeCurrentProduct = false;
        		    
        		    for (ProductFixedDTO productAnt : partial.getResultantProductFixedList()) {
        			
        			boolean existeActual = false;
        	        		for (Information information : productAnt.getCharacteristics()) {
        	                		if(information.getValue().equals(rulesValue.getPRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT())) {
        	                		    existeActual = true;
        	                		}
        	        		
        	        		}
        	        		
        	        	if(!existeActual) {
        	        	    if(!(productAnt.getCurrentProduct() == null)) {
        	        		if(productAnt.getCurrentProduct().equals("Producto Actual")) {
        	        		    existeActual = true;
        	        		    existeCurrentProduct = true;
        	        		}
        	        	    }
        	        	}
        	        		
        	        	if(existeActual) {
        	        	    
        	        	    productSelected = productAnt;
        	        	    
        	        	    break;
        	        	}else {
        	        	    
        	        	}
        			
        		    }
        		    
        		    int indice = -1;
        		    
        		    for (int i = 0; i < partial.getResultantProductFixedList().size(); i++) {
				
        			if(i > 0) {       			    
        			
        			
                			boolean existeNoActual = false;
        	        		for (Information information : partial.getResultantProductFixedList().get(i).getCharacteristics()) {
        	                		if(information.getValue().equals(rulesValue.getPRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT())) {
        	                		    existeNoActual = true;
        	                		}
        	        		
        	        		}
                			
        	        		if(existeNoActual) {
        	        		
        	        		    	if(!(productSelected==null)) {
                                			if(productSelected.getInternetSpeed().equals(partial.getResultantProductFixedList().get(i).getInternetSpeed())) {
                                			    indice = i;
                                			}
        	        		    	}
                			
        	        		}
	        		
        			}
        			
			    }
        		    
        		    if(indice == -1 && existeCurrentProduct) {
        			for (int i = 0; i < partial.getResultantProductFixedList().size(); i++) {
        			    if(i > 0) { 
        	        		    	if(!(productSelected==null)) {
                                			if(productSelected.getInternetSpeed().equals(partial.getResultantProductFixedList().get(i).getInternetSpeed())) {
                                			    indice = i;
                                			}
        	        		    	}
        			    }
        			}
        		    }
        		    
        		    if(indice > -1) {        			
        			
        		    partial.getResultantProductFixedList().remove(indice);
        		    existeMigracion = false;
        		    
        		    }
        		}
        	    }
	    
	    }	    
	    
	    for (ProductFixedDTO product : partial.getResultantProductFixedList()) {
		contar++;
		prodOffer = new ProductOffer();
		/* Obtain product offers MT */
		List<Information> billingOffersInfo = new ArrayList<>();

		//produtOffersMTResponse(fixedPark, product, billingOffersInfo, 1, partial.getMobileCustomerInformation());
				

		produtOffersMTResponseMT(fixedPark, product, billingOffersInfo, 1, partial.getMobileCustomerInformation(), contar, p, commercialInfo, cantidad, pmt, modemEquipmentOfferCopy, existeMigracion);
		/** ValidaciÃ³n extra para que se muestre solo cÃ¡scara en el response si es paquete_tipo = movil y 
		*   su pocode no coincide con ofertas habilitadas en la catalog_product_fijo
		**/
		if(product.getPackageType()!=null) {
			if(!product.getPackageType().equals(Constant.TIPO_PAQUETE_MOVIL)) {
				
				prodOffer.setBillingOffersInfo(billingOffersInfo);
			}
			else {
				//if(partial.getPo_code().equals(Constant.POCODE_LMA)) {
				if(product.getMobil0Minutes()!=0 && product.getMobil0QuantityData()!=0) {
					prodOffer.setBillingOffersInfo(billingOffersInfo);
				}
			}
		}
		//prodOffer.setBillingOffersInfo(billingOffersInfo);

		
		/* Obtain Current offers to keep */
		List<CurrentProductOffer> currentOffersToKeep = new ArrayList<>();
		/** ValidaciÃ³n extra para que se muestre solo cÃ¡scara en el response si es paquete_tipo = movil y 
		*   su pocode no coincide con ofertas habilitadas en la catalog_product_fijo
		**/
		if(product.getPackageType()!=null) {
			if(!product.getPackageType().equals(Constant.TIPO_PAQUETE_MOVIL)) {
				
				currentOffersToKeepResponse(product, currentOffersToKeep);
			}
			else {
				//if(partial.getPo_code().equals(Constant.POCODE_LMA)) {
				if(product.getMobil0Minutes()!=0 && product.getMobil0QuantityData()!=0) {
					currentOffersToKeepResponse(product, currentOffersToKeep);
				}
			}
		}
		prodOffer.setCurrentOffersToKeep(currentOffersToKeep);
		
		/* Obtain Sales Conditions */
		
        Integer cantFinan = 0;
		
		if(planFinanLst1.size()>0) cantFinan++;
		else if(planFinanLst2.size()>0) cantFinan++;
		
		List<SaleCondition> saleConditionList = new ArrayList<>();
		salesConditionsResponse(product, saleConditionList, cantFinan);
		/** ValidaciÃ³n extra para que se muestre solo cÃ¡scara en el response si es paquete_tipo = movil y 
		*   su pocode no coincide con ofertas habilitadas en la catalog_product_fijo
		**/
		if(product.getPackageType()!=null) {
			if(!product.getPackageType().equals(Constant.TIPO_PAQUETE_MOVIL)) {
				
				prodOffer.setSaleConditions(saleConditionList);
			}
			else {
				if(product.getMobil0Minutes()!=0 && product.getMobil0QuantityData()!=0) {
				//if(partial.getPo_code().equals(Constant.POCODE_LMA)) {
					prodOffer.setSaleConditions(saleConditionList);
				}
			}
			
		}
		/* Obtain Product Offer Details */
		prodOffer.setPoId(product.getBillingProductDto().getPoId());
		prodOffer.setPoCode(product.getBillingProductDto().getPoCode());
		prodOffer.setPoNameEs(product.getBillingProductDto().getPoNameEsp());
		prodOffer.setBoId(product.getBillingProductDto().getBoId());
		prodOffer.setBoCode(product.getBillingProductDto().getBoCode());
		prodOffer.setBoNameEs(product.getBillingProductDto().getBoNameEsp());
		prodOffer.setPrice(product.getBillingProductDto().getBoPrice());
		if (product.getPriority() != null) {
		    prodOffer.setPriority(Integer.parseInt(product.getPriority()));
		}
		/* Add to product Offer list */
		productOffers.add(prodOffer);
	    }
	    
	 // llamando a la función generalSalesConditionsResponse, para poder ver los planes de financiamiento
	    List<GeneralSaleCondition> generalSaleConditionList = new ArrayList<GeneralSaleCondition>();
	    generalSalesConditionsResponse(planFinanLst1, planFinanLst2, generalSaleConditionList, quantityFinancing, sfinance, commercialInfo);
	    
	    response.setGeneralSaleCondition(generalSaleConditionList);
	    
	    response.setProductOffers(productOffers);
	    break;
	}
	

	
	return response;
    }

    /* Metodo para obtner los productos actuales */
    private void actualProductsInformationResponse(FixedDTO fixedPark, PartialRequestDTO partial, List<Information> currentProductInfo) {
	if (fixedPark.getPresentRents() != null) {
	    currentProductInfo.addAll(fixedPark.getPresentRents());
	}
	if (fixedPark.getSummatoryPresentRents() != null) {
	    currentProductInfo.add(Information.builder().code(ResponseConstants.RENTATOTAL_ADICIONALESACTUALES)
		    .value(fixedPark.getSummatoryPresentRents().toString()).build());
	}
	if (fixedPark.getCycle() != null) {
	    currentProductInfo
		    .add(Information.builder().code(ResponseConstants.FIJO_CICLO_ACTUAL).value(fixedPark.getCycle().toString()).build());
	}
	if (fixedPark.getSpeedInternet() != null) {
	    currentProductInfo.add(Information.builder().code(ResponseConstants.PF_INTERNET_VELOCIDAD)
		    .value(fixedPark.getSpeedInternet().toString()).build());
	}
	
	if (fixedPark.getTechnologyInternet() != null) {
	    currentProductInfo.add(Information.builder().code(ResponseConstants.PF_INTERNET_TECNOLOGIA)
		    .value(fixedPark.getTechnologyInternet()).build());
	}
	if (fixedPark.getCableType() != null) {
	    currentProductInfo.add(Information.builder().code(ResponseConstants.PF_CABLE_TIPO)
		    .value(fixedPark.getCableType()).build());
	}
	if (fixedPark.getCableQuantityDecoSmart() != null) {
	    currentProductInfo.add(Information.builder().code(ResponseConstants.PF_CABLE_CANTIDADDECOSSMART)
		    .value(fixedPark.getCableQuantityDecoSmart().toString()).build());
	}
	if (fixedPark.getCableQuantityDecoHd() != null) {
	    currentProductInfo.add(Information.builder().code(ResponseConstants.PF_CABLE_CANTIDADDECOSHD)
		    .value(fixedPark.getCableQuantityDecoHd().toString()).build());
	}
	if (fixedPark.getQuantityWifiUltraInternet() != null) {
	    currentProductInfo.add(Information.builder().code(ResponseConstants.PF_INTERNET_ULTRAWIFICANTIDAD)
		    .value(fixedPark.getQuantityWifiUltraInternet().toString()).build());
	}
	if (fixedPark.getActualFixedRent() != null) {
	    currentProductInfo.add(Information.builder().code(ResponseConstants.PF_PAQUETE_RENTA)
		    .value(fixedPark.getActualFixedRent().toString()).build());
	}
	if (fixedPark.getPermanentDiscount() != null) {
	    currentProductInfo.add(Information.builder().code(ResponseConstants.PF_DESCUENTOPERMANENTE)
		    .value(fixedPark.getPermanentDiscount().toString()).build());
	}
	
	if (fixedPark.getPresentARPA() != null) {
	    currentProductInfo.add(Information.builder().code(ResponseConstants.PF_ARPA)
			    .value(fixedPark.getPresentARPA().toString()).build());
	}
	
	if(fixedPark.getModenNameInternet() != null) {
	    currentProductInfo.add(Information.builder().code(ResponseConstants.PF_INTERNET_MODEM)
		    .value(fixedPark.getModenNameInternet()).build());
	}
	
	if (fixedPark.getDeposed() != null) {
	    currentProductInfo.add(Information.builder().code(ResponseConstants.FLAG_DESPOSICIONADO)
		    .value(fixedPark.getDeposed().toString()).build());
	}

	int number = 1;
	for (MobileDTO mobile : partial.getMobileCustomerInformation()) {
	    currentProductInfo.add(Information.builder().code(String.format(ResponseConstants.PM_MOVILX_RENTA, number))
		    .value(mobile.getMobileRentMonoProduct().toString()).build());
	    currentProductInfo.add(Information.builder().code(String.format(ResponseConstants.PM_MOVILX_DESCPERMANENTE, number))
		    .value(mobile.getPermanentDisccount().toString()).build());
	    currentProductInfo.add(Information.builder().code(String.format(ResponseConstants.PM_MOVILX_CANTDATOS, number))
		    .value(mobile.getMobileQuantityData().toString()).build());
	    currentProductInfo.add(Information.builder().code(String.format(ResponseConstants.PM_MOVILX_CONSUMO_MB, number))
		    .value(mobile.getMobileMbConsumption().toString()).build());
	    if (mobile.getMobileApps() != null) {
		currentProductInfo.add(Information.builder().code(String.format(ResponseConstants.PM_MOVILX_APPS, number))
			.value(mobile.getMobileApps().toString()).build());
	    }
	    if (mobile.getDuplicateBonusFlag() != null) {
		currentProductInfo.add(Information.builder().code(String.format(ResponseConstants.PM_MOVIL_BONODUPLICA_FLAG, number))
			.value(mobile.getDuplicateBonusFlag().toString()).build());
	    }
	    if (mobile.getDuplicateBonusExpiration() != null) {
		currentProductInfo.add(Information.builder().code(String.format(ResponseConstants.PM_MOVIL_BONODUPLICA_VENC, number))
			.value(mobile.getDuplicateBonusExpiration().toString()).build());
	    }
	    if (mobile.getPresentDuplicateBonusDataQuantity() != null) {
		currentProductInfo.add(Information.builder().code(String.format(ResponseConstants.PM_MOVIL_BONODUPLICA_CANT, number))
			.value(mobile.getPresentDuplicateBonusDataQuantity().toString()).build());
	    }
	    if (StringUtils.isNotBlank(mobile.getPresentDuplicateBonusEndDate())) {
		currentProductInfo.add(Information.builder().code(String.format(ResponseConstants.PM_MOVIL_BONODUPLICA_FECHA_FIN, number))
			.value(mobile.getPresentDuplicateBonusEndDate()).build());
	    } 
	    if (mobile.getPresentTotalDataQuantity() != null) {
		currentProductInfo.add(Information.builder().code(String.format(ResponseConstants.PM_MOVIL_BONODUPLICA_DATOS_TOTAL, number))
			.value(mobile.getPresentTotalDataQuantity().toString()).build());
	    }
	    if (mobile.getPromotionalDiscount() != null) {
		currentProductInfo.add(Information.builder().code(String.format(ResponseConstants.PM_MOVIL_DESCUENTOPROM, number))
			.value(mobile.getPromotionalDiscount().toString()).build());
	    }
	    if (mobile.getPromotionalDiscountExpiration() != null) {
		currentProductInfo.add(Information.builder().code(String.format(ResponseConstants.PM_MOVIL_DESCUENTOPROM_VENC, number))
			.value(mobile.getPromotionalDiscountExpiration().toString()).build());
	    }
	    if (mobile.getPresentPromotionalDiscountRent() != null) {
		currentProductInfo.add(Information.builder().code(String.format(ResponseConstants.PM_MOVIL_DESCUENTOPROM_RENTA, number))
			.value(mobile.getPresentPromotionalDiscountRent().toString()).build());
	    }
	    if (mobile.getPresentPromotionalFinalRent() != null) {
		currentProductInfo
			.add(Information.builder().code(String.format(ResponseConstants.PM_MOVIL_DESCUENTOPROM_RENTA_FINAL, number))
				.value(mobile.getPresentPromotionalFinalRent().toString()).build());
	    }
	    if (StringUtils.isNotBlank(mobile.getPresentPromotionalEndDate())) {
		currentProductInfo.add(Information.builder().code(String.format(ResponseConstants.PM_MOVIL_DESCUENTOPROM_FECHA_FIN, number))
			.value(mobile.getPresentPromotionalEndDate()).build());
	    }
	    number++;
	}
	if (partial.getActualRent() != null) {
	    currentProductInfo
		    .add(Information.builder().code(ResponseConstants.RENTA_ACTUAL).value(partial.getActualRent().toString()).build());
	}
    }
    
    /* Metodos para obtner las ofertas MT */
    private void produtOffersMTResponse(FixedDTO fixedPark, ProductFixedDTO product, List<Information> billingOffersInfo, Integer cont, List<MobileDTO> movilDevices, int oferta, ProductFixedDTO p, CommercialOperationInfo commerInfo) {
	
    boolean validaUpfront = false;
    String campaignValue = null;
    Information respValUpFrn = new Information("","");

    //Information respValUpFrn = new Information("","");
    	
    if (StringUtils.isNotBlank(product.getLineName())) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.FIJO_LINEA).value(product.getLineName()).build());
	}
	if (product.getInternetSpeed() != null) {
	    billingOffersInfo
		    .add(Information.builder().code(ResponseConstants.FIJO_INTERNET).value(product.getInternetSpeed().toString()).build());
	}
	if (StringUtils.isNotBlank(product.getCableType())) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.FIJO_TELEVISION).value(product.getCableType()).build());
	}
	
	
	if (product.getMobil0Minutes() != null) {
	    billingOffersInfo
		    .add(Information.builder().code(ResponseConstants.MOVIL1_MINUTOS).value(product.getMobil0Minutes().toString()).build());
	}
	
	boolean actual = false;
	
	if(product.getCurrentProduct()!=null) {
		
        	if(product.getCurrentProduct().equals(Constant.CURRENT_OFFER_TYPE)) {
        	    	billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL1_DATOS)
    		.value(product.getMobil0QuantityData().toString()).build());
        	    	actual = true;
        	}
    	
	}
	
	if(!actual) {
	
            if (product.getMobil0QuantityData() != null) {
                if (fixedPark.getOnlyOneMobileIndicator() && product.getMobileQuantityDataSumatory() != null) {
            		
            	//billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL1_DATOS)
            	//	.value(product.getMobileQuantityDataSumatory().toString()).build());
            		
            	billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL1_DATOS)
            		.value(product.getMobileQuantityDataSumatory().toString()).build());
            		 
                } else {
            	//Ojo: const MOVIL1_DATOS se relaciona a getMobil0QuantityData(), puede inducir a error de llamar getMobil1QuantityData()
            	billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL1_DATOS)
            		.value(product.getMobil0QuantityData().toString()).build());
                }
            }
            
	}
	
	
	

	//if(!(product.getEnabled().equals(Constant.TWO))) {
	if(product.getPackageType()!=null) {
		if(!(product.getPackageType().equals(Constant.TIPO_PAQUETE_MOVIL))) {

	if(product.getOfferTotalRent()!=null) {
	    ParamMovTotal pmt = getArcheType(product.getOfferTotalRent());
	   
	    if(pmt!=null) {
	    
        	    billingOffersInfo.add(Information.builder().code(ResponseConstants.PLAN_OFFER_ARQUETIPO)
        		    .value(pmt.getValor()).build());
	    
	    }
	}
	/*
	if(product.getInternetTechnology().equals(rulesValue.INTERNET_TECNOLOGIA_FTTH)) {
	    
		billingOffersInfo.add(Information.builder().code(ResponseConstants.PLAN_OFFER_ARQUETIPO)
			    .value(ResponseConstants.ARQUETIPO_PRO).build());
	    
	}else {
	    if (oferta == 1 || oferta == 2) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.PLAN_OFFER_ARQUETIPO)
			    .value(ResponseConstants.ARQUETIPO_LITE).build());
		}
		
	    if (oferta == 3 || oferta == 4) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.PLAN_OFFER_ARQUETIPO)
			    .value(ResponseConstants.ARQUETIPO_FULL).build());
		}
	}*/
	
	/*if(product.getInternetTechnology().equals(rulesValue.INTERNET_TECNOLOGIA_FTTH)) {
	    
		billingOffersInfo.add(Information.builder().code(ResponseConstants.PAQUETE_BENEFICIO)
			    .value(ResponseConstants.PAQUETE_BENEFICIO_EXPERTO_WIFI).build());
	    
	}*/
	
	if(product.getPackageBenefit()!=null) {
		if(!product.getPackageBenefit().equals("")) {
			billingOffersInfo.add(Information.builder().code(ResponseConstants.PAQUETE_BENEFICIO)
				    .value(product.getPackageBenefit()).build());
			//.value(ResponseConstants.PAQUETE_BENEFICIO_EXPERTO_WIFI).build());
		}
	}

	
	if(!(product.getEnabled().equals(Constant.TWO))) {
	

        	if(product.getInternetSpeed()  != null && fixedPark.getSpeedInternet() != null) {	    
        	            	
                	billingOffersInfo.add(Information.builder().code(ResponseConstants.PF_INTERNET_VELOCIDAD_GAP)
                		    .value(String.valueOf(product.getInternetSpeed()-fixedPark.getSpeedInternet())).build());
        		
        	}else {
        	    
        	    int pis = 0, fsi = 0;
        	    if(product.getInternetSpeed()  == null) {
        		pis = 0;
        	    }else {
        		pis = product.getInternetSpeed();
        	    }
        	    
        	    if(fixedPark.getSpeedInternet()  == null) {
        		fsi = 0;
        	    }else {
        		fsi = fixedPark.getSpeedInternet();
        	    }
        	    
        	    billingOffersInfo.add(Information.builder().code(ResponseConstants.PF_INTERNET_VELOCIDAD_GAP)
        		    .value(String.valueOf(pis-fsi)).build());
        	}
	
		}
	}
	
	
	if(product.getCurrentProduct()!=null) {
	
        	if(product.getCurrentProduct().equals(Constant.CURRENT_OFFER_TYPE)) {
        	    
        	    billingOffersInfo.add(Information.builder().code(ResponseConstants.CARACTERISTICA)
        		    .value(rulesValue.PRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT).build());
        	}
	
	}
	
	//Se toma el valor del po_id del LMA recibido del ProductFixedDTO,
	//debido a que cada vez que se agrega LMA a alguna oferta se Setea el valor
	if(product.getBillingProductDto().getPoId() != null) {
		if(product.getLma_Po() != null){
			billingOffersInfo.add(Information.builder().code(ResponseConstants.PO_ID)
        		    .value(product.getLma_Po()).build());	
		}       	
	}
	
	//Se toma el valor del bo_id del LMA recibido del ProductFixedDTO,
	//debido a que cada vez que se agrega LMA a alguna oferta se Setea el valor
	if(product.getBillingProductDto().getBoId() != null) {		
		if(product.getLma_Bo() != null){
			billingOffersInfo.add(Information.builder().code(ResponseConstants.BO_ID)
	    		    .value(product.getLma_Bo()).build());	
		}    	
	}
	
	if (product.getMobil0Apps() != null) {
	    billingOffersInfo
		    .add(Information.builder().code(ResponseConstants.MOVIL1_APPS).value(product.getMobil0Apps().toString()).build());
	}
	
	if(!actual) {
	
        	if (product.getMobil0Minutes() != null && !fixedPark.getOnlyOneMobileIndicator()) {
        	    /*billingOffersInfo
        		    .add(Information.builder().code(ResponseConstants.MOVIL2_MINUTOS).value(product.getMobil0Minutes().toString()).build());*/
        	    
        	      billingOffersInfo
        		    .add(Information.builder().code(ResponseConstants.MOVIL2_MINUTOS).value(String.valueOf(product.getMobil0Minutes()-10)).build());
        	      
        	      billingOffersInfo
        		    .add(Information.builder().code(ResponseConstants.MOVIL2_MINUTOS_OTROS_OPERADORES).value(String.valueOf(10)).build());
        	}
	
	}else {
	    if(commerInfo.getCommercialOpers().size()>2) {
        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
        		    
        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
        		
                		if(actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT)) {
                		    
                		    if (product.getMobil0Minutes() != null && !fixedPark.getOnlyOneMobileIndicator()) {
                			
                        		    billingOffersInfo
                        		    .add(Information.builder().code(ResponseConstants.MOVIL2_MINUTOS).value(String.valueOf(product.getMobil0Minutes()-10)).build());
                        	      
                        		    billingOffersInfo
                        		    .add(Information.builder().code(ResponseConstants.MOVIL2_MINUTOS_OTROS_OPERADORES).value(String.valueOf(10)).build());
                		    }
                		}
        		
        		    }
        		
        	    }
	    }
	    
	}
	
	
	if (product.getMobil1QuantityData() != null && !fixedPark.getOnlyOneMobileIndicator()) {
	   /* billingOffersInfo.add(
		    Information.builder().code(ResponseConstants.MOVIL2_DATOS).value(product.getMobil1QuantityData().toString()).build());*/
	    
	    //System.out.println("product.getMobil1QuantityData() ====> " + product.getMobil1QuantityData());
	    //System.out.println("movilDevices.get(1).getMobileQuantityData() ====> " + movilDevices.get(1).getMobileQuantityData());
	    //System.out.println("RESTA: "+(product.getMobil1QuantityData() - movilDevices.get(1).getMobileQuantityData()));
	    
	    if(movilDevices.size() > 1) {
		
		//if(!actual && (commerInfo.getCommercialOpers().get(2).getSubscriber()==null)) {
		if(!actual && (commerInfo.getCommercialOpers().get(2).getSubscriber()==null || commerInfo.getCommercialOpers().get(2).getSubscriber().getBoId()==null)) {
		
        	    billingOffersInfo.add(
        		   // Information.builder().code(ResponseConstants.MOVIL2_DATOS).value(String.valueOf(product.getMobil1QuantityData() - movilDevices.get(1).getMobileQuantityData())).build());
        	    Information.builder().code(ResponseConstants.MOVIL2_DATOS).value(String.valueOf(product.getMobil1QuantityData())).build());
		}
	    }else {
		 billingOffersInfo.add(
		    Information.builder().code(ResponseConstants.MOVIL2_DATOS).value(product.getMobil1QuantityData().toString()).build());
	
	    }
	}
	
	if(p!=null) {
	    	
	    if (product.getMobil1QuantityData() != null && p.getMobil1QuantityData() != null) {
		   
		    
		    
		    
		    billingOffersInfo.add(
			   // Information.builder().code(ResponseConstants.MOVIL2_DATOS).value(String.valueOf(product.getMobil1QuantityData() - movilDevices.get(1).getMobileQuantityData())).build());
		    Information.builder().code(ResponseConstants.PF_DATOS_MOVILES_GAP).value(String.valueOf(product.getMobil0QuantityData() - p.getMobil0QuantityData())).build());
		    
		   
	    }
	    
	}else {
	
        	if(movilDevices!=null) {
        		
                	if (product.getMobil1QuantityData() != null && movilDevices.get(0).getMobileQuantityData() != null) {
                		   /* billingOffersInfo.add(
                			    Information.builder().code(ResponseConstants.MOVIL2_DATOS).value(product.getMobil1QuantityData().toString()).build());*/
                		    
                		    //System.out.println("product.getMobil1QuantityData() ====> " + product.getMobil1QuantityData());
                		    //System.out.println("movilDevices.get(1).getMobileQuantityData() ====> " + movilDevices.get(0).getMobileQuantityData());
                		    //System.out.println("RESTA: "+(product.getMobil1QuantityData() - movilDevices.get(0).getMobileQuantityData()));
                		    
                		    
                		    
                		    billingOffersInfo.add(
                			   // Information.builder().code(ResponseConstants.MOVIL2_DATOS).value(String.valueOf(product.getMobil1QuantityData() - movilDevices.get(1).getMobileQuantityData())).build());
                		    Information.builder().code(ResponseConstants.PF_DATOS_MOVILES_GAP).value(String.valueOf(product.getMobil0QuantityData() - movilDevices.get(0).getMobileQuantityData())).build());
                		    
                		   
                	}
        	
        	}
	
	}
	
	if(!actual) {
	
        	if (product.getMobil0Apps() != null && !fixedPark.getOnlyOneMobileIndicator()) {
        	    billingOffersInfo
        		    .add(Information.builder().code(ResponseConstants.MOVIL2_APPS).value(product.getMobil0Apps().toString()).build());
        	}
	
	}else {
	    if(commerInfo.getCommercialOpers().size()>2) {
        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
        		    
        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
        		
                		if(actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT)) {
                		    if (product.getMobil0Apps() != null && !fixedPark.getOnlyOneMobileIndicator()) {
                        		    billingOffersInfo
                        		    .add(Information.builder().code(ResponseConstants.MOVIL2_APPS).value(product.getMobil0Apps().toString()).build());
                		    }
                		}
        		
        		    }
        		
        	    }
	    }
	    
	}
	
	
	if (product.getMobil0Sms() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL1_SMS).value(product.getMobil0Sms()).build());
	}

	if(!actual) {
        	if (product.getMobil0Sms() != null && !fixedPark.getOnlyOneMobileIndicator()) {
        	    billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL2_SMS).value(product.getMobil0Sms()).build());
        	}
	}else {
	    if(commerInfo.getCommercialOpers().size()>2) {
        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
        		    
        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
        		
                		if(actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT)) {
                		    if (product.getMobil0Sms() != null && !fixedPark.getOnlyOneMobileIndicator()) {
                			billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL2_SMS).value(product.getMobil0Sms()).build());
                		    }
                		}
        		
        		    }
        		
        	    }
	    }
	    
	}
	
	if (product.getMobil0Roaming() != null) {
	    billingOffersInfo
		    .add(Information.builder().code(ResponseConstants.MOVIL1_ROAMING).value(product.getMobil0Roaming().toString()).build());
	}
	
	if(!actual) {
        	if (product.getMobil1Roaming() != null && !fixedPark.getOnlyOneMobileIndicator()) {
        	    billingOffersInfo
        		    .add(Information.builder().code(ResponseConstants.MOVIL2_ROAMING).value(product.getMobil1Roaming().toString()).build());
        	}
	}else {
	    if(commerInfo.getCommercialOpers().size()>2) {
        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
        		    
        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
        		
                		if(actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT)) {
                		    if (product.getMobil1Roaming() != null && !fixedPark.getOnlyOneMobileIndicator()) {
                        		    billingOffersInfo
                        		    .add(Information.builder().code(ResponseConstants.MOVIL2_ROAMING).value(product.getMobil1Roaming().toString()).build());
                		    }
                		}
        		
        		    }
        		
        	    }
	    }
	    
	}
	
	
	if (product.getMobil0WaInt() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL1_WAINT).value(product.getMobil0WaInt()).build());
	}
	
	if(!actual) {
        	if (product.getMobil0WaInt() != null && !fixedPark.getOnlyOneMobileIndicator()) {
        	    billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL2_WAINT).value(product.getMobil0WaInt()).build());
        	}
	}else {
	    if(commerInfo.getCommercialOpers().size()>2) {
        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
        		    
        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
        		
                		if(actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT)) {
                		    if (product.getMobil0WaInt() != null && !fixedPark.getOnlyOneMobileIndicator()) {
                			billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL2_WAINT).value(product.getMobil0WaInt()).build());
                		    }
                		}
        		
        		    }
        		
        	    }
	    }
	    
	}
	
	
	if (product.getMobil0Rent() != null) {
	    if (fixedPark.getOnlyOneMobileIndicator() && product.getMobil1Rent() != null) {
		billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL1_RENTA)
			.value(String.valueOf(product.getMobil0Rent() + product.getMobil1Rent())).build());
	    } else {
		billingOffersInfo
			.add(Information.builder().code(ResponseConstants.MOVIL1_RENTA).value(product.getMobil0Rent().toString()).build());
	    }
	}
	
	if(!actual) {
        	if (product.getMobil1Rent() != null && !fixedPark.getOnlyOneMobileIndicator() && movilDevices.size() == 2) {
        	    billingOffersInfo
        	    .add(Information.builder().code(ResponseConstants.MOVIL2_RENTA).value(product.getMobil1Rent().toString()).build());
        	}
	}else {
	    if(commerInfo.getCommercialOpers().size()>2) {
        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
        		    
        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
        		
                		if(actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT)) {
                		    if (product.getMobil1Rent() != null && !fixedPark.getOnlyOneMobileIndicator() && movilDevices.size() == 2) {
                        		    billingOffersInfo
                                	    .add(Information.builder().code(ResponseConstants.MOVIL2_RENTA).value(product.getMobil1Rent().toString()).build());
                		    }
                		}
        		
        		    }
        		
        	    }
	    }
	    
	}
	
	
	if (product.getMobil0RoamingCoverage() != null) {
	    billingOffersInfo.add(
		    Information.builder().code(ResponseConstants.MOVIL1_ROAMINGCOVERAGE).value(product.getMobil0RoamingCoverage()).build());
	}
	
	if(!actual) {
		/** Se añade validación extra para MOVIL1_ROAMINGCOVERAGE en lugar de MOVIL2_ROAMINGCOVERAGE - solo LMA **/
    	if (product.getMobil1RoamingCoverage() != null) {
    		if(!commerInfo.getProductType().equals(ProductType.MOBILE.getCodeDesc()))
    	    billingOffersInfo.add(
    		    Information.builder().code(ResponseConstants.MOVIL2_ROAMINGCOVERAGE).value(product.getMobil1RoamingCoverage()).build());
    		else 
    		billingOffersInfo.add(
            	Information.builder().code(ResponseConstants.MOVIL1_ROAMINGCOVERAGE).value(product.getMobil1RoamingCoverage()).build());         		
    	}

	}else {
	    if(commerInfo.getCommercialOpers().size()>2) {
        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
        		    
        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
        		
                		if(actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT)) {
                		    if (product.getMobil1RoamingCoverage() != null) {
                        		    billingOffersInfo.add(
                                		    Information.builder().code(ResponseConstants.MOVIL2_ROAMINGCOVERAGE).value(product.getMobil1RoamingCoverage()).build());
                		    }
                		}
        		
        		    }
        		
        	    }
	    }
	    
	}
	
	if (product.getMobil0GigaPass() != null) {
	    billingOffersInfo.add(
		    Information.builder().code(ResponseConstants.MOVIL1_GIGAPASS).value(product.getMobil0GigaPass().toString()).build());
	}
	
	if(!actual) {
        	if (product.getMobil1GigaPass() != null && !fixedPark.getOnlyOneMobileIndicator()) {
        	    billingOffersInfo.add(
        		    Information.builder().code(ResponseConstants.MOVIL2_GIGAPASS).value(product.getMobil1GigaPass().toString()).build());
        	}
	}else {
	    if(commerInfo.getCommercialOpers().size()>2) {
        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
        		    
        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
        		
                		if(actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT)) {
                		    if (product.getMobil1GigaPass() != null && !fixedPark.getOnlyOneMobileIndicator()) {
                        		    billingOffersInfo.add(
                                		    Information.builder().code(ResponseConstants.MOVIL2_GIGAPASS).value(product.getMobil1GigaPass().toString()).build());
                		    }
                		}
        		
        		    }
        		
        	    }
	    }
	    
	}
	
	if (product.getPackageFixedRent() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.PAQUETE_RENTA_FIJA)
		    .value(product.getPackageFixedRent().toString()).build());
	}

	if (product.getPackageRent() != null) {
	    billingOffersInfo
		    .add(Information.builder().code(ResponseConstants.RENTAPAQUETE).value(product.getPackageRent().toString()).build());
	}
	if (product.getRegularPrice() != null) {
	    billingOffersInfo
		    .add(Information.builder().code(ResponseConstants.PRECIOREGULAR).value(product.getRegularPrice().toString()).build());
	}
	if (product.getRegularPrice() != null && product.getPackageRent() != null) {
	    double discount = product.getRegularPrice() - product.getPackageRent();
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.DESCUENTO).value(String.valueOf(discount)).build());
	}
	if (product.getPackageName() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.NOMBRE).value(product.getPackageName()).build());
	}
	if (product.getPackagePs() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.PS).value(product.getPackagePs().toString()).build());
	}
	if (product.getHomeCostAlone() != null) {
	    billingOffersInfo
		    .add(Information.builder().code(ResponseConstants.COSTOHOGARSOLO).value(product.getHomeCostAlone().toString()).build());
	}
	if (product.getMobil1CostAlone() != null) {
	    if (fixedPark.getOnlyOneMobileIndicator() && product.getMobil2CostAlone() != null) {
		billingOffersInfo.add(Information.builder().code(ResponseConstants.COSTOMOVIL1_SOLO)
			.value(String.valueOf(product.getMobil1CostAlone() + product.getMobil2CostAlone())).build());
	    } else {
		billingOffersInfo.add(Information.builder().code(ResponseConstants.COSTOMOVIL1_SOLO)
			.value(product.getMobil1CostAlone().toString()).build());
	    }
	}
	
	if(!actual) {
	
        	if (product.getMobil2CostAlone() != null && !fixedPark.getOnlyOneMobileIndicator()) {
        	    billingOffersInfo.add(
        		    Information.builder().code(ResponseConstants.COSTOMOVIL2_SOLO).value(product.getMobil2CostAlone().toString()).build());
        	}
	
	}else {
	    if(commerInfo.getCommercialOpers().size()>2) {
        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
        		    
        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
        		
                		if(actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT)) {
                		    if (product.getMobil2CostAlone() != null && !fixedPark.getOnlyOneMobileIndicator()) {
                        		billingOffersInfo.add(
                        		    Information.builder().code(ResponseConstants.COSTOMOVIL2_SOLO).value(product.getMobil2CostAlone().toString()).build());
                		    }
                		}
        		
        		    }
        		
        	    }
	    }
	    
	}
	
	if (product.getMovistarPrix() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVISTAR_PRIX).value(product.getMovistarPrix()).build());
	}
	if (product.getMovistarPlay() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVISTAR_PLAY).value(product.getMovistarPlay()).build());
	}
	if (product.getNetflix() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.NETFLIX).value(product.getNetflix()).build());
	}
	if (product.getDuplicateBonus() != null) {
	    for (Integer position : fixedPark.getCommOperProvidePortaPositions()) {
		
		if(!actual && (position==1)) {
		
		billingOffersInfo.add(Information.builder().code(String.format(ResponseConstants.MOVIL_BONO_DUPLICA, position))
			.value(product.getDuplicateBonus()).build());
		
		}
		
		if(!actual && (position==2)) {
			
			billingOffersInfo.add(Information.builder().code(String.format(ResponseConstants.MOVIL_BONO_DUPLICA, position))
				.value(product.getDuplicateBonus()).build());
			
		}
		
		if(actual && (position==1)) {
			
			billingOffersInfo.add(Information.builder().code(String.format(ResponseConstants.MOVIL_BONO_DUPLICA, position))
				.value(product.getDuplicateBonus()).build());
			
		}	
		
		if(commerInfo.getCommercialOpers().size()>2) {
        		if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
        		    
        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
        		
                		if(actual && (position==2) && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT)) {
                			
                			billingOffersInfo.add(Information.builder().code(String.format(ResponseConstants.MOVIL_BONO_DUPLICA, position))
                				.value(product.getDuplicateBonus()).build());
                			
                		}
        		
        		    }
        		
        		}
		}
	    }
	}
	if (product.getMinutesLine() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.LINEA_MINUTOS).value(product.getMinutesLine()).build());
	}
	if (product.getCableChannels() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.CABLE_CANALES).value(product.getCableChannels()).build());
	}
	/*if (product.getOfferTotalRent() != null) {
	    billingOffersInfo
		    .add(Information.builder().code(ResponseConstants.RENTATOTAL).value(product.getOfferTotalRent().toString()).build());
	}*/
	if (StringUtils.isNotBlank(product.getModemEquipment())) {
	    billingOffersInfo
		    .add(Information.builder().code(ResponseConstants.FIJO_EQUIPAMIENTO_MODEM).value(product.getModemEquipment()).build());
	}
	
	
		
		
		if (product.getOfferTotalRent() != null) {
		    if(product.getCurrentProduct() != null && product.getCurrentProduct().equals(Constant.CURRENT_OFFER_TYPE)) {
			billingOffersInfo
    		    		.add(Information.builder().code(ResponseConstants.RENTATOTAL).value(String.valueOf((product.getOfferTotalRent()))).build());
		    }else {
        		    if(!(product.getEnabled().equals(Constant.TWO))) {
        	        	    
        	        		
        	        		
        	        		String codeUpfront0;
        	        		String valueUpfront0;
        	        		String codeUpfront1;
        	        		String valueUpfront1;
        	       
        	        		//List<Information>commerInfo.AdditionalOperationInformation
        	        	//Percy lo hizo
        	        		 
        	        		boolean existeUp=false;
        	        		
        	        		if(!(commerInfo.getAdditionalOperationInformation() == null)) {
        	        		
                	        		for (Information info : commerInfo.getAdditionalOperationInformation()) {
                	        		    if(info.getCode().equals("upfront") &&
                	        			    info.getValue().equals("1")) {
                	        			existeUp = true;
                	        		    }
        					}
        	        		
        	        		}
        	        		
        	        		 /*codeUpfront0 = commerInfo.getAdditionalOperationInformation().get(0).getCode();
        	        		 valueUpfront0 = commerInfo.getAdditionalOperationInformation().get(0).getCode();
        	        		 codeUpfront1 = commerInfo.getAdditionalOperationInformation().get(1).getCode();
        	        		 valueUpfront1 = commerInfo.getAdditionalOperationInformation().get(1).getCode();*/
        	        		 
        	        		int existe = 0;
        	        		
        	        		//AÃ±adiendo Campos de costo de InstalaciÃ²n y Cuotas de InstalaciÃ²n
        	        			if (product.getInstalacionCosto() != null) {
        	        			    existe++;
        	        			    
        	        			}
        	        			if (product.getInstalacionCuotas() != null) {
        	        			    existe++;
        	        			    
        	        			}
        	        			
        	        		
        	        		if(existe == 2) {
	        	        		if(existeUp) {
	        	        			
	        	        			billingOffersInfo.add(Information.builder().code(ResponseConstants.RENTATOTAL).value(String.valueOf(product.getOfferTotalRent())).build());	   
	        	        			
	        	        		} else {
	        	        		    
	        	        		    	if (product.getInstalacionCosto() != null) {
	        	        			    billingOffersInfo
	        	        				    .add(Information.builder().code(ResponseConstants.INSTALACION_COSTO).value(product.getInstalacionCosto().toString()).build());
	        	        			}
	        	        			if (product.getInstalacionCuotas() != null) {
	        	        			    billingOffersInfo
	        	        				    .add(Information.builder().code(ResponseConstants.INSTALACION_CUOTAS).value(product.getInstalacionCuotas().toString()).build());
	        	        			}
	        	        			billingOffersInfo.add(Information.builder().code(ResponseConstants.RENTATOTAL).value(String.valueOf((product.getOfferTotalRent()+(product.getInstalacionCosto() != null? product.getInstalacionCosto() : 0)))).build());
	        	        		}
        	        		 
        	        		
        	        		}else {
        	        		
                	        		if(movilDevices.size() == 2) {
                	        		    billingOffersInfo
                	        		    .add(Information.builder().code(ResponseConstants.RENTATOTAL).value(String.valueOf((product.getOfferTotalRent()+product.getMobil1Rent()))).build());
                	        		}else {
                	                		billingOffersInfo
                	                		    .add(Information.builder().code(ResponseConstants.RENTATOTAL).value(String.valueOf((product.getOfferTotalRent()))).build());
                	        		}
        	        	    }
        		    }else {
        			billingOffersInfo
        			    .add(Information.builder().code(ResponseConstants.RENTATOTAL).value(String.valueOf((product.getOfferTotalRent()+product.getMobil1Rent()))).build());
        		    }
		    }
		}
	
	if (StringUtils.isNotBlank(product.getDecoEquipment())) {
	    billingOffersInfo
		    .add(Information.builder().code(ResponseConstants.FIJO_EQUIPAMIENTO_DECO).value(product.getDecoEquipment()).build());
	}
	if (product.getRentByAditionals() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.RENTAPORADICIONALES)
		    .value(product.getRentByAditionals().toString()).build());
	}
	if (product.getJump() != null && product.getEmployeeJump() == null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.SALTO).value(product.getJump().toString()).build());
	} else {
	    if (product.getJump() != null) {
		billingOffersInfo
			.add(Information.builder().code(ResponseConstants.SALTO).value(product.getEmployeeJump().toString()).build());
	    }
	    if (product.getEmployeeDiscount() != null) {
		billingOffersInfo.add(Information.builder().code(ResponseConstants.DESC_EMPLEADO_MONTO)
			.value(product.getEmployeeDiscount().toString()).build());
		billingOffersInfo.add(Information.builder().code(ResponseConstants.DESC_EMPLEADO_RENTA_PAQUETE)
			.value(product.getEmployeePackageRent().toString()).build());
	    }
	}
	// reconozca la 2 o 3ra iteracion no muestra
	if (cont.equals(1) && (product.getPriority() != null)) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.PRIORIDAD).value(product.getPriority()).build());
	}
	
	if(product.getCurrentProduct()==null) {
	
        	if (CollectionUtils.isNotEmpty(product.getCharacteristics())) {
        	    for (Information information : product.getCharacteristics()) {
        		//billingOffersInfo.add(Information.builder().code(ResponseConstants.CARACTERISTICA).value(information.getValue()).build());
        		if(information.getCode().equals(rulesValue.getPRODUCT_IND_OFFER())) {
        		    billingOffersInfo.add(Information.builder().code(rulesValue.getPRODUCT_IND_OFFER()).value(information.getValue()).build());
        		}else if(information.getCode().equals(rulesValue.getPRODUCT_MIGRATION_TECHNOLOGY())) {
        		    billingOffersInfo.add(Information.builder().code(rulesValue.getPRODUCT_MIGRATION_TECHNOLOGY()).value(information.getValue()).build());
        		}else{
        		    billingOffersInfo.add(Information.builder().code(ResponseConstants.CARACTERISTICA).value(information.getValue()).build());
        		}
        	    }
        	}
	
	}

	if (CollectionUtils.isNotEmpty(product.getPenaltiesPlankRank())) {
	    billingOffersInfo.addAll(product.getPenaltiesPlankRank());
	}
	if (CollectionUtils.isNotEmpty(product.getComparativeComponents())) {
	    billingOffersInfo.addAll(product.getComparativeComponents());
	}
	if (product.getAditionalRentsToKeep() != null) {
	    billingOffersInfo.addAll(product.getAditionalRentsToKeep());
	}

	if (product.getAditionalRentsToKeepSummatory() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.AD_RENTAADICIONALESMANTENER)
		    .value(product.getAditionalRentsToKeepSummatory().toString()).build());
	}
	if (product.getAditionalBlocksTokeep() != null) {
	    for (Information blocks : product.getAditionalBlocksTokeep()) {
		if (rulesValue.getBLOCK_HD_KEEP().equals(blocks.getCode())) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.AD_HD_BLOQUE).value(blocks.getValue()).build());
		}
		if (rulesValue.getBLOCK_HBO_KEEP().equals(blocks.getCode())) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.AD_HBO_BLOQUE).value(blocks.getValue()).build());
		}
		if (rulesValue.getBLOCK_FOX_KEEP().equals(blocks.getCode())) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.AD_FOX_BLOQUE).value(blocks.getValue()).build());
		}
		if (rulesValue.getBLOCK_EST_KEEP().equals(blocks.getCode())) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.AD_ESTELAR_BLOQUE).value(blocks.getValue()).build());
		}
		if (rulesValue.getBLOCK_GOLDPREM_KEEP().equals(blocks.getCode())) {
		    billingOffersInfo
			    .add(Information.builder().code(ResponseConstants.AD_GOLDPREM_BLOQUE).value(blocks.getValue()).build());
		}
		if (rulesValue.getBLOCK_HTPACK_KEEP().equals(blocks.getCode())) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.AD_HTPACK_BLOQUE).value(blocks.getValue()).build());
		}
	    }
	}
	if (product.getAditionalBlocksTokeepRent() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.AD_RENTABLOQUESADICIONALESMANTENER)
		    .value(product.getAditionalBlocksTokeepRent().toString()).build());
	}
	if (product.getTotalCostAloneProducts() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.COSTOTOTALPRODUCTOSSEPARADOS)
		    .value(product.getTotalCostAloneProducts().toString()).build());
	}
	if (product.getInternetDestinyTechnology() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.INTERNET_TECNOLOGIADESTINO)
		    .value(product.getInternetDestinyTechnology()).build());
	}
	
	//if(!(product.getEnabled().equals(Constant.TWO))) {
	if(product.getPackageType()!=null) {
		if(!(product.getPackageType().equals(Constant.TIPO_PAQUETE_MOVIL))) {
			billingOffersInfo.add(Information.builder().code(ResponseConstants.EQUIPAMIENTO_CANTIDADDECOS).value(
				product.getEquipmentDecoQuantity() == null ? Constant.ZERO.toString() : product.getEquipmentDecoQuantity().toString())
				.build());	
			

			billingOffersInfo.add(Information.builder().code(ResponseConstants.EQUIPAMIENTO_CANTIDADREPETIDORWIFI)
				.value(product.getEquipmentUltraWifiRepQuantity() == null ? Constant.ZERO.toString()
					: product.getEquipmentUltraWifiRepQuantity().toString())
				.build());
			
			}
	}
	
	if(product.getMerchandisingType() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.PARRILLA_PERTENENCIA)
		    .value(product.getMerchandisingType()).build());
	}
	
	
	
        	if(product.getCodeAutomatizer() != null) {
        	    
        	    if(commerInfo.getAdditionalOperationInformation()!=null) {
        	    	List<ParamMovTotal> campaniaOfertas = new ArrayList<ParamMovTotal>();
        	    	campaniaOfertas = paramMovTotalRepo.findByGrupoParam(Constant.DESC_TEMP_CAMP);
        	    
        	    	respValUpFrn = TotalUtil.validationUpfront(commerInfo.getAdditionalOperationInformation(),campaniaOfertas);
        	    	campaignValue = respValUpFrn.getValue();
                	    //if(commerInfo.getAdditionalOperationInformation().get(0).getValue().equals("1")) {
                	    if(campaignValue.equals("1")) {	    
                		    billingOffersInfo
                		         .add(Information.builder().code(ResponseConstants.CODIGO_AUTOMATIZADOR).value(product.getCodeAutomatizer()).build());
                		    /* campos a agregar por cada oferta (setear con los campos de la bd luego de agregar los campos en la bd)*/
                		    billingOffersInfo
                		         .add(Information.builder().code(ResponseConstants.MONTO_DEVOLUCION_UPFRONT).value(String.valueOf(product.getAmountCounted())).build());
                		    billingOffersInfo
                		         .add(Information.builder().code(ResponseConstants.PERIODO_UPFRONT).value(String.valueOf(product.getPeriodByMonth())).build());
                		    billingOffersInfo
                		         .add(Information.builder().code(ResponseConstants.RENTA_PROMOCIONAL_UPFRONT).value(String.valueOf(product.getPromotionalRent())).build());
                		    billingOffersInfo
                		         .add(Information.builder().code(ResponseConstants.DESCUENTO_PROMOCIONAL_UPFRONT).value(product.getDiscount()).build());
                		    
                	    //} else  if(commerInfo.getAdditionalOperationInformation().get(0).getValue().equals("2")) {
                	    } else  if(campaignValue.equals("2")) {
                		billingOffersInfo
            		         .add(Information.builder().code(ResponseConstants.CODIGO_AUTOMATIZADOR).value(product.getCodeAutomatizer()).build());
                	    }
        	    } else {
        		billingOffersInfo
   		         .add(Information.builder().code(ResponseConstants.CODIGO_AUTOMATIZADOR).value(product.getCodeAutomatizer()).build());
        	    }
        	}
	
	

	if (product.getCatalogDiscountTempDTO() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.DESCUENTO_PS)
		    .value(product.getCatalogDiscountTempDTO().getDiscountPS() == null ? Constant.ZERO.toString()
			    : product.getCatalogDiscountTempDTO().getDiscountPS().toString())
		    .build());
	    /*billingOffersInfo.add(Information.builder().code(ResponseConstants.DESCUENTO_NOMBRE)
		    .value(product.getCatalogDiscountTempDTO().getDiscountName() == null ? Constant.ZERO.toString()
			    : product.getCatalogDiscountTempDTO().getDiscountName())
		    .build());*/
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.DESCUENTO_MONTO)
		    .value(product.getCatalogDiscountTempDTO().getDiscountAmount() == null ? Constant.ZERO.toString()
			    : product.getCatalogDiscountTempDTO().getDiscountAmount().toString())
		    .build());
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.DESCUENTO_DURACION)
		    .value(product.getCatalogDiscountTempDTO().getDiscountduration() == null ? Constant.ZERO.toString()
			    : product.getCatalogDiscountTempDTO().getDiscountduration().toString())
		    .build());
//	    billingOffersInfo.add(Information.builder().code(ResponseConstants.DESCUENTO_CAMPANIA)
//		    .value(product.getCatalogDiscountTempDTO().getCampain() == null ? Constant.ZERO.toString()
//			    : product.getCatalogDiscountTempDTO().getCampain())
//		    .build());
	}
	
	//if(!(product.getEnabled().equals(Constant.TWO))) {
	if(product.getPackageType()!=null) {
		if(!product.getPackageType().equals(Constant.TIPO_PAQUETE_MOVIL)) {
			if(product.getCampaign() != null) {
        	    billingOffersInfo
        	         .add(Information.builder().code(ResponseConstants.DESCUENTO_CAMPANIA).value(product.getCampaign()).build());
        	}
		}        	
	
	}

		//}        	
	
	//}

	
	
	if (product.getCatalogPromoTempDTO() != null) {
	    
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.DESCUENTO_NOMBRE)
		    .value(product.getCatalogPromoTempDTO().getDiscountName() == null ? Constant.ZERO.toString()
			    : product.getCatalogPromoTempDTO().getDiscountName())
		    .build());
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.PROMO_PS)
		    .value(product.getCatalogPromoTempDTO().getDiscountPS() == null ? Constant.ZERO.toString()
			    : product.getCatalogPromoTempDTO().getDiscountPS().toString())
		    .build());
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.PROMO_NOMBRE)
		    .value(product.getCatalogPromoTempDTO().getDiscountName() == null ? Constant.ZERO.toString()
			    : product.getCatalogPromoTempDTO().getDiscountName())
		    .build());
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.PROMO_MONTO)
		    .value(product.getCatalogPromoTempDTO().getSpeedAmount() == null ? Constant.ZERO.toString()
			    : product.getCatalogPromoTempDTO().getSpeedAmount().toString())
		    .build());
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.PROMO_DURACION)
		    .value(product.getCatalogPromoTempDTO().getDiscountduration() == null ? Constant.ZERO.toString()
			    : product.getCatalogPromoTempDTO().getDiscountduration().toString())
		    .build());
//	    billingOffersInfo.add(Information.builder().code(ResponseConstants.DESCUENTO_CAMPANIA)
//		    .value(product.getCatalogDiscountTempDTO().getCampain() == null ? Constant.ZERO.toString()
//			    : product.getCatalogDiscountTempDTO().getCampain())
//		    .build());
	}
	
	//if(product.getPackageType()!=null) {
		//if(!product.getPackageType().equals(Constant.TIPO_PAQUETE_MOVIL)) {
			/*if(product.getCampaignPromo() != null) {
    	    billingOffersInfo
    	         .add(Information.builder().code(ResponseConstants.PROMO_CAMPANIA).value(product.getCampaignPromo()).build());
    	}*/

	
	ParamMovTotal paramMov = getUpDown(product);
	
	if(!(paramMov == null)) {
	    
	    billingOffersInfo.add(Information.builder().code("Fijo_InternetUp")
		    .value(paramMov.getCol1())
		    .build());
	    
	    billingOffersInfo.add(Information.builder().code("Fijo_InternetDown")
		    .value(paramMov.getValor())
		    .build());
	    
	}
	
	}
	 
	
	
//	billingOffersInfo.add(Information.builder().code(ResponseConstants.DATOS_MOVIL_ILIM).
//		    value(String.valueOf(TotalUtil.getNumericValueOf(product.getUnlimitedMovil()))).build());
//	
//	billingOffersInfo.add(Information.builder().code(ResponseConstants.CANTIDAD_LINEAS).
//		    value(String.valueOf(TotalUtil.getNumericValueOf(product.getQuantityMobilLines()))).build());
	
    }
    
    private void produtOffersMTResponseLAM(FixedDTO fixedPark, ProductFixedDTO product, List<Information> billingOffersInfo, Integer cont, List<MobileDTO> movilDevices, int oferta, ProductFixedDTO p, CommercialOperationInfo commerInfo) {
	
	    boolean validaUpfront = false;
	    String campaignValue = null;
	    Information respValUpFrn = new Information("","");

	    //Information respValUpFrn = new Information("","");
	    	
	    if (StringUtils.isNotBlank(product.getLineName())) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.FIJO_LINEA).value(product.getLineName()).build());
		}
		if (product.getInternetSpeed() != null) {
		    billingOffersInfo
			    .add(Information.builder().code(ResponseConstants.FIJO_INTERNET).value(product.getInternetSpeed().toString()).build());
		}
		if (StringUtils.isNotBlank(product.getCableType())) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.FIJO_TELEVISION).value(product.getCableType()).build());
		}
		
		
		if (product.getMobil0Minutes() != null) {
		    billingOffersInfo
			    .add(Information.builder().code(ResponseConstants.MOVIL1_MINUTOS).value(product.getMobil0Minutes().toString()).build());
		}
		
		boolean actual = false;
		
		if(product.getCurrentProduct()!=null) {
			
	        	if(product.getCurrentProduct().equals(Constant.CURRENT_OFFER_TYPE)) {
	        	    	billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL1_DATOS)
	    		.value(product.getMobil0QuantityData().toString()).build());
	        	    	actual = true;
	        	}
	    	
		}
		
		if(!actual) {
		
	            if (product.getMobil0QuantityData() != null) {
	                if (fixedPark.getOnlyOneMobileIndicator() && product.getMobileQuantityDataSumatory() != null) {
	            		
	            	//billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL1_DATOS)
	            	//	.value(product.getMobileQuantityDataSumatory().toString()).build());
	            		
	            	billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL1_DATOS)
	            		.value(product.getMobileQuantityDataSumatory().toString()).build());
	            		 
	                } else {
	            	//Ojo: const MOVIL1_DATOS se relaciona a getMobil0QuantityData(), puede inducir a error de llamar getMobil1QuantityData()
	            	billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL1_DATOS)
	            		.value(product.getMobil0QuantityData().toString()).build());
	                }
	            }
	            
		}
		
		
		

		//if(!(product.getEnabled().equals(Constant.TWO))) {
		if(product.getPackageType()!=null) {
			if(!(product.getPackageType().equals(Constant.TIPO_PAQUETE_MOVIL))) {

		if(product.getOfferTotalRent()!=null) {
		    ParamMovTotal pmt = getArcheType(product.getOfferTotalRent());
		   
		    if(pmt!=null) {
		    
	        	    billingOffersInfo.add(Information.builder().code(ResponseConstants.PLAN_OFFER_ARQUETIPO)
	        		    .value(pmt.getValor()).build());
		    
		    }
		}
		/*
		if(product.getInternetTechnology().equals(rulesValue.INTERNET_TECNOLOGIA_FTTH)) {
		    
			billingOffersInfo.add(Information.builder().code(ResponseConstants.PLAN_OFFER_ARQUETIPO)
				    .value(ResponseConstants.ARQUETIPO_PRO).build());
		    
		}else {
		    if (oferta == 1 || oferta == 2) {
			    billingOffersInfo.add(Information.builder().code(ResponseConstants.PLAN_OFFER_ARQUETIPO)
				    .value(ResponseConstants.ARQUETIPO_LITE).build());
			}
			
		    if (oferta == 3 || oferta == 4) {
			    billingOffersInfo.add(Information.builder().code(ResponseConstants.PLAN_OFFER_ARQUETIPO)
				    .value(ResponseConstants.ARQUETIPO_FULL).build());
			}
		}*/
		
		/*if(product.getInternetTechnology().equals(rulesValue.INTERNET_TECNOLOGIA_FTTH)) {
		    
			billingOffersInfo.add(Information.builder().code(ResponseConstants.PAQUETE_BENEFICIO)
				    .value(ResponseConstants.PAQUETE_BENEFICIO_EXPERTO_WIFI).build());
		    
		}*/
		
		if(product.getPackageBenefit()!=null) {
			if(!product.getPackageBenefit().equals("")) {
				billingOffersInfo.add(Information.builder().code(ResponseConstants.PAQUETE_BENEFICIO)
					    .value(product.getPackageBenefit()).build());
				//.value(ResponseConstants.PAQUETE_BENEFICIO_EXPERTO_WIFI).build());
			}
		}

		
		if(!(product.getEnabled().equals(Constant.TWO))) {
		

	        	if(product.getInternetSpeed()  != null && fixedPark.getSpeedInternet() != null) {	    
	        	            	
	                	billingOffersInfo.add(Information.builder().code(ResponseConstants.PF_INTERNET_VELOCIDAD_GAP)
	                		    .value(String.valueOf(product.getInternetSpeed()-fixedPark.getSpeedInternet())).build());
	        		
	        	}else {
	        	    
	        	    int pis = 0, fsi = 0;
	        	    if(product.getInternetSpeed()  == null) {
	        		pis = 0;
	        	    }else {
	        		pis = product.getInternetSpeed();
	        	    }
	        	    
	        	    if(fixedPark.getSpeedInternet()  == null) {
	        		fsi = 0;
	        	    }else {
	        		fsi = fixedPark.getSpeedInternet();
	        	    }
	        	    
	        	    billingOffersInfo.add(Information.builder().code(ResponseConstants.PF_INTERNET_VELOCIDAD_GAP)
	        		    .value(String.valueOf(pis-fsi)).build());
	        	}
		
			}
		}
		
		
		if(product.getCurrentProduct()!=null) {
		
	        	if(product.getCurrentProduct().equals(Constant.CURRENT_OFFER_TYPE)) {
	        	    
	        	    billingOffersInfo.add(Information.builder().code(ResponseConstants.CARACTERISTICA)
	        		    .value(rulesValue.PRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT).build());
	        	}
		
		}
		
		//Se toma el valor del po_id del LMA recibido del ProductFixedDTO,
		//debido a que cada vez que se agrega LMA a alguna oferta se Setea el valor
		if(product.getBillingProductDto().getPoId() != null) {
			if(product.getLma_Po() != null){
				billingOffersInfo.add(Information.builder().code(ResponseConstants.PO_ID)
	        		    .value(product.getLma_Po()).build());	
			}       	
		}
		
		//Se toma el valor del bo_id del LMA recibido del ProductFixedDTO,
		//debido a que cada vez que se agrega LMA a alguna oferta se Setea el valor
		if(product.getBillingProductDto().getBoId() != null) {		
			if(product.getLma_Bo() != null){
				billingOffersInfo.add(Information.builder().code(ResponseConstants.BO_ID)
		    		    .value(product.getLma_Bo()).build());	
			}    	
		}
		
		if (product.getMobil0Apps() != null) {
		    billingOffersInfo
			    .add(Information.builder().code(ResponseConstants.MOVIL1_APPS).value(product.getMobil0Apps().toString()).build());
		}
		
		if(!actual) {
		
	        	if (product.getMobil0Minutes() != null && !fixedPark.getOnlyOneMobileIndicator()) {
	        	    /*billingOffersInfo
	        		    .add(Information.builder().code(ResponseConstants.MOVIL2_MINUTOS).value(product.getMobil0Minutes().toString()).build());*/
	        	    
	        	      billingOffersInfo
	        		    .add(Information.builder().code(ResponseConstants.MOVIL2_MINUTOS).value(String.valueOf(product.getMobil0Minutes()-10)).build());
	        	      
	        	      billingOffersInfo
	        		    .add(Information.builder().code(ResponseConstants.MOVIL2_MINUTOS_OTROS_OPERADORES).value(String.valueOf(10)).build());
	        	}
		
		}else {
		    if(commerInfo.getCommercialOpers().size()>2) {
	        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
	        		    
	        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
	        		
	                		if(actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT)) {
	                		    
	                		    if (product.getMobil0Minutes() != null && !fixedPark.getOnlyOneMobileIndicator()) {
	                			
	                        		    billingOffersInfo
	                        		    .add(Information.builder().code(ResponseConstants.MOVIL2_MINUTOS).value(String.valueOf(product.getMobil0Minutes()-10)).build());
	                        	      
	                        		    billingOffersInfo
	                        		    .add(Information.builder().code(ResponseConstants.MOVIL2_MINUTOS_OTROS_OPERADORES).value(String.valueOf(10)).build());
	                		    }
	                		}
	        		
	        		    }
	        		
	        	    }
		    }
		    
		}
		
		
		if (product.getMobil1QuantityData() != null && !fixedPark.getOnlyOneMobileIndicator()) {
		   /* billingOffersInfo.add(
			    Information.builder().code(ResponseConstants.MOVIL2_DATOS).value(product.getMobil1QuantityData().toString()).build());*/
		    
		    //System.out.println("product.getMobil1QuantityData() ====> " + product.getMobil1QuantityData());
		    //System.out.println("movilDevices.get(1).getMobileQuantityData() ====> " + movilDevices.get(1).getMobileQuantityData());
		    //System.out.println("RESTA: "+(product.getMobil1QuantityData() - movilDevices.get(1).getMobileQuantityData()));
		    
		    if(movilDevices.size() > 1) {
			
			//if(!actual && (commerInfo.getCommercialOpers().get(2).getSubscriber()==null)) {
			if(!actual && (commerInfo.getCommercialOpers().get(2).getSubscriber()==null || commerInfo.getCommercialOpers().get(2).getSubscriber().getBoId()==null)) {
			
	        	    billingOffersInfo.add(
	        		   // Information.builder().code(ResponseConstants.MOVIL2_DATOS).value(String.valueOf(product.getMobil1QuantityData() - movilDevices.get(1).getMobileQuantityData())).build());
	        	    Information.builder().code(ResponseConstants.MOVIL2_DATOS).value(String.valueOf(product.getMobil1QuantityData())).build());
			}
		    }else {
			 billingOffersInfo.add(
			    Information.builder().code(ResponseConstants.MOVIL2_DATOS).value(product.getMobil1QuantityData().toString()).build());
		
		    }
		}
		
		if(p!=null) {
		    	
		    if (product.getMobil1QuantityData() != null && p.getMobil1QuantityData() != null) {
			   
			    
			    
			    
			    billingOffersInfo.add(
				   // Information.builder().code(ResponseConstants.MOVIL2_DATOS).value(String.valueOf(product.getMobil1QuantityData() - movilDevices.get(1).getMobileQuantityData())).build());
			    Information.builder().code(ResponseConstants.PF_DATOS_MOVILES_GAP).value(String.valueOf(product.getMobil0QuantityData() - p.getMobil0QuantityData())).build());
			    
			   
		    }
		    
		}else {
		
	        	if(movilDevices!=null) {
	        		
	                	if (product.getMobil1QuantityData() != null && movilDevices.get(0).getMobileQuantityData() != null) {
	                		   /* billingOffersInfo.add(
	                			    Information.builder().code(ResponseConstants.MOVIL2_DATOS).value(product.getMobil1QuantityData().toString()).build());*/
	                		    
	                		    //System.out.println("product.getMobil1QuantityData() ====> " + product.getMobil1QuantityData());
	                		    //System.out.println("movilDevices.get(1).getMobileQuantityData() ====> " + movilDevices.get(0).getMobileQuantityData());
	                		    //System.out.println("RESTA: "+(product.getMobil1QuantityData() - movilDevices.get(0).getMobileQuantityData()));
	                		    
	                		    
	                		    
	                		    billingOffersInfo.add(
	                			   // Information.builder().code(ResponseConstants.MOVIL2_DATOS).value(String.valueOf(product.getMobil1QuantityData() - movilDevices.get(1).getMobileQuantityData())).build());
	                		    Information.builder().code(ResponseConstants.PF_DATOS_MOVILES_GAP).value(String.valueOf(product.getMobil0QuantityData() - movilDevices.get(0).getMobileQuantityData())).build());
	                		    
	                		   
	                	}
	        	
	        	}
		
		}
		
		if(!actual) {
		
	        	if (product.getMobil0Apps() != null && !fixedPark.getOnlyOneMobileIndicator()) {
	        	    billingOffersInfo
	        		    .add(Information.builder().code(ResponseConstants.MOVIL2_APPS).value(product.getMobil0Apps().toString()).build());
	        	}
		
		}else {
		    if(commerInfo.getCommercialOpers().size()>2) {
	        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
	        		    
	        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
	        		
	                		if(actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT)) {
	                		    if (product.getMobil0Apps() != null && !fixedPark.getOnlyOneMobileIndicator()) {
	                        		    billingOffersInfo
	                        		    .add(Information.builder().code(ResponseConstants.MOVIL2_APPS).value(product.getMobil0Apps().toString()).build());
	                		    }
	                		}
	        		
	        		    }
	        		
	        	    }
		    }
		    
		}
		
		
		if (product.getMobil0Sms() != null) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL1_SMS).value(product.getMobil0Sms()).build());
		}

		if(!actual) {
	        	if (product.getMobil0Sms() != null && !fixedPark.getOnlyOneMobileIndicator()) {
	        	    billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL2_SMS).value(product.getMobil0Sms()).build());
	        	}
		}else {
		    if(commerInfo.getCommercialOpers().size()>2) {
	        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
	        		    
	        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
	        		
	                		if(actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT)) {
	                		    if (product.getMobil0Sms() != null && !fixedPark.getOnlyOneMobileIndicator()) {
	                			billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL2_SMS).value(product.getMobil0Sms()).build());
	                		    }
	                		}
	        		
	        		    }
	        		
	        	    }
		    }
		    
		}
		
		if (product.getMobil0Roaming() != null) {
		    billingOffersInfo
			    .add(Information.builder().code(ResponseConstants.MOVIL1_ROAMING).value(product.getMobil0Roaming().toString()).build());
		}
		
		if(!actual) {
	        	if (product.getMobil1Roaming() != null && !fixedPark.getOnlyOneMobileIndicator()) {
	        	    billingOffersInfo
	        		    .add(Information.builder().code(ResponseConstants.MOVIL2_ROAMING).value(product.getMobil1Roaming().toString()).build());
	        	}
		}else {
		    if(commerInfo.getCommercialOpers().size()>2) {
	        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
	        		    
	        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
	        		
	                		if(actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT)) {
	                		    if (product.getMobil1Roaming() != null && !fixedPark.getOnlyOneMobileIndicator()) {
	                        		    billingOffersInfo
	                        		    .add(Information.builder().code(ResponseConstants.MOVIL2_ROAMING).value(product.getMobil1Roaming().toString()).build());
	                		    }
	                		}
	        		
	        		    }
	        		
	        	    }
		    }
		    
		}
		
		
		if (product.getMobil0WaInt() != null) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL1_WAINT).value(product.getMobil0WaInt()).build());
		}
		
		if(!actual) {
	        	if (product.getMobil0WaInt() != null && !fixedPark.getOnlyOneMobileIndicator()) {
	        	    billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL2_WAINT).value(product.getMobil0WaInt()).build());
	        	}
		}else {
		    if(commerInfo.getCommercialOpers().size()>2) {
	        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
	        		    
	        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
	        		
	                		if(actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT)) {
	                		    if (product.getMobil0WaInt() != null && !fixedPark.getOnlyOneMobileIndicator()) {
	                			billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL2_WAINT).value(product.getMobil0WaInt()).build());
	                		    }
	                		}
	        		
	        		    }
	        		
	        	    }
		    }
		    
		}
		
		
		if (product.getMobil0Rent() != null) {
		    if (fixedPark.getOnlyOneMobileIndicator() && product.getMobil1Rent() != null) {
			billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL1_RENTA)
				.value(String.valueOf(product.getMobil0Rent() + product.getMobil1Rent())).build());
		    } else {
			billingOffersInfo
				.add(Information.builder().code(ResponseConstants.MOVIL1_RENTA).value(product.getMobil0Rent().toString()).build());
		    }
		}
		
		if(!actual) {
	        	if (product.getMobil1Rent() != null && !fixedPark.getOnlyOneMobileIndicator() && movilDevices.size() == 2) {
	        	    billingOffersInfo
	        	    .add(Information.builder().code(ResponseConstants.MOVIL2_RENTA).value(product.getMobil1Rent().toString()).build());
	        	}
		}else {
		    if(commerInfo.getCommercialOpers().size()>2) {
	        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
	        		    
	        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
	        		
	                		if(actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT)) {
	                		    if (product.getMobil1Rent() != null && !fixedPark.getOnlyOneMobileIndicator() && movilDevices.size() == 2) {
	                        		    billingOffersInfo
	                                	    .add(Information.builder().code(ResponseConstants.MOVIL2_RENTA).value(product.getMobil1Rent().toString()).build());
	                		    }
	                		}
	        		
	        		    }
	        		
	        	    }
		    }
		    
		}
		
		
		if (product.getMobil0RoamingCoverage() != null) {
		    billingOffersInfo.add(
			    Information.builder().code(ResponseConstants.MOVIL1_ROAMINGCOVERAGE).value(product.getMobil0RoamingCoverage()).build());
		}
		
		if(!actual) {
			/** Se añade validación extra para MOVIL1_ROAMINGCOVERAGE en lugar de MOVIL2_ROAMINGCOVERAGE - solo LMA **/
	    	if (product.getMobil1RoamingCoverage() != null) {
	    		if(!commerInfo.getProductType().equals(ProductType.MOBILE.getCodeDesc()))
	    	    billingOffersInfo.add(
	    		    Information.builder().code(ResponseConstants.MOVIL2_ROAMINGCOVERAGE).value(product.getMobil1RoamingCoverage()).build());
	    		else 
	    		billingOffersInfo.add(
	            	Information.builder().code(ResponseConstants.MOVIL1_ROAMINGCOVERAGE).value(product.getMobil1RoamingCoverage()).build());         		
	    	}

		}else {
		    if(commerInfo.getCommercialOpers().size()>2) {
	        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
	        		    
	        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
	        		
	                		if(actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT)) {
	                		    if (product.getMobil1RoamingCoverage() != null) {
	                        		    billingOffersInfo.add(
	                                		    Information.builder().code(ResponseConstants.MOVIL2_ROAMINGCOVERAGE).value(product.getMobil1RoamingCoverage()).build());
	                		    }
	                		}
	        		
	        		    }
	        		
	        	    }
		    }
		    
		}
		
		if (product.getMobil0GigaPass() != null) {
		    billingOffersInfo.add(
			    Information.builder().code(ResponseConstants.MOVIL1_GIGAPASS).value(product.getMobil0GigaPass().toString()).build());
		}
		
		if(!actual) {
	        	if (product.getMobil1GigaPass() != null && !fixedPark.getOnlyOneMobileIndicator()) {
	        	    billingOffersInfo.add(
	        		    Information.builder().code(ResponseConstants.MOVIL2_GIGAPASS).value(product.getMobil1GigaPass().toString()).build());
	        	}
		}else {
		    if(commerInfo.getCommercialOpers().size()>2) {
	        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
	        		    
	        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
	        		
	                		if(actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT)) {
	                		    if (product.getMobil1GigaPass() != null && !fixedPark.getOnlyOneMobileIndicator()) {
	                        		    billingOffersInfo.add(
	                                		    Information.builder().code(ResponseConstants.MOVIL2_GIGAPASS).value(product.getMobil1GigaPass().toString()).build());
	                		    }
	                		}
	        		
	        		    }
	        		
	        	    }
		    }
		    
		}
		
		if (product.getPackageFixedRent() != null) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.PAQUETE_RENTA_FIJA)
			    .value(product.getPackageFixedRent().toString()).build());
		}

		if (product.getPackageRent() != null) {
		    billingOffersInfo
			    .add(Information.builder().code(ResponseConstants.RENTAPAQUETE).value(product.getPackageRent().toString()).build());
		}
		if (product.getRegularPrice() != null) {
		    billingOffersInfo
			    .add(Information.builder().code(ResponseConstants.PRECIOREGULAR).value(product.getRegularPrice().toString()).build());
		}
		if (product.getRegularPrice() != null && product.getPackageRent() != null) {
		    double discount = product.getRegularPrice() - product.getPackageRent();
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.DESCUENTO).value(String.valueOf(discount)).build());
		}
		if (product.getPackageName() != null) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.NOMBRE).value(product.getPackageName()).build());
		}
		if (product.getPackagePs() != null) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.PS).value(product.getPackagePs().toString()).build());
		}
		if (product.getHomeCostAlone() != null) {
		    billingOffersInfo
			    .add(Information.builder().code(ResponseConstants.COSTOHOGARSOLO).value(product.getHomeCostAlone().toString()).build());
		}
		if (product.getMobil1CostAlone() != null) {
		    if (fixedPark.getOnlyOneMobileIndicator() && product.getMobil2CostAlone() != null) {
			billingOffersInfo.add(Information.builder().code(ResponseConstants.COSTOMOVIL1_SOLO)
				.value(String.valueOf(product.getMobil1CostAlone() + product.getMobil2CostAlone())).build());
		    } else {
			billingOffersInfo.add(Information.builder().code(ResponseConstants.COSTOMOVIL1_SOLO)
				.value(product.getMobil1CostAlone().toString()).build());
		    }
		}
		
		if(!actual) {
		
	        	if (product.getMobil2CostAlone() != null && !fixedPark.getOnlyOneMobileIndicator()) {
	        	    billingOffersInfo.add(
	        		    Information.builder().code(ResponseConstants.COSTOMOVIL2_SOLO).value(product.getMobil2CostAlone().toString()).build());
	        	}
		
		}else {
		    if(commerInfo.getCommercialOpers().size()>2) {
	        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
	        		    
	        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
	        		
	                		if(actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT)) {
	                		    if (product.getMobil2CostAlone() != null && !fixedPark.getOnlyOneMobileIndicator()) {
	                        		billingOffersInfo.add(
	                        		    Information.builder().code(ResponseConstants.COSTOMOVIL2_SOLO).value(product.getMobil2CostAlone().toString()).build());
	                		    }
	                		}
	        		
	        		    }
	        		
	        	    }
		    }
		    
		}
		
		if (product.getMovistarPrix() != null) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVISTAR_PRIX).value(product.getMovistarPrix()).build());
		}
		if (product.getMovistarPlay() != null) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVISTAR_PLAY).value(product.getMovistarPlay()).build());
		}
		if (product.getNetflix() != null) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.NETFLIX).value(product.getNetflix()).build());
		}
		if (product.getDuplicateBonus() != null) {
		    for (Integer position : fixedPark.getCommOperProvidePortaPositions()) {
			
			if(!actual && (position==1)) {
			
			billingOffersInfo.add(Information.builder().code(String.format(ResponseConstants.MOVIL_BONO_DUPLICA, position))
				.value(product.getDuplicateBonus()).build());
			
			}
			
			if(!actual && (position==2)) {
				
				billingOffersInfo.add(Information.builder().code(String.format(ResponseConstants.MOVIL_BONO_DUPLICA, position))
					.value(product.getDuplicateBonus()).build());
				
			}
			
			if(actual && (position==1)) {
				
				billingOffersInfo.add(Information.builder().code(String.format(ResponseConstants.MOVIL_BONO_DUPLICA, position))
					.value(product.getDuplicateBonus()).build());
				
			}	
			
			if(commerInfo.getCommercialOpers().size()>2) {
	        		if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
	        		    
	        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
	        		
	                		if(actual && (position==2) && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT)) {
	                			
	                			billingOffersInfo.add(Information.builder().code(String.format(ResponseConstants.MOVIL_BONO_DUPLICA, position))
	                				.value(product.getDuplicateBonus()).build());
	                			
	                		}
	        		
	        		    }
	        		
	        		}
			}
		    }
		}
		if (product.getMinutesLine() != null) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.LINEA_MINUTOS).value(product.getMinutesLine()).build());
		}
		if (product.getCableChannels() != null) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.CABLE_CANALES).value(product.getCableChannels()).build());
		}
		/*if (product.getOfferTotalRent() != null) {
		    billingOffersInfo
			    .add(Information.builder().code(ResponseConstants.RENTATOTAL).value(product.getOfferTotalRent().toString()).build());
		}*/
		if (StringUtils.isNotBlank(product.getModemEquipment())) {
		    billingOffersInfo
			    .add(Information.builder().code(ResponseConstants.FIJO_EQUIPAMIENTO_MODEM).value(product.getModemEquipment()).build());
		}
		
		
			
			
			if (product.getOfferTotalRent() != null) {
			    if(product.getCurrentProduct() != null && product.getCurrentProduct().equals(Constant.CURRENT_OFFER_TYPE)) {
				billingOffersInfo
	    		    		.add(Information.builder().code(ResponseConstants.RENTATOTAL).value(String.valueOf((product.getOfferTotalRent()))).build());
			    }else {
	        		    if(!(product.getEnabled().equals(Constant.TWO))) {
	        	        	    
	        	        		
	        	        		
	        	        		String codeUpfront0;
	        	        		String valueUpfront0;
	        	        		String codeUpfront1;
	        	        		String valueUpfront1;
	        	       
	        	        		//List<Information>commerInfo.AdditionalOperationInformation
	        	        	//Percy lo hizo
	        	        		 
	        	        		boolean existeUp=false;
	        	        		
	        	        		if(!(commerInfo.getAdditionalOperationInformation() == null)) {
	        	        		
	                	        		for (Information info : commerInfo.getAdditionalOperationInformation()) {
	                	        		    if(info.getCode().equals("upfront") &&
	                	        			    info.getValue().equals("1")) {
	                	        			existeUp = true;
	                	        		    }
	        					}
	        	        		
	        	        		}
	        	        		
	        	        		 /*codeUpfront0 = commerInfo.getAdditionalOperationInformation().get(0).getCode();
	        	        		 valueUpfront0 = commerInfo.getAdditionalOperationInformation().get(0).getCode();
	        	        		 codeUpfront1 = commerInfo.getAdditionalOperationInformation().get(1).getCode();
	        	        		 valueUpfront1 = commerInfo.getAdditionalOperationInformation().get(1).getCode();*/
	        	        		 
	        	        		int existe = 0;
	        	        		
	        	        		//AÃ±adiendo Campos de costo de InstalaciÃ²n y Cuotas de InstalaciÃ²n
	        	        			if (product.getInstalacionCosto() != null) {
	        	        			    existe++;
	        	        			    
	        	        			}
	        	        			if (product.getInstalacionCuotas() != null) {
	        	        			    existe++;
	        	        			    
	        	        			}
	        	        			
	        	        		
	        	        		if(existe == 2) {
		        	        		if(existeUp) {
		        	        			
		        	        			billingOffersInfo.add(Information.builder().code(ResponseConstants.RENTATOTAL).value(String.valueOf(product.getOfferTotalRent())).build());	   
		        	        			
		        	        		} else {
		        	        		    
		        	        		    	if (product.getInstalacionCosto() != null) {
		        	        			    billingOffersInfo
		        	        				    .add(Information.builder().code(ResponseConstants.INSTALACION_COSTO).value(product.getInstalacionCosto().toString()).build());
		        	        			}
		        	        			if (product.getInstalacionCuotas() != null) {
		        	        			    billingOffersInfo
		        	        				    .add(Information.builder().code(ResponseConstants.INSTALACION_CUOTAS).value(product.getInstalacionCuotas().toString()).build());
		        	        			}
		        	        			billingOffersInfo.add(Information.builder().code(ResponseConstants.RENTATOTAL).value(String.valueOf((product.getOfferTotalRent()+(product.getInstalacionCosto() != null? product.getInstalacionCosto() : 0)))).build());
		        	        		}
	        	        		 
	        	        		
	        	        		}else {
	        	        		
	                	        		if(movilDevices.size() == 2) {
	                	        		    billingOffersInfo
	                	        		    .add(Information.builder().code(ResponseConstants.RENTATOTAL).value(String.valueOf((product.getOfferTotalRent()+product.getMobil1Rent()))).build());
	                	        		}else {
	                	                		billingOffersInfo
	                	                		    .add(Information.builder().code(ResponseConstants.RENTATOTAL).value(String.valueOf((product.getOfferTotalRent()))).build());
	                	        		}
	        	        	    }
	        		    }else {
	        			billingOffersInfo
	        			    .add(Information.builder().code(ResponseConstants.RENTATOTAL).value(String.valueOf((product.getOfferTotalRent()+product.getMobil1Rent()))).build());
	        		    }
			    }
			}
		
		if (StringUtils.isNotBlank(product.getDecoEquipment())) {
		    billingOffersInfo
			    .add(Information.builder().code(ResponseConstants.FIJO_EQUIPAMIENTO_DECO).value(product.getDecoEquipment()).build());
		}
		if (product.getRentByAditionals() != null) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.RENTAPORADICIONALES)
			    .value(product.getRentByAditionals().toString()).build());
		}
		if (product.getJump() != null && product.getEmployeeJump() == null) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.SALTO).value(product.getJump().toString()).build());
		} else {
		    if (product.getJump() != null) {
			billingOffersInfo
				.add(Information.builder().code(ResponseConstants.SALTO).value(product.getEmployeeJump().toString()).build());
		    }
		    if (product.getEmployeeDiscount() != null) {
			billingOffersInfo.add(Information.builder().code(ResponseConstants.DESC_EMPLEADO_MONTO)
				.value(product.getEmployeeDiscount().toString()).build());
			billingOffersInfo.add(Information.builder().code(ResponseConstants.DESC_EMPLEADO_RENTA_PAQUETE)
				.value(product.getEmployeePackageRent().toString()).build());
		    }
		}
		// reconozca la 2 o 3ra iteracion no muestra
		if (cont.equals(1) && (product.getPriority() != null)) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.PRIORIDAD).value(product.getPriority()).build());
		}
		
		if(product.getCurrentProduct()==null) {
		
	        	if (CollectionUtils.isNotEmpty(product.getCharacteristics())) {
	        	    for (Information information : product.getCharacteristics()) {
	        		//billingOffersInfo.add(Information.builder().code(ResponseConstants.CARACTERISTICA).value(information.getValue()).build());
	        		if(information.getCode().equals(rulesValue.getPRODUCT_IND_OFFER())) {
	        		    billingOffersInfo.add(Information.builder().code(rulesValue.getPRODUCT_IND_OFFER()).value(information.getValue()).build());
	        		}else if(information.getCode().equals(rulesValue.getPRODUCT_MIGRATION_TECHNOLOGY())) {
	        		    billingOffersInfo.add(Information.builder().code(rulesValue.getPRODUCT_MIGRATION_TECHNOLOGY()).value(information.getValue()).build());
	        		}else{
	        		    billingOffersInfo.add(Information.builder().code(ResponseConstants.CARACTERISTICA).value(information.getValue()).build());
	        		}
	        	    }
	        	}
		
		}

		if (CollectionUtils.isNotEmpty(product.getPenaltiesPlankRank())) {
		    billingOffersInfo.addAll(product.getPenaltiesPlankRank());
		}
		if (CollectionUtils.isNotEmpty(product.getComparativeComponents())) {
		    billingOffersInfo.addAll(product.getComparativeComponents());
		}
		if (product.getAditionalRentsToKeep() != null) {
		    billingOffersInfo.addAll(product.getAditionalRentsToKeep());
		}

		if (product.getAditionalRentsToKeepSummatory() != null) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.AD_RENTAADICIONALESMANTENER)
			    .value(product.getAditionalRentsToKeepSummatory().toString()).build());
		}
		if (product.getAditionalBlocksTokeep() != null) {
		    for (Information blocks : product.getAditionalBlocksTokeep()) {
			if (rulesValue.getBLOCK_HD_KEEP().equals(blocks.getCode())) {
			    billingOffersInfo.add(Information.builder().code(ResponseConstants.AD_HD_BLOQUE).value(blocks.getValue()).build());
			}
			if (rulesValue.getBLOCK_HBO_KEEP().equals(blocks.getCode())) {
			    billingOffersInfo.add(Information.builder().code(ResponseConstants.AD_HBO_BLOQUE).value(blocks.getValue()).build());
			}
			if (rulesValue.getBLOCK_FOX_KEEP().equals(blocks.getCode())) {
			    billingOffersInfo.add(Information.builder().code(ResponseConstants.AD_FOX_BLOQUE).value(blocks.getValue()).build());
			}
			if (rulesValue.getBLOCK_EST_KEEP().equals(blocks.getCode())) {
			    billingOffersInfo.add(Information.builder().code(ResponseConstants.AD_ESTELAR_BLOQUE).value(blocks.getValue()).build());
			}
			if (rulesValue.getBLOCK_GOLDPREM_KEEP().equals(blocks.getCode())) {
			    billingOffersInfo
				    .add(Information.builder().code(ResponseConstants.AD_GOLDPREM_BLOQUE).value(blocks.getValue()).build());
			}
			if (rulesValue.getBLOCK_HTPACK_KEEP().equals(blocks.getCode())) {
			    billingOffersInfo.add(Information.builder().code(ResponseConstants.AD_HTPACK_BLOQUE).value(blocks.getValue()).build());
			}
		    }
		}
		if (product.getAditionalBlocksTokeepRent() != null) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.AD_RENTABLOQUESADICIONALESMANTENER)
			    .value(product.getAditionalBlocksTokeepRent().toString()).build());
		}
		if (product.getTotalCostAloneProducts() != null) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.COSTOTOTALPRODUCTOSSEPARADOS)
			    .value(product.getTotalCostAloneProducts().toString()).build());
		}
		if (product.getInternetDestinyTechnology() != null) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.INTERNET_TECNOLOGIADESTINO)
			    .value(product.getInternetDestinyTechnology()).build());
		}
		
		//if(!(product.getEnabled().equals(Constant.TWO))) {
		if(product.getPackageType()!=null) {
			if(!(product.getPackageType().equals(Constant.TIPO_PAQUETE_MOVIL))) {
				billingOffersInfo.add(Information.builder().code(ResponseConstants.EQUIPAMIENTO_CANTIDADDECOS).value(
					product.getEquipmentDecoQuantity() == null ? Constant.ZERO.toString() : product.getEquipmentDecoQuantity().toString())
					.build());	
				

				billingOffersInfo.add(Information.builder().code(ResponseConstants.EQUIPAMIENTO_CANTIDADREPETIDORWIFI)
					.value(product.getEquipmentUltraWifiRepQuantity() == null ? Constant.ZERO.toString()
						: product.getEquipmentUltraWifiRepQuantity().toString())
					.build());
				
				}
		}
		
		if(product.getMerchandisingType() != null) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.PARRILLA_PERTENENCIA)
			    .value(product.getMerchandisingType()).build());
		}
		
		
		
	        	if(product.getCodeAutomatizer() != null) {
	        	    
	        	    if(commerInfo.getAdditionalOperationInformation()!=null) {
	        	    	List<ParamMovTotal> campaniaOfertas = new ArrayList<ParamMovTotal>();
	        	    	campaniaOfertas = paramMovTotalRepo.findByGrupoParam(Constant.DESC_TEMP_CAMP);
	        	    
	        	    	respValUpFrn = TotalUtil.validationUpfront(commerInfo.getAdditionalOperationInformation(),campaniaOfertas);
	        	    	campaignValue = respValUpFrn.getValue();
	                	    //if(commerInfo.getAdditionalOperationInformation().get(0).getValue().equals("1")) {
	                	    if(campaignValue.equals("1")) {	    
	                		    billingOffersInfo
	                		         .add(Information.builder().code(ResponseConstants.CODIGO_AUTOMATIZADOR).value(product.getCodeAutomatizer()).build());
	                		    /* campos a agregar por cada oferta (setear con los campos de la bd luego de agregar los campos en la bd)*/
	                		    billingOffersInfo
	                		         .add(Information.builder().code(ResponseConstants.MONTO_DEVOLUCION_UPFRONT).value(String.valueOf(product.getAmountCounted())).build());
	                		    billingOffersInfo
	                		         .add(Information.builder().code(ResponseConstants.PERIODO_UPFRONT).value(String.valueOf(product.getPeriodByMonth())).build());
	                		    billingOffersInfo
	                		         .add(Information.builder().code(ResponseConstants.RENTA_PROMOCIONAL_UPFRONT).value(String.valueOf(product.getPromotionalRent())).build());
	                		    billingOffersInfo
	                		         .add(Information.builder().code(ResponseConstants.DESCUENTO_PROMOCIONAL_UPFRONT).value(product.getDiscount()).build());
	                		    
	                	    //} else  if(commerInfo.getAdditionalOperationInformation().get(0).getValue().equals("2")) {
	                	    } else  if(campaignValue.equals("2")) {
	                		billingOffersInfo
	            		         .add(Information.builder().code(ResponseConstants.CODIGO_AUTOMATIZADOR).value(product.getCodeAutomatizer()).build());
	                	    }
	        	    } else {
	        		billingOffersInfo
	   		         .add(Information.builder().code(ResponseConstants.CODIGO_AUTOMATIZADOR).value(product.getCodeAutomatizer()).build());
	        	    }
	        	}
		
		

		if (product.getCatalogDiscountTempDTO() != null) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.DESCUENTO_PS)
			    .value(product.getCatalogDiscountTempDTO().getDiscountPS() == null ? Constant.ZERO.toString()
				    : product.getCatalogDiscountTempDTO().getDiscountPS().toString())
			    .build());
		    /*billingOffersInfo.add(Information.builder().code(ResponseConstants.DESCUENTO_NOMBRE)
			    .value(product.getCatalogDiscountTempDTO().getDiscountName() == null ? Constant.ZERO.toString()
				    : product.getCatalogDiscountTempDTO().getDiscountName())
			    .build());*/
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.DESCUENTO_MONTO)
			    .value(product.getCatalogDiscountTempDTO().getDiscountAmount() == null ? Constant.ZERO.toString()
				    : product.getCatalogDiscountTempDTO().getDiscountAmount().toString())
			    .build());
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.DESCUENTO_DURACION)
			    .value(product.getCatalogDiscountTempDTO().getDiscountduration() == null ? Constant.ZERO.toString()
				    : product.getCatalogDiscountTempDTO().getDiscountduration().toString())
			    .build());
//		    billingOffersInfo.add(Information.builder().code(ResponseConstants.DESCUENTO_CAMPANIA)
//			    .value(product.getCatalogDiscountTempDTO().getCampain() == null ? Constant.ZERO.toString()
//				    : product.getCatalogDiscountTempDTO().getCampain())
//			    .build());
		}
		
		//if(!(product.getEnabled().equals(Constant.TWO))) {
		if(product.getPackageType()!=null) {
			if(!product.getPackageType().equals(Constant.TIPO_PAQUETE_MOVIL)) {
				if(product.getCampaign() != null) {
	        	    billingOffersInfo
	        	         .add(Information.builder().code(ResponseConstants.DESCUENTO_CAMPANIA).value(product.getCampaign()).build());
	        	}
			}        	
		
		}

			//}        	
		
		//}

		
		
		if (product.getCatalogPromoTempDTO() != null) {
		    
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.DESCUENTO_NOMBRE)
			    .value(product.getCatalogPromoTempDTO().getDiscountName() == null ? Constant.ZERO.toString()
				    : product.getCatalogPromoTempDTO().getDiscountName())
			    .build());
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.PROMO_PS)
			    .value(product.getCatalogPromoTempDTO().getDiscountPS() == null ? Constant.ZERO.toString()
				    : product.getCatalogPromoTempDTO().getDiscountPS().toString())
			    .build());
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.PROMO_NOMBRE)
			    .value(product.getCatalogPromoTempDTO().getDiscountName() == null ? Constant.ZERO.toString()
				    : product.getCatalogPromoTempDTO().getDiscountName())
			    .build());
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.PROMO_MONTO)
			    .value(product.getCatalogPromoTempDTO().getSpeedAmount() == null ? Constant.ZERO.toString()
				    : product.getCatalogPromoTempDTO().getSpeedAmount().toString())
			    .build());
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.PROMO_DURACION)
			    .value(product.getCatalogPromoTempDTO().getDiscountduration() == null ? Constant.ZERO.toString()
				    : product.getCatalogPromoTempDTO().getDiscountduration().toString())
			    .build());
//		    billingOffersInfo.add(Information.builder().code(ResponseConstants.DESCUENTO_CAMPANIA)
//			    .value(product.getCatalogDiscountTempDTO().getCampain() == null ? Constant.ZERO.toString()
//				    : product.getCatalogDiscountTempDTO().getCampain())
//			    .build());
		}
		
		//if(product.getPackageType()!=null) {
			//if(!product.getPackageType().equals(Constant.TIPO_PAQUETE_MOVIL)) {
				/*if(product.getCampaignPromo() != null) {
	    	    billingOffersInfo
	    	         .add(Information.builder().code(ResponseConstants.PROMO_CAMPANIA).value(product.getCampaignPromo()).build());
	    	}*/

		
		/*ParamMovTotal paramMov = getUpDown(product);
		
		if(!(paramMov == null)) {
		    
		    billingOffersInfo.add(Information.builder().code("Fijo_InternetUp")
			    .value(paramMov.getCol1())
			    .build());
		    
		    billingOffersInfo.add(Information.builder().code("Fijo_InternetDown")
			    .value(paramMov.getValor())
			    .build());
		    
		}*/
		
		}
		 
		
		
//		billingOffersInfo.add(Information.builder().code(ResponseConstants.DATOS_MOVIL_ILIM).
//			    value(String.valueOf(TotalUtil.getNumericValueOf(product.getUnlimitedMovil()))).build());
	//	
//		billingOffersInfo.add(Information.builder().code(ResponseConstants.CANTIDAD_LINEAS).
//			    value(String.valueOf(TotalUtil.getNumericValueOf(product.getQuantityMobilLines()))).build());
		
	    }
    
    private void produtOffersMTResponseMT(FixedDTO fixedPark, ProductFixedDTO product, List<Information> billingOffersInfo, Integer cont, List<MobileDTO> movilDevices, int oferta, ProductFixedDTO p, CommercialOperationInfo commerInfo, int cantidad, ParamMovTotal pmtA, String modemEquipmentOfferCopy, boolean existeMigracion) {
	if (StringUtils.isNotBlank(product.getLineName())) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.FIJO_LINEA).value(product.getLineName()).build());
	}
	if (product.getInternetSpeed() != null) {
	    billingOffersInfo
		    .add(Information.builder().code(ResponseConstants.FIJO_INTERNET).value(product.getInternetSpeed().toString()).build());
	}
	if (StringUtils.isNotBlank(product.getCableType())) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.FIJO_TELEVISION).value(product.getCableType()).build());
	}
	
	
	if (product.getMobil0Minutes() != null) {
	    billingOffersInfo
		    .add(Information.builder().code(ResponseConstants.MOVIL1_MINUTOS).value(product.getMobil0Minutes().toString()).build());
	}
	
	boolean actual = false;
	
	if(product.getCurrentProduct()!=null) {
		
        	if(product.getCurrentProduct().equals(Constant.CURRENT_OFFER_TYPE)) {
        	    	billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL1_DATOS)
    		.value(product.getMobil0QuantityData().toString()).build());
        	    	actual = true;
        	}
    	
	}
	
	if(!actual) {
	
            if (product.getMobil0QuantityData() != null) {
                if (fixedPark.getOnlyOneMobileIndicator() && product.getMobileQuantityDataSumatory() != null) {
            		
            	//billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL1_DATOS)
            	//	.value(product.getMobileQuantityDataSumatory().toString()).build());
            		
            	billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL1_DATOS)
            		.value(product.getMobileQuantityDataSumatory().toString()).build());
            		 
                } else {
            	//Ojo: const MOVIL1_DATOS se relaciona a getMobil0QuantityData(), puede inducir a error de llamar getMobil1QuantityData()
            	billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL1_DATOS)
            		.value(product.getMobil0QuantityData().toString()).build());
                }
            }
            
	}
	
	
	

	//if(!(product.getEnabled().equals(Constant.TWO))) {
	if(product.getPackageType()!=null) {
		if(!(product.getPackageType().equals(Constant.TIPO_PAQUETE_MOVIL))) {

	if(product.getOfferTotalRent()!=null) {
	    ParamMovTotal pmt = getArcheType(product.getOfferTotalRent());
	   
	    if(pmt!=null) {
	    
        	    billingOffersInfo.add(Information.builder().code(ResponseConstants.PLAN_OFFER_ARQUETIPO)
        		    .value(pmt.getValor()).build());
	    
	    }
	}
	/*
	if(product.getInternetTechnology().equals(rulesValue.INTERNET_TECNOLOGIA_FTTH)) {
	    
		billingOffersInfo.add(Information.builder().code(ResponseConstants.PLAN_OFFER_ARQUETIPO)
			    .value(ResponseConstants.ARQUETIPO_PRO).build());
	    
	}else {
	    if (oferta == 1 || oferta == 2) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.PLAN_OFFER_ARQUETIPO)
			    .value(ResponseConstants.ARQUETIPO_LITE).build());
		}
		
	    if (oferta == 3 || oferta == 4) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.PLAN_OFFER_ARQUETIPO)
			    .value(ResponseConstants.ARQUETIPO_FULL).build());
		}
	}*/
	
	/*if(product.getInternetTechnology().equals(rulesValue.INTERNET_TECNOLOGIA_FTTH)) {
	    
		billingOffersInfo.add(Information.builder().code(ResponseConstants.PAQUETE_BENEFICIO)
			    .value(ResponseConstants.PAQUETE_BENEFICIO_EXPERTO_WIFI).build());
	    
	}*/
	
	if(product.getPackageBenefit()!=null) {
		if(!product.getPackageBenefit().equals("")) {
			billingOffersInfo.add(Information.builder().code(ResponseConstants.PAQUETE_BENEFICIO)
				    .value(product.getPackageBenefit()).build());
			//.value(ResponseConstants.PAQUETE_BENEFICIO_EXPERTO_WIFI).build());
		}
	}

	
	if(!(product.getEnabled().equals(Constant.TWO))) {
	

        	if(product.getInternetSpeed()  != null && fixedPark.getSpeedInternet() != null) {	    
        	            	
                	billingOffersInfo.add(Information.builder().code(ResponseConstants.PF_INTERNET_VELOCIDAD_GAP)
                		    .value(String.valueOf(product.getInternetSpeed()-fixedPark.getSpeedInternet())).build());
        		
        	}else {
        	    
        	    int pis = 0, fsi = 0;
        	    if(product.getInternetSpeed()  == null) {
        		pis = 0;
        	    }else {
        		pis = product.getInternetSpeed();
        	    }
        	    
        	    if(fixedPark.getSpeedInternet()  == null) {
        		fsi = 0;
        	    }else {
        		fsi = fixedPark.getSpeedInternet();
        	    }
        	    
        	    billingOffersInfo.add(Information.builder().code(ResponseConstants.PF_INTERNET_VELOCIDAD_GAP)
        		    .value(String.valueOf(pis-fsi)).build());
        	}
	
		}
	}
	
	
	if(product.getCurrentProduct()!=null) {
	    	//if(oferta<7) {
	    	if(oferta<cantidad) {
                	if(product.getCurrentProduct().equals(Constant.CURRENT_OFFER_TYPE)) {
                	    
                	    billingOffersInfo.add(Information.builder().code(ResponseConstants.CARACTERISTICA)
                		    .value(rulesValue.PRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT).build());
                	}
	    	}
	
	}
	
	//Se toma el valor del po_id del LMA recibido del ProductFixedDTO,
	//debido a que cada vez que se agrega LMA a alguna oferta se Setea el valor
	if(product.getBillingProductDto().getPoId() != null) {
		if(product.getLma_Po() != null){
			billingOffersInfo.add(Information.builder().code(ResponseConstants.PO_ID)
        		    .value(product.getLma_Po()).build());	
		}       	
	}
	
	//Se toma el valor del bo_id del LMA recibido del ProductFixedDTO,
	//debido a que cada vez que se agrega LMA a alguna oferta se Setea el valor
	if(product.getBillingProductDto().getBoId() != null) {		
		if(product.getLma_Bo() != null){
			billingOffersInfo.add(Information.builder().code(ResponseConstants.BO_ID)
	    		    .value(product.getLma_Bo()).build());	
		}    	
	}
	
	if (product.getMobil0Apps() != null) {
	    billingOffersInfo
		    .add(Information.builder().code(ResponseConstants.MOVIL1_APPS).value(product.getMobil0Apps().toString()).build());
	}
	
	if(!actual) {
	
        	if (product.getMobil0Minutes() != null && !fixedPark.getOnlyOneMobileIndicator()) {
        	    /*billingOffersInfo
        		    .add(Information.builder().code(ResponseConstants.MOVIL2_MINUTOS).value(product.getMobil0Minutes().toString()).build());*/
        	    
        	      billingOffersInfo
        		    .add(Information.builder().code(ResponseConstants.MOVIL2_MINUTOS).value(String.valueOf(product.getMobil0Minutes()-10)).build());
        	      
        	      billingOffersInfo
        		    .add(Information.builder().code(ResponseConstants.MOVIL2_MINUTOS_OTROS_OPERADORES).value(String.valueOf(10)).build());
        	}
	
	}else {
	    if(commerInfo.getCommercialOpers().size()>2) {
        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
        		    
        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
        		
                		if((actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT))||(actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.CAEQ.getCodeDesc()))||(actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.PORTABILITY.getCodeDesc()))) {
                		    
                		    if (product.getMobil0Minutes() != null && !fixedPark.getOnlyOneMobileIndicator()) {
                			
                        		    billingOffersInfo
                        		    .add(Information.builder().code(ResponseConstants.MOVIL2_MINUTOS).value(String.valueOf(product.getMobil0Minutes()-10)).build());
                        	      
                        		    billingOffersInfo
                        		    .add(Information.builder().code(ResponseConstants.MOVIL2_MINUTOS_OTROS_OPERADORES).value(String.valueOf(10)).build());
                		    }
                		}
        		
        		    }
        		
        	    }else {
        		if(commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.ACTIVATION.getCodeDesc())) {
        		    billingOffersInfo
        		    .add(Information.builder().code(ResponseConstants.MOVIL2_MINUTOS).value(String.valueOf(product.getMobil0Minutes()-10)).build());
        	      
        		    billingOffersInfo
        		    .add(Information.builder().code(ResponseConstants.MOVIL2_MINUTOS_OTROS_OPERADORES).value(String.valueOf(10)).build());
        		}
        	    }
	    }
	    
	}
	
	
	if (product.getMobil1QuantityData() != null && !fixedPark.getOnlyOneMobileIndicator()) {
	   /* billingOffersInfo.add(
		    Information.builder().code(ResponseConstants.MOVIL2_DATOS).value(product.getMobil1QuantityData().toString()).build());*/
	    
	    //System.out.println("product.getMobil1QuantityData() ====> " + product.getMobil1QuantityData());
	    //System.out.println("movilDevices.get(1).getMobileQuantityData() ====> " + movilDevices.get(1).getMobileQuantityData());
	    //System.out.println("RESTA: "+(product.getMobil1QuantityData() - movilDevices.get(1).getMobileQuantityData()));
	    
	    if(movilDevices.size() > 1) {
		
		//if(!actual && (commerInfo.getCommercialOpers().get(2).getSubscriber()==null)) {
		if(!actual && (commerInfo.getCommercialOpers().get(2).getSubscriber()==null || commerInfo.getCommercialOpers().get(2).getSubscriber().getBoId()==null)) {
		
        	    billingOffersInfo.add(
        		   // Information.builder().code(ResponseConstants.MOVIL2_DATOS).value(String.valueOf(product.getMobil1QuantityData() - movilDevices.get(1).getMobileQuantityData())).build());
        	    Information.builder().code(ResponseConstants.MOVIL2_DATOS).value(String.valueOf(product.getMobil1QuantityData())).build());
		}else if((actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.CAEQ.getCodeDesc()))||
			(actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.PORTABILITY.getCodeDesc())||
			(actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.ACTIVATION.getCodeDesc())))) {
		    billingOffersInfo.add(
	        		   // Information.builder().code(ResponseConstants.MOVIL2_DATOS).value(String.valueOf(product.getMobil1QuantityData() - movilDevices.get(1).getMobileQuantityData())).build());
	        	    Information.builder().code(ResponseConstants.MOVIL2_DATOS).value(String.valueOf(product.getMobil1QuantityData())).build());
		}
	    }else {
		 billingOffersInfo.add(
		    Information.builder().code(ResponseConstants.MOVIL2_DATOS).value(product.getMobil1QuantityData().toString()).build());
	
	    }
	}
	
	if(p!=null) {
	    	
	    if (product.getMobil1QuantityData() != null && p.getMobil1QuantityData() != null) {
		   
		    
		    
		    
		    billingOffersInfo.add(
			   // Information.builder().code(ResponseConstants.MOVIL2_DATOS).value(String.valueOf(product.getMobil1QuantityData() - movilDevices.get(1).getMobileQuantityData())).build());
		    Information.builder().code(ResponseConstants.PF_DATOS_MOVILES_GAP).value(String.valueOf(product.getMobil0QuantityData() - p.getMobil0QuantityData())).build());
		    
		   
	    }
	    
	}else {
	
        	if(movilDevices!=null) {
        		
                	if (product.getMobil1QuantityData() != null && movilDevices.get(0).getMobileQuantityData() != null) {
                		   /* billingOffersInfo.add(
                			    Information.builder().code(ResponseConstants.MOVIL2_DATOS).value(product.getMobil1QuantityData().toString()).build());*/
                		    
                		    //System.out.println("product.getMobil1QuantityData() ====> " + product.getMobil1QuantityData());
                		    //System.out.println("movilDevices.get(1).getMobileQuantityData() ====> " + movilDevices.get(0).getMobileQuantityData());
                		    //System.out.println("RESTA: "+(product.getMobil1QuantityData() - movilDevices.get(0).getMobileQuantityData()));
                		    
                		    
                		    
                		    billingOffersInfo.add(
                			   // Information.builder().code(ResponseConstants.MOVIL2_DATOS).value(String.valueOf(product.getMobil1QuantityData() - movilDevices.get(1).getMobileQuantityData())).build());
                		    Information.builder().code(ResponseConstants.PF_DATOS_MOVILES_GAP).value(String.valueOf(product.getMobil0QuantityData() - movilDevices.get(0).getMobileQuantityData())).build());
                		    
                		   
                	}
        	
        	}
	
	}
	
	if(!actual) {
	
        	if (product.getMobil0Apps() != null && !fixedPark.getOnlyOneMobileIndicator()) {
        	    billingOffersInfo
        		    .add(Information.builder().code(ResponseConstants.MOVIL2_APPS).value(product.getMobil0Apps().toString()).build());
        	}
	
	}else {
	    if(commerInfo.getCommercialOpers().size()>2) {
        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
        		    
        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
        		
                		if((actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT))||(actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.CAEQ.getCodeDesc()))||(actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.PORTABILITY.getCodeDesc()))) {
                		    if (product.getMobil0Apps() != null && !fixedPark.getOnlyOneMobileIndicator()) {
                        		    billingOffersInfo
                        		    .add(Information.builder().code(ResponseConstants.MOVIL2_APPS).value(product.getMobil0Apps().toString()).build());
                		    }
                		}
        		
        		    }
        		
        	    }else {
        		if(commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.ACTIVATION.getCodeDesc())) {
        		    billingOffersInfo
        		    .add(Information.builder().code(ResponseConstants.MOVIL2_APPS).value(product.getMobil0Apps().toString()).build());
        		}
        	    }
	    }
	    
	}
	
	
	if (product.getMobil0Sms() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL1_SMS).value(product.getMobil0Sms()).build());
	}

	if(!actual) {
        	if (product.getMobil0Sms() != null && !fixedPark.getOnlyOneMobileIndicator()) {
        	    billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL2_SMS).value(product.getMobil0Sms()).build());
        	}
	}else {
	    if(commerInfo.getCommercialOpers().size()>2) {
        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
        		    
        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
        		
                		if((actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT))||(actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.CAEQ.getCodeDesc()))||(actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.PORTABILITY.getCodeDesc()))) {
                		    if (product.getMobil0Sms() != null && !fixedPark.getOnlyOneMobileIndicator()) {
                			billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL2_SMS).value(product.getMobil0Sms()).build());
                		    }
                		}
        		
        		    }
        		
        	    }else {
        		if(commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.ACTIVATION.getCodeDesc())) {
        		    billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL2_SMS).value(product.getMobil0Sms()).build());
        		}
        	    }
	    }
	    
	}
	
	if (product.getMobil0Roaming() != null) {
	    billingOffersInfo
		    .add(Information.builder().code(ResponseConstants.MOVIL1_ROAMING).value(product.getMobil0Roaming().toString()).build());
	}
	
	if(!actual) {
        	if (product.getMobil1Roaming() != null && !fixedPark.getOnlyOneMobileIndicator()) {
        	    billingOffersInfo
        		    .add(Information.builder().code(ResponseConstants.MOVIL2_ROAMING).value(product.getMobil1Roaming().toString()).build());
        	}
	}else {
	    if(commerInfo.getCommercialOpers().size()>2) {
        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
        		    
        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
        		
                		if((actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT))||(actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.CAEQ.getCodeDesc()))||(actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.PORTABILITY.getCodeDesc()))) {
                		    if (product.getMobil1Roaming() != null && !fixedPark.getOnlyOneMobileIndicator()) {
                        		    billingOffersInfo
                        		    .add(Information.builder().code(ResponseConstants.MOVIL2_ROAMING).value(product.getMobil1Roaming().toString()).build());
                		    }
                		}
        		
        		    }
        		
        	    }else {
        		if(commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.ACTIVATION.getCodeDesc())) {
        		    if(product.getMobil1Roaming()!=null) {
                		    billingOffersInfo
                		    .add(Information.builder().code(ResponseConstants.MOVIL2_ROAMING).value(product.getMobil1Roaming().toString()).build());
        		    }
        		}
        	    }
	    }
	    
	}
	
	
	if (product.getMobil0WaInt() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL1_WAINT).value(product.getMobil0WaInt()).build());
	}
	
	if(!actual) {
        	if (product.getMobil0WaInt() != null && !fixedPark.getOnlyOneMobileIndicator()) {
        	    billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL2_WAINT).value(product.getMobil0WaInt()).build());
        	}
	}else {
	    if(commerInfo.getCommercialOpers().size()>2) {
        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
        		    
        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
        		
                		if((actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT))||(actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.CAEQ.getCodeDesc()))||(actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.PORTABILITY.getCodeDesc()))) {
                		    if (product.getMobil0WaInt() != null && !fixedPark.getOnlyOneMobileIndicator()) {
                			billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL2_WAINT).value(product.getMobil0WaInt()).build());
                		    }
                		}
        		
        		    }
        		
        	    }else {
        		if(commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.ACTIVATION.getCodeDesc())) {
        		    billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL2_WAINT).value(product.getMobil0WaInt()).build());
        		}
        	    }
	    }
	    
	}
	
	
	if (product.getMobil0Rent() != null) {
	    
	    
	    if(commerInfo.getCommercialOpers().size() == 3) {
        	    if((actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.CAEQ.getCodeDesc()))||
        			(actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.PORTABILITY.getCodeDesc())||
        			(actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.ACTIVATION.getCodeDesc())))) {
        		    billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL1_RENTA)
        			.value(String.valueOf(product.getMobil0Rent())).build());
        	    }
	    }else if (fixedPark.getOnlyOneMobileIndicator() && product.getMobil1Rent() != null) {
		
		
        		billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVIL1_RENTA)
        			.value(String.valueOf(product.getMobil0Rent() + product.getMobil1Rent())).build());
		
	    } else {
		billingOffersInfo
			.add(Information.builder().code(ResponseConstants.MOVIL1_RENTA).value(product.getMobil0Rent().toString()).build());
	    }
	}
	
	if(!actual) {
        	if (product.getMobil1Rent() != null && !fixedPark.getOnlyOneMobileIndicator() && movilDevices.size() == 2) {
        	    billingOffersInfo
        	    .add(Information.builder().code(ResponseConstants.MOVIL2_RENTA).value(product.getMobil1Rent().toString()).build());
        	}
	}else {
	    if(commerInfo.getCommercialOpers().size()>2) {
        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
        		    
        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
        		
                		if((actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT))||(actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.CAEQ.getCodeDesc()))||(actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.PORTABILITY.getCodeDesc()))) {
                		    if (product.getMobil1Rent() != null && !fixedPark.getOnlyOneMobileIndicator() && movilDevices.size() == 2) {
                        		    billingOffersInfo
                                	    .add(Information.builder().code(ResponseConstants.MOVIL2_RENTA).value(product.getMobil1Rent().toString()).build());
                		    }
                		}
        		
        		    }
        		
        	    }else {
        		if(commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.ACTIVATION.getCodeDesc())) {
        		    
        		    if(product.getMobil1Rent() == null) {
        				billingOffersInfo
                    	    		.add(Information.builder().code(ResponseConstants.MOVIL2_RENTA).value("0.0").build());
        		    }else {
        		    
                		    billingOffersInfo
                        	    .add(Information.builder().code(ResponseConstants.MOVIL2_RENTA).value(product.getMobil1Rent().toString()).build());
        		    
        		    }
        		}
        	    }
	    }
	    
	}
	
	
	if (product.getMobil0RoamingCoverage() != null) {
	    billingOffersInfo.add(
		    Information.builder().code(ResponseConstants.MOVIL1_ROAMINGCOVERAGE).value(product.getMobil0RoamingCoverage()).build());
	}
	
	if(!actual) {
		/** Se añade validación extra para MOVIL1_ROAMINGCOVERAGE en lugar de MOVIL2_ROAMINGCOVERAGE - solo LMA **/
    	if (product.getMobil1RoamingCoverage() != null) {
    		if(!commerInfo.getProductType().equals(ProductType.MOBILE.getCodeDesc()))
    	    billingOffersInfo.add(
    		    Information.builder().code(ResponseConstants.MOVIL2_ROAMINGCOVERAGE).value(product.getMobil1RoamingCoverage()).build());
    		else 
    		billingOffersInfo.add(
            	Information.builder().code(ResponseConstants.MOVIL1_ROAMINGCOVERAGE).value(product.getMobil1RoamingCoverage()).build());         		
    	}

	}else {
	    if(commerInfo.getCommercialOpers().size()>2) {
        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
        		    
        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
        		
                		if((actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT))||(actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.CAEQ.getCodeDesc()))||(actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.PORTABILITY.getCodeDesc()))) {
                		    if (product.getMobil1RoamingCoverage() != null) {
                        		    billingOffersInfo.add(
                                		    Information.builder().code(ResponseConstants.MOVIL2_ROAMINGCOVERAGE).value(product.getMobil1RoamingCoverage()).build());
                		    }
                		}
        		
        		    }
        		
        	    }else {
        		if(commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.ACTIVATION.getCodeDesc())) {
        		    billingOffersInfo.add(
                		    Information.builder().code(ResponseConstants.MOVIL2_ROAMINGCOVERAGE).value(product.getMobil1RoamingCoverage()).build());
        		}
        	    }
	    }
	    
	}
	
	if (product.getMobil0GigaPass() != null) {
	    billingOffersInfo.add(
		    Information.builder().code(ResponseConstants.MOVIL1_GIGAPASS).value(product.getMobil0GigaPass().toString()).build());
	}
	
	if(!actual) {
        	if (product.getMobil1GigaPass() != null && !fixedPark.getOnlyOneMobileIndicator()) {
        	    billingOffersInfo.add(
        		    Information.builder().code(ResponseConstants.MOVIL2_GIGAPASS).value(product.getMobil1GigaPass().toString()).build());
        	}
	}else {
	    if(commerInfo.getCommercialOpers().size()>2) {
        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
        		    
        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
        		
                		if((actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT))||(actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.CAEQ.getCodeDesc()))||(actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.PORTABILITY.getCodeDesc()))) {
                		    if (product.getMobil1GigaPass() != null && !fixedPark.getOnlyOneMobileIndicator()) {
                        		    billingOffersInfo.add(
                                		    Information.builder().code(ResponseConstants.MOVIL2_GIGAPASS).value(product.getMobil1GigaPass().toString()).build());
                		    }
                		}
        		
        		    }
        		
        	    }else {
        		if(commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.ACTIVATION.getCodeDesc())) {
        		    if(product.getMobil1GigaPass() == null) {
                		    billingOffersInfo.add(
                        		    Information.builder().code(ResponseConstants.MOVIL2_GIGAPASS).value("0").build());
        		    }else {
        			    billingOffersInfo.add(
        				    Information.builder().code(ResponseConstants.MOVIL2_GIGAPASS).value(product.getMobil1GigaPass().toString()).build());
        		    }
        		}
        	    }
	    }
	    
	}
	
	if (product.getPackageFixedRent() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.PAQUETE_RENTA_FIJA)
		    .value(product.getPackageFixedRent().toString()).build());
	}

	if (product.getPackageRent() != null) {
	    billingOffersInfo
		    .add(Information.builder().code(ResponseConstants.RENTAPAQUETE).value(product.getPackageRent().toString()).build());
	}
	if (product.getRegularPrice() != null) {
	    billingOffersInfo
		    .add(Information.builder().code(ResponseConstants.PRECIOREGULAR).value(product.getRegularPrice().toString()).build());
	}
	if (product.getRegularPrice() != null && product.getPackageRent() != null) {
	    double discount = product.getRegularPrice() - product.getPackageRent();
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.DESCUENTO).value(String.valueOf(discount)).build());
	}
	if (product.getPackageName() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.NOMBRE).value(product.getPackageName()).build());
	}
	if (product.getPackagePs() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.PS).value(product.getPackagePs().toString()).build());
	}
	if (product.getHomeCostAlone() != null) {
	    billingOffersInfo
		    .add(Information.builder().code(ResponseConstants.COSTOHOGARSOLO).value(product.getHomeCostAlone().toString()).build());
	}
	if (product.getMobil1CostAlone() != null) {
	    if (fixedPark.getOnlyOneMobileIndicator() && product.getMobil2CostAlone() != null) {
		billingOffersInfo.add(Information.builder().code(ResponseConstants.COSTOMOVIL1_SOLO)
			.value(String.valueOf(product.getMobil1CostAlone() + product.getMobil2CostAlone())).build());
	    } else {
		billingOffersInfo.add(Information.builder().code(ResponseConstants.COSTOMOVIL1_SOLO)
			.value(product.getMobil1CostAlone().toString()).build());
	    }
	}
	
	if(!actual) {
	
        	if (product.getMobil2CostAlone() != null && !fixedPark.getOnlyOneMobileIndicator()) {
        	    billingOffersInfo.add(
        		    Information.builder().code(ResponseConstants.COSTOMOVIL2_SOLO).value(product.getMobil2CostAlone().toString()).build());
        	}
	
	}else {
	    if(commerInfo.getCommercialOpers().size()>2) {
        	    if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
        		    
        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
        		
                		if((actual && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT))||(actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.CAEQ.getCodeDesc()))||(actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.PORTABILITY.getCodeDesc()))) {
                		    if (product.getMobil2CostAlone() != null && !fixedPark.getOnlyOneMobileIndicator()) {
                        		billingOffersInfo.add(
                        		    Information.builder().code(ResponseConstants.COSTOMOVIL2_SOLO).value(product.getMobil2CostAlone().toString()).build());
                		    }
                		}
        		
        		    }
        		
        	    }else {
        		if(commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.ACTIVATION.getCodeDesc())) {
        		    if(product.getMobil1GigaPass() == null) {
                		    billingOffersInfo.add(
                        		    Information.builder().code(ResponseConstants.COSTOMOVIL2_SOLO).value("0.0").build());
        		    }else {
        			    billingOffersInfo.add(
        				    Information.builder().code(ResponseConstants.COSTOMOVIL2_SOLO).value(product.getMobil2CostAlone().toString()).build());
        		    }
        		}
        	    }
	    }
	    
	}
	
	if (product.getMovistarPrix() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVISTAR_PRIX).value(product.getMovistarPrix()).build());
	}
	if (product.getMovistarPlay() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.MOVISTAR_PLAY).value(product.getMovistarPlay()).build());
	}
	if (product.getNetflix() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.NETFLIX).value(product.getNetflix()).build());
	}
	if (product.getDuplicateBonus() != null) {
	    for (Integer position : fixedPark.getCommOperProvidePortaPositions()) {
		
		if(!actual && (position==1)) {
		
		billingOffersInfo.add(Information.builder().code(String.format(ResponseConstants.MOVIL_BONO_DUPLICA, position))
			.value(product.getDuplicateBonus()).build());
		
		}
		
		if(!actual && (position==2)) {
			
			billingOffersInfo.add(Information.builder().code(String.format(ResponseConstants.MOVIL_BONO_DUPLICA, position))
				.value(product.getDuplicateBonus()).build());
			
		}
		
		if(actual && (position==1)) {
			
			billingOffersInfo.add(Information.builder().code(String.format(ResponseConstants.MOVIL_BONO_DUPLICA, position))
				.value(product.getDuplicateBonus()).build());
			
		}	
		
		if(commerInfo.getCommercialOpers().size()>2) {
        		if(commerInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
        		    
        		    if(commerInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {
        		
                		if((actual && (position==2) && commerInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TIPO_MT))||(actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.CAEQ.getCodeDesc()))||(actual && commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.PORTABILITY.getCodeDesc()))) {
                			
                			billingOffersInfo.add(Information.builder().code(String.format(ResponseConstants.MOVIL_BONO_DUPLICA, position))
                				.value(product.getDuplicateBonus()).build());
                			
                		}
        		
        		    }
        		
        		}else {
                		if(commerInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.ACTIVATION.getCodeDesc())) {
                		    billingOffersInfo.add(Information.builder().code(String.format(ResponseConstants.MOVIL_BONO_DUPLICA, position))
            				.value(product.getDuplicateBonus()).build());
                		}
                	    }
		}
	    }
	}
	if (product.getMinutesLine() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.LINEA_MINUTOS).value(product.getMinutesLine()).build());
	}
	if (product.getCableChannels() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.CABLE_CANALES).value(product.getCableChannels()).build());
	}
	/*if (product.getOfferTotalRent() != null) {
	    billingOffersInfo
		    .add(Information.builder().code(ResponseConstants.RENTATOTAL).value(product.getOfferTotalRent().toString()).build());
	}*/
	/*if (StringUtils.isNotBlank(product.getModemEquipment())) {
	    billingOffersInfo
		    .add(Information.builder().code(ResponseConstants.FIJO_EQUIPAMIENTO_MODEM).value(product.getModemEquipment()).build());
	}*/
	
	if (StringUtils.isNotBlank(product.getModemEquipment()) || product.getModemEquipment() == null) {
	    /*if(oferta < cantidad) {
        	    billingOffersInfo
        		    .add(Information.builder().code(ResponseConstants.FIJO_EQUIPAMIENTO_MODEM).value(product.getModemEquipment()).build());
	    }else {
		billingOffersInfo
		    .add(Information.builder().code(ResponseConstants.FIJO_EQUIPAMIENTO_MODEM).value(fixedPark.getModemEquipmentOfferCopy()).build());
	    }*/
	    
	    /*if(product.getModemEquipment() == null) {
		billingOffersInfo
		    .add(Information.builder().code(ResponseConstants.FIJO_EQUIPAMIENTO_MODEM).value(fixedPark.getModemEquipmentOfferCopy()).build());
	    }else {*/
		if(existeMigracion) {
		    if(fixedPark.getContar() == null) {
			billingOffersInfo
    		    .add(Information.builder().code(ResponseConstants.FIJO_EQUIPAMIENTO_MODEM).value(product.getModemEquipment()==null? "":product.getModemEquipment()).build());
		    }else {
        		if(fixedPark.getContar().equals(oferta)) {
        		
                    		billingOffersInfo
                    		    .add(Information.builder().code(ResponseConstants.FIJO_EQUIPAMIENTO_MODEM).value(fixedPark.getModemEquipmentOfferCopy()==null? "":fixedPark.getModemEquipmentOfferCopy()).build());
        		
        		}
        		else {
                		    billingOffersInfo
                		    .add(Information.builder().code(ResponseConstants.FIJO_EQUIPAMIENTO_MODEM).value(product.getModemEquipment()==null? "":product.getModemEquipment()).build());
        		}
		    }
		}else {
		    billingOffersInfo
		    .add(Information.builder().code(ResponseConstants.FIJO_EQUIPAMIENTO_MODEM).value(product.getModemEquipment()).build());
		}
	    //}
	}
	
	
	
	
	int existe = 0;
	
	//AÃ±adiendo Campos de costo de InstalaciÃ²n y Cuotas de InstalaciÃ²n
		if (product.getInstalacionCosto() != null) {
		    existe++;
		    billingOffersInfo
			    .add(Information.builder().code(ResponseConstants.INSTALACION_COSTO).value(product.getInstalacionCosto().toString()).build());
		}
		if (product.getInstalacionCuotas() != null) {
		    existe++;
		    billingOffersInfo
			    .add(Information.builder().code(ResponseConstants.INSTALACION_CUOTAS).value(product.getInstalacionCuotas().toString()).build());
		}
		
		
		
		if (product.getOfferTotalRent() != null) {
		    if(product.getCurrentProduct() != null && product.getCurrentProduct().equals(Constant.CURRENT_OFFER_TYPE)) {
			billingOffersInfo
    		    		.add(Information.builder().code(ResponseConstants.RENTATOTAL).value(String.valueOf((product.getOfferTotalRent()))).build());
		    }else {
        		    if(!(product.getEnabled().equals(Constant.TWO))) {
        	        	    if(existe == 2) {
        	        		billingOffersInfo
        	                		    .add(Information.builder().code(ResponseConstants.RENTATOTAL).value(String.valueOf((product.getOfferTotalRent()+(product.getInstalacionCosto() != null? product.getInstalacionCosto() : 0)))).build());
        	        	    }else {
        	        		
        	        		if(movilDevices.size() == 2) {
        	        		    billingOffersInfo
        	        		    .add(Information.builder().code(ResponseConstants.RENTATOTAL).value(String.valueOf((product.getOfferTotalRent()+product.getMobil1Rent()))).build());
        	        		}else {
        	                		billingOffersInfo
        	                		    .add(Information.builder().code(ResponseConstants.RENTATOTAL).value(String.valueOf((product.getOfferTotalRent()))).build());
        	        		}
        	        	    }
        		    }else {
        			billingOffersInfo
        			    .add(Information.builder().code(ResponseConstants.RENTATOTAL).value(String.valueOf((product.getOfferTotalRent()+product.getMobil1Rent()))).build());
        		    }
		    }
		}
	
	if (StringUtils.isNotBlank(product.getDecoEquipment())) {
	    billingOffersInfo
		    .add(Information.builder().code(ResponseConstants.FIJO_EQUIPAMIENTO_DECO).value(product.getDecoEquipment()).build());
	}
	if (product.getRentByAditionals() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.RENTAPORADICIONALES)
		    .value(product.getRentByAditionals().toString()).build());
	}
	if (product.getJump() != null && product.getEmployeeJump() == null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.SALTO).value(product.getJump().toString()).build());
	} else {
	    if (product.getJump() != null) {
		billingOffersInfo
			.add(Information.builder().code(ResponseConstants.SALTO).value(product.getEmployeeJump().toString()).build());
	    }
	    if (product.getEmployeeDiscount() != null) {
		billingOffersInfo.add(Information.builder().code(ResponseConstants.DESC_EMPLEADO_MONTO)
			.value(product.getEmployeeDiscount().toString()).build());
		billingOffersInfo.add(Information.builder().code(ResponseConstants.DESC_EMPLEADO_RENTA_PAQUETE)
			.value(product.getEmployeePackageRent().toString()).build());
	    }
	}
	// reconozca la 2 o 3ra iteracion no muestra
	if (cont.equals(1) && (product.getPriority() != null)) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.PRIORIDAD).value(product.getPriority()).build());
	}
	
	if(product.getCurrentProduct()==null) {
	
        	if (CollectionUtils.isNotEmpty(product.getCharacteristics())) {
        	    for (Information information : product.getCharacteristics()) {
        		//billingOffersInfo.add(Information.builder().code(ResponseConstants.CARACTERISTICA).value(information.getValue()).build());
        		if(information.getCode().equals(rulesValue.getPRODUCT_IND_OFFER())) {
        		    billingOffersInfo.add(Information.builder().code(rulesValue.getPRODUCT_IND_OFFER()).value(information.getValue()).build());
        		}else if(information.getCode().equals(rulesValue.getPRODUCT_MIGRATION_TECHNOLOGY())) {
        		    billingOffersInfo.add(Information.builder().code(rulesValue.getPRODUCT_MIGRATION_TECHNOLOGY()).value(information.getValue()).build());
        		}else {
        		    
        		    if(oferta == cantidad) {
        			billingOffersInfo.add(Information.builder().code(ResponseConstants.CARACTERISTICA).value(information.getValue()).build());
        		    }else {
        			if(!(information.getCode().equals(rulesValue.getPRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT()))) {
        			    billingOffersInfo.add(Information.builder().code(ResponseConstants.CARACTERISTICA).value(information.getValue()).build());
        			}
        		    }
        		    
        		}
        	    }
        	}
	
	}else {
	    
	    if(!(pmtA.getValor()==null)) {
	    
        	    if(pmtA.getValor().equals("1")) {
        	    
                	    if(oferta == cantidad) {
                		billingOffersInfo.add(Information.builder().code(rulesValue.getPRODUCT_MIGRATION_TECHNOLOGY()).value("1").build());
                	    }else if(oferta == 1){
                		billingOffersInfo.add(Information.builder().code(rulesValue.getPRODUCT_MIGRATION_TECHNOLOGY()).value("0").build());
                	    }
        	    
        	    }
	    
	    }
	}

	if (CollectionUtils.isNotEmpty(product.getPenaltiesPlankRank())) {
	    billingOffersInfo.addAll(product.getPenaltiesPlankRank());
	}
	if (CollectionUtils.isNotEmpty(product.getComparativeComponents())) {
	    billingOffersInfo.addAll(product.getComparativeComponents());
	}
	if (product.getAditionalRentsToKeep() != null) {
	    billingOffersInfo.addAll(product.getAditionalRentsToKeep());
	}

	if (product.getAditionalRentsToKeepSummatory() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.AD_RENTAADICIONALESMANTENER)
		    .value(product.getAditionalRentsToKeepSummatory().toString()).build());
	}
	if (product.getAditionalBlocksTokeep() != null) {
	    for (Information blocks : product.getAditionalBlocksTokeep()) {
		if (rulesValue.getBLOCK_HD_KEEP().equals(blocks.getCode())) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.AD_HD_BLOQUE).value(blocks.getValue()).build());
		}
		if (rulesValue.getBLOCK_HBO_KEEP().equals(blocks.getCode())) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.AD_HBO_BLOQUE).value(blocks.getValue()).build());
		}
		if (rulesValue.getBLOCK_FOX_KEEP().equals(blocks.getCode())) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.AD_FOX_BLOQUE).value(blocks.getValue()).build());
		}
		if (rulesValue.getBLOCK_EST_KEEP().equals(blocks.getCode())) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.AD_ESTELAR_BLOQUE).value(blocks.getValue()).build());
		}
		if (rulesValue.getBLOCK_GOLDPREM_KEEP().equals(blocks.getCode())) {
		    billingOffersInfo
			    .add(Information.builder().code(ResponseConstants.AD_GOLDPREM_BLOQUE).value(blocks.getValue()).build());
		}
		if (rulesValue.getBLOCK_HTPACK_KEEP().equals(blocks.getCode())) {
		    billingOffersInfo.add(Information.builder().code(ResponseConstants.AD_HTPACK_BLOQUE).value(blocks.getValue()).build());
		}
	    }
	}
	if (product.getAditionalBlocksTokeepRent() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.AD_RENTABLOQUESADICIONALESMANTENER)
		    .value(product.getAditionalBlocksTokeepRent().toString()).build());
	}
	if (product.getTotalCostAloneProducts() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.COSTOTOTALPRODUCTOSSEPARADOS)
		    .value(product.getTotalCostAloneProducts().toString()).build());
	}
	if (product.getInternetDestinyTechnology() != null) {
	    if(oferta < cantidad) {
        	    billingOffersInfo.add(Information.builder().code(ResponseConstants.INTERNET_TECNOLOGIADESTINO)
        		    .value(product.getInternetDestinyTechnology()).build());
	    }else {
		
		boolean existeAct = false;
		
		for (Information information : product.getCharacteristics()) {
        		if(information.getValue().equals(rulesValue.getPRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT())) {
        		    existeAct = true;
        		    if(product.getInternetDestinyTechnology().equals("HFC")) {
        			billingOffersInfo.add(Information.builder().code(ResponseConstants.INTERNET_TECNOLOGIADESTINO)
        	        		    .value("FTTH").build());
        		    }
        		}
		
		}
		
		if(!existeAct) {
		
        		if(!(p==null)) {
        		
                		if(p.getInternetSpeed().equals(product.getInternetSpeed())) {
                		    
                		    billingOffersInfo.add(Information.builder().code(ResponseConstants.INTERNET_TECNOLOGIADESTINO)
        	        		    .value("FTTH").build());
                		    
                		}
        		
        		}
		
		}
	    }
	}
	
	//if(!(product.getEnabled().equals(Constant.TWO))) {
	if(product.getPackageType()!=null) {
		if(!(product.getPackageType().equals(Constant.TIPO_PAQUETE_MOVIL))) {
			billingOffersInfo.add(Information.builder().code(ResponseConstants.EQUIPAMIENTO_CANTIDADDECOS).value(
				product.getEquipmentDecoQuantity() == null ? Constant.ZERO.toString() : product.getEquipmentDecoQuantity().toString())
				.build());	
			

			billingOffersInfo.add(Information.builder().code(ResponseConstants.EQUIPAMIENTO_CANTIDADREPETIDORWIFI)
				.value(product.getEquipmentUltraWifiRepQuantity() == null ? Constant.ZERO.toString()
					: product.getEquipmentUltraWifiRepQuantity().toString())
				.build());
			
			}
	}
	
	if(product.getMerchandisingType() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.PARRILLA_PERTENENCIA)
		    .value(product.getMerchandisingType()).build());
	}
	 
	if(product.getCodeAutomatizer() != null) {
		    billingOffersInfo
		         .add(Information.builder().code(ResponseConstants.CODIGO_AUTOMATIZADOR).value(product.getCodeAutomatizer()).build());
	}

	if (product.getCatalogDiscountTempDTO() != null) {
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.DESCUENTO_PS)
		    .value(product.getCatalogDiscountTempDTO().getDiscountPS() == null ? Constant.ZERO.toString()
			    : product.getCatalogDiscountTempDTO().getDiscountPS().toString())
		    .build());
	    /*billingOffersInfo.add(Information.builder().code(ResponseConstants.DESCUENTO_NOMBRE)
		    .value(product.getCatalogDiscountTempDTO().getDiscountName() == null ? Constant.ZERO.toString()
			    : product.getCatalogDiscountTempDTO().getDiscountName())
		    .build());*/
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.DESCUENTO_MONTO)
		    .value(product.getCatalogDiscountTempDTO().getDiscountAmount() == null ? Constant.ZERO.toString()
			    : product.getCatalogDiscountTempDTO().getDiscountAmount().toString())
		    .build());
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.DESCUENTO_DURACION)
		    .value(product.getCatalogDiscountTempDTO().getDiscountduration() == null ? Constant.ZERO.toString()
			    : product.getCatalogDiscountTempDTO().getDiscountduration().toString())
		    .build());
//	    billingOffersInfo.add(Information.builder().code(ResponseConstants.DESCUENTO_CAMPANIA)
//		    .value(product.getCatalogDiscountTempDTO().getCampain() == null ? Constant.ZERO.toString()
//			    : product.getCatalogDiscountTempDTO().getCampain())
//		    .build());
	}
	
	//if(!(product.getEnabled().equals(Constant.TWO))) {
	if(product.getPackageType()!=null) {
		if(!product.getPackageType().equals(Constant.TIPO_PAQUETE_MOVIL)) {
			if(product.getCampaign() != null) {
        	    billingOffersInfo
        	         .add(Information.builder().code(ResponseConstants.DESCUENTO_CAMPANIA).value(product.getCampaign()).build());
        	}
		}        	
	
	}
	
	if (product.getCatalogPromoTempDTO() != null) {
	    
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.DESCUENTO_NOMBRE)
		    .value(product.getCatalogPromoTempDTO().getDiscountName() == null ? Constant.ZERO.toString()
			    : product.getCatalogPromoTempDTO().getDiscountName())
		    .build());
	    
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.PROMO_PS)
		    .value(product.getCatalogPromoTempDTO().getDiscountPS() == null ? Constant.ZERO.toString()
			    : product.getCatalogPromoTempDTO().getDiscountPS().toString())
		    .build());
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.PROMO_NOMBRE)
		    .value(product.getCatalogPromoTempDTO().getDiscountName() == null ? Constant.ZERO.toString()
			    : product.getCatalogPromoTempDTO().getDiscountName())
		    .build());
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.PROMO_MONTO)
		    .value(product.getCatalogPromoTempDTO().getSpeedAmount() == null ? Constant.ZERO.toString()
			    : product.getCatalogPromoTempDTO().getSpeedAmount().toString())
		    .build());
	    billingOffersInfo.add(Information.builder().code(ResponseConstants.PROMO_DURACION)
		    .value(product.getCatalogPromoTempDTO().getDiscountduration() == null ? Constant.ZERO.toString()
			    : product.getCatalogPromoTempDTO().getDiscountduration().toString())
		    .build());
//	    billingOffersInfo.add(Information.builder().code(ResponseConstants.DESCUENTO_CAMPANIA)
//		    .value(product.getCatalogDiscountTempDTO().getCampain() == null ? Constant.ZERO.toString()
//			    : product.getCatalogDiscountTempDTO().getCampain())
//		    .build());
	}
	
	ParamMovTotal paramMov = new ParamMovTotal();
	
	if(!(paramMov == null)) {
	    
	    if(oferta < cantidad) {
		
		    paramMov = getUpDown(product);
		    
        	    billingOffersInfo.add(Information.builder().code("Fijo_InternetUp")
        		    .value(paramMov.getCol1())
        		    .build());
        	    
        	    billingOffersInfo.add(Information.builder().code("Fijo_InternetDown")
        		    .value(paramMov.getValor())
        		    .build());
	    
	    }else {
		
		boolean existeAct = false;
		
		String internetTechnologyDestiny = "";
		
		for (Information information : product.getCharacteristics()) {
		    
        		if(information.getValue().equals(rulesValue.getPRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT())) {
        		    existeAct = true;
        		    if(product.getInternetDestinyTechnology().equals("HFC")) {
        			internetTechnologyDestiny = "FTTH";
        			paramMov = getUpDownCopyOffer(product, internetTechnologyDestiny);
        			
        			billingOffersInfo.add(Information.builder().code("Fijo_InternetUp")
        	        		    .value(paramMov.getCol1())
        	        		    .build());
        	        	    
        	        	billingOffersInfo.add(Information.builder().code("Fijo_InternetDown")
        	        		    .value(paramMov.getValor())
        	        		    .build());
        		    }
        		}
		
		}
		
		if(!existeAct) {
			
        		if(!(p==null)) {
        		
                		if(p.getInternetSpeed().equals(product.getInternetSpeed())) {
                		    
                		    internetTechnologyDestiny = "FTTH";
            			    paramMov = getUpDownCopyOffer(product, internetTechnologyDestiny);
            			    
            			    billingOffersInfo.add(Information.builder().code("Fijo_InternetUp")
                        		    .value(paramMov.getCol1())
                        		    .build());
                    	    
            			    billingOffersInfo.add(Information.builder().code("Fijo_InternetDown")
                        		    .value(paramMov.getValor())
                        		    .build());
                		    
                		}else {
                		    paramMov = getUpDown(product);  
                		    
                		    billingOffersInfo.add(Information.builder().code("Fijo_InternetUp")
                        		    .value(paramMov.getCol1())
                        		    .build());
                    	    
            			    billingOffersInfo.add(Information.builder().code("Fijo_InternetDown")
                        		    .value(paramMov.getValor())
                        		    .build());
                		}
        		
        		}
		
		}
		
	    }
	    
	}
	
	//if(product.getPackageType()!=null) {
		//if(!product.getPackageType().equals(Constant.TIPO_PAQUETE_MOVIL)) {
			/*if(product.getCampaignPromo() != null) {
    	    billingOffersInfo
    	         .add(Information.builder().code(ResponseConstants.PROMO_CAMPANIA).value(product.getCampaignPromo()).build());
    	}*/
	
	}
	 
	
	
//	billingOffersInfo.add(Information.builder().code(ResponseConstants.DATOS_MOVIL_ILIM).
//		    value(String.valueOf(TotalUtil.getNumericValueOf(product.getUnlimitedMovil()))).build());
//	
//	billingOffersInfo.add(Information.builder().code(ResponseConstants.CANTIDAD_LINEAS).
//		    value(String.valueOf(TotalUtil.getNumericValueOf(product.getQuantityMobilLines()))).build());
	
    }

    /* Metodos de current offers to keep */
    private void currentOffersToKeepResponse(ProductFixedDTO product, List<CurrentProductOffer> currentOffersToKeep) {
	CurrentProductOffer currentProductOfferToKeep = new CurrentProductOffer();
	List<Information> billingsOffersInfoKeep = new ArrayList<>();
	if (product.getAditionalBlocksTokeep() != null) {
	    for (Information blocks : product.getAditionalBlocksTokeep()) {
		if (rulesValue.getBLOCK_HD_KEEP().equals(blocks.getCode())) {
		    billingsOffersInfoKeep.add(Information.builder().code(ResponseConstants.AD_HD_BLOQUE).value(blocks.getValue()).build());
		}
		if (rulesValue.getBLOCK_HBO_KEEP().equals(blocks.getCode())) {
		    billingsOffersInfoKeep
			    .add(Information.builder().code(ResponseConstants.AD_HBO_BLOQUE).value(blocks.getValue()).build());
		}
		if (rulesValue.getBLOCK_FOX_KEEP().equals(blocks.getCode())) {
		    billingsOffersInfoKeep
			    .add(Information.builder().code(ResponseConstants.AD_FOX_BLOQUE).value(blocks.getValue()).build());
		}

	    }
	}

	currentProductOfferToKeep.setBillingOffersInfo(billingsOffersInfoKeep);
	currentOffersToKeep.add(currentProductOfferToKeep);
    }
    
    /* Metodos de ArcheType */
    private ParamMovTotal getArcheType(Double offerTotalRent) {
	List<ParamMovTotal> listParamMovTotal = paramMovTotalRepo.findAll();
	ParamMovTotal pmt = new ParamMovTotal();
	for (ParamMovTotal paramMovTotal : listParamMovTotal) {
	    if(!(paramMovTotal.getCodValor() == null)) {
        	    if(paramMovTotal.getCodValor().equals("03")) {
        		
        		if((paramMovTotal.getCol1() != null) &&  (paramMovTotal.getCol2() != null) ) {
        		    	
        		    	if(Double.parseDouble(paramMovTotal.getCol1()) < offerTotalRent && Double.parseDouble(paramMovTotal.getCol2()) > offerTotalRent) {
        		    	    
        		    	    pmt = paramMovTotal;
        		    	    
        			}
        		    	
        		}
        	    }
	    }
	    
	}
	
	return pmt;
    }
    
    /* Metodos de UpDown */
    private ParamMovTotal getUpDown(ProductFixedDTO product) {
	List<ParamMovTotal> listParamMovTotal = paramMovTotalRepo.findAll();
	ParamMovTotal pmt = new ParamMovTotal();
	for (ParamMovTotal paramMovTotal : listParamMovTotal) {
	    if((paramMovTotal.getGrupoParam().equals("SUBIDA_BAJADA_VELOCIDAD"))) {
		if(product.getInternetDestinyTechnology().equals(paramMovTotal.getCodValor()))
        	    if(product.getInternetSpeed() == Integer.parseInt(paramMovTotal.getValor())) {
        		pmt = paramMovTotal;
        		
        		break;
        	    }
	    }
	    
	}
	
	return pmt;
    }
    
    private ParamMovTotal getUpDownCopyOffer(ProductFixedDTO product, String internetTechnologyDestiny) {
	List<ParamMovTotal> listParamMovTotal = paramMovTotalRepo.findAll();
	ParamMovTotal pmt = new ParamMovTotal();
	for (ParamMovTotal paramMovTotal : listParamMovTotal) {
	    if((paramMovTotal.getGrupoParam().equals("SUBIDA_BAJADA_VELOCIDAD"))) {
		if(internetTechnologyDestiny.equals(paramMovTotal.getCodValor()))
        	    if(product.getInternetSpeed() == Integer.parseInt(paramMovTotal.getValor())) {
        		pmt = paramMovTotal;
        		
        		break;
        	    }
	    }
	    
	}
	
	return pmt;
    }

    /* Metodos de Sales conditions */
    private void salesConditionsResponse(ProductFixedDTO product, List<SaleCondition> saleConditionList, Integer cantFinancing) {
	/*if (product.getFinancingSaleCondition() != null) {
	    List<FinancingSaleCondition> salesConditionsDto = product.getFinancingSaleCondition();
	    for (FinancingSaleCondition financial : salesConditionsDto) {
		SaleCondition saleCond = new SaleCondition();
		saleCond.setOperationCommId(financial.getCommercialOperationId());
		saleCond.setCondition(Information.builder().code(financial.getName()).value(financial.getValue().toString()).build());
		saleConditionList.add(saleCond);
	    }
	}*/
	if (product.getPostPaidEasy() != null) {
		if(cantFinancing>0) {
			List<PostPaidEasyDTO> postPaidEasy = product.getPostPaidEasy();
		    for (PostPaidEasyDTO postPaid : postPaidEasy) {
			SaleCondition saleCond = new SaleCondition();
			saleCond.setOperationCommId(postPaid.getCommercialOperationId());
			saleCond.setCondition(Information.builder().code(postPaid.getCode()).value(Constant.FALSE).build());
			saleConditionList.add(saleCond);
		    }
		}
		else {
			List<PostPaidEasyDTO> postPaidEasy = product.getPostPaidEasy();
		    for (PostPaidEasyDTO postPaid : postPaidEasy) {
			SaleCondition saleCond = new SaleCondition();
			saleCond.setOperationCommId(postPaid.getCommercialOperationId());
			saleCond.setCondition(Information.builder().code(postPaid.getCode()).value(postPaid.getValue()).build());
			saleConditionList.add(saleCond);
		    }
		}
	}
    }
    
    /**
     * Se implementa la función generalSalesConditionsResponse , para poder añadir generalSaleConditions al response
     * y mostrar los planes de financiamiento.
     * 
     * @param planFinanLst1
     * @param planFinanLst2
     * @param generalSaleConditionList
     * @param quantityFinancing
     * @param sfinance
     * @param coi
     */
    private void generalSalesConditionsResponse(List<PlanFinancingEntity> planFinanLst1, List<PlanFinancingEntity> planFinanLst2,
    List<GeneralSaleCondition> generalSaleConditionList, Integer quantityFinancing, String sfinance, CommercialOperationInfo coi) {
		
	GeneralSaleCondition gsc = new GeneralSaleCondition();        	    
	gsc.setAdditionalInfo(new ArrayList<Information>());
	
	if(quantityFinancing!=0) {
	    
	    	if(coi.getCommercialOpers().size() > 1) {
        		if(sfinance.equals("10")) {
        			if(quantityFinancing==1 && planFinanLst1.size()>0) {
        				for(PlanFinancingEntity pfl : planFinanLst1) {
        		    		gsc.getAdditionalInfo().add(Information.builder().code(pfl.getFinancialPlanCode()).value(pfl.getFinancialPlanDescription()).build());
        		    		
        		    	}
        				gsc.setCondition(Information.builder().code("financiamiento").value(planFinanLst1.get(0).getPercentage().toString()).build());        		
        	 			gsc.setOperationCommId(coi.getCommercialOpers().get(1).getOperationCommId());
        	 				
        	    		generalSaleConditionList.add(gsc);
        			}			
        		}
        		
        		else if(sfinance.equals("01")) {
        			if(quantityFinancing==1 && planFinanLst1.size()>0) {
        				for(PlanFinancingEntity pfl : planFinanLst1) {
        		    		gsc.getAdditionalInfo().add(Information.builder().code(pfl.getFinancialPlanCode()).value(pfl.getFinancialPlanDescription()).build());
        		    		
        		    	}
        				gsc.setCondition(Information.builder().code("financiamiento").value(planFinanLst1.get(0).getPercentage().toString()).build());        		
        	 			gsc.setOperationCommId(coi.getCommercialOpers().get(2).getOperationCommId());
        	 				
        	    		generalSaleConditionList.add(gsc);
        			}
        			
        		}
        		
        		else if(sfinance.equals("11")) {
        			if(quantityFinancing>0) {
        				
        				
        				//Mobile 1
        				
        				if(planFinanLst1.size()>0) {
        					gsc = new GeneralSaleCondition();
        					gsc.setAdditionalInfo(new ArrayList<Information>());
        					for(PlanFinancingEntity pfl : planFinanLst1) {
        			    		gsc.getAdditionalInfo().add(Information.builder().code(pfl.getFinancialPlanCode()).value(pfl.getFinancialPlanDescription()).build());
        			    		
        			    	}
        					gsc.setCondition(Information.builder().code("financiamiento").value(planFinanLst1.get(0).getPercentage().toString()).build());        		
        		 			gsc.setOperationCommId(coi.getCommercialOpers().get(1).getOperationCommId());
        		 				
        		    		generalSaleConditionList.add(gsc);
        				}
        				
        	    		
        	    		//Mobile 2
        	    		
        	    		if(planFinanLst2.size()>0) {
        	    			gsc = new GeneralSaleCondition();
        	    			gsc.setAdditionalInfo(new ArrayList<Information>());
        	    			for(PlanFinancingEntity pfl : planFinanLst2) {
        			    		gsc.getAdditionalInfo().add(Information.builder().code(pfl.getFinancialPlanCode()).value(pfl.getFinancialPlanDescription()).build());
        			    		
        			    	}
        					gsc.setCondition(Information.builder().code("financiamiento").value(planFinanLst2.get(0).getPercentage().toString()).build());        		
        		 			gsc.setOperationCommId(coi.getCommercialOpers().get(2).getOperationCommId());
        		 				
        		    		generalSaleConditionList.add(gsc);
        	    		}
        	    		//System.out.println(generalSaleConditionList);
             		   
        			}
        			
        		}
		
	    	}else {
	    	    
	    	    
        	    	if(quantityFinancing==1 && planFinanLst1.size()>0) {
        			for(PlanFinancingEntity pfl : planFinanLst1) {
        	    		gsc.getAdditionalInfo().add(Information.builder().code(pfl.getFinancialPlanCode()).value(pfl.getFinancialPlanDescription()).build());
        	    		
        	    	}
        			gsc.setCondition(Information.builder().code("financiamiento").value(planFinanLst1.get(0).getPercentage().toString()).build());        		
        			gsc.setOperationCommId(coi.getCommercialOpers().get(0).getOperationCommId());
        				
        		generalSaleConditionList.add(gsc);
        		}
	    	    
	    	}
	}
	
	
	}
	

}
