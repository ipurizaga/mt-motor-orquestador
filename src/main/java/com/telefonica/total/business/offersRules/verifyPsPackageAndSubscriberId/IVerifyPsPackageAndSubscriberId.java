package com.telefonica.total.business.offersRules.verifyPsPackageAndSubscriberId;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.pojo.req.CommercialOperation;

public interface IVerifyPsPackageAndSubscriberId {

    /***
     * Método que se encarga de actualizar los campos consultados del parque fijo y
     * de devolver un indicador si los campos de subscriber Id del request y
     * paquetePS son distintos.
     * 
     * @param commercialOpeList
     * @param fixedParkDto
     * @param catalogProductFixedDto
     * @return
     */
    boolean execute(List<CommercialOperation> commercialOpeList, FixedDTO fixedParkDto,
	    List<ProductFixedDTO> catalogProductFixedDto);
}
