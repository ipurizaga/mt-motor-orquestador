package com.telefonica.total.business.offersRules.failPriorizations;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.model.ParamMovTotal;
import com.telefonica.total.pojo.req.CommercialOperation;

public interface IFailPriorizations {

    /***
     * Método que se encarga de las priorizaciones dentro del flujo de averias,
     * puede ser averia de tipo 1 o de tipo 2.
     * 
     * @param filteredProductList
     * @param type
     * @param commercialOpeList
     * @param mobileDevices
     * @return
     */
   List<ProductFixedDTO> executeFail(FixedDTO fixedParkDto, List<ProductFixedDTO> filteredProductList, List<ProductFixedDTO> filteredDisabledOffers, int type,
	    List<CommercialOperation> commercialOpeList, List<MobileDTO> mobileDevices, List<ProductFixedDTO> productsMtList, String coverage, List<ParamMovTotal> lstParam);
}
