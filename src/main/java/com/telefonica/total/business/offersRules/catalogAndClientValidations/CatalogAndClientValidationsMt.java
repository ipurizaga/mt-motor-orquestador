package com.telefonica.total.business.offersRules.catalogAndClientValidations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.telefonica.total.business.offersRules.productsRules.IProductsRules;
import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.RulesValueProp;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.common.util.UtilCollections;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.exception.BusinessException;
import com.telefonica.total.generic.CommOperCollection;
import com.telefonica.total.model.MasterUbigeo;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.CommercialOperationInfo;
import com.telefonica.total.repository.MasterUbigeoRepo;

@Service
@Qualifier("plantMt")
public class CatalogAndClientValidationsMt implements ICatalogAndClientValidations {

    @Autowired
    private RulesValueProp rulesValue;
    
    @Autowired
    private MasterUbigeoRepo masterUbigeoRepo;
    
    @Autowired
    private IProductsRules productRules;

    @Override
    public List<ProductFixedDTO> executeLMAMT(FixedDTO fixedParkDto, List<ProductFixedDTO> productList, List<ProductFixedDTO> productLMAList, List<MobileDTO> mobileDevices,
	    CommercialOperationInfo commercialInfo, boolean provideOrPortability, ProductFixedDTO product_current) {
	List<ProductFixedDTO> filteredProductList = new ArrayList<>();
	
	fixedParkDto.setBoIdRequest(commercialInfo.getCommercialOpers().get(1).getSubscriber().getBoId());
	fixedParkDto.setPoIdRequest(commercialInfo.getCommercialOpers().get(1).getSubscriber().getPoId());
	
	ProductFixedDTO product_actual = new ProductFixedDTO();
	
	/*aquí hacer la validación del type LMA*/
	if(commercialInfo.getCommercialOpers().size() == 3) {
	    	if(commercialInfo.getCommercialOpers().get(2).getSubscriber()!=null) { 
                	if(commercialInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TYPE_LMA)) {
                	
                	    addLMAToMT(productList, productLMAList) ;
                	
                	}else {        	
                	    
                	    if(commercialInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.TYPE_LMA)) {
                		addLMAToMT(productList, productLMAList) ;   
                	    }else {
                		if(commercialInfo.getCommercialOpers().get(2).getOperation().equals(Constant.OPERATION_PORTABILITY)) {
                    			addLMAToMT(productList, productLMAList) ;
                        	}else {
                        	    
                        	    if(commercialInfo.getCommercialOpers().get(2).getOperation().equals(Constant.OPERATION_REPLACEOFFER) && 
                        	       commercialInfo.getCommercialOpers().get(2).getSubscriber().getType().equals(Constant.MOBILE)) {
                        		addLMAToMT(productList, productLMAList) ;
                        	    }else {
                        	    
                        		product_actual = findCurrentOffer2Mobile(commercialInfo);   
                        		product_current = product_actual;
                            	    	addMT(productList, product_actual) ;
                        	    }
                        	}
                	    }
                	}
        	}else {
        	    if(commercialInfo.getCommercialOpers().get(2).getOperation().equals(Constant.OPERATION_ALTA)) {
        		addLMAToMT(productList, productLMAList) ;
        	    }else if(commercialInfo.getCommercialOpers().get(2).getOperation().equals(Constant.OPERATION_ALTA)) {
        		addLMAToMT(productList, productLMAList) ;
        	    }
        	}
	}
	
	
	/* sub regla 1 - Cobertura Iquitos */
	haveIquitosCoverage(fixedParkDto, productList);
	/* sub regla 2 - Cobertura diferente a HFC o FTTH */
	haveCoverageHfcOrFtth(productList, commercialInfo, fixedParkDto);
	/* sub regla 3 - localización con FTTH */
	//haveUbigeo(productList, fixedParkDto);
	/* sub regla 5 - Limite de credito movil del cliente */
	haveLimitMobileCredit(productList, mobileDevices, commercialInfo);
	filteredProductList.addAll(productList);
	return filteredProductList;
	
    }

    /***
     * Añadir LMA a todas las ofertas
     * 
     * @param fixedParkDto
     * @param productList
     */
    private void addLMAToMT(List<ProductFixedDTO> productList, List<ProductFixedDTO> productLMAList) {
	
	ProductFixedDTO pfd = productLMAList.get(0);
	
	for (ProductFixedDTO productFixedDTO : productList) {
	    productFixedDTO.setMobil1QuantityData(pfd.getMobil1QuantityData());
	    productFixedDTO.setMobil1Roaming(pfd.getMobil1Roaming());
	    productFixedDTO.setMobil1RoamingCoverage(pfd.getMobil1RoamingCoverage());
	    productFixedDTO.setMobil1Rent(pfd.getMobil1Rent());
	    productFixedDTO.setMobil1Name(pfd.getMobil1Name());
	    productFixedDTO.setMobil2CostAlone(pfd.getMobil2CostAlone());
	    productFixedDTO.setMobil1GigaPass(pfd.getMobil1GigaPass());
	  //Agregando bo y po del LMA a la oferta p3
	    productFixedDTO.setLma_Bo(pfd.getPackageMobilCode());
	    productFixedDTO.setLma_Po(pfd.getPoPackage());
	}
    }
    
    private void addMT(List<ProductFixedDTO> productList, ProductFixedDTO productMTList) {
	
	ProductFixedDTO pfd = productMTList;
	
	for (ProductFixedDTO productFixedDTO : productList) {
	    productFixedDTO.setMobil1QuantityData(pfd.getMobil1QuantityData());
	    productFixedDTO.setMobil1Roaming(pfd.getMobil1Roaming());
	    productFixedDTO.setMobil1RoamingCoverage(pfd.getMobil1RoamingCoverage());
	    productFixedDTO.setMobil1Rent(pfd.getMobil1Rent());
	    productFixedDTO.setMobil1Name(pfd.getMobil1Name());
	    productFixedDTO.setMobil2CostAlone(pfd.getMobil2CostAlone());
	    productFixedDTO.setMobil1GigaPass(pfd.getMobil1GigaPass());
	  //Agregando bo y po del LMA a la oferta p3
	    productFixedDTO.setLma_Bo(pfd.getPackageMobilCode());
	    productFixedDTO.setLma_Po(pfd.getPoPackage());
	}
    }
    
    public ProductFixedDTO findCurrentOffer2Mobile(CommercialOperationInfo commercialInfo) {

	CommOperCollection<CommercialOperation> commOperCollection = new CommOperCollection<>(commercialInfo.getCommercialOpers());
	
	List<ProductFixedDTO> catalogProductFixedDto = productRules.getProductFixed();
	
	ProductFixedDTO prod_current = new ProductFixedDTO();
	
	boolean no_existe = true;
	
	for (ProductFixedDTO productFixedDTO : catalogProductFixedDto) {
	    
	    if(productFixedDTO.getPackagePs().equals(Integer.parseInt(commOperCollection.get(0).getSubscriber().getSubscriberId()))) {
		
		//System.out.println("chau");
		
		if(commOperCollection.get(1).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO.getPackageMobilCode()))) {
		    prod_current = productFixedDTO;
		    no_existe = false;
		    
		}else if (commOperCollection.get(2).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO.getPackageMobilCode()))) {
		    
		    prod_current = productFixedDTO;
		    no_existe = false;
		}
		
	    }
	    
	}
	
	
	if(no_existe) {
	    if (commOperCollection.get(1).getSubscriber().getBoId().equals(commOperCollection.get(2).getSubscriber().getBoId())) {
        	    for (ProductFixedDTO productFixedDTO2 : catalogProductFixedDto) {
        		if(commOperCollection.get(1).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO2.getPackageMobilCode()))) {
        		    prod_current = productFixedDTO2;
        		    no_existe = false;
        		}
        	    }
	    
	    }else{
		for (ProductFixedDTO productFixedDTO3 : catalogProductFixedDto) {
        		if(commOperCollection.get(1).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO3.getPackageMobilCode()))) {
        		    prod_current = productFixedDTO3;
        		    no_existe = false;
        		    
        		}else if (commOperCollection.get(2).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO3.getPackageMobilCode()))) {
        		    
        		    prod_current = productFixedDTO3;
        		    no_existe = false;
        		}
		}
	    }
	}
	
	if(no_existe) {
	    throw new BusinessException(Constant.BE_2023);
	}
	
	//System.out.println("hola");
	
	return prod_current;
	
    }
    
    public ProductFixedDTO findCurrentOffer1Mobile(CommercialOperationInfo commercialInfo) {

	
	
	CommOperCollection<CommercialOperation> commOperCollection = new CommOperCollection<>(commercialInfo.getCommercialOpers());
	
	ProductFixedDTO prod_current = new ProductFixedDTO();
		
	List<ProductFixedDTO> catalogProductFixedDto = productRules.getProductFixed();
	
	String parrilla = "";
	
	for (ProductFixedDTO productFixedDTO : catalogProductFixedDto) {
	    
	    if(commOperCollection.get(1).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO.getPackageMobilCode()))) {
		
		parrilla = productFixedDTO.getMerchandisingType();
		break;		
	    }
	    
	}
	
	boolean existe=false;
	
	for (ProductFixedDTO productFixedDTO2 : catalogProductFixedDto) {
	
	    	if(productFixedDTO2.getPackagePs().equals(Integer.parseInt(commOperCollection.get(0).getSubscriber().getSubscriberId()))
	    		&& productFixedDTO2.getMerchandisingType().equals(parrilla)) {
		
	    	    //System.out.println("chau");		
	    	    
	    	    productFixedDTO2.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
	    	    prod_current = productFixedDTO2;
		    
		    existe = true;
		    break;
		}
	}
	
	if(!existe) {
	    throw new BusinessException(Constant.BE_1057);
	}
	
	//System.out.println("hola");
	return prod_current;
	
	
    }
    
    /***
     * Clientes en Iquitos solo pueden adquirir productos para Iquitos.
     * 
     * @param fixedParkDto
     * @param productList
     */
    private void haveIquitosCoverage(FixedDTO fixedParkDto, List<ProductFixedDTO> productList) {
	List<ProductFixedDTO> removeList = new ArrayList<>();
	for (ProductFixedDTO productFixed : productList) {
	    if (!fixedParkDto.getIquitosCobInternet().equals(productFixed.getInternetCoverageIquitos())) {
		removeList.add(productFixed);
	    }
	}
	productList.removeAll(removeList);
	TotalUtil.availableOffersMT(productList, Constant.BE_1068);
    }
    
    private void haveUbigeo(List<ProductFixedDTO> productList, FixedDTO fixedParkDto) {
	List<MasterUbigeo> listMasterUbigeo = this.masterUbigeoRepo.findAll();
	
	List<ProductFixedDTO> removeList = new ArrayList<>();
	
	boolean eliminar = false;
	
	boolean bdepa = true, bprov = true, bdist = true;
	
	if(fixedParkDto.getDepartmentLocation() == null) {
	    bdepa  = false;
	}
	
        if(fixedParkDto.getProvinceLocation() == null) {
            bprov  = false;
        }
        
        if(fixedParkDto.getDistrictLocation() == null) {
            bdist  = false;
        }
        
        if(bdepa && bprov && bdist) {
	
        	List<MasterUbigeo> listMasterUbigeoHabilitados = new ArrayList<MasterUbigeo>();
        	
        	for (MasterUbigeo masterUbigeo : listMasterUbigeo) {
        	    if(masterUbigeo.getHabilitado().equals("1")) {
        		    listMasterUbigeoHabilitados.add(masterUbigeo);
        	    }
        	}
        	
        	List<MasterUbigeo> listMasterUbigeo3campos = new ArrayList<MasterUbigeo>();
        	List<MasterUbigeo> listMasterUbigeo3camposNull = new ArrayList<MasterUbigeo>();
        	
        	for (MasterUbigeo masterUbigeoHabilitados : listMasterUbigeoHabilitados) {
        	    if(masterUbigeoHabilitados.getDepartamento() != null) {
        		listMasterUbigeo3campos.add(masterUbigeoHabilitados);
        	    }else {
        		listMasterUbigeo3camposNull.add(masterUbigeoHabilitados);
        	    }
        		/*else {
        		eliminar = true;
        	    }*/
        	}
        	
        	List<MasterUbigeo> listMasterUbigeo2campos = new ArrayList<MasterUbigeo>();
        	List<MasterUbigeo> listMasterUbigeo2camposNull = new ArrayList<MasterUbigeo>();
        	
        	for (MasterUbigeo masterUbigeo3campos : listMasterUbigeo3campos) {
        	    if(masterUbigeo3campos.getProvincia() != null) {
        		listMasterUbigeo2campos.add(masterUbigeo3campos);
        	    }else {
        		listMasterUbigeo2camposNull.add(masterUbigeo3campos);
        	    }
        	    /*if(masterUbigeo3campos.getProvincia() == null) {
        		eliminar = true;
        		//throw new BusinessException(Constant.BE_1056);
        	    }else {
        		listMasterUbigeo2campos.add(masterUbigeo3campos);
        	    }*/
        	}
        	
        	List<MasterUbigeo> listMasterUbigeo1campo = new ArrayList<MasterUbigeo>();
        	
        	for (MasterUbigeo masterUbigeo2campos : listMasterUbigeo2campos) {
        	    if(masterUbigeo2campos.getDistrito() != null) {
        		listMasterUbigeo1campo.add(masterUbigeo2campos);
        	    }
        	    /*if(masterUbigeo2campos.getDistrito() == null) {
        		eliminar = true;
        		//throw new BusinessException(Constant.BE_1056);
        	    }else {
        		listMasterUbigeo1campo.add(masterUbigeo2campos);
        	    }*/
        	}
        	
        	if(listMasterUbigeo1campo.size() > 0) {
        	    for (MasterUbigeo masterUbigeo1field : listMasterUbigeo1campo) {
        		if((fixedParkDto.getDistrictLocation().equals(masterUbigeo1field.getDistrito()) && 
        			fixedParkDto.getProvinceLocation().equals(masterUbigeo1field.getProvincia()) &&
        			fixedParkDto.getDepartmentLocation().equals(masterUbigeo1field.getDepartamento()))) {
        		    	eliminar = true;
        		    	break;
        		}
        	    }
        	    
        	    /*if(!eliminar) {
        		for (MasterUbigeo masterUbigeo1field : listMasterUbigeo1campo) {
        		    if(masterUbigeo1field.getProvincia() == null && masterUbigeo1field.getDistrito() !=null) {
        			eliminar = false;
        		    }
        		}
        	    }*/
        	    
        	}else {
        	    if(listMasterUbigeo2camposNull.size() > 0) {
        		for (MasterUbigeo masterUbigeo2fieldNull : listMasterUbigeo2camposNull) {
        			if((fixedParkDto.getProvinceLocation().equals(masterUbigeo2fieldNull.getProvincia()) &&
        				fixedParkDto.getDepartmentLocation().equals(masterUbigeo2fieldNull.getDepartamento()))) {
        			    	eliminar = true;
        			    	break;
        			}
        		}
        	    }else {
        		if(listMasterUbigeo3camposNull.size() > 0) {
        		    for (MasterUbigeo masterUbigeo3fieldNull : listMasterUbigeo3camposNull) {
        			if((fixedParkDto.getDepartmentLocation().equals(masterUbigeo3fieldNull.getDepartamento()))) {
        			    	eliminar = true;
        			    	break;
        			}
        		    }
        		}
        	    }
        	}
	
        }
	/*for (MasterUbigeo masterUbigeo : listMasterUbigeo) {
	    if(masterUbigeo.getHabilitado().equals("1")) {
		
		boolean bdepa = true, bprov = true, bdist = true;
		
		if(masterUbigeo.getDepartamento() == null) {
		    bdepa = false;
		}
		
		if(masterUbigeo.getProvincia() == null) {
		    bprov = false;
		}

		if(masterUbigeo.getDistrito() == null) {
		    bdist = false;
		}
		
		if(bdepa && bprov && bdist) {
		
        		if((fixedParkDto.getDistrictLocation().equals(masterUbigeo.getDistrito()) && 
        			fixedParkDto.getProvinceLocation().equals(masterUbigeo.getProvincia()) &&
        			fixedParkDto.getDepartmentLocation().equals(masterUbigeo.getDepartamento()))) {
        		    	eliminar = true;
        		    	break;
        		}
		
		}else {	    	
		    
		    	if(bdepa && bprov) {
        		    	if((fixedParkDto.getDistrictLocation().equals(masterUbigeo.getDepartamento()) && 
                			fixedParkDto.getProvinceLocation().equals(masterUbigeo.getProvincia()))) {
                		    	eliminar = true;
                		    	break;
                		}
		    	}else if(bdepa) {
        		    	if((fixedParkDto.getDistrictLocation().equals(masterUbigeo.getDepartamento()))) {
                		    	eliminar = true;
                		    	break;
                		}
		    	}
		    	
		    	
		}
	    }
	}*/
	
	//fixedParkDto.getSpeedInternet() >=60D
	
	for (ProductFixedDTO produ : productList) {
	    if(produ.getInternetSpeed() >=500000) {
		
		if(!eliminar) {
		    removeList.add(produ);
		}		
	    }
	}
	
	productList.removeAll(removeList);
	//TotalUtil.availableOffersMT(productList, Constant.BE_1069);
	
    }

    /***
     * Clientes con cobertura diferente a HFC o FTTH.
     * 
     * @param productList
     * @param removeList
     */
    private void haveCoverageHfcOrFtth(List<ProductFixedDTO> productList, CommercialOperationInfo commercialInfo, FixedDTO fixedParkDto) {
	List<ProductFixedDTO> removeList = new ArrayList<>();
	//CAMBIOS PARA PARRILLA FTTH
	/*
	if (!productList.isEmpty()) {
	    for (ProductFixedDTO productFixed : productList) {
		if (!(rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(commercialInfo.getCoverage())
			|| rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(commercialInfo.getCoverage()))) {
		    removeList.add(productFixed);
		}
	    }
	    productList.removeAll(removeList);
	    TotalUtil.availableOffersMT(productList, Constant.BE_1069);
	}*/
	/**Tomando la cobertura de la zona**/
	String coverage = commercialInfo.getCoverage().toLowerCase().equals("fiber")?"FTTH":commercialInfo.getCoverage();
	
	if(coverage.equals("HFC") && (fixedParkDto.getHfcCobInternet() == null? "": fixedParkDto.getHfcCobInternet()).toString().equals("2")) {
		coverage = "FTTH";
	}
	
	if (!productList.isEmpty()) {		
		switch(coverage) {
		/**SI LA COBERTURA DE LA ZONA ES HFC SE EVALUA DEPENDIENDO LA TECNOLOGIA ACTUAL**/
		 case "HFC":
			 if(fixedParkDto.getTechnologyInternet()!=null) {
				 ///SI LA TECNOLOGIA ACTUAL ES DISTINTA A FTTH, SE TOMA SOLO LAS OFERTAS EN TECNOLOGIA HFC
				 if(!fixedParkDto.getTechnologyInternet().equals("FTTH")) {
					 for (ProductFixedDTO productFixed : productList) {
						 if (!coverage.equals(productFixed.getInternetTechnology())) {
							 removeList.add(productFixed);
						 }
					 }
				 }
				 ///SI LA TECNOLOGIA FTTH, SE TOMA SOLO LAS OFERTAS EN TECNOLOGIA FTTH
				 /*else {
					 for (ProductFixedDTO productFixed : productList) {
						 if (!productFixed.getInternetTechnology().equals("FTTH")) {
							 removeList.add(productFixed);
						 }
					 }
				 }*/
			 }
	     /**SI LA COBERTURA DE LA ZONA ES FTTH SE EVALUA DEPENDIENDO LA TECNOLOGIA ACTUAL**/	 
		 case "FTTH":
		     	
			 //if(fixedParkDto.getTechnologyInternet()!=null) {
				 /**SI LA TECNOLOGIA ACTUAL ES DISTINTA A HFC, SE TOMA SOLO LAS OFERTAS EN TECNOLOGIA FTTH**/
			//	 if(!fixedParkDto.getTechnologyInternet().equals("HFC")) {
			/*		 for (ProductFixedDTO productFixed : productList) {
						 if (!coverage.equals(productFixed.getInternetTechnology())) {
							 removeList.add(productFixed);
						 }
					 }
					 if (removeList.size()==productList.size()) {
						 removeList.clear();
					 }
				 }*/
				 /**SI LA TECNOLOGIA ACTUAL ES HFC, NO SE MODIFICA LA LISTA DE OFERTAS ACTUALES, LUEGO SE VALIDAN EN 
				  * EquipmentValidationsMt.java EN EL MÉTODO: determinateInternetDestinyTechnology**/
			 //}			 			 
		}
	}
	productList.removeAll(removeList);
    TotalUtil.availableOffersMT(productList, Constant.BE_1069);
    }

    /***
     * Si el cliente excede su límite crediticio móvil no puede adquirir el producto
     * (*Importante).
     * 
     * @param fixedParkDto
     * @param productList
     * @param mobileDevices
     * @param commercialInfo
     */
    private void haveLimitMobileCredit(List<ProductFixedDTO> productList, List<MobileDTO> mobileDevices,
	    CommercialOperationInfo commercialInfo) {

	List<ProductFixedDTO> removeList = new ArrayList<>();
	if (!productList.isEmpty()) {
	    List<CommercialOperation> filteredMobilelist = UtilCollections.getCollectionByType(commercialInfo.getCommercialOpers(),
		    Constant.MOBILE);
	    List<Double> creditLimits = new ArrayList<>();
	    for (int i = 0; i < filteredMobilelist.size(); i++) {
		creditLimits.add(filteredMobilelist.get(i).getCreditData().getCreditLimit());
	    }

	    double clientCreditLimit = Collections.max(creditLimits);
	    removeFromProductList(productList, clientCreditLimit, removeList);
	}
	TotalUtil.availableOffersMT(productList, Constant.BE_1070);
    }

    /***
     * Método que se encarga de remover elementos de la lista de productos acorde al
     * limite crediticio del cliente.
     * 
     * @param productList
     * @param clientCreditLimit
     * @param removeList
     */
    private void removeFromProductList(List<ProductFixedDTO> productList, double clientCreditLimit, List<ProductFixedDTO> removeList) {
	for (ProductFixedDTO productFixed : productList) {
	    if ( clientCreditLimit < (productFixed.getMobil0Rent() + productFixed.getMobil1Rent())) {
	    	//if (clientCreditLimit < (productFixed.getMobil0Rent() + productFixed.getMobil1Rent()== null ? 0 : productFixed.getMobil1Rent())) {
		removeList.add(productFixed);
	    }
	}
	productList.removeAll(removeList);
    }

    @Override
    public List<ProductFixedDTO> execute(FixedDTO fixedParkDto, List<ProductFixedDTO> productList,
	    List<MobileDTO> mobileDevices, CommercialOperationInfo commercialInfo, boolean provideOrPortability) {
	// TODO Auto-generated method stub
	return null;
    }



    @Override
    public List<ProductFixedDTO> executeLMAFailed(FixedDTO fixedParkDto, List<ProductFixedDTO> productListAll,
	    List<ProductFixedDTO> productList, List<ProductFixedDTO> productLMAList, List<MobileDTO> mobileDevices,
	    CommercialOperationInfo commercialInfo, boolean provideOrPortability) {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public List<ProductFixedDTO> executeLMA(FixedDTO fixedParkDto, List<ProductFixedDTO> productList, List<ProductFixedDTO> productLMAList,
	    List<MobileDTO> mobileDevices, CommercialOperationInfo commercialInfo, boolean provideOrPortability) {
	// TODO Auto-generated method stub
	return null;
    }
}
