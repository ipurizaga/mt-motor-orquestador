package com.telefonica.total.business.offersRules.upSellPriorizationsSupposedPlantMT;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.pojo.req.CommercialOperationInfo;

public interface IUpSellPriorizationsSupposedPlantMT {

    /***
     * Método que se encarga de priorizar las ofertas por BO Y ps , cuando no
     * encuentra el producto actual
     * 
     * @param fixedParkDto
     */
    List<ProductFixedDTO> executeMt(List<ProductFixedDTO> filteredProductList,
	    List<ProductFixedDTO> productsMtList, FixedDTO fixedParkDto, CommercialOperationInfo commercialInfo, List<ProductFixedDTO> actualProducts);
}
