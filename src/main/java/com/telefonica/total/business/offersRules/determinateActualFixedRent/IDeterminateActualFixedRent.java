package com.telefonica.total.business.offersRules.determinateActualFixedRent;

import com.telefonica.total.dto.FixedDTO;

public interface IDeterminateActualFixedRent {

    /***
     * Método que se encarga de calcular la renta fija actual del cliente con el
     * paquete renta y las rentas monoproductos.
     * 
     * @param fixedParkDto
     */
    void execute(FixedDTO fixedParkDto);
}
