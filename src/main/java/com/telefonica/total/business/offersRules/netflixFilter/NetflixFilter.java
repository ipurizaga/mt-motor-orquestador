package com.telefonica.total.business.offersRules.netflixFilter;

import java.util.List;

import org.springframework.stereotype.Service;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.enums.SalesChannelType;
import com.telefonica.total.pojo.req.CommercialOperationInfo;

@Service
public class NetflixFilter implements INetflixFilter {

    @Override
    public void execute(FixedDTO fixedParkDto, List<ProductFixedDTO> filteredProductList, CommercialOperationInfo commercialInfo) {

	for (ProductFixedDTO product : filteredProductList) {
	    if (!(SalesChannelType.O.getCode().equalsIgnoreCase(commercialInfo.getSalesChannelType())
		    || SalesChannelType.OC.getCode().equalsIgnoreCase(commercialInfo.getSalesChannelType())
		    || SalesChannelType.OW.getCode().equalsIgnoreCase(commercialInfo.getSalesChannelType())
		    || SalesChannelType.OD.getCode().equalsIgnoreCase(commercialInfo.getSalesChannelType()))
		    && (product.getPackageRent() == 269 || product.getPackageRent() == 299 || product.getPackageRent() == 199
			    || product.getPackageRent() == 229)) {
		product.setNetflix(null);
	    }
	}
    }
}
