package com.telefonica.total.business.offersRules.equifaxPatch;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.enums.OperationComm;
import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.generic.CommOperCollection;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.CommercialOperationInfo;

@Service
class EquifaxPatch implements IEquifaxPatch{

    @Override
    public void execute(List<MobileDTO> mobileDevices, CommercialOperationInfo commercialInfo) {
	CommOperCollection<CommercialOperation> reqList = new CommOperCollection<>(commercialInfo.getCommercialOpers());
	reqList.filterByProduct(SuscriptionType.MOBILE);
	if (CollectionUtils.isNotEmpty(reqList)) {
	    double rentMonoProd = 0D;
	    for (CommercialOperation commercialOperation : reqList) {
		for (MobileDTO mobile : mobileDevices) {
		    if (commercialOperation.getSubscriber() != null && commercialOperation.getSubscriber().getServiceNumber() != null
			    && mobile.getMobileNumber().equals(commercialOperation.getSubscriber().getServiceNumber())) {
			rentMonoProd = mobile.getMobileRentMonoProduct();
		    }
		}
		determinateComercialOperation(commercialOperation, rentMonoProd);
	    }
	}
    }
    
    private void determinateComercialOperation(CommercialOperation commercialOperation, double rentMonoProd) {
	if (commercialOperation.getSubscriber() != null && ((Constant.ONE_INT == commercialOperation.getSubscriber().getIsRecentProvide()
		&& OperationComm.CAPL.getCodeDesc().equalsIgnoreCase(commercialOperation.getOperation())
		&& Constant.CERO == commercialOperation.getSubscriber().getRentMonoProduct().doubleValue())
		|| (Constant.TWO_INT == commercialOperation.getSubscriber().getIsRecentProvide()
			&& OperationComm.CAPL.getCodeDesc().equalsIgnoreCase(commercialOperation.getOperation())))) {
	    commercialOperation.getCreditData().setCreditLimit(commercialOperation.getCreditData().getCreditLimit() + rentMonoProd);
	}
    }
}
