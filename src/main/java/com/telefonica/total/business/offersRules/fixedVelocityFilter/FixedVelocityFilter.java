package com.telefonica.total.business.offersRules.fixedVelocityFilter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.RulesValueProp;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.pojo.req.Information;

@Service
public class FixedVelocityFilter implements IFixedVelocityFilter{

    @Autowired
    private RulesValueProp rulesValue;
    
    @Override
    public void execute(FixedDTO fixedParkDto, List<ProductFixedDTO> filteredProductList) {
	for (ProductFixedDTO product : filteredProductList) {
	    if (fixedParkDto.getSpeedInternet() >=0D && fixedParkDto.getSpeedInternet() < 50D) {
		
	    } else if (fixedParkDto.getSpeedInternet() >=60D && fixedParkDto.getSpeedInternet() < 100D) {
		
	    } else if (fixedParkDto.getSpeedInternet() >=100D && fixedParkDto.getSpeedInternet() < 120D) {
		
	    } else if (fixedParkDto.getSpeedInternet() >=20D) {
		
	    }
	    if (product.getInternetSpeed() - fixedParkDto.getSpeedInternet() < rulesValue.getLIMIT_FIXED_VELOCITY()) {
		product.getCharacteristics().add(Information.builder().code(rulesValue.getPRODUCT_CHARACTERISTIC())
			.value(rulesValue.getPRODUCT_CHARACTERISTIC_MINOR_VELOCITY()).build());
	    }
	}
    }
}
