package com.telefonica.total.business.offersRules.removeAllExceptActualProductFilter;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.RulesValueProp;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.generic.CommOperCollection;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.Information;

@Service
public class RemoveAllExceptActualProductFilter implements IRemoveAllExceptActualProductFilter{

    @Autowired
    private RulesValueProp rulesValue;
    
    @Override
    public void executeFail(int type, List<ProductFixedDTO> filteredProductList, List<MobileDTO> mobileDevices,
	    List<CommercialOperation> commercialOpeList) {

	if (type == Constant.AVERIA_TIPO3) {
	    CommOperCollection<CommercialOperation> requestList = new CommOperCollection<>(commercialOpeList);
	    requestList.filterByProduct(SuscriptionType.FIXED);
	    String subscriberId = StringUtils.EMPTY;
	    if (requestList.size() == 1) {
		subscriberId = requestList.get(0).getSubscriber().getSubscriberId();
	    }

	    List<ProductFixedDTO> actualProduct = new ArrayList<>();
	    int index = 0;
	    for (int i = 0; i < mobileDevices.size(); i++) {
		if (mobileDevices.get(i).getIsMtm() != null && mobileDevices.get(i).getBoId() != null && mobileDevices.get(i).getIsMtm()) {
		    index = i;
		}
	    }
	    for (ProductFixedDTO product : filteredProductList) {
		if (subscriberId.equals(product.getPackagePs().toString())
			&& mobileDevices.get(index).getBoId().equals(Integer.parseInt(product.getPackageMobilCode()))) {
		    product.getCharacteristics().add(Information.builder().code(rulesValue.getPRODUCT_CHARACTERISTIC())
			    .value(rulesValue.getPRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT()).build());
		    actualProduct.add(product);
		    break;
		}
	    }
	    filteredProductList.clear();
	    filteredProductList.addAll(actualProduct);
	}
    }
}
