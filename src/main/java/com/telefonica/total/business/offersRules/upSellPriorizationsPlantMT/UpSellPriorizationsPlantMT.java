package com.telefonica.total.business.offersRules.upSellPriorizationsPlantMT;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.business.offersRules.productsRules.IProductsRules;
import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.RulesValueProp;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.enums.OperationDevice;
import com.telefonica.total.exception.BusinessException;
import com.telefonica.total.generic.CommOperCollection;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.CommercialOperationInfo;
import com.telefonica.total.pojo.req.Information;

@Service
public class UpSellPriorizationsPlantMT implements IUpSellPriorizationsPlantMT{
    
    @Autowired
    private RulesValueProp rulesValue;
    
    @Autowired
    private IProductsRules productRules;
    
    @Override
    public List<ProductFixedDTO> executeMt(List<ProductFixedDTO> filteredProductList,
	    ProductFixedDTO inhabilitatedActualOffer, List<ProductFixedDTO> catalogProductFixedDto, 
	    FixedDTO fixedParkDto, CommercialOperationInfo commercialInfo, List<ProductFixedDTO> actualProducts) {

	//List<ProductFixedDTO> actualProducts = new ArrayList<>();
	List<ProductFixedDTO> productsWithoutActual = new ArrayList<>();
	List<ProductFixedDTO> priorizatedProducts = new ArrayList<>();
	productsWithoutActual.addAll(filteredProductList);
	if (inhabilitatedActualOffer != null) {
	    /* oferta antigua como producto actual - Migracion parrilla nueva */
	    newGrillMigration(filteredProductList, inhabilitatedActualOffer, catalogProductFixedDto, fixedParkDto, actualProducts,
		    productsWithoutActual, priorizatedProducts, commercialInfo);
	} else {
	    /* cuando venga oferta nueva actual - Flujo normal */
	    regularPriorizationMT(productsWithoutActual, filteredProductList, actualProducts, priorizatedProducts, fixedParkDto);
	    
	}
	return priorizatedProducts;
    }

    private void regularPriorizationMT(List<ProductFixedDTO> productsWithoutActual, List<ProductFixedDTO> filteredProductList,
	    List<ProductFixedDTO> actualProducts, List<ProductFixedDTO> priorizatedProducts, FixedDTO fixedParkDto) {
    	
	removeActualProductFromPriorizations(productsWithoutActual, filteredProductList, actualProducts, fixedParkDto);
	Collections.sort(productsWithoutActual, ProductFixedDTO.rentComparator);
	iteratePriorizationsRegularFlux(priorizatedProducts, productsWithoutActual, actualProducts);
	
	//Quitando pr0ducto actual de Gestion de planta parrilla actual
	//priorizatedProducts.add(0, actualProducts.get(0));
    }

    private void newGrillMigration(List<ProductFixedDTO> filteredProductList, ProductFixedDTO inhabilitatedActualOffer,
	    List<ProductFixedDTO> catalogProductFixedDto, FixedDTO fixedParkDto, List<ProductFixedDTO> actualProducts,
	    List<ProductFixedDTO> productsWithoutActual, List<ProductFixedDTO> priorizatedProducts, CommercialOperationInfo commercialInfo) {
	// hallar oferta espejo
	//ProductFixedDTO product = TotalUtil.getActualMirrorOfferMT(filteredProductList, catalogProductFixedDto, fixedParkDto);
	ProductFixedDTO product = new ProductFixedDTO();
	
	if(fixedParkDto.getQuantityMobile() == 1) {
	    product = findCurrentOffer1Mobile(commercialInfo);
	}else {
	    
	    if(commercialInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
	    
		if(commercialInfo.getCommercialOpers().get(2).getSubscriber().getBoId()!=null) {
		    product = findCurrentOffer2Mobile(commercialInfo);
		}else {
		    product = findCurrentOffer1Mobile(commercialInfo);
		}
	    
	    }else {
		product = findCurrentOffer1Mobile(commercialInfo);
	    }
	}
	
	product.setCoverageRquest(fixedParkDto.getCoverage());
		product.setCob_hfc(fixedParkDto.getHfcCobInternet());
		product.setTypeF(fixedParkDto.getTypeF());
		product.setTypeM1(fixedParkDto.getTypeM1());
		product.setTypeM2(fixedParkDto.getTypeM2());
		product.setDeviceOperationM1(fixedParkDto.getDeviceOperationM1());
		product.setDeviceOperationM2(fixedParkDto.getDeviceOperationM2());
		product.setQuantityMobile(fixedParkDto.getQuantityMobile());
		
		if(product.getPackagePs().equals(inhabilitatedActualOffer.getPackagePs())) {
		    product.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
		}
	
	// agregarla mirror ofer como producto actual
	actualProducts.add(product);
	//actualProducts.add(inhabilitatedActualOffer);
	// remover la oferta espejo de los productos actuales
	removeMirrorOfferFromFilteredList(product, productsWithoutActual);
	// ordenar los productos sin el actual mirror
	Collections.sort(productsWithoutActual, ProductFixedDTO.rentComparator);
	// añadir como primero el mirror
	productsWithoutActual.add(0, product);
	// agregar las priorizaciones
	iteratePriorizationsMigration(priorizatedProducts, productsWithoutActual, actualProducts);
	// agregar el actual inhabilitado
	
	if(!(product.getPackagePs().equals(inhabilitatedActualOffer.getPackagePs()))) {
	
	    priorizatedProducts.add(0, inhabilitatedActualOffer);
	
	}else {
	
	boolean existeActual = false;
	
	if(!(fixedParkDto.getQuantityMobile() == 1)) {
	    
	    for (ProductFixedDTO productFixedDTOT : filteredProductList) {
		
		for (Information info : productFixedDTOT.getCharacteristics()) {
		    
		    if(info.getCode().equals(rulesValue.PRODUCT_CHARACTERISTIC) && info.getValue().equals(rulesValue.PRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT)) {
			
			existeActual = true;
			
			break;
			
		    }
		    
		}
		
		if(existeActual) {
		    break;
		}
		
	    }
	    
	    if(!existeActual) {
	    
        	    for (ProductFixedDTO productFixedDTOTP : priorizatedProducts) {
        		
        		for (Information infoP : productFixedDTOTP.getCharacteristics()) {
        		    
        		    if(infoP.getCode().equals(rulesValue.PRODUCT_CHARACTERISTIC) && infoP.getValue().equals(rulesValue.PRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT)) {
        			
        			existeActual = true;
        			
        			break;
        			
        		    }
        		    
        		}
        		
        		if(existeActual) {
        		    break;
        		}
        		
        	    }
	    
	    }
	    
	}
	
		if(!existeActual) {
		    filteredProductList.add(0, inhabilitatedActualOffer);
		}
	
	}
	
	
    }

    private void removeMirrorOfferFromFilteredList(ProductFixedDTO productMirror, List<ProductFixedDTO> productsWithoutActual) {
	List<ProductFixedDTO> toRemove = new ArrayList<>();
	for (ProductFixedDTO product : productsWithoutActual) {
	    if (product.getMirrorOffer().equals(productMirror.getMirrorOffer())) {
		toRemove.add(product);
		break;
	    }
	}
	productsWithoutActual.removeAll(toRemove);
    }

    private void iteratePriorizationsMigration(List<ProductFixedDTO> priorizatedProducts, List<ProductFixedDTO> productsWithoutActual,
	    List<ProductFixedDTO> actualProducts) {
	int iterator = 0;
	
	boolean prioridad = false;
	
	if(actualProducts.get(0).getCob_hfc()==null) {
	    actualProducts.get(0).setCob_hfc('0');
	}
	
	for (ProductFixedDTO fixProd : productsWithoutActual) {
	    if (iterator > 0) {
		break;
	    }
	    //if (fixProd.getPackageRent() >= actualProducts.get(0).getPackageRent()) {
	    
	    if(actualProducts.get(0).getCoverageRquest().equals("HFC") && !(actualProducts.get(0).getCob_hfc().toString().equals("2"))) {
		
		
		
		if(actualProducts.get(0).getTypeF() != null) {
        		if(actualProducts.get(0).getTypeF().equals(Constant.TYPE_MTF)) {
        		    	if(actualProducts.get(0).getQuantityMobile() == 1) {
                    		    	if (fixProd.getPackageRent() > actualProducts.get(0).getPackageRent() &&  fixProd.getInternetSpeed() > actualProducts.get(0).getInternetSpeed()) {
                				fixProd.setPriority(String.valueOf(1));
                        			priorizatedProducts.add(fixProd);
                        			iterator++;
                        			prioridad = true;
            				}
        		    	}else {
        		    
                			if(actualProducts.get(0).getTypeM1()!=null && actualProducts.get(0).getTypeM2()==null) {
                			    
                			    if((actualProducts.get(0).getTypeM1().equals(Constant.TYPE_MTM)) && 
                				    (actualProducts.get(0).getDeviceOperationM2().equals(OperationDevice.ACTIVATION.getCodeDesc()) ||
                					    actualProducts.get(0).getDeviceOperationM2().equals(OperationDevice.PORTABILITY.getCodeDesc()) )) {
                				if (fixProd.getPackageRent() > actualProducts.get(0).getPackageRent() &&  fixProd.getInternetSpeed() > actualProducts.get(0).getInternetSpeed()) {
                        				fixProd.setPriority(String.valueOf(1));
                                			priorizatedProducts.add(fixProd);
                                			iterator++;
                                			prioridad = true;
                				}
                			    }else if((actualProducts.get(0).getTypeM1().equals(Constant.TYPE_MTM))) {
                				
                				
                                		if (fixProd.getPackageRent() > actualProducts.get(0).getPackageRent() &&  fixProd.getInternetSpeed() > actualProducts.get(0).getInternetSpeed()) {
                                			fixProd.setPriority(String.valueOf(1));
                                			priorizatedProducts.add(fixProd);
                                			iterator++;
                                			prioridad = true;
                                			
                                		}	
                			    }
                			    
                			    
                			    
                			}
                			    
                			if(actualProducts.get(0).getTypeM1()!=null && actualProducts.get(0).getTypeM2()!=null) {
                			    if(actualProducts.get(0).getTypeM1().equals(Constant.TYPE_MTM) && actualProducts.get(0).getTypeM2().equals(Constant.TYPE_MTM)) {
                    				if (fixProd.getPackageRent() > actualProducts.get(0).getPackageRent() &&  fixProd.getInternetSpeed() > actualProducts.get(0).getInternetSpeed()) {
                                			fixProd.setPriority(String.valueOf(1));
                                			priorizatedProducts.add(fixProd); 
                                			iterator++;
                                			prioridad = true;
                    				}
                			    }
                			    
                			    if(actualProducts.get(0).getTypeM1().equals(Constant.TYPE_MTM) && actualProducts.get(0).getTypeM2().equals(Constant.TYPE_LMA)) {
                				if(actualProducts.get(0).getDeviceOperationM2().equals(OperationDevice.CAEQ.getCodeDesc())) {
                        				if (fixProd.getPackageRent() > actualProducts.get(0).getPackageRent() &&  fixProd.getInternetSpeed() > actualProducts.get(0).getInternetSpeed()) {
                                        			fixProd.setPriority(String.valueOf(1));
                                        			priorizatedProducts.add(fixProd);    
                                        			iterator++;
                                        			prioridad = true;
                        				}
                				
                				}
                			    }
                			    
                			    if(actualProducts.get(0).getTypeM1().equals(Constant.TYPE_LMA) && actualProducts.get(0).getTypeM2().equals(Constant.TYPE_MTM)) {
                				if(actualProducts.get(0).getDeviceOperationM2().equals(OperationDevice.CAEQ.getCodeDesc())) {
                        				if (fixProd.getPackageRent() > actualProducts.get(0).getPackageRent() &&  fixProd.getInternetSpeed() > actualProducts.get(0).getInternetSpeed()) {
                                        			fixProd.setPriority(String.valueOf(1));
                                        			priorizatedProducts.add(fixProd);         
                                        			iterator++;
                                        			prioridad = true;
                        				}
                				}
                			    }	
                			}
        		    	}
        		 }else {
        		     if (fixProd.getPackageRent() >= actualProducts.get(0).getPackageRent() ) {
        				fixProd.setPriority(String.valueOf(1));
        				priorizatedProducts.add(fixProd);
        				iterator++;
        				prioridad = true;
        				
        		     }
        		 }
		}
	    }else {
		if (fixProd.getPackageRent() >= actualProducts.get(0).getPackageRent() ) {
			fixProd.setPriority(String.valueOf(1));
			priorizatedProducts.add(fixProd);
			iterator++;
			prioridad = true;
			
		}
	    }	    
	    
	    /*else if (fixProd.getPackageRent() > actualProducts.get(0).getPackageRent()) {
		fixProd.setPriority(String.valueOf(iterator + 1));
		priorizatedProducts.add(fixProd);
		iterator++;
	    }*/
	}
	
	Double paqueteRenta = 0.0;
	    Integer velocidadInternet = 0;
	    
	    boolean prioridad2 = false;
	    
	     if(!prioridad) {
		 
		 for (ProductFixedDTO fixProdPriority : productsWithoutActual) {
		     
		     if (fixProdPriority.getPackageRent() > paqueteRenta &&  fixProdPriority.getInternetSpeed() > velocidadInternet) {
			 
			 paqueteRenta = fixProdPriority.getPackageRent();
			 velocidadInternet = fixProdPriority.getInternetSpeed();
			 
		     }
		     
		 }
		 
		 for (ProductFixedDTO fixProdPriority : productsWithoutActual) {
		     
		     
		     if (fixProdPriority.getPackageRent() == paqueteRenta &&  fixProdPriority.getInternetSpeed() == velocidadInternet) {
			 if (!(paqueteRenta == actualProducts.get(0).getPackageRent() &&  velocidadInternet == actualProducts.get(0).getInternetSpeed())) {
        			 	fixProdPriority.setPriority(String.valueOf(1));
        				priorizatedProducts.add(fixProdPriority);
        				prioridad2 = true;
			 }
			 
		     }
		     
		 }
		 
	     }  	     
	     
	     ProductFixedDTO priority = new ProductFixedDTO();
	     
	     if(!prioridad2 && !prioridad) {
		 for (ProductFixedDTO fixProdPriority : productsWithoutActual) {
		     if (!(fixProdPriority.getPackageRent() == actualProducts.get(0).getPackageRent() &&  fixProdPriority.getInternetSpeed() == actualProducts.get(0).getInternetSpeed())) {
			 if ((fixProdPriority.getPackageRent() < actualProducts.get(0).getPackageRent() &&  fixProdPriority.getInternetSpeed() < actualProducts.get(0).getInternetSpeed())) {
			     priority = fixProdPriority;
			 }else {
			     break;
			 }
		     }
		 }
		 
		 for (ProductFixedDTO fixProdPriority : productsWithoutActual) {
		     
		     if (fixProdPriority.getPackageRent() == priority.getPackageRent() &&  fixProdPriority.getInternetSpeed() == priority.getInternetSpeed()) {
			 fixProdPriority.setFlagMTDownsellPriority(1);
			 fixProdPriority.setPriority(String.valueOf(1));
			 priorizatedProducts.add(fixProdPriority);
		     }
		 }
		 
	     }
    }
    
    public static final Comparator<ProductFixedDTO> rentComparator = new Comparator<ProductFixedDTO>() {
	@Override
	public int compare(ProductFixedDTO pf1, ProductFixedDTO pf2) {
	    return pf2.getPackageRent().compareTo(pf1.getPackageRent());
	}
    };
    
    private void iteratePriorizationsRegularFlux(List<ProductFixedDTO> priorizatedProducts, List<ProductFixedDTO> productsWithoutActual,
	    List<ProductFixedDTO> actualProducts) {
	int iterator = 0;
	for (ProductFixedDTO fixProd : productsWithoutActual) {
	    if (iterator > 0) {
		break;
	    }
	    //if (fixProd.getPackageRent() >= actualProducts.get(0).getPackageRent()) {
	    /*if(actualProducts.get(0).getCoverageRquest().equals("HFC") && !(actualProducts.get(0).getCob_hfc().toString().equals("2"))) {
		fixProd.setPriority(String.valueOf(iterator + 1));
		priorizatedProducts.add(fixProd);
		iterator++;
	    }*/
	    
	    if(actualProducts.get(0).getCoverageRquest().equals("HFC") && !(actualProducts.get(0).getCob_hfc().toString().equals("2"))) {
		
		
		
		if(actualProducts.get(0).getTypeF() != null) {
        		if(actualProducts.get(0).getTypeF().equals(Constant.TYPE_MTF)) {
        		    	if(actualProducts.get(0).getQuantityMobile() == 1) {
                    		    	if (fixProd.getPackageRent() >= actualProducts.get(0).getPackageRent() &&  fixProd.getInternetSpeed() >= actualProducts.get(0).getInternetSpeed()) {
                				fixProd.setPriority(String.valueOf(1));
                        			priorizatedProducts.add(fixProd);
            				}
        		    	}else {
        		    
                			if(actualProducts.get(0).getTypeM1()!=null && actualProducts.get(0).getTypeM2()==null) {
                			    
                			    if((actualProducts.get(0).getTypeM1().equals(Constant.TYPE_MTM)) && 
                				    (actualProducts.get(0).getDeviceOperationM2().equals(OperationDevice.ACTIVATION.getCodeDesc()) ||
                					    actualProducts.get(0).getDeviceOperationM2().equals(OperationDevice.PORTABILITY.getCodeDesc()) )) {
                				if (fixProd.getPackageRent() >= actualProducts.get(0).getPackageRent() &&  fixProd.getInternetSpeed() >= actualProducts.get(0).getInternetSpeed()) {
                        				fixProd.setPriority(String.valueOf(1));
                                			priorizatedProducts.add(fixProd);
                				}
                			    }else if((actualProducts.get(0).getTypeM1().equals(Constant.TYPE_MTM))) {
                				
                				
                                		if (fixProd.getPackageRent() >= actualProducts.get(0).getPackageRent() &&  fixProd.getInternetSpeed() >= actualProducts.get(0).getInternetSpeed()) {
                                			fixProd.setPriority(String.valueOf(1));
                                			priorizatedProducts.add(fixProd);
                                			
                                		}	
                			    }
                			    
                			    
                			    
                			}
                			    
                			if(actualProducts.get(0).getTypeM1()!=null && actualProducts.get(0).getTypeM2()!=null) {
                			    if(actualProducts.get(0).getTypeM1().equals(Constant.TYPE_MTM) && actualProducts.get(0).getTypeM2().equals(Constant.TYPE_MTM)) {
                    				if (fixProd.getPackageRent() >= actualProducts.get(0).getPackageRent() &&  fixProd.getInternetSpeed() >= actualProducts.get(0).getInternetSpeed()) {
                                			fixProd.setPriority(String.valueOf(1));
                                			priorizatedProducts.add(fixProd);                        			
                    				}
                			    }
                			    
                			    if(actualProducts.get(0).getTypeM1().equals(Constant.TYPE_MTM) && actualProducts.get(0).getTypeM2().equals(Constant.TYPE_LMA)) {
                				if(actualProducts.get(0).getDeviceOperationM2().equals(OperationDevice.CAEQ.getCodeDesc())) {
                        				if (fixProd.getPackageRent() >= actualProducts.get(0).getPackageRent() &&  fixProd.getInternetSpeed() >= actualProducts.get(0).getInternetSpeed()) {
                                        			fixProd.setPriority(String.valueOf(1));
                                        			priorizatedProducts.add(fixProd);                    			
                        				}
                				
                				}
                			    }
                			    
                			    if(actualProducts.get(0).getTypeM1().equals(Constant.TYPE_LMA) && actualProducts.get(0).getTypeM2().equals(Constant.TYPE_MTM)) {
                				if(actualProducts.get(0).getDeviceOperationM2().equals(OperationDevice.CAEQ.getCodeDesc())) {
                        				if (fixProd.getPackageRent() >= actualProducts.get(0).getPackageRent() &&  fixProd.getInternetSpeed() >= actualProducts.get(0).getInternetSpeed()) {
                                        			fixProd.setPriority(String.valueOf(1));
                                        			priorizatedProducts.add(fixProd);                    			
                        				}
                				}
                			    }	
                			}
        		    	}
        		 }else {
        		     if (fixProd.getPackageRent() >= actualProducts.get(0).getPackageRent() ) {
        				fixProd.setPriority(String.valueOf(1));
        				priorizatedProducts.add(fixProd);
        				
        		     }
        		 }
		}
	    }else {
		if (fixProd.getPackageRent() >= actualProducts.get(0).getPackageRent() ) {
			fixProd.setPriority(String.valueOf(1));
			priorizatedProducts.add(fixProd);
			
		}
	    }
	}
    }
    
    public ProductFixedDTO findCurrentOffer2Mobile(CommercialOperationInfo commercialInfo) {

	CommOperCollection<CommercialOperation> commOperCollection = new CommOperCollection<>(commercialInfo.getCommercialOpers());
	
	List<ProductFixedDTO> catalogProductFixedDto = productRules.getProductFixed();
	
	ProductFixedDTO prod_current = new ProductFixedDTO();
	
	boolean no_existe = true;
	
	for (ProductFixedDTO productFixedDTO : catalogProductFixedDto) {
	    
	    if(productFixedDTO.getPackagePs().equals(Integer.parseInt(commOperCollection.get(0).getSubscriber().getSubscriberId()))) {
		
		//System.out.println("chau");
		
		if(commOperCollection.get(1).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO.getPackageMobilCode()))) {
		    prod_current = productFixedDTO;
		    no_existe = false;
		    
		}else if (commOperCollection.get(2).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO.getPackageMobilCode()))) {
		    
		    prod_current = productFixedDTO;
		    no_existe = false;
		}
		
	    }
	    
	}
	
	
	if(no_existe) {
	    if (commOperCollection.get(1).getSubscriber().getBoId().equals(commOperCollection.get(2).getSubscriber().getBoId())) {
        	    for (ProductFixedDTO productFixedDTO2 : catalogProductFixedDto) {
        		if(commOperCollection.get(1).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO2.getPackageMobilCode()))) {
        		    prod_current = productFixedDTO2;
        		    no_existe = false;
        		}
        	    }
	    
	    }else{
		for (ProductFixedDTO productFixedDTO3 : catalogProductFixedDto) {
        		if(commOperCollection.get(1).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO3.getPackageMobilCode()))) {
        		    prod_current = productFixedDTO3;
        		    no_existe = false;
        		    
        		}else if (commOperCollection.get(2).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO3.getPackageMobilCode()))) {
        		    
        		    prod_current = productFixedDTO3;
        		    no_existe = false;
        		}
		}
	    }
	}
	
	if(no_existe) {
	    throw new BusinessException(Constant.BE_2023);
	}
	
	//System.out.println("hola");
	
	return prod_current;
	
    }
    
    public ProductFixedDTO findCurrentOffer1Mobile(CommercialOperationInfo commercialInfo) {

	
	
	CommOperCollection<CommercialOperation> commOperCollection = new CommOperCollection<>(commercialInfo.getCommercialOpers());
	
	ProductFixedDTO prod_current = new ProductFixedDTO();
		
	List<ProductFixedDTO> catalogProductFixedDto = productRules.getProductFixed();
	
	List<ProductFixedDTO> catalogProductFixedDtoTemp =  productRules.getDisabledOffers(catalogProductFixedDto);
	
	String parrilla = "";
	
	for (ProductFixedDTO productFixedDTO : catalogProductFixedDto) {
	    
	    if(commOperCollection.get(1).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO.getPackageMobilCode()))) {
		
		parrilla = productFixedDTO.getMerchandisingType();
		break;		
	    }
	    
	}
	
	boolean existe=false;
	
	for (ProductFixedDTO productFixedDTO2 : catalogProductFixedDto) {
	
	    	if(parrilla.equals("P1") || parrilla.equals("P2")) {	    	
	    
        	    	if(productFixedDTO2.getPackagePs().equals(Integer.parseInt(commOperCollection.get(0).getSubscriber().getSubscriberId()))
        	    		&& productFixedDTO2.getMerchandisingType().equals(parrilla)) {
        		
        	    	    //System.out.println("chau");		
        	    	    
        	    	    productFixedDTO2.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
        	    	    prod_current = productFixedDTO2;
        		    
        		    existe = true;
        		    break;
        		}
	    	
		}else if(parrilla.equals("P3") /*&& productFixedDTO2.getEnabled().equals(Constant.ONE)*/) {
		    
		    ProductFixedDTO ps_antiguo = new ProductFixedDTO();
		    
		    for (ProductFixedDTO productFixedDTO3 : catalogProductFixedDtoTemp) {
			
			if(productFixedDTO3.getMerchandisingType().equals(parrilla) && productFixedDTO3.getPackagePs().equals(Integer.parseInt(commOperCollection.get(0).getSubscriber().getSubscriberId()))) {
			    ps_antiguo =  productFixedDTO3;
			}
			
			//break;
			
		    }
		    
		    if(ps_antiguo == null) {
		    
        		    if(productFixedDTO2.getPackagePs().equals(Integer.parseInt(commOperCollection.get(0).getSubscriber().getSubscriberId()))
            	    		&& productFixedDTO2.getMerchandisingType().equals(parrilla)) {
            		
            	    	    //System.out.println("chau");		
            	    	    
                	    	    productFixedDTO2.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
                	    	    prod_current = productFixedDTO2;
                		    
                		    existe = true;
                		    break;
                		}
		    }else {
			
			if(productFixedDTO2.getInternetSpeed().equals(ps_antiguo.getInternetSpeed())) {
			    productFixedDTO2.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
        	    	    prod_current = productFixedDTO2;
        		    
        		    existe = true;
        		    break;
			}
		    }
	    	}
	}
	
	if(!existe) {
	    throw new BusinessException(Constant.BE_1057);
	}
	
	//System.out.println("hola");
	return prod_current;
	
	
    }
    
    private void removeActualProductFromPriorizations(List<ProductFixedDTO> productsWithoutActual,
	    List<ProductFixedDTO> filteredProductList, List<ProductFixedDTO> actualProduct, FixedDTO fixedParkDto) {

	search: for (ProductFixedDTO product : filteredProductList) {
	    if (product.getCharacteristics() != null && CollectionUtils.isNotEmpty(product.getCharacteristics())) {
		for (Information information : product.getCharacteristics()) {
		    if (information.getValue().equals(rulesValue.getPRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT())) {
			product.setCoverageRquest(fixedParkDto.getCoverage());
				product.setCob_hfc(fixedParkDto.getHfcCobInternet());
				product.setTypeF(fixedParkDto.getTypeF());
				product.setTypeM1(fixedParkDto.getTypeM1());
				product.setTypeM2(fixedParkDto.getTypeM2());
				product.setDeviceOperationM1(fixedParkDto.getDeviceOperationM1());
				product.setDeviceOperationM2(fixedParkDto.getDeviceOperationM2());
				product.setQuantityMobile(fixedParkDto.getQuantityMobile());
			actualProduct.add(product);
			productsWithoutActual.remove(product);
			break search;
		    }
		}
	    }
	}
	if (CollectionUtils.isEmpty(actualProduct)) {
	    throw new BusinessException(Constant.BE_1062);
	}

    }
}
