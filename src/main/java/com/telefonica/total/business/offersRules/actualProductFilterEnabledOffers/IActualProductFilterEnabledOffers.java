package com.telefonica.total.business.offersRules.actualProductFilterEnabledOffers;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.pojo.req.CommercialOperationInfo;

public interface IActualProductFilterEnabledOffers {

    /***
     * Método que se encarga de buscar el producto actual en las ofertas actuales
     * que se encuentran habilitadas.
     * 
     * @param fixedParkDto
     * @param filteredProductList
     */
    void executeMt(FixedDTO fixedParkDto, List<ProductFixedDTO> filteredProductList, CommercialOperationInfo commercialInfo);
}
