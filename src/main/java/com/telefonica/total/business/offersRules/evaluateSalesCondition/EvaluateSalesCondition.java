package com.telefonica.total.business.offersRules.evaluateSalesCondition;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import com.telefonica.total.dto.FinancingSaleCondition;
import com.telefonica.total.dto.FourthDigitFinanceDTO;
import com.telefonica.total.dto.PartialRequestDTO;
import com.telefonica.total.dto.PostPaidEasyDTO;
import com.telefonica.total.dto.ProductFixedDTO;

@Service
public class EvaluateSalesCondition implements IEvaluateSalesCondition{

    @Override
    public void execute(PartialRequestDTO partial, List<FourthDigitFinanceDTO> getFinancialPercentage) {
	for (ProductFixedDTO product : partial.getResultantProductFixedList()) {
	    for (FourthDigitFinanceDTO financial : getFinancialPercentage) {
		if (CollectionUtils.isEmpty(product.getPostPaidEasy())) {
		    assignFinancingCondition(financial.getCommercialOperationId(), product, financial.getPercentage());
		} else {
		    Long commOperationId = financial.getCommercialOperationId();
		    if (!havePPFCondition(product, commOperationId)) {
			assignFinancingCondition(financial.getCommercialOperationId(), product, financial.getPercentage());
		    }
		}
	    }
	}
    }
    
    private Boolean havePPFCondition(ProductFixedDTO product, Long financialId) {
	boolean value = false;
	for (PostPaidEasyDTO postpaid : product.getPostPaidEasy()) {
	    value |= Boolean.TRUE.toString().equals(postpaid.getValue()) && postpaid.getCommercialOperationId().equals(financialId);
	}
	return value;
    }

    private void assignFinancingCondition(Long commercialOpId, ProductFixedDTO product, Integer percentage) {
	FinancingSaleCondition saleCondition = new FinancingSaleCondition();
	saleCondition.setCommercialOperationId(commercialOpId);
	saleCondition.setName("financiamiento");
	saleCondition.setValue(percentage);
	product.getFinancingSaleCondition().add(saleCondition);
    }
}
