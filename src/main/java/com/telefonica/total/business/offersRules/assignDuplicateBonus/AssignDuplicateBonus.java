package com.telefonica.total.business.offersRules.assignDuplicateBonus;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.generic.CommOperCollection;
import com.telefonica.total.pojo.req.CommercialOperation;

@Service
public class AssignDuplicateBonus implements IAssignDuplicateBonus {

    public void execute(FixedDTO fixedParkDto, List<ProductFixedDTO> filteredProductList, boolean movileProvideOrPortability,
	    List<CommercialOperation> commercialOpeList) {
	List<Integer> positions = new ArrayList<>();
	CommOperCollection<CommercialOperation> opeComProviPort = new CommOperCollection<>(commercialOpeList);
	opeComProviPort.filterByProduct(SuscriptionType.MOBILE);
	for (int i = 0; i < opeComProviPort.size(); i++) {
	    positions.add(i+1);
	}
	fixedParkDto.setCommOperProvidePortaPositions(positions);
    }
}
