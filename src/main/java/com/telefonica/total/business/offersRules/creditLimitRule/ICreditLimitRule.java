package com.telefonica.total.business.offersRules.creditLimitRule;

import java.util.List;

import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.pojo.req.CommercialOperationInfo;
import com.telefonica.total.pojo.req.Customer;
import com.telefonica.total.pojo.req.Offer;

public interface ICreditLimitRule {

    void executeSa(List<ProductFixedDTO> productList, CommercialOperationInfo commercialInfo);
    void executeSaLMA(List<ProductFixedDTO> productList, CommercialOperationInfo commercialInfo, Customer customer, Offer offer);
}
