package com.telefonica.total.business.offersRules.determinateTotalRentByOffer;

import java.util.List;

import org.springframework.stereotype.Service;

import com.telefonica.total.dto.ProductFixedDTO;

@Service
public class DeterminateTotalRentByOffer implements IDeterminateTotalRentByOffer{

    @Override
    public void execute(List<ProductFixedDTO> priorizationList) {
	for (ProductFixedDTO product : priorizationList) {
	    double packageRent = product.getPackageRent() != null ? product.getPackageRent() : 0;
	    double blocksTokeepRent = product.getAditionalBlocksTokeepRent() != null ? product.getAditionalBlocksTokeepRent() : 0;
	    double aditionalsToKeepSumatory = product.getAditionalRentsToKeepSummatory() != null
		    ? product.getAditionalRentsToKeepSummatory()
		    : 0;
	    double productDiscount = product.getEmployeeDiscount() != null ? product.getEmployeeDiscount() : 0;
	    product.setRentByAditionals(blocksTokeepRent + aditionalsToKeepSumatory);
	    //product.setOfferTotalRent(packageRent + blocksTokeepRent + aditionalsToKeepSumatory - productDiscount);
	    product.setOfferTotalRent(packageRent + blocksTokeepRent + aditionalsToKeepSumatory);
	}
    }
}
