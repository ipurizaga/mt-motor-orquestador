package com.telefonica.total.business.offersRules.upSellPriorizationsPlantMT;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.pojo.req.CommercialOperationInfo;

public interface IUpSellPriorizationsPlantMT {

    /***
     * Priorizacion del flujo de altaMT donde se priorizan solo productos que sean
     * upsell y ademas si tenemos producto actual de las ofertas inhabilitadas se
     * coloca esta en primera posición.
     * 
     * @param filteredProductList
     * @param inhabilitatedActualOffer
     * @param catalogProductFixedDto
     * @param fixedParkDto
     * @return
     */
    List<ProductFixedDTO> executeMt(List<ProductFixedDTO> filteredProductList, ProductFixedDTO inhabilitatedActualOffer,
	    List<ProductFixedDTO> catalogProductFixedDto, FixedDTO fixedParkDto, CommercialOperationInfo commercialInfo, List<ProductFixedDTO> actualProducts);
}
