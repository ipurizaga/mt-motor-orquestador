package com.telefonica.total.business.offersRules.determinateAdditionalBlocksToKeep;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;

public interface IDeterminateAdditionalBlocksToKeep {

    /***
     * Método que se determinar los bloques de canales que se deben mantener segun
     * las ofertas presentadas en movistar total, con la finalidad de que el cliente
     * no pierda los canales peremiun con los que cuenta actualmente.
     * 
     * @param priorizationList
     * @param fixedParkDto
     */
    void execute(List<ProductFixedDTO> priorizationList, FixedDTO fixedParkDto);

}
