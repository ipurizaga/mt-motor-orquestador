package com.telefonica.total.business.offersRules.determinateActualFixedRent;

import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.dto.FixedDTO;

@Service
public class DeterminateActualFixedRent implements IDeterminateActualFixedRent{

    @Override
    public void execute(FixedDTO fixedParkDto) {
	//System.out.println("Hola");
	fixedParkDto.setActualFixedRent(fixedParkDto.getPackageRent() - Math.abs(fixedParkDto.getPermanentDiscount())
		+ TotalUtil.getNumericValueOf(fixedParkDto.getPresentLine()) * fixedParkDto.getRentMonoProdLine()
		+ TotalUtil.getNumericValueOf(fixedParkDto.getPresentInternet()) * fixedParkDto.getRentMonoProdInternet()
		+ TotalUtil.getNumericValueOf(fixedParkDto.getCablePresent()) * fixedParkDto.getCableRentMonoProd());
    }
}
