package com.telefonica.total.business.offersRules.comparisonoffer;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.ComparativeConstant;
import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.common.util.UtilCollections;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.enums.OperationComm;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.CommercialOperationInfo;
import com.telefonica.total.pojo.req.Subscription;

@Service
public class CompareComponentsOffers implements ICompareComponentsOffers {
    
   

    @Override
    public void execute(FixedDTO fixedParkDto, List<ProductFixedDTO> filteredProductList, List<MobileDTO> mobileDevices,
	    CommercialOperationInfo commercialInfo) {
	
	List<CommercialOperation> filteredMobilelist = UtilCollections.getCollectionByType(commercialInfo.getCommercialOpers(),
		Constant.MOBILE);

	for (ProductFixedDTO productFixedDTO : filteredProductList) {
	    comparePossesionFixedInternet(productFixedDTO,fixedParkDto);
	    comparePossesionCable(productFixedDTO, fixedParkDto);
	    comparePossesionBlockHD(productFixedDTO, fixedParkDto);
	    compareFixedInternetVelocity(productFixedDTO, fixedParkDto);

	    Double roaming1MT = (productFixedDTO.getMobil0Roaming() != null) ? productFixedDTO.getMobil0Roaming() : Constant.CERO;
	    Double roaming2MT = (productFixedDTO.getMobil1Roaming() != null) ? productFixedDTO.getMobil1Roaming() : Constant.CERO;
	    Double roamingMT = roaming1MT;
	    Integer dataQuantityMT = TotalUtil.getNumericValueOf(productFixedDTO.getMobil0QuantityData());
	    Double roamingOfferMt = roaming1MT + roaming2MT;
	    Integer dataQuantityOfferMt = TotalUtil.getNumericValueOf(productFixedDTO.getMobil0QuantityData()) +
		    			  TotalUtil.getNumericValueOf(productFixedDTO.getMobil1QuantityData())	;
	    if (mobileDevices.size() == 1) {
		dataQuantityMT = dataQuantityOfferMt;
		roamingMT = roamingOfferMt;
	    } 

	    int number = 1;
	    int cantCapl = 0;
	    Integer dataQuantityTotal = Constant.CERO_INT;
	    Double roamingTotal = Constant.CERO;
	    for (int i = 0; i < mobileDevices.size(); i++) {
		MobileDTO mobileDTO = mobileDevices.get(i);
		Integer hasMobile = (TotalUtil.getNumericValueOf(mobileDTO.getMobileQuantityData()) > 0) ? Constant.ONE_INT
			: Constant.CERO_INT;
		productFixedDTO.getComparativeComponents().addAll(TotalUtil.compareComponent(hasMobile, Constant.ONE_INT,
			String.format(ComparativeConstant.DATOS_MOVILES_TENENCIA, number)));

		CommercialOperation opeCom = filteredMobilelist.get(i);
		Subscription suscriber = opeCom.getSubscriber();

		Integer dataQuantity = TotalUtil.getNumericValueOf(mobileDTO.getMobileQuantityData());
		Double dataRoaming = Double.valueOf(TotalUtil.getNumericValueOf(mobileDTO.getMobileCdtMbRoaming()));

		
		if (suscriber != null && suscriber.getIsRecentProvide() == 2) {
		    dataQuantity = TotalUtil.divideByThousand(dataQuantity);
		    dataRoaming = BigDecimal.valueOf(dataRoaming / 1000d).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue();
		}
		
		dataQuantityTotal += dataQuantity ;
		roamingTotal += dataRoaming;
		     productFixedDTO.getComparativeComponents().addAll(TotalUtil.compareComponent(dataQuantity,
			TotalUtil.divideByThousand(dataQuantityMT), String.format(ComparativeConstant.DATOS_MOVILES_DATOS, number)));
		     productFixedDTO.getComparativeComponents().addAll(
			TotalUtil.compareComponentD(dataRoaming, roamingMT, String.format(ComparativeConstant.DATOS_MOVILES_ROAM, number)));
		number++;

		if (OperationComm.CAPL.getCodeDesc().equalsIgnoreCase(opeCom.getOperation())) {
		    cantCapl++;
		}
	    }
	    
	    productFixedDTO.getComparativeComponents().addAll(TotalUtil.compareComponent(dataQuantityTotal,
			TotalUtil.divideByThousand(dataQuantityOfferMt), ComparativeConstant.DATOS_MOVILES_DATOS_TOTAL));
	    productFixedDTO.getComparativeComponents().addAll(
			TotalUtil.compareComponentD(roamingTotal, roamingOfferMt, ComparativeConstant.DATOS_MOVILES_ROAM_TOTAL));
	    
	    productFixedDTO.getComparativeComponents()
		    .addAll(TotalUtil.compareComponent(cantCapl, Constant.TWO_INT, ComparativeConstant.CANTIDAD_LINEAS));
	    
//	    productFixedDTO.getComparativeComponents().
//	    	    add(Information.builder().code(ComparativeConstant.DATOS_MOVIL_ILIM).
//	    		    value(String.valueOf(TotalUtil.getNumericValueOf(productFixedDTO.getUnlimitedMovil()))).build());
	}
    }
    
    private void comparePossesionFixedInternet(ProductFixedDTO productFixedDTO, FixedDTO fixedParkDto) {
   	productFixedDTO.getComparativeComponents()
   	    .addAll(TotalUtil.compareComponent(TotalUtil.getNumericValueOf(fixedParkDto.getPresentInternet()),
   		    TotalUtil.getNumericValueOf(productFixedDTO.getInternetPresent()), ComparativeConstant.INTERNET_FIJO));
    }
    
    private void comparePossesionCable(ProductFixedDTO productFixedDTO, FixedDTO fixedParkDto) {
	 productFixedDTO.getComparativeComponents()
	    .addAll(TotalUtil.compareComponent(TotalUtil.getNumericValueOf(fixedParkDto.getCablePresent()),
		    TotalUtil.getNumericValueOf(productFixedDTO.getCablePresent()), ComparativeConstant.TELEVISION_TENENCIA));
    }
    
    private void comparePossesionBlockHD(ProductFixedDTO productFixedDTO, FixedDTO fixedParkDto) {
	productFixedDTO.getComparativeComponents()
	    .addAll(TotalUtil.compareComponent(TotalUtil.getNumericValueOf(fixedParkDto.getCablePresentPremHd()),
		    TotalUtil.getNumericValueOf(productFixedDTO.getPremiumHdPresent()), ComparativeConstant.BLOQUEHD_TENENCIA));
    }
    
    private void compareFixedInternetVelocity(ProductFixedDTO productFixedDTO, FixedDTO fixedParkDto) {
	  productFixedDTO.getComparativeComponents()
	    .addAll(TotalUtil.compareComponent(TotalUtil.divideByThousand(fixedParkDto.getSpeedInternet()),
		    TotalUtil.divideByThousand(productFixedDTO.getInternetSpeed()), ComparativeConstant.INTERNET_VELOCIDAD));
    }
    
}
