package com.telefonica.total.business.offersRules.removeRuleByRentFromProductOffers;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;

@Service
@Qualifier("totalization")
public class RemoveRule implements IRemoveRule{

    public void execute(List<ProductFixedDTO> filteredProductList, FixedDTO fixedParkDto) {
	TotalUtil.removeRuleByRentFromProductOffers(filteredProductList, 359);
	
//	if(!fixedParkDto.getOnlyOneMobileIndicator()) {
//	    TotalUtil.removeRuleByRentFromProductOffers(filteredProductList, 199);
//	    TotalUtil.removeRuleByRentFromProductOffers(filteredProductList, 229);
//	}
    }

    @Override
    public void execute(List<ProductFixedDTO> priorizationList, List<ProductFixedDTO> filteredProductList) {
	// TODO Auto-generated method stub
	
    }
    
}
