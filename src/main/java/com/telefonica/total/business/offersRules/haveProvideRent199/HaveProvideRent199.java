package com.telefonica.total.business.offersRules.haveProvideRent199;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.RulesValueProp;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;

@Service
public class HaveProvideRent199 implements IHaveProvideRent199{

    @Autowired
    private RulesValueProp rulesValue;
    
    
    
    @Override
    public void execute(List<ProductFixedDTO> productList, FixedDTO fixedParkDto) {
	if (fixedParkDto.getPresentARPA() > rulesValue.getRENTA199_ARPA_LIMIT()) {
	    removeRuleByRentFromProductOffers(productList, 199);
	}
	TotalUtil.availableOffersMT(productList, Constant.BE_1055);
    }
    
    public void removeRuleByRentFromProductOffers(List<ProductFixedDTO> filteredProductList, int rent) {
	List<ProductFixedDTO> removeList = new ArrayList<>();
	removeRule(filteredProductList, removeList, rent);
	filteredProductList.removeAll(removeList);
    }
    
    private void removeRule(List<ProductFixedDTO> productList, List<ProductFixedDTO> removeList, int rent) {
	for (ProductFixedDTO product : productList) {
	    if (product.getPackageRent() == rent) {
		removeList.add(product);
	    }
	}
    }
}
