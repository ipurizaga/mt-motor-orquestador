package com.telefonica.total.business.offersRules.productsRules;

import java.util.List;

import com.telefonica.total.dto.BillingProductDTO;
import com.telefonica.total.dto.FourthDigitDTO;
import com.telefonica.total.dto.PartialRequestDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.generic.BillProdCatalogCollection;
import com.telefonica.total.pojo.req.CommercialOperationInfo;
import com.telefonica.total.pojo.req.Offer;

/**
 * 
 * @Author: jomapozo.
 * @Datecreation: 2 nov. 2018 10:51:19
 * @FileName: IProductsRules.java
 * @AuthorCompany: Telefonica
 * @version: 0.1
 * @Description: Rules for Catalog CATALOG_PROD_FIJO and
 *               CATALOG_BILLOFF_PRODOFF;
 */
public interface IProductsRules {

    /***
     * Método que se encarga de filtrar los Billing offers - product offers con los
     * datos enviados desde el request(DealerCode, Department, SalesChannel)
     * 
     * @param partial
     * @param commercialInfo
     * @return
     */
	//Se añade variable flux , para validación de mensaje error si es LMA o flujo MT en caso no se encuentren ofertas
    BillProdCatalogCollection<BillingProductDTO> obtainFilteredListBoPo(PartialRequestDTO partial, CommercialOperationInfo commercialInfo, int flux);

    BillProdCatalogCollection<BillingProductDTO> getLstMobile(List<Offer> plans);
    
    BillProdCatalogCollection<BillingProductDTO> getLstMobileLMA(List<Offer> plans);

    List<ProductFixedDTO> getProductFixed();
    
    List<ProductFixedDTO> getProductFixedLMA();

    List<ProductFixedDTO> assignBillingOfferClientInterested(List<ProductFixedDTO> riskFilteredList, List<Offer> optionalOffers);

    List<ProductFixedDTO> assignBillingOfferClientInterestedLMA(List<ProductFixedDTO> riskFilteredList, List<Offer> optionalOffers);
    
    List<ProductFixedDTO> assignBillingOfferProductFixed(PartialRequestDTO partial, String scoreDigits);

    /** Agregando la variable productType para poder filtrar la asignación si es movil **/
    void assignBillingProductValues(PartialRequestDTO partial,
	    BillProdCatalogCollection<BillingProductDTO> boPoListFromFilteredFixedProducts, String productType);

    void assignPpfCondition(Long commercialOpId, ProductFixedDTO product, Boolean bool);
    
    void assignPpfConditionFalse(Long commercialOpId, ProductFixedDTO product, Boolean bool);


    Boolean evaluateCondition(int planRank, FourthDigitDTO objDigit);

    /***
     * Método que se encarga de filtar los productos que se encuentran habilitados.
     * 
     * @param catalogProductFixedDto
     * @return
     */
    List<ProductFixedDTO> getEnabledOffers(List<ProductFixedDTO> catalogProductFixedDto);
    
    /***
     * Método que se encarga de filtar los productos que se encuentran inhabilitados.
     * 
     * @param catalogProductFixedDto
     * @return
     */
    List<ProductFixedDTO> getDisabledOffers(List<ProductFixedDTO> catalogProductFixedDto);
    
    /***
     * Método que se encarga de filtar los productos que se encuentran en LMA.
     * 
     * @param catalogProductFixedDto
     * @return
     */    
    List<ProductFixedDTO> getEnabledOffersLMA(List<ProductFixedDTO> catalogProductFixedDto);
    
    void determinatePenaltyByPlanRank(PartialRequestDTO partial);

    List<ProductFixedDTO> assignBillingOfferClientInterestedLMA(PartialRequestDTO partial, List<ProductFixedDTO> riskFilteredList,
	    List<Offer> optionalOffers);
}
