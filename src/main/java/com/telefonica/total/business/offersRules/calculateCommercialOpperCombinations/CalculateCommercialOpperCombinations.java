package com.telefonica.total.business.offersRules.calculateCommercialOpperCombinations;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.telefonica.total.business.offersRules.catalogAndClientValidations.ICatalogAndClientValidations;
import com.telefonica.total.business.offersRules.productsRules.IProductsRules;
import com.telefonica.total.dto.BeanMapper;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.generic.CommOperCollection;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.CommercialOperationInfo;

public class CalculateCommercialOpperCombinations implements ICalculateCommercialOpperCombinations {

    @Autowired
    private BeanMapper mapper;

    @Autowired
    private IProductsRules productRules;

    @Autowired
    @Qualifier("totalization")
    private ICatalogAndClientValidations haveCreditLimit;

    @Override
    public void execute(CommercialOperationInfo commercialInfo, List<CommercialOperationInfo> commercialInfoList) {

	List<CommercialOperation> commercialOpeList = commercialInfo.getCommercialOpers();
	List<ProductFixedDTO> catalogProductFixedDto = productRules.getProductFixed();
	List<ProductFixedDTO> enabledOffers = productRules.getEnabledOffers(catalogProductFixedDto);
	CommOperCollection<CommercialOperation> fixedOperations = new CommOperCollection<>(commercialOpeList);
	fixedOperations.filterByProduct(SuscriptionType.FIXED);
	CommOperCollection<CommercialOperation> mobileOperations = new CommOperCollection<>(commercialOpeList);
	mobileOperations.filterByProduct(SuscriptionType.MOBILE);
	if (fixedOperations.size() == 1 && mobileOperations.size() == 2) {
	    // 3 combinatorias
	    commercialInfoList.add(commercialInfo);
	    for (int i = 0; i < 2; i++) {
		if (haveLimitMobileCredit(enabledOffers, mobileOperations.get(i))) {
		    CommercialOperationInfo cloneObject = mapper.commOperInfoToCloneCommOper(commercialInfo);
		    cloneObject.getCommercialOpers().clear();
		    cloneObject.getCommercialOpers().add(fixedOperations.get(0));
		    cloneObject.getCommercialOpers().add(mobileOperations.get(i));
		    commercialInfoList.add(cloneObject);
		}

	    }

	} else if (fixedOperations.size() == 1 && mobileOperations.size() == 1) {
	    // flujo de siempre
	    commercialInfoList.add(commercialInfo);
	}
    }
    
    
    /***
     * Si el cliente excede su límite crediticio móvil no puede adquirir el producto
     * (*Importante).
     * 
     * @param fixedParkDto
     * @param productList
     * @param mobileDevices
     * @param commercialInfo
     */
    public Boolean haveLimitMobileCredit(List<ProductFixedDTO> productList, CommercialOperation movilOperation) {
	List<ProductFixedDTO> removeList = new ArrayList<>();
	List<ProductFixedDTO> productListRemove = new ArrayList<>();
	productListRemove.addAll(productList);
	if (!productList.isEmpty()) {
	    double clientCreditLimit = movilOperation.getCreditData().getCreditLimit();
	    removeFromProductList(productListRemove, clientCreditLimit, removeList);
	}
	if (productListRemove.isEmpty()) {
	    return Boolean.FALSE;
	}
	return Boolean.TRUE;
    }
    
    /***
     * Método que se encarga de remover elementos de la lista de productos acorde al
     * limite crediticio del cliente.
     * 
     * @param productList
     * @param clientCreditLimit
     * @param removeList
     */
    private void removeFromProductList(List<ProductFixedDTO> productList, double clientCreditLimit, List<ProductFixedDTO> removeList) {
	for (ProductFixedDTO productFixed : productList) {
	    if (clientCreditLimit < (productFixed.getMobil0Rent() + productFixed.getMobil1Rent())) {
		removeList.add(productFixed);
	    }
	}
	productList.removeAll(removeList);
    }

}
