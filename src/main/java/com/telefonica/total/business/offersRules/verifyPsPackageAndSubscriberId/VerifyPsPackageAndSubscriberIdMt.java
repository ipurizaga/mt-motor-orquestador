package com.telefonica.total.business.offersRules.verifyPsPackageAndSubscriberId;

import java.util.List;

import org.springframework.stereotype.Service;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.generic.CommOperCollection;
import com.telefonica.total.pojo.req.CommercialOperation;

@Service
public class VerifyPsPackageAndSubscriberIdMt implements IVerifyPsPackageAndSubscriberId{
    @Override
    public boolean execute(List<CommercialOperation> commercialOpeList, FixedDTO fixedParkDto,
	    List<ProductFixedDTO> catalogProductFixedDto) {

	CommOperCollection<CommercialOperation> requestList = new CommOperCollection<>(commercialOpeList);
	requestList.filterByProduct(SuscriptionType.FIXED);
	if (requestList.size() == 1) {
	    fixedParkDto.setPsRequest(requestList.get(0).getSubscriber().getSubscriberId());
	}
	if (fixedParkDto.getPackagePs().toString().equals(fixedParkDto.getPsRequest())) {
	    return Boolean.FALSE;
	} else {
	    updateFixedParkWithActualProductInfo(fixedParkDto, catalogProductFixedDto, fixedParkDto.getPsRequest());
	    return Boolean.TRUE;
	}
    }
    
    /***
     * Método con el que se asignan los valores del producto con ps actual al parque
     * fijo.
     * 
     * @param fixedParkDto
     * @param catalogProductFixedDto
     * @param subscriberId
     */
    private void updateFixedParkWithActualProductInfo(FixedDTO fixedParkDto, List<ProductFixedDTO> catalogProductFixedDto,
	    String subscriberId) {

	for (ProductFixedDTO product : catalogProductFixedDto) {
	    if (product.getPackagePs().toString().equals(subscriberId)) {
		fixedParkDto.setPackagePs(product.getPackagePs());
		fixedParkDto.setSpeedInternet(product.getInternetSpeed());
		fixedParkDto.setCableType(product.getCableType());
		fixedParkDto.setPackageRent(product.getPackageRent());
		fixedParkDto.setPermanentDiscount(0D);
		break;
	    }
	}
    }
}
