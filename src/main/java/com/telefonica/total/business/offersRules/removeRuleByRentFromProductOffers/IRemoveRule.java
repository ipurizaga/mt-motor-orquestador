package com.telefonica.total.business.offersRules.removeRuleByRentFromProductOffers;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;

public interface IRemoveRule {

    /***
     * Método que se encarga de remover el producto indicado segun renta.
     * 
     * @param filteredProductList
     * @param rent
     */
    void execute(List<ProductFixedDTO> filteredProductList, FixedDTO fixedParkDto);

    /***
     * Método que se encarga de remover el producto indicado segun renta.
     * 
     * @param filteredProductList
     * @param rent
     */
    void execute(List<ProductFixedDTO> priorizationList, List<ProductFixedDTO> filteredProductList);
}
