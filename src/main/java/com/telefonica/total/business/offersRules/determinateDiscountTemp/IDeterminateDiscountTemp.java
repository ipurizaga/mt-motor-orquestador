package com.telefonica.total.business.offersRules.determinateDiscountTemp;

import java.util.List;

import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.pojo.req.CommercialOperationInfo;
import com.telefonica.total.pojo.req.Customer;

public interface IDeterminateDiscountTemp {

    /***
     * Metodo que se encargar de calcular el descuento temporal por cada oferta
     * 
     * @param commercialOperationInfo
     * @param priorizationList
     */
    void execute(CommercialOperationInfo commercialOperationInfo, Customer customer,
	    List<ProductFixedDTO> priorizationList, boolean existeUpFront);    
}
