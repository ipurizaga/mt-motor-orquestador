package com.telefonica.total.business.offersRules.determinatequantitymobile;

import java.util.List;

import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.pojo.req.CommercialOperation;

public interface IDeterminateQuantityMobile {

    void execute(List<CommercialOperation> listComercialOperation);
    void executeMt(List<CommercialOperation> listComercialOperation, List<ProductFixedDTO> enabledOffers);
    void executeFail(List<CommercialOperation> listComercialOperation,Integer flowType, List<ProductFixedDTO> enabledOffers);
}
