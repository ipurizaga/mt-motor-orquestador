package com.telefonica.total.business.offersRules.totalizationPriorizations;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.model.ParamMovTotal;
import com.telefonica.total.pojo.req.CommercialOperation;

public interface ITotalizationPriorizations {

    /***
     * Priorizacion del ofertas para Totalizacion(Altas MT)
     * 
     * @param fixedParkDto
     * @param filteredProductList
     * @param commercialOpeList
     * @return
     */
    List<ProductFixedDTO> execute(FixedDTO fixedParkDto, List<ProductFixedDTO> filteredProductList,
	    List<CommercialOperation> commercialOpeList, String coverage, List<ParamMovTotal> lstParam);
}
