package com.telefonica.total.business.offersRules.determinatePresentBonusAndPresentDiscounts;

import java.util.List;

import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.Customer;

public interface IDeterminatePresentBonusAndPresentDiscounts {

    /***
     * Método que se encarga de realizar los calculos para los bonos y descuentos
     * presentes, esto se realiza sólo para las operaciones comerciales moviles que
     * sean cambio de plan.
     * 
     * @param customer
     * @param commercialOpeList
     * @param mobileDevices
     */
    void execute(Customer customer, List<CommercialOperation> commercialOpeList,
	    List<MobileDTO> mobileDevices);
}
