package com.telefonica.total.business.offersRules.obtainActualProductDisabledOffers;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.pojo.req.CommercialOperationInfo;

public interface IObtainActualProductDisabledOffers {

    /***
     * Método que obtienen el producto actual en caso no se encuentre en las ofertas
     * actuales.
     * 
     * @param fixedParkDto
     * @param catalogProductFixedDto
     * @param filteredProductList
     * @return
     */
    ProductFixedDTO executeMt(FixedDTO fixedParkDto, List<ProductFixedDTO> catalogProductFixedDto,
	    List<ProductFixedDTO> filteredProductList, CommercialOperationInfo commercialInfo);
}
