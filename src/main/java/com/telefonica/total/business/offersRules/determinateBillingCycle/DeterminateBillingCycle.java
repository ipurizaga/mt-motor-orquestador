package com.telefonica.total.business.offersRules.determinateBillingCycle;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.enums.OperationComm;
import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.generic.CommOperCollection;
import com.telefonica.total.pojo.req.CommercialOperation;

@Service
public class DeterminateBillingCycle implements IDeterminateBillingCycle {

    @Override
    public void execute(FixedDTO fixedParkDto, List<CommercialOperation> commercialOpeList) {
	CommOperCollection<CommercialOperation> commOperCollection = new CommOperCollection<>(commercialOpeList);
	commOperCollection.filterByProduct(SuscriptionType.FIXED);
	if (commOperCollection.get(0).getOperation().equalsIgnoreCase(OperationComm.PROVIDE.getCodeDesc())) {
	    Calendar calendar = Calendar.getInstance();
	    calendar.set(Calendar.YEAR, 2021);
    	    // Month. 0 is January, 11 is November
	    //calendar.set(Calendar.MONTH, Calendar.MAY);
	    //calendar.set(Calendar.DAY_OF_MONTH, 13);
	    calendar.setTime(new Date());
	    calendar.add(Calendar.DAY_OF_MONTH, 6);
	    int day = calendar.getTime().getDate();
	    if (day >= 2 && day <= 18)
		fixedParkDto.setCycle(18);
	    else
		fixedParkDto.setCycle(01);
	}
    }

}
