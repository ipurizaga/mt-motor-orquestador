package com.telefonica.total.business.offersRules.financialPercentage;

import java.util.List;

import com.telefonica.total.dto.FourthDigitFinanceDTO;
import com.telefonica.total.pojo.req.CommercialOperation;

public interface IFinancialPercentage {

    List<FourthDigitFinanceDTO> execute(List<CommercialOperation> commercialOperations);
}
