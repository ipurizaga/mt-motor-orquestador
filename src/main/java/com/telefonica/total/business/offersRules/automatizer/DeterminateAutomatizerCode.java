package com.telefonica.total.business.offersRules.automatizer;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.common.util.UtilCollections;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.exception.BusinessException;
import com.telefonica.total.jdbc.PlanDeviceEntity;
import com.telefonica.total.jdbc.PlanFinancingDao;
import com.telefonica.total.jdbc.UpFrontAutomatizerDao;
import com.telefonica.total.jdbc.UpFrontAutomatizerFinancedEntity;
import com.telefonica.total.jdbc.UpFrontAutomatizerCountedEntity;
import com.telefonica.total.model.CatalogAutomatizer;
import com.telefonica.total.model.ParamMovTotal;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.CommercialOperationInfo;
import com.telefonica.total.pojo.req.Customer;
import com.telefonica.total.pojo.req.Information;
import com.telefonica.total.repository.CatalogAutomatizerRepo;
import com.telefonica.total.repository.ParamMovTotalRepo;

@Service
public class DeterminateAutomatizerCode implements IDeterminateAutomatizerCode {

    @Autowired
    private CatalogAutomatizerRepo catalogAutomatizerRepo;
    
    @Autowired
    private UpFrontAutomatizerDao     upFrontAutoDao;
    
	@Autowired
    private ParamMovTotalRepo paramMovTotalRepo;

    @Override
    public void execute(CommercialOperationInfo commercialOperationInfo, Customer customer, List<ProductFixedDTO> priorizationList) {

	List<UpFrontAutomatizerCountedEntity> upFrontAutomatizerCountedLst = new ArrayList<UpFrontAutomatizerCountedEntity>();
	
	List<UpFrontAutomatizerFinancedEntity> upFrontAutomatizerFinancedLst = new ArrayList<UpFrontAutomatizerFinancedEntity>();
	
	Boolean validaUpfront = false;
	String campaignValue = null;
	String campaignCode = null;
	ParamMovTotal campaignDefault = new ParamMovTotal();
	Information respValUpFrn = new Information("","");
	/*try {
	    
	    if(Integer.parseInt(commercialOperationInfo.getAdditionalOperationInformation().get(0).getValue()) == 1) {
		upFrontAutomatizerCountedLst = upFrontAutoDao.getUpFrontAutomatizerCounted("provide", "R", "MOVISTAR TOTAL", 23321, 235.00
        		    , "Modem GPON", "2 Decos HD",Integer.parseInt(commercialOperationInfo.getAdditionalOperationInformation().get(0).getValue()));
		System.out.println(upFrontAutomatizerCountedLst.get(0).getProductCode());
	    }else if (Integer.parseInt(commercialOperationInfo.getAdditionalOperationInformation().get(0).getValue()) == 2){
		upFrontAutomatizerFinancedLst = upFrontAutoDao.getUpFrontAutomatizerFinanced("provide", "R", "MOVISTAR TOTAL", 23321, 235.00
			    , "Modem GPON", "2 Decos HD",Integer.parseInt(commercialOperationInfo.getAdditionalOperationInformation().get(0).getValue()));
		System.out.println(upFrontAutomatizerFinancedLst.get(0).getProductCode());
	    }
		
	}catch(Exception e) {
	    e.getStackTrace();
		//throw new BusinessException("2031");
	}*/
	
	
        	CommercialOperation comFija = UtilCollections.getCollectionByType(commercialOperationInfo.getCommercialOpers(), Constant.FIX)
        		.get(0);
        
        	//List<CatalogAutomatizer> automatizerCodes = catalogAutomatizerRepo.findAll();
        	for (ProductFixedDTO productFixedDTO : priorizationList) {
        	    //for (CatalogAutomatizer catalogAutomatizer : automatizerCodes) {
//        		if (validateOpeComm(catalogAutomatizer, comFija, commercialOperationInfo)
//        		    && validateOffers(catalogAutomatizer,productFixedDTO)
//        		    && validateCustomer(catalogAutomatizer, customer)) {
//        		    productFixedDTO.setCodeAutomatizer(catalogAutomatizer.getProductCode());
//        		    break;
//        		}
        	    //}
        		/** Validacion Upfront - Playas **/
        		
        		List<ParamMovTotal> campaniaOfertas = new ArrayList<ParamMovTotal>();
    	    	campaniaOfertas = paramMovTotalRepo.findByGrupoParam(Constant.DESC_TEMP_CAMP);
    	    	campaignDefault = TotalUtil.campaniaDefault(campaniaOfertas);
    	    	
        	    if(commercialOperationInfo.getAdditionalOperationInformation()!=null) {
        		/*
        	    	for(Information info : commercialOperationInfo.getAdditionalOperationInformation()) {
        	    		if(info.getCode().equals(Constant.UPFRONT)) {
        	    			campaignValue = info.getValue();
        	    			validaUpfront = true;
        	    			break;
        	    		}
        	    	}
        	    	
        	    	for(Information info : commercialOperationInfo.getAdditionalOperationInformation()) {
        	    		if(info.getCode().equals(Constant.PRODUCTO_PLAYA)) {
        	    			if(!validaUpfront) {
            	    			if(info.getValue() != null) {
            	    				campaignValue = "2";
            	    			}            	    			
        	    			}
        	    		}
        	    	}
        		*/
        	    	
        	    	respValUpFrn = TotalUtil.validationUpfront(commercialOperationInfo.getAdditionalOperationInformation(), campaniaOfertas);
        	    	campaignValue = respValUpFrn.getValue();
        	    	campaignCode = respValUpFrn.getCode();
        	    	
        	    	/*System.out.println("Operacion: " + comFija.getOperation());
    		    	System.out.println("CustomerType: " + customer.getCustomerType());
    		    	System.out.println("Campaña: " + campaignCode);
    		    	System.out.println("PackagePs: " + productFixedDTO.getPackagePs());
    		    	System.out.println("PackageRent: " + productFixedDTO.getPackageRent());
    		    	System.out.println("ModemEquipment: " + productFixedDTO.getModemEquipment());
    		    	System.out.println("DecoEquipment: " + productFixedDTO.getDecoEquipment());
    		    	System.out.println("AdditionalInformationValue: " +Integer.parseInt(campaignValue));*/
            		    //if(Integer.parseInt(commercialOperationInfo.getAdditionalOperationInformation().get(0).getValue()) == 1) {
            		    if(Integer.parseInt(campaignValue) == 1) {
            		    	
            			try {
            			upFrontAutomatizerCountedLst = upFrontAutoDao.getUpFrontAutomatizerCounted(comFija.getOperation(), customer.getCustomerType(), campaignCode
                    		    , productFixedDTO.getPackagePs(), productFixedDTO.getPackageRent(), productFixedDTO.getModemEquipment(), productFixedDTO.getDecoEquipment(), Integer.parseInt(campaignValue));
                    			productFixedDTO.setPaymentModality(upFrontAutomatizerCountedLst.get(0).getPaymentModality());
                    			productFixedDTO.setPeriodByMonth(upFrontAutomatizerCountedLst.get(0).getPeriodByMonth());
                    			productFixedDTO.setPromotionalRent(upFrontAutomatizerCountedLst.get(0).getPromotionalRent());
                    			productFixedDTO.setDiscount(upFrontAutomatizerCountedLst.get(0).getDiscount());
                    			productFixedDTO.setAmountCounted(upFrontAutomatizerCountedLst.get(0).getAmountCounted());
                    			productFixedDTO.setCodeAutomatizer(upFrontAutomatizerCountedLst.get(0).getProductCode());
            			}catch(Exception e) {
            				System.out.println("No se obtuvo o no se encontro Codigo Automatizador / " + "Campaign -> "+ campaignCode + " / CampaignValue -> " + campaignValue);
                			productFixedDTO.setCodeAutomatizer(null);
            			}
            			
            			
                	    }else if (Integer.parseInt(campaignValue) == 2){
                		try {
                        		upFrontAutomatizerFinancedLst = upFrontAutoDao.getUpFrontAutomatizerFinanced(comFija.getOperation(), customer.getCustomerType(), campaignCode
                            		    , productFixedDTO.getPackagePs(), productFixedDTO.getPackageRent(), productFixedDTO.getModemEquipment(), productFixedDTO.getDecoEquipment(), Integer.parseInt(campaignValue));	
                        		productFixedDTO.setCodeAutomatizer(upFrontAutomatizerFinancedLst.get(0).getProductCode());
                		}catch(Exception e) {
                			System.out.println("No se obtuvo o no se encontro Codigo Automatizador / " + "Campaign -> "+ campaignCode + " / CampaignValue -> " + campaignValue);
                			productFixedDTO.setCodeAutomatizer(null);
                		}
                		                		
                	    }
        	    } else {
        		try {
                		upFrontAutomatizerFinancedLst = upFrontAutoDao.getUpFrontAutomatizerFinanced(comFija.getOperation(), customer.getCustomerType(), campaignDefault.getCol1()
                        		    , productFixedDTO.getPackagePs(), productFixedDTO.getPackageRent(), productFixedDTO.getModemEquipment(), productFixedDTO.getDecoEquipment(), Integer.parseInt(campaignDefault.getValor()));
                    	productFixedDTO.setCodeAutomatizer(upFrontAutomatizerFinancedLst.get(0).getProductCode());
        		}catch(Exception e) {
        			System.out.println("No se obtuvo o no se encontro Codigo Automatizador / " + "Campaign -> "+ campaignDefault.getCol1() + " / CampaignValue -> " + Integer.parseInt(campaignDefault.getValor()));
        		    	productFixedDTO.setCodeAutomatizer(null);
        		}
        		/*
        		if(upFrontAutomatizerFinancedLst.size()==0 && !validaUpfront) {
        			upFrontAutomatizerFinancedLst = upFrontAutoDao.getUpFrontAutomatizerFinanced(comFija.getOperation(), customer.getCustomerType(), Constant.UPFRONT
                		    , productFixedDTO.getPackagePs(), productFixedDTO.getPackageRent(), productFixedDTO.getModemEquipment(), productFixedDTO.getDecoEquipment(), Constant.TWO_INT);
        			productFixedDTO.setCodeAutomatizer(upFrontAutomatizerFinancedLst.get(0).getProductCode());
        		}*/
        		
        	    } 
        	    
        	    
        	}
	
	

    }

    private Boolean validateOffers(CatalogAutomatizer catalogAuto, ProductFixedDTO productFixed) {
	return  ((catalogAuto.getPsCode() == null || catalogAuto.getPsCode().trim().isEmpty())
		   || (Integer.parseInt(catalogAuto.getPsCode()) == productFixed.getPackagePs().intValue()))
		&& (catalogAuto.getPackageRent() == null  
		   || (catalogAuto.getPackageRent().doubleValue() == productFixed.getPackageRent().doubleValue()))
		&& (catalogAuto.getCampaign() == null || catalogAuto.getCampaign().trim().isEmpty()
		   || (catalogAuto.getCampaign().equals(productFixed.getCampaign())))
		&& (!StringUtils.isNotBlank(catalogAuto.getEquipmmentModem()) || !StringUtils.isNotBlank(productFixed.getModemEquipment()) 
		   || catalogAuto.getEquipmmentModem().equals(productFixed.getModemEquipment()))
		&& (!StringUtils.isNotBlank(catalogAuto.getEquipmentDeco()) || !StringUtils.isNotBlank(productFixed.getDecoEquipment())
		   || catalogAuto.getEquipmentDeco().equals(productFixed.getDecoEquipment()))  ;
    }

    private Boolean validateOpeComm(CatalogAutomatizer catalogAutomatizer, CommercialOperation comFija, CommercialOperationInfo commeOperInfo) {
	return ((catalogAutomatizer.getCommercialOperation() == null || catalogAutomatizer.getCommercialOperation().trim().isEmpty())
   			|| (catalogAutomatizer.getCommercialOperation().equals(comFija.getOperation())))
		&& ((catalogAutomatizer.getChannel() == null || catalogAutomatizer.getChannel().trim().isEmpty())
		    || (commeOperInfo.getSalesChannelGroup() == null || commeOperInfo.getSalesChannelGroup().trim().isEmpty())	
		    || (catalogAutomatizer.getChannel().equals(commeOperInfo.getSalesChannelGroup())));
    }
    
    private Boolean validateCustomer(CatalogAutomatizer catalogAuto, Customer customer) {
	return (!StringUtils.isNotBlank(catalogAuto.getSegment()) || !StringUtils.isNotBlank(customer.getCustomerType())
		    || catalogAuto.getSegment().equals(customer.getCustomerType()))
		&& (!StringUtils.isNotBlank(catalogAuto.getProvince()) || !StringUtils.isNotBlank(customer.getProvince())
		    ||  StringUtils.contains(catalogAuto.getProvince(), customer.getProvince()))
		&& (!StringUtils.isNotBlank(catalogAuto.getDistrict()) || !StringUtils.isNotBlank(customer.getDistrict())
		    || StringUtils.contains(catalogAuto.getDistrict(), customer.getDistrict()));
    }
	    

}
