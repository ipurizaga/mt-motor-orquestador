package com.telefonica.total.business.offersRules.productsRules;

import com.telefonica.total.business.offersRules.productsRules.IProductsRules;
import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.RulesValueProp;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.common.util.UtilCollections;
import com.telefonica.total.configuration.NoLoggeable;
import com.telefonica.total.dto.BeanMapper;
import com.telefonica.total.dto.BillingProductDTO;
import com.telefonica.total.dto.FourthDigitDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.PartialRequestDTO;
import com.telefonica.total.dto.PostPaidEasyDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.exception.BusinessException;
import com.telefonica.total.generic.BillProdCatalogCollection;
import com.telefonica.total.jdbc.BillProdOffDao;
import com.telefonica.total.model.CatalogBillingProduct;
import com.telefonica.total.model.CatalogProductFixed;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.CommercialOperationInfo;
import com.telefonica.total.pojo.req.Information;
import com.telefonica.total.pojo.req.Offer;
import com.telefonica.total.repository.CatalogProdFixedRepo;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductsRules implements IProductsRules {
  @Autowired
  private BillProdOffDao billProd;
  
  @Autowired
  private CatalogProdFixedRepo catProdFixedRepo;
  
  @Autowired
  private RulesValueProp rulesValue;
  
  @Autowired
  private BeanMapper beanMap;
  //Se añade variable flux , para validación de mensaje error si es LMA o flujo MT en caso no se encuentren ofertas
  public BillProdCatalogCollection<BillingProductDTO> obtainFilteredListBoPo(PartialRequestDTO partial, CommercialOperationInfo commercialInfo, int flux) {
    List<BillingProductDTO> responseList = new ArrayList<>();
    List<CatalogBillingProduct> catBillProdList = this.billProd.getAllBillProd();
    searchItemsByBoPoSalesChannelDealerCodeDepartment(commercialInfo, partial, catBillProdList, responseList);
    if (responseList.isEmpty()) {
    	if(flux==1) {
    		throw new BusinessException("1061"); 
    	}
    	else if(flux==2) {
    		throw new BusinessException("1065"); 
    	}
    }
      
    return new BillProdCatalogCollection(responseList);
  }
  
  private void searchItemsByBoPoSalesChannelDealerCodeDepartment(CommercialOperationInfo commercialInfo, PartialRequestDTO partial, List<CatalogBillingProduct> catBillProdList, List<BillingProductDTO> responseList) {
    int count = 0, agregados = 0;
    int cantidadOther = 0;
    //Añadiendo multiples validaciones para solo LMA
    String poCode = partial.getPo_code() != null ? partial.getPo_code():"";
    String saleChannel = partial.getSaleChannel() != null ? partial.getSaleChannel():"";
    String storeDepartment = partial.getStoreDepartment() != null ? partial.getStoreDepartment():"";
    String dealerCode = partial.getDealerCode() != null ? partial.getDealerCode():"";
    String storeId = partial.getStoreId() != null ? partial.getStoreId():"";
    boolean withSaleChannel = saleChannel.equals("") ? false : true, withPoCode = false;
    int sc = 0, sd = 0, dc = 0, si = 0;
    
    List<BillingProductDTO> responseListWithoutPOCODE = new ArrayList<>(), responseListLMA = new ArrayList<>(), responseListPOBO = new ArrayList<>(), responseListOther = new ArrayList<>();
    for (ProductFixedDTO product : partial.getResultantProductFixedList()) {
      for (CatalogBillingProduct object : catBillProdList) {    
	  
	/*if(object.getBoId().equals("4182468")) {
	    System.out.println("hola");
	}*/
	  
        if (partial.getPo_code() == null) {
          if (object.getBoId().equals(product.getPackageMobilCode()) && object.getPoId().equals(product.getPoPackage()) && 
            StringUtils.contains(object.getSalesChannel(), commercialInfo.getSalesChannel()))
            responseListWithoutPOCODE.add(this.beanMap.catalogBillingProductToBillingProductDTO(object)); 
          continue;
        } 
        if (partial.getPo_code().equals(object.getPoCode()) ) {
        	withPoCode = true;
          if (object.getBoId().equals(product.getPackageMobilCode()) && object.getPoId().equals(product.getPoPackage())) {
        	//	  && 
            //StringUtils.contains(object.getSalesChannel(), commercialInfo.getSalesChannel())) {
            //String tipo = "";        	   
            if (commercialInfo.getCommercialOpers().size() == 1) {
              //if (partial.getPo_code().equals(object.getPoCode()) && partial.getSaleChannel().equals(object.getSalesChannel())
              //    	  && partial.getStoreDepartment().equals(object.getDepartment()) && partial.getStoreId().equals(object.getStoreId())) {
            	if (poCode.equals(object.getPoCode())) {
            		//Validación extra para cuando saleChannel es nulo o vacío
            		if (StringUtils.contains(object.getSalesChannel(), !saleChannel.equals("") ? saleChannel : "null")) {
            			
            			if (storeDepartment.equals(object.getDepartment())) {
            				if (dealerCode.equals(object.getDealerCode())) {
            					if (storeId.equals(object.getStoreId())) {
            						si++;
            						if (((CommercialOperation)commercialInfo.getCommercialOpers().get(0)).getSubscriber() == null) {            							
            	                        responseListLMA.add(this.beanMap.catalogBillingProductToBillingProductDTO(object));
            	                      } else if (((CommercialOperation)commercialInfo.getCommercialOpers().get(0)).getSubscriber().getType().equals("movil")) {
            	                        responseListLMA.add(this.beanMap.catalogBillingProductToBillingProductDTO(object));
            	                      } 
            					}else {
            						if (si==0) {
            							dc++;
            							if (((CommercialOperation)commercialInfo.getCommercialOpers().get(0)).getSubscriber() == null) {
                	                        responseListLMA.add(this.beanMap.catalogBillingProductToBillingProductDTO(object));
                	                      } else if (((CommercialOperation)commercialInfo.getCommercialOpers().get(0)).getSubscriber().getType().equals("movil")) {
                	                        responseListLMA.add(this.beanMap.catalogBillingProductToBillingProductDTO(object));
                	                      }
            						}
            					}
            				}else {
            					if (dc==0 && si==0) {
        							sd++;
        							if (((CommercialOperation)commercialInfo.getCommercialOpers().get(0)).getSubscriber() == null) {
            	                        responseListLMA.add(this.beanMap.catalogBillingProductToBillingProductDTO(object));
            	                      } else if (((CommercialOperation)commercialInfo.getCommercialOpers().get(0)).getSubscriber().getType().equals("movil")) {
            	                        responseListLMA.add(this.beanMap.catalogBillingProductToBillingProductDTO(object));
            	                      }
        						}
            				}            				
            			}else {
            				if (sd==0 && dc==0 && si==0) {
    							sc++;
    							if (((CommercialOperation)commercialInfo.getCommercialOpers().get(0)).getSubscriber() == null) {
        	                        responseListLMA.add(this.beanMap.catalogBillingProductToBillingProductDTO(object));
        	                      } else if (((CommercialOperation)commercialInfo.getCommercialOpers().get(0)).getSubscriber().getType().equals("movil")) {
        	                        responseListLMA.add(this.beanMap.catalogBillingProductToBillingProductDTO(object));
        	                      }
    						}
            			}
            			
            		}else {
            			/*if (sd==0 && dc==0 && si==0 && sc==0) {
							if (((CommercialOperation)commercialInfo.getCommercialOpers().get(0)).getSubscriber() == null) {
    	                        responseListLMA.add(this.beanMap.catalogBillingProductToBillingProductDTO(object));
    	                      } else if (((CommercialOperation)commercialInfo.getCommercialOpers().get(0)).getSubscriber().getType().equals("movil")) {
    	                        responseListLMA.add(this.beanMap.catalogBillingProductToBillingProductDTO(object));
    	                      }
						}*/
            			//Mensaje de error cuando SaleChannel es vacío o nulo
            			if (saleChannel.equals(""))
            			throw new BusinessException("1082");
            		}
            		
            		 
            	}
            	else {
                  responseListPOBO.add(this.beanMap.catalogBillingProductToBillingProductDTO(object));
                }  
              //}                
            } 
            else {
              responseListPOBO.add(this.beanMap.catalogBillingProductToBillingProductDTO(object));
            } 
            agregados++;
            //continue;
          }
          else {
          	responseListOther.add(this.beanMap.catalogBillingProductToBillingProductDTO(object)); 
        	  cantidadOther++;
          }
          
        } 
        //else {
        	//responseListOther.add(this.beanMap.catalogBillingProductToBillingProductDTO(object)); 
        //}
      	  
        
        
      } 
      //System.out.println(" agregados: " + agregados);
    } 
    
    if (!withSaleChannel && withPoCode) {
    	throw new BusinessException("1082");
    }
    
    int valida = 0;
    //int cantidadWithoutPOCODE = responseListWithoutPOCODE.size(), cantidadLMA = responseListLMA.size(), cantidadPOBO = responseListPOBO.size(), cantidadOther = responseListOther.size();
    int cantidadWithoutPOCODE = responseListWithoutPOCODE.size(), cantidadLMA = responseListLMA.size(), cantidadPOBO = responseListPOBO.size();
    if (cantidadLMA > 0) {
      responseList.addAll(responseListLMA);
      valida = 1;
    } 
    if (cantidadPOBO > 0 && 
      valida != 1) {
      responseList.addAll(responseListPOBO);
      valida = 2;
    } 
    if (cantidadOther > 0 && 
      valida != 2 && valida != 1) {
    	for (CatalogBillingProduct object : catBillProdList) {
    		if (partial.getPo_code().equals(object.getPoCode())) {
    			responseListOther.add(this.beanMap.catalogBillingProductToBillingProductDTO(object));
    		}
    	}
      responseList.addAll(responseListOther);
      valida = 3;
    } 
    if (cantidadWithoutPOCODE > 0 && 
      valida != 3 && valida != 2 && valida != 1)
      responseList.addAll(responseListWithoutPOCODE); 
    
    /*List<CommercialOperation> filteredMobilelist = UtilCollections.getCollectionByType(commercialInfo.getCommercialOpers(),
	    Constant.MOBILE);
    
    List<Double> creditLimits = new ArrayList<>();
    
    for (CommercialOperation commercialOperation : filteredMobilelist) {
	creditLimits.add(commercialOperation.getCreditData().getCreditLimit());
    }
    
    double clientCreditLimit = Collections.max(creditLimits);
    
    for (BillingProductDTO billingProductDTO : responseList) {
	
	if (clientCreditLimit < (Double.valueOf(billingProductDTO.getBoPlankRank().intValue()))) {
	    throw new BusinessException(Constant.BE_1081);
	}
	
    }*/
  }
  
  public BillProdCatalogCollection<BillingProductDTO> getLstMobile(List<Offer> plans) {
    BillProdCatalogCollection<BillingProductDTO> products = new BillProdCatalogCollection();
    return products;
  }
  
  public BillProdCatalogCollection<BillingProductDTO> getLstMobileLMA(List<Offer> plans) {
    BillProdCatalogCollection<BillingProductDTO> products = new BillProdCatalogCollection();
    List<CatalogBillingProduct> lp = this.billProd.getAllBillProd();
    List<BillingProductDTO> bp = new ArrayList<>();
    for (CatalogBillingProduct list : lp)
      bp.add(this.beanMap.catalogBillingProductToBillingProductDTO(list)); 
    for (BillingProductDTO billingProductDTO : bp) {
    	if(plans.get(0).getPoCode().equals(billingProductDTO.getPoCode())) {
  	      products.add(billingProductDTO); 
    	}
    }

    return products;
  }
  
  public List<ProductFixedDTO> getProductFixed() {
    List<CatalogProductFixed> fixedList = this.catProdFixedRepo.findAll();
    if (!fixedList.isEmpty())
      return getOffersByPackageType(this.beanMap.productFixedToDTOs(fixedList), "4Play"); 
    return new ArrayList<>();
  }
  
  public List<ProductFixedDTO> getProductFixedLMA() {
    List<CatalogProductFixed> fixedList = this.catProdFixedRepo.findAll();
    List<ProductFixedDTO> productFixedList = new ArrayList<>();
    if (!fixedList.isEmpty()) {
      for (ProductFixedDTO pfd : getOffersByPackageType(this.beanMap.productFixedToDTOs(fixedList), "Movil")) {
        if (pfd.getEnabled().equals(Constant.TWO))
          productFixedList.add(pfd); 
      } 
      return productFixedList;
    } 
    return new ArrayList<>();
  }
  
  public List<ProductFixedDTO> assignBillingOfferClientInterested(List<ProductFixedDTO> riskFilteredList, List<Offer> optionalOffers) {
    List<ProductFixedDTO> interestFilteredList = new ArrayList<>();
    BillProdCatalogCollection<BillingProductDTO> boPoListFromInterestedClient = getLstMobile(optionalOffers);
    for (ProductFixedDTO product : riskFilteredList) {
      for (BillingProductDTO billingProductDTO : boPoListFromInterestedClient) {
        if (product.getBillingProductDto().getBoCode().equals(billingProductDTO.getBoCode()) && product
          .getBillingProductDto().getPoCode().equals(billingProductDTO.getPoCode()))
          interestFilteredList.add(product); 
      } 
    } 
    if (CollectionUtils.isEmpty(interestFilteredList))
      throw new BusinessException("0000"); 
    return interestFilteredList;
  }
  
  public List<ProductFixedDTO> assignBillingOfferClientInterestedLMA(PartialRequestDTO partial, List<ProductFixedDTO> riskFilteredList, List<Offer> optionalOffers) {
    List<ProductFixedDTO> interestFilteredList = new ArrayList<>();
    BillProdCatalogCollection<BillingProductDTO> boPoListFromInterestedClient = getLstMobileLMA(optionalOffers);
    for (ProductFixedDTO product : riskFilteredList) {
      for (BillingProductDTO billingProductDTO : boPoListFromInterestedClient) {
    	//Se añade filtro de busqueda de ofertas por DealerCode,Departmen,StoreId
        if (product.getBillingProductDto().getBoCode().equals(billingProductDTO.getBoCode()) 
        && product.getBillingProductDto().getPoCode().equals(billingProductDTO.getPoCode())
        && product.getBillingProductDto().getDealerCode().equals(billingProductDTO.getDealerCode())
        && product.getBillingProductDto().getDepartment().equals(billingProductDTO.getDepartment())
        && product.getBillingProductDto().getStoreId().equals(billingProductDTO.getStoreId()))
          interestFilteredList.add(product); 
      } 
    } 
    if (CollectionUtils.isEmpty(interestFilteredList) && !partial.getProductType().equals("movil"))
      throw new BusinessException("0000"); 
    return interestFilteredList;
  }
  
  public List<ProductFixedDTO> assignBillingOfferProductFixed(PartialRequestDTO partial, String scoreDigits) {
    List<ProductFixedDTO> riskFilteredList = new ArrayList<>();
    List<ProductFixedDTO> filteredList = partial.getResultantProductFixedList();
    for (int i = 0; i < filteredList.size(); i++) {
      if (((ProductFixedDTO)filteredList.get(i)).getBillingProductDto() != null && 
        Integer.parseInt(((ProductFixedDTO)filteredList.get(i)).getBillingProductDto().getBoPlanCreditScore()) <= Integer.parseInt(scoreDigits))
        riskFilteredList.add(filteredList.get(i)); 
      if (i + 1 == filteredList.size())
        break; 
      if (((ProductFixedDTO)filteredList.get(i)).getBillingProductDto() != null && 
        Integer.parseInt(((ProductFixedDTO)filteredList.get(i)).getBillingProductDto().getBoPlanCreditScore()) > Integer.parseInt(scoreDigits) && (
        (ProductFixedDTO)filteredList.get(i)).getPriority() != null)
        ((ProductFixedDTO)filteredList.get(i + 1)).setPriority(((ProductFixedDTO)filteredList.get(i)).getPriority()); 
    } 
    if (CollectionUtils.isEmpty(riskFilteredList) && !partial.getProductType().equals("movil"))
      throw new BusinessException("1060"); 
    return riskFilteredList;
  }
  
  /** Agregando la variable productType para poder filtrar la asignación si es movil **/
  public void assignBillingProductValues(PartialRequestDTO partial, BillProdCatalogCollection<BillingProductDTO> boPoListFromFilteredFixedProducts, String productType) {
    int prodOnlyBillOffCatalog = 0;
    int isMovil = 0, isBoPo=0;
	for (ProductFixedDTO product : partial.getResultantProductFixedList()) {
      for (CatalogBillingProduct billingProduct : boPoListFromFilteredFixedProducts) {
    	if(!productType.equals("movil")) {
    	  if (product.getPackageMobilCode().equals(billingProduct.getBoId()) && product.getPoPackage().equals(billingProduct.getPoId()))
    		product.setBillingProductDto(this.beanMap.catalogBillingProductToBillingProductDTO(billingProduct));
    	}        
        else {
          isMovil++;
          if (product.getPackageMobilCode().equals(billingProduct.getBoId()) && product.getPoPackage().equals(billingProduct.getPoId())) {
        	  product.setBillingProductDto(this.beanMap.catalogBillingProductToBillingProductDTO(billingProduct));
        	  isBoPo++;
          }   
          else {
        	  prodOnlyBillOffCatalog++;
          }
        }    	
      } 
    } 
	
	if(prodOnlyBillOffCatalog != 0 && isBoPo==0) {
		if(isMovil!=0) {
			//for (ProductFixedDTO product : partial.getResultantProductFixedList()) {
			ProductFixedDTO prd;
			partial.getResultantProductFixedList().clear();
			      for (CatalogBillingProduct billingProduct : boPoListFromFilteredFixedProducts) {		
			    	  prd = new ProductFixedDTO();
			    	  prd.setBillingProductDto(this.beanMap.catalogBillingProductToBillingProductDTO(billingProduct));
			    	  
			    	  partial.getResultantProductFixedList().add(prd);
			      }
			//}
		}
		else {
			throw new BusinessException(Constant.BE_1080);
		}
	}
	
  }
  
  @NoLoggeable
  public void assignPpfCondition(Long commercialOpId, ProductFixedDTO product, Boolean bool) {
    PostPaidEasyDTO postpaid = new PostPaidEasyDTO();
    postpaid.setCommercialOperationId(commercialOpId);
    postpaid.setCode("ppf");
    postpaid.setValue(bool.toString());
    product.getPostPaidEasy().add(postpaid);
  }
  
  @NoLoggeable
  public void assignPpfConditionFalse(Long commercialOpId,ProductFixedDTO product, Boolean bool) {
    PostPaidEasyDTO postpaid = new PostPaidEasyDTO();
    postpaid.setCommercialOperationId(commercialOpId);
    postpaid.setCode("ppf");
    postpaid.setValue(bool.toString());
    product.getPostPaidEasy().add(postpaid);
  }
  
  @NoLoggeable
  public Boolean evaluateCondition(int planRank, FourthDigitDTO objDigit) {
    if (objDigit.getFlagPpf().equals(this.rulesValue.getPPF_INDICATOR_TRUE())) {
      if (objDigit.getAmountPlanRank().intValue() >= 0)
        return (planRank >= objDigit.getAmountPlanRank().intValue()) ? Boolean.TRUE : Boolean.FALSE; 
      return Boolean.TRUE;
    } 
    if (objDigit.getFlagPpf().equals(this.rulesValue.getPPF_INDICATOR_FALSE()))
      return Boolean.FALSE; 
    return Boolean.FALSE;
  }
  
  public List<ProductFixedDTO> getEnabledOffers(List<ProductFixedDTO> catalogProductFixedDto) {
    List<ProductFixedDTO> enabledOffers = new ArrayList<>();
    enabledOffers.addAll(catalogProductFixedDto);
    CollectionUtils.filter(enabledOffers, new Predicate<ProductFixedDTO>() {
	    @Override
	    public boolean evaluate(ProductFixedDTO object) {
		if (object.getEnabled().equals(Constant.ONE)) {
		    return true;
		}
		return false;
	    }
	});
    return enabledOffers;
  }
  
  public List<ProductFixedDTO> getEnabledOffersLMA(List<ProductFixedDTO> catalogProductFixedDto) {
    List<ProductFixedDTO> enabledOffers = new ArrayList<>();
    enabledOffers.addAll(catalogProductFixedDto);
    CollectionUtils.filter(enabledOffers, new Predicate<ProductFixedDTO>() {
	    @Override
	    public boolean evaluate(ProductFixedDTO object) {
		if (object.getEnabled().equals(Constant.TWO)) {
		    return true;
		}
		return false;
	    }
	});
    return enabledOffers;
  }
  
  public List<ProductFixedDTO> getDisabledOffers(List<ProductFixedDTO> catalogProductFixedDto) {
    List<ProductFixedDTO> enabledOffers = new ArrayList<>();
    enabledOffers.addAll(catalogProductFixedDto);
    CollectionUtils.filter(enabledOffers, new Predicate<ProductFixedDTO>() {
	    @Override
	    public boolean evaluate(ProductFixedDTO object) {
		if (object.getEnabled().equals(Constant.ZERO)) {
		    return true;
		}
		return false;
	    }
	});
    return enabledOffers;
  }
  
  private List<ProductFixedDTO> getOffersByPackageType(List<ProductFixedDTO> catalogProductFixedDto, String packageType) {
    List<ProductFixedDTO> enabledOffers = new ArrayList<>();
    for (ProductFixedDTO productFixedDTO : catalogProductFixedDto) {
      if (productFixedDTO.getPackageType().equals(packageType))
        enabledOffers.add(productFixedDTO); 
    } 
    return enabledOffers;
  }
  
  public void determinatePenaltyByPlanRank(PartialRequestDTO partial) {
    List<CatalogBillingProduct> catBillProdList = this.billProd.getAllBillProd();
    int contador = 0;
    for (MobileDTO mobil : partial.getMobileCustomerInformation()) {
      contador++;
      CatalogBillingProduct catBoPo = TotalUtil.getCatalogBillingProductBoId(catBillProdList, mobil.getBoId());
      for (ProductFixedDTO productos : partial.getResultantProductFixedList()) {
        Information information = new Information();
        information.setCode(String.format("Penalidad Plan Rank_Movil%s", new Object[] { Integer.valueOf(contador) }));
        if (!TotalUtil.isOfferMTActualProduct(productos, this.rulesValue.getPRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT()) && catBoPo != null && productos
          .getBillingProductDto() != null && catBoPo
          .getBoPlankRank().intValue() > productos.getBillingProductDto().getBoPlankRank().intValue()) {
          information.setValue("true");
        } else {
          information.setValue("false");
        } 
        productos.getPenaltiesPlankRank().add(information);
      } 
    } 
  }
  
  public List<ProductFixedDTO> assignBillingOfferClientInterestedLMA(List<ProductFixedDTO> riskFilteredList, List<Offer> optionalOffers) {
    return null;
  }
}
