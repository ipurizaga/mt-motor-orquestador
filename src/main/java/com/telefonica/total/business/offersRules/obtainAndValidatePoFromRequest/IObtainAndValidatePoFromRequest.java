package com.telefonica.total.business.offersRules.obtainAndValidatePoFromRequest;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.ProductFixedDTO;

public interface IObtainAndValidatePoFromRequest {

    /***
     * Método que se encarga de obtener la ps po y bo del request.
     * 
     * @param fixedParkDto
     * @param mobileDevices
     * @param commercialOpeList
     */
    void execute(FixedDTO fixedParkDto, List<MobileDTO> mobileDevices, List<ProductFixedDTO> filteredProductList);

}
