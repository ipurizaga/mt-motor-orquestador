package com.telefonica.total.business.offersRules.determinateAdditionalRentsToKeep;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.RulesValueProp;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.pojo.req.Information;

@Service
public class DeterminateAdditionalRentsToKeep implements IDeterminateAdditionalRentsToKeep{

    @Autowired
    private RulesValueProp rulesValue;
    
    @Override
    public void execute(List<ProductFixedDTO> priorizationList, FixedDTO fixedParkDto) {
	for (ProductFixedDTO product : priorizationList) {
	    DecimalFormat df = new DecimalFormat("#.00");
	    Double rentAcumulator = 0D;
	    List<Information> list = new ArrayList<>();
	    if (fixedParkDto.getDecoSmartRent() != null) {
		list.add(Information.builder().code(rulesValue.getRENT_DECO_SMART_KEEP()).value(fixedParkDto.getDecoSmartRent().toString())
			.build());
		rentAcumulator += fixedParkDto.getDecoSmartRent();
	    }
	    if (fixedParkDto.getHdAditionalPointRent() != null) {
		list.add(Information.builder().code(rulesValue.getRENT_ADDITIONAL_POINT_KEEP())
			.value(fixedParkDto.getHdAditionalPointRent().toString()).build());
		rentAcumulator += fixedParkDto.getHdAditionalPointRent();
	    }
	    if (fixedParkDto.getRentWifiUltraInternet() != null) {
		list.add(Information.builder().code(rulesValue.getRENT_ULTRA_WIFI_KEEP())
			.value(fixedParkDto.getRentWifiUltraInternet().toString()).build());
		rentAcumulator += Double.parseDouble(fixedParkDto.getRentWifiUltraInternet().toString());
	    }
	    if (fixedParkDto.getInternetHavePamRent() != null) {
		list.add(Information.builder().code(rulesValue.getRENT_TOTAL_SECURITY_KEEP())
			.value(fixedParkDto.getInternetHavePamRent().toString()).build());
		rentAcumulator += fixedParkDto.getInternetHavePamRent();
	    }
	    if (fixedParkDto.getRentMonoProdPlan() != null) {
		list.add(Information.builder().code(rulesValue.getRENT_MULTIDESTINO_KEEP())
			.value(fixedParkDto.getRentMonoProdPlan().toString()).build());
		rentAcumulator += fixedParkDto.getRentMonoProdPlan();
	    }
	    if (fixedParkDto.getRentVasMonoProdLine() != null) {
		list.add(Information.builder().code(rulesValue.getRENT_MANTENIMIENTO_KEEP())
			.value(fixedParkDto.getRentVasMonoProdLine().toString()).build());
		rentAcumulator += fixedParkDto.getRentVasMonoProdLine();
	    }
	    if (fixedParkDto.getAditionalRent() != null) {
		list.add(Information.builder().code(rulesValue.getRENT_OTHERS_KEEP()).value(fixedParkDto.getAditionalRent().toString())
			.build());
		rentAcumulator += fixedParkDto.getAditionalRent();
	    }
	    product.setAditionalRentsToKeep(list);
	    product.setAditionalRentsToKeepSummatory(Double.parseDouble(df.format(rentAcumulator).replace(',', '.')));
	}

    }
}
