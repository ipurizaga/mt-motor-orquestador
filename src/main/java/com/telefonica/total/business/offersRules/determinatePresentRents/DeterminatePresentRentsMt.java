package com.telefonica.total.business.offersRules.determinatePresentRents;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.RulesValueProp;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.pojo.req.Information;

@Service
@Qualifier("plantMt")
public class DeterminatePresentRentsMt implements IDeterminatePresentRents{
    
    @Autowired
    private RulesValueProp rulesValue;

    public void executeMt(FixedDTO fixedParkDto, boolean distinctPS) {
	List<Information> list = new ArrayList<>();
	Double acumulator = determinateRentVasMonoProd(fixedParkDto, distinctPS, list);

	if (fixedParkDto.getRentVasMonoProdLine() != null) {
	    acumulator += fixedParkDto.getRentVasMonoProdLine();
	    list.add(Information.builder().code(rulesValue.getRENT_ADITIONAL_MANTENIMIENTO())
		    .value(fixedParkDto.getRentVasMonoProdLine().toString()).build());
	}

	if (fixedParkDto.getDecoSmartRent() != null) {
	    acumulator += fixedParkDto.getDecoSmartRent();
	    list.add(Information.builder().code(rulesValue.getRENT_ADITIONAL_DECO_SMART()).value(fixedParkDto.getDecoSmartRent().toString())
		    .build());
	}
	if (fixedParkDto.getHdAditionalPointRent() != null) {
	    acumulator += fixedParkDto.getHdAditionalPointRent();
	    list.add(Information.builder().code(rulesValue.getRENT_ADITIONAL_POINT_HD())
		    .value(fixedParkDto.getHdAditionalPointRent().toString()).build());
	}
	if (fixedParkDto.getRentWifiUltraInternet() != null) {
	    acumulator += Double.parseDouble(fixedParkDto.getRentWifiUltraInternet().toString());
	    list.add(Information.builder().code(rulesValue.getRENT_ADITIONAL_ULTRA_WIFI())
		    .value(fixedParkDto.getRentWifiUltraInternet().toString()).build());
	}
	if (fixedParkDto.getInternetHavePamRent() != null) {
	    acumulator += fixedParkDto.getInternetHavePamRent();
	    list.add(Information.builder().code(rulesValue.getRENT_ADITIONAL_TOTAL_SECURITY())
		    .value(fixedParkDto.getInternetHavePamRent().toString()).build());
	}
	if (fixedParkDto.getAditionalRent() != null) {
	    acumulator += fixedParkDto.getAditionalRent();
	    list.add(Information.builder().code(rulesValue.getRENT_ADITIONAL_OTHERS()).value(fixedParkDto.getAditionalRent().toString())
		    .build());
	}
	fixedParkDto.setSummatoryPresentRents(acumulator);
	fixedParkDto.setPresentRents(list);
    }

    public Double determinateRentVasMonoProd(FixedDTO fixedParkDto, boolean distinctPS, List<Information> list) {
	Double acumulator = 0D;
	if (fixedParkDto.getCableRentMonoProdPremHd() != null && !distinctPS) {
	    acumulator += fixedParkDto.getCableRentMonoProdPremHd();
	    list.add(Information.builder().code(rulesValue.getRENT_ADITIONAL_HD())
		    .value(fixedParkDto.getCableRentMonoProdPremHd().toString()).build());
	}
	if (fixedParkDto.getCableRentMonoProdPremFox() != null) {
	    acumulator += fixedParkDto.getCableRentMonoProdPremFox();
	    list.add(Information.builder().code(rulesValue.getRENT_ADITIONAL_FOX())
		    .value(fixedParkDto.getCableRentMonoProdPremFox().toString()).build());
	}
	if (fixedParkDto.getCableRentMonoProdPremHbo() != null) {
	    acumulator += fixedParkDto.getCableRentMonoProdPremHbo();
	    list.add(Information.builder().code(rulesValue.getRENT_ADITIONAL_HBO())
		    .value(fixedParkDto.getCableRentMonoProdPremHbo().toString()).build());
	}
	if ((fixedParkDto.getCableRentMonoProdEstellar() == null || fixedParkDto.getCableRentMonoProdEstellar() == 0)
		&& (StringUtils.contains(fixedParkDto.getCablePremiumChannels(), Constant.PRODUCT_ESTELLAR))) {
	    acumulator += 20.0;
	    list.add(Information.builder().code(rulesValue.getRENT_ADITIONAL_ESTELLAR()).value("20.0").build());
	} else if (fixedParkDto.getCableRentMonoProdEstellar() != null) {
	    acumulator += fixedParkDto.getCableRentMonoProdEstellar();
	    list.add(Information.builder().code(rulesValue.getRENT_ADITIONAL_ESTELLAR())
		    .value(fixedParkDto.getCableRentMonoProdEstellar().toString()).build());
	}

	if (fixedParkDto.getCableRentMonoProdHTPack() != null) {
	    acumulator += fixedParkDto.getCableRentMonoProdHTPack();
	    list.add(Information.builder().code(rulesValue.getRENT_ADITIONAL_HTPACK())
		    .value(fixedParkDto.getCableRentMonoProdHTPack().toString()).build());
	}

	if (fixedParkDto.getCableRentMonoProdGoldPrem() != null) {
	    acumulator += fixedParkDto.getCableRentMonoProdGoldPrem();
	    list.add(Information.builder().code(rulesValue.getRENT_ADITIONAL_GOLDPREM())
		    .value(fixedParkDto.getCableRentMonoProdGoldPrem().toString()).build());
	}

	if (fixedParkDto.getRentMonoProdPlan() != null) {
	    acumulator += fixedParkDto.getRentMonoProdPlan();
	    list.add(Information.builder().code(rulesValue.getRENT_ADITIONAL_MULTIDESTINO())
		    .value(fixedParkDto.getRentMonoProdPlan().toString()).build());
	}
	return acumulator;
    }

    @Override
    public void execute(FixedDTO fixedParkDto) {
    }
}
