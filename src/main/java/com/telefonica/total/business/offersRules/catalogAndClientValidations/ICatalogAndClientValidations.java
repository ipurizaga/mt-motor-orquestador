package com.telefonica.total.business.offersRules.catalogAndClientValidations;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.pojo.req.CommercialOperationInfo;

public interface ICatalogAndClientValidations {

    /***
     * Método que se encarga de filtrar los productos existentes en el catálogo de
     * productos fijos para determinar cuales pueden ofrecerse al cliente.
     * 
     * @param fixedParkDto
     * @param productList
     * @param mobileDevices
     * @param commercialInfo
     * @param customer
     * @param provideOrPortability
     * @param parkMobileByDocument
     * @return
     */
    List<ProductFixedDTO> execute(FixedDTO fixedParkDto, List<ProductFixedDTO> productList, List<MobileDTO> mobileDevices,
	    CommercialOperationInfo commercialInfo, boolean provideOrPortability);

    List<ProductFixedDTO> executeLMA(FixedDTO fixedParkDto, List<ProductFixedDTO> productList, List<ProductFixedDTO> productLMAList,
	    List<MobileDTO> mobileDevices, CommercialOperationInfo commercialInfo, boolean provideOrPortability);
    
    List<ProductFixedDTO> executeLMAFailed(FixedDTO fixedParkDto, List<ProductFixedDTO> productListAll, List<ProductFixedDTO> productList, List<ProductFixedDTO> productLMAList,
	    List<MobileDTO> mobileDevices, CommercialOperationInfo commercialInfo, boolean provideOrPortability);
    
    List<ProductFixedDTO> executeLMAMT(FixedDTO fixedParkDto, List<ProductFixedDTO> productList, List<ProductFixedDTO> productLMAList,
	    List<MobileDTO> mobileDevices, CommercialOperationInfo commercialInfo, boolean provideOrPortability, ProductFixedDTO product_current);
}
