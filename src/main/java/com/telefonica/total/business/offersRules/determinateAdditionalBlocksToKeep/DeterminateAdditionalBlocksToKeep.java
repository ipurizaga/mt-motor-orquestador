package com.telefonica.total.business.offersRules.determinateAdditionalBlocksToKeep;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.RulesValueProp;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.pojo.req.Information;

@Service
public class DeterminateAdditionalBlocksToKeep implements IDeterminateAdditionalBlocksToKeep {

    @Autowired
    private RulesValueProp rulesValue;

    @Override
    public void execute(List<ProductFixedDTO> priorizationList, FixedDTO fixedParkDto) {
	if (rulesValue.getCABLE_PRESENT_ONE().equals(fixedParkDto.getCablePresent())) {
	    for (ProductFixedDTO product : priorizationList) {
		List<String> actualChannels = getActualChannels(fixedParkDto);
		List<String> offerChannels = getOfferChannelsByProduct(product);
		actualChannels.removeAll(offerChannels);
		if (!actualChannels.isEmpty()) {
		    Double rentAcumulator = 0D;
		    List<Information> aditionalBlocksTokeep = new ArrayList<>();
		    for (String channel : actualChannels) {
			Information information = new Information();
			rentAcumulator += determinateInformationAndRentAcumulator(channel, information, fixedParkDto);
			aditionalBlocksTokeep.add(information);
		    }
		    product.setAditionalBlocksTokeep(aditionalBlocksTokeep);
		    product.setAditionalBlocksTokeepRent(rentAcumulator);
		}
	    }
	}
    }

    /***
     * Método que se encarga de obtener los canales con los que cuenta actualmente
     * el cliente.
     * 
     * @param fixedParkDto
     * @return
     */
    private List<String> getActualChannels(FixedDTO fixedParkDto) {
	List<String> list = new ArrayList<>();
	addCablePresentPremHd(list, fixedParkDto);
	addCablePresentPremHbo(list, fixedParkDto);
	addCablePresentPremFox(list, fixedParkDto);
	addCablePresentEstellar(list, fixedParkDto);
	if (fixedParkDto.getCablePresentGoldPrem() != null && fixedParkDto.getCablePresentGoldPrem().toString().equals("1")) {
	    list.add(Constant.PRODUCT_GOLDPREM);
	}
	if (fixedParkDto.getCablePresentHTPack() != null && fixedParkDto.getCablePresentHTPack().toString().equals("1")) {
	    list.add(Constant.PRODUCT_HTPACK);
	}
	return list;
    }

    public void addCablePresentPremHd(List<String> list, FixedDTO fixedParkDto) {
	if ((fixedParkDto.getCablePresentPremHd() != null && fixedParkDto.getCablePresentPremHd().toString().equals("1"))
		|| (fixedParkDto.getCablePremiumChannels() != null
			&& StringUtils.contains(fixedParkDto.getCablePremiumChannels(), Constant.PRODUCT_FIXED_HD))) {
	    list.add(Constant.PRODUCT_FIXED_HD);
	}
    }

    public void addCablePresentPremHbo(List<String> list, FixedDTO fixedParkDto) {
	if ((fixedParkDto.getCablePresentPremHbo() != null && fixedParkDto.getCablePresentPremHbo().toString().equals("1"))
		|| (fixedParkDto.getCablePremiumChannels() != null
			&& StringUtils.contains(fixedParkDto.getCablePremiumChannels(), Constant.PRODUCT_FIXED_HBO))) {
	    list.add(Constant.PRODUCT_FIXED_HBO);
	}
    }

    public void addCablePresentPremFox(List<String> list, FixedDTO fixedParkDto) {
	if ((fixedParkDto.getCablePresentPremFox() != null && fixedParkDto.getCablePresentPremFox().toString().equals("1"))
		|| (fixedParkDto.getCablePremiumChannels() != null
			&& (StringUtils.contains(fixedParkDto.getCablePremiumChannels(), Constant.PRODUCT_FIXED_FOX)
				|| StringUtils.contains(fixedParkDto.getCablePremiumChannels(), Constant.PRODUCT_MOVIECITY)))) {
	    list.add(Constant.PRODUCT_FIXED_FOX);
	}
    }

    public void addCablePresentEstellar(List<String> list, FixedDTO fixedParkDto) {
	if ((fixedParkDto.getCablePresentEstellar() != null && fixedParkDto.getCablePresentEstellar().toString().equals("1"))
		|| (fixedParkDto.getCablePremiumChannels() != null
			&& StringUtils.contains(fixedParkDto.getCablePremiumChannels(), Constant.PRODUCT_ESTELLAR))
		|| "Estelar".equals(fixedParkDto.getCableType())) {
	    list.add(Constant.PRODUCT_ESTELLAR);
	}
    }

    /***
     * Método que se encargda de obtener los canales presentes en el producto
     * ofrecido
     * 
     * @param product
     * @return
     */
    private List<String> getOfferChannelsByProduct(ProductFixedDTO product) {
	List<String> list = new ArrayList<>();
	if (product.getPremiumHdPresent() != null && product.getPremiumHdPresent().toString().equals("1")) {
	    list.add(Constant.PRODUCT_FIXED_HD);
	}
	if (product.getPremiumHboPresent() != null && product.getPremiumHboPresent().toString().equals("1")) {
	    list.add(Constant.PRODUCT_FIXED_HBO);
	}
	if (product.getPremiumFoxPresent() != null && product.getPremiumFoxPresent().toString().equals("1")) {
	    list.add(Constant.PRODUCT_FIXED_FOX);
	}
	return list;
    }

    private Double determinateInformationAndRentAcumulator(String channel, Information information,
	    FixedDTO fixedParkDto) {
	Double rentAcumulator = 0D;
	if (channel.equals(Constant.PRODUCT_FIXED_HD)) {
	    information.setCode(rulesValue.getBLOCK_HD_KEEP());
	    information.setValue(fixedParkDto.getCableRentMonoProdPremHd().toString());
	    rentAcumulator += fixedParkDto.getCableRentMonoProdPremHd();
	} else if (channel.equals(Constant.PRODUCT_FIXED_HBO)) {
	    determinateValueRentMonoProd(fixedParkDto, Constant.PRODUCT_FIXED_HBO);
	    information.setCode(rulesValue.getBLOCK_HBO_KEEP());
	    information.setValue(fixedParkDto.getCableRentMonoProdPremHbo().toString());
	    rentAcumulator += fixedParkDto.getCableRentMonoProdPremHbo();
	} else if (channel.equals(Constant.PRODUCT_FIXED_FOX)) {
	    determinateValueRentMonoProd(fixedParkDto, Constant.PRODUCT_FIXED_FOX);
	    information.setCode(rulesValue.getBLOCK_FOX_KEEP());
	    information.setValue(fixedParkDto.getCableRentMonoProdPremFox().toString());
	    rentAcumulator += fixedParkDto.getCableRentMonoProdPremFox();
	} else if (channel.equals(Constant.PRODUCT_ESTELLAR)) {
	    determinateValueRentMonoProd(fixedParkDto, Constant.PRODUCT_ESTELLAR);
	    information.setCode(rulesValue.getBLOCK_EST_KEEP());
	    information.setValue(fixedParkDto.getCableRentMonoProdEstellar().toString());
	    rentAcumulator += fixedParkDto.getCableRentMonoProdEstellar();
	} else if (channel.equals(Constant.PRODUCT_GOLDPREM)) {
	    determinateValueRentMonoProd(fixedParkDto, Constant.PRODUCT_GOLDPREM);
	    information.setCode(rulesValue.getBLOCK_GOLDPREM_KEEP());
	    information.setValue(fixedParkDto.getCableRentMonoProdGoldPrem().toString());
	    rentAcumulator += fixedParkDto.getCableRentMonoProdGoldPrem();
	} else if (channel.equals(Constant.PRODUCT_HTPACK)) {
	    determinateValueRentMonoProd(fixedParkDto, Constant.PRODUCT_HTPACK);
	    information.setCode(rulesValue.getBLOCK_HTPACK_KEEP());
	    information.setValue(fixedParkDto.getCableRentMonoProdHTPack().toString());
	    rentAcumulator += fixedParkDto.getCableRentMonoProdHTPack();
	}
	return rentAcumulator;
    }

    private void determinateValueRentMonoProd(FixedDTO fixedParkDto, String valueType) {

	if (Constant.PRODUCT_FIXED_FOX.equals(valueType) && fixedParkDto.getCableRentMonoProdPremFox() == 0) {
	    fixedParkDto.setCableRentMonoProdPremFox(35.0);
	}
	if (Constant.PRODUCT_FIXED_HBO.equals(valueType) && fixedParkDto.getCableRentMonoProdPremHbo() == 0) {
	    fixedParkDto.setCableRentMonoProdPremHbo(35.0);
	}
	if (Constant.PRODUCT_ESTELLAR.equals(valueType) && fixedParkDto.getCableRentMonoProdEstellar() == 0) {
	    fixedParkDto.setCableRentMonoProdEstellar(20.0);
	}

    }
}
