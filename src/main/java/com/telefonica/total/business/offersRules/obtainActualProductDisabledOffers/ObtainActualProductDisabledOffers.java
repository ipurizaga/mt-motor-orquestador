package com.telefonica.total.business.offersRules.obtainActualProductDisabledOffers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.RulesValueProp;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.enums.OperationDevice;
import com.telefonica.total.pojo.req.CommercialOperationInfo;
import com.telefonica.total.pojo.req.Information;

@Service
public class ObtainActualProductDisabledOffers implements IObtainActualProductDisabledOffers{

    @Autowired
    private RulesValueProp rulesValue;
    
    @Override
    public ProductFixedDTO executeMt(FixedDTO fixedParkDto, List<ProductFixedDTO> catalogProductFixedDto,
	    List<ProductFixedDTO> filteredProductList, CommercialOperationInfo commercialInfo) {
	ProductFixedDTO product = TotalUtil.getActualDisabledOfferMT(catalogProductFixedDto, fixedParkDto);
	if (product != null) {
	    product.getCharacteristics().add(Information.builder().code(rulesValue.getPRODUCT_CHARACTERISTIC())
		    .value(rulesValue.getPRODUCT_CHARACTERISTIC_DISABLED()).build());
	    product.getCharacteristics().add(Information.builder().code(rulesValue.getPRODUCT_CHARACTERISTIC())
		    .value(rulesValue.getPRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT()).build());
	    if(commercialInfo.getCommercialOpers().size()==3) {
        	    if(commercialInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.ACTIVATION.getCodeDesc())||
        		    commercialInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.CAEQ.getCodeDesc())||
        		    commercialInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.PORTABILITY.getCodeDesc())) {
        		    product.setMobil0QuantityData(product.getMobil0QuantityData());
        		    product.setMobil0Rent(product.getMobil0Rent());
        	    }
	    }else {
		    product.setMobil0QuantityData(product.getMobil0QuantityData());
		    if(commercialInfo.getCommercialOpers().size()==2) {
			product.setMobil0Rent(product.getMobil0Rent()/*+product.getMobil1Rent()*/);
		    }
	    }
	    
	    Double packageRent = null;
	    for (ProductFixedDTO productMirror : filteredProductList) {
		if (product.getMirrorOffer().equals(productMirror.getMirrorOffer())) {
		    productMirror.getCharacteristics().add(Information.builder().code(rulesValue.getPRODUCT_CHARACTERISTIC())
			    .value(rulesValue.getPRODUCT_CHARACTERISTIC_MIRROR_OFFER()).build());
		    packageRent = product.getPackageRent();
		    break;

		}
	    }

	    if (packageRent != null && packageRent != 359) {
		TotalUtil.removeRuleByRentFromProductOffers(filteredProductList, 359);
	    }
	    if (packageRent != null && packageRent != 199) {
		TotalUtil.removeRuleByRentFromProductOffers(filteredProductList, 199);
	    }
	}
	return product;
    }
}
