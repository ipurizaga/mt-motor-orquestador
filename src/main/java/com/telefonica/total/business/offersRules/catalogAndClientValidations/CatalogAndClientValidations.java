package com.telefonica.total.business.offersRules.catalogAndClientValidations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.RulesValueProp;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.common.util.UtilCollections;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.CommercialOperationInfo;

@Service
@Qualifier("totalization")
public class CatalogAndClientValidations implements ICatalogAndClientValidations {

    @Autowired
    private RulesValueProp rulesValue;

    @Override
    public List<ProductFixedDTO> executeLMAFailed(FixedDTO fixedParkDto, List<ProductFixedDTO> productListAll, List<ProductFixedDTO> productList, List<ProductFixedDTO> productLMAList, List<MobileDTO> mobileDevices,
	    CommercialOperationInfo commercialInfo, boolean provideOrPortability) {

	List<ProductFixedDTO> filteredProductList = new ArrayList<>();
	
	FixedDTO fd = fixedParkDto;
	
	if(mobileDevices.size() == 2) {
	
        	fd.setBoIdRequest(commercialInfo.getCommercialOpers().get(2).getSubscriber().getBoId());
        	fd.setPoIdRequest(commercialInfo.getCommercialOpers().get(2).getSubscriber().getPoId());
        	
        	ProductFixedDTO prodFixDTO = findGrill(fd, productListAll);
        	
        	ProductFixedDTO prodFixDTO2 = new ProductFixedDTO();
        	
        	//System.out.println("PARRILLA ==> "+(prodFixDTO == null ? "SI": "NO" ));
        	
        	if(prodFixDTO.getMerchandisingType().equals(Constant.PARRILLA_1)) {
        	    prodFixDTO2 = migrateGrill(prodFixDTO, productListAll, Constant.PARRILLA_2);
        	    addMigraParrillas(productList, prodFixDTO2);
        	}else if(prodFixDTO.getMerchandisingType().equals(Constant.PARRILLA_2)) {
        	    migrateGrill(prodFixDTO, productListAll, Constant.PARRILLA_2);
        	    addMigraParrillas(productList, prodFixDTO);
        	}else {
        	    //addLMAToMT(productList, productLMAList) ;
        	    addLMAToFailed(productList, productLMAList) ;
        	}
	
	}
	
	
	/* sub regla 1 - Cobertura Iquitos */
	haveIquitosCoverage(fixedParkDto, productList);
	if (!provideOrPortability) {
	     /*sub regla 3 - Paquete PS */
	    havePSPackage(fixedParkDto, productList);
	}
	/* sub regla 2 - Cobertura diferente a HFC o FTTH */
	haveCoverageHfcOrFtth(productList, commercialInfo, fixedParkDto);
	/* sub regla 5 - Limite de credito movil del cliente */
	haveLimitMobileCredit(productList, commercialInfo);
	/* ¿Tiene ofertas disponibles? */
	filteredProductList.addAll(productList);
	return filteredProductList;
    }
    
    @Override
    public List<ProductFixedDTO> executeLMA(FixedDTO fixedParkDto,
	    List<ProductFixedDTO> productList, List<ProductFixedDTO> productLMAList, List<MobileDTO> mobileDevices,
	    CommercialOperationInfo commercialInfo, boolean provideOrPortability) {

	List<ProductFixedDTO> filteredProductList = new ArrayList<>();
	//addLMAToMT(productList, productLMAList) ;
	
	/* sub regla 1 - Cobertura Iquitos */
	haveIquitosCoverage(fixedParkDto, productList);
	if (!provideOrPortability) {
	     /*sub regla 3 - Paquete PS */
	    havePSPackage(fixedParkDto, productList);
	}
	/* sub regla 2 - Cobertura diferente a HFC o FTTH */
	haveCoverageHfcOrFtth(productList, commercialInfo, fixedParkDto);
	/* sub regla 5 - Limite de credito movil del cliente */
	haveLimitMobileCredit(productList, commercialInfo);
	/* ¿Tiene ofertas disponibles? */
	filteredProductList.addAll(productList);
	return filteredProductList;
    }
    
    /***
     * Añadir LMA a todas las ofertas
     * 
     * @param fixedParkDto
     * @param productList
     */
    private void addMigraParrillas(List<ProductFixedDTO> productList, ProductFixedDTO product) {
	
	ProductFixedDTO pfd = product;
	
	for (ProductFixedDTO productFixedDTO : productList) {
	    productFixedDTO.setMobil1QuantityData(pfd.getMobil1QuantityData());
	    productFixedDTO.setMobil1Roaming(pfd.getMobil1Roaming());
	    productFixedDTO.setMobil1RoamingCoverage(pfd.getMobil1RoamingCoverage());
	    productFixedDTO.setMobil1Rent(pfd.getMobil1Rent());
	    productFixedDTO.setMobil1Name(pfd.getMobil1Name());
	    productFixedDTO.setMobil2CostAlone(pfd.getMobil2CostAlone());
	    productFixedDTO.setMobil1GigaPass(pfd.getMobil1GigaPass());
	    //Agregando bo y po del LMA a la oferta p3
	    productFixedDTO.setLma_Bo(pfd.getPackageMobilCode());
	    productFixedDTO.setLma_Po(pfd.getPoPackage());
	}
    }
    
    
    
    public ProductFixedDTO findGrill(FixedDTO fixedParkDto, List<ProductFixedDTO> productListAll) {
	
	ProductFixedDTO pfd = new ProductFixedDTO();
	
	for (ProductFixedDTO productFixedDTO : productListAll) {
	    
	    if(fixedParkDto.getPoIdRequest().equals(Integer.parseInt(productFixedDTO.getPoPackage())) && fixedParkDto.getBoIdRequest().equals(Integer.parseInt(productFixedDTO.getPackageMobilCode()))){
		
		pfd = productFixedDTO;
		
	    }
	    
	}
	
	return pfd;
	
    }
    
    public ProductFixedDTO migrateGrill(ProductFixedDTO pfd, List<ProductFixedDTO> productList, String parrilla) {
	
	ProductFixedDTO pf = new ProductFixedDTO();
	
	for (ProductFixedDTO productFixedDTO : productList) {
	    
	    if(productFixedDTO.getMirrorOffer().equals(pfd.getMirrorOffer()) && productFixedDTO.getMerchandisingType().equals(parrilla)) {
		pf = productFixedDTO;
	    }
	    
	}
	
	//System.out.println("COBERTURA: "+pf.getMobil1RoamingCoverage());
	
	return pf;
	
    }
    
    /***
     * Añadir LMA a todas las ofertas
     * 
     * @param fixedParkDto
     * @param productList
     */
    private void addLMAToMT(List<ProductFixedDTO> productList, List<ProductFixedDTO> productLMAList) {
	
	ProductFixedDTO pfd = productLMAList.get(1);
	
	for (ProductFixedDTO productFixedDTO : productList) {
	    productFixedDTO.setMobil1QuantityData(pfd.getMobil1QuantityData());
	    productFixedDTO.setMobil1Roaming(pfd.getMobil1Roaming());
	    productFixedDTO.setMobil1RoamingCoverage(pfd.getMobil1RoamingCoverage());
	    productFixedDTO.setMobil1Rent(pfd.getMobil1Rent());
	    productFixedDTO.setMobil1Name(pfd.getMobil1Name());
	    productFixedDTO.setMobil2CostAlone(pfd.getMobil2CostAlone());
	    productFixedDTO.setMobil1GigaPass(pfd.getMobil1GigaPass());
	    //Agregando bo y po del LMA a la oferta p3
	    productFixedDTO.setLma_Bo(pfd.getPackageMobilCode());
	    productFixedDTO.setLma_Po(pfd.getPoPackage());
	}
    }
    
    /***
     * Añadir LMA a todas las ofertas
     * 
     * @param fixedParkDto
     * @param productList
     */
    private void addLMAToFailed(List<ProductFixedDTO> productList, List<ProductFixedDTO> productLMAList) {
	
	ProductFixedDTO pfd = productLMAList.get(0);
	
	for (ProductFixedDTO productFixedDTO : productList) {
	    productFixedDTO.setMobil1QuantityData(pfd.getMobil1QuantityData());
	    productFixedDTO.setMobil1Roaming(pfd.getMobil1Roaming());
	    productFixedDTO.setMobil1RoamingCoverage(pfd.getMobil1RoamingCoverage());
	    productFixedDTO.setMobil1Rent(pfd.getMobil1Rent());
	    productFixedDTO.setMobil1Name(pfd.getMobil1Name());
	    productFixedDTO.setMobil2CostAlone(pfd.getMobil2CostAlone());
	    productFixedDTO.setMobil1GigaPass(pfd.getMobil1GigaPass());
	    //Agregando bo y po del LMA a la oferta p3
	    productFixedDTO.setLma_Bo(pfd.getPackageMobilCode());
	    productFixedDTO.setLma_Po(pfd.getPoPackage());
	}
    }

    /***
     * Clientes en Iquitos solo pueden adquirir productos para Iquitos.
     * 
     * @param fixedParkDto
     * @param productList
     */
    private void haveIquitosCoverage(FixedDTO fixedParkDto, List<ProductFixedDTO> productList) {
	List<ProductFixedDTO> removeList = new ArrayList<>();
	for (ProductFixedDTO productFixed : productList) {
	    if (!fixedParkDto.getIquitosCobInternet().equals(productFixed.getInternetCoverageIquitos())) {
		removeList.add(productFixed);
	    }
	}
	productList.removeAll(removeList);
	TotalUtil.availableOffersMT(productList, Constant.BE_1068);
    }

    /***
     * Cliente no puede adquirir el mismo producto
     * 
     * @param fixedParkDto
     * @param productList
     */
    private void havePSPackage(FixedDTO fixedParkDto, List<ProductFixedDTO> productList) {
	List<ProductFixedDTO> removeList = new ArrayList<>();
	if (!productList.isEmpty()) {
	    for (ProductFixedDTO productFixed : productList) {
		if (fixedParkDto.getPackagePs() == productFixed.getPackagePs()) {
		    removeList.add(productFixed);
		}
	    }
	    productList.removeAll(removeList);
	    TotalUtil.availableOffersMT(productList, Constant.BE_1071);
	}
    }

    /***
     * Clientes con cobertura diferente a HFC o FTTH.
     * 
     * @param productList
     * @param removeList
     */
    private void haveCoverageHfcOrFtth(List<ProductFixedDTO> productList, CommercialOperationInfo commercialInfo, FixedDTO fixedParkDto) {
	List<ProductFixedDTO> removeList = new ArrayList<>();
	//CAMBIOS PARA PARRILLA FTTH
	/*
	if (!productList.isEmpty()) {
	    for (ProductFixedDTO productFixed : productList) {
		if (!(rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(commercialInfo.getCoverage())
			|| rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(commercialInfo.getCoverage()))) {
		    removeList.add(productFixed);
		}
	    }
	    productList.removeAll(removeList);
	    TotalUtil.availableOffersMT(productList, Constant.BE_1069);
	}*/
	/**Tomando la cobertura de la zona**/
	String coverage = commercialInfo.getCoverage().toLowerCase().equals("fiber")?"FTTH":commercialInfo.getCoverage();
	String actualTechnology = fixedParkDto.getTechnologyInternet() != null ? fixedParkDto.getTechnologyInternet() : "";
	Character hfcCobInternet = fixedParkDto.getHfcCobInternet() != null? fixedParkDto.getHfcCobInternet() : '0';
	
	if(actualTechnology != null) {
		if(actualTechnology.equals("HFC") && hfcCobInternet.toString().equals("2")) {
			coverage = "FTTH";
		}
	}
	
	if (!productList.isEmpty()) {		
		switch(coverage) {
		/**SI LA COBERTURA DE LA ZONA ES HFC SE EVALUA DEPENDIENDO LA TECNOLOGIA ACTUAL**/
		 case "HFC":
			 if(fixedParkDto.getTechnologyInternet()!=null) {
				 /**SI LA TECNOLOGIA ACTUAL ES DISTINTA A FTTH, SE TOMA SOLO LAS OFERTAS EN TECNOLOGIA HFC**/
				 if(!fixedParkDto.getTechnologyInternet().equals("FTTH")) {
					 for (ProductFixedDTO productFixed : productList) {
						 if (!coverage.equals(productFixed.getInternetTechnology())) {
							 removeList.add(productFixed);
						 }
					 }
				 }
				 /**SI LA TECNOLOGIA ACTUAL ES FTTH, SE TOMA SOLO LAS OFERTAS EN TECNOLOGIA FTTH**/
				 else {
					 /*for (ProductFixedDTO productFixed : productList) {
						 if (!productFixed.getInternetTechnology().equals("FTTH")) {
							 removeList.add(productFixed);
						 }
					 }*/
				 }
			 }
			 /**SI ES ALTA NUEVA SE TOMA SOLO LAS OFERTAS CON LA MISMA TECNOLOGIA DE LA COBERTURA**/
			 else {
				 for (ProductFixedDTO productFixed : productList) {
					 if (!productFixed.getInternetTechnology().equals(coverage)) {
						 removeList.add(productFixed);
					 }
				 }
			 }
	     /**SI LA COBERTURA DE LA ZONA ES FTTH SE EVALUA DEPENDIENDO LA TECNOLOGIA ACTUAL**/	 
		 case "FTTH":
			 if(fixedParkDto.getTechnologyInternet()!=null) {
				 /**SI LA TECNOLOGIA ACTUAL ES DISTINTA A HFC, SE TOMA SOLO LAS OFERTAS EN TECNOLOGIA FTTH**/
				 if(!fixedParkDto.getTechnologyInternet().equals("HFC")) {
					 for (ProductFixedDTO productFixed : productList) {
						 if (!coverage.equals(productFixed.getInternetTechnology())) {
							 //removeList.add(productFixed);
						 }
					 }
					 if (removeList.size()==productList.size()) {
						 removeList.clear();
					 }
				 }
				 /**SI LA TECNOLOGIA ACTUAL ES HFC, NO SE MODIFICA LA LISTA DE OFERTAS ACTUALES, LUEGO SE VALIDAN EN 
				  * EquipmentValidations.java EN EL MÉTODO: determinateInternetDestinyTechnology**/
			 }
			 /**SI ES ALTA NUEVA SE SOLO LAS OFERTAS CON LA MISMA TECNOLOGIA DE LA COBERTURA**/
			 else {
				 for (ProductFixedDTO productFixed : productList) {
					 if (!productFixed.getInternetTechnology().equals(coverage)) {
						// removeList.add(productFixed);
					 }
				 }
				 if (removeList.size()==productList.size()) {
					 removeList.clear();
				 }
			 }
		}
	}
	productList.removeAll(removeList);
    TotalUtil.availableOffersMT(productList, Constant.BE_1069);
    }

    /***
     * 
     * Si el cliente excede su límite crediticio móvil no puede adquirir el producto
     * (*Importante).
     * 
     * @param fixedParkDto
     * @param productList
     * @param mobileDevices
     * @param commercialInfo
     */
    private void haveLimitMobileCredit(List<ProductFixedDTO> productList,
	    CommercialOperationInfo commercialInfo) {

	List<ProductFixedDTO> removeList = new ArrayList<>();
	if (!productList.isEmpty()) {
	    List<CommercialOperation> filteredMobilelist = UtilCollections.getCollectionByType(commercialInfo.getCommercialOpers(),
		    Constant.MOBILE);
	    List<Double> creditLimits = new ArrayList<>();
	    for (int i = 0; i < filteredMobilelist.size(); i++) {
		creditLimits.add(filteredMobilelist.get(i).getCreditData().getCreditLimit());
	    }

	    double clientCreditLimit = Collections.max(creditLimits);
	    removeFromProductList(productList, clientCreditLimit, removeList);
	}
	TotalUtil.availableOffersMT(productList, Constant.BE_1070);
    }

    /***
     * Método que se encarga de remover elementos de la lista de productos acorde al
     * limite crediticio del cliente.
     * 
     * @param productList
     * @param clientCreditLimit
     * @param removeList
     */
    private void removeFromProductList(List<ProductFixedDTO> productList, double clientCreditLimit, List<ProductFixedDTO> removeList) {
	for (ProductFixedDTO productFixed : productList) {
	    if (clientCreditLimit < (productFixed.getMobil0Rent() + productFixed.getMobil1Rent())) {
		removeList.add(productFixed);
	    }
	}
	productList.removeAll(removeList);
    }

    @Override
    public List<ProductFixedDTO> execute(FixedDTO fixedParkDto, List<ProductFixedDTO> productList, List<MobileDTO> mobileDevices,
	    CommercialOperationInfo commercialInfo, boolean provideOrPortability) {
	// TODO Auto-generated method stub
	List<ProductFixedDTO> filteredProductList = new ArrayList<>();
	
	
	/* sub regla 1 - Cobertura Iquitos */
	haveIquitosCoverage(fixedParkDto, productList);
	if (!provideOrPortability) {
	     /*sub regla 3 - Paquete PS */
	    havePSPackage(fixedParkDto, productList);
	}
	/* sub regla 2 - Cobertura diferente a HFC o FTTH */
	haveCoverageHfcOrFtth(productList, commercialInfo, fixedParkDto);
	/* sub regla 5 - Limite de credito movil del cliente */
	haveLimitMobileCredit(productList, commercialInfo);
	/* ¿Tiene ofertas disponibles? */
	filteredProductList.addAll(productList);
	return filteredProductList;
    }

    @Override
    public List<ProductFixedDTO> executeLMAMT(FixedDTO fixedParkDto, List<ProductFixedDTO> productList,
	    List<ProductFixedDTO> productLMAList, List<MobileDTO> mobileDevices, CommercialOperationInfo commercialInfo,
	    boolean provideOrPortability, ProductFixedDTO product_current) {
	// TODO Auto-generated method stub
	return null;
    }

    

    

}
