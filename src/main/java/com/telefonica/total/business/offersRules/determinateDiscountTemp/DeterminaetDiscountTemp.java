package com.telefonica.total.business.offersRules.determinateDiscountTemp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.common.util.UtilCollections;
import com.telefonica.total.dto.BeanMapper;
import com.telefonica.total.dto.CatalogDiscountTempDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.generic.CommOperCollection;
import com.telefonica.total.generic.CatalogDiscountCollection;
import com.telefonica.total.model.CatalogDiscountTemp;
import com.telefonica.total.model.ParamMovTotal;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.CommercialOperationInfo;
import com.telefonica.total.pojo.req.Customer;
import com.telefonica.total.pojo.req.Information;
import com.telefonica.total.repository.CatalogDiscountTempRepo;
import com.telefonica.total.repository.ParamMovTotalRepo;

@Service
public class DeterminaetDiscountTemp implements IDeterminateDiscountTemp{

    @Autowired
    private CatalogDiscountTempRepo catalogDiscountTempRepo;
    
    @Autowired
    private BeanMapper beanMap;
    
    @Autowired
    private ParamMovTotalRepo paramMovTotalRepo;
    
    @Override
    public void execute(CommercialOperationInfo commercialOperationInfo, Customer customer,
	    List<ProductFixedDTO> priorizationList, boolean existeUpFront) {
    	
    	List<Information> listAOI = commercialOperationInfo.getAdditionalOperationInformation() != null ? commercialOperationInfo.getAdditionalOperationInformation() : null;
    	if(existeUpFront) {
        	if (!customer.getIsEmployee()) {
        	    CommercialOperation comFija = UtilCollections.getCollectionByType(commercialOperationInfo.getCommercialOpers(), Constant.FIX)
        		    .get(0);
        	    String origenProducto = null;
        	    if (comFija.getSubscriber() != null && comFija.getSubscriber().getType() != null) {
        		origenProducto = comFija.getSubscriber().getType();
        	    }
        	    List<CatalogDiscountTemp> temporalDiscounts = catalogDiscountTempRepo.findAll();
           
        	    List<CatalogDiscountTempDTO> temporalDiscountFilter = new ArrayList<CatalogDiscountTempDTO>();
        	    for (CatalogDiscountTemp catDiscTemp : temporalDiscounts) {
        		CatalogDiscountTempDTO cdtd = beanMap.catalogDiscountTempToDto(catDiscTemp);
        		temporalDiscountFilter.add(cdtd);
        	    }
        	    CatalogDiscountCollection<CatalogDiscountTempDTO> catDisColl = new CatalogDiscountCollection<CatalogDiscountTempDTO>(temporalDiscountFilter);
        	    
        	    catDisColl.filterByType("DESCUENTO");
        	    for (ProductFixedDTO productFixedDTO : priorizationList) {
        		for (CatalogDiscountTemp catDiscTemp : catDisColl) {
        		    if (evaluateCatDiscTempWithProductFixedDto(catDiscTemp, productFixedDTO)
        			    && evaluateCustomerWithCatDiscTemp(customer, catDiscTemp)
        			    && evaluateCommercialOperationInfoWithCatDiscTemp(commercialOperationInfo, catDiscTemp)
        			    && evaluateComFijaWithCatDiscTemp(comFija, catDiscTemp)
        			    && (origenProducto == null
        				    || (catDiscTemp.getOriginProduct() == null || catDiscTemp.getOriginProduct().trim().isEmpty())
        				    || (origenProducto.equals(catDiscTemp.getOriginProduct())))) {
        
        			CatalogDiscountTempDTO catDisTemp = beanMap.catalogDiscountTempToDto(catDiscTemp);
        			productFixedDTO.setCatalogDiscountTempDTO(catDisTemp);
        			productFixedDTO.setCampaign(catDisTemp.getCampain());			
        		    }
        		}
        	    }
        	}
	
    	}
	
	
	//Añadiendo validacion para Campaña Playa Movistar Total
	
	ParamMovTotal pmt = paramMovTotalRepo.findByCode(Constant.PRODUCTO_PLAYA);
	Information info = null;
	
	if(listAOI!=null) {
		if(listAOI.size()>0) {
			for(Information inf : listAOI) {
				if(inf.getCode().equals(Constant.PRODUCTO_PLAYA)) {
					info = inf;
					break;
				}
			}
		}
	}
	

	for (ProductFixedDTO productFixedDTO : priorizationList) {

	for (ProductFixedDTO pfd : priorizationList) {


		if(info != null) {
			if(info.getCode().equals(pmt.getCodValor())) {
				if(info.getValue().equals(pmt.getValor())) {

					productFixedDTO.setCampaign(pmt.getCol1());
					pfd.setCampaign(pmt.getCol1());

				}
			}
	    }
	}
	}
	
    }

    private Boolean evaluateCatDiscTempWithProductFixedDto(CatalogDiscountTemp catDiscTemp, ProductFixedDTO productFixedDTO) {

	//System.out.println("productFixedDTO.getPackagePs() => " + productFixedDTO.getPackagePs());
	//System.out.println("productFixedDTO.getPoPackage() => " + productFixedDTO.getPoPackage());
	//System.out.println("productFixedDTO.getPackageMobilCode() => " + productFixedDTO.getPackageMobilCode());
	//System.out.println("productFixedDTO.getInternetDestinyTechnology() => " + productFixedDTO.getInternetDestinyTechnology());
	//System.out.println("productFixedDTO.getJump() => " + productFixedDTO.getJump());
	
	return ((catDiscTemp.getOfferPS() == null || productFixedDTO.getPackagePs().intValue() == catDiscTemp.getOfferPS().intValue())
		&& ((catDiscTemp.getOfferPO() == null || catDiscTemp.getOfferPO().trim().isEmpty())
			|| productFixedDTO.getPoPackage().equals(catDiscTemp.getOfferPO()))
		&& ((catDiscTemp.getOfferBO() == null || catDiscTemp.getOfferBO().trim().isEmpty())
			|| productFixedDTO.getPackageMobilCode().equals(catDiscTemp.getOfferBO()))
		&& (catDiscTemp.getInitDate() != null && catDiscTemp.getEndDate() != null
			&& TotalUtil.isCurrentDateBetweenDates(catDiscTemp.getInitDate(), catDiscTemp.getEndDate()))
		&& ((catDiscTemp.getOfferTechnology() == null || catDiscTemp.getOfferTechnology().trim().isEmpty())
			|| (catDiscTemp.getOfferTechnology().equals(productFixedDTO.getInternetDestinyTechnology())))
		&& (catDiscTemp.getDiscountAmount() > Constant.CERO) && (productFixedDTO.getJump() > catDiscTemp.getJump()));
    }

    private Boolean evaluateCustomerWithCatDiscTemp(Customer customer, CatalogDiscountTemp catDiscTemp) {
	return ((customer.getDepartment() == null || customer.getDepartment().trim().isEmpty())
		|| (catDiscTemp.getDepartmemt() == null || catDiscTemp.getDepartmemt().trim().isEmpty())
		|| (customer.getDepartment().equals(catDiscTemp.getDepartmemt())))
		&& ((customer.getProvince() == null || customer.getProvince().trim().isEmpty())
			|| (catDiscTemp.getProvince() == null || catDiscTemp.getProvince().trim().isEmpty())
			|| (customer.getProvince().equals(catDiscTemp.getProvince())))
		&& ((customer.getDistrict() == null || customer.getDistrict().trim().isEmpty())
			|| (catDiscTemp.getDistrict() == null || catDiscTemp.getDistrict().trim().isEmpty())
			|| (customer.getDistrict().equals(catDiscTemp.getDistrict())));
    }

    private Boolean evaluateComFijaWithCatDiscTemp(CommercialOperation comFija, CatalogDiscountTemp catDiscTemp) {
	return ((comFija.getOperation() == null || comFija.getOperation().trim().isEmpty())
		|| (catDiscTemp.getOriginOpeCommercial() == null || catDiscTemp.getOriginOpeCommercial().trim().isEmpty())
		|| (comFija.getOperation().equals(catDiscTemp.getOriginOpeCommercial())))
		&& ((comFija.getProduct() == null || comFija.getProduct().trim().isEmpty())
			|| (catDiscTemp.getOriginTypeProduct() == null || catDiscTemp.getOriginTypeProduct().trim().isEmpty())
			|| (comFija.getProduct().equals(catDiscTemp.getOriginTypeProduct())));
    }

    private Boolean evaluateCommercialOperationInfoWithCatDiscTemp(CommercialOperationInfo commercialOperationInfo,
	    CatalogDiscountTemp catDiscTemp) {
	return ((commercialOperationInfo.getSalesChannelType() == null || commercialOperationInfo.getSalesChannelType().trim().isEmpty())
		|| (catDiscTemp.getSalesChannel() == null || catDiscTemp.getSalesChannel().trim().isEmpty())
		|| (commercialOperationInfo.getSalesChannelType().equals(catDiscTemp.getSalesChannel())));
    }
}
