package com.telefonica.total.business.offersRules.equipmentValidations;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.model.CatalogDeco;
import com.telefonica.total.model.CatalogModem;
import com.telefonica.total.pojo.req.CommercialOperationInfo;

public interface IEquipmentValidations {

    /***
     * Método que se encarga de recorrer todos la lista de productos priorizados y
     * evaluar las validaciones de equipos para decodificadores y modems.
     * 
     * @param priorizationList
     */
    void execute(List<CatalogDeco> catalogDecoList, List<CatalogModem> catalogModemList, List<ProductFixedDTO> priorizationList, FixedDTO fixedParkDto, CommercialOperationInfo commercialInfo,
	    boolean provideOrPortability);
    
    /**
     * Método del flujo de gestion de planta que se encarga de recorrer todos la
     * lista de productos priorizados y evaluar las validaciones de equipos para
     * decodificadores y modems.
     * 
     * @param priorizationList
     * @param fixedParkDto
     * @param commercialInfo
     * @param distinctPS
     */
    void executeMt(List<CatalogDeco> catalogDecoList, List<CatalogModem> catalogModemList,List<ProductFixedDTO> priorizationList, FixedDTO fixedParkDto,
	    CommercialOperationInfo commercialInfo, boolean distinctPS, String modemEquipmentOfferCopy);
}
