package com.telefonica.total.business.offersRules.validatePsAndBo;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.ProductFixedDTO;

public interface IValidatePsAndBo {

    /***
     * Metodo que se encarga de validar las Ps y las Bo de las ofertas de MT, para
     * determinar cual es el flujo de priorizacion que deberian seguir.
     * 
     * @param fixedParkDto
     * @param mobileDevices
     * @param allProductsMT
     */
    void executeMt(FixedDTO fixedParkDto, List<MobileDTO> mobileDevices, List<ProductFixedDTO> allProductsMT);
}
