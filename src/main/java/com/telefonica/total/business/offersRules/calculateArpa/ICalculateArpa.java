package com.telefonica.total.business.offersRules.calculateArpa;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;

public interface ICalculateArpa {

    /***
     * Método que se encarga de calcular el ARPA del cliente actual.
     * 
     * @param fixedParkDto
     * @param mobileDevices
     */
    void execute(FixedDTO fixedParkDto, List<MobileDTO> mobileDevices);
}
