package com.telefonica.total.business.offersRules.determinatePresentRents;

import com.telefonica.total.dto.FixedDTO;

public interface IDeterminatePresentRents {

    /***
     * Método que se encargar de determinar las rentas presentes y la sumatoria de
     * las rentas presentes.
     * 
     * @param fixedParkDto
     */
    void execute(FixedDTO fixedParkDto);
    
    /***
     * Método que se encargar de determinar las rentas presentes y la sumatoria de
     * las rentas presentes.
     * 
     * @param fixedParkDto
     * @param distinctPS
     */
    void executeMt(FixedDTO fixedParkDto, boolean distinctPS);
}
