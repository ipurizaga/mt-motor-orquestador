package com.telefonica.total.business.offersRules.equipmentValidations;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.RulesValueProp;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.model.CatalogDeco;
import com.telefonica.total.model.CatalogModem;
import com.telefonica.total.model.InternetTechnology;
import com.telefonica.total.model.ParamMovTotal;
import com.telefonica.total.pojo.req.CommercialOperationInfo;
import com.telefonica.total.pojo.req.Information;
import com.telefonica.total.repository.CatalogInternetTechnology;
import com.telefonica.total.repository.ParamMovTotalRepo;

@Service
@Qualifier("plantMt")
public class EquipmentValidationsMt implements IEquipmentValidations{

    @Autowired
    private RulesValueProp rulesValue;
    
    @Autowired
    private CatalogInternetTechnology technology;
    
    @Autowired
    private ParamMovTotalRepo paramMovTotalRepo;
    
    @Override
    public void executeMt(List<CatalogDeco> catalogDecoList, List<CatalogModem> catalogModemList,List<ProductFixedDTO> priorizationList, FixedDTO fixedParkDto,
	    CommercialOperationInfo commercialInfo, boolean distinctPS, String modemEquipmentOfferCopy) {
    	
	List<ParamMovTotal> listParamMovTotal = paramMovTotalRepo.findAll();
	
	String pp = TotalUtil.getValuePlayas(commercialInfo);
	
	boolean esCopia = false;
	
	@SuppressWarnings("unused")
	boolean existeUW = activateUltraWifi(listParamMovTotal), aplicaCambioTech = false;
	
	Integer velocidad = 0, psOffer =0;
	//String psOffer = "";
	int contar = 0, contarProdAct=0, lastOffer;
	
	lastOffer = priorizationList.size();	
	
	for (ProductFixedDTO product : priorizationList) {
	    
	    contar++;
	    
	    boolean prodActual = false;
		
		for (Information info : product.getCharacteristics()) {
		    if (info.getValue().equals(rulesValue.getPRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT())) {
			
			prodActual = true;
			contarProdAct++;
			break;
			
		    }
		}
		
		if(!prodActual) {
		    
		    if(!(product.getCurrentProduct() == null)){
            		    if(product.getCurrentProduct().equals("Producto Actual")){
            			prodActual = true;
            			contarProdAct++;
            		    }
		    }
		    
		}
		if(contar == lastOffer) {
		    //System.out.println("CONTANDO");
		}
		
	    if(contarProdAct > 1) {
		prodActual = false;
	    }
		
	    determinateInternetTecnology(product, fixedParkDto, commercialInfo);
	    if (!distinctPS && prodActual && contarProdAct == 1) {
		product.setModemEquipment(fixedParkDto.getModenNameInternet());
		velocidad = product.getInternetSpeed();
		psOffer = product.getPackagePs();
		flagDeco(catalogDecoList, product, fixedParkDto);
	    }else if (!distinctPS && !prodActual) {
		flagDeco(catalogDecoList, product, fixedParkDto);
		
		if(velocidad.equals(product.getInternetSpeed())) {
		    
		    boolean isOC= false;
		    
		    if(!(product.getEnabled().equals(Constant.ONE))) {
		    
        		    for (Information information : product.getCharacteristics()) {
                		if(information.getValue().equals(rulesValue.getPRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT())) {
                		    if(product.getInternetDestinyTechnology().equals("HFC")) {
                			/*billingOffersInfo.add(Information.builder().code(ResponseConstants.INTERNET_TECNOLOGIADESTINO)
                	        		    .value("FTTH").build());*/
                			isOC = true;
                		    }
                		}		
        		    
        		    }
        		    
        		    if(!isOC) {
        			if(product.getInternetDestinyTechnology().equals("HFC")) {
        			    isOC = true;
        			}
        		    }
        		    
        		    esCopia = true;
        		    aplicaCambioTech = true;
        		    flagModem(catalogModemList, product, fixedParkDto, existeUW, listParamMovTotal, pp, commercialInfo, esCopia, isOC, contar, aplicaCambioTech);
		    
		    }else {
			
				if(psOffer.equals(product.getPackagePs())) {
			
                			for (Information information : product.getCharacteristics()) {
                                		if(information.getValue().equals(rulesValue.getPRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT())) {
                                		    if(product.getInternetDestinyTechnology().equals("HFC")) {
                                			/*billingOffersInfo.add(Information.builder().code(ResponseConstants.INTERNET_TECNOLOGIADESTINO)
                                	        		    .value("FTTH").build());*/
                                			isOC = true;
                                		    }
                                		}		
                    		    
                    		    	}
                    		    
                        		    if(!isOC) {
                        			if(product.getInternetDestinyTechnology().equals("HFC")) {
                        			    isOC = true;
                        			}
                        		    }
                        		    
                        		    if(isOC && product.getInternetTechnology().equals("HFC")) {
                        			isOC = true;
                        		    }else {
                        			isOC = false;
                        		    }
                        		    
                        		esCopia = true;
                        		aplicaCambioTech = true;
            				flagModem(catalogModemList, product, fixedParkDto, existeUW, listParamMovTotal, pp, commercialInfo, esCopia, isOC, contar, aplicaCambioTech); 
                        		   
                		}else {
                		    	isOC = true;
                		    	esCopia = true;
                		    	aplicaCambioTech = false;
        				flagModem(catalogModemList, product, fixedParkDto, existeUW, listParamMovTotal, pp, commercialInfo, esCopia, isOC, contar, aplicaCambioTech); 
                		    
                		}
			
				
		    	}
		    
		    
		}else {
		    esCopia = false;
		    flagModem(catalogModemList, product, fixedParkDto, existeUW, listParamMovTotal, pp, commercialInfo, esCopia, false, contar, aplicaCambioTech);
		}
		
	    }
	    
	    
	}
	
	//System.out.println("VALIDANDO");

    }
    
    /***
     * Método que se encarga de indicar con que tecnologia se le venderá al cliente.
     */
    private void determinateInternetTecnology(ProductFixedDTO product, FixedDTO fixedParkDto, CommercialOperationInfo commercialInfo) {

	List<InternetTechnology> matrix = technology.findAll();
	List<String> matrixTechnology = new ArrayList<>();
	for(InternetTechnology m : matrix) {
		if(m.getInternetTechnology()!=null)
		matrixTechnology.add(m.getInternetTechnology());
	}
	
	String realTechnology = Constant.CERO_INT.toString().equals(fixedParkDto.getTechnologyInternet()) ? null
		: fixedParkDto.getTechnologyInternet();
	String realCoverage = obtainRealCoverage(fixedParkDto, commercialInfo, realTechnology);
	
	if(realTechnology != null) {
		if(realTechnology.equals("HFC") && (fixedParkDto.getHfcCobInternet() == null? "": fixedParkDto.getHfcCobInternet()).toString().equals("2")) {
			realCoverage = "FTTH";
		}
	}

	
	/*for (InternetTechnology intenerTechnology : matrix) {
	    String matrixActualTechnology = intenerTechnology.getInternetTechnology().equals("NULL") ? null
		    : intenerTechnology.getInternetTechnology();
	    String matrixCoverage = intenerTechnology.getCoverage().equals("NULL") ? null : intenerTechnology.getCoverage();*/
	    
	    if(product.getCharacteristics() != null) {
		
		boolean prodActual = false;
		
		for (Information info : product.getCharacteristics()) {
		    if (info.getValue().equals(rulesValue.getPRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT())
        			&& fixedParkDto.getPriorizationFlux()==1) {
			
			prodActual = true;
			
			break;
			
		    }
		}
		
		if (prodActual) {
		    product.setInternetDestinyTechnology(fixedParkDto.getTechnologyInternet());
		}else {
		    determinateInternetDestinyTechnology(matrixTechnology, realTechnology, realCoverage, product, fixedParkDto);
		}
	    }else {
		determinateInternetDestinyTechnology(matrixTechnology, realTechnology, realCoverage, product, fixedParkDto);
	    }	    
	    
	//}
    }
    
    private String obtainRealCoverage(FixedDTO fixedParkDto, CommercialOperationInfo commercialInfo, String realTechnology) {
	if (rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(realTechnology)
		&& rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(commercialInfo.getCoverage().toLowerCase().equals("fiber")?"FTTH":commercialInfo.getCoverage())) {
	    return Constant.ONE.equals(fixedParkDto.getHfcCobInternet()) ? rulesValue.getINTERNET_TECNOLOGIA_HFC()
		    : rulesValue.getINTERNET_TECNOLOGIA_FTTH();
	} else {
	    return commercialInfo.getCoverage().toLowerCase().equals("fiber")?"FTTH":commercialInfo.getCoverage();
	}
    }
    
    /***
     * Método que se encarga de modificar la TECNOLOGIA DE INTERNET de destino por criterios
     * Criterio Actual: Si Tecnología actual es HFC y Tecnología destino es FTTH
     * UMBRAL Actual  : 100 MB 
     * @param matrixActualTechnology ---> Todas las tecnologías actuales
     * @param realTechnology		 ---> Tecnologia Actual
     * @param matrixCoverage		 ---> Todas las coberturas
     * @param realCoverage		 	 ---> Cobertura Actual
     * @param intenerTechnology		 ---> Iterador de Tecnologias de Internet
     */
    private void determinateInternetDestinyTechnology(List<String> matrixTechnology, String realTechnology,
	    String realCoverage, ProductFixedDTO product, FixedDTO fixedParkDto) {
	
	boolean prodActual = false, prodActualSel = false;
	
	ParamMovTotal paramMovSelected = new ParamMovTotal();
	
	List<ParamMovTotal> listParamMov = new ArrayList<ParamMovTotal>();
	
	listParamMov = paramMovTotalRepo.findAll();
	
	for (ParamMovTotal paramMovTotal : listParamMov) {
	    if(paramMovTotal.getCodValor().equals("um_tecn")) {
		paramMovSelected = paramMovTotal;
		break;
	    }
	}
	
	for (Information info : product.getCharacteristics()) {
	    if (info.getValue().equals(rulesValue.getPRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT())) {
		
		prodActual = true;
		
		break;
		
	    }
	}
	
	if(product.getCurrentProduct()!=null || prodActual) {
	    
	    prodActualSel = true;
	    
	}
	
	if(!prodActualSel) {
	    	
	    if(realTechnology!=null) {
	    
	    	if (matrixTechnology.contains(realTechnology) && matrixTechnology.contains(realCoverage)) {
        	    if (rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(realTechnology)
        		    && rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(realCoverage)
        		    && TotalUtil.getNumericValueOf(product.getInternetSpeed()) >= TotalUtil.getNumericValueOf(paramMovSelected.getValor())) {

        		product.setInternetDestinyTechnology(rulesValue.getINTERNET_TECNOLOGIA_FTTH());
        	    } else if (rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(realTechnology)
        		    && rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(realCoverage)
        		    && TotalUtil.getNumericValueOf(product.getInternetSpeed()) < TotalUtil.getNumericValueOf(paramMovSelected.getValor())) {
        		product.setInternetDestinyTechnology(rulesValue.getINTERNET_TECNOLOGIA_HFC());
        	    } else {
        		product.setInternetDestinyTechnology(realCoverage);
        	    }
        	}else {
        	    product.setInternetDestinyTechnology(rulesValue.getINTERNET_TECNOLOGIA_HFC());
        	}
	    	
	    } else {
            	if(matrixTechnology.contains(realCoverage)) {
		    if (rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(realTechnology)
    			    && rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(realCoverage)
    			    && TotalUtil.getNumericValueOf(product.getInternetSpeed()) >= Integer.parseInt(paramMovSelected.getValor())) {
    			product.setInternetDestinyTechnology(rulesValue.getINTERNET_TECNOLOGIA_FTTH());
    		} else if (rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(realTechnology)
    			    && rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(realCoverage)
    			    && TotalUtil.getNumericValueOf(product.getInternetSpeed()) < Integer.parseInt(paramMovSelected.getValor())) {
    			product.setInternetDestinyTechnology(rulesValue.getINTERNET_TECNOLOGIA_HFC());
    		} else {
    			product.setInternetDestinyTechnology(realCoverage);
    		}
            }
	    }
	
	}else {
	    //product.setInternetDestinyTechnology(product.getInternetTechnology());
	    product.setInternetDestinyTechnology(fixedParkDto.getTechnologyInternet());
	}
    }


    private boolean compare(String str1, String str2) {
	return (str1 == null ? str2 == null : str1.equals(str2));
    }
    
    /***
     * Metodo que se encarga de verificar si el producto cuenta con el flag regla
     * deco
     * 
     * @param product
     * @param fixedParkDto
     */
    private void flagDeco(List<CatalogDeco> catalogDecoList, ProductFixedDTO product, FixedDTO fixedParkDto) {
	if (product.getFlagRuleDeco().equals(rulesValue.getFLAG_RULE_DECO_ONE())) {
	    moreThan6Decos(catalogDecoList, product, fixedParkDto);
	}
    }
    
    /***
     * Método que se encarga de verificar si la cantidad de decodificadores del
     * cliente es mayor que 6.
     * 
     * @param product
     * @param fixedParkDto
     */
    private void moreThan6Decos(List<CatalogDeco> catalogDecoList, ProductFixedDTO product, FixedDTO fixedParkDto) {
	int decoQuantity = TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantitydigitalDeco())
		+ TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoHd())
		+ TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart())
		+ TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoDvr());
	if (!(decoQuantity > rulesValue.getDECO_QUANTITY_SIX()
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) > 0)) {
	    decoEquipmentValidations(catalogDecoList, product, fixedParkDto);
	}
    }
    
    /***
     * Método que se encarga de definir cuales seran los decodificadores asignados
     * segun las reglas de negocio y las rentas.
     * 
     * @param product
     * @param fixedParkDto
     */
    /*private void decoEquipmentValidations(ProductFixedDTO product, FixedDTO fixedParkDto) {
	/* Sin deco(altas nuevas) /
	determinateEquipmentValidationWithOutDeco(product, fixedParkDto);
	/* Solo tiene decos digitales /
	determinateEquipmentValidationWithOnlyDigitalDecos(product, fixedParkDto);
	/* Sin tiene digital hd y no tienen smart /
	determinateEquipmentValidationWithHdDigitalAndWithOutSmart(product, fixedParkDto);
	/* Si solo tiene hd /
	determinateEquipmentValidationWithOnlyHd(product, fixedParkDto);
	/* Si solo tiene hd y smart y no tiene digital /
	determinateEquipmentWithHdAndSmartWithOutDigital(product, fixedParkDto);
	/* Si solo tiene smart /
	determinateEquipmentWithOnlySmart(product, fixedParkDto);
    }*/
    
    private void decoEquipmentValidations(List<CatalogDeco> catalogDecoList, ProductFixedDTO product, FixedDTO fixedParkDto) {
        for (CatalogDeco catalogDeco : catalogDecoList) {
        	    
        	    if (catalogDeco.getRulerActive().equals("1")) {
        	
        		determinateEquipmentValidationDeco(catalogDeco, product, fixedParkDto);
        	    
        	    }
        	    
                	
        	}
    }
    
private void determinateEquipmentValidationDeco(CatalogDeco catalogDeco, ProductFixedDTO product, FixedDTO fixedParkDto) {
	
	int flagDDF = 0, flagHDF = 0, flagSDF = 0, total = 0;
	
	if(catalogDeco.getDigitalDecoFlag().equals("1")) {
	    
	    flagDDF = fixedParkDto.getCableQuantitydigitalDeco();
	    
	}
	
	if(catalogDeco.getHdDecoFlag().equals("1")) {
	    
	    flagHDF = fixedParkDto.getCableQuantityDecoHd();
	    
	}
	
	if(catalogDeco.getSmartwifiDecoFlag().equals("1")) {
	    
	    flagSDF = fixedParkDto.getCableQuantityDecoSmart();
	    
	}
	
	total = flagDDF + flagHDF + flagSDF;
	
	if(catalogDeco.getOfferDecoConditional().equals("=")) {
	    //if(total == TotalUtil.getNumericValueOf(catalogDeco.getOfferDecoQuantityConditional())) {
	    if(Integer.valueOf(total) == Integer.valueOf(catalogDeco.getOfferDecoQuantityConditional())) {
		
		if(catalogDeco.getRentVaild().equals("0")) {
		    
        		product.setEquipmentDecoQuantity(TotalUtil.getNumericValueOf(catalogDeco.getOfferDecoQuantity()));
        		product.setDecoEquipment(catalogDeco.getOfferDeco());	
		
		}else {
		    
		    	if(catalogDeco.getConditionalRent().equals("<=")) {
		    	    //if(product.getPackageRent() <= TotalUtil.getNumericValueOf(catalogDeco.getRent())) {
		    	    if(product.getPackageRent() <= Double.valueOf(catalogDeco.getRent())) {
		    		product.setEquipmentDecoQuantity(TotalUtil.getNumericValueOf(catalogDeco.getOfferDecoQuantity()));
	        		product.setDecoEquipment(catalogDeco.getOfferDeco());	
		    	    }
		    	}else {
		    	    if(product.getPackageRent() > Double.valueOf(catalogDeco.getRent())) {
		    		product.setEquipmentDecoQuantity(TotalUtil.getNumericValueOf(catalogDeco.getOfferDecoQuantity()));
	        		product.setDecoEquipment(catalogDeco.getOfferDeco());	
		    	    }
		    	}
		}
	    }
	}
	
	if(catalogDeco.getOfferDecoConditional().equals(">")) {
	    if(Integer.valueOf(total) > Integer.valueOf(catalogDeco.getOfferDecoQuantityConditional())) {
        	    if(catalogDeco.getRentVaild().equals("0")) {
                	    	
                        		product.setEquipmentDecoQuantity(TotalUtil.getNumericValueOf(catalogDeco.getOfferDecoQuantity()));
                        		product.setDecoEquipment(catalogDeco.getOfferDeco());
                	    		    
        	    }else {
            			if(catalogDeco.getConditionalRent().equals("<=")) {
            			    
                	    	    if(product.getPackageRent() <= Double.valueOf(catalogDeco.getRent())) {
                	    		product.setEquipmentDecoQuantity(TotalUtil.getNumericValueOf(catalogDeco.getOfferDecoQuantity()));
                        		product.setDecoEquipment(catalogDeco.getOfferDeco());	
                	    	    }
                	    	}else {
                	    	    if(product.getPackageRent() > Double.valueOf(catalogDeco.getRent())) {
                	    		product.setEquipmentDecoQuantity(TotalUtil.getNumericValueOf(catalogDeco.getOfferDecoQuantity()));
                        		product.setDecoEquipment(catalogDeco.getOfferDeco());	
                	    	    }
                	    	}
        	    }
        	    
	    }
	    
	}
	
	if(catalogDeco.getOfferDecoConditional().equals(">=")) {
	    if(Integer.valueOf(total) >= Integer.valueOf(catalogDeco.getOfferDecoQuantityConditional())) {
        	    if(catalogDeco.getRentVaild().equals("0")) {
        	    
                	    
                		product.setEquipmentDecoQuantity(TotalUtil.getNumericValueOf(catalogDeco.getOfferDecoQuantity()));
                		product.setDecoEquipment(catalogDeco.getOfferDeco());
                	    
        	    
        	    }
	    }	
	}/*else {
		if(catalogDeco.getConditionalRent().equals("<=")) {
	    	    if(product.getPackageRent() <= Double.valueOf(catalogDeco.getRent())) {
	    		product.setEquipmentDecoQuantity(TotalUtil.getNumericValueOf(catalogDeco.getOfferDecoQuantity()));
        		product.setDecoEquipment(catalogDeco.getOfferDeco());	
	    	    }
	    	}else {
	    	    if(!(total == TotalUtil.getNumericValueOf(catalogDeco.getOfferDecoQuantityConditional()))) {
        	    	    if(product.getPackageRent() > Double.valueOf(catalogDeco.getRent())) {
        	    		product.setEquipmentDecoQuantity(TotalUtil.getNumericValueOf(catalogDeco.getOfferDecoQuantity()));
                		product.setDecoEquipment(catalogDeco.getOfferDeco());	
        	    	    }
	    	    }
	    	}
	}*/
	
    }
    
    private void determinateEquipmentValidationWithOutDeco(ProductFixedDTO product, FixedDTO fixedParkDto) {
	if (TotalUtil.getNumericValueOf(fixedParkDto.getCablePresent()) == 0) {
	    if (product.getPackageRent() <= 299) {
		product.setEquipmentDecoQuantity(2);
		product.setDecoEquipment(rulesValue.getDECO_EQUIPMENT_2HD());
	    }
	    if (product.getPackageRent() > 299) {
		product.setEquipmentDecoQuantity(2);
		product.setDecoEquipment(rulesValue.getDECO_EQUIPMENT_SMART_HD());
	    }
	}
    }

    private void determinateEquipmentValidationWithOnlyDigitalDecos(ProductFixedDTO product, FixedDTO fixedParkDto) {
	if (TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantitydigitalDeco()) > 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoHd()) == 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) == 0 && product.getPackageRent() <= 299) {
	    product.setEquipmentDecoQuantity(1);
	    product.setDecoEquipment(rulesValue.getDECO_EQUIPMENT_1HD());
	}
	if (TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantitydigitalDeco()) > 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoHd()) == 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) == 0 && product.getPackageRent() > 299) {
	    product.setEquipmentDecoQuantity(1);
	    product.setDecoEquipment(rulesValue.getDECO_EQUIPMENT_SMART());
	}
    }

    private void determinateEquipmentValidationWithHdDigitalAndWithOutSmart(ProductFixedDTO product, FixedDTO fixedParkDto) {
	if (TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantitydigitalDeco()) > 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoHd()) > 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) == 0 && product.getPackageRent() <= 299) {
	    product.setEquipmentDecoQuantity(0);
	}
	if (TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantitydigitalDeco()) > 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoHd()) > 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) == 0 && product.getPackageRent() > 299) {
	    product.setEquipmentDecoQuantity(1);
	    product.setDecoEquipment(rulesValue.getDECO_EQUIPMENT_SMART());
	}
    }

    private void determinateEquipmentValidationWithOnlyHd(ProductFixedDTO product, FixedDTO fixedParkDto) {
	if (TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantitydigitalDeco()) == 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoHd()) > 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) == 0 && product.getPackageRent() <= 299) {
	    product.setEquipmentDecoQuantity(0);
	}
	if (TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantitydigitalDeco()) == 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoHd()) > 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) == 0 && product.getPackageRent() > 299) {
	    product.setEquipmentDecoQuantity(1);
	    product.setDecoEquipment(rulesValue.getDECO_EQUIPMENT_SMART());
	}
    }

    private void determinateEquipmentWithHdAndSmartWithOutDigital(ProductFixedDTO product, FixedDTO fixedParkDto) {
	if (TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantitydigitalDeco()) == 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoHd()) > 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) > 0 && product.getPackageRent() <= 299) {
	    product.setEquipmentDecoQuantity(0);
	}
	if (TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantitydigitalDeco()) == 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoHd()) > 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) > 0 && product.getPackageRent() > 299) {
	    product.setEquipmentDecoQuantity(1);
	    product.setDecoEquipment(rulesValue.getDECO_EQUIPMENT_SMART());
	}
    }

    private void determinateEquipmentWithOnlySmart(ProductFixedDTO product, FixedDTO fixedParkDto) {
	if (TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantitydigitalDeco()) == 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoHd()) == 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) > 0 && product.getPackageRent() <= 299) {
	    product.setEquipmentDecoQuantity(0);
	}
	if (TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantitydigitalDeco()) == 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoHd()) == 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) > 0 && product.getPackageRent() > 299) {
	    product.setEquipmentDecoQuantity(1);
	    product.setDecoEquipment(rulesValue.getDECO_EQUIPMENT_SMART());
	}
    }
    
    /***
     * Método que se encarga de verificar si el producto cuenta con el flag regla
     * modem
     * 
     * @param product
     * @param fixedParkDto
     */
    private void flagModem(List<CatalogModem> catalogModemList, ProductFixedDTO product, FixedDTO fixedParkDto, boolean existeUW, List<ParamMovTotal> listParamMovTotal, String pp, CommercialOperationInfo commercialInfo, boolean esCopia, boolean isOC, int contar, boolean cambioTecnologia) {
	if (product.getFlagRuleModem().equals(rulesValue.getFLAG_RULE_MODEM_ONE())) {
	    modemEquipmentValidations(catalogModemList, product, fixedParkDto, existeUW, listParamMovTotal, pp, commercialInfo, esCopia, isOC, contar, cambioTecnologia);
	}
    }
    
    /*private void modemEquipmentValidations(ProductFixedDTO product, FixedDTO fixedParkDto, boolean existeUW, List<ParamMovTotal> listParamMovTotal, String pp) {
	
	
	 Sin modem(Alta nueva) 
	if (fixedParkDto.getPresentInternet().toString().equals("0")) {
	    determinatePresentInternet(product, listParamMovTotal);
	}
	// Migracion de ADSL -> HFC /
	else if (rulesValue.getINTERNET_TECNOLOGIA_ADSL().equals(fixedParkDto.getTechnologyInternet())
		&& rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(product.getInternetDestinyTechnology())) {
	    determinateInternetTecnologyAdsl(product, fixedParkDto, listParamMovTotal);
	}
	// Migración HFC -> HFC /
	else if (rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(fixedParkDto.getTechnologyInternet())
		&& rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(product.getInternetDestinyTechnology())) {
	    determinateInternetTecnologyHfc(product, fixedParkDto);
	}
	/ Migración ADSL -> FTTH /
	else if (rulesValue.getINTERNET_TECNOLOGIA_ADSL().equals(fixedParkDto.getTechnologyInternet())
		&& rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(product.getInternetDestinyTechnology())) {
	    determinateInternetTecnologyAdslAndFtth(product, fixedParkDto, listParamMovTotal);
	}
	/ Migración FTTH -> FTTH /
	else if (rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(fixedParkDto.getTechnologyInternet())
		&& rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(product.getInternetDestinyTechnology())) {
	    determinateInternetTecnologyFtth(product, fixedParkDto, listParamMovTotal);
	}
	/ Migración HFC -> FTTH /
	else if (rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(fixedParkDto.getTechnologyInternet())
		&& rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(product.getInternetDestinyTechnology())) {
	    determinateInternetTecnologyHfcAndFtth(product, fixedParkDto, listParamMovTotal);

	}
	
	if(existeUW) {
	
	    determinateUltraWifiRep(product, pp);
	
	}
	
    }*/
    
private void modemEquipmentValidations(List<CatalogModem> catalogModemList, ProductFixedDTO product, FixedDTO fixedParkDto, boolean existeUW, List<ParamMovTotal> listParamMovTotal, String pp, CommercialOperationInfo comercialInfo, boolean esCopia, boolean isOC, int contar, boolean cambioTecnologia) {
	
    for (CatalogModem catalogModem : catalogModemList) {
	    
	    if (catalogModem.getActiveFlag().equals("1")) {
	    
		/* Sin modem(Alta nueva) */
    	//if (fixedParkDto.getPresentInternet().toString().equals("0")) {
    	
		if (!(catalogModem.getOriginTechnology().equals("0"))) {
		    
		    if(isOC) {
			
    			    if(determinateRulesOC(product, catalogModem, fixedParkDto, comercialInfo, esCopia, contar, cambioTecnologia)){
        			break;
        		    }
    			    
		    }else {
		    
        		    if(determinateRules(product, catalogModem, fixedParkDto, comercialInfo, esCopia)){
        			break;
        		    }
        		    
		    }
		}else {
		    if(isOC) {
			
    			    if(determinatePresentInternetOC(product, catalogModem, fixedParkDto, comercialInfo, esCopia)) {
        			break;
        		    }
    			    
		    }else {
			
        		    if(determinatePresentInternet(product, catalogModem, fixedParkDto, comercialInfo, esCopia)) {
        			break;
        		    }
        		    
		    }
		    
		}
    	//}	
    	
    	
	    }
    	
	}
	
	
	if(existeUW) {
	
	    determinateUltraWifiRep(product, pp);
	
	}
	
    }

private boolean determinateRules(ProductFixedDTO product, CatalogModem catalogModem, FixedDTO fixedParkDto, CommercialOperationInfo comercialInfo, boolean esCopia) {
	
	boolean resp = false;
	
	/*if(rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(product.getInternetDestinyTechnology())) {
	   
	    	product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_GPON());       
	}else {
	    	
	product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_SMART_WIFI());
	}*/

	if(//comercialInfo.getCoverage().equals(product.getInternetDestinyTechnology()) 
		//&& 
		product.getInternetDestinyTechnology().equals(catalogModem.getDestinyTechnology())
		&& catalogModem.getOriginTechnology().equals(fixedParkDto.getTechnologyInternet())
		&& ((fixedParkDto.getModenNameInternet()!=null) && fixedParkDto.getModenNameInternet().equals(catalogModem.getTenancyModemInternet()))) {
		
	    if(catalogModem.getRentVaild().equals("1")) {
	     
		//if(catalogModem.getOfferConditionalRent().equals(">")) {
		
		if(catalogModem.getOfferConditionalRent().equals("<")) {
		//if(product.getPackageRent() > (Double.parseDouble(catalogModem.getOfferRent()))) {
		if(product.getPackageRent() < (Double.parseDouble(catalogModem.getOfferRent()))) {
		    	if(esCopia) {
		    	    //modemEquipmentOfferCopy = catalogModem.getOfferModemInternet();
		    	    fixedParkDto.setModemEquipmentOfferCopy(catalogModem.getOfferModemInternet());
		    	}else {
		    	
		    	    product.setModemEquipment(catalogModem.getOfferModemInternet());
		    	}
		    	resp = true;
	    	
		}
		
		}else {
		    
		    //if(product.getPackageRent() <= (Double.parseDouble(catalogModem.getOfferRent()))) {
		    if(product.getPackageRent() >= (Double.parseDouble(catalogModem.getOfferRent()))) {
			
			if(esCopia) {
			    //modemEquipmentOfferCopy = catalogModem.getOfferModemInternet(); 
			    fixedParkDto.setModemEquipmentOfferCopy(catalogModem.getOfferModemInternet());
			}else {
			
			    product.setModemEquipment(catalogModem.getOfferModemInternet());
			}
			resp = true;
	    	
		    }
		
	    	}
	    	
	    }else {
		if(esCopia) {
		    //modemEquipmentOfferCopy = catalogModem.getOfferModemInternet();	
		    fixedParkDto.setModemEquipmentOfferCopy(catalogModem.getOfferModemInternet());
		}else {
		
		    product.setModemEquipment(catalogModem.getOfferModemInternet());
		}
		resp = true;
	    }
	    	
	}/*else {
	    	
		product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_SMART_WIFI());
	}*/
	
	return resp;

}

private boolean determinateRulesOC(ProductFixedDTO product, CatalogModem catalogModem, FixedDTO fixedParkDto, CommercialOperationInfo comercialInfo, boolean esCopia, int contar, boolean cambioTecnologia) {
	
	boolean resp = false;
	
	String internetDestinyTechnology = "";
	
	if(cambioTecnologia) {
	    internetDestinyTechnology = "FTTH";
	}else {
	    internetDestinyTechnology = product.getInternetDestinyTechnology();
	}
	
	if(internetDestinyTechnology.equals(catalogModem.getDestinyTechnology())
		&& catalogModem.getOriginTechnology().equals(fixedParkDto.getTechnologyInternet())
		&& ((fixedParkDto.getModenNameInternet()!=null) && fixedParkDto.getModenNameInternet().equals(catalogModem.getTenancyModemInternet()))) {
		
	    if(catalogModem.getRentVaild().equals("1")) {
	     
		
		if(catalogModem.getOfferConditionalRent().equals("<")) {
		    
		if(product.getPackageRent() < (Double.parseDouble(catalogModem.getOfferRent()))) {
		    	if(esCopia) {
		    	    
		    	    fixedParkDto.setModemEquipmentOfferCopy(catalogModem.getOfferModemInternet());
		    	    fixedParkDto.setContar(Integer.valueOf(contar));
		    	
		    	}else {
		    	
		    	    product.setModemEquipment(catalogModem.getOfferModemInternet());
		    	}
		    	resp = true;
	    	
		}
		
		}else {
		    
		    if(product.getPackageRent() >= (Double.parseDouble(catalogModem.getOfferRent()))) {
			
			if(esCopia) {
			    fixedParkDto.setModemEquipmentOfferCopy(catalogModem.getOfferModemInternet());
			    fixedParkDto.setContar(Integer.valueOf(contar));
			}else {
			
			    product.setModemEquipment(catalogModem.getOfferModemInternet());
			}
			resp = true;
	    	
		    }
		
	    	}
	    	
	    }else {
		if(esCopia) {	
		    fixedParkDto.setModemEquipmentOfferCopy(catalogModem.getOfferModemInternet());
		    fixedParkDto.setContar(Integer.valueOf(contar));
		}else {
		
		    product.setModemEquipment(catalogModem.getOfferModemInternet());
		}
		resp = true;
	    }
	    	
	}
	
	return resp;

}

private boolean determinatePresentInternet(ProductFixedDTO product, CatalogModem catalogModem, FixedDTO fixedParkDto, CommercialOperationInfo comercialInfo, boolean esCopia) {
	boolean resp = false;
	
    	if(catalogModem.getDestinyTechnology().equals(product.getInternetDestinyTechnology())
    		&& fixedParkDto.getPresentInternet().equals('0')) {
    	        		
    	    if(catalogModem.getRentVaild().equals("1")) {
    		if(catalogModem.getOfferConditionalRent().equals("<")) {
    		    	if(product.getPackageRent() < (Double.parseDouble(catalogModem.getOfferRent()))) {
    		    	    if(esCopia) {
    		    		fixedParkDto.setModemEquipmentOfferCopy(catalogModem.getOfferModemInternet());
    		    	    }else {
    		    	    
    		    	
    		    		product.setModemEquipment(catalogModem.getOfferModemInternet());
    		    	    
    		    	    }
    		    	
    		    	    resp = true;
            	    	
            		}
    		
    		}else {
    		    
    		    if(product.getPackageRent() >= (Double.parseDouble(catalogModem.getOfferRent()))) {	
    				if(esCopia) {     				    
    				    fixedParkDto.setModemEquipmentOfferCopy(catalogModem.getOfferModemInternet());
    				}else {
    				
    				    product.setModemEquipment(catalogModem.getOfferModemInternet());
    				}
    				resp = true;
        		    }
        		
        	    	}
    	    	
    	    }else {
    		if(esCopia) {
    		    fixedParkDto.setModemEquipmentOfferCopy(catalogModem.getOfferModemInternet());
    		}else {
    		
    		    product.setModemEquipment(catalogModem.getOfferModemInternet());
    		}
    		resp = true;
    	    }
    	    	
    	}
    	
    	return resp;
	
	}

private boolean determinatePresentInternetOC(ProductFixedDTO product, CatalogModem catalogModem, FixedDTO fixedParkDto, CommercialOperationInfo comercialInfo, boolean esCopia) {
	boolean resp = false;
	
	String internetDestinyTechnology = "FTTH";	
	
	if(catalogModem.getDestinyTechnology().equals(internetDestinyTechnology)
		&& fixedParkDto.getPresentInternet().equals('0')) {
	        		
	    if(catalogModem.getRentVaild().equals("1")) {
		if(catalogModem.getOfferConditionalRent().equals("<")) {
		    	if(product.getPackageRent() < (Double.parseDouble(catalogModem.getOfferRent()))) {
		    	    if(esCopia) {
		    		fixedParkDto.setModemEquipmentOfferCopy(catalogModem.getOfferModemInternet());
		    	    }else {
		    	    
		    	
		    		product.setModemEquipment(catalogModem.getOfferModemInternet());
		    	    
		    	    }
		    	
		    	    resp = true;
        	    	
        		}
		
		}else {
		    
		    if(product.getPackageRent() >= (Double.parseDouble(catalogModem.getOfferRent()))) {	
				if(esCopia) {     				    
				    fixedParkDto.setModemEquipmentOfferCopy(catalogModem.getOfferModemInternet());
				}else {
				
				    product.setModemEquipment(catalogModem.getOfferModemInternet());
				}
				resp = true;
    		    }
    		
    	    	}
	    	
	    }else {
		if(esCopia) {
		    fixedParkDto.setModemEquipmentOfferCopy(catalogModem.getOfferModemInternet());
		}else {
		
		    product.setModemEquipment(catalogModem.getOfferModemInternet());
		}
		resp = true;
	    }
	    	
	}
	
	return resp;
	
	}

    private void determinatePresentInternet(ProductFixedDTO product, List<ParamMovTotal> listParamMovTotal) {
		
	for (ParamMovTotal paramMovTotal : listParamMovTotal) {
	    //TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) 
	    if(paramMovTotal.getCodValor().equals(rulesValue.getINTERNET_TECNOLOGIA_HFC())
		&& rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(product.getInternetDestinyTechnology())
		&& (paramMovTotal.getCol2() != null && product.getPackageRent() <= Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol2()))))
		&& (paramMovTotal.getCol1() != null && product.getPackageRent() > Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol1()))))) {
		product.setModemEquipment(paramMovTotal.getValor());
		
		break;
	    }
	    
	}
	
	/*if (product.getPackageRent() <= 199) {
	    //product.setEquipmentUltraWifiRepQuantity(0);
	    //product.setEquipmentUltraWifiRepQuantity(1);
	    product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_DOCSIS3());
	}
	if (product.getPackageRent() > 199) {
	    //product.setEquipmentUltraWifiRepQuantity(0);
	    //product.setEquipmentUltraWifiRepQuantity(1);
	    product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_SMART_WIFI());
	}*/
    }

    private void determinateInternetTecnologyAdsl(ProductFixedDTO product, FixedDTO fixedParkDto, List<ParamMovTotal> listParamMovTotal) {
	if (fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_ADSL())) {
	    /*if (product.getPackageRent() <= 199) {
		//product.setEquipmentUltraWifiRepQuantity(0);
		//product.setEquipmentUltraWifiRepQuantity(1);
		product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_DOCSIS3());
	    }
	    if (product.getPackageRent() > 199) {
		//product.setEquipmentUltraWifiRepQuantity(0);
		//product.setEquipmentUltraWifiRepQuantity(1);
		product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_SMART_WIFI());
	    }*/
	    for (ParamMovTotal paramMovTotal : listParamMovTotal) {
		    //TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) 
		    if(paramMovTotal.getCodValor().equals(rulesValue.getINTERNET_TECNOLOGIA_HFC())
			&& rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(product.getInternetDestinyTechnology())
			&& (paramMovTotal.getCol2() != null && product.getPackageRent() <= Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol2()))))
			&& (paramMovTotal.getCol1() != null && product.getPackageRent() > Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol1()))))) {
			product.setModemEquipment(paramMovTotal.getValor());
			break;
		    }
		    
		}
	    
	}
	if (fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_DUALBAND())) {
	    
	    
	    /*if (product.getPackageRent() <= 199) {
		//product.setEquipmentUltraWifiRepQuantity(0);
		//product.setEquipmentUltraWifiRepQuantity(1);
		product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_DOCSIS3());
	    }
	    if (product.getPackageRent() > 199) {
		//product.setEquipmentUltraWifiRepQuantity(0);
		//product.setEquipmentUltraWifiRepQuantity(1);
		product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_SMART_WIFI());
	    }*/
	    for (ParamMovTotal paramMovTotal : listParamMovTotal) {
		    //TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) 
		    if(paramMovTotal.getCodValor().equals(rulesValue.getINTERNET_TECNOLOGIA_HFC())
			&& rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(product.getInternetDestinyTechnology())
			&& (paramMovTotal.getCol2() != null && product.getPackageRent() <= Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol2()))))
			&& (paramMovTotal.getCol1() != null && product.getPackageRent() > Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol1()))))) {
			product.setModemEquipment(paramMovTotal.getValor());
			break;
		    }
		    
		}
	}
    }

    private void determinateInternetTecnologyHfc(ProductFixedDTO product, FixedDTO fixedParkDto) {
	if (fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_ADSL())) {
	    // determinateEquipmentDocsis(product,fixedParkDto),
	    determinateModemEquipmentDocsis3(product, fixedParkDto);
	    determinateModemEquipmentDualband(product, fixedParkDto);
	}
    }

    private void determinateModemEquipmentDocsis3(ProductFixedDTO product, FixedDTO fixedParkDto) {
	if (fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_DOCSIS3())) {
	    if (product.getPackageRent() <= 199) {
		//product.setEquipmentUltraWifiRepQuantity(0);
		//product.setEquipmentUltraWifiRepQuantity(1);
		return;
	    }
	    if (product.getPackageRent() > 199) {
		//product.setEquipmentUltraWifiRepQuantity(0);
		//product.setEquipmentUltraWifiRepQuantity(1);
	    }
	}
    }

    private void determinateModemEquipmentDualband(ProductFixedDTO product, FixedDTO fixedParkDto) {
	if (fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_DUALBAND())) {
	    if (product.getPackageRent() <= 199) {
		//product.setEquipmentUltraWifiRepQuantity(0);
		//product.setEquipmentUltraWifiRepQuantity(1);
		return;
	    }
	    if (product.getPackageRent() > 199) {
		//product.setEquipmentUltraWifiRepQuantity(0);
		//product.setEquipmentUltraWifiRepQuantity(1);
	    }
	}
    }

    private void determinateInternetTecnologyAdslAndFtth(ProductFixedDTO product, FixedDTO fixedParkDto, List<ParamMovTotal> listParamMovTotal) {
	if (fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_ADSL())
		&& (fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_ADSL())
			|| fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_DUALBAND()))) {
	    
	    
	    for (ParamMovTotal paramMovTotal : listParamMovTotal) {
		    //TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) 
		    if(paramMovTotal.getCodValor().equals(rulesValue.getINTERNET_TECNOLOGIA_FTTH())
			&& rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(product.getInternetDestinyTechnology())
			&& (paramMovTotal.getCol2() != null && product.getPackageRent() <= Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol2()))))
			&& (paramMovTotal.getCol1() != null && product.getPackageRent() > Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol1()))))) {
			product.setModemEquipment(paramMovTotal.getValor());
			break;
		    }
		    
	    }
	    
	    /*if (product.getPackageRent() <= 199) {
		//product.setEquipmentUltraWifiRepQuantity(0);
		//product.setEquipmentUltraWifiRepQuantity(1);
		product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_GPON());
	    }
	    if (product.getPackageRent() > 199) {
		//product.setEquipmentUltraWifiRepQuantity(0);
		//product.setEquipmentUltraWifiRepQuantity(1);
		product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_GPON());
	    }*/
	}

    }

    private void determinateInternetTecnologyFtth(ProductFixedDTO product, FixedDTO fixedParkDto, List<ParamMovTotal> listParamMovTotal) {
	if (fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_ADSL())) {
	    if (fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_SMART_WIFI())) {
		/*if (product.getPackageRent() <= 199) {
		    //product.setEquipmentUltraWifiRepQuantity(0);
		    //product.setEquipmentUltraWifiRepQuantity(1);
		    return;
		}
		if (product.getPackageRent() > 199) {
		    //product.setEquipmentUltraWifiRepQuantity(0);
		    //product.setEquipmentUltraWifiRepQuantity(1);
		}*/
		
		for (ParamMovTotal paramMovTotal : listParamMovTotal) {
		    //TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) 
		    if(paramMovTotal.getCodValor().equals(rulesValue.getINTERNET_TECNOLOGIA_FTTH())
			&& rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(product.getInternetDestinyTechnology())
			&& (paramMovTotal.getCol2() != null && product.getPackageRent() <= Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol2()))))
			&& (paramMovTotal.getCol1() != null && product.getPackageRent() > Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol1()))))) {
			product.setModemEquipment(paramMovTotal.getValor());
			break;
		    }
		    
		}
	    }
	}
    }

    private void determinateInternetTecnologyHfcAndFtth(ProductFixedDTO product, FixedDTO fixedParkDto, List<ParamMovTotal> listParamMovTotal) {
	if (fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_ADSL())) {

	    determinateModenNameDocsis2(product, fixedParkDto, listParamMovTotal);

	    determinateModenNameDocsis3(product, fixedParkDto, listParamMovTotal);

	    determinateModenNameDualband(product, fixedParkDto, listParamMovTotal);
	}
    }
    
    private void determinateModenNameDocsis2(ProductFixedDTO product, FixedDTO fixedParkDto, List<ParamMovTotal> listParamMovTotal) {
	if (fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_DOCSIS2())) {
	    /*if (product.getPackageRent() <= 199) {
		//product.setEquipmentUltraWifiRepQuantity(0);
		//product.setEquipmentUltraWifiRepQuantity(1);
		product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_GPON());
	    }
	    if (product.getPackageRent() > 199) {
		//product.setEquipmentUltraWifiRepQuantity(0);
		//product.setEquipmentUltraWifiRepQuantity(1);
		product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_GPON());
	    }*/
	    for (ParamMovTotal paramMovTotal : listParamMovTotal) {
		    //TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) 
		    if(paramMovTotal.getCodValor().equals(rulesValue.getINTERNET_TECNOLOGIA_FTTH())
			&& rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(product.getInternetDestinyTechnology())
			&& (paramMovTotal.getCol2() != null && product.getPackageRent() <= Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol2()))))
			&& (paramMovTotal.getCol1() != null && product.getPackageRent() > Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol1()))))) {
			product.setModemEquipment(paramMovTotal.getValor());
			break;
		    }
		    
		}
	}
    }

    private void determinateModenNameDocsis3(ProductFixedDTO product, FixedDTO fixedParkDto, List<ParamMovTotal> listParamMovTotal) {
	if (fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_DOCSIS3())) {
	    /*if (product.getPackageRent() <= 199) {
		//product.setEquipmentUltraWifiRepQuantity(0);
		//product.setEquipmentUltraWifiRepQuantity(1);
		product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_GPON());
	    }
	    if (product.getPackageRent() > 199) {
		//product.setEquipmentUltraWifiRepQuantity(0);
		//product.setEquipmentUltraWifiRepQuantity(1);
		product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_GPON());
	    }*/
	    
	    for (ParamMovTotal paramMovTotal : listParamMovTotal) {
		    //TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) 
		    if(paramMovTotal.getCodValor().equals(rulesValue.getINTERNET_TECNOLOGIA_FTTH())
			&& rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(product.getInternetDestinyTechnology())
			&& (paramMovTotal.getCol2() != null && product.getPackageRent() <= Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol2()))))
			&& (paramMovTotal.getCol1() != null && product.getPackageRent() > Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol1()))))) {
			product.setModemEquipment(paramMovTotal.getValor());
			break;
		    }
		    
		}
	}
    }

    private void determinateModenNameDualband(ProductFixedDTO product, FixedDTO fixedParkDto, List<ParamMovTotal> listParamMovTotal) {
	if (fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_DUALBAND())) {
	    /*if (product.getPackageRent() <= 199) {
		//product.setEquipmentUltraWifiRepQuantity(0);
		//product.setEquipmentUltraWifiRepQuantity(1);
		product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_GPON());
	    }
	    if (product.getPackageRent() > 199) {
		//product.setEquipmentUltraWifiRepQuantity(0);
		//product.setEquipmentUltraWifiRepQuantity(1);
		product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_GPON());
	    }*/
	    for (ParamMovTotal paramMovTotal : listParamMovTotal) {
		    //TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) 
		    if(paramMovTotal.getCodValor().equals(rulesValue.getINTERNET_TECNOLOGIA_FTTH())
			&& rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(product.getInternetDestinyTechnology())
			&& (paramMovTotal.getCol2() != null && product.getPackageRent() <= Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol2()))))
			&& (paramMovTotal.getCol1() != null && product.getPackageRent() > Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol1()))))) {
			product.setModemEquipment(paramMovTotal.getValor());
			break;
		    }
		    
		}
	}
    }
    
    private void determinateUltraWifiRep(ProductFixedDTO product, String pp) {
	
	if(!(pp.equals(Constant.ONES))) {
	
        	if(product.getPackageRent() >= 280) {
        	    product.setEquipmentUltraWifiRepQuantity(1);
        	}
	
	}
	
    }
    
private boolean activateUltraWifi(List<ParamMovTotal> listParamMovTotal) {
	
	boolean existe = false;
	
	List<ParamMovTotal> listParam = this.paramMovTotalRepo.findAll();
	
	for (ParamMovTotal paramMovTotal : listParamMovTotal) {
	    
	    if(paramMovTotal.getGrupoParam().equals("ULTRA_WIFI")) {
		
		if(paramMovTotal.getValor().equals("1")) {
		    existe = true;
		}
		
	    }
	    
	}
	
	return existe;
	
    }

@Override
public void execute(List<CatalogDeco> catalogDecoList, List<CatalogModem> catalogModemList, List<ProductFixedDTO> priorizationList,
	FixedDTO fixedParkDto, CommercialOperationInfo commercialInfo, boolean provideOrPortability) {
    // TODO Auto-generated method stub
    
}
    
    
}
