package com.telefonica.total.business.offersRules.determinateAppsQuantityByRentMonoProduct;

import java.util.List;

import org.springframework.stereotype.Service;

import com.telefonica.total.dto.MobileDTO;

@Service
public class DeterminateAppsQuantityByRentMonoProduct implements IDeterminateAppsQuantityByRentMonoProduct{

    @Override
    public void execute(List<MobileDTO> mobileDevices) {

	for (MobileDTO mobileDTO : mobileDevices) {
	    if (mobileDTO.getMobileRentMonoProduct() != null && mobileDTO.getMobileRentMonoProduct() > 0) {
		if (mobileDTO.getMobileRentMonoProduct() <= 29.9) {
		    mobileDTO.setMobileApps(5);
		} else if (mobileDTO.getMobileRentMonoProduct() > 29.9 && mobileDTO.getMobileRentMonoProduct() <= 85.9) {
		    mobileDTO.setMobileApps(6);
		} else {
		    mobileDTO.setMobileApps(7);
		}
	    } else {
		mobileDTO.setMobileApps(null);
	    }
	}
    }
}
