package com.telefonica.total.business.offersRules.comparisonoffer;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.pojo.req.CommercialOperationInfo;

public interface ICompareComponentsOffers {
    
    /***
     * Método que se encarga de comparar los componentes de la tenencia del cliente
     * versus las ofertas MT, y calcula la diferencia
     * 
     * @param fixedParkDto
     * @param filteredProductList
     * @param mobileDevices
     * @param commercialInfo
     */
    void execute(FixedDTO fixedParkDto, List<ProductFixedDTO> filteredProductList, List<MobileDTO> mobileDevices,
	    CommercialOperationInfo commercialInfo);

}
