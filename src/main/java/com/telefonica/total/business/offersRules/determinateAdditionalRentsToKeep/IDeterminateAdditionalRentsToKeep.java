package com.telefonica.total.business.offersRules.determinateAdditionalRentsToKeep;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;

public interface IDeterminateAdditionalRentsToKeep {

    /***
     * Método que se encarga de determinar los rentas adicionales que se deben
     * mantener para que el cliente no pierda servicios.
     * 
     * @param fixedParkDto
     */
    void execute(List<ProductFixedDTO> priorizationList, FixedDTO fixedParkDto);
}
