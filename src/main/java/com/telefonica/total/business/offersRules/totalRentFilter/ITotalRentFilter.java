package com.telefonica.total.business.offersRules.totalRentFilter;

import java.util.List;

import com.telefonica.total.dto.ProductFixedDTO;

public interface ITotalRentFilter {

    /***
     * Método valida si los productos disponibles tienen saldo negativo y les agrega
     * la caracteristica "Downsell".
     * 
     * @param filteredProductList
     */
    void execute(List<ProductFixedDTO> filteredProductList);
}
