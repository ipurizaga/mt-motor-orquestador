package com.telefonica.total.business.offersRules.calculateArpa;

import java.util.List;

import org.springframework.stereotype.Service;

import com.telefonica.total.business.offersRules.equifaxPatch.Constant;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;

@Service
public class CalculateArpa implements ICalculateArpa {

    @Override
    public void execute(FixedDTO fixedParkDto, List<MobileDTO> mobileDevices) {
	double mobileAcumulator = 0D;
	for (MobileDTO mobile : mobileDevices) {
	    mobileAcumulator += mobile.getMobileRentMonoProduct() - Math.abs(mobile.getPermanentDisccount());
	}
	
	// si algÃºn valor es nulo, se coloca por defecto valor cero.
	
	Double packageRent = (fixedParkDto.getPackageRent() != null) ? fixedParkDto.getPackageRent() : Constant.CERO;
	Double cableRentMonoProdPremHd = (fixedParkDto.getCableRentMonoProdPremHd() != null) ? fixedParkDto.getCableRentMonoProdPremHd() : Constant.CERO;
	Double permanentDiscount = (fixedParkDto.getPermanentDiscount() != null) ? fixedParkDto.getPermanentDiscount() : Constant.CERO;
	Double rentMonoProdLine = (fixedParkDto.getRentMonoProdLine() != null) ? fixedParkDto.getRentMonoProdLine() : Constant.CERO;
	Double rentMonoProdInternet = (fixedParkDto.getRentMonoProdInternet() != null) ? fixedParkDto.getRentMonoProdInternet() : Constant.CERO;
	Double cableRentMonoProd = (fixedParkDto.getCableRentMonoProd() != null) ? fixedParkDto.getCableRentMonoProd() : Constant.CERO;
	
	double arpa = packageRent + cableRentMonoProdPremHd 
		- Math.abs(permanentDiscount) + rentMonoProdLine
		+ rentMonoProdInternet + cableRentMonoProd + mobileAcumulator;
	fixedParkDto.setPresentARPA(arpa);
	//System.out.println("Arpa actual: " + arpa);
    }
}
