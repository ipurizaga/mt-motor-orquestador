package com.telefonica.total.business.offersRules.verifyNumberOfMobiles;

import java.util.List;

import org.springframework.stereotype.Service;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;

@Service
public class VerifyNumberOfMobiles implements IVerifyNumberOfMobiles{

    @Override
    public void execute(FixedDTO fixedParkDto, List<MobileDTO> mobileDevices) {
	fixedParkDto.setOnlyOneMobileIndicator(mobileDevices.size() == 1 ? Boolean.TRUE : Boolean.FALSE);
    }
}
