package com.telefonica.total.business.offersRules.mobileConsumptionFilter;

import java.util.List;

import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.ProductFixedDTO;

public interface IMobileConsumptionFilter {

    /***
     * Método que valida si cada uno de los productos disponibles tiene una cantidad
     * de datos menor al consumo promedio de los moviles
     * 
     * @param filteredProductList
     * @param mobileDevices
     */
    void execute(List<ProductFixedDTO> filteredProductList, List<MobileDTO> mobileDevices);

}
