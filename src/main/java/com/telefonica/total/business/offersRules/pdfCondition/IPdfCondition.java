package com.telefonica.total.business.offersRules.pdfCondition;

import java.util.List;

import com.telefonica.total.dto.FourthDigitDTO;
import com.telefonica.total.pojo.req.CommercialOperation;

public interface IPdfCondition {

    /***
     * Método que se encarga de realizar el calculo de la condicion de venta PPF
     * 
     * @param commercialOperations
     * @return
     */
    List<FourthDigitDTO> execute(List<CommercialOperation> commercialOperations);
}
