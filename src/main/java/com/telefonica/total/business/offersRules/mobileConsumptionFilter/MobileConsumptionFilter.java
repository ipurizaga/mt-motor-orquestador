package com.telefonica.total.business.offersRules.mobileConsumptionFilter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.RulesValueProp;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.pojo.req.Information;

@Service
public class MobileConsumptionFilter implements IMobileConsumptionFilter{

    @Autowired
    private RulesValueProp rulesValue;
    
    @Override
    public void execute(List<ProductFixedDTO> filteredProductList, List<MobileDTO> mobileDevices) {
	double clientMobileConsumption = 0;
	for (MobileDTO mobile : mobileDevices) {
	    clientMobileConsumption += mobile.getMobileMbConsumption();
	}
	for (ProductFixedDTO product : filteredProductList) {
	    product.setMobileQuantityDataSumatory((product.getMobil0QuantityData()==null? 0: product.getMobil0QuantityData())+ (product.getMobil1QuantityData()==null? 0: product.getMobil1QuantityData()));
	    if (product.getMobileQuantityDataSumatory() - clientMobileConsumption < rulesValue.getLIMIT_MOBILE_CONSUME()) {
		product.getCharacteristics().add(Information.builder().code(rulesValue.getPRODUCT_CHARACTERISTIC())
			.value(rulesValue.getPRODUCT_CHARACTERISTIC_MINOR_CONSUMPTION()).build());
	    }
	}
    }
}
