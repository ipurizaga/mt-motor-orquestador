package com.telefonica.total.business.offersRules.determinatequantitymobile;

import java.util.List;
import java.util.ArrayList;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.exception.BusinessException;
import com.telefonica.total.generic.CommOperCollection;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.Subscription;

@Service
public class DeterminateQuantityMobile implements IDeterminateQuantityMobile {

	/***
     * Método que se encarga de realizar las validaciones de cantidad de móviles para 
     * PLANTA NUEVA.
     * 
     * @param listComercialOperation
     * @return
     */
    @Override
    public void execute(List<CommercialOperation> listComercialOperation) {
	CommOperCollection<CommercialOperation> reqLst = new CommOperCollection<>(listComercialOperation);
	reqLst.filterByProduct(SuscriptionType.MOBILE);
	validateProvide(reqLst);
    }
    /***
     * Método que se encarga de realizar las validaciones de cantidad de móviles para 
     * PLANTA GESTIÓN DE PLANTA.
     * 
     * @param listComercialOperation
     * @return
     */
    @Override
    public void executeMt(List<CommercialOperation> listComercialOperation, List<ProductFixedDTO> enabledOffers) {
	int ps=0,bo=0;
	List<Integer> resList = new ArrayList<Integer>();
    CommOperCollection<CommercialOperation> reqLst = new CommOperCollection<>(listComercialOperation);
    reqLst.filterByProduct(SuscriptionType.MOBILE);
    resList = validateActualOffers(listComercialOperation,bo, ps);
	ps = resList.get(0);
	bo = resList.get(1);	
	
	if(ps!=0 || bo!=0) {
		for(ProductFixedDTO enOffer:enabledOffers) {
			//Si es parrilla nueva solo 1 móvil
			if(enOffer.getPackagePs()==ps && enOffer.getPackageMobilCode().equals(String.valueOf(bo))) {
				validateOnlyFixed(reqLst);
				
			}else if(enOffer.getPackageMobilCode().equals(String.valueOf(bo))){
				validateOnlyFixed(reqLst);
			}
		}
	}
	//Si es parrilla antigua 2 móviles
		validateOnlyMobile(reqLst);
	
	}
    /***
     * Método que se encarga de realizar las validaciones de cantidad de móviles para 
     * SEMICOMPLETOS y/o CAIDOS.
     * 
     * @param listComercialOperation
     * @return
     */
    @Override
    public void executeFail(List<CommercialOperation> listComercialOperation,Integer flowType, List<ProductFixedDTO> enabledOffers) {
    	int ps=0,bo=0;
    	List<Integer> resList = new ArrayList<Integer>();
    CommOperCollection<CommercialOperation> reqLst = new CommOperCollection<>(listComercialOperation);
	reqLst.filterByProduct(SuscriptionType.MOBILE);
	if(flowType == Constant.AVERIA_TIPO1) {
		resList = validateActualOffers(listComercialOperation, bo, ps);
		ps = resList.get(0);
		bo = resList.get(1);
		if(ps!=0 ) {
			for(ProductFixedDTO enOffer:enabledOffers) {
			//Si es parrilla nueva fijo
				if(enOffer.getPackagePs()==ps) {
					validateOnlyFixed(reqLst);			
				}
			}			
		}
		validateOnlyMobile(reqLst);
	} else if(flowType == Constant.AVERIA_TIPO2) {

		resList = validateActualOffers(listComercialOperation, bo, ps);
		ps = resList.get(0);
		bo = resList.get(1);
	
		/*if(bo!=0) {
			for(ProductFixedDTO enOffer:enabledOffers) {			
			//Si es parrilla nueva solo 1 móvil
				if(enOffer.getPackageMobilCode().equals(String.valueOf(bo))) {
					validateOnlyFixed(reqLst);				
				}
			}
		}*/
	//Si es parrilla antigua 2 móviles
			validateOnlyMobile(reqLst);
	  }
    }
    
    //Validando cuando es alta
    private void validateProvide(List<CommercialOperation> listComercialOperation) {
    	if (CollectionUtils.isNotEmpty(listComercialOperation) && listComercialOperation.size() > 1) {
    		throw new BusinessException(Constant.BE_1053);
    	}
    }
    
    //Validando solo un movil y fijo
    private void validateOnlyFixed(List<CommercialOperation> listComercialOperation) {
    	if (CollectionUtils.isNotEmpty(listComercialOperation) && listComercialOperation.size() > 1) {
    		throw new BusinessException(Constant.BE_1073);
    	}
    }
    
    //validando un moviles 1 o 2 dependiendo el flujo
    private void validateOnlyMobile(List<CommercialOperation> listComercialOperation) {
	int cantMtm = 0, cantMob = 0;

		for (CommercialOperation commercialOperation : listComercialOperation) {
	    Subscription suscriber =   commercialOperation.getSubscriber();   
	    	if( suscriber!=null && suscriber.getType().equals(SuscriptionType.MTM.getCode())) {
	    		cantMtm++;
	    	}
	    	else if(suscriber!=null && suscriber.getType().equals(SuscriptionType.MOBILE.getCode())) {
	    		cantMob++;
	    	}
		}
		//Añadiendo validación para semicompletos, no se acepta móvil alta si ya va junto a un MTM actual
		if(cantMob==0) {
			if(cantMtm >2 || listComercialOperation.size() > 2 ) {
			    throw new BusinessException(Constant.BE_1053);
			}
		}
		//Añadiendo validación para semicompletos, se acepta móvil alta si va junto a un MTM antiguo
		else if(cantMob==1){
			if(cantMtm>1) {
				throw new BusinessException(Constant.BE_1073);
			}
		}
		else {
			throw new BusinessException(Constant.BE_1073);
		}
				
    }
    /***
     * Método que se encarga de validar si la oferta actual es MT
     * Retorna los valores de PS y BO para despues poder usar validaciones.
     * 
     * @param listComercialOperation
     * @param bo
     * @param ps
     * @return
     */
    private List<Integer> validateActualOffers(List<CommercialOperation> listComercialOperation, int bo, int ps) {
    	List<Integer> res = new ArrayList<Integer>();
    			//Validando si el producto actual pertenece a Parrilla Nueva o Antigua
    	    	for(CommercialOperation ComOpe:listComercialOperation) {
    				if(ComOpe.getProduct()!=null) {
    					if(ComOpe.getSubscriber()!=null && ComOpe.getProduct().equals(SuscriptionType.FIXED.getCode())) {
    						if(ComOpe.getSubscriber().getType().equals(SuscriptionType.MTF.getCode()) && ComOpe.getSubscriber().getSubscriberId()!=null) {
        						ps = Integer.parseInt(ComOpe.getSubscriber().getSubscriberId());
        					}
    					}else if(ComOpe.getProduct().equals(SuscriptionType.MOBILE.getCode()) && ComOpe.getSubscriber()!=null) {
    						if(ComOpe.getSubscriber().getType().equals(SuscriptionType.MTM.getCode()) && ComOpe.getSubscriber().getBoId()!=null) {
        						if(ComOpe.getSubscriber().getBoId()>bo) {
        							bo = ComOpe.getSubscriber().getBoId();
        						}
        					}
    					}    					    					
    				}		
    			 }    		
    	 res.add(ps);
    	 res.add(bo);
    	 return res;
    }

}
