package com.telefonica.total.business.offersRules.separatedProductsTotalCost;

import java.util.List;

import org.springframework.stereotype.Service;

import com.telefonica.total.dto.ProductFixedDTO;

@Service
public class SeparatedProductsTotalCost implements ISeparatedProductsTotalCost{

    @Override
    public void execute(List<ProductFixedDTO> priorizationList) {
	for (ProductFixedDTO product : priorizationList) {
	    double blocksTokeepRent = product.getAditionalBlocksTokeepRent() != null ? product.getPackageRent() : 0;
	    double blocksToKeepSumatory = product.getAditionalRentsToKeepSummatory() != null ? product.getPackageRent() : 0;
	    product.setTotalCostAloneProducts((product.getHomeCostAlone()!=null? product.getHomeCostAlone(): 0.0) + (product.getMobil1CostAlone() != null? product.getMobil1CostAlone() : 0.0) + (product.getMobil2CostAlone()!= null? product.getMobil2CostAlone() : 0.0)
		    + blocksTokeepRent + blocksToKeepSumatory);
	}
    }
}
