package com.telefonica.total.business.offersRules.totalizationPriorizations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.RulesValueProp;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.enums.InternetTechnologyType;
import com.telefonica.total.enums.OperationComm;
import com.telefonica.total.enums.OperationDevice;
import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.generic.CommOperCollection;
import com.telefonica.total.model.ParamMovTotal;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.Information;
import com.telefonica.total.repository.ParamMovTotalRepo;

@Service
public class TotalizationPriorizations implements ITotalizationPriorizations {

    @Autowired
    private RulesValueProp rulesValue;
    
    private TotalUtil tu = new TotalUtil();
    
    @Autowired
    private ParamMovTotalRepo paramMovTotalRepo;

    @Override
    public List<ProductFixedDTO> execute(FixedDTO fixedParkDto, List<ProductFixedDTO> filteredProductList,
	    List<CommercialOperation> commercialOpeList, String coverage, List<ParamMovTotal> lstParam) {
	List<ProductFixedDTO> productsToPrioritize = new ArrayList<>();
	List<ProductFixedDTO> priorizatedProducts = new ArrayList<>();
	boolean ValidaReplace = commercialOpeList.get(0).getOperation().equals(Constant.OPERATION_REPLACEOFFER) ? true : false;
	boolean ValidaMovil = commercialOpeList.size() > 1 ? true : false;
	boolean ValidaPostpago = ValidaMovil && commercialOpeList.get(1).getDeviceOperation().equals(OperationDevice.CAEQ.getCodeDesc()) ? true : false;
	Integer typeTotalization = ValidaReplace && ValidaPostpago ? 1 : 2;
	Double arpa = fixedParkDto.getPresentARPA();
	String productType = commercialOpeList.get(0).getSubscriber() != null ? commercialOpeList.get(0).getSubscriber().getType() : "fijo";
	Character cobIntHfc = fixedParkDto.getHfcCobInternet() != null ? fixedParkDto.getHfcCobInternet() : 0;
	String actualTechnology = fixedParkDto.getTechnologyInternet() != null ? fixedParkDto.getTechnologyInternet() : "";
	
	if(actualTechnology != null) {
		if(actualTechnology.equals("HFC")) {
			if(cobIntHfc.toString().equals("2")) {
				coverage = "FTTH";
			}
		}
	}

	
	/**
	 * Se añade al método obtainOfferMTWithPositiveJump las variables : 
	 * typeTotalization (Verifica si es Totalizado o Solo Fijo),
	 * arpa (el arpa actual del cliente)
	 * productType (Verifica el tipo de producto fijo actual : TRIO, DUO, DUO BA, etc.  )
	 * 
	 * Se añade la variable coverage, para evaluar la priorizacion de acuerdo al tipo de cobertura
	 * ya sea HFC o FTTH
	 **/	
	
/*
	if (arpa > 199D) {
	    //TotalUtil.removeRuleByRentFromProductOffers(filteredProductList, 199);
	    if (fixedParkDto.getDeposed().equals('1') || (trioOrDuo(commercialOpeList) && mobilesHaveReplaceOffers(commercialOpeList))) {
		priorizeDownsell(filteredProductList, productsToPrioritize);
	    }
	} else {
	    priorizeRule199(filteredProductList, productsToPrioritize);
	}
*/
	//int iterator = 0;
	
	List<ParamMovTotal> arpaVelocidad = paramMovTotalRepo.findByGrupoParam(Constant.ARPA_VELOCIDAD);
	
	boolean priorTot = false;
	
	for (ParamMovTotal paramMovTotal : arpaVelocidad) {
	    if(arpa >= Double.valueOf(paramMovTotal.getCol1()) && arpa <= Double.valueOf(paramMovTotal.getCol2()) && (fixedParkDto.getSpeedInternet() <= Integer.parseInt(paramMovTotal.getCol3()))) {
		boolean prior = false;
		
		for (ProductFixedDTO fixProd : filteredProductList) {
		    if(fixProd.getPackageRent().equals(Double.valueOf(paramMovTotal.getValor()))) {
			fixProd.setPriority(String.valueOf(1));
			    priorizatedProducts.add(fixProd);
			    prior = true;			
		    }
		}
		
		if(!prior) {
		    TotalUtil.obtainOfferMTWithPositiveJump(filteredProductList, productsToPrioritize, productType, typeTotalization, arpa, coverage, lstParam, fixedParkDto);
			Collections.sort(productsToPrioritize, ProductFixedDTO.jumpComparator);
			boolean prioridad1 = false;
	        	for (ProductFixedDTO fixProd : productsToPrioritize) {
	        	    //if (iterator > 2) {
	        	    if (prioridad1) {
	        		break;
	        	    }
	        	    //fixProd.setPriority(String.valueOf(iterator + 1));
	        	    fixProd.setPriority(String.valueOf(1));
	        	    priorizatedProducts.add(fixProd);
	        	    prioridad1=true;
	        	}
	        	
	        	priorTot = true;
	        	
	        	break;
		}else {
		    	priorTot = true; 
			
			break;
		}
		
		
	    }	    
	}
	
	if(!priorTot) {
	    TotalUtil.obtainOfferMTWithPositiveJump(filteredProductList, productsToPrioritize, productType, typeTotalization, arpa, coverage, lstParam, fixedParkDto);
		Collections.sort(productsToPrioritize, ProductFixedDTO.jumpComparator);
		boolean prioridad1 = false;
        	for (ProductFixedDTO fixProd : productsToPrioritize) {
        	    //if (iterator > 2) {
        	    if (prioridad1) {
        		break;
        	    }
        	    //fixProd.setPriority(String.valueOf(iterator + 1));
        	    fixProd.setPriority(String.valueOf(1));
        	    priorizatedProducts.add(fixProd);
        	    prioridad1=true;
        	}
	}
	
	//asdsdf
	
	
	//return priorizatedProducts;
	
	return orderPriorization(filteredProductList/*, productsToPrioritize*/, priorizatedProducts, coverage);
    }

    private boolean mobilesHaveReplaceOffers(List<CommercialOperation> commercialOpeList) {
	CommOperCollection<CommercialOperation> mobileOperations = new CommOperCollection<>(commercialOpeList);
	mobileOperations.filterByProduct(SuscriptionType.MOBILE);
	int count = 0;
	for (CommercialOperation operations : mobileOperations) {
	    if (operations.getOperation().equals(OperationComm.CAPL.getCodeDesc())) {
		count++;
	    }
	}
	if (count > 0) {
	    return Boolean.TRUE;
	} else {
	    return Boolean.FALSE;
	}
    }

    public void priorizeRule199(List<ProductFixedDTO> filteredProductList, List<ProductFixedDTO> productsToPrioritize) {

	ProductFixedDTO element = null;
	for (ProductFixedDTO productFixedDTO : filteredProductList) {

	    if (productFixedDTO.getPackageRent() == 199d) {
		element = productFixedDTO;
		productsToPrioritize.remove(element);
		break;
	    }
	}
	if (element != null) {
	    productsToPrioritize.add(0, element);
	}
    }

    private boolean trioOrDuo(List<CommercialOperation> commercialOpeList) {
	CommOperCollection<CommercialOperation> fixedOperations = new CommOperCollection<>(commercialOpeList);
	fixedOperations.filterByProduct(SuscriptionType.FIXED);
	if (fixedOperations.get(0).getSubscriber() == null) {
	    return Boolean.FALSE;
	}

	if (!fixedOperations.isEmpty() && (fixedOperations.get(0).getSubscriber().getType().equals(SuscriptionType.DUOCABLE.getCode())
		|| fixedOperations.get(0).getSubscriber().getType().equals(SuscriptionType.DUOINTERNET.getCode())
		|| fixedOperations.get(0).getSubscriber().getType().equals(SuscriptionType.TRIO.getCode()))) {
	    return Boolean.TRUE;
	} else {
	    return Boolean.FALSE;
	}
    }

    private void priorizeDownsell(List<ProductFixedDTO> filteredProductList, List<ProductFixedDTO> productsToPrioritize) {
	List<ProductFixedDTO> productsWithoutCharacteristics = new ArrayList<>();
	List<ProductFixedDTO> downselList = new ArrayList<>();
	for (ProductFixedDTO product : filteredProductList) {
	    verifyCharacteristics(productsWithoutCharacteristics, downselList, product);
	}
	if (CollectionUtils.isNotEmpty(downselList)) {
	    productsToPrioritize.removeAll(downselList);
	    Collections.sort(downselList, ProductFixedDTO.downsellJumpComparator);
	    productsToPrioritize.add(0, downselList.get(0));
	}
    }

    /***
     * Método que se encarga de la verificación de las caracteristicas de cada
     * producto para determinar si tiene menor velocidad, menor consumo o si es
     * downsell.
     * 
     * @param productsWithoutCharacteristics
     * @param downselList
     * @param product
     */
    private void verifyCharacteristics(List<ProductFixedDTO> productsWithoutCharacteristics, List<ProductFixedDTO> downselList,
	    ProductFixedDTO product) {
	if (product.getCharacteristics() == null || product.getCharacteristics().isEmpty()) {
	    productsWithoutCharacteristics.add(product);
	} else {
	    for (Information information : product.getCharacteristics()) {
		// if
		// (information.getValue().equals(rulesValue.getPRODUCT_CHARACTERISTIC_MINOR_VELOCITY())
		// ||
		// information.getValue().equals(rulesValue.getPRODUCT_CHARACTERISTIC_MINOR_CONSUMPTION()))
		// {
		// continue;
		// }
		if (information.getValue().equals(rulesValue.getPRODUCT_CHARACTERISTIC_DOWNSELL())) {
		    downselList.add(product);
		}
	    }
	}
    }
    
    /***
     * Se ordena segun renta, luego se recibe el producto actual como parametro y se
     * coloca como prioridad 1 y los dos consecutivos como prioridad 2 y 3
     * respectivamente.
     * 
     * @param temporalList
     * @param actualProductList
     * @return
     */
    
    /** Se separa las ofertas por tecnologia y se genera prioridad FTTH en caso el cliente sea 
    Overlay
    **/
    private List<ProductFixedDTO> orderPriorization(List<ProductFixedDTO> temporalList/*, List<ProductFixedDTO> actualProductList*/, List<ProductFixedDTO> priorizatedProducts, String evaluateCoverage) {

	List<ProductFixedDTO> firstListItems = new ArrayList<>();
	List<ProductFixedDTO> productsHFC = new ArrayList<>();
	List<ProductFixedDTO> productsFTTH = new ArrayList<>();

	
	TotalUtil.productsPerTechnology(temporalList, productsHFC, InternetTechnologyType.HFC.getCode());
	Collections.sort(productsHFC, ProductFixedDTO.rentComparator);
	
	TotalUtil.productsPerTechnology(temporalList, productsFTTH, InternetTechnologyType.FTTH.getCode());
	Collections.sort(productsFTTH, ProductFixedDTO.rentComparator);
	
	Collections.sort(temporalList, ProductFixedDTO.rentComparator);
	
	/*if(evaluateCoverage.equals(InternetTechnologyType.FTTH.getCode())) {
		TotalUtil.clearPriority(productsFTTH);
		TotalUtil.clearPriority(temporalList);
		setProductPriority(productsFTTH, actualProductList, firstListItems);
	}
	else {*/
		firstListItems = priorizatedProducts;
	//}

	return firstListItems;
    }
    
   private void  setProductPriority(List<ProductFixedDTO> temporalList, List<ProductFixedDTO> actualProductList, List<ProductFixedDTO> firstListItems) {
	   for (ProductFixedDTO product : temporalList) {
		    /*if (iterator > 0) {
			break;
		    }*/
		    if (product.getPackageRent() >= actualProductList.get(0).getPackageRent()) {
			product.setPriority(String.valueOf(1));
			firstListItems.add(product);
			break;
			//iterator++;
		    }
		}
   }
   
}
