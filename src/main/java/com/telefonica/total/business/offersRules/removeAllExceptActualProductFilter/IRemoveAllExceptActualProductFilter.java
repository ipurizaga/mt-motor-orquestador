package com.telefonica.total.business.offersRules.removeAllExceptActualProductFilter;

import java.util.List;

import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.pojo.req.CommercialOperation;

public interface IRemoveAllExceptActualProductFilter {

    /***
     * Método que se ejecuta cuando una averia es de tipo MTMV y elimina todos los
     * productos de la lista menos el producto actual.
     * 
     * @param fixedParkDto
     * @param type
     * @param filteredProductList
     * @param mobileDevices
     * @param commercialOpeList
     */
    void executeFail(int type, List<ProductFixedDTO> filteredProductList, List<MobileDTO> mobileDevices,
	    List<CommercialOperation> commercialOpeList);
}
