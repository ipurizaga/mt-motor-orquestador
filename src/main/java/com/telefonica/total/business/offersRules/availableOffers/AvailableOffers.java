package com.telefonica.total.business.offersRules.availableOffers;

import java.util.List;

import org.springframework.stereotype.Service;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.PartialRequestDTO;
import com.telefonica.total.dto.ProductFixedDTO;

@Service
public class AvailableOffers implements IAvailableOffers{

    @Override
    public PartialRequestDTO execute(FixedDTO fixedParkDto, List<ProductFixedDTO> priorizationList, List<MobileDTO> mobileDevices) {
	PartialRequestDTO partial = new PartialRequestDTO();
	partial.setCustomerInformation(fixedParkDto);
	partial.setMobileCustomerInformation(mobileDevices);
	partial.setResultantProductFixedList(priorizationList);
	partial.setActualRent(calculateActualRent(fixedParkDto, mobileDevices));
	return partial;
    }
    
    private Double calculateActualRent(FixedDTO fixedParkDto, List<MobileDTO> mobileDevices) {
	double rentMobile = 0D;
	for (MobileDTO mobile : mobileDevices) {
	    rentMobile += mobile.getMobileRentMonoProduct() - Math.abs(mobile.getPermanentDisccount());
	}
	return fixedParkDto.getPackageRent() - Math.abs(fixedParkDto.getPermanentDiscount()) + rentMobile;
    }
}
