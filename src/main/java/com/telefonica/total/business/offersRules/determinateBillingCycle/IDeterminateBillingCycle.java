package com.telefonica.total.business.offersRules.determinateBillingCycle;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.pojo.req.CommercialOperation;

public interface IDeterminateBillingCycle {

    void execute(FixedDTO fixedParkDto, List<CommercialOperation> commercialOpeList);
}
