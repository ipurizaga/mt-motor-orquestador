package com.telefonica.total.business.offersRules.salesOrRetentions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.RulesValueProp;
import com.telefonica.total.exception.BusinessException;
import com.telefonica.total.pojo.req.CommercialOperationInfo;

@Service
public class SalesOrRetentions implements ISalesOrRetentions{

    @Autowired
    private RulesValueProp rulesValue;
    
    @Override
    public void execute(CommercialOperationInfo commercialInfo) {
	if (rulesValue.getNEGOTIATION_MODE_RETENTION().equals(commercialInfo.getNegotiationMode())) {
	    throw new BusinessException(Constant.BE_1053);
	}
    }
}

