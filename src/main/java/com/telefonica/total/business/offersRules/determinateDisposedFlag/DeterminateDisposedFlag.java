package com.telefonica.total.business.offersRules.determinateDisposedFlag;

import java.util.List;

import org.springframework.stereotype.Service;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.generic.CommOperCollection;
import com.telefonica.total.pojo.req.CommercialOperation;

@Service
public class DeterminateDisposedFlag implements IDeterminateDisposedFlag {

    @Override
    public void execute(FixedDTO fixedParkDto, List<CommercialOperation> commercialOpeList, List<MobileDTO> mobileDevices) {
	CommOperCollection<CommercialOperation> commOperCollectionFixed = new CommOperCollection<>(commercialOpeList);
	commOperCollectionFixed.filterByProduct(SuscriptionType.FIXED);
	
	if(fixedParkDto.getDeposed() == null) {
	    fixedParkDto.setDeposed('0');
	}
	
	if (fixedParkDto.getDeposed().equals('0')) {
	    CommOperCollection<CommercialOperation> commOperCollectionMobile = new CommOperCollection<>(commercialOpeList);
		commOperCollectionMobile.filterByProduct(SuscriptionType.MOBILE);
		for (MobileDTO mobileDevice : mobileDevices) {
		    if (mobileDevice.getMobileRentMonoProduct() > 149) {
			fixedParkDto.setDeposed('1');
			return;
		    }
		}
	}
	
	
    }

}
