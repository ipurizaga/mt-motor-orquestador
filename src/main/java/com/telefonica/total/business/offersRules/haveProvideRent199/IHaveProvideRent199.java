package com.telefonica.total.business.offersRules.haveProvideRent199;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;

public interface IHaveProvideRent199 {

    /***
     * Método que se encarga de validar la oferta de 199 MT
     * 
     * @param productList
     * @param fixedParkDto
     */
    void execute(List<ProductFixedDTO> productList, FixedDTO fixedParkDto);

}
