package com.telefonica.total.business.offersRules.failPriorizations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.enums.OperationDevice;
import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.generic.CommOperCollection;
import com.telefonica.total.model.ParamMovTotal;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.repository.ParamMovTotalRepo;
import com.telefonica.total.enums.InternetTechnologyType;

@Service
public class FailPriorizations implements IFailPriorizations{
	
	 @Autowired
	 private ParamMovTotalRepo pmtr;
		
	private ParamMovTotal pmt;

    @Override
    public List<ProductFixedDTO> executeFail(FixedDTO fixedParkDto, List<ProductFixedDTO> filteredProductList, List<ProductFixedDTO> filteredDisabledOffers, int type,
	    List<CommercialOperation> commercialOpeList, List<MobileDTO> mobileDevices, List<ProductFixedDTO> productsMtList, String coverage, List<ParamMovTotal> lstParam) {

	List<ProductFixedDTO> actualProductList = new ArrayList<>();

	//List<ProductFixedDTO> productsToPrioritize = new ArrayList<>();
	//List<ProductFixedDTO> priorizatedProducts = new ArrayList<>();
	boolean ValidaReplace = commercialOpeList.get(0).getOperation().equals(Constant.OPERATION_REPLACEOFFER) ? true : false;
	boolean ValidaMovil = commercialOpeList.size() > 1 ? true : false;
	boolean ValidaPostpago = ValidaMovil && commercialOpeList.get(1).getDeviceOperation().equals(OperationDevice.CAEQ.getCodeDesc()) ? true : false;
	Integer typeTotalization = ValidaReplace && ValidaPostpago ? 1 : 2;
	Double arpa = fixedParkDto.getPresentARPA();
	String productType = commercialOpeList.get(0).getSubscriber() != null ? commercialOpeList.get(0).getSubscriber().getType() : "fijo";
	Character cobIntHfc = fixedParkDto.getHfcCobInternet() != null ? fixedParkDto.getHfcCobInternet() : '0';
	String actualTechnology = fixedParkDto.getTechnologyInternet() != null ? fixedParkDto.getTechnologyInternet() : "";
	
	if(actualTechnology != null) {
		if(actualTechnology.equals("HFC")) {
			if(cobIntHfc.toString().equals("2")) {
				coverage = "FTTH";
			}
		}
	}
	

	TotalUtil.obtainOfferMTWithPositiveJump(filteredProductList, actualProductList, productType, typeTotalization, arpa, coverage, lstParam, fixedParkDto);
	Collections.sort(actualProductList, ProductFixedDTO.jumpComparator);
	
	if (type == Constant.AVERIA_TIPO1) {
	    treatmentFaultType1(filteredProductList, filteredDisabledOffers,  commercialOpeList, actualProductList);
	} else if (type == Constant.AVERIA_TIPO2) {
	    treatmentFaultType2(filteredProductList, mobileDevices, actualProductList, productsMtList);
	} else if (type == Constant.AVERIA_TIPO3) {
	    return filteredProductList;
	}
	
	TotalUtil.availableOffersMT(actualProductList, Constant.BE_1055);

	return orderFailPriorization(filteredProductList, filteredDisabledOffers, actualProductList, coverage, cobIntHfc, fixedParkDto, commercialOpeList);
    }
    
    /***
     * Halla el prod actual por ps, y colocar todos los productos sin el actual en
     * una lista temporal.
     * 
     * @param filteredProductList
     * @param fixedParkDto
     * @param actualProductList
     */
    private void treatmentFaultType1(List<ProductFixedDTO> filteredProductList, List<ProductFixedDTO> filteredDisabledOffers, List<CommercialOperation> commercialOpeList,
	    List<ProductFixedDTO> actualProductList) {

    List<ProductFixedDTO> alternProductList = new ArrayList<ProductFixedDTO>();
	CommOperCollection<CommercialOperation> reqList = new CommOperCollection<>(commercialOpeList);
	reqList.filterByProduct(SuscriptionType.FIXED);
	String psRequest = reqList.get(0).getSubscriber().getSubscriberId();

	String merchandisingType = filteredProductList.get(0).getMerchandisingType();
	int numberGrillEnabled = Integer.valueOf(merchandisingType.substring(1));
	String grillOld = "P"+(numberGrillEnabled-1);
	String offerMirror = "";
	//alternProductList = actualProductList;
	
	for (ProductFixedDTO actualProduct : actualProductList) {
	    alternProductList.add(actualProduct);
	}
	
	actualProductList.removeAll(alternProductList);
	
	pmt = pmtr.findByCode(Constant.ACTUAL_TECHNOLOGY);
	String actGrill = "";
	Double maxRent = 0D;
	   
	   for (ProductFixedDTO product : filteredDisabledOffers) {
		    if (psRequest.equals(product.getPackagePs().toString())) {
		    	actGrill = product.getMerchandisingType();
		    	break;
		    }
	    }
	   
	   if(actGrill.equals("")) {
		   for (ProductFixedDTO product : filteredProductList) {
			   if (psRequest.equals(product.getPackagePs().toString())) {
			    	actGrill = product.getMerchandisingType();
			    	break;
			    }
		    }
	   }
	
	/*
	TotalUtil.removeRuleByRentFromProductOffers(filteredProductList, 359);
	for (ProductFixedDTO product : filteredProductList) {
	    if (psRequest.equals(product.getPackagePs().toString())) {
		actualProductList.add(product);
	    }
	}
	if (actualProductList.size() > 1) {
	    actualProductList.remove(Collections.max(actualProductList, ProductFixedDTO.rentComparator));
	}

	if (!actualProductList.isEmpty()) {
	    ProductFixedDTO product = actualProductList.get(0);
	    if (product.getPackageRent() != 199) {
		TotalUtil.removeRuleByRentFromProductOffers(filteredProductList, 199);
	    }
	  //marca
	}*/
	//else { 
	
	if(!actGrill.equals(pmt.getValor())) {
		    for (ProductFixedDTO product : filteredDisabledOffers) {
			    if (psRequest.equals(product.getPackagePs().toString())) {
				offerMirror = product.getMirrorOffer();
			    }
		    }
		    
		    for (ProductFixedDTO product : filteredProductList) {
			    if (offerMirror.equals(product.getMirrorOffer())) {
				actualProductList.add(product);
			    }
		    }
		    
		    if(actualProductList.isEmpty()) {
				/*Collections.sort(filteredProductList, new Comparator<ProductFixedDTO>(){

				    @Override
				    public int compare(ProductFixedDTO o1, ProductFixedDTO o2) {
				        return o1.getMirrorOffer().compareToIgnoreCase(o2.getMirrorOffer());
				    }
				});*/
			    	
			    Collections.sort(filteredProductList, ProductFixedDTO.rentComparator);
				
				String mirrorOffer = filteredProductList.get(filteredProductList.size()-1).getMirrorOffer();
				
				for (ProductFixedDTO product : filteredProductList) {
				    if (mirrorOffer.equals(product.getMirrorOffer().toString())) {
					actualProductList.add(product);
				    }
				}
				
			 }
			 
	}
	else {
		ProductFixedDTO actualProd = new ProductFixedDTO();
		Collections.sort(filteredProductList, ProductFixedDTO.rentComparator);
		
		//ProductFixedDTO ps_antiguo = new ProductFixedDTO();
		
		
		
		if(actualProductList.isEmpty()) {
			   for(ProductFixedDTO prod : filteredProductList) {
				   if(psRequest.equals(prod.getPackagePs().toString())) {
					   actualProd = prod;
					   break;
				   }
			   }
			   
			   if(!(actualProd.getPackageRent() == null)) {
			   
        			   for(ProductFixedDTO prod : filteredProductList) {
        				   maxRent = actualProd.getPackageRent();
        				   if(prod.getPackageRent()>actualProd.getPackageRent()) {
        					   actualProductList.add(prod);
        					   break;
        				   }
        			   }
			   
			   }else {
			       
			       for (ProductFixedDTO product : filteredDisabledOffers) {
				    if (psRequest.equals(product.getPackagePs().toString())) {
					actualProd = product;
				    }
				}
			       
			       for(ProductFixedDTO prod : filteredProductList) {
				   maxRent = actualProd.getPackageRent();
				   if(prod.getInternetSpeed().equals(actualProd.getInternetSpeed())) {
					   actualProductList.add(prod);
					   break;
				   }
			       }
			       
			   }
		}
		
		if(actualProductList.isEmpty()) {
			for(ProductFixedDTO prod : filteredProductList) {
				if(prod.getPackageRent()==maxRent) {
					   actualProductList.add(prod);
					   break;
				   }
			}
		}

	}
	
	actualProductList.addAll(alternProductList);
	
	    
	    
	    
	//}
    }

    /***
     * Halla el prod actual por bo y po, y colocar todos los productos sin el actual
     * en una lista temporal
     * 
     * @param filteredProductList
     * @param mobileDevices
     * @param actualProductList
     */
    private void treatmentFaultType2(List<ProductFixedDTO> filteredProductList, List<MobileDTO> mobileDevices,
	    List<ProductFixedDTO> actualProductList, List<ProductFixedDTO> productsMtList) {

	int index = 0;
	Integer boId = 0;
	for (int i = 0; i < mobileDevices.size(); i++) {
	    if (mobileDevices.get(i).getIsMtm() != null && mobileDevices.get(i).getBoId() != null && mobileDevices.get(i).getPoId() != null
		    && mobileDevices.get(i).getIsMtm()) {
		index = i;
		boId = mobileDevices.get(i).getBoId();
		break;
	    }
	}
	for (ProductFixedDTO product : filteredProductList) {
	    if (mobileDevices.get(index).getPoId().equals(Integer.parseInt(product.getPoPackage()))
		    && mobileDevices.get(index).getBoId().equals(Integer.parseInt(product.getPackageMobilCode()))) {
		actualProductList.add(product);
	    }
	}
	determinateIfActualProductListIsEmpty(actualProductList, filteredProductList, productsMtList, boId);
	determinateIfActualProductListIsNotEmpty(actualProductList, filteredProductList);
    }
    
    private void determinateIfActualProductListIsEmpty(List<ProductFixedDTO> actualProductList, List<ProductFixedDTO> filteredProductList,
	    List<ProductFixedDTO> productsMtList, Integer boId) {
	if (actualProductList.isEmpty()) {
	    ProductFixedDTO product = TotalUtil.getActualMirrorOfferMTByBo(filteredProductList, productsMtList, boId.toString());
	    if (product != null) {
		actualProductList.add(product);
	    }
	}
    }

    private void determinateIfActualProductListIsNotEmpty(List<ProductFixedDTO> actualProductList,
	    List<ProductFixedDTO> filteredProductList) {
	if (!actualProductList.isEmpty()) {
	    ProductFixedDTO product = actualProductList.get(0);
	    if (product.getPackageRent() != 359) {
		TotalUtil.removeRuleByRentFromProductOffers(filteredProductList, 359);
	    }
	    if (product.getPackageRent() != 199) {
		TotalUtil.removeRuleByRentFromProductOffers(filteredProductList, 199);
	    }
	}
    }
    
    /***
     * Se ordena segun renta, luego se recibe el producto actual como parametro y se
     * coloca como prioridad 1 y los dos consecutivos como prioridad 2 y 3
     * respectivamente.
     * 
     * @param temporalList
     * @param actualProductList
     * @return
     */
    
    /** Se separa las ofertas por tecnologia y se genera prioridad FTTH en caso el cliente sea 
    Overlay
    **/
    private List<ProductFixedDTO> orderFailPriorization(List<ProductFixedDTO> temporalList, List<ProductFixedDTO> filteredDisabledOffers, List<ProductFixedDTO> actualProductList, String evaluateCoverage, Character cobIntHfc, FixedDTO fixedParkDto,
    		List<CommercialOperation> commercialOpeList) {

	List<ProductFixedDTO> firstListItems = new ArrayList<>();
	List<ProductFixedDTO> secondListItems = new ArrayList<>();
	/*List<ProductFixedDTO> productsHFC = new ArrayList<>();
	List<ProductFixedDTO> productsFTTH = new ArrayList<>();
	
	TotalUtil.productsPerTechnology(temporalList, productsHFC, InternetTechnologyType.HFC.getCode());
	Collections.sort(productsHFC, ProductFixedDTO.rentComparator);
	
	TotalUtil.productsPerTechnology(temporalList, productsFTTH, InternetTechnologyType.FTTH.getCode());
	Collections.sort(productsFTTH, ProductFixedDTO.rentComparator);
	
	Collections.sort(temporalList, ProductFixedDTO.rentComparator);
	
	if(evaluateCoverage.equals(InternetTechnologyType.FTTH.getCode())) {
		TotalUtil.clearPriority(productsFTTH);
		TotalUtil.clearPriority(temporalList);
		setProductPriority(productsFTTH, actualProductList, firstListItems);
	}*/
	//else {
		setProductPriority(temporalList, filteredDisabledOffers, actualProductList, firstListItems, fixedParkDto, commercialOpeList);
		//firstListItems.add(actualProductList.get(0));
	//}

	return firstListItems;
    }
    
   private void  setProductPriority(List<ProductFixedDTO> temporalList, List<ProductFixedDTO> filteredDisabledOffers, List<ProductFixedDTO> actualProductList, List<ProductFixedDTO> firstListItems, FixedDTO fixedParkDto,
		   List<CommercialOperation> commercialOpeList) {
	   
	   /*CommOperCollection<CommercialOperation> reqList = new CommOperCollection<>(commercialOpeList);
		reqList.filterByProduct(SuscriptionType.FIXED);
		String psRequest = reqList.get(0).getSubscriber().getSubscriberId();
	   
	   //Integer ps = Integer.valueOf(psRequest);
	   pmt = pmtr.findByCode(Constant.ACTUAL_TECHNOLOGY);
	   String actGrill = "";
	   
	   for (ProductFixedDTO product : filteredDisabledOffers) {
		    if (psRequest.equals(product.getPackagePs().toString())) {
		    	actGrill = product.getMerchandisingType();
		    	break;
		    }
	    }
	   
	   if(actGrill.equals("")) {
		   for (ProductFixedDTO product : temporalList) {
			   if (psRequest.equals(product.getPackagePs().toString())) {
			    	actGrill = product.getMerchandisingType();
			    	break;
			    }
		    }
	   }*/
	   
	   
	   
	   if(actualProductList.size()>0) {
		   ProductFixedDTO actualProduct = actualProductList.get(0);
		   //if(!actGrill.equals(pmt.getValor())) {
			   actualProduct.setPriority("1");
			   firstListItems.add(actualProduct);
		   //}
		   /*else {
			   Collections.sort(temporalList, ProductFixedDTO.rentComparator);
			   for(ProductFixedDTO prod : temporalList) {
				   if(prod.getPackageRent()>actualProduct.getPackageRent()) {
					   prod.setPriority("1");
					   firstListItems.add(prod);
					   break;
				   }
			   }
		   }*/
	   }
   }
   
   
}
