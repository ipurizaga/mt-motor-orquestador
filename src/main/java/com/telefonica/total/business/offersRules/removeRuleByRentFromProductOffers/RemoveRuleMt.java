package com.telefonica.total.business.offersRules.removeRuleByRentFromProductOffers;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;

@Service
@Qualifier("plantMt")
public class RemoveRuleMt implements IRemoveRule {

    @Override
    public void execute(List<ProductFixedDTO> filteredProductList, FixedDTO fixedParkDto) {
	// TODO Auto-generated method stub

    }

    @Override
    public void execute(List<ProductFixedDTO> priorizationList, List<ProductFixedDTO> filteredProductList) {
	TotalUtil.removeOfferMtByRentFromNoPriorizited(priorizationList, filteredProductList, 199);
	TotalUtil.removeOfferMtByRentFromNoPriorizited(priorizationList, filteredProductList, 359);
    }

}
