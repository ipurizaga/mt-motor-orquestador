package com.telefonica.total.business.offersRules.evaluateSalesCondition;

import java.util.List;

import com.telefonica.total.dto.FourthDigitFinanceDTO;
import com.telefonica.total.dto.PartialRequestDTO;

public interface IEvaluateSalesCondition {

    /***
     * Métod que se encarga de la evaluacion de las condiciones de venta para
     * determinar si se le asignara la condicion de post pago facil o no.
     * 
     * @param partial
     * @param getFinancialPercentage
     */
    void execute(PartialRequestDTO partial, List<FourthDigitFinanceDTO> getFinancialPercentage);
}
