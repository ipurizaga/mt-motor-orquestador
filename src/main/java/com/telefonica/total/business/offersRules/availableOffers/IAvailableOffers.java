package com.telefonica.total.business.offersRules.availableOffers;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.PartialRequestDTO;
import com.telefonica.total.dto.ProductFixedDTO;

public interface IAvailableOffers {

    /***
     * Método que se encarga de agregar la lista priorizada dentro de el objeto
     * FixedDTO para que sea devuelto encapsulado.
     * 
     * @param fixedParkDto
     * @param priorizationList
     * @param mobileDevices
     * @return
     */
    PartialRequestDTO execute(FixedDTO fixedParkDto, List<ProductFixedDTO> priorizationList, List<MobileDTO> mobileDevices);

}
