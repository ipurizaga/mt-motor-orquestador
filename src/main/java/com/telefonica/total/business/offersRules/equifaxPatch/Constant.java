package com.telefonica.total.business.offersRules.equifaxPatch;

public class Constant {

    public static final Double	  CERO			 = 0D;
    public static final Integer	  CERO_INT		 = 0;
    public static final Character ZERO			 = '0';
    public static final Character ONE			 = '1';
    public static final Integer	  ONE_INT		 = 1;
    public static final Integer	  TWO_INT		 = 2;
}
