package com.telefonica.total.business.offersRules.verifyNumberOfMobiles;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;

public interface IVerifyNumberOfMobiles {

    /***
     * Método que se encarga de verificar si se esta enviando solo un movil en el
     * request y actualiza el indicador de onlyOneMobileIndicator
     * 
     * @param mobileDevices
     */
    void execute(FixedDTO fixedParkDto, List<MobileDTO> mobileDevices);
}
