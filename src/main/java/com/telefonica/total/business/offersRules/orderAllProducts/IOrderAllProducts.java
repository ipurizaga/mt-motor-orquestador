package com.telefonica.total.business.offersRules.orderAllProducts;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;

public interface IOrderAllProducts {

    /***
     * Método que se encarga de ordenar todos los productos que no sean los
     * priorizados enn orden descendente.
     * 
     * @param priorizationList
     * @param filteredProductList
     */
    void execute(List<ProductFixedDTO> priorizationList, List<ProductFixedDTO> filteredProductList, String coverage, Character cobIntHfc, FixedDTO fixedParkDto);
    void executeMT(List<ProductFixedDTO> priorizationList, List<ProductFixedDTO> filteredProductList, List<ProductFixedDTO> actualProducts, FixedDTO fixedParkDto);


}
