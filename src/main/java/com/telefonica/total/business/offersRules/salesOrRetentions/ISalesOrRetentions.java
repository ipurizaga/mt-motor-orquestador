package com.telefonica.total.business.offersRules.salesOrRetentions;

import com.telefonica.total.pojo.req.CommercialOperationInfo;

public interface ISalesOrRetentions {

    /***
     * Método que determina si se realiza una venta o una retención
     * 
     * @param commercialInfo
     */
    void execute(CommercialOperationInfo commercialInfo);
}
