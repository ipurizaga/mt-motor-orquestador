package com.telefonica.total.business.offersRules.determinatePromo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.RulesValueProp;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.common.util.UtilCollections;
import com.telefonica.total.dto.BeanMapper;
import com.telefonica.total.dto.CatalogDiscountTempDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.generic.CatalogDiscountCollection;
import com.telefonica.total.model.CatalogDiscountTemp;
import com.telefonica.total.model.ParamMovTotal;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.CommercialOperationInfo;
import com.telefonica.total.pojo.req.Customer;
import com.telefonica.total.pojo.req.Information;
import com.telefonica.total.repository.CatalogDiscountTempRepo;
import com.telefonica.total.repository.ParamMovTotalRepo;

@Service
public class DeterminatePromo implements IDeterminatePromo{

    @Autowired
    private CatalogDiscountTempRepo catalogDiscountTempRepo;
    
    @Autowired
    private BeanMapper beanMap;
    
    @Autowired
    private RulesValueProp rulesValue;
    
    @Autowired
    private ParamMovTotalRepo paramMovTotalRepo;
    
    private ParamMovTotal pmt;
    
    @Override
    public void execute(CommercialOperationInfo commercialOperationInfo, Customer customer,
	    List<ProductFixedDTO> priorizationList) {

	if (!customer.getIsEmployee()) {
	    CommercialOperation comFija = UtilCollections.getCollectionByType(commercialOperationInfo.getCommercialOpers(), Constant.FIX)
		    .get(0);
	    
	    List<Information> listAdditionalOperationInformation = commercialOperationInfo.getAdditionalOperationInformation();
	    
	    boolean existe=false;
	    int prod_playa_1 = 0;
	    
	    if(listAdditionalOperationInformation == null || listAdditionalOperationInformation.size() == 0) {
		existe = false;
	    }else {
	    
        	    for (Information information : listAdditionalOperationInformation) {
        		if(information.getCode().equals(rulesValue.PRODUCT_CHARACTERISTIC_BEACH_PRODUCT)) {
        		    if(information.getValue().equals("1")) {
        			//existe = true;
        			
        			prod_playa_1 = 1;
        		    }
        		}
        	    }
        	    
        	    if(prod_playa_1 == 1) {
        		
        		
        		existe = true;
        	    }
	    
	    }
	    
	    List<ParamMovTotal> getAllParam = paramMovTotalRepo.findAll();
		
		for (ParamMovTotal paramMovTotal : getAllParam) {
		    if(paramMovTotal.getGrupoParam().equals(Constant.GROUP_PARAM_DESC_TEMP_CAMPANIA) && paramMovTotal.getCodValor().equals(Constant.PRODUCTO_PLAYA)) {
			
			pmt = paramMovTotal;
			break;
			
		    }
		}
	    
	    
	    String origenProducto = null;
	    if (comFija.getSubscriber() != null && comFija.getSubscriber().getType() != null) {
		origenProducto = comFija.getSubscriber().getType();
	    }
	    List<CatalogDiscountTemp> temporalDiscounts = catalogDiscountTempRepo.findAll();
	    
	    List<CatalogDiscountTempDTO> temporalDiscountFilter = new ArrayList<CatalogDiscountTempDTO>();
	    //temporalDiscountFilter.addAll(temporalDiscounts);
	    for (CatalogDiscountTemp catDiscTemp : temporalDiscounts) {
		CatalogDiscountTempDTO cdtd = beanMap.catalogDiscountTempToDto(catDiscTemp);
		temporalDiscountFilter.add(cdtd);
	    }
	    CatalogDiscountCollection<CatalogDiscountTempDTO> catDisColl = new CatalogDiscountCollection<CatalogDiscountTempDTO>(temporalDiscountFilter);
	    
	    catDisColl.filterByType("VELOCIDAD");
	    
	    if(prod_playa_1 == 0) {
	    
        	    for (ProductFixedDTO productFixedDTO : priorizationList) {
        		
        		
        		for (CatalogDiscountTempDTO catDiscTemp : catDisColl) {
        		    
        		    if(!(catDiscTemp.getCampain().equals(pmt.getCol1()))) {
        		    
                		    if (evaluateCatDiscTempWithProductFixedDto(catDiscTemp, productFixedDTO, comFija) && !existe) {
                
                			CatalogDiscountTempDTO catDisTemp = beanMap.catalogDiscountTempToDto(catDiscTemp);
                			productFixedDTO.setCatalogPromoTempDTO(catDisTemp);
                			productFixedDTO.setCampaignPromo(catDisTemp.getCampain());
                		    }
        		    
        		    }
        		}
        	    }
	    
	    }else {
		for (ProductFixedDTO productFixedDTO : priorizationList) {
    		
    		
        		for (CatalogDiscountTemp catDiscTemp : temporalDiscounts) {
        		    
        		    if(catDiscTemp.getCampain().equals(pmt.getCol1())) {
        		    
                		    if (evaluateCatDiscTempWithProductFixedDto(catDiscTemp, productFixedDTO, comFija) /*&& !existe*/) {
                
                			CatalogDiscountTempDTO catDisTemp = beanMap.catalogDiscountTempToDto(catDiscTemp);
                			productFixedDTO.setCatalogPromoTempDTO(catDisTemp);
                			productFixedDTO.setCampaignPromo(catDisTemp.getCampain());
                		    }
        		    
        		    }
        		}
    	    	}
	    }
	    
	}
    }
    
private Boolean evaluateCatDiscTempWithProductFixedDto(CatalogDiscountTemp catDiscTemp, ProductFixedDTO productFixedDTO, CommercialOperation comFija) {
	
    /*if(!(((catDiscTemp.getOfferPS() == null || productFixedDTO.getPackagePs().intValue() == catDiscTemp.getOfferPS().intValue())
		&& ((catDiscTemp.getOfferPO() == null || catDiscTemp.getOfferPO().trim().isEmpty())
			|| productFixedDTO.getPoPackage().equals(catDiscTemp.getOfferPO()))
		&& ((catDiscTemp.getOfferBO() == null || catDiscTemp.getOfferBO().trim().isEmpty())
			|| productFixedDTO.getPackageMobilCode().equals(catDiscTemp.getOfferBO()))
		&& (catDiscTemp.getInitDate() != null && catDiscTemp.getEndDate() != null
			&& TotalUtil.isCurrentDateBetweenDates(catDiscTemp.getInitDate(), catDiscTemp.getEndDate()))
		&& ((comFija.getOperation() == null || comFija.getOperation().trim().isEmpty())
			|| (catDiscTemp.getOriginOpeCommercial() == null || catDiscTemp.getOriginOpeCommercial().trim().isEmpty())
			|| (comFija.getOperation().equals(catDiscTemp.getOriginOpeCommercial())))
		&& ((catDiscTemp.getOfferTechnology() == null || catDiscTemp.getOfferTechnology().trim().isEmpty())
			|| (catDiscTemp.getOfferTechnology().equals(productFixedDTO.getInternetDestinyTechnology())))))) {
	
	System.out.println("ESTO ES FALSE");
	
    }else {
	System.out.println("ESTO ES TRUE");
    }*/
	
	return ((catDiscTemp.getOfferPS() == null || productFixedDTO.getPackagePs().intValue() == catDiscTemp.getOfferPS().intValue())
		&& ((catDiscTemp.getOfferPO() == null || catDiscTemp.getOfferPO().trim().isEmpty())
			|| productFixedDTO.getPoPackage().equals(catDiscTemp.getOfferPO()))
		&& ((catDiscTemp.getOfferBO() == null || catDiscTemp.getOfferBO().trim().isEmpty())
			|| productFixedDTO.getPackageMobilCode().equals(catDiscTemp.getOfferBO()))
		&& (catDiscTemp.getInitDate() != null && catDiscTemp.getEndDate() != null
			&& TotalUtil.isCurrentDateBetweenDates(catDiscTemp.getInitDate(), catDiscTemp.getEndDate()))
		&& ((comFija.getOperation() == null || comFija.getOperation().trim().isEmpty())
			|| (catDiscTemp.getOriginOpeCommercial() == null || catDiscTemp.getOriginOpeCommercial().trim().isEmpty())
			|| (comFija.getOperation().equals(catDiscTemp.getOriginOpeCommercial())))
		&& ((catDiscTemp.getOfferTechnology() == null || catDiscTemp.getOfferTechnology().trim().isEmpty())
			|| (catDiscTemp.getOfferTechnology().equals(productFixedDTO.getInternetDestinyTechnology()))));
    }

    
}
