package com.telefonica.total.business.offersRules.calculateCommercialOpperCombinations;

import java.util.List;

import com.telefonica.total.pojo.req.CommercialOperationInfo;

public interface ICalculateCommercialOpperCombinations {
    
    void execute(CommercialOperationInfo commercialInfo, List<CommercialOperationInfo> commercialInfoList);

}
