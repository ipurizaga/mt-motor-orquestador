package com.telefonica.total.business.offersRules.orderAllProducts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.RulesValueProp;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.model.ParamMovTotal;
import com.telefonica.total.pojo.req.Information;
import com.telefonica.total.repository.ParamMovTotalRepo;
import com.telefonica.total.enums.InternetTechnologyType;

@Service
public class OrderAllProducts implements IOrderAllProducts{

    @Autowired
    private RulesValueProp rulesValue;
    
    @Autowired
    private ParamMovTotalRepo paramMovTotalRepo;
    
    @Override
    public void execute(List<ProductFixedDTO> priorizationList, List<ProductFixedDTO> filteredProductList, String coverage, Character cobIntHfc, FixedDTO fixedParkDto) {
	List<ProductFixedDTO> temporalList = new ArrayList<>();
	List<ProductFixedDTO> productsHFC = new ArrayList<>();
	List<ProductFixedDTO> productsFTTH = new ArrayList<>();
	String actualProduct = fixedParkDto.getTechnologyInternet() != null ? fixedParkDto.getTechnologyInternet() : "";
	
	if(actualProduct != null) {
		if(actualProduct.equals("HFC")) {
			if(cobIntHfc.toString().equals("2")) {
				coverage = "FTTH";
			}
		}
	}

    	
    filteredProductList.removeAll(priorizationList);	
	//Collections.sort(filteredProductList, ProductFixedDTO.rentComparator);
    	Collections.sort(filteredProductList, ProductFixedDTO.majorToMinorRentComparator);
	priorizationList.addAll(filteredProductList);
			
	//FIX PARA BUSCAR REPETIDOS EN priorizationList , LUEGO SE BORRA
	int t = priorizationList.size();
	int p2=0;
	
	for(int i=0; i<t; i++) {		
			if(i+1 == t) break;		
			if(priorizationList.get(i+1).getPriority()==null){
				p2 = 0;
			}else {
				p2 = Integer.parseInt(priorizationList.get(i+1).getPriority());
			}
			//Borrando reptidos con priorización distinta
			if(priorizationList.get(i).getPackagePs()==priorizationList.get(i+1).getPackagePs()
					&& priorizationList.get(i).getPackageMobilCode().equals(priorizationList.get(i+1).getPackageMobilCode())
					&& Integer.parseInt(priorizationList.get(i).getPriority())!=p2) {
				priorizationList.remove(i+1);
				break;
			}
			//Borrando repetidos con priorización igual
			else if(priorizationList.get(i).getPackagePs()==priorizationList.get(i+1).getPackagePs()
					&& priorizationList.get(i).getPackageMobilCode().equals(priorizationList.get(i+1).getPackageMobilCode())
					&& Integer.parseInt(priorizationList.get(i).getPriority())==p2) {
				priorizationList.remove(i+1);
				break;
			}				
	}
	
	//Separando ofertas por tecnología en caso sea Overlay se prioriza el minimo FTTH
	temporalList = priorizationList;
	TotalUtil.productsPerTechnology(priorizationList, productsFTTH, InternetTechnologyType.FTTH.getCode());
	TotalUtil.productsPerTechnology(priorizationList, productsHFC, InternetTechnologyType.HFC.getCode());
	
	if(!actualProduct.equals("FTTH")) {
		if( coverage.equals("FTTH")) {
			priorizationList.removeAll(temporalList);
			priorizationList.addAll(productsFTTH);
			priorizationList.addAll(productsHFC);
			TotalUtil.clearPriority(priorizationList);
			priorizationList.get(0).setPriority("1");
		}
	}
	
    }
    
    
    @Override
    public void executeMT(List<ProductFixedDTO> priorizationList, List<ProductFixedDTO> filteredProductList, List<ProductFixedDTO> actualProducts, FixedDTO fixedParkDto) {
	
	boolean flagMTDownsellPriority = false;
	
	for (ProductFixedDTO productFixedDTO : filteredProductList) {
	    
	    if(productFixedDTO.getFlagMTDownsellPriority()!=null) {
        	    if(productFixedDTO.getFlagMTDownsellPriority() == 1) {
        		flagMTDownsellPriority = true;
        		break;
        	    }
	    }
	}
	
	filteredProductList.removeAll(priorizationList);	
	Collections.sort(filteredProductList, ProductFixedDTO.majorToMinorRentComparator);
	priorizationList.addAll(filteredProductList);
	
	ProductFixedDTO productoActual = new ProductFixedDTO();
	
	
	
	
	int j = priorizationList.size();
	
	boolean existeActual = false, existeChrActual = false;
	
	for(int i=0; i<j; i++) {
	    
	    boolean eliminar = false;
	    
	    for (Information information : priorizationList.get(i).getCharacteristics()) {
		 if (information.getValue().equals(rulesValue.getPRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT())) {
		     existeChrActual = true;
		     eliminar = true;
		     existeActual = true;
		 }
	    }
	    
	    if(eliminar) {
		productoActual = priorizationList.get(i);
		priorizationList.remove(i);
		break;
	    }
	    
	}
	
	j = priorizationList.size();
	
	for(int i=0; i<j; i++) {
	    
	    boolean eliminar = false;
	    
	    if(priorizationList.get(i).getCurrentProduct()!=null) {	    
	    
        	    if(priorizationList.get(i).getCurrentProduct().equals(Constant.CURRENT_OFFER_TYPE)) {
        		     
        		eliminar = true;
        		existeActual = true;
        		 
        	    }
	    
	    }
	    
	    if(eliminar) {
		if(!existeChrActual) {
		    productoActual = priorizationList.get(i);
		}
		priorizationList.remove(i);
		break;
	    }
	    
	}
	
	ParamMovTotal paramMovSelected = new ParamMovTotal();
	
	List<ParamMovTotal> listParamMov = new ArrayList<ParamMovTotal>();
	
	listParamMov = paramMovTotalRepo.findAll();
	
	for (ParamMovTotal paramMovTotal : listParamMov) {
	    if(!(paramMovTotal.getCodValor()==null)){
        	    if(paramMovTotal.getCodValor().equals("um_teon")) {
        		paramMovSelected = paramMovTotal;
        		break;
        	    }
	    }
	}
	
	Collections.sort(priorizationList, ProductFixedDTO.minorToMajorRentComparator);
	
	//if(flagMTDownsellPriority) {
	
	    TotalUtil.priorizationsMigration(priorizationList, actualProducts, fixedParkDto, paramMovSelected);
	
	    /*ProductFixedDTO productoPriority1 = new ProductFixedDTO();
	    
	    for(int i=0; i<j; i++) {
		    
		    boolean eliminar = false;
		    
		    if(priorizationList.get(i).getPriority()!=null) {
		    
        		    if(priorizationList.get(i).getPriority().equals("1")) {
        			eliminar = true;
        		    }	
		    
		    }
		    
		    if(eliminar) {
			productoPriority1 = priorizationList.get(i);
			priorizationList.remove(i);
			break;
		    }
		    
		}*/
	    
	    TotalUtil.orderListPriorization(priorizationList, actualProducts);
	    
	    //Collections.sort(priorizationList, ProductFixedDTO.majorToMinorRentComparator);
	    
	    
	    
	    //priorizationList.add(0, productoPriority1);
	    
	    if(existeActual) {
		
		int i = 0, indice = -1;
		
		for (Information information : productoActual.getCharacteristics()) {
			 if (information.getValue().equals(rulesValue.getPRODUCT_CHARACTERISTIC_DOWNSELL())) {
			     indice = i;
			     break;
			 }
			 
			 i++;
		}
		
		if(indice >= 0) {
		    productoActual.getCharacteristics().remove(indice);
		}
		
		productoActual.setPriority(null);
		priorizationList.add(0, productoActual);
		
		//actualProducts.clear();
		    //actualProducts.add(productoActual);		
	    
	    }
	  
	    
	    
	    
	    boolean existe = false;
	    
	    for (ProductFixedDTO productFixedDTO : priorizationList) {
		    for (Information info : productFixedDTO.getCharacteristics()) {
			if(info.getValue().equals(rulesValue.getPRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT())) {
			    actualProducts.add(productFixedDTO);
			    existe = false;
			}
	    }
	    }
		    
		    if(!existe) {
			    for (ProductFixedDTO productFixedDTO : priorizationList) {
				if(productFixedDTO.getCurrentProduct()!=null) {	    
				    
		        	    if(productFixedDTO.getCurrentProduct().equals(Constant.CURRENT_OFFER_TYPE)) {
		        		actualProducts.add(productFixedDTO);
		        	    }
				}
			    }
		    }
	    
	boolean existePrioridad = false;    
	    
	for (ProductFixedDTO produPrior : priorizationList) {
	    if(produPrior.getPriority()!=null) {
		existePrioridad = true;
		break;
	    }
	}
			
	if(existePrioridad) {
	
	//FIX PARA BUSCAR REPETIDOS EN priorizationList , LUEGO SE BORRA
        	int t = priorizationList.size();
        	int p2=0;
        	
        	for(int i=0; i<t; i++) {		
        			if(i+1 == t) break;		
        			if(priorizationList.get(i+1).getPriority()==null){
        				p2 = 0;
        			}else {
        				p2 = Integer.parseInt(priorizationList.get(i+1).getPriority());
        			}
        			//Borrando reptidos con priorización distinta
        			if(priorizationList.get(i).getPriority()!=null && priorizationList.get(i).getPackagePs()==priorizationList.get(i+1).getPackagePs()
        					&& priorizationList.get(i).getPackageMobilCode().equals(priorizationList.get(i+1).getPackageMobilCode())
        					&& Integer.parseInt(priorizationList.get(i).getPriority())!=p2) {
        				priorizationList.remove(i+1);
        				break;
        			}
        			//Borrando repetidos con priorización igual
        			else if(priorizationList.get(i).getPriority()!=null && priorizationList.get(i).getPackagePs()==priorizationList.get(i+1).getPackagePs()
        					&& priorizationList.get(i).getPackageMobilCode().equals(priorizationList.get(i+1).getPackageMobilCode())
        					&& Integer.parseInt(priorizationList.get(i).getPriority())==p2) {
        				priorizationList.remove(i+1);
        				break;
        			}				
        	}
	
	}	

    }


}
