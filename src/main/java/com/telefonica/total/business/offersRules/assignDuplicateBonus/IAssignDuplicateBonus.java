package com.telefonica.total.business.offersRules.assignDuplicateBonus;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.pojo.req.CommercialOperation;

public interface IAssignDuplicateBonus {

    /***
     * Método que se encarga de las asignaciones de los bonos dupica para los casos
     * de cambio de plan y portabilidad.
     * 
     * @param fixedParkDto
     * @param filteredProductList
     * @param caplOrPortability
     * @param commercialOpeList
     */
    void execute(FixedDTO fixedParkDto, List<ProductFixedDTO> filteredProductList, boolean caplOrPortability,
	    List<CommercialOperation> commercialOpeList);
}
