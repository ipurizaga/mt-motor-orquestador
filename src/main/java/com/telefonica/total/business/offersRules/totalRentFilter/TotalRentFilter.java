package com.telefonica.total.business.offersRules.totalRentFilter;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.RulesValueProp;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.pojo.req.Information;

@Service
public class TotalRentFilter implements ITotalRentFilter{

    @Autowired
    private RulesValueProp rulesValue;
    
    @Override
    public void execute(List<ProductFixedDTO> filteredProductList) {
	for (ProductFixedDTO product : filteredProductList) {
	    if (product.getJump() < rulesValue.getPRODUCT_JUMP_CERO()) {
		product.getCharacteristics().add(Information.builder().code(rulesValue.getPRODUCT_CHARACTERISTIC())
			.value(rulesValue.getPRODUCT_CHARACTERISTIC_DOWNSELL()).build());
		product.getCharacteristics().add(Information.builder().code(rulesValue.getPRODUCT_IND_OFFER())
			.value(rulesValue.getPRODUCT_IND_OFFER_DOWN()).build());
	    }else {
		product.getCharacteristics().add(Information.builder().code(rulesValue.getPRODUCT_CHARACTERISTIC())
			.value(rulesValue.getPRODUCT_CHARACTERISTIC_UPSELL()).build());
		product.getCharacteristics().add(Information.builder().code(rulesValue.getPRODUCT_IND_OFFER())
			.value(rulesValue.getPRODUCT_IND_OFFER_UP()).build());
	    }
	}
    }
}
