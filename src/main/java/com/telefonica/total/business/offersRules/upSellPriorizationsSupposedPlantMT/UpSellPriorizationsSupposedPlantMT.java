package com.telefonica.total.business.offersRules.upSellPriorizationsSupposedPlantMT;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.business.offersRules.productsRules.IProductsRules;
import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.PartialRequestDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.enums.OperationDevice;
import com.telefonica.total.exception.BusinessException;
import com.telefonica.total.generic.CommOperCollection;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.CommercialOperationInfo;
import com.telefonica.total.pojo.req.Offer;

@Service
public class UpSellPriorizationsSupposedPlantMT implements IUpSellPriorizationsSupposedPlantMT{

    @Autowired
    private IProductsRules productRules;
    
    @Override
    public List<ProductFixedDTO> executeMt(List<ProductFixedDTO> filteredProductList,
	    List<ProductFixedDTO> productsMtList, FixedDTO fixedParkDto, CommercialOperationInfo commercialInfo, List<ProductFixedDTO> actualProducts) {
	TotalUtil.removeRuleByRentFromProductOffers(filteredProductList, 359);
	//Se corrigió : Se estaba usando la lista de productos MT actuales (filteredProductList)
	//en lugar de la lista de todos los productos MT (productsMtList)
	
	ProductFixedDTO product_current = new ProductFixedDTO ();
	
	if(commercialInfo.getCommercialOpers().size() == 3) {
		
	    product_current = findCurrentOffer2Mobile(commercialInfo);
	
	}else if(commercialInfo.getCommercialOpers().size() == 2) {
	    
	    product_current = findCurrentOffer1Mobile(commercialInfo);
	    
	    
	}
	
	product_current.setCoverageRquest(fixedParkDto.getCoverage());
	product_current.setCob_hfc(fixedParkDto.getHfcCobInternet());
	product_current.setTypeF(fixedParkDto.getTypeF());
	product_current.setTypeM1(fixedParkDto.getTypeM1());
	product_current.setTypeM2(fixedParkDto.getTypeM2());
	product_current.setDeviceOperationM1(fixedParkDto.getDeviceOperationM1());
	product_current.setDeviceOperationM2(fixedParkDto.getDeviceOperationM2());
	product_current.setQuantityMobile(fixedParkDto.getQuantityMobile());
	
	//List<ProductFixedDTO> products = new ArrayList<>();
	//ProductFixedDTO product = TotalUtil.getOfferMTByBo(productsMtList, String.valueOf(fixedParkDto.getBoIdRequest()));
	actualProducts.add(product_current);
	
	ProductFixedDTO product_copy = new ProductFixedDTO();
	
	for (ProductFixedDTO productFixedDTOFinal : filteredProductList) {
	    
	    if(product_current.getMerchandisingType().equals(productFixedDTOFinal.getMerchandisingType())
		    && product_current.getInternetSpeed().equals(productFixedDTOFinal.getInternetSpeed())
		    && !(product_current.getPackageRent().equals(productFixedDTOFinal.getPackageRent()))) {
		product_copy = productFixedDTOFinal;
		break;
	    }
	    
	}
	
	filteredProductList.remove(product_copy);
	
	//System. out. println("Tamaño" + products.size());
	TotalUtil.availableOffersMT(actualProducts, Constant.BE_1055);
	return upSellPriorizations(filteredProductList, actualProducts, product_current);
    }
    
    public List<ProductFixedDTO> upSellPriorizations(List<ProductFixedDTO> filteredProductList, List<ProductFixedDTO> actualProducts, ProductFixedDTO product) {
	List<ProductFixedDTO> priorizatedProducts = new ArrayList<>();
	Collections.sort(filteredProductList, ProductFixedDTO.rentComparator);
	int iterator = 1;
	boolean prioridad_1 = false;
	for (ProductFixedDTO fixProd : filteredProductList) {
	    
	    if (/*iterator > 2 &&*/ prioridad_1 == true) {
		break;
	    }
	    
	    //if (fixProd.getPackageRent() >= product.getPackageRent()) {
	    //if (fixProd.getPackageRent() >= actualProductList.get(0).getPackageRent()) {
	    //if (fixProd.getMirrorOffer().equals(product.getMirrorOffer())) {
	    
	    
	    if(actualProducts.get(0).getCoverageRquest().equals("HFC") && !((actualProducts.get(0).getCob_hfc() == null ? "": actualProducts.get(0).getCob_hfc()).toString().equals("2"))) {
		
		
		
		if(actualProducts.get(0).getTypeF() != null) {
        		if(actualProducts.get(0).getTypeF().equals(Constant.TYPE_MTF)) {
        			if(actualProducts.get(0).getTypeM1()!=null && actualProducts.get(0).getTypeM2()==null) {
        			    
        			    if((actualProducts.get(0).getTypeM1().equals(Constant.TYPE_MTM)) && 
        				    (actualProducts.get(0).getDeviceOperationM1().equals(OperationDevice.ACTIVATION.getCodeDesc()) ||
        					    actualProducts.get(0).getDeviceOperationM1().equals(OperationDevice.PORTABILITY.getCodeDesc())  )) {
        				
        			    }else if((actualProducts.get(0).getTypeM1().equals(Constant.TYPE_MTM))) {
        				
        				
                        		if (fixProd.getPackageRent() >= actualProducts.get(0).getPackageRent() &&  fixProd.getInternetSpeed() >= actualProducts.get(0).getInternetSpeed()) {
                        		    fixProd.setPriority(String.valueOf(1));
                        			priorizatedProducts.add(fixProd);
                        			
                        			prioridad_1 = true;
                        			
                        		}	
        			    }
        			    
        			    
        			    
        			}
        			    
        			if(actualProducts.get(0).getTypeM1()!=null && actualProducts.get(0).getTypeM2()!=null) {
        			    if(actualProducts.get(0).getTypeM1().equals(Constant.TYPE_MTM) && actualProducts.get(0).getTypeM2().equals(Constant.TYPE_MTM)) {
            				if (fixProd.getPackageRent() >= actualProducts.get(0).getPackageRent() &&  fixProd.getInternetSpeed() >= actualProducts.get(0).getInternetSpeed()) {
                        				fixProd.setPriority(String.valueOf(1));
                        				priorizatedProducts.add(fixProd);
                        				
                        				prioridad_1 = true;                        			
            				}
        			    }
        			    
        			    if(actualProducts.get(0).getTypeM1().equals(Constant.TYPE_MTM) && actualProducts.get(0).getTypeM2().equals(Constant.TYPE_LMA)) {
        				if(actualProducts.get(0).getDeviceOperationM2().equals(OperationDevice.CAEQ.getCodeDesc())) {
                				if (fixProd.getPackageRent() >= actualProducts.get(0).getPackageRent() &&  fixProd.getInternetSpeed() >= actualProducts.get(0).getInternetSpeed()) {
                				    	fixProd.setPriority(String.valueOf(1));
                					priorizatedProducts.add(fixProd);
                					
                					prioridad_1 = true;                    			
                				}
        				}
        			    }
        			    
        			    if(actualProducts.get(0).getTypeM1().equals(Constant.TYPE_LMA) && actualProducts.get(0).getTypeM2().equals(Constant.TYPE_MTM)) {
        				if(actualProducts.get(0).getDeviceOperationM2().equals(OperationDevice.CAEQ.getCodeDesc())) {
                				if (fixProd.getPackageRent() >= actualProducts.get(0).getPackageRent() &&  fixProd.getInternetSpeed() >= actualProducts.get(0).getInternetSpeed()) {
                				    	fixProd.setPriority(String.valueOf(1));
                					priorizatedProducts.add(fixProd);
                					
                					prioridad_1 = true;                    			
                				}	
        				}
        			    }	
        			}
        		 }else {
        		     if (fixProd.getPackageRent() >= actualProducts.get(0).getPackageRent() ) {
        			 	fixProd.setPriority(String.valueOf(1));
        				priorizatedProducts.add(fixProd);
        				
        				prioridad_1 = true;
        				
        		     }
        		 }
		}
	    }else {
		if (fixProd.getMirrorOffer().equals(product.getMirrorOffer())) {
			fixProd.setPriority(String.valueOf(1));
			priorizatedProducts.add(fixProd);
			
		}
	    }
	    
		//fixProd.setPriority(String.valueOf(1));
		//priorizatedProducts.add(fixProd);
		
		//prioridad_1 = true;
		
			
	    /*}else {
		if (iterator <= 2 && prioridad_1 == true) {
        		fixProd.setPriority(String.valueOf(iterator+1));
        		priorizatedProducts.add(fixProd);
        		
        		iterator++;
		}
	    }*/
		//iterator++;
	    //}
	}
	
	//posiblemente lo comentare
	if(priorizatedProducts.size() == 0) {
	    ProductFixedDTO fixProd = new ProductFixedDTO();
	    
	    fixProd = filteredProductList.get(filteredProductList.size()-1);
	    fixProd.setPriority(String.valueOf(1));
	    priorizatedProducts.add(fixProd);
	}
	
	priorizatedProducts.add(actualProducts.get(0));
	
	return priorizatedProducts;
    }
    
    public ProductFixedDTO findCurrentOffer2Mobile(CommercialOperationInfo commercialInfo) {

	CommOperCollection<CommercialOperation> commOperCollection = new CommOperCollection<>(commercialInfo.getCommercialOpers());
	
	List<ProductFixedDTO> catalogProductFixedDto = productRules.getProductFixed();
	
	ProductFixedDTO prod_current = new ProductFixedDTO();
	
	boolean sin_segundo_subscriber = true;
	
	if((commercialInfo.getCommercialOpers().get(2).getDeviceOperation().equals(Constant.OPERATION_REPLACEOFFER) ||
		commercialInfo.getCommercialOpers().get(2).getDeviceOperation().equals(Constant.OPERATION_PORTABILITY)
		
		&& commercialInfo.getCommercialOpers().get(1).getSubscriber().getType().equals(Constant.TYPE_MTM)))
	    
	{
	    
	    String parrilla = "";
		
            for (ProductFixedDTO productFixedDTO6 : catalogProductFixedDto) {
            		    
            	if(commOperCollection.get(1).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO6.getPackageMobilCode()))) {
            			
            		parrilla = productFixedDTO6.getMerchandisingType();
            			break;		
            	}
            		    
            }
            
            boolean existe=false;
		
		for (ProductFixedDTO productFixedDTO7 : catalogProductFixedDto) {
		
		    	if(productFixedDTO7.getPackagePs().equals(Integer.parseInt(commOperCollection.get(0).getSubscriber().getSubscriberId()))
		    		&& productFixedDTO7.getMerchandisingType().equals(parrilla)) {
			
		    	    //System.out.println("chau");		
		    	    
		    	    productFixedDTO7.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
		    	    productFixedDTO7.setMobil0QuantityData(productFixedDTO7.getMobil0QuantityData()+productFixedDTO7.getMobil1QuantityData());
		    	    prod_current = productFixedDTO7;
			    existe = true;
			    break;
			}
		}
		
		if(!existe) {
		    throw new BusinessException(Constant.BE_1057);
		}
	}else {
	
	for (ProductFixedDTO productFixedDTO : catalogProductFixedDto) {
	    
	    if (commOperCollection.get(2).getSubscriber()!=null) {
		if(commOperCollection.get(2).getSubscriber().getBoId()!=null) {
		
        	    if(productFixedDTO.getPackagePs().equals(Integer.parseInt(commOperCollection.get(0).getSubscriber().getSubscriberId()))) {
        		
        		//System.out.println("chau");
        		
        		if(commOperCollection.get(1).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO.getPackageMobilCode()))) {
        		    productFixedDTO.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);	        		    
        		    prod_current = productFixedDTO;
        		    sin_segundo_subscriber = false;
        		}else if (commOperCollection.get(2).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO.getPackageMobilCode()))) {
        		    productFixedDTO.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
        		    prod_current = productFixedDTO;
        		    sin_segundo_subscriber = false;
        		}
        		
        	    }
		}else {
		    if(productFixedDTO.getPackagePs().equals(Integer.parseInt(commOperCollection.get(0).getSubscriber().getSubscriberId()))) {
        		
        		//System.out.println("chau");
        		
        		if(commOperCollection.get(1).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO.getPackageMobilCode()))) {
        		    productFixedDTO.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);	        		    
        		    prod_current = productFixedDTO;
        		    sin_segundo_subscriber = false;
        		}
        		
        	    }
		}
	    }else {
		if(productFixedDTO.getPackagePs().equals(Integer.parseInt(commOperCollection.get(0).getSubscriber().getSubscriberId()))) {
    		
        		//System.out.println("chau");
        		
        		if(commOperCollection.get(1).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO.getPackageMobilCode()))) {
        		    productFixedDTO.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);        		    
        		    prod_current = productFixedDTO;
        		    sin_segundo_subscriber = false;
        		}
        		
        	 }
	    }
	    
	}
	
	if(sin_segundo_subscriber) {
	    
	    String parrilla = "";
	    
	    for (ProductFixedDTO productFixedDTO : catalogProductFixedDto) {
		    
		    if(commOperCollection.get(1).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO.getPackageMobilCode()))) {
			
			parrilla = productFixedDTO.getMerchandisingType();
			break;		
		    }
		    
	    }
	    
	    boolean existe=false;
		
		for (ProductFixedDTO productFixedDTO2 : catalogProductFixedDto) {
		
		    	if(productFixedDTO2.getPackagePs().equals(Integer.parseInt(commOperCollection.get(0).getSubscriber().getSubscriberId()))
		    		&& productFixedDTO2.getMerchandisingType().equals(parrilla)) {
			
		    	    //System.out.println("chau");		
		    	    
		    	    productFixedDTO2.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
		    	    prod_current = productFixedDTO2;
			    
			    existe = true;
			    break;
			}
		}
		
		
		if(!existe) {
		    
			for (ProductFixedDTO productFixedDTO11 : catalogProductFixedDto) {
				
			    	if(productFixedDTO11.getPackagePs().equals(Integer.parseInt(commOperCollection.get(0).getSubscriber().getSubscriberId()))
			    		&& productFixedDTO11.getMerchandisingType().equals("P3")) {
				
			    	    //System.out.println("chau");		
			    	    
			    	    productFixedDTO11.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
			    	    prod_current = productFixedDTO11;
				    
				    existe = true;
				    break;
				}
			}
		    
		}
		
		if(!existe) {
		    throw new BusinessException(Constant.BE_1057);
		}
	    
	}
	
	}
	
	//System.out.println("hola");
	
	return prod_current;
	
    }
    
    public ProductFixedDTO findCurrentOffer1Mobile(CommercialOperationInfo commercialInfo) {

	
	
	CommOperCollection<CommercialOperation> commOperCollection = new CommOperCollection<>(commercialInfo.getCommercialOpers());
	
	ProductFixedDTO prod_current = new ProductFixedDTO();
		
	List<ProductFixedDTO> catalogProductFixedDto = productRules.getProductFixed();
	
	String parrillaFijo = "", parrillaMovil1 = ""; 
	
	
	parrillaFijo = hallarParrilaxPS(catalogProductFixedDto, (commOperCollection.get(1).getSubscriber().getSubscriberId() == null? "0": commOperCollection.get(1).getSubscriber().getSubscriberId()));
	
	String parrilla = "";
	
	for (ProductFixedDTO productFixedDTO : catalogProductFixedDto) {
	    
	    if(commOperCollection.get(1).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO.getPackageMobilCode()))) {
		
		parrilla = productFixedDTO.getMerchandisingType();
		break;		
	    }
	    
	}
	
	boolean existe=false;
	
	if(parrillaFijo.equals("P3")) {
	    if(parrilla.equals("P1") || parrilla.equals("P2")) {
		for (ProductFixedDTO productFixedDTO2 : catalogProductFixedDto) {
		    if(productFixedDTO2.getMerchandisingType().equals(parrillaFijo)) {
				productFixedDTO2.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
		    	    prod_current = productFixedDTO2;
			    
			    existe = true;
			    break;
		    }
		}
	    }
	}else if(parrillaFijo.equals("P1") || parrillaFijo.equals("P2")) {
	    if(parrilla.equals("P1") || parrilla.equals("P2")) {
		for (ProductFixedDTO productFixedDTO2 : catalogProductFixedDto) {
    		    if(productFixedDTO2.getMerchandisingType().equals("P2")) {
    			productFixedDTO2.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
        	    	    prod_current = productFixedDTO2;
        		    
        		    existe = true;
        		    break;
    		    }
		}
	    }
	}
	
	
	
	for (ProductFixedDTO productFixedDTO2 : catalogProductFixedDto) {
	
	    	if(productFixedDTO2.getPackagePs().equals(Integer.parseInt(commOperCollection.get(0).getSubscriber().getSubscriberId()))
	    		&& productFixedDTO2.getMerchandisingType().equals(parrilla)) {
		
	    	    //System.out.println("chau");		
	    	    
	    	    productFixedDTO2.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
	    	    prod_current = productFixedDTO2;
		    
		    existe = true;
		    break;
		}
	}
	
	if(!existe) {
	
	    	String psfijo = "", psfijoreq = "";
	    
        	for (ProductFixedDTO productFixedDTO2 : catalogProductFixedDto) {
        		
        	    	psfijo = String.valueOf(productFixedDTO2.getPackagePs());
        	    	
        	    	psfijoreq = commOperCollection.get(0).getSubscriber().getSubscriberId();
        	    
        	    	if(productFixedDTO2.getPackagePs().equals(Integer.parseInt(commOperCollection.get(0).getSubscriber().getSubscriberId()))
        	    		) {
        		
        	    	    //System.out.println("chau");		
        	    	    
        	    	    productFixedDTO2.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
        	    	    prod_current = productFixedDTO2;
        		    
        		    existe = true;
        		    break;
        		}
        	}
	
	}
	
	if(!existe) {
	    
		for (ProductFixedDTO productFixedDTO11 : catalogProductFixedDto) {
			
		    	if(productFixedDTO11.getPackagePs().equals(Integer.parseInt(commOperCollection.get(0).getSubscriber().getSubscriberId()))
		    		&& productFixedDTO11.getMerchandisingType().equals("P3")) {
			
		    	    //System.out.println("chau");		
		    	    
		    	    productFixedDTO11.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
		    	    prod_current = productFixedDTO11;
			    
			    existe = true;
			    break;
			}
		}
	    
	}
	
	if(!existe) {

	    throw new BusinessException(Constant.BE_1057);
	}
	
	//System.out.println("hola");
	return prod_current;
	
	
    }
    
    public String hallarParrilaxPS(List<ProductFixedDTO> catalogProductFixedDto, String ps) {
	
	String parrilla="";
	
	for (ProductFixedDTO productFixedDTO : catalogProductFixedDto) {
	    
	    String psfijo = String.valueOf(productFixedDTO.getPackagePs());
	    
	    if(ps.equals(psfijo)) {
		
		parrilla = productFixedDTO.getMerchandisingType();
		break;		
	    }
	    
	}
	
	return parrilla;
    }

}
