package com.telefonica.total.business.offersRules.actualProductFilterEnabledOffers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.RulesValueProp;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.enums.OperationDevice;
import com.telefonica.total.pojo.req.CommercialOperationInfo;
import com.telefonica.total.pojo.req.Information;

@Service
public class ActualProductFilterEnabledOffers implements IActualProductFilterEnabledOffers{

    @Autowired
    private RulesValueProp rulesValue;
    
    @Override
    public void executeMt(FixedDTO fixedParkDto, List<ProductFixedDTO> filteredProductList, CommercialOperationInfo commercialInfo) {
	// busca el producto actual por la ps - po - bo
	Double packageRent = null;
	for (ProductFixedDTO product : filteredProductList) {
	    if (fixedParkDto.getPsRequest().equals(product.getPackagePs().toString())
		    && fixedParkDto.getPoIdRequest().equals(Integer.parseInt(product.getPoPackage()))
		    && fixedParkDto.getBoIdRequest().equals(Integer.parseInt(product.getPackageMobilCode()))) {
		product.getCharacteristics().add(Information.builder().code(rulesValue.getPRODUCT_CHARACTERISTIC())
			.value(rulesValue.getPRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT()).build());
		
		if(commercialInfo.getCommercialOpers().size()==3) {
        		if(commercialInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.ACTIVATION.getCodeDesc())||
        			    commercialInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.CAEQ.getCodeDesc())||
        			    commercialInfo.getCommercialOpers().get(2).getDeviceOperation().equals(OperationDevice.PORTABILITY.getCodeDesc())) {
        		    product.setMobil0QuantityData(product.getMobil0QuantityData());
        		}
		}else {
		    product.setMobil0QuantityData(product.getMobil0QuantityData()+product.getMobil1QuantityData());
		}
		packageRent = product.getPackageRent();
		break;
	    }
	}
	if (packageRent != null && packageRent != 359) {
	    TotalUtil.removeRuleByRentFromProductOffers(filteredProductList, 359);
	}
	if (packageRent != null && packageRent != 199) {
	    TotalUtil.removeRuleByRentFromProductOffers(filteredProductList, 199);
	}
    }
}
