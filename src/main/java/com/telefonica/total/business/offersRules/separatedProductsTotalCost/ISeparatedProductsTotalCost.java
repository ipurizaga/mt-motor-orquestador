package com.telefonica.total.business.offersRules.separatedProductsTotalCost;

import java.util.List;

import com.telefonica.total.dto.ProductFixedDTO;

public interface ISeparatedProductsTotalCost {

    /***
     * Método que se encarga de calcular los costos a pagar por el cliente si este
     * contratase los productos por separado.
     * 
     * @param priorizationList
     */
    void execute(List<ProductFixedDTO> priorizationList);
}
