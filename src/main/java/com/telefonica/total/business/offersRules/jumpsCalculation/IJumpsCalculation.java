package com.telefonica.total.business.offersRules.jumpsCalculation;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.Customer;

public interface IJumpsCalculation {

    /***
     * Método que se encarga del cálculo de los saltos.
     * 
     * @param fixedParkDto
     * @param filteredProductList
     * @param mobileDevices
     * @param customer
     */
    void execute(FixedDTO fixedParkDto, List<ProductFixedDTO> filteredProductList, List<MobileDTO> mobileDevices,
	    List<CommercialOperation> commercialOpeList, Customer customer);
}
