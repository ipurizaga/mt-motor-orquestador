package com.telefonica.total.business.offersRules.jumpsCalculation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.RulesValueProp;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.common.util.UtilCollections;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.generic.CommOperCollection;
import com.telefonica.total.model.ParamMovTotal;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.Customer;
import com.telefonica.total.repository.ParamMovTotalRepo;
import com.telefonica.total.common.util.Constant;
import org.apache.commons.lang3.StringUtils;

@Service
public class JumpsCalculation implements IJumpsCalculation{
    
    @Autowired
    private RulesValueProp rulesValue;
    
    @Autowired
    private ParamMovTotalRepo paramMovTotalRepo;
    
    @Override
    public void execute(FixedDTO fixedParkDto, List<ProductFixedDTO> filteredProductList, List<MobileDTO> mobileDevices,
	    List<CommercialOperation> commercialOpeList, Customer customer) {
	CommOperCollection<CommercialOperation> commOperCollection = new CommOperCollection<>(commercialOpeList);
	commOperCollection.filterByProduct(SuscriptionType.MOBILE);
	double mobileRent = 0;
	for (MobileDTO mobile : mobileDevices) {
	    mobileRent += mobile.getMobileRentMonoProduct() - Math.abs(mobile.getPermanentDisccount());
	}
	/* CÃ¡lculo del salto */
	
	Double originRent = (fixedParkDto.getPackageRent() != null) ? fixedParkDto.getPackageRent() : Constant.CERO;
	Double cableRentMonoProdPremHd = (fixedParkDto.getCableRentMonoProdPremHd() != null) ? fixedParkDto.getCableRentMonoProdPremHd() : Constant.CERO;
	Double permanentDiscount = (fixedParkDto.getPermanentDiscount() != null) ? fixedParkDto.getPermanentDiscount() : Constant.CERO;
	Double rentMonoProdLine = (fixedParkDto.getRentMonoProdLine() != null) ? fixedParkDto.getRentMonoProdLine() : Constant.CERO;
	Double rentMonoProdInternet = (fixedParkDto.getRentMonoProdInternet() != null) ? fixedParkDto.getRentMonoProdInternet() : Constant.CERO;
	Double cableRentMonoProd = (fixedParkDto.getCableRentMonoProd() != null) ? fixedParkDto.getCableRentMonoProd() : Constant.CERO;
	Double cableRentMonoProdPremFox = (fixedParkDto.getCableRentMonoProdPremFox() != null) ? fixedParkDto.getCableRentMonoProdPremFox() : Constant.CERO;
	Double cableRentMonoProdPremHbo = (fixedParkDto.getCableRentMonoProdPremHbo() != null) ? fixedParkDto.getCableRentMonoProdPremHbo() : Constant.CERO;
	
	for (ProductFixedDTO product : filteredProductList) {
		double gap = 0D;
		double packageRent = product.getPackageRent() != null ? product.getPackageRent() : 0D;
	    double actualRent = originRent * TotalUtil.getNumericValueOf(product.getIsFixedProduct())
		    - Math.abs(permanentDiscount) * TotalUtil.getNumericValueOf(product.getIsFixedProduct())
		    + rentMonoProdLine * TotalUtil.getNumericValueOf(product.getLinePresent())
		    + rentMonoProdInternet * TotalUtil.getNumericValueOf(product.getInternetPresent())
		    + cableRentMonoProd * TotalUtil.getNumericValueOf(product.getCablePresent())
		    + cableRentMonoProdPremHd * TotalUtil.getNumericValueOf(product.getPremiumHdPresent())
		    
		    + cableRentMonoProdPremFox * TotalUtil.getNumericValueOf(product.getPremiumFoxPresent())
		    + cableRentMonoProdPremHbo * TotalUtil.getNumericValueOf(product.getPremiumHboPresent())
		    + (TotalUtil.getNumericValueOf(rulesValue.getTYPE_PACKAGE_4PLAY().equals(product.getPackageType()))) * mobileRent;
	    product.setJump(packageRent - actualRent + getCableRentMonoProdEstellar(fixedParkDto, commercialOpeList) + getCablePresentPremFox(fixedParkDto, commercialOpeList) + getCablePresentPremHbo(fixedParkDto, commercialOpeList));
	    //Se Setea el gap_velocity del producto actual , en porcentaje
	    if(fixedParkDto.getSpeedInternet()!=null) {
	    	gap = ((product.getInternetSpeed() - fixedParkDto.getSpeedInternet())*1.0/fixedParkDto.getSpeedInternet()) * 100;
	    }
	    
	    product.setGap_velocity(gap);
	}
	/* Calcular el salto Fijo */
	fixedJumpCalculation(fixedParkDto, filteredProductList, commercialOpeList);
	if (customer.getIsEmployee()) {
	    employeeJumpCalculation(fixedParkDto, mobileDevices, filteredProductList);
	}
    }
    
    private void fixedJumpCalculation(FixedDTO fixedParkDto, List<ProductFixedDTO> filteredProductList, List<CommercialOperation> commercialOpeList) {
	
	Double packageRent = (fixedParkDto.getPackageRent() != null) ? fixedParkDto.getPackageRent() : Constant.CERO;
	Double cableRentMonoProdPremHd = (fixedParkDto.getCableRentMonoProdPremHd() != null) ? fixedParkDto.getCableRentMonoProdPremHd() : Constant.CERO;
	Double permanentDiscount = (fixedParkDto.getPermanentDiscount() != null) ? fixedParkDto.getPermanentDiscount() : Constant.CERO;
	Double rentMonoProdLine = (fixedParkDto.getRentMonoProdLine() != null) ? fixedParkDto.getRentMonoProdLine() : Constant.CERO;
	Double rentMonoProdInternet = (fixedParkDto.getRentMonoProdInternet() != null) ? fixedParkDto.getRentMonoProdInternet() : Constant.CERO;
	Double cableRentMonoProd = (fixedParkDto.getCableRentMonoProd() != null) ? fixedParkDto.getCableRentMonoProd() : Constant.CERO;
	Double cableRentMonoProdPremFox = (fixedParkDto.getCableRentMonoProdPremFox() != null) ? fixedParkDto.getCableRentMonoProdPremFox() : Constant.CERO;
	Double cableRentMonoProdPremHbo = (fixedParkDto.getCableRentMonoProdPremHbo() != null) ? fixedParkDto.getCableRentMonoProdPremHbo() : Constant.CERO;
	
	
	for (ProductFixedDTO product : filteredProductList) {
	    double fixedOriginRent = packageRent - Math.abs(permanentDiscount)
		    + rentMonoProdLine * TotalUtil.getNumericValueOf(product.getLinePresent())
		    + rentMonoProdInternet * TotalUtil.getNumericValueOf(product.getInternetPresent())
		    + cableRentMonoProd * TotalUtil.getNumericValueOf(product.getCablePresent())
		    //+ fixedParkDto.getCableRentMonoProdPremHd() * TotalUtil.getNumericValueOf(product.getPremiumHdPresent())
		    + getCableRentMonoProdEstellar(fixedParkDto, commercialOpeList)
		    + cableRentMonoProdPremFox * TotalUtil.getNumericValueOf(product.getPremiumFoxPresent())
		    + cableRentMonoProdPremHbo * TotalUtil.getNumericValueOf(product.getPremiumHboPresent());
	    product.setFixedJump(packageRent - fixedOriginRent + getCableRentMonoProdEstellar(fixedParkDto, commercialOpeList) + getCablePresentPremFox(fixedParkDto, commercialOpeList) + getCablePresentPremHbo(fixedParkDto, commercialOpeList));
	}
    }
    
    private Double getCableRentMonoProdEstellar(FixedDTO fixedParkDto, List<CommercialOperation> commercialOpeList) {
	
	Double monto=0.0;
	
	boolean fixedProvideOrPortability = UtilCollections.verifyProvideOrPortability(commercialOpeList,
		SuscriptionType.FIXED);
	
	String channel = "";
	
	if(!fixedProvideOrPortability) {
	    if ((fixedParkDto.getCablePresentEstellar() != null && fixedParkDto.getCablePresentEstellar().toString().equals("1"))
			|| (fixedParkDto.getCablePremiumChannels() != null
				&& StringUtils.contains(fixedParkDto.getCablePremiumChannels(), Constant.PRODUCT_ESTELLAR))
			|| "Estelar".equals(fixedParkDto.getCableType())) {
			channel=Constant.PRODUCT_ESTELLAR;
	    }
	    
	    if (Constant.PRODUCT_ESTELLAR.equals(channel) && fixedParkDto.getCableRentMonoProdEstellar() == 0) {
			monto = 20.0;
	    }
	}
	
	return monto;
	
    }
    
    private Double getCablePresentPremFox(FixedDTO fixedParkDto, List<CommercialOperation> commercialOpeList) {
	
	Double monto=0.0;
	
	boolean fixedProvideOrPortability = UtilCollections.verifyProvideOrPortability(commercialOpeList,
		SuscriptionType.FIXED);
	
	String channel = "";
	
	if(!fixedProvideOrPortability) {	    
	    
	    if ((fixedParkDto.getCablePresentPremFox() != null && fixedParkDto.getCablePresentPremFox().toString().equals("1"))
			|| (fixedParkDto.getCablePremiumChannels() != null
				&& (StringUtils.contains(fixedParkDto.getCablePremiumChannels(), Constant.PRODUCT_FIXED_FOX)
					|| StringUtils.contains(fixedParkDto.getCablePremiumChannels(), Constant.PRODUCT_MOVIECITY)))) {
			channel=Constant.PRODUCT_FIXED_FOX;
	    }
	    
	    if (Constant.PRODUCT_FIXED_FOX.equals(channel) && fixedParkDto.getCableRentMonoProdPremFox() == 0) {
			monto = 35.0;
	    }
	}
	
	return monto;
	
    }

    private Double getCablePresentPremHbo(FixedDTO fixedParkDto, List<CommercialOperation> commercialOpeList) {
	
	Double monto=0.0;
	
	boolean fixedProvideOrPortability = UtilCollections.verifyProvideOrPortability(commercialOpeList,
		SuscriptionType.FIXED);
	
	String channel = "";
	
	if(!fixedProvideOrPortability) {
	    if ((fixedParkDto.getCablePresentPremHbo() != null && fixedParkDto.getCablePresentPremHbo().toString().equals("1"))
			|| (fixedParkDto.getCablePremiumChannels() != null
				&& StringUtils.contains(fixedParkDto.getCablePremiumChannels(), Constant.PRODUCT_FIXED_HBO))) {
			channel=Constant.PRODUCT_FIXED_HBO;
	    }	    
	    
	    if (Constant.PRODUCT_FIXED_HBO.equals(channel) && fixedParkDto.getCableRentMonoProdPremHbo() == 0) {
			monto = 35.0;
	    }
	}
	
	return monto;
	
    }
    
    private void employeeJumpCalculation(FixedDTO fixedParkDto, List<MobileDTO> mobileDevices, List<ProductFixedDTO> filteredProductList) {
	double mobileRent = 0;
	double discountMobileRent = 0;
	for (MobileDTO mobile : mobileDevices) {
	    mobileRent += mobile.getMobileRentMonoProduct() - Math.abs(mobile.getPermanentDisccount());
	    discountMobileRent += TotalUtil.getNumericValueOf(mobile.getPromotionalDiscount()) * 0.3 * mobile.getMobileRentMonoProduct();
	}
	/* CÃ¡lculo del salto */
	for (ProductFixedDTO product : filteredProductList) {
	    double originRent = fixedParkDto.getPackageRent() * TotalUtil.getNumericValueOf(product.getIsFixedProduct())
		    - Math.abs(fixedParkDto.getPermanentDiscount()) * TotalUtil.getNumericValueOf(product.getIsFixedProduct())
		    + fixedParkDto.getRentMonoProdLine() * TotalUtil.getNumericValueOf(product.getLinePresent())
		    + fixedParkDto.getRentMonoProdInternet() * TotalUtil.getNumericValueOf(product.getInternetPresent())
		    + fixedParkDto.getCableRentMonoProd() * TotalUtil.getNumericValueOf(product.getCablePresent())
		    + fixedParkDto.getCableRentMonoProdPremHd() * TotalUtil.getNumericValueOf(product.getPremiumHdPresent())
		    + fixedParkDto.getCableRentMonoProdPremFox() * TotalUtil.getNumericValueOf(product.getPremiumFoxPresent())
		    + fixedParkDto.getCableRentMonoProdPremHbo() * TotalUtil.getNumericValueOf(product.getPremiumHboPresent())
		    + (TotalUtil.getNumericValueOf(rulesValue.getTYPE_PACKAGE_4PLAY().equals(product.getPackageType()))) * mobileRent;

	    //if (product.getPackageRent() >= 269) {
	    if (product.getPackageRent() >= Double.valueOf(getRent().getValor())) {
		product.setEmployeeDiscount(Double.valueOf(getDescuentoEmpleado().getValor()));
		//System.out.println("JUMMMMMMMP "+(product.getPackageRent() +  "-" + product.getEmployeeDiscount()+  ") - (" + originRent +  "- ("+ fixedParkDto.getPresentEmployeeDiscount() + "*" + fixedParkDto.getPresentEmployeeDiscountAmmount())+ ") - " + discountMobileRent );
		product.setEmployeeJump((product.getPackageRent() - product.getEmployeeDiscount())
			- (originRent - ((fixedParkDto.getPresentEmployeeDiscount()==null? 0 : fixedParkDto.getPresentEmployeeDiscount()) * (fixedParkDto.getPresentEmployeeDiscountAmmount()==null? 0 : fixedParkDto.getPresentEmployeeDiscountAmmount())))
			- discountMobileRent);
		product.setEmployeePackageRent(product.getPackageRent() - product.getEmployeeDiscount());
	    } else {
		product.setEmployeeJump(product.getPackageRent()
			- (originRent - (fixedParkDto.getPresentEmployeeDiscount() * fixedParkDto.getPresentEmployeeDiscountAmmount()))
			- discountMobileRent);
		product.setEmployeeDiscount(null);
		product.setEmployeePackageRent(null);
	    }
	}
    }
    
    /* Metodos de ArcheType */
    private ParamMovTotal getRent() {
	List<ParamMovTotal> listParamMovTotal = paramMovTotalRepo.findAll();
	ParamMovTotal pmt = new ParamMovTotal();
	for (ParamMovTotal paramMovTotal : listParamMovTotal) {
	    
	    if(paramMovTotal.getGrupoParam().equals("RENTA")) {
		
		pmt = paramMovTotal;
		
		break;
		
	    }
	    
	}
	
	return pmt;
    }
    
    /* Metodos de ArcheType */
    private ParamMovTotal getDescuentoEmpleado() {
	List<ParamMovTotal> listParamMovTotal = paramMovTotalRepo.findAll();
	ParamMovTotal pmt = new ParamMovTotal();
	for (ParamMovTotal paramMovTotal : listParamMovTotal) {
	    
	    if(!(paramMovTotal.getCodValor() ==null)) {
	    
        	    if(paramMovTotal.getCodValor().equals("desc_empl")) {
        		
        		pmt = paramMovTotal;
        		
        		break;
        		
        	    }
	    
	    }
	    
	}
	
	return pmt;
    }
}
