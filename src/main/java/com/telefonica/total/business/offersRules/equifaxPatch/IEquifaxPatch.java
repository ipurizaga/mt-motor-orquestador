package com.telefonica.total.business.offersRules.equifaxPatch;

import java.util.List;

import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.pojo.req.CommercialOperationInfo;

public interface IEquifaxPatch {

    /***
     * Método que se encarga de actualizar el campo de limite de credito de las
     * operaciones comerciales si son CAEQ con renta del request 0
     * 
     * @param mobileDevices
     * @param commercialInfo
     */
    void execute(List<MobileDTO> mobileDevices, CommercialOperationInfo commercialInfo);
}
