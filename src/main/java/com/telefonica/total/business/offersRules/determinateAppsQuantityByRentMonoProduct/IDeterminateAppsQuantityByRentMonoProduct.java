package com.telefonica.total.business.offersRules.determinateAppsQuantityByRentMonoProduct;

import java.util.List;

import com.telefonica.total.dto.MobileDTO;

public interface IDeterminateAppsQuantityByRentMonoProduct {

    /***
     * Método que se encarga de determinar la cantidad de apps de cada movil por su
     * renta
     * 
     * @param mobileDevices
     */
    void execute(List<MobileDTO> mobileDevices);
}
