package com.telefonica.total.business.offersRules.equipmentValidations;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.RulesValueProp;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.model.CatalogDeco;
import com.telefonica.total.model.CatalogModem;
import com.telefonica.total.model.InternetTechnology;
import com.telefonica.total.model.ParamMovTotal;
import com.telefonica.total.pojo.req.CommercialOperationInfo;
import com.telefonica.total.repository.CatalogInternetTechnology;
import com.telefonica.total.repository.ParamMovTotalRepo;



@Service
@Qualifier("totalization")
public class EquipmentValidations implements IEquipmentValidations{

    @Autowired
    private RulesValueProp rulesValue;
    
    @Autowired
    private CatalogInternetTechnology technology;	
    
    @Autowired
    private ParamMovTotalRepo pmtr;
	
	private ParamMovTotal pmt;
    
	@Override
	public void execute(List<CatalogDeco> catalogDecoList, List<CatalogModem> catalogModemList, List<ProductFixedDTO> priorizationList,
		    FixedDTO fixedParkDto, CommercialOperationInfo commercialInfo, boolean provideOrPortability) {
	
	List<ParamMovTotal> listParamMovTotal = pmtr.findAll();
	
	boolean existeUW = activateUltraWifi(listParamMovTotal);
	
	String pp = TotalUtil.getValuePlayas(commercialInfo);	
	
	for (ProductFixedDTO product : priorizationList) {
	    if (!provideOrPortability) {
		flagDeco(catalogDecoList, product, fixedParkDto);
		determinateInternetTecnology(product, fixedParkDto, commercialInfo);
	    } else {
		determinateInternetTecnology(product, fixedParkDto, commercialInfo);
		decoEquipmentValidations(catalogDecoList, product, fixedParkDto);
	    }
	    flagModem(catalogModemList, product, fixedParkDto, existeUW, listParamMovTotal, pp, commercialInfo);
	    //private void flagModem(List<CatalogModem> catalogModemList, ProductFixedDTO product, FixedDTO fixedParkDto, boolean existeUW, List<ParamMovTotal> listParamMovTotal, String pp, CommercialOperationInfo commercialInfo) {
	}
    }
    
    /***
     * Metodo que se encarga de verificar si el producto cuenta con el flag regla
     * deco
     * 
     * @param product
     * @param fixedParkDto
     */
    private void flagDeco(List<CatalogDeco> catalogDecoList, ProductFixedDTO product, FixedDTO fixedParkDto) {
	if (product.getFlagRuleDeco().equals(rulesValue.getFLAG_RULE_DECO_ONE())) {
	    moreThan6Decos(catalogDecoList, product, fixedParkDto);
	}
    }
    
    /***
     * Método que se encarga de indicar con que tecnologia se le venderá al cliente.
     */
    private void determinateInternetTecnology(ProductFixedDTO product, FixedDTO fixedParkDto, CommercialOperationInfo commercialInfo) {

	List<InternetTechnology> matrix = technology.findAll();
	List<String> matrixTechnology = new ArrayList<>();
	for(InternetTechnology m : matrix) {
		if(m.getInternetTechnology()!=null)
		matrixTechnology.add(m.getInternetTechnology());
	}
	String realTechnology = Constant.CERO_INT.toString().equals(fixedParkDto.getTechnologyInternet()) ? null
		: fixedParkDto.getTechnologyInternet();
	String realCoverage = obtainRealCoverage(fixedParkDto, commercialInfo, realTechnology);
	
	Character hfcCobInternet = fixedParkDto.getHfcCobInternet() != null? fixedParkDto.getHfcCobInternet() : '0';
	
	if(realTechnology != null) {
		if(realTechnology.equals("HFC")) {
			if(hfcCobInternet.toString().equals("2")) {
				realCoverage = "FTTH";
			}
		}
	}

	
	//for (InternetTechnology intenerTechnology : matrix) {
	//    String matrixActualTechnology = intenerTechnology.getInternetTechnology().equals("NULL") ? null
	//	    : intenerTechnology.getInternetTechnology();
	//    String matrixCoverage = intenerTechnology.getCoverage().equals("NULL") ? null : intenerTechnology.getCoverage();
	    determinateInternetDestinyTechnology(matrixTechnology, realTechnology, realCoverage, product);
	//	    intenerTechnology);
	}
    
    
    private String obtainRealCoverage(FixedDTO fixedParkDto, CommercialOperationInfo commercialInfo, String realTechnology) {
	if (rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(realTechnology)
		&& rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(commercialInfo.getCoverage().toLowerCase().equals("fiber")?"FTTH":commercialInfo.getCoverage())) {
	    return Constant.ONE.equals(fixedParkDto.getHfcCobInternet()) ? rulesValue.getINTERNET_TECNOLOGIA_HFC()
		    : rulesValue.getINTERNET_TECNOLOGIA_FTTH();
	} else {
	    return commercialInfo.getCoverage().toLowerCase().equals("fiber")?"FTTH":commercialInfo.getCoverage();
	}
    }
    
    /***
     * Método que se encarga de verificar si la cantidad de decodificadores del
     * cliente es mayor que 6.
     * 
     * @param product
     * @param fixedParkDto
     */
    private void moreThan6Decos(List<CatalogDeco> catalogDecoList, ProductFixedDTO product, FixedDTO fixedParkDto) {
	int decoQuantity = TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantitydigitalDeco())
		+ TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoHd())
		+ TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart())
		+ TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoDvr());
	if (!(decoQuantity > rulesValue.getDECO_QUANTITY_SIX()
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) > 0)) {
	    decoEquipmentValidations(catalogDecoList, product, fixedParkDto);
	}
    }
    
    /***
     * Método que se encarga de modificar la TECNOLOGIA DE INTERNET de destino por criterios
     * Criterio Actual: Si Tecnología actual es HFC y Tecnología destino es FTTH
     * UMBRAL Actual  : 100 MB 
     * @param matrixActualTechnology ---> Todas las tecnologías actuales
     * @param realTechnology		 ---> Tecnologia Actual
     * @param matrixCoverage		 ---> Todas las coberturas
     * @param realCoverage		 	 ---> Cobertura Actual
     * @param intenerTechnology		 ---> Iterador de Tecnologias de Internet
     */
    //private void determinateInternetDestinyTechnology(String matrixActualTechnology, String realTechnology, String matrixCoverage,
	//    String realCoverage, ProductFixedDTO product, InternetTechnology intenerTechnology) {
    private void determinateInternetDestinyTechnology(List<String> matrixTechnology, String realTechnology,
    	String realCoverage, ProductFixedDTO product) {
    	pmt = pmtr.findByCode(Constant.UMBTECNO);
    	if(realTechnology!=null) {
    		if (matrixTechnology.contains(realTechnology) && matrixTechnology.contains(realCoverage)) {
    		    if (rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(realTechnology)
    			    && rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(realCoverage)
    			    && TotalUtil.getNumericValueOf(product.getInternetSpeed()) >= Integer.parseInt(pmt.getValor())) {
    			product.setInternetDestinyTechnology(rulesValue.getINTERNET_TECNOLOGIA_FTTH());
    		    } else if (rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(realTechnology)
    			    && rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(realCoverage)
    			    && TotalUtil.getNumericValueOf(product.getInternetSpeed()) < Integer.parseInt(pmt.getValor())) {
    			product.setInternetDestinyTechnology(rulesValue.getINTERNET_TECNOLOGIA_HFC());
    		    } else {
    		    	if(!realTechnology.equals(rulesValue.getINTERNET_TECNOLOGIA_FTTH())) {
    		    		product.setInternetDestinyTechnology(realCoverage);
    		    	}
    		    	else
    		    		product.setInternetDestinyTechnology(realTechnology);
    		    }
    		}
    	} else {
        	if(matrixTechnology.contains(realCoverage)) {
    		    if (rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(realTechnology)
        			    && rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(realCoverage)
        			    && TotalUtil.getNumericValueOf(product.getInternetSpeed()) >= Integer.parseInt(pmt.getValor())) {
        			product.setInternetDestinyTechnology(rulesValue.getINTERNET_TECNOLOGIA_FTTH());
        		} else if (rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(realTechnology)
        			    && rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(realCoverage)
        			    && TotalUtil.getNumericValueOf(product.getInternetSpeed()) < Integer.parseInt(pmt.getValor())) {
        			product.setInternetDestinyTechnology(rulesValue.getINTERNET_TECNOLOGIA_HFC());
        		} else {
        			product.setInternetDestinyTechnology(realCoverage);
        		}
        	}
    	}


    }
    
    private boolean compare(String str1, String str2) {
	return (str1 == null ? str2 == null : str1.equals(str2));
    }
    
    /***
     * Método que se encarga de definir cuales seran los decodificadores asignados
     * segun las reglas de negocio y las rentas.
     * 
     * @param product
     * @param fixedParkDto
     */
    /*private void decoEquipmentValidations(ProductFixedDTO product, FixedDTO fixedParkDto) {
	// Sin deco(altas nuevas) /
	determinateEquipmentValidationWithOutDeco(product, fixedParkDto);
	// Solo tiene decos digitales /
	determinateEquipmentValidationWithOnlyDigitalDecos(product, fixedParkDto);
	// Solo tiene 1 Deco HD o Smart /
	determinateEquipmentValidationWithHdDigitalOrWithSmart(product, fixedParkDto);
	// Si tiene Decos HD y/o Smart /
	determinateEquipmentValidationWithHDAndOrWithSmart(product, fixedParkDto);
	
    }*/
    
   
   private void decoEquipmentValidations(List<CatalogDeco> catalogDecoList, ProductFixedDTO product, FixedDTO fixedParkDto) {
       for (CatalogDeco catalogDeco : catalogDecoList) {
	    
	    if (catalogDeco.getRulerActive().equals("1")) {
	
		determinateEquipmentValidationDeco(catalogDeco, product, fixedParkDto);
	    
	    }
	    
       	/* Sin deco(altas nuevas) */
       	//determinateEquipmentValidationWithOutDeco(catalogDeco, product, fixedParkDto);
       	/* Solo tiene decos digitales */
       	//determinateEquipmentValidationWithOnlyDigitalDecos(catalogDeco, product, fixedParkDto);
       	/*Solo tiene 1 Deco HD o Smart*/
       	//determinateEquipmentValidationWithHdDigitalOrWithSmart(catalogDeco, product, fixedParkDto);
       	/*Si tiene Decos HD y/o Smart*/
       	//determinateEquipmentValidationWithHDAndOrWithSmart(catalogDeco, product, fixedParkDto);
	
	}
	
   }
   
   private void determinateEquipmentValidationDeco(CatalogDeco catalogDeco, ProductFixedDTO product, FixedDTO fixedParkDto) {
	
	int flagDDF = 0, flagHDF = 0, flagSDF = 0, total = 0;
	
	if(catalogDeco.getDigitalDecoFlag().equals("1")) {
	    
	    flagDDF = fixedParkDto.getCableQuantitydigitalDeco();
	    
	}
	
	if(catalogDeco.getHdDecoFlag().equals("1")) {
	    
	    flagHDF = fixedParkDto.getCableQuantityDecoHd();
	    
	}
	
	if(catalogDeco.getSmartwifiDecoFlag().equals("1")) {
	    
	    flagSDF = fixedParkDto.getCableQuantityDecoSmart();
	    
	}
	
	total = flagDDF + flagHDF + flagSDF;
	
	if(catalogDeco.getOfferDecoConditional().equals("=")) {
	    //if(total == TotalUtil.getNumericValueOf(catalogDeco.getOfferDecoQuantityConditional())) {
	    if(Integer.valueOf(total) == Integer.valueOf(catalogDeco.getOfferDecoQuantityConditional())) {
		
		if(catalogDeco.getRentVaild().equals("0")) {
		    
       		product.setEquipmentDecoQuantity(TotalUtil.getNumericValueOf(catalogDeco.getOfferDecoQuantity()));
       		product.setDecoEquipment(catalogDeco.getOfferDeco());	
		
		}else {
		    
		    	if(catalogDeco.getConditionalRent().equals("<=")) {
		    	    //if(product.getPackageRent() <= TotalUtil.getNumericValueOf(catalogDeco.getRent())) {
		    	    if(product.getPackageRent() <= Double.valueOf(catalogDeco.getRent())) {
		    		product.setEquipmentDecoQuantity(TotalUtil.getNumericValueOf(catalogDeco.getOfferDecoQuantity()));
	        		product.setDecoEquipment(catalogDeco.getOfferDeco());	
		    	    }
		    	}else {
		    	    if(product.getPackageRent() > Double.valueOf(catalogDeco.getRent())) {
		    		product.setEquipmentDecoQuantity(TotalUtil.getNumericValueOf(catalogDeco.getOfferDecoQuantity()));
	        		product.setDecoEquipment(catalogDeco.getOfferDeco());	
		    	    }
		    	}
		}
	    }
	}
	
	if(catalogDeco.getOfferDecoConditional().equals(">")) {
	    if(Integer.valueOf(total) > Integer.valueOf(catalogDeco.getOfferDecoQuantityConditional())) {
       	    if(catalogDeco.getRentVaild().equals("0")) {
               	    	
                       		product.setEquipmentDecoQuantity(TotalUtil.getNumericValueOf(catalogDeco.getOfferDecoQuantity()));
                       		product.setDecoEquipment(catalogDeco.getOfferDeco());
               	    		    
       	    }else {
           			if(catalogDeco.getConditionalRent().equals("<=")) {
           			    
               	    	    if(product.getPackageRent() <= Double.valueOf(catalogDeco.getRent())) {
               	    		product.setEquipmentDecoQuantity(TotalUtil.getNumericValueOf(catalogDeco.getOfferDecoQuantity()));
                       		product.setDecoEquipment(catalogDeco.getOfferDeco());	
               	    	    }
               	    	}else {
               	    	    if(product.getPackageRent() > Double.valueOf(catalogDeco.getRent())) {
               	    		product.setEquipmentDecoQuantity(TotalUtil.getNumericValueOf(catalogDeco.getOfferDecoQuantity()));
                       		product.setDecoEquipment(catalogDeco.getOfferDeco());	
               	    	    }
               	    	}
       	    }
       	    
	    }
	    
	}
	
	if(catalogDeco.getOfferDecoConditional().equals(">=")) {
	    if(Integer.valueOf(total) >= Integer.valueOf(catalogDeco.getOfferDecoQuantityConditional())) {
       	    if(catalogDeco.getRentVaild().equals("0")) {
       	    
               	    
               		product.setEquipmentDecoQuantity(TotalUtil.getNumericValueOf(catalogDeco.getOfferDecoQuantity()));
               		product.setDecoEquipment(catalogDeco.getOfferDeco());
               	    
       	    
       	    }
	    }	
	}/*else {
		if(catalogDeco.getConditionalRent().equals("<=")) {
	    	    if(product.getPackageRent() <= Double.valueOf(catalogDeco.getRent())) {
	    		product.setEquipmentDecoQuantity(TotalUtil.getNumericValueOf(catalogDeco.getOfferDecoQuantity()));
       		product.setDecoEquipment(catalogDeco.getOfferDeco());	
	    	    }
	    	}else {
	    	    if(!(total == TotalUtil.getNumericValueOf(catalogDeco.getOfferDecoQuantityConditional()))) {
       	    	    if(product.getPackageRent() > Double.valueOf(catalogDeco.getRent())) {
       	    		product.setEquipmentDecoQuantity(TotalUtil.getNumericValueOf(catalogDeco.getOfferDecoQuantity()));
               		product.setDecoEquipment(catalogDeco.getOfferDeco());	
       	    	    }
	    	    }
	    	}
	}*/
	
   }
    
    private void determinateEquipmentValidationWithOutDeco(ProductFixedDTO product, FixedDTO fixedParkDto) {
	//if ((TotalUtil.getNumericValueOf(fixedParkDto.getCablePresent()) == 0) || fixedParkDto.getCableQuantitydigitalDeco() == 0 || fixedParkDto.getCableQuantityDecoHd() == 0 || fixedParkDto.getCableQuantityDecoSmart() == 0) {
	if ((TotalUtil.getNumericValueOf(fixedParkDto.getCablePresent()) == 0) || (fixedParkDto.getCableQuantitydigitalDeco() == 0 && fixedParkDto.getCableQuantityDecoHd() == 0 && fixedParkDto.getCableQuantityDecoSmart() == 0)) {
			product.setEquipmentDecoQuantity(2);
		product.setDecoEquipment(rulesValue.getDECO_EQUIPMENT_2HD());
	}
    }

    private void determinateEquipmentValidationWithOnlyDigitalDecos(ProductFixedDTO product, FixedDTO fixedParkDto) {
	if (TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantitydigitalDeco()) > 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoHd()) == 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) == 0/* && product.getPackageRent() <= 299*/) {
	    
	    product.setEquipmentDecoQuantity(1);
	    product.setDecoEquipment(rulesValue.getDECO_EQUIPMENT_1HD());
	}
    }
    
    private void determinateEquipmentValidationWithHdDigitalOrWithSmart(ProductFixedDTO product, FixedDTO fixedParkDto) {
	//if (TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoHd()) == 1
	//	|| TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) == 1) {
	if (TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoHd()) 
		+ TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) == 1) {
	    //product.setDecoEquipment(rulesValue.getDECO_EQUIPMENT_1HD());
	}
    }
    
    private void determinateEquipmentValidationWithHDAndOrWithSmart(ProductFixedDTO product, FixedDTO fixedParkDto) {
	
	int cantidad = TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoHd()) + TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart());
	
	if (cantidad >= 2) {
	    product.setEquipmentDecoQuantity(0);
	}
	
    }

    private void determinateEquipmentValidationWithHdDigitalAndWithOutSmart(ProductFixedDTO product, FixedDTO fixedParkDto) {
	if (TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantitydigitalDeco()) > 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoHd()) > 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) == 0 && product.getPackageRent() <= 299) {
	    product.setEquipmentDecoQuantity(0);
	}
	if (TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantitydigitalDeco()) > 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoHd()) > 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) == 0 && product.getPackageRent() > 299) {
	    product.setEquipmentDecoQuantity(1);
	    product.setDecoEquipment(rulesValue.getDECO_EQUIPMENT_SMART());
	}
    }

    private void determinateEquipmentValidationWithOnlyHd(ProductFixedDTO product, FixedDTO fixedParkDto) {
	if (TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantitydigitalDeco()) == 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoHd()) > 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) == 0 && product.getPackageRent() <= 299) {
	    product.setEquipmentDecoQuantity(0);
	}
	if (TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantitydigitalDeco()) == 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoHd()) > 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) == 0 && product.getPackageRent() > 299) {
	    product.setEquipmentDecoQuantity(1);
	    product.setDecoEquipment(rulesValue.getDECO_EQUIPMENT_SMART());
	}
    }

    private void determinateEquipmentWithHdAndSmartWithOutDigital(ProductFixedDTO product, FixedDTO fixedParkDto) {
	if (TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantitydigitalDeco()) == 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoHd()) > 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) > 0 && product.getPackageRent() <= 299) {
	    product.setEquipmentDecoQuantity(0);
	}
	if (TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantitydigitalDeco()) == 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoHd()) > 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) > 0 && product.getPackageRent() > 299) {
	    product.setEquipmentDecoQuantity(1);
	    product.setDecoEquipment(rulesValue.getDECO_EQUIPMENT_SMART());
	}
    }

    private void determinateEquipmentWithOnlySmart(ProductFixedDTO product, FixedDTO fixedParkDto) {
	if (TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantitydigitalDeco()) == 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoHd()) == 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) > 0 && product.getPackageRent() <= 299) {
	    product.setEquipmentDecoQuantity(0);
	}
	if (TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantitydigitalDeco()) == 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoHd()) == 0
		&& TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) > 0 && product.getPackageRent() > 299) {
	    product.setEquipmentDecoQuantity(1);
	    product.setDecoEquipment(rulesValue.getDECO_EQUIPMENT_SMART());
	}
    }
    
    /***
     * Método que se encarga de verificar si el producto cuenta con el flag regla
     * modem
     * 
     * @param product
     * @param fixedParkDto
     */
    private void flagModem(List<CatalogModem> catalogModemList, ProductFixedDTO product, FixedDTO fixedParkDto, boolean existeUW, List<ParamMovTotal> listParamMovTotal, String pp, CommercialOperationInfo commercialInfo) {
	if (product.getFlagRuleModem().equals(rulesValue.getFLAG_RULE_MODEM_ONE())) {
	    modemEquipmentValidations(catalogModemList,product, fixedParkDto, commercialInfo, existeUW, listParamMovTotal, pp);
	}
    }
    
private void modemEquipmentValidations(List<CatalogModem> catalogModemList, ProductFixedDTO product, FixedDTO fixedParkDto, CommercialOperationInfo comercialInfo, boolean existeUW, List<ParamMovTotal> listParamMovTotal, String pp) {
	
	for (CatalogModem catalogModem : catalogModemList) {
	    
	    if (catalogModem.getActiveFlag().equals("1")) {
	    
		/* Sin modem(Alta nueva) */
        	//if (fixedParkDto.getPresentInternet().toString().equals("0")) {
        	
		if (!(catalogModem.getOriginTechnology().equals("0"))) {
		    if(determinateRules(product, catalogModem, fixedParkDto, comercialInfo)){
			break;
		    }
		}else {
		    
		    if(determinatePresentInternet(product, catalogModem, fixedParkDto, comercialInfo)) {
			break;
		    }
		    
		}
		
		
        	//}	
        	/* Migracion de ADSL -> HFC /
        	else if (rulesValue.getINTERNET_TECNOLOGIA_ADSL().equals(fixedParkDto.getTechnologyInternet())
        		&& rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(product.getInternetDestinyTechnology())) {
        	    determinateInternetTecnologyAdsl(product, fixedParkDto);
        	}   	
        	
        	/* MigraciÃƒÂ³n HFC -> HFC /
        	else if (rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(fixedParkDto.getTechnologyInternet())
        		&& rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(product.getInternetDestinyTechnology())) {
        	    determinateInternetTecnologyHfc(product, fixedParkDto);
        	}
        	
        	/* MigraciÃƒÂ³n ADSL -> FTTH /
        	else if (rulesValue.getINTERNET_TECNOLOGIA_ADSL().equals(fixedParkDto.getTechnologyInternet())
        		&& rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(product.getInternetDestinyTechnology())) {
        	    determinateInternetTecnologyAdslAndFtth(product, fixedParkDto);
        	}
        	/* MigraciÃƒÂ³n FTTH -> FTTH /
        	else if (rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(fixedParkDto.getTechnologyInternet())
        		&& rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(product.getInternetDestinyTechnology())) {
        	    determinateInternetTecnologyFtth(product, fixedParkDto);
        	}*/
        	
	    }
        	
	}
	
	if(existeUW) {
	    	
    	    determinateUltraWifiRep(product, pp);
    	
    	}
	
    }

private boolean determinateRules(ProductFixedDTO product, CatalogModem catalogModem, FixedDTO fixedParkDto, CommercialOperationInfo comercialInfo) {
	
	boolean resp = false;
	
	/*if(rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(product.getInternetDestinyTechnology())) {
	   
	    	product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_GPON());       
	}else {
	    	
	product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_SMART_WIFI());
	}*/

	if(//comercialInfo.getCoverage().equals(product.getInternetDestinyTechnology()) 
		//&& 
		product.getInternetDestinyTechnology().equals(catalogModem.getDestinyTechnology())
		&& catalogModem.getOriginTechnology().equals(fixedParkDto.getTechnologyInternet())
		&& ((fixedParkDto.getModenNameInternet()!=null) && fixedParkDto.getModenNameInternet().equals(catalogModem.getTenancyModemInternet()))) {
		
	    if(catalogModem.getRentVaild().equals("1")) {
	     
		//if(catalogModem.getOfferConditionalRent().equals(">")) {
		
		if(catalogModem.getOfferConditionalRent().equals("<")) {
		//if(product.getPackageRent() > (Double.parseDouble(catalogModem.getOfferRent()))) {
		if(product.getPackageRent() < (Double.parseDouble(catalogModem.getOfferRent()))) {
	    		product.setModemEquipment(catalogModem.getOfferModemInternet());
	    		
	    		resp = true;
	    	
		}
		
		}else {
		    
		    //if(product.getPackageRent() <= (Double.parseDouble(catalogModem.getOfferRent()))) {
		    if(product.getPackageRent() >= (Double.parseDouble(catalogModem.getOfferRent()))) {
    		
	    		product.setModemEquipment(catalogModem.getOfferModemInternet());
	    		
	    		resp = true;
	    	
		    }
		
	    	}
	    	
	    }else {
		product.setModemEquipment(catalogModem.getOfferModemInternet());
		
		resp = true;
	    }
	    	
	}/*else {
	    	
		product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_SMART_WIFI());
	}*/
	
	return resp;

}

private boolean determinatePresentInternet(ProductFixedDTO product, CatalogModem catalogModem, FixedDTO fixedParkDto, CommercialOperationInfo comercialInfo) {
	boolean resp = false;
		/*if(rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(product.getInternetDestinyTechnology())) {
		   
		    	product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_GPON());       
		}else {
	    	    	
	        	product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_SMART_WIFI());
		}*/
	
    	if(//comercialInfo.getCoverage().equals(product.getInternetDestinyTechnology()) 
    		//&& catalogModem.getDestinyTechnology().equals(comercialInfo.getCoverage())
    		//&& 
    		catalogModem.getDestinyTechnology().equals(product.getInternetDestinyTechnology())
    		&& fixedParkDto.getPresentInternet().equals('0')) {
    	    
    	    //product.setModemEquipment(catalogModem.getOfferModemInternet());
    		
    	    if(catalogModem.getRentVaild().equals("1")) {
    	     
    		//if(catalogModem.getOfferConditionalRent().equals(">")) {
    		if(catalogModem.getOfferConditionalRent().equals("<")) {
            		//if(product.getPackageRent() > (Double.parseDouble(catalogModem.getOfferRent()))) {
    		    	if(product.getPackageRent() < (Double.parseDouble(catalogModem.getOfferRent()))) {
            	    		product.setModemEquipment(catalogModem.getOfferModemInternet());
            	    		
            	    		resp = true;
            	    	
            		}
    		
    		}else {
    		    
        		    //if(product.getPackageRent() <= (Double.parseDouble(catalogModem.getOfferRent()))) {
    		    if(product.getPackageRent() >= (Double.parseDouble(catalogModem.getOfferRent()))) {	
        	    		product.setModemEquipment(catalogModem.getOfferModemInternet());
        	    		
        	    			resp = true;
        	    	
        		    }
        		
        	    	}
    	    	
    	    }else {
    		product.setModemEquipment(catalogModem.getOfferModemInternet());
	    		
	    			resp = true;
    	    }
    	    	
    	}/*else {
    	    	
        		product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_SMART_WIFI());
    	}*/
    	
    	return resp;
	
	}
    
    /*private void modemEquipmentValidations(ProductFixedDTO product, FixedDTO fixedParkDto, boolean existeUW, List<ParamMovTotal> listParamMovTotal, String pp) {
	// Sin modem(Alta nueva) /
    	if (fixedParkDto.getPresentInternet().toString().equals("0")) {
    	    
    	    determinatePresentInternet(product/,catalogModem/, listParamMovTotal);
    	}
    	// Migracion de ADSL -> HFC /
    	else if (rulesValue.getINTERNET_TECNOLOGIA_ADSL().equals(fixedParkDto.getTechnologyInternet())
    		&& rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(product.getInternetDestinyTechnology())) {
    	    determinateInternetTecnologyAdsl(product, fixedParkDto, listParamMovTotal);
    	}   	
    	
    	// Migración HFC -> HFC /
    	else if (rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(fixedParkDto.getTechnologyInternet())
    		&& rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(product.getInternetDestinyTechnology())) {
    	    determinateInternetTecnologyHfc(product, fixedParkDto, listParamMovTotal);
    	}
    	
    	// Migración ADSL -> FTTH /
    	else if (rulesValue.getINTERNET_TECNOLOGIA_ADSL().equals(fixedParkDto.getTechnologyInternet())
    		&& rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(product.getInternetDestinyTechnology())) {
    	    determinateInternetTecnologyAdslAndFtth(product, fixedParkDto, listParamMovTotal);
    	}
    	// Migración FTTH -> FTTH /
    	else if (rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(fixedParkDto.getTechnologyInternet())
    		&& rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(product.getInternetDestinyTechnology())) {
    	    determinateInternetTecnologyFtth(product, fixedParkDto, listParamMovTotal);
    	}
    	// Mi/
    	else if (rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(fixedParkDto.getTechnologyInternet())
    		&& rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(product.getInternetDestinyTechnology())) {
    	    //determinateInternetTecnologyFtth(product, fixedParkDto);
    	    determinateInternetTecnologyFtthHfc(product, fixedParkDto, listParamMovTotal);
    	}
    	
    	if(existeUW) {
    	
    	    determinateUltraWifiRep(product, pp);
    	
    	}
	
    }*/

    private void determinatePresentInternet(ProductFixedDTO product/*, CatalogModem catalogModem*/, List<ParamMovTotal> listParamMovTotal) {
	
		if(rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(product.getInternetDestinyTechnology())) {
		   
		    //product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_GPON());    
		    
		    for (ParamMovTotal paramMovTotal : listParamMovTotal) {
			    //TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) 
			    if(paramMovTotal.getCodValor().equals(rulesValue.getINTERNET_TECNOLOGIA_FTTH())
				&& rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(product.getInternetDestinyTechnology())
				&& (paramMovTotal.getCol2() != null && product.getPackageRent() <= Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol2()))))
				&& (paramMovTotal.getCol1() != null && product.getPackageRent() > Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol1()))))) {
				product.setModemEquipment(paramMovTotal.getValor());
			    }
			    
		    }
		}else {
	    	    	
		    /*if (product.getPackageRent() < 195) {
		    
			product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_DOCSIS3());
	        	
		    }else {
			
			product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_SMART_WIFI());
			
		    }*/
		    
		    for (ParamMovTotal paramMovTotal : listParamMovTotal) {
			    //TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) 
			    if(paramMovTotal.getCodValor().equals(rulesValue.getINTERNET_TECNOLOGIA_HFC())
				&& rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(product.getInternetDestinyTechnology())
				&& (paramMovTotal.getCol2() != null && product.getPackageRent() <= Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol2()))))
				&& (paramMovTotal.getCol1() != null && product.getPackageRent() > Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol1()))))) {
				product.setModemEquipment(paramMovTotal.getValor());
			    }
			    
		    }
		}
	
    }

    private void determinateInternetTecnologyAdsl(ProductFixedDTO product, FixedDTO fixedParkDto/*, CatalogModem catalogModem*/, List<ParamMovTotal> listParamMovTotal) {
	
	
	if (fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_ADSL()) || fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_DUALBAND())) {
	    /*if (product.getPackageRent() < 195) {
		product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_DOCSIS3());
	    }else {
		product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_SMART_WIFI());
	    }*/
	    
	    for (ParamMovTotal paramMovTotal : listParamMovTotal) {
		    //TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) 
		    if(paramMovTotal.getCodValor().equals(rulesValue.getINTERNET_TECNOLOGIA_HFC())
			&& rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(product.getInternetDestinyTechnology())
			&& (paramMovTotal.getCol2() != null && product.getPackageRent() <= Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol2()))))
			&& (paramMovTotal.getCol1() != null && product.getPackageRent() > Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol1()))))) {
			product.setModemEquipment(paramMovTotal.getValor());
		    }
		    
	    }
	}
	
	
    }

    private void determinateInternetTecnologyHfc(ProductFixedDTO product, FixedDTO fixedParkDto/*, CatalogModem catalogModem*/, List<ParamMovTotal> listParamMovTotal) {
	if (fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_ADSL()) || 
		fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_DOCSIS2())) {
	    
	    
	    determinateModenNameDocsis2(product, fixedParkDto, listParamMovTotal);
	}
	
	
    }    

    private void determinateModemEquipmentDocsis3(ProductFixedDTO product, FixedDTO fixedParkDto/*, CatalogModem catalogModem*/) {
	if (fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_DOCSIS3())) {
	    if (product.getPackageRent() <= 199) {
		//product.setEquipmentUltraWifiRepQuantity(0);
		product.setEquipmentUltraWifiRepQuantity(1);
		return;
	    }
	    if (product.getPackageRent() > 199) {
		//product.setEquipmentUltraWifiRepQuantity(0);
		product.setEquipmentUltraWifiRepQuantity(1);
	    }
	}
	
	
    }

    private void determinateModemEquipmentDualband(ProductFixedDTO product, FixedDTO fixedParkDto/*, CatalogModem catalogModem*/) {
	if (fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_DUALBAND())) {
	    if (product.getPackageRent() <= 199) {
		//product.setEquipmentUltraWifiRepQuantity(0);
		product.setEquipmentUltraWifiRepQuantity(1);
		return;
	    }
	    if (product.getPackageRent() > 199) {
		//product.setEquipmentUltraWifiRepQuantity(0);
		product.setEquipmentUltraWifiRepQuantity(1);
	    }
	}
	
	
    }

    private void determinateInternetTecnologyAdslAndFtth(ProductFixedDTO product, FixedDTO fixedParkDto/*, CatalogModem catalogModem*/, List<ParamMovTotal> listParamMovTotal) {
	if ((fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_ADSL())
			|| fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_DUALBAND()))) {
	    
		//product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_GPON());
	    for (ParamMovTotal paramMovTotal : listParamMovTotal) {
		    //TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) 
		    if(paramMovTotal.getCodValor().equals(rulesValue.getINTERNET_TECNOLOGIA_FTTH())
			&& rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(product.getInternetDestinyTechnology())
			&& (paramMovTotal.getCol2() != null && product.getPackageRent() <= Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol2()))))
			&& (paramMovTotal.getCol1() != null && product.getPackageRent() > Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol1()))))) {
			product.setModemEquipment(paramMovTotal.getValor());
		    }
		    
	    }
	  
	}

    }
    

    private void determinateInternetTecnologyFtth(ProductFixedDTO product, FixedDTO fixedParkDto/*, CatalogModem catalogModem*/, List<ParamMovTotal> listParamMovTotal) {
	
	
	    if (fixedParkDto.getModenNameInternet()!=null) {
        	    if (fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_SMART_WIFI())) {
        		if (product.getPackageRent() <= 199) {
        		    //product.setEquipmentUltraWifiRepQuantity(0);
        		    //product.setEquipmentUltraWifiRepQuantity(1);
        		    return;
        		}
        		if (product.getPackageRent() > 199) {
        		    //product.setEquipmentUltraWifiRepQuantity(0);
        		    //product.setEquipmentUltraWifiRepQuantity(1);
        		}
        	    }
	    
	    }
	
    }
    
    private void determinateInternetTecnologyFtthHfc(ProductFixedDTO product, FixedDTO fixedParkDto/*, CatalogModem catalogModem*/, List<ParamMovTotal> listParamMovTotal) {
	
	    //if (fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_SMART_WIFI())) {
		//if (product.getPackageRent() <= 235) {
		    //product.setEquipmentUltraWifiRepQuantity(0);
		    //product.setEquipmentUltraWifiRepQuantity(1);
		    //product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_GPON());
	
	for (ParamMovTotal paramMovTotal : listParamMovTotal) {
	    //TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) 
	    if(paramMovTotal.getCodValor().equals(rulesValue.getINTERNET_TECNOLOGIA_FTTH())
		&& rulesValue.getINTERNET_TECNOLOGIA_FTTH().equals(product.getInternetDestinyTechnology())
		&& (paramMovTotal.getCol2() != null && product.getPackageRent() <= Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol2()))))
		&& (paramMovTotal.getCol1() != null && product.getPackageRent() > Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol1()))))) {
		product.setModemEquipment(paramMovTotal.getValor());
		break;
	    }
	    
    }
		    return;
		//}
		//if (product.getPackageRent() > 235) {
		    //product.setEquipmentUltraWifiRepQuantity(0);
		    //product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_GPON());
		//}
	    //}
	
    }

    private void determinateInternetTecnologyHfcAndFtth(ProductFixedDTO product, FixedDTO fixedParkDto/*, CatalogModem catalogModem*/) {
	if (fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_ADSL())) {

	    determinateModenNameDocsis22(product, fixedParkDto);

	    determinateModenNameDocsis3(product, fixedParkDto);

	    determinateModenNameDualband(product, fixedParkDto);
	    
	}
	
    }
    
    private void determinateModenNameDocsis2(ProductFixedDTO product, FixedDTO fixedParkDto/*, CatalogModem catalogModem*/, List<ParamMovTotal> listParamMovTotal) {
	
	
	//if (fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_DOCSIS2())) {
	    
	    /*if (product.getPackageRent() < 195) {
		product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_DOCSIS3());
	    }else {
		product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_SMART_WIFI());
	    }*/
	
	for (ParamMovTotal paramMovTotal : listParamMovTotal) {
	    //TotalUtil.getNumericValueOf(fixedParkDto.getCableQuantityDecoSmart()) 
	    if(paramMovTotal.getCodValor().equals(rulesValue.getINTERNET_TECNOLOGIA_HFC())
		&& rulesValue.getINTERNET_TECNOLOGIA_HFC().equals(product.getInternetDestinyTechnology())
		&& (paramMovTotal.getCol2() != null && product.getPackageRent() <= Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol2()))))
		&& (paramMovTotal.getCol1() != null && product.getPackageRent() > Double.valueOf((TotalUtil.getNumericValueOf(paramMovTotal.getCol1()))))) {
		product.setModemEquipment(paramMovTotal.getValor());
		break;
	    }
	    
    }
	   
	//}
	
	
    }
    
    private void determinateModenNameDocsis22(ProductFixedDTO product, FixedDTO fixedParkDto/*, CatalogModem catalogModem*/) {
	
	
	if (fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_DOCSIS2())) {
	    
	    if (product.getPackageRent() <= 199) {
		product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_GPON());
	    }else {
		product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_GPON());
	    }
	   
	}
	
	
    }

    private void determinateModenNameDocsis3(ProductFixedDTO product, FixedDTO fixedParkDto/*, CatalogModem catalogModem*/) {
	if (fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_DOCSIS3())) {
	    if (product.getPackageRent() <= 199) {
		//product.setEquipmentUltraWifiRepQuantity(0);
		product.setEquipmentUltraWifiRepQuantity(1);
		product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_GPON());
	    }
	    if (product.getPackageRent() > 199) {
		//product.setEquipmentUltraWifiRepQuantity(0);
		product.setEquipmentUltraWifiRepQuantity(1);
		product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_GPON());
	    }
	}
	
	
    }

    private void determinateModenNameDualband(ProductFixedDTO product, FixedDTO fixedParkDto/*, CatalogModem catalogModem¨*/) {
	if (fixedParkDto.getModenNameInternet().equalsIgnoreCase(rulesValue.getMODEM_EQUIPMENT_DUALBAND())) {
	    if (product.getPackageRent() <= 199) {
		//product.setEquipmentUltraWifiRepQuantity(0);
		product.setEquipmentUltraWifiRepQuantity(1);
		product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_GPON());
	    }
	    if (product.getPackageRent() > 199) {
		//product.setEquipmentUltraWifiRepQuantity(0);
		product.setEquipmentUltraWifiRepQuantity(1);
		product.setModemEquipment(rulesValue.getMODEM_EQUIPMENT_GPON());
	    }
	}
	
	
    }
    
    private void determinateUltraWifiRep(ProductFixedDTO product, String pp) {
	
	if(!(pp.equals(Constant.ONES))) {
	
        	if(product.getPackageRent() >= 280) {
        	    product.setEquipmentUltraWifiRepQuantity(1);
        	}
	
	}
	
    }
    
    private boolean activateUltraWifi(List<ParamMovTotal> listParamMovTotal) {
	
	boolean existe = false;
	
	List<ParamMovTotal> listParam = this.pmtr.findAll();
	
	for (ParamMovTotal paramMovTotal : listParam) {
	    
	    if(paramMovTotal.getGrupoParam().equals("ULTRA_WIFI")) {
		
		if(paramMovTotal.getValor().equals("1")) {
		    existe = true;
		}
		
	    }
	    
	}
	
	return existe;
	
    }

    @Override
    public void executeMt(List<CatalogDeco> catalogDecoList, List<CatalogModem> catalogModemList, List<ProductFixedDTO> priorizationList,
	    FixedDTO fixedParkDto, CommercialOperationInfo commercialInfo, boolean distinctPS, String modemEquipmentOfferCopy) {
	// TODO Auto-generated method stub
	
    }

    
}
