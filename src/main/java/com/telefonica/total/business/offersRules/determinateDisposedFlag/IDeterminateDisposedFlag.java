package com.telefonica.total.business.offersRules.determinateDisposedFlag;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.pojo.req.CommercialOperation;

public interface IDeterminateDisposedFlag {

    void execute(FixedDTO fixedParkDto, List<CommercialOperation> commercialOpeList, List<MobileDTO> mobileDevices);
}
