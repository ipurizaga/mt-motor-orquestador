package com.telefonica.total.business.offersRules.financialPercentage;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.dto.FourthDigitFinanceDTO;
import com.telefonica.total.exception.BusinessException;
import com.telefonica.total.model.CatalogFourthDigitFinance;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.repository.Catalog4DigitFinanRepo;

@Service
public class FinancialPercentage implements IFinancialPercentage{

    @Autowired
    private Catalog4DigitFinanRepo catalog4DigitFinanRepo;
    
    public List<FourthDigitFinanceDTO> execute(List<CommercialOperation> commercialOperations) {
	List<FourthDigitFinanceDTO> outList = new ArrayList<>();
	List<CatalogFourthDigitFinance> queryList = catalog4DigitFinanRepo.findAll();
	for (CommercialOperation operation : commercialOperations) {
	    String operationType = operation.getDeviceOperation();
	    String fourth = operation.getCreditData().getCreditScore().substring(3);
	    if (fourth.isEmpty()) {
		    throw new BusinessException(Constant.BE_1064);
	    }
	    for (CatalogFourthDigitFinance finance : queryList) {
		
        		if (finance.getRisk() == Integer.parseInt(fourth) && ((finance.getFlow() == null ) || finance.getFlow().equals(operationType))) {
        		    FourthDigitFinanceDTO obj = new FourthDigitFinanceDTO();
        		    obj.setCommercialOperationId(operation.getOperationCommId());
        		    obj.setPercentage(finance.getPercentage());
        		    outList.add(obj);
        		    break;
        		}		
		
	    }
	}
	return outList;
    }
}
