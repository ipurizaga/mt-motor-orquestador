package com.telefonica.total.business.offersRules.pdfCondition;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.dto.FourthDigitDTO;
import com.telefonica.total.exception.BusinessException;
import com.telefonica.total.model.CatalogFouthDigitPPF;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.repository.Catalog4DigitPPFRepo;

@Service
public class PdfCondition implements IPdfCondition{
    
    @Autowired
    private Catalog4DigitPPFRepo catalog4DigitPPFRepo;

    @Override
    public List<FourthDigitDTO> execute(List<CommercialOperation> commercialOperations) {
	FourthDigitDTO postPaid = null;
	List<FourthDigitDTO> outList = new ArrayList<>();
	List<CatalogFouthDigitPPF> queryList = catalog4DigitPPFRepo.findAll();
	for (CommercialOperation operation : commercialOperations) {
	    String fourth = operation.getCreditData().getCreditScore().substring(3);
	    if (fourth.isEmpty()) {
		    throw new BusinessException(Constant.BE_1064);
	    }
	    for (CatalogFouthDigitPPF digit : queryList) {
		if (digit.getFourthDigitScore().equals(fourth)) {
		    postPaid = new FourthDigitDTO();
		    postPaid.setCommercialOperationId(operation.getOperationCommId().toString());
		    postPaid.setAmountPlanRank(digit.getAmountPlanRank());
		    postPaid.setFlagPpf(digit.getFlagPpf());
		    outList.add(postPaid);
		    break;
		}
	    }
	}
	return outList;
    }
}
