package com.telefonica.total.business.offersRules.creditLimitRule;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.UtilCollections;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.exception.BusinessException;
import com.telefonica.total.jdbc.BillProdOffDao;
import com.telefonica.total.model.CatalogBillingProduct;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.CommercialOperationInfo;
import com.telefonica.total.pojo.req.Customer;
import com.telefonica.total.pojo.req.Offer;

@Service
public class CreditLimitRule implements ICreditLimitRule {

    @Autowired
    private BillProdOffDao billProd;
    
    public void executeSa(List<ProductFixedDTO> productList, CommercialOperationInfo commercialInfo) {
	List<ProductFixedDTO> removeList = new ArrayList<>();
	if (!productList.isEmpty()) {
	    List<CommercialOperation> filteredMobilelist = UtilCollections.getCollectionByType(commercialInfo.getCommercialOpers(),
		    Constant.MOBILE);
	    List<Double> creditLimits = new ArrayList<>();
	    for (int i = 0; i < filteredMobilelist.size(); i++) {
		creditLimits.add(filteredMobilelist.get(i).getCreditData().getCreditLimit());
	    }

	    double clientCreditLimit = Collections.max(creditLimits);
	    removeProductsST(productList, clientCreditLimit, removeList);
	    //availableOffersMT(productList, Constant.BE_1068);
	    /** Se soluciona el error con el mensaje de iquitos cuando no calificaa scoring**/
	    availableOffersMT(productList, Constant.BE_1081);
	}
    }

    private void removeProductsST(List<ProductFixedDTO> productList, double clientCreditLimit, List<ProductFixedDTO> removeList) {
	for (ProductFixedDTO productFixed : productList) {
	    if (clientCreditLimit < (productFixed.getPackageRent())) {
		removeList.add(productFixed);
	    }
	}
	productList.removeAll(removeList);
    }
    
    private void availableOffersMT(final List<ProductFixedDTO> filteredProductList, String exceptionParameter) {
	if (filteredProductList.isEmpty()) {
	    throw new BusinessException(exceptionParameter);
	}
    }
    
    public void executeSaLMA(List<ProductFixedDTO> productList, CommercialOperationInfo commercialInfo, Customer customer, Offer offer) {
	
	List<CatalogBillingProduct> catBillProdList = this.billProd.getAllBillProd();
	
	List<CatalogBillingProduct> catBillProdListFinal = new ArrayList<CatalogBillingProduct>();
	
	List<CommercialOperation> filteredMobilelist = UtilCollections.getCollectionByType(commercialInfo.getCommercialOpers(),
		    Constant.MOBILE);
	    
	    List<Double> creditLimits = new ArrayList<>();
	    
	    List<Integer> plansRank = new ArrayList<>();
	    
	    List<Integer> plansRankMenores = new ArrayList<>();
	    
	    for (CommercialOperation commercialOperation : filteredMobilelist) {
		creditLimits.add(commercialOperation.getCreditData().getCreditLimit());
	    }
	    
	    double clientCreditLimit = Collections.max(creditLimits);
	
	for (CatalogBillingProduct catalogBillingProduct : catBillProdList) {
	    
	    //if((catalogBillingProduct.getPoCustomerType() == null || customer.getCustomerType().equals(catalogBillingProduct.getPoCustomerType())) && offer.getPoCode().equals(catalogBillingProduct.getPoCode())) {
	    if((catalogBillingProduct.getPoCustomerType() == null || (customer.getCustomerType().equals(catalogBillingProduct.getPoCustomerType())) && offer.getPoCode().equals(catalogBillingProduct.getPoCode()))) {
		//catBillProdListFinal.add(catalogBillingProduct);
		
		plansRank.add(catalogBillingProduct.getBoPlankRank());
		
	    }
	    
	}
	
	
	
	Integer planRankMin = Collections.min(plansRank);
	
	if (clientCreditLimit < (Double.valueOf(planRankMin.intValue()))) {
	    
	    throw new BusinessException(Constant.BE_1081);
	    
	}/*else {
	    
	    List<ProductFixedDTO> removeList = new ArrayList<>();
		if (!productList.isEmpty()) {
		    removeProductsST(productList, clientCreditLimit, removeList);
		    //availableOffersMT(productList, Constant.BE_1068);
		    // Se soluciona el error con el mensaje de iquitos cuando no calificaa scoring
		    availableOffersMT(productList, Constant.BE_1081);
		}
	}*/
	
	/*for (CatalogBillingProduct catalogBillingProductFinal : catBillProdListFinal) {
	    if (clientCreditLimit < (Double.valueOf(catalogBillingProductFinal.getBoPlankRank().intValue()))) {
		    throw new BusinessException(Constant.BE_1081);
	    }
	}*/
	
    }
}
