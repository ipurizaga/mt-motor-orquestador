package com.telefonica.total.business.offersRules.obtainAndValidatePoFromRequest;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.exception.BusinessException;

@Service
public class ObtainAndValidatePoFromRequestMt implements IObtainAndValidatePoFromRequest{

    @Override
    public void execute(FixedDTO fixedParkDto, List<MobileDTO> mobileDevices, List<ProductFixedDTO> allProductsMT) {

	Set<Integer> mobilePo = new HashSet<>();
	List<Integer> mobilePoList = new ArrayList<>();
	for (MobileDTO mobile : mobileDevices) {
	    if (mobile.getIsMtm() != null && mobile.getIsMtm()) {
		mobilePo.add(mobile.getPoId());
		mobilePoList.add(mobile.getPoId());
	    }

	}
	boolean distinctPo = mobilePo.size() == 1 ? Boolean.TRUE : Boolean.FALSE;
	if (distinctPo) {
	    boolean existPO = Boolean.FALSE;
	    for (ProductFixedDTO product : allProductsMT) {
		if (mobilePo.contains(Integer.parseInt(product.getPoPackage()))) {
		    existPO |= Boolean.TRUE;
		} else {
		    existPO &= Boolean.FALSE;
		}
	    }
	    if (existPO) {
		fixedParkDto.setPoIdRequest(mobilePoList.get(0));
	    }
	} else {
	    throw new BusinessException(Constant.BE_1067);
	}
    }
}
