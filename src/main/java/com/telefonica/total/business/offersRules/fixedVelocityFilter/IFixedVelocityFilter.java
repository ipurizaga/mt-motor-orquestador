package com.telefonica.total.business.offersRules.fixedVelocityFilter;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;

public interface IFixedVelocityFilter {

    /***
     * Método que valida si los productos disponibles tienen una velocidad de
     * internet menor al plan actual y le agrega la caracteristica "Menor_Velocidad"
     * 
     * @param fixedParkDto
     * @param filteredProductList
     */
    void execute(FixedDTO fixedParkDto, List<ProductFixedDTO> filteredProductList);

}
