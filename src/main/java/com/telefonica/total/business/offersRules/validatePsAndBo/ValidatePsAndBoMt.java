package com.telefonica.total.business.offersRules.validatePsAndBo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.IdentifierProductDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.ProductFixedDTO;

@Service
public class ValidatePsAndBoMt implements IValidatePsAndBo{

    @Override
    public void executeMt(FixedDTO fixedParkDto, List<MobileDTO> mobileDevices, List<ProductFixedDTO> allProductsMT) {

	Set<IdentifierProductDTO> identifiers = new HashSet<>();
	List<IdentifierProductDTO> identifiersList = new ArrayList<>();
	List<ProductFixedDTO> inexistentList = new ArrayList<>();
	List<ProductFixedDTO> completeOperationList = new ArrayList<>();

	for (MobileDTO mobile : mobileDevices) {
	    Boolean existent = false;
	    if (mobile.getIsMtm() != null && mobile.getIsMtm()) {
		IdentifierProductDTO productDto = new IdentifierProductDTO();
		for (ProductFixedDTO products : allProductsMT) {
		    if (fixedParkDto.getPsRequest().equals(products.getPackagePs().toString())
			    && fixedParkDto.getPoIdRequest().equals(Integer.valueOf(products.getPoPackage()))
			    && mobile.getBoId().equals(Integer.valueOf(products.getPackageMobilCode()))) {
			productDto.setPs(Integer.parseInt(fixedParkDto.getPsRequest()));
			productDto.setBo(mobile.getBoId());
			productDto.setState(products.getEnabled());
			productDto.setMirror(products.getMirrorOffer());
			identifiers.add(productDto);
			identifiersList.add(productDto);
			existent |= true;
		    }
		}
	    } else {
		completeOperationList.add(new ProductFixedDTO());
	    }

	    if (existent == Boolean.FALSE) {
		inexistentList.add(new ProductFixedDTO());
	    }

	}
	verifyAndAsignPriorizationFlux(fixedParkDto, identifiers, identifiersList, inexistentList, completeOperationList);

    }

    private void verifyAndAsignPriorizationFlux(FixedDTO fixedParkDto, Set<IdentifierProductDTO> identifiers,
	    List<IdentifierProductDTO> identifiersList, List<ProductFixedDTO> inexistentList, List<ProductFixedDTO> completeOperationList) {
	if (completeOperationList.isEmpty() && !inexistentList.isEmpty()) {
	    fixedParkDto.setPriorizationFlux(Constant.FLUJO_INEXISTENTE);
	} else {
	    if (identifiers.size() == 1) {
		/* caso 1 - verificar habilitado o deshabilitada */
		fixedParkDto.setBoIdRequest(identifiersList.get(0).getBo());
		if (identifiersList.get(0).getState().equals(Constant.ONE)) {
		    fixedParkDto.setPriorizationFlux(Constant.FLUJO_REGULAR);
		} else {
		    fixedParkDto.setPriorizationFlux(Constant.FLUJO_MIGRACION_PARRILLA);
		}
	    } else {
		/* caso 2 - verificar bo si son equivalentes y los estados distintos */
		determinateIdentifiersList(identifiersList, fixedParkDto);
	    }
	}
    }
    
    private void determinateIdentifiersList(List<IdentifierProductDTO> identifiersList, FixedDTO fixedParkDto) {
	if (!identifiersList.isEmpty() && compareMirror(identifiersList) == Boolean.TRUE
		&& compareState(identifiersList) == Boolean.FALSE) {
	    for (IdentifierProductDTO elemet : identifiersList) {
		if (elemet.getState().equals(Constant.ZERO)) {
		    fixedParkDto.setBoIdRequest(elemet.getBo());
		}
	    }
	    fixedParkDto.setPriorizationFlux(Constant.FLUJO_MIGRACION_PARRILLA);
	} else {
	    fixedParkDto.setPriorizationFlux(Constant.FLUJO_INEXISTENTE);
	}
    }
    
    private boolean compareMirror(List<IdentifierProductDTO> identifiersList) {
	boolean equalsMirror = false;
	IdentifierProductDTO item = identifiersList.get(0);
	for (IdentifierProductDTO identifier : identifiersList) {
	    if (item.getMirror().equals(identifier.getMirror())) {
		equalsMirror |= true;
	    } else {
		equalsMirror &= false;
	    }
	}
	return equalsMirror;
    }

    private boolean compareState(List<IdentifierProductDTO> identifiersList) {
	boolean equalsState = false;
	IdentifierProductDTO item = identifiersList.get(0);
	for (IdentifierProductDTO identifier : identifiersList) {
	    if (item.getState().toString().equals(identifier.getState().toString())) {
		equalsState |= true;
	    } else {
		equalsState &= false;
	    }
	}
	return equalsState;
    }
}
