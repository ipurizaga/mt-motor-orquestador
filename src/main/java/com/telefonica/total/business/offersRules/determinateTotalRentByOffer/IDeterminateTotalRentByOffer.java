package com.telefonica.total.business.offersRules.determinateTotalRentByOffer;

import java.util.List;

import com.telefonica.total.dto.ProductFixedDTO;

public interface IDeterminateTotalRentByOffer {

    /***
     * Método que se encarga de realizar el calculo de la renta total por cada
     * oferta de la lista de productos priorizados.
     * 
     * @param priorizationList
     */
    void execute(List<ProductFixedDTO> priorizationList);
}
