package com.telefonica.total.business.offersRules.netflixFilter;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.pojo.req.CommercialOperationInfo;

public interface INetflixFilter {

    /***
     * Método que se encarga de remover el campo de netflix de los productos con
     * renta mayor a S/.359
     * 
     * @param fixedParkDto
     * @param filteredProductList
     * @param commercialInfo
     */
    void execute(FixedDTO fixedParkDto, List<ProductFixedDTO> filteredProductList, CommercialOperationInfo commercialInfo);

}
