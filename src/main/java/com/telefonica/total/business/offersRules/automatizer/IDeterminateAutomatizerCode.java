package com.telefonica.total.business.offersRules.automatizer;

import java.util.List;

import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.pojo.req.CommercialOperationInfo;
import com.telefonica.total.pojo.req.Customer;

public interface IDeterminateAutomatizerCode {
    
    /**
     * Método que se encarga de calcular el codigo para automatizador por cada oferta
     * 
     * @param commercialOperationInfo
     * @param customer
     * @param priorizationList
     */
    void execute(CommercialOperationInfo commercialOperationInfo, Customer customer,
	    List<ProductFixedDTO> priorizationList);

}
