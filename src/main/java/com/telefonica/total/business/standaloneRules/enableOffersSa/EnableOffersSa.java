package com.telefonica.total.business.standaloneRules.enableOffersSa;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.dto.ProductFixedDTO;

@Service
public class EnableOffersSa implements IEnableOffersSa{

    @Override
    public List<ProductFixedDTO> execute(List<ProductFixedDTO> catalogProductFixedDto) {
	List<ProductFixedDTO> enabledOffers = new ArrayList<>();
	enabledOffers.addAll(catalogProductFixedDto);
	CollectionUtils.filter(enabledOffers, new Predicate<ProductFixedDTO>() {
	    @Override
	    public boolean evaluate(ProductFixedDTO object) {
		if (object.getEnabled().equals(Constant.ONE)) {
		    return true;
		}
		return false;
	    }
	});
	return enabledOffers;
    }
}
