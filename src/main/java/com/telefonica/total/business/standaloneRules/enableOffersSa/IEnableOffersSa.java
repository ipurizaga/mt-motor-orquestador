package com.telefonica.total.business.standaloneRules.enableOffersSa;

import java.util.List;

import com.telefonica.total.dto.ProductFixedDTO;

public interface IEnableOffersSa {

    /***
     * Método que se encarga de filtar los productos que se encuentran habilitados.
     * 
     * @param catalogProductFixedDto
     * @return
     */
    List<ProductFixedDTO> execute(List<ProductFixedDTO> catalogProductFixedDto);
}
