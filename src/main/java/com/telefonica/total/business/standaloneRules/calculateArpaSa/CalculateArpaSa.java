package com.telefonica.total.business.standaloneRules.calculateArpaSa;

import java.util.List;

import org.springframework.stereotype.Service;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;

@Service
public class CalculateArpaSa implements ICalculateArpaSa{

    public void executeSa(FixedDTO fixedParkDto, List<MobileDTO> mobileDevices) {
	double mobileAcumulator = 0D;
	for (MobileDTO mobile : mobileDevices) {
	    mobileAcumulator += mobile.getMobileRentMonoProduct() - Math.abs(mobile.getPermanentDisccount());
	}
	fixedParkDto.setPresentARPA(mobileAcumulator);
    }

}
