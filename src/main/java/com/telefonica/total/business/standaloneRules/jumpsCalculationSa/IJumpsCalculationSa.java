package com.telefonica.total.business.standaloneRules.jumpsCalculationSa;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.Customer;

public interface IJumpsCalculationSa {

    void executeSa(FixedDTO fixedParkDto, List<ProductFixedDTO> filteredProductList, List<MobileDTO> mobileDevices,
	    List<CommercialOperation> commercialOpeList, Customer customer);
}
