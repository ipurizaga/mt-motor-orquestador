package com.telefonica.total.business.standaloneRules.determinatePresentBonusAndPresentDiscountsSa;

import java.util.List;

import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.common.util.UtilCollections;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.Customer;

@Service
public class DeterminatePresentBonusAndPresentDiscountsSa implements IDeterminatePresentBonusAndPresentDiscountsSa{

    public void executeSa(Customer customer, List<CommercialOperation> commercialOpeList,
	    List<MobileDTO> mobileDevices) {
	List<String> numbers = UtilCollections.obtainMobileCaeqCaplNumbers(commercialOpeList);
	for (MobileDTO mobile : mobileDevices) {
	    for (String number : numbers) {
		if (number.equals(mobile.getMobileNumber())) {
		    calculatePresentDuplicateBonus(mobile);
		    if (!customer.getIsEmployee()) {
			calculatePresentDiscounts(mobile);
		    }
		}
	    }
	}
    }
    
    private void calculatePresentDuplicateBonus(MobileDTO mobile) {
	if (mobile.getDuplicateBonusFlag().equals(Constant.ONE)) {
	    mobile.setPresentDuplicateBonusDataQuantity(mobile.getMobileQuantityData());
	    mobile.setPresentTotalDataQuantity((double) mobile.getMobileQuantityData() + mobile.getPresentDuplicateBonusDataQuantity());
	    mobile.setPresentDuplicateBonusEndDate(
		    TotalUtil.addMonthsReturnStringFromSqlDate(mobile.getDuplicateBonusAsig(), mobile.getDuplicateBonusExpiration()));
	}
    }

    private void calculatePresentDiscounts(MobileDTO mobile) {
	if (mobile.getPromotionalDiscount().equals(Constant.ONE)) {
	    mobile.setPresentPromotionalDiscountRent(0.5 * mobile.getMobileRentMonoProduct());
	    mobile.setPresentPromotionalFinalRent(mobile.getMobileRentMonoProduct() - mobile.getPresentPromotionalDiscountRent());
	    mobile.setPresentPromotionalEndDate(TotalUtil.addMonthsReturnStringFromSqlDate(mobile.getPromotionalDiscounAsig(),
		    mobile.getPromotionalDiscountExpiration()));
	}
    }
}
