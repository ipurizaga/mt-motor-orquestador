package com.telefonica.total.business.standaloneRules.prioritizationSa;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;

import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.model.ParamMovTotal;

@Service
public class PriorizationSa implements IPriorizationSa {

    public List<ProductFixedDTO> executeSa(List<ProductFixedDTO> filteredProductList, List<ParamMovTotal> lstParam, FixedDTO fixedParkDto) {
	List<ProductFixedDTO> productsToPrioritize = new ArrayList<>();
	List<ProductFixedDTO> priorizatedProducts = new ArrayList<>();
	String productType = SuscriptionType.MOBILE.getCode();
	TotalUtil.obtainOfferMTWithPositiveJump(filteredProductList, productsToPrioritize,productType, 0, 0.0,"MOBILE", lstParam, fixedParkDto);
	Collections.sort(productsToPrioritize, ProductFixedDTO.jumpComparator);
	int iterator = 0;
	for (ProductFixedDTO fixProd : productsToPrioritize) {
	    if (iterator > 2) {
		break;
	    }
	    fixProd.setPriority(String.valueOf(iterator + 1));
	    priorizatedProducts.add(fixProd);
	    iterator++;
	}
	return priorizatedProducts;
    }
}
