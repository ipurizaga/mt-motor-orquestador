package com.telefonica.total.business.standaloneRules.determinatePresentBonusAndPresentDiscountsSa;

import java.util.List;

import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.Customer;

public interface IDeterminatePresentBonusAndPresentDiscountsSa {

    void executeSa(Customer customer, List<CommercialOperation> commercialOpeList,
	    List<MobileDTO> mobileDevices);
}
