package com.telefonica.total.business.standaloneRules.mobileConsumptionFilterSa;

import java.util.List;

import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.ProductFixedDTO;

public interface IMobileConsumptionFilterSa {

    void executeSa(List<ProductFixedDTO> filteredProductList, List<MobileDTO> mobileDevices);
    void executeLMA(List<ProductFixedDTO> filteredProductList, List<MobileDTO> mobileDevices);
}
