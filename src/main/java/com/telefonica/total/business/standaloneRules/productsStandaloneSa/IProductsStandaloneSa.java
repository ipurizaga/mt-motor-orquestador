package com.telefonica.total.business.standaloneRules.productsStandaloneSa;

import java.util.List;

import com.telefonica.total.dto.ProductFixedDTO;

public interface IProductsStandaloneSa {

    List<ProductFixedDTO> execute();
    
    List<ProductFixedDTO> executeLMA();
}
