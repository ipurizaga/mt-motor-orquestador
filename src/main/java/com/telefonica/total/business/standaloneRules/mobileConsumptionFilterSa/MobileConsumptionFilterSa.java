package com.telefonica.total.business.standaloneRules.mobileConsumptionFilterSa;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.RulesValueProp;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.pojo.req.Information;

@Service
public class MobileConsumptionFilterSa implements IMobileConsumptionFilterSa{

    @Autowired
    private RulesValueProp rulesValue;
    
    public void executeSa(List<ProductFixedDTO> filteredProductList, List<MobileDTO> mobileDevices) {
	double clientMobileConsumption = 0;
	for (MobileDTO mobile : mobileDevices) {
	    clientMobileConsumption += mobile.getMobileMbConsumption();
	}
	for (ProductFixedDTO product : filteredProductList) {
	    product.setMobileQuantityDataSumatory(product.getMobil0QuantityData());
	    if (product.getMobileQuantityDataSumatory() - clientMobileConsumption < rulesValue.getLIMIT_MOBILE_CONSUME()) {
		product.getCharacteristics().add(Information.builder().code(rulesValue.getPRODUCT_CHARACTERISTIC())
			.value(rulesValue.getPRODUCT_CHARACTERISTIC_MINOR_CONSUMPTION()).build());
	    }
	}
    }
    
    public void executeLMA(List<ProductFixedDTO> filteredProductList, List<MobileDTO> mobileDevices) {
	double clientMobileConsumption = 0;
	for (MobileDTO mobile : mobileDevices) {
	    clientMobileConsumption += mobile.getMobileMbConsumption();
	}
	for (ProductFixedDTO product : filteredProductList) {
	    product.setMobileQuantityDataSumatory(product.getMobil1QuantityData());
	    if (product.getMobileQuantityDataSumatory() - clientMobileConsumption < rulesValue.getLIMIT_MOBILE_CONSUME()) {
		product.getCharacteristics().add(Information.builder().code(rulesValue.getPRODUCT_CHARACTERISTIC())
			.value(rulesValue.getPRODUCT_CHARACTERISTIC_MINOR_CONSUMPTION()).build());
	    }
	}
    }
}
