package com.telefonica.total.business.standaloneRules.productsStandaloneSa;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.dto.BeanMapper;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.model.CatalogProductFixed;
import com.telefonica.total.repository.CatalogProdFixedRepo;

@Service
public class ProductsStandaloneSa implements IProductsStandaloneSa {

    @Autowired
    private CatalogProdFixedRepo catProdFixedRepo;

    @Autowired
    private BeanMapper beanMap;

    @Override
    public List<ProductFixedDTO> execute() {
	List<CatalogProductFixed> fixedList = catProdFixedRepo.findAll();
	if (!fixedList.isEmpty()) {
	    List<ProductFixedDTO> listSt = getOffersByPackageType(beanMap.productFixedToDTOs(fixedList), Constant.TIPO_PAQUETE_MOVIL);
	    for (ProductFixedDTO productFixedDTO : listSt) {
		productFixedDTO.setMobil1Rent(Constant.CERO);
	    }
	    return listSt;
	}
	return new ArrayList<>();
    }
        
    @Override
    public List<ProductFixedDTO> executeLMA() {
	List<CatalogProductFixed> fixedList = catProdFixedRepo.findAll();
	if (!fixedList.isEmpty()) {
	    List<ProductFixedDTO> listSt = getOffersByPackageType(beanMap.productFixedToDTOs(fixedList), Constant.TIPO_PAQUETE_MOVIL);
	    
	    List<ProductFixedDTO> listNolma = new ArrayList<ProductFixedDTO>();
	    
	    /*for (ProductFixedDTO productFixedDTO : listSt) {
		productFixedDTO.setMobil1Rent(Constant.CERO);
		if(!(productFixedDTO.getEnabled().equals(Constant.TWO))) {
		    listNolma.add(productFixedDTO);
		}
	    }
	    
	    listSt.removeAll(listNolma);*/
	    return listSt;
	}
	return new ArrayList<>();
    }

    private List<ProductFixedDTO> getOffersByPackageType(List<ProductFixedDTO> catalogProductFixedDto, String packageType) {
	List<ProductFixedDTO> enabledOffers = new ArrayList<>();
	for (ProductFixedDTO productFixedDTO : catalogProductFixedDto) {
	    if (productFixedDTO.getPackageType().equals(packageType)) {
		enabledOffers.add(productFixedDTO);
	    }
	}
	return enabledOffers;
    }
}
