package com.telefonica.total.business.standaloneRules.prioritizationSa;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.ProductFixedDTO;

import com.telefonica.total.model.ParamMovTotal;

public interface IPriorizationSa {

    List<ProductFixedDTO> executeSa(List<ProductFixedDTO> filteredProductList, List<ParamMovTotal> lstParam, FixedDTO fixedParkDto);
}
