package com.telefonica.total.business.standaloneRules.calculateArpaSa;

import java.util.List;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;

public interface ICalculateArpaSa {

    void executeSa(FixedDTO fixedParkDto, List<MobileDTO> mobileDevices);
}
