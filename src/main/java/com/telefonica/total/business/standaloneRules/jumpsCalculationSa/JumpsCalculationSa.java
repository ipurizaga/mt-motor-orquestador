package com.telefonica.total.business.standaloneRules.jumpsCalculationSa;

import java.util.List;

import org.springframework.stereotype.Service;

import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.generic.CommOperCollection;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.Customer;

@Service
public class JumpsCalculationSa implements IJumpsCalculationSa{

    public void executeSa(FixedDTO fixedParkDto, List<ProductFixedDTO> filteredProductList, List<MobileDTO> mobileDevices,
	    List<CommercialOperation> commercialOpeList, Customer customer) {
	CommOperCollection<CommercialOperation> commOperCollection = new CommOperCollection<>(commercialOpeList);
	commOperCollection.filterByProduct(SuscriptionType.MOBILE);
	double mobileRent = 0;
	for (MobileDTO mobile : mobileDevices) {
	    mobileRent += mobile.getMobileRentMonoProduct() - Math.abs(mobile.getPermanentDisccount());
	}
	/* Cálculo del salto */
	for (ProductFixedDTO product : filteredProductList) {
	    product.setJump(product.getPackageRent() - mobileRent);
	}
    }
}
