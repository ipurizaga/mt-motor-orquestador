package com.telefonica.total.business;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.dto.BeanMapper;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.enums.OperationComm;
import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.exception.BusinessException;
import com.telefonica.total.generic.CommOperCollection;
import com.telefonica.total.model.CatalogPS;
import com.telefonica.total.model.MasterParkFixed;
import com.telefonica.total.model.MasterParkMobil;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.Subscription;
import com.telefonica.total.repository.CatalogPSRepo;
import com.telefonica.total.repository.MasterParkFixedRepo;
import com.telefonica.total.repository.MasterParkMobilRepo;

@Service
public class MasterParkRules implements IMastersParksRules {

    @Autowired
    private MasterParkMobilRepo masterParkMobilRepo;

    @Autowired
    private MasterParkFixedRepo masterParkFixedRepo;

    @Autowired
    private CatalogPSRepo catalogPSRepo;

    @Autowired
    private BeanMapper beanMap;

    @Override
    public List<MobileDTO> getMasterMobile(List<CommercialOperation> subscriptions) {
	CommOperCollection<CommercialOperation> reqLst = new CommOperCollection<>(subscriptions);
	reqLst.filterByProduct(SuscriptionType.MOBILE);
	List<MobileDTO> outputlist = new ArrayList<>();
	if (CollectionUtils.isNotEmpty(reqLst)) {

	    for (CommercialOperation commercialOperation : reqLst) {
		if (OperationComm.PORTABILITY.getCodeDesc().equalsIgnoreCase(commercialOperation.getOperation())
			|| OperationComm.PROVIDE.getCodeDesc().equalsIgnoreCase(commercialOperation.getOperation())) {
		    MasterParkMobil mobileMaster = TotalUtil.obtainDefaultMobileInfo();
		    validatePoBoType(commercialOperation, mobileMaster, outputlist);
		} else if (OperationComm.CAPL.getCodeDesc().equalsIgnoreCase(commercialOperation.getOperation())) {
		    if (commercialOperation.getSubscriber() != null && commercialOperation.getSubscriber().getIsRecentProvide() != null) {
			recentProvideValidations(commercialOperation, outputlist);
		    } else {
			throw new BusinessException(Constant.BE_2025);
		    }
		} else {
		    throw new BusinessException(Constant.BE_2021);
		}
	    }
	    return outputlist;
	}
	return new ArrayList<>();
    }

    /***
     * MÃ©todo que se encarga de la validacion del campo recentProvide y que este sea
     * solo 0, 1 Ã³ 2
     * 
     * @param commercialOperation
     * @param outputlist
     */
    private void recentProvideValidations(CommercialOperation commercialOperation, List<MobileDTO> outputlist) {
	
	if (commercialOperation.getSubscriber().getIsRecentProvide() == 0
		|| commercialOperation.getSubscriber().getIsRecentProvide() == 1) {
	    obtainDataFromRequest(commercialOperation, outputlist);

	} else if (commercialOperation.getSubscriber().getIsRecentProvide() == 2) {
	    MasterParkMobil mobileMaster = masterParkMobilRepo.findByMobileNumber(commercialOperation.getSubscriber().getServiceNumber());
	    if (mobileMaster == null) {
		mobileMaster = TotalUtil.obtainDefaultMobileInfo();
	    }
	    validatePoBoType(commercialOperation, mobileMaster, outputlist);

	} else {
	    throw new BusinessException(Constant.BE_2026);
	}
    }

    /***
     * MÃ©todo que se encarga de la validaciÃ³n de los campos PoId, BoId y Mtm que
     * vienen dentro del campo subscriber.
     * 
     * @param commercialOperation
     * @param mobileMaster
     * @param outputlist
     */
    private void validatePoBoType(CommercialOperation commercialOperation, MasterParkMobil mobileMaster, List<MobileDTO> outputlist) {
	if (mobileMaster != null) {
	    MobileDTO mobileDto = beanMap.mobileParkToDto(mobileMaster);
	    if (commercialOperation.getSubscriber() != null && !OperationComm.PORTABILITY.getCodeDesc().equalsIgnoreCase(commercialOperation.getOperation())) {
		asignatePo(commercialOperation.getSubscriber(), mobileDto);
		asignateBo(commercialOperation.getSubscriber(), mobileDto);
		asignateSubscriberType(commercialOperation.getSubscriber(), mobileDto);
		mobileDto.setIsMtm(
			commercialOperation.getSubscriber().getType().equals(SuscriptionType.MTM.getCode()) ? Boolean.TRUE : Boolean.FALSE);
	    }
	    outputlist.add(mobileDto);
	}
    }
    
    private void asignateSubscriberType(Subscription subscription, MobileDTO mobileDto) {
	if(subscription.getType() != null) {
	    mobileDto.setSubscriptionType(subscription.getType());
	}
    }

    private void asignatePo(Subscription subscription, MobileDTO mobileDto) {
	if (subscription.getPoId() != null) {
	    mobileDto.setPoId(subscription.getPoId());
	} else {
	    throw new BusinessException(Constant.BE_1065);
	}
    }

    private void asignateBo(Subscription subscription, MobileDTO mobileDto) {
	if (subscription.getBoId() != null) {
	    mobileDto.setBoId(subscription.getBoId());
	} else {
	    throw new BusinessException(Constant.BE_1066);
	}
    }

    /****
     * MÃ©todo para obtener y setear toda la data del request y agregarla a la lista
     * de informaciÃ³n de los datos moviles que serian equivalentes a los datos del
     * parque movil.
     * 
     * @param commercialOperation
     * @param outputlist
     */
    private void obtainDataFromRequest(CommercialOperation commercialOperation, List<MobileDTO> outputlist) {
	MasterParkMobil mobileMaster = new MasterParkMobil();
	mobileMaster.setMobileNumber(
		commercialOperation.getSubscriber() != null ? commercialOperation.getSubscriber().getServiceNumber() : null);
	mobileMaster.setIsOwner(commercialOperation.getSubscriber().getIsOwner() == Boolean.TRUE ? Constant.ONE : Constant.ZERO);
	mobileMaster.setMobileRentMonoProduct(
		commercialOperation.getSubscriber().getRentMonoProduct() != null ? commercialOperation.getSubscriber().getRentMonoProduct()
			: Constant.CERO);
	mobileMaster.setMobileQuantityData(
		commercialOperation.getSubscriber().getDataQuantity() != null ? commercialOperation.getSubscriber().getDataQuantity() : 0);
	isRecentProvideItems(mobileMaster, commercialOperation.getSubscriber().getIsRecentProvide(),
		commercialOperation.getSubscriber().getServiceNumber(), commercialOperation);
	mobileMaster.setMobileCdtMbRoaming(commercialOperation.getSubscriber().getMbRoamingQuantity() != null
		? commercialOperation.getSubscriber().getMbRoamingQuantity()
		: 0);
	mobileMaster.setPermanence(commercialOperation.getSubscriber().getPermanency());
	mobileMaster.setEarlyCaeq(commercialOperation.getSubscriber().getIsEarlyCaeq() == Boolean.TRUE ? Constant.ONE : Constant.ZERO);
	mobileMaster.setPermanentDisccount(Constant.CERO);
	validatePoBoType(commercialOperation, mobileMaster, outputlist);



    }

    /***
     * MÃ©todo que se encarga de la evaluacion de la cantidad de datos dependiendo
     * del tipo de recentProvide.
     * 
     * @param mobileMaster
     * @param type
     * @param serviceNumber
     * @return
     */

    private void isRecentProvideItems(MasterParkMobil mobileMaster, Integer type, String serviceNumber,
	    CommercialOperation commercialOperation) {
	if (type == 0) {
	    mobileMaster.setMobileRentMonoProduct(commercialOperation.getSubscriber().getRentMonoProduct() != null
		    ? commercialOperation.getSubscriber().getRentMonoProduct()
		    : Constant.CERO);
	    mobileMaster.setDuplicateBonusFlag('0');
	    mobileMaster.setDuplicateBonusExpiration(0);
	    mobileMaster.setDuplicateBonusAsig(null);
	    mobileMaster.setPromotionalDiscount('0');
	    mobileMaster.setPromotionalDiscountExpiration(0);
	    mobileMaster.setPromotionalDiscounAsig(null);
	    mobileMaster.setMobileMbConsumption(Double.valueOf(mobileMaster.getMobileQuantityData() * Constant.CONSUMPTION_PERCENTAGE));
	} else if (type == 1) {
	    MasterParkMobil mobileRegistry = masterParkMobilRepo.findByMobileNumber(serviceNumber);
	    mobileMaster.setMobileMbConsumption(
		    mobileRegistry != null && mobileRegistry.getMobileMbConsumption() != null ? mobileRegistry.getMobileMbConsumption()
			    : Double.valueOf(mobileMaster.getMobileQuantityData() * Constant.CONSUMPTION_PERCENTAGE));
	    /* se agrega la renta mono producto para el parche equifax */
	    equifaxPatchRentMonoproduct(mobileMaster, commercialOperation, mobileRegistry);
	    duplicateBonusAndPromotionalDiscounts(mobileMaster, mobileRegistry);
	}
    }

    /***
     * MÃ©todo que permite la asignacion de los bonos y descuentos promocionales.
     * 
     * @param mobileMaster
     * @param mobileRegistry
     */
    private void duplicateBonusAndPromotionalDiscounts(MasterParkMobil mobileMaster, MasterParkMobil mobileRegistry) {
	mobileMaster.setDuplicateBonusFlag(mobileRegistry != null ? mobileRegistry.getDuplicateBonusFlag() : Constant.ZERO);
	mobileMaster.setDuplicateBonusExpiration(mobileRegistry != null ? mobileRegistry.getDuplicateBonusExpiration() : Constant.CERO_INT);
	mobileMaster.setDuplicateBonusAsig(mobileRegistry != null ? mobileRegistry.getDuplicateBonusAsig() : null);
	mobileMaster.setPromotionalDiscount(mobileRegistry != null ? mobileRegistry.getPromotionalDiscount() : Constant.ZERO);
	mobileMaster.setPromotionalDiscountExpiration(
		mobileRegistry != null ? mobileRegistry.getPromotionalDiscountExpiration() : Constant.CERO_INT);
	mobileMaster.setPromotionalDiscounAsig(mobileRegistry != null ? mobileRegistry.getPromotionalDiscounAsig() : null);
    }

    /***
     * MÃ©todo para la asignacion del parche equifax cuando la rentamonoproducto sea
     * 0
     * 
     * @param mobileMaster
     * @param commercialOperation
     * @param mobileRegistry
     */
    private void equifaxPatchRentMonoproduct(MasterParkMobil mobileMaster, CommercialOperation commercialOperation,
	    MasterParkMobil mobileRegistry) {
	if (commercialOperation.getSubscriber().getRentMonoProduct() == 0) {
	    mobileMaster.setMobileRentMonoProduct(
		    mobileRegistry != null && mobileRegistry.getMobileRentMonoProduct() != null ? mobileRegistry.getMobileRentMonoProduct()
			    : Constant.CERO);
	} else {
	    mobileMaster.setMobileRentMonoProduct(commercialOperation.getSubscriber().getRentMonoProduct() != null
		    ? commercialOperation.getSubscriber().getRentMonoProduct()
		    : Constant.CERO);
	}
    }

    @Override
    public FixedDTO getMasterFixed(List<CommercialOperation> commercialOperations) {
	CommOperCollection<CommercialOperation> reqList = new CommOperCollection<>(commercialOperations);
	reqList.filterByProduct(SuscriptionType.FIXED);
	reqList.filterByOperationType(SuscriptionType.FIXED);
	String serviceNumber = Constant.DEFAULT_NUMBER_FIXED;
	if (reqList.size() == 1) {
	    serviceNumber = reqList.get(0).getSubscriber().getServiceNumber();
	}
	MasterParkFixed fixedMaster = masterParkFixedRepo.findByPhoneNumber(Integer.parseInt(serviceNumber));
	if (fixedMaster != null) {
	    return beanMap.fixedParkToDTO(fixedMaster);
	} else {
	    String suscriberId = reqList.get(0).getSubscriber().getSubscriberId();
	    if (suscriberId != null && !suscriberId.isEmpty()) {
		//CatalogPS catalogPS = catalogPSRepo.findByPackagePs(Integer.parseInt(suscriberId));
		CatalogPS catalogPS = null;
		List<CatalogPS> listCatalogs = catalogPSRepo.findAll();
		for (CatalogPS catalogPS2 : listCatalogs) {
		    if(catalogPS2.getPackagePs().equals(Integer.parseInt(suscriberId))) {
			catalogPS = catalogPS2;
			break;
		    }
		}
		if (catalogPS != null) {
		    fixedMaster = new MasterParkFixed();
		    obtainDataFromCatalogoPS(fixedMaster, catalogPS);
		    return beanMap.fixedParkToDTO(fixedMaster);
		} else {
		    throw new BusinessException(Constant.BE_2024);
		}
	    } else {
		throw new BusinessException(Constant.BE_2024);
	    }
	}
    }

    @Override
    public List<MasterParkMobil> getMasterMobileByDocument(String document) {
	List<MasterParkMobil> outputList = masterParkMobilRepo.findAllByDocument(document);
	if (CollectionUtils.isNotEmpty(outputList)) {
	    return outputList;
	}
	return new ArrayList<>();
    }

    private void obtainDataFromCatalogoPS(MasterParkFixed fixedMaster, CatalogPS catalogPS) {
	fixedMaster.setPackagePs(catalogPS.getPackagePs());
	fixedMaster.setPackageCategory(catalogPS.getCategory());
	fixedMaster.setPackageName(catalogPS.getPackageName());
	fixedMaster.setPackageRent(catalogPS.getPackageRent());
	fixedMaster.setPresentLine(catalogPS.getPresentLine());
	fixedMaster.setRentMonoProdLine(catalogPS.getRentMonoProdLine());
	fixedMaster.setPresentInternet(catalogPS.getPresentInternet());
	fixedMaster.setRentMonoProdInternet(catalogPS.getRentMonoProdInternet());
	fixedMaster.setTechnologyInternet(catalogPS.getTechnologyInternet());
	fixedMaster.setSpeedInternet(TotalUtil.getNumericValueOf(catalogPS.getSpeedInternet()));
	fixedMaster.setModenNameInternet(catalogPS.getModenNameInternet());
	fixedMaster.setCablePresent(catalogPS.getCablePresent());
	fixedMaster.setCableRentMonoProd(catalogPS.getCableRentMonoProd());
	fixedMaster.setCableType(catalogPS.getCableType());
	fixedMaster.setCableQuantitydigitalDeco(catalogPS.getCableQuantitydigitalDeco());
	fixedMaster.setCableQuantityDecoHd(catalogPS.getCableQuantityDecoHd());
	fixedMaster.setCableQuantityDecoSmart(catalogPS.getCableQuantityDecoSmart());
	fixedMaster.setCableQuantityDecoDvr(catalogPS.getCableQuantityDecoDvr());

	fixedMaster.setIquitosCobInternet(Constant.ZERO);
	fixedMaster.setPermanentDiscount(Constant.CERO);
	fixedMaster.setCableRentMonoProdPremHd(Constant.CERO);
	fixedMaster.setCableRentMonoProdPremFox(Constant.CERO);
	fixedMaster.setCableRentMonoProdPremHbo(Constant.CERO);
	fixedMaster.setCablePresentEstellar(Constant.ZERO);
	fixedMaster.setDeposed(Constant.ZERO);
    }

}
