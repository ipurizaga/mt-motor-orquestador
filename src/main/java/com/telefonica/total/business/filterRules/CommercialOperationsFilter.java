package com.telefonica.total.business.filterRules;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.business.IMastersParksRules;
import com.telefonica.total.business.offersRules.determinateActualFixedRent.IDeterminateActualFixedRent;
import com.telefonica.total.dto.BeanMapper;
import com.telefonica.total.dto.CatalogPsDTO;
import com.telefonica.total.dto.FilterRequestDataDTO;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.enums.CableTechnologyType;
import com.telefonica.total.enums.CategoryType;
import com.telefonica.total.enums.InternetTechnologyType;
import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.exception.BusinessException;
import com.telefonica.total.generic.CommOperCollection;
import com.telefonica.total.model.CatalogPS;
import com.telefonica.total.model.MasterParkFixed;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.ReqData;
import com.telefonica.total.repository.CatalogPSRepo;
import com.telefonica.total.repository.MasterParkFixedRepo;

@Service
public class CommercialOperationsFilter {

    @Autowired
    private CatalogPSRepo catalogPs;

    @Autowired
    private MasterParkFixedRepo masterParkFixedRepo;

    @Autowired
    private IMastersParksRules parkRules;

    @Autowired
    private BeanMapper beanMap;
    
    @Autowired
    private IDeterminateActualFixedRent determinateActualFixedRent;

    public FilterRequestDataDTO execute(ReqData request) {
	List<CommercialOperation> commercialOperations = request.getCommOperationInfo().getCommercialOpers();
	// Extraer operaciones comeriales fijas
	CommOperCollection<CommercialOperation> fixedOperations = filterFixedOperations(commercialOperations);
	List<CatalogPsDTO> fixedOperationsList = searchOperationsInCatalog(fixedOperations);
	// Movistar total Fijo
	filterByOperationMTF(fixedOperationsList);
	// Categoria de Paquete
	filterByPackage(fixedOperationsList);
	// Tecnologia internet
	filterByInternetTechnology(fixedOperationsList);
	// Tecnologia cable
	//filterByCableTechnology(fixedOperationsList);
	// Renta
	filterByFixedRentAndBlock(fixedOperationsList);
	// Extraer operaciones comeriales mobiles
	CommOperCollection<CommercialOperation> mobileOperations = filterMobileOperations(commercialOperations);
	List<MobileDTO> mobileOperationsList = searchOperationsInPark(mobileOperations);
	// Movistar total movil
	filterByOperationMTM(mobileOperationsList);
	// renta
	filterByMobileRent(mobileOperationsList);
	// validar si el cliente ya es MT
	validateClientMT(fixedOperationsList, mobileOperationsList);
	// retornar los campos
	return new FilterRequestDataDTO(fixedOperationsList, mobileOperationsList);

    }

    private void validateClientMT(List<CatalogPsDTO> fixedOperationsList, List<MobileDTO> mobileOperationsList) {
	boolean isMtf = fixedOperationsList.get(0).getSubscriberType().equalsIgnoreCase(SuscriptionType.MTM.getCode()) ? Boolean.TRUE : Boolean.FALSE;
	boolean isMtm = false;
	for (MobileDTO mobile : mobileOperationsList) {
	    if (mobile.getIsMtm() != null && mobile.getIsMtm()) {
		isMtm |= true;
	    }
	}
	if (isMtf && isMtm) {
	    throw new BusinessException(Constant.BE_1072);
	}
    }

    private void filterByMobileRent(List<MobileDTO> mobileOperationsList) {
	// (mayor a menor)
	Collections.sort(mobileOperationsList, MobileDTO.majorToMinorRentComparator);
	List<MobileDTO> foundItems = new ArrayList<>();
	int count = 0;
	for (MobileDTO item : mobileOperationsList) {
	    if (count < 2) {
		foundItems.add(item);
	    }
	    count++;
	}
	mobileOperationsList.clear();
	mobileOperationsList.addAll(foundItems);
	foundItems = null;
    }

    private void filterByFixedRentAndBlock(List<CatalogPsDTO> fixedOperationsList) {
	// (renta fija)
	if (verifyNumberofOperations(fixedOperationsList)) {
	    int iteration = 0;
	    List<FixedDTO> fixedDtoList = new ArrayList<>();
	    for (CatalogPsDTO item : fixedOperationsList) {
		MasterParkFixed fixedMaster = masterParkFixedRepo.findByPhoneNumber(Integer.parseInt(item.getServiceNumber()));
		if (fixedMaster != null) {
		    FixedDTO fixedDto = beanMap.fixedParkToDTO(fixedMaster);
		    determinateActualFixedRent.execute(fixedDto);
		    fixedDto.setEquivalentCommercialOperation(iteration); 
		    fixedDtoList.add(fixedDto);
		    iteration++;
		}
	    }
	    Collections.sort(fixedDtoList, FixedDTO.majorToMinorRentComparator);
	    List<CatalogPsDTO> foundItem = new ArrayList<>();	    
	    foundItem.add(fixedOperationsList.get(fixedDtoList.get(0).getEquivalentCommercialOperation()));
	    fixedOperationsList.clear();
	    fixedOperationsList.addAll(foundItem);
	    fixedDtoList = null;
	    foundItem = null;
	}
    }

    private void filterByCableTechnology(List<CatalogPsDTO> fixedOperationsList) {
	// catv - dth
	/*if (verifyNumberofOperations(fixedOperationsList)) {
	    List<CatalogPsDTO> filtrados = new ArrayList<>();
	    for (String categoryCode : CableTechnologyType.getCableTechonologyList()) {
		for (CatalogPsDTO operation : fixedOperationsList) {
		    if (operation.getCableTechnology() != null && categoryCode.equalsIgnoreCase(operation.getCableTechnology())) {
			filtrados.add(operation);
		    }
		}
		if (!filtrados.isEmpty()) {
		    break;
		}
	    }
	    fixedOperationsList.clear();
	    fixedOperationsList.addAll(filtrados);
	    filtrados = null;
	}*/
    }

    private void filterByInternetTechnology(List<CatalogPsDTO> fixedOperationsList) {
	// ftth - hfc - adsl
	if (verifyNumberofOperations(fixedOperationsList)) {
	    List<CatalogPsDTO> filtrados = new ArrayList<>();
	    for (String categoryCode : InternetTechnologyType.getInternetTechonologyList()) {
		for (CatalogPsDTO operation : fixedOperationsList) {
		    if (operation.getTechnologyInternet() != null && categoryCode.equalsIgnoreCase(operation.getTechnologyInternet())) {
			filtrados.add(operation);
		    }
		}
		if (!filtrados.isEmpty()) {
		    break;
		}
	    }
	    fixedOperationsList.clear();
	    fixedOperationsList.addAll(filtrados);
	    filtrados = null;
	}
    }

    private void filterByPackage(List<CatalogPsDTO> fixedOperationsList) {
	if (verifyNumberofOperations(fixedOperationsList)) {
	    List<CatalogPsDTO> filtrados = new ArrayList<>();
	    for (String categoryCode : CategoryType.getCategoryTypeCodeList()) {
		for (CatalogPsDTO operation : fixedOperationsList) {
		    if (operation.getCategory() != null && categoryCode.equalsIgnoreCase(operation.getCategory())) {
			filtrados.add(operation);
		    }
		}
		if (!filtrados.isEmpty()) {
		    break;
		}
	    }
	    fixedOperationsList.clear();
	    fixedOperationsList.addAll(filtrados);
	    filtrados = null;
	}
    }
    
    private void filterByOperationMTM(List<MobileDTO> mobileOperationsList) {
	if (verifyNumberofOperations(mobileOperationsList)) {
	    List<MobileDTO> filtrados = new ArrayList<>();
	    for (MobileDTO item : mobileOperationsList) {
		if (item.getSubscriptionType().equalsIgnoreCase(SuscriptionType.MTM.getCode())) {
		    filtrados.add(item);
		}
	    }
	    if(!filtrados.isEmpty()) {
		mobileOperationsList.clear();
		mobileOperationsList.addAll(filtrados);
	    }
	    filtrados = null;
	}
    }

    private void filterByOperationMTF(List<CatalogPsDTO> fixedOperationsList) {
	if (verifyNumberofOperations(fixedOperationsList)) {
	    List<CatalogPsDTO> filtrados = new ArrayList<>();
	    for (CatalogPsDTO item : fixedOperationsList) {
		if (item.getSubscriberType().equalsIgnoreCase(SuscriptionType.MTF.getCode())) {
		    filtrados.add(item);
		}
	    }
	    if(!filtrados.isEmpty()) {
		fixedOperationsList.clear();
		fixedOperationsList.addAll(filtrados);
	    }
	    filtrados = null;
	}
    }

    private boolean verifyNumberofOperations(List<?> operations) {
	return operations.size() > 1 ? Boolean.TRUE : Boolean.FALSE;
    }

    private List<MobileDTO> searchOperationsInPark(CommOperCollection<CommercialOperation> mobileOperations) {
	CommOperCollection<CommercialOperation> operations = new CommOperCollection<>();
	operations.addAll(mobileOperations);
	operations.filterCAPL();
	if (!operations.isEmpty()) {
	    return parkRules.getMasterMobile(operations);
	} else {
	    mobileOperations.obtainProvideAndPortaOperations();
	    return parkRules.getMasterMobile(mobileOperations);
	}
    }

    private List<CatalogPsDTO> searchOperationsInCatalog(CommOperCollection<CommercialOperation> fixedOperations) {
	List<CatalogPsDTO> list = new ArrayList<>();
	for (CommercialOperation operation : fixedOperations) {
	    CatalogPS catalog = findOperationByPS(operation.getSubscriber().getSubscriberId());
	    if (catalog != null) {
		CatalogPsDTO catalogPsDto = beanMap.catalogPSToDto(catalog);
		catalogPsDto.setServiceNumber(operation.getSubscriber().getServiceNumber());
		catalogPsDto.setSubscriberType(operation.getSubscriber().getType());
		list.add(catalogPsDto);
	    }
	    catalog = null;
	}
	return list;
    }

    private CatalogPS findOperationByPS(String subscriberId) {
	List<CatalogPS> psProducts = catalogPs.findAll();
	for (CatalogPS item : psProducts) {
	    if (item.getPackagePs() == Integer.parseInt(subscriberId)) {
		return item;
	    }
	}
	return null;
    }

    private CommOperCollection<CommercialOperation> filterMobileOperations(List<CommercialOperation> commercialOperations) {
	CommOperCollection<CommercialOperation> mobileOperations = new CommOperCollection<>(commercialOperations);
	mobileOperations.filterByProduct(SuscriptionType.MOBILE);
	return mobileOperations;
    }

    private CommOperCollection<CommercialOperation> filterFixedOperations(List<CommercialOperation> commercialOperations) {
	CommOperCollection<CommercialOperation> fixedOperations = new CommOperCollection<>(commercialOperations);
	fixedOperations.filterByProduct(SuscriptionType.FIXED);
	fixedOperations.filterCAPL();
	return fixedOperations;
    }

}
