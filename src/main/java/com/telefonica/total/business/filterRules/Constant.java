package com.telefonica.total.business.filterRules;

public class Constant {

    public static final String BE_1072 = "1072";
    public static final String FIJO = "Fijo";
    public static final String MOVIL = "Movil_%s";

    private Constant() {
    }

}
