package com.telefonica.total.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.StoredProcedureQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.telefonica.total.common.util.Constant;

@Repository
public class PlanFinancingDao {

    @Autowired
    private EntityManager em1,em2;

    @Transactional
    @SuppressWarnings("unchecked")
    public List<PlanFinancingEntity> getPlanFinancing(String pc_customer_type, String pc_customer_subtype, String pc_cod_fina, String pc_finan_value, String pc_name_fina) {
        List<PlanFinancingEntity> lstPlanFinancing = null;
        StoredProcedureQuery store = this.em1.createNamedStoredProcedureQuery(Constant.FU_NAME_LSTPLANFINANCING);
    	store.setParameter(1, pc_customer_type);
    	store.setParameter(2, pc_customer_subtype);
    	store.setParameter(3, pc_cod_fina);
    	store.setParameter(4, pc_finan_value);
    	store.setParameter(5, pc_name_fina);
    	

    	lstPlanFinancing = (List<PlanFinancingEntity>) store.getResultList();
    	this.em1.clear();
    	if (lstPlanFinancing != null) {
    	    return lstPlanFinancing;
    	} else {
    	    return new ArrayList<>();
    	}
        }
    
}
