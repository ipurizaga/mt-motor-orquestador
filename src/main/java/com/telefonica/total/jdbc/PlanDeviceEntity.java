package com.telefonica.total.jdbc;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;

import com.telefonica.total.common.util.Constant;

import java.util.ArrayList;

import lombok.Data;

@NamedStoredProcedureQuery(name = Constant.SP_NAME_LSTPLANDEVICE, procedureName = "PR_CALL_PR_LSPLEQMON", resultClasses = PlanDeviceEntity.class, parameters = {
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	
	@StoredProcedureParameter(mode = ParameterMode.IN, type = Character.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = Character.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = Integer.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	/*
	 * Se añade los parámetros codFina y finanValue para el cálculo de Financiamiento de Equipos, 
	 * debido a los cambios añadidos 
	 * de la Regla 18 en el paquete de la base de datos.
	 */
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.REF_CURSOR, type = Void.class) })

@Entity
@Data
public class PlanDeviceEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    private Integer   id;
    @Column(name = "PO_CODE")
    private String    poCode;
    @Column(name = "PO_ID")
    private String    poId;
    @Column(name = "PO_NAME_ES")
    private String    poNameEs;
    @Column(name = "PO_PRODUCT")
    private String    poProduct;
    @Column(name = "BO_CODE")
    private String    boCode;
    @Column(name = "BO_ID")
    private String    boId;
    @Column(name = "BO_NAME_ES")
    private String    boNameEs;
    @Column(name = "BO_PRICE")
    private Double    boPrice;
    @Column(name = "BO_CURRENCY")
    private String    boCurrency;
    @Column(name = "BO_PLAN_RANK")
    private String    boPlankRank;
    @Column(name = "BO_PLAN_GROUP")
    private String    boPlankGroup;
    @Column(name = "SAPID")
    private String    sapId;
    @Column(name = "CURRENCY_CODE")
    private String    currencyCode;
    @Column(name = "PRECIO_SUGERIDO")
    private Double    suggestedPrice;
    @Column(name = "COMM_6_MONTH_RATE")
    private Double    priceCommitment6;
    @Column(name = "COMM_12_MONTH_RATE")
    private Double    priceCommitment12;
    @Column(name = "COMM_18_MONTH_RATE")
    private Double    priceCommitment18;
    @Column(name = "COMM_24_MONTH_RATE")
    private Double    priceCommitment24;
    @Column(name = "COMM_36_MONTH_RATE")
    private Double    priceCommitment36;
    @Column(name = "BO_POSTPAGOFACMONTHSTODELAYDIS")
    private String    postPaidFacMonthDelay;
    @Column(name = "COSTO_ADICIONAL_PPF")
    private Double    additionalCostPpf;
    @Column(name = "DESCUENTO")
    private Double    discount;
    @Column(name = "MOVIL0_ESTITULAR")
    private String    isOwner;
    @Column(name = "DOCUMENTO")
    private String    document;
    @Column(name = "NOMBRE")
    private String    name;
    @Column(name = "FLAG_EARLYCAEQ")
    private Character flagEarlyCaeq;
    @Column(name = "PERMANENCIA")
    private Integer   permanency;
    @Column(name = "GVS")
    private String    gvs;
    @Column(name = "BO_POSTPAGOFACDISDURATION")
    private Integer   posPaidFacDisDuration;
    @Column(name = "BO_POSTPAGOFACDISPERCENTAGE")
    private Integer   postPaidFacDisPercentage;
    @Column(name = "FINANCIAL_PLAN_CODE")
    private String   financingPlanCode;
    @Column(name = "FINANCIAL_PLAN_DESCRIPTION")
    private String   financingPlanDesc;
    @Column(name = "PERCENTAGE")
    private Integer   percentage;
    @Column(name = "CUOTA_INICIAL")
    private Double   initialFee;
}
