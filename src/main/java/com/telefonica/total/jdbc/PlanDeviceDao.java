package com.telefonica.total.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.StoredProcedureQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.telefonica.total.common.util.Constant;

@Repository
public class PlanDeviceDao {

    @Autowired
    private EntityManager em;

    @Transactional
    @SuppressWarnings("unchecked")
    public List<PlanDeviceEntity> getPlanDevices(String mobile, String operation, String customerType, String customerSubType,
	    String storeProvince, String storeBrand, String storeId, String salesChannel, Character isOwner, String document, String gvs, Character isEarlyCaeq, Integer permanency, 
	    String isProvide, String bussiness, String codValor, String amount, String codFina, String finanValue, String nameFina, String plans, String devices) {

	List<PlanDeviceEntity> lstPlanDevice = null;

	StoredProcedureQuery store = this.em.createNamedStoredProcedureQuery(Constant.SP_NAME_LSTPLANDEVICE);
	store.setParameter(1, mobile);
	store.setParameter(2, operation);
	store.setParameter(3, customerType);
	store.setParameter(4, customerSubType);
	store.setParameter(5, storeProvince);
	store.setParameter(6, storeBrand);
	store.setParameter(7, storeId);
	store.setParameter(8, salesChannel);
	store.setParameter(9, isOwner);
	store.setParameter(10,document);
	store.setParameter(11, gvs);
	store.setParameter(12, isEarlyCaeq);
	store.setParameter(13, permanency);
	store.setParameter(14, isProvide);
	store.setParameter(15, bussiness);
	store.setParameter(16, codValor);
	store.setParameter(17, amount);
	/*
	 * Se añade los parámetros codFina y finanValue para el cálculo de Financiamiento de Equipos, 
	 * debido a los cambios añadidos 
	 * de la Regla 18 en el paquete de la base de datos.
	 */
	store.setParameter(18, codFina);
	store.setParameter(19, finanValue);
	store.setParameter(20, nameFina);
	store.setParameter(21, plans);
	store.setParameter(22, devices);

	lstPlanDevice = (List<PlanDeviceEntity>) store.getResultList();

	if (lstPlanDevice != null) {
	    return lstPlanDevice;
	} else {
	    return new ArrayList<>();
	}
    }

}
