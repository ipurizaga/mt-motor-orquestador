package com.telefonica.total.jdbc;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;

import com.telefonica.total.common.util.Constant;

import java.util.ArrayList;

import lombok.Data;

@NamedStoredProcedureQuery(name = Constant.FU_NAME_LSTPLANFINANCING, procedureName = "PR_CALL_FUN_COND_VENTA", resultClasses = PlanFinancingEntity.class, parameters = {
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	
	@StoredProcedureParameter(mode = ParameterMode.REF_CURSOR, type = Void.class) })

@Entity
@Data
public class PlanFinancingEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "FINANCIAL_PLAN_CODE")
    private String   financialPlanCode;
    @Column(name = "FINANCIAL_PLAN_DESCRIPTION")
    private String    financialPlanDescription;
    @Column(name = "PERCENTAGE")
    private Integer    percentage;
    @Column(name = "COD_FINA")
    private String    codFina;
    
}
