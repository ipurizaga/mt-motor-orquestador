package com.telefonica.total.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.StoredProcedureQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.telefonica.total.common.util.Constant;

@Repository
public class UpFrontAutomatizerDao {

    @Autowired
    private EntityManager em1,em2;

    @Transactional
    @SuppressWarnings("unchecked")
    public List<UpFrontAutomatizerCountedEntity> getUpFrontAutomatizerCounted(String pc_Operacion, String pc_customerType
	    , String pc_campania, Integer pn_codigo_ps, Double pn_paquete_renta, String pc_equipa_modem, 
	    String pc_equipa_deco, Integer pn_modalidad_pago) {
	
        List<UpFrontAutomatizerCountedEntity> lstUpFrontAutomatizer = null;
        StoredProcedureQuery store = this.em1.createNamedStoredProcedureQuery(Constant.FU_NAME_UPFRONT_COUNTED);
    	store.setParameter(1, pc_Operacion);
    	store.setParameter(2, pc_customerType);
    	store.setParameter(3, pc_campania);
    	store.setParameter(4, pn_codigo_ps);
    	store.setParameter(5, pn_paquete_renta);
    	store.setParameter(6, pc_equipa_modem);
    	store.setParameter(7, pc_equipa_deco);
    	store.setParameter(8, pn_modalidad_pago);
    	

    	lstUpFrontAutomatizer = (List<UpFrontAutomatizerCountedEntity>) store.getResultList();
    	this.em1.clear();
    	if (lstUpFrontAutomatizer != null) {
    	    return lstUpFrontAutomatizer;
    	} else {
    	    return new ArrayList<>();
    	}
   }
    
    @Transactional
    @SuppressWarnings("unchecked")
    public List<UpFrontAutomatizerFinancedEntity> getUpFrontAutomatizerFinanced(String pc_Operacion, String pc_customerType
	    , String pc_campania, Integer pn_codigo_ps, Double pn_paquete_renta, String pc_equipa_modem, 
	    String pc_equipa_deco, Integer pn_modalidad_pago) {
	
        List<UpFrontAutomatizerFinancedEntity> lstUpFrontAutomatizer = null;
        StoredProcedureQuery store = this.em1.createNamedStoredProcedureQuery(Constant.FU_NAME_UPFRONT_FINANCED);
    	store.setParameter(1, pc_Operacion);
    	store.setParameter(2, pc_customerType);
    	store.setParameter(3, pc_campania);
    	store.setParameter(4, pn_codigo_ps);
    	store.setParameter(5, pn_paquete_renta);
    	store.setParameter(6, pc_equipa_modem);
    	store.setParameter(7, pc_equipa_deco);
    	store.setParameter(8, pn_modalidad_pago);
    	

    	lstUpFrontAutomatizer = (List<UpFrontAutomatizerFinancedEntity>) store.getResultList();
    	this.em1.clear();
    	if (lstUpFrontAutomatizer != null) {
    	    return lstUpFrontAutomatizer;
    	} else {
    	    return new ArrayList<>();
    	}
   }
    
}
