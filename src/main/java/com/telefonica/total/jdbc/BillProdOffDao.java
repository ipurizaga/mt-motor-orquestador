package com.telefonica.total.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.exception.BusinessException;
import com.telefonica.total.model.CatalogBillingProduct;

@Repository
public class BillProdOffDao {

    @Autowired
    private DataSource dataSource;

    @Cacheable(value = "catalogBillingProduct", unless = "#result == null or #result.size() == 0")
    public List<CatalogBillingProduct> getAllBillProd() {
	Statement st = null;
	Connection conn = null;
	ResultSet rs = null;
	String query = "select * from CATALOG_BILLOFF_PRODOFF";
	List<CatalogBillingProduct> lstProd = new ArrayList<>();
	try {
	    conn = dataSource.getConnection();
	    st = conn.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
	    st.setFetchSize(1000);
	    rs = st.executeQuery(query);
	    while (rs.next()) {
		CatalogBillingProduct bill = new CatalogBillingProduct(rs.getInt(26), rs.getString(1), rs.getString(2), rs.getString(3),
			rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9),
			rs.getString(10), rs.getString(11), rs.getString(12), rs.getDouble(13), rs.getString(14), rs.getInt(15),
			rs.getString(16), rs.getString(17), rs.getString(18), rs.getInt(19), rs.getInt(20), rs.getInt(21), rs.getString(22),
			rs.getString(23), rs.getString(24), rs.getString(25));
		lstProd.add(bill);
	    }
	} catch (SQLException ex) {
	    throw new BusinessException(ex, Constant.GENERIC);
	} finally {
	    try {
		if (rs != null && st != null) {
		    rs.close();
		    st.close();
		    conn.close();
		}
	    } catch (SQLException e) {
		throw new BusinessException(e, Constant.GENERIC);
	    }
	}
	return lstProd;
    }

}
