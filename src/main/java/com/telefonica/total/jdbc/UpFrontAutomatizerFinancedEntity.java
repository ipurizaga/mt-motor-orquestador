package com.telefonica.total.jdbc;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;

import com.telefonica.total.common.util.Constant;

import java.util.ArrayList;

import lombok.Data;

@NamedStoredProcedureQuery(name = Constant.FU_NAME_UPFRONT_FINANCED, procedureName = "PCK_REGLAS_MT.PR_AUTOMATIZADOR_ALTAS", resultClasses = UpFrontAutomatizerFinancedEntity.class, parameters = {
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = Integer.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = Double.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class),
	@StoredProcedureParameter(mode = ParameterMode.IN, type = Integer.class),
	
	@StoredProcedureParameter(mode = ParameterMode.REF_CURSOR, type = Void.class) })

@Entity
@Data
public class UpFrontAutomatizerFinancedEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "CODIGO_PRODUCTO")
    private String    productCode;
    
}
