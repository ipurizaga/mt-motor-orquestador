package com.telefonica.total.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.enums.CustomerType;
import com.telefonica.total.enums.ProductType;
import com.telefonica.total.enums.QueryType;
import com.telefonica.total.exception.BusinessException;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.ReqData;
import com.telefonica.total.pojo.resp.RespData;

/**
 * 
 * @Author: jomapozo.
 * @Datecreation: 19 dic. 2018 15:04:05
 * @FileName: SuggestedProduct.java
 * @AuthorCompany: Telefonica
 * @version: 0.1
 * @Description: clase con evaluaciones del cliente donde se decide que flujo de operacion debe seguir.
 */
@Service
public class SuggestedProduct implements ISuggestedProduct {

    @Autowired
    private FlowFactory factory;

    @Override
    public RespData getSuggestedOffers(ReqData request) {
	RespData response = null;
	CustomerType customerType = CustomerType.getCustomType(request.getCustomer().getCustomerType());
	QueryType queryType = QueryType.getQueryType(request.getQueryType());
	ProductType productType = ProductType.getProducType(request.getCommOperationInfo().getProductType());
	List<CommercialOperation> lstCommOpers = request.getCommOperationInfo().getCommercialOpers();

	if (customerType != CustomerType.RESIDENCIAL) {
	    throw new BusinessException(Constant.BE_1051);
	}
	
	

	int consultType = TotalUtil.evaluateMT(lstCommOpers, queryType, productType);
	response = factory.getInstance(queryType, consultType, productType).executeLogicOperations(request);
	return response;
    }

}
