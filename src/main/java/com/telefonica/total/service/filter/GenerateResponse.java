package com.telefonica.total.service.filter;

import java.util.ArrayList;
import java.util.List;

import com.telefonica.total.business.filterRules.Constant;
import com.telefonica.total.dto.FilterRequestDataDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.pojo.req.Information;
import com.telefonica.total.pojo.resp.RespData;

public class GenerateResponse {

    public RespData execute(FilterRequestDataDTO data) {
	
	RespData response = new RespData();
	List<Information> additionalInfo = new ArrayList<>();	
	additionalInfo.add(Information.builder().code(Constant.FIJO).value(data.getFixedOperationsList().get(0).getServiceNumber()).build());
	int i = 1;
	for(MobileDTO mobiles : data.getMobileOperationsList()) {
	    additionalInfo.add(Information.builder().code(String.format(Constant.MOVIL, i)).value(mobiles.getMobileNumber()).build());
	    i++;
	}
	response.setAdditionalInfo(additionalInfo);
	return response;
    }
    
}
