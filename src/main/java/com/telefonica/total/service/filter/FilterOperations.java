package com.telefonica.total.service.filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.business.filterRules.CommercialOperationsFilter;
import com.telefonica.total.common.service.BaseMTService;
import com.telefonica.total.dto.FilterRequestDataDTO;
import com.telefonica.total.pojo.req.ReqData;
import com.telefonica.total.pojo.resp.RespData;

@Service
public class FilterOperations implements BaseMTService {

    @Autowired
    private CommercialOperationsFilter commercialOperationsFilter;

    @Override
    public RespData executeLogicOperations(ReqData request) {
	GenerateResponse response = new GenerateResponse();
	// ejecutar regla de filtro de operaciones comerciales
	FilterRequestDataDTO data = commercialOperationsFilter.execute(request);
	return response.execute(data);
    }

}
