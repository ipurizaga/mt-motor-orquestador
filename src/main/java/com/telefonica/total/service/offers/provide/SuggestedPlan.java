package com.telefonica.total.service.offers.provide;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.telefonica.total.business.GenerateOnlyPlansResponse;
import com.telefonica.total.business.IMastersParksRules;
import com.telefonica.total.business.deviceRules.riskFlag.IRiskFlag;
import com.telefonica.total.business.offersRules.assignDuplicateBonus.IAssignDuplicateBonus;
import com.telefonica.total.business.offersRules.automatizer.IDeterminateAutomatizerCode;
import com.telefonica.total.business.offersRules.availableOffers.IAvailableOffers;
import com.telefonica.total.business.offersRules.calculateArpa.ICalculateArpa;
import com.telefonica.total.business.offersRules.catalogAndClientValidations.ICatalogAndClientValidations;
import com.telefonica.total.business.offersRules.determinateActualFixedRent.IDeterminateActualFixedRent;
import com.telefonica.total.business.offersRules.determinateAdditionalBlocksToKeep.IDeterminateAdditionalBlocksToKeep;
import com.telefonica.total.business.offersRules.determinateAdditionalRentsToKeep.IDeterminateAdditionalRentsToKeep;
import com.telefonica.total.business.offersRules.determinateAppsQuantityByRentMonoProduct.IDeterminateAppsQuantityByRentMonoProduct;
import com.telefonica.total.business.offersRules.determinateBillingCycle.IDeterminateBillingCycle;
import com.telefonica.total.business.offersRules.determinateDiscountTemp.IDeterminateDiscountTemp;
import com.telefonica.total.business.offersRules.determinateDisposedFlag.IDeterminateDisposedFlag;
import com.telefonica.total.business.offersRules.determinatePresentBonusAndPresentDiscounts.IDeterminatePresentBonusAndPresentDiscounts;
import com.telefonica.total.business.offersRules.determinatePresentRents.IDeterminatePresentRents;
import com.telefonica.total.business.offersRules.determinatePromo.IDeterminatePromo;
import com.telefonica.total.business.offersRules.determinateTotalRentByOffer.IDeterminateTotalRentByOffer;
import com.telefonica.total.business.offersRules.determinatequantitymobile.IDeterminateQuantityMobile;
import com.telefonica.total.business.offersRules.equifaxPatch.IEquifaxPatch;
import com.telefonica.total.business.offersRules.equipmentValidations.IEquipmentValidations;
import com.telefonica.total.business.offersRules.fixedVelocityFilter.IFixedVelocityFilter;
import com.telefonica.total.business.offersRules.haveProvideRent199.IHaveProvideRent199;
import com.telefonica.total.business.offersRules.jumpsCalculation.IJumpsCalculation;
import com.telefonica.total.business.offersRules.mobileConsumptionFilter.IMobileConsumptionFilter;
import com.telefonica.total.business.offersRules.netflixFilter.INetflixFilter;
import com.telefonica.total.business.offersRules.orderAllProducts.IOrderAllProducts;
import com.telefonica.total.business.offersRules.productsRules.IProductsRules;
import com.telefonica.total.business.offersRules.removeRuleByRentFromProductOffers.IRemoveRule;
import com.telefonica.total.business.offersRules.salesOrRetentions.ISalesOrRetentions;
import com.telefonica.total.business.offersRules.separatedProductsTotalCost.ISeparatedProductsTotalCost;
import com.telefonica.total.business.offersRules.totalRentFilter.ITotalRentFilter;
import com.telefonica.total.business.offersRules.totalizationPriorizations.ITotalizationPriorizations;
import com.telefonica.total.business.offersRules.verifyNumberOfMobiles.IVerifyNumberOfMobiles;
import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.common.util.UtilCollections;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.PartialRequestDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.enums.OperationComm;
import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.exception.BusinessException;
import com.telefonica.total.generic.CommOperCollection;
import com.telefonica.total.jdbc.PlanDeviceDao;
import com.telefonica.total.jdbc.PlanDeviceEntity;
import com.telefonica.total.jdbc.PlanFinancingDao;
import com.telefonica.total.jdbc.PlanFinancingEntity;
import com.telefonica.total.model.BlackList;
import com.telefonica.total.model.CatalogDeco;
import com.telefonica.total.model.CatalogFouthDigitPPF;
import com.telefonica.total.model.CatalogModem;
import com.telefonica.total.model.ParamMovTotal;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.CommercialOperationInfo;
import com.telefonica.total.pojo.req.Customer;
import com.telefonica.total.pojo.req.Offer;
import com.telefonica.total.pojo.req.ReqData;
import com.telefonica.total.pojo.resp.RespData;
import com.telefonica.total.repository.BlackListRepo;
import com.telefonica.total.repository.CatalogDecoRepo;
import com.telefonica.total.repository.CatalogModemRepo;
import com.telefonica.total.repository.ParamMovTotalRepo;
import com.telefonica.total.service.offers.ASuggestedPlan;

@Service
public class SuggestedPlan extends ASuggestedPlan {

    @Autowired
    private IMastersParksRules masterParkRules;

    @Autowired
    private IProductsRules productRules;

    @Autowired
    @Qualifier("totalization")
    private ICatalogAndClientValidations catalogAndClientValidations;

    @Autowired
    private GenerateOnlyPlansResponse response;

    @Autowired
    private IEquifaxPatch equifaxPatch;

    @Autowired
    private IAssignDuplicateBonus assingDuplicateBonus;

    @Autowired
    private INetflixFilter netflixFilter;

    @Autowired
    private ICalculateArpa calculateArpa;

    @Autowired
    private IHaveProvideRent199 haveProvideRent199;

    @Autowired
    @Qualifier("totalization")
    private IRemoveRule removeRule;

    @Autowired
    private IJumpsCalculation jumpsCalculation;

    @Autowired
    private ISalesOrRetentions salesOrRetentions;

    @Autowired
    private ITotalRentFilter totalRentFilter;

    @Autowired
    private IFixedVelocityFilter fixedVelocityFilter;

    @Autowired
    private IMobileConsumptionFilter mobileConsumptionFilter;

    @Autowired
    private ITotalizationPriorizations totalizationPriorizations;

    @Autowired
    private IOrderAllProducts orderAllProducts;

    @Autowired
    @Qualifier("totalization")
    private IEquipmentValidations equipmentValidations;

    @Autowired
    @Qualifier("totalization")
    private IDeterminatePresentRents determinatePresentRents;

    @Autowired
    private IDeterminateAdditionalBlocksToKeep determinateAdditionalBlocksToKeep;

    @Autowired
    private IDeterminateDiscountTemp determinateDiscountTemp;
    
    @Autowired
    private IDeterminatePromo determinatePromoTemp;

    @Autowired
    private IDeterminateAdditionalRentsToKeep determinateAdditionalRentsToKepp;

    @Autowired
    private IDeterminateTotalRentByOffer determinateTotalRentByOffer;

    @Autowired
    private IDeterminateActualFixedRent determinateActualFixedRent;

    @Autowired
    private ISeparatedProductsTotalCost separatedProductsTotalCost;

    @Autowired
    private IDeterminateAppsQuantityByRentMonoProduct determinateAppsQuantityByRentMonoProduct;

    @Autowired
    private IDeterminatePresentBonusAndPresentDiscounts determinatePresentBonusAndPresentDiscounts;

    @Autowired
    private IAvailableOffers availableOffers;

    @Autowired
    private IVerifyNumberOfMobiles verifyNumberOfMobiles;

    @Autowired
    private BlackListRepo blackListRepo;
    
    @Autowired
    private CatalogModemRepo catalogModemRepo;
    
    @Autowired
    private CatalogDecoRepo catalogDecoRepo;
    
    @Autowired
    private ParamMovTotalRepo paramMovTotalRepo;

    @Autowired
    private IDeterminateBillingCycle determinateBillingCycle;

    @Autowired
    private IDeterminateDisposedFlag determinateDisposedFlag;

    @Autowired
    private IDeterminateAutomatizerCode determinateAutomatizerCode;
    
    @Autowired
    private IDeterminateQuantityMobile determinateQuantityMobile;
    
    @Autowired
    private PlanFinancingDao     planFinanDao;
    
    @Autowired
    private IRiskFlag riskFlag;

    @Override
    public RespData executeLogicOperations(ReqData request) {

	List<CommercialOperationInfo> commercialInfoList = new ArrayList<>();
	List<PartialRequestDTO> partialList = new ArrayList<>();

	CommercialOperationInfo commercialOperInfo = request.getCommOperationInfo();
	validateCaplBlackList(commercialOperInfo, request);
	Customer customer = request.getCustomer();
	List<Offer> optionalOffers = request.getOfferFilters();

	// PARCHE
	commercialInfoList.add(commercialOperInfo);
	
	if(commercialOperInfo.getCommercialOpers().get(0).getSubscriber()!=null) {
	    if(commercialOperInfo.getCommercialOpers().get(0).getOperation().equals(OperationComm.CAPL.getCodeDesc())) {
		determinateType(commercialOperInfo);
	    }
	}
	
	// FIN PARCHE
	List<CatalogModem> catalogModemList = catalogModemRepo.findAll();
	
	List<CatalogDeco> catalogDecoList = catalogDecoRepo.findAll();
	for (CommercialOperationInfo commercialInfo : commercialInfoList) {
	    /* A - Determinar ofertas segun reglas fijas */
	    PartialRequestDTO partial = getOffersByFixedRules(catalogDecoList, catalogModemList, commercialInfo, customer);
	    /* B - Validar si el cliente aplica a por lo menos una oferta de MT */
	    clientApplyToMT(partial);
	    /*
	     * C y D - Filtrar los planes de acuerdo al score de riesgo y el interes ,
	     * internamente realiza la regla de filtrar las listas de catalog Po-Bo.
	     */
	    filterRiskScoreAndInterest(partial, commercialInfo, optionalOffers);
	    /* E - Determinar condiciones de venta postpago facil */
	    if(activePostPayEasy()) {
		determinatePpfCondition(commercialInfo, partial);

	    }else {
		determinatePpfConditionFalse(commercialInfo, partial);

	    }
	    /* F - ¿Tiene financiamiento actual? */
	    Boolean currentFinancing = haveCurrentFinancing(commercialInfo, partial);
	    if (!currentFinancing) {
		/* G - Determinar condiciones de venta financiamiento */
		determinateSalesConditions(commercialInfo, partial);
	    }
	    
	    /* H - Determinar paquete beneficios */
	    packageBenefit(partial);

	    partialList.add(partial);
	}
	
	List<PlanDeviceEntity> planDeviceLst = new ArrayList<PlanDeviceEntity>();
	
		
	String sfinance = "11";	
	if(commercialOperInfo.getCommercialOpers().size() == 3) {
	
        	if(commercialOperInfo.getCommercialOpers().get(1).getCreditData().getFinanceValue()!=null && commercialOperInfo.getCommercialOpers().get(2).getCreditData().getFinanceValue()==null) {
        	    sfinance = "10";
        	}
        	
        	if(commercialOperInfo.getCommercialOpers().get(1).getCreditData().getFinanceValue()==null && commercialOperInfo.getCommercialOpers().get(2).getCreditData().getFinanceValue()!=null) {
        	    sfinance = "01";
        	}
        	
        	if(commercialOperInfo.getCommercialOpers().get(1).getCreditData().getFinanceValue()==null && commercialOperInfo.getCommercialOpers().get(2).getCreditData().getFinanceValue()==null) {
        	    sfinance = "00";
        	}
	
	}else {
    	    	if(commercialOperInfo.getCommercialOpers().get(1).getCreditData().getFinanceValue()!=null) {
        	    sfinance = "10";
        	}else {
        	    sfinance = "00";
        	}
	}
	
	int cantMoviles = 0,cantFinan = 0;
	List<String> financeValue = new ArrayList(), deviceOperation = new ArrayList();

	for(CommercialOperation op : commercialOperInfo.getCommercialOpers()) {
		if(op.getProduct().equals(Constant.MOBILE)) {
			cantMoviles++;
			if(op.getCreditData().getFinanceValue() != null && op.getDeviceOperation() != null) {
				cantFinan++;
				financeValue.add(op.getCreditData().getFinanceValue());
				deviceOperation.add(op.getDeviceOperation());
			}
		}
	}
	
	//System.out.println(request.getCustomer().getCustomerType());
	//System.out.println(this.validateString(request.getCustomer().getCustomerSubType()));
	//System.out.println(financeValue);
	//System.out.println(deviceOperation);
	
	CatalogFouthDigitPPF catalogFourth = null;
	catalogFourth = riskFlag.execute(request.getCommOperationInfo().getCommercialOpers());
	
	
	//LLamando función planes de financiamiento
		List<PlanFinancingEntity> planFinanLst1 = new ArrayList<PlanFinancingEntity>();
		List<PlanFinancingEntity> planFinanLst2 = new ArrayList<PlanFinancingEntity>();
		
		if(cantFinan==1) {
			try {
				planFinanLst1 = planFinanDao.getPlanFinancing(
						request.getCustomer().getCustomerType(), this.validateString(request.getCustomer().getCustomerSubType()), 
						catalogFourth.getFourthDigitScore().toString(), financeValue.get(0), deviceOperation.get(0));
			}catch(Exception e) {
				
			}
		}
		else if(cantFinan==2) {
			try {
				planFinanLst1 = planFinanDao.getPlanFinancing(
						request.getCustomer().getCustomerType(), this.validateString(request.getCustomer().getCustomerSubType()), 
						catalogFourth.getFourthDigitScore().toString(), financeValue.get(0), deviceOperation.get(0));
			}catch(Exception e) {
				
			}
			
			try {
				planFinanLst2 = planFinanDao.getPlanFinancing(
						request.getCustomer().getCustomerType(), this.validateString(request.getCustomer().getCustomerSubType()), 
						catalogFourth.getFourthDigitScore().toString(), financeValue.get(1), deviceOperation.get(1));
			}catch(Exception e) {
				
			}
		}
		
	return response.sendResponse(partialList, commercialOperInfo, planFinanLst1, planFinanLst2, cantFinan,sfinance/*, customer, optionalOffers.get(0)*/);
    }

    /***
     * Método que se encarga de realizar todas las validaciones del documento de
     * reglas fijas del motor de Movistar Total.
     * 
     * @param commercialInfo
     * @return
     */
    private PartialRequestDTO getOffersByFixedRules(List<CatalogDeco> catalogDecoList, List<CatalogModem> catalogModemList, CommercialOperationInfo commercialInfo, Customer customer) {
	/* Inputs */
    String coverage = commercialInfo.getCoverage() != null ? (commercialInfo.getCoverage().toLowerCase().equals("fiber")?"FTTH":commercialInfo.getCoverage()) : "";
	List<CommercialOperation> commercialOpeList = commercialInfo.getCommercialOpers();
	FixedDTO fixedParkDto = masterParkRules.getMasterFixed(commercialOpeList);
	List<ProductFixedDTO> catalogProductFixedDto = productRules.getProductFixed();
	List<ProductFixedDTO> catalogProductLMA = productRules.getProductFixedLMA();
	List<ProductFixedDTO> enabledOffers = productRules.getEnabledOffers(catalogProductFixedDto);	
	List<ProductFixedDTO> offerLMA = productRules.getEnabledOffersLMA(catalogProductLMA);
	List<ParamMovTotal> listParam = paramMovTotalRepo.findAll();
	Character cobIntHfc = fixedParkDto.getHfcCobInternet() != null ? fixedParkDto.getHfcCobInternet() : '0';
	//Llamando funcion para validar el movil 2 si está vacío
		TotalUtil.valMobile2(enabledOffers);
	//Llamando funcion para añadir costo y cuotas de instalación
		//TotalUtil.updateCostFeeInstall(enabledOffers, listParam);
		
	determinateQuantityMobile.execute(commercialOpeList);
	List<MobileDTO> mobileDevices = masterParkRules.getMasterMobile(commercialOpeList);
	verifyNumberOfMobiles.execute(fixedParkDto, mobileDevices);
	boolean fixedProvideOrPortability = UtilCollections.verifyProvideOrPortability(commercialInfo.getCommercialOpers(),
		SuscriptionType.FIXED);
	boolean movilecaplOrPortability = UtilCollections.verifyCaplOrPortability(commercialInfo.getCommercialOpers(),
		SuscriptionType.MOBILE);
	
	/***************************************************************************************************/
	
	/* Outputs */
	List<ProductFixedDTO> priorizationList = new ArrayList<>();

	/* Rules: */
	/* 0. Parche equifax */
	equifaxPatch.execute(mobileDevices, commercialInfo);
	/* 1. Validaciones catalogo y cliente */
	List<ProductFixedDTO> filteredProductList = catalogAndClientValidations.executeLMA(fixedParkDto, enabledOffers, offerLMA, mobileDevices,
		commercialInfo, fixedProvideOrPortability);
	/* 2. Asignacion de Bono Duplica a las ofertas ofrecidas */
	assingDuplicateBonus.execute(fixedParkDto, filteredProductList, movilecaplOrPortability, commercialOpeList);
	/* 3. Filtro de Netflix segun renta y salesChannel */
	netflixFilter.execute(fixedParkDto, filteredProductList, commercialInfo);
	/* 4. Calculo del ARPA */
	calculateArpa.execute(fixedParkDto, mobileDevices);
	/* 5. Tenencia Oferta Renta 199 */
	haveProvideRent199.execute(filteredProductList, fixedParkDto);
	/* Remover reglas */
	removeRule.execute(filteredProductList, fixedParkDto);
	/* 6. Calculo de saltos */
	jumpsCalculation.execute(fixedParkDto, filteredProductList, mobileDevices, commercialOpeList, customer);
	/* 7. Ventas o Retenciones */
	salesOrRetentions.execute(commercialInfo);
	/* 8. Filtro Renta Total */
	totalRentFilter.execute(filteredProductList);
	/* 9. Filtro Velocidad Fija */
	if (!fixedProvideOrPortability) {
	    fixedVelocityFilter.execute(fixedParkDto, filteredProductList);
	}
	/* 10. Filtro Consumo Mobil */
	mobileConsumptionFilter.execute(filteredProductList, mobileDevices);
	/* 11. Envio de flag de desposicionado */
	determinateDisposedFlag.execute(fixedParkDto, commercialOpeList, mobileDevices);
	
	/* 14 - 18. validaciones de equipos decos y modems */
	equipmentValidations.execute(catalogDecoList, catalogModemList, filteredProductList, fixedParkDto, commercialInfo, fixedProvideOrPortability);
	
	/* 12. Priorizaciones Downsell y Upsell */
	priorizationList.addAll(totalizationPriorizations.execute(fixedParkDto, filteredProductList, commercialOpeList, coverage, listParam));
	
	/* 13. Orden de productos */
	orderAllProducts.execute(priorizationList, filteredProductList, coverage, cobIntHfc, fixedParkDto);
	
	onlyThreeOffersDownsell(priorizationList);
	
	
	
	showMigraTechnology(commercialInfo, fixedParkDto, priorizationList);
	
	TotalUtil.validateCostFeeInstall(fixedParkDto, commercialInfo, priorizationList, listParam);
	
	if (!fixedProvideOrPortability) {
	    /* 19. Determinar y calcular las rentas adicionales actuales */
	    determinatePresentRents.execute(fixedParkDto);
	    /* 20. Determinar bloques adicionales a mantener */
	    determinateAdditionalBlocksToKeep.execute(priorizationList, fixedParkDto);
	}
	
	//if(!validateUpFront(commercialInfo)) {
	
        	// validación para filtrar por paquete categoria para aplicar el descuento MT
        	if(!(fixedParkDto.getPackageCategory().equals("Duos TV+BA")||(fixedParkDto.getPackageCategory().equals("Trios DTH")||(fixedParkDto.getPackageCategory().equals("Trios CATV"))))) {
        	/* 21. Calcular Descuentos Temporales */
        	determinateDiscountTemp.execute(commercialInfo, customer, priorizationList, !validateUpFront(commercialInfo));
        	
        	}
	
	//}
	/* 21. Calcular Promos Temporales */
	determinatePromoTemp.execute(commercialInfo, customer, priorizationList);
	/* 22. Determinar y calcular rentas adicionales a mantener */
	determinateAdditionalRentsToKepp.execute(priorizationList, fixedParkDto);
	/* 23. Calcular Renta Total */
	determinateTotalRentByOffer.execute(priorizationList);
	/* 24. Calcular Renta Fija actual */
	determinateActualFixedRent.execute(fixedParkDto);
	/* 25. Calcular el Costo Total Productos separados */
	separatedProductsTotalCost.execute(priorizationList);
	/* 26. Calcular la cantidad de apps por Renta */
	determinateAppsQuantityByRentMonoProduct.execute(mobileDevices);
	/* 27. Determinar los bonos y descuentos actuales */
	determinatePresentBonusAndPresentDiscounts.execute(customer, commercialOpeList, mobileDevices);
	/* 28. Calculo de la fecha de financiamiento */
	determinateBillingCycle.execute(fixedParkDto, commercialOpeList);
	/* 29. Automatizador */
	validateBeach(commercialInfo);
	determinateAutomatizerCode.execute(commercialInfo, customer, priorizationList);
	/* 30. Enviar Ofertas Disponibles */
	return availableOffers.execute(fixedParkDto, priorizationList, mobileDevices);

    }

    /**
     * Método que se encarga de validar si el documento de identidad esta en lista
     * negra.
     * 
     * @param document
     * @return
     */
    private Boolean filterBlackList(String document) {
	BlackList blackList = blackListRepo.findByDni(document);
	if (blackList != null) {
	    return Boolean.TRUE;
	} else {
	    return Boolean.FALSE;
	}
    }

    private void validateCaplBlackList(CommercialOperationInfo commercialInfo, ReqData request) {
	List<CommercialOperation> commercialOpeList = commercialInfo.getCommercialOpers();
	CommOperCollection<CommercialOperation> reqLst = new CommOperCollection<>(commercialOpeList);
	if (CollectionUtils.isNotEmpty(reqLst) && !request.getCustomer().getIsEmployee()
		&& filterBlackList(request.getCustomer().getDocumentNumber())) {
	    throw new BusinessException(Constant.BE_1059);
	}
    }
    
    private String validateString(String text) {
	if (null == text) {
	    return StringUtils.EMPTY;
	} else {
	    return text;
	}
    }

}
