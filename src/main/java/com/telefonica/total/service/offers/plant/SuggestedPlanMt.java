package com.telefonica.total.service.offers.plant;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.telefonica.total.business.GenerateOnlyPlansResponse;
import com.telefonica.total.business.IMastersParksRules;
import com.telefonica.total.business.deviceRules.riskFlag.IRiskFlag;
import com.telefonica.total.business.offersRules.actualProductFilterEnabledOffers.IActualProductFilterEnabledOffers;
import com.telefonica.total.business.offersRules.assignDuplicateBonus.IAssignDuplicateBonus;
import com.telefonica.total.business.offersRules.availableOffers.IAvailableOffers;
import com.telefonica.total.business.offersRules.catalogAndClientValidations.ICatalogAndClientValidations;
import com.telefonica.total.business.offersRules.determinateActualFixedRent.IDeterminateActualFixedRent;
import com.telefonica.total.business.offersRules.determinateAdditionalBlocksToKeep.IDeterminateAdditionalBlocksToKeep;
import com.telefonica.total.business.offersRules.determinateAdditionalRentsToKeep.IDeterminateAdditionalRentsToKeep;
import com.telefonica.total.business.offersRules.determinateAppsQuantityByRentMonoProduct.IDeterminateAppsQuantityByRentMonoProduct;
import com.telefonica.total.business.offersRules.determinatePresentBonusAndPresentDiscounts.IDeterminatePresentBonusAndPresentDiscounts;
import com.telefonica.total.business.offersRules.determinatePresentRents.IDeterminatePresentRents;
import com.telefonica.total.business.offersRules.determinatePromo.IDeterminatePromo;
import com.telefonica.total.business.offersRules.determinateTotalRentByOffer.IDeterminateTotalRentByOffer;
import com.telefonica.total.business.offersRules.determinatequantitymobile.IDeterminateQuantityMobile;
import com.telefonica.total.business.offersRules.equifaxPatch.IEquifaxPatch;
import com.telefonica.total.business.offersRules.equipmentValidations.IEquipmentValidations;
import com.telefonica.total.business.offersRules.fixedVelocityFilter.IFixedVelocityFilter;
import com.telefonica.total.business.offersRules.jumpsCalculation.IJumpsCalculation;
import com.telefonica.total.business.offersRules.mobileConsumptionFilter.IMobileConsumptionFilter;
import com.telefonica.total.business.offersRules.netflixFilter.INetflixFilter;
import com.telefonica.total.business.offersRules.obtainActualProductDisabledOffers.IObtainActualProductDisabledOffers;
import com.telefonica.total.business.offersRules.obtainAndValidatePoFromRequest.IObtainAndValidatePoFromRequest;
import com.telefonica.total.business.offersRules.orderAllProducts.IOrderAllProducts;
import com.telefonica.total.business.offersRules.productsRules.IProductsRules;
import com.telefonica.total.business.offersRules.removeRuleByRentFromProductOffers.IRemoveRule;
import com.telefonica.total.business.offersRules.salesOrRetentions.ISalesOrRetentions;
import com.telefonica.total.business.offersRules.separatedProductsTotalCost.ISeparatedProductsTotalCost;
import com.telefonica.total.business.offersRules.totalRentFilter.ITotalRentFilter;
import com.telefonica.total.business.offersRules.upSellPriorizationsPlantMT.IUpSellPriorizationsPlantMT;
import com.telefonica.total.business.offersRules.upSellPriorizationsSupposedPlantMT.IUpSellPriorizationsSupposedPlantMT;
import com.telefonica.total.business.offersRules.validatePsAndBo.IValidatePsAndBo;
import com.telefonica.total.business.offersRules.verifyNumberOfMobiles.IVerifyNumberOfMobiles;
import com.telefonica.total.business.offersRules.verifyPsPackageAndSubscriberId.IVerifyPsPackageAndSubscriberId;
import com.telefonica.total.common.util.UtilCollections;
import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.PartialRequestDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.exception.BusinessException;
import com.telefonica.total.jdbc.PlanFinancingDao;
import com.telefonica.total.jdbc.PlanFinancingEntity;
import com.telefonica.total.model.CatalogDeco;
import com.telefonica.total.model.CatalogFouthDigitPPF;
import com.telefonica.total.model.CatalogModem;
import com.telefonica.total.model.ParamMovTotal;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.CommercialOperationInfo;
import com.telefonica.total.pojo.req.Customer;
import com.telefonica.total.pojo.req.Offer;
import com.telefonica.total.pojo.req.ReqData;
import com.telefonica.total.pojo.resp.RespData;
import com.telefonica.total.repository.CatalogDecoRepo;
import com.telefonica.total.repository.CatalogModemRepo;
import com.telefonica.total.repository.ParamMovTotalRepo;
import com.telefonica.total.service.offers.ASuggestedPlan;

@Service
public class SuggestedPlanMt extends ASuggestedPlan {

    @Autowired
    private IMastersParksRules masterParkRules;

    @Autowired
    private IProductsRules productRules;

    @Autowired
    @Qualifier("plantMt")
    private ICatalogAndClientValidations catalogAndClientValidations;

    @Autowired
    private GenerateOnlyPlansResponse response;

    @Autowired
    private IEquifaxPatch equifaxPatch;

    @Autowired
    private IVerifyPsPackageAndSubscriberId verifyPsPackageAndSubscriberId;

    @Autowired
    private IAssignDuplicateBonus assingDuplicateBonus;

    @Autowired
    private INetflixFilter netflixFilter;

    @Autowired
    @Qualifier("plantMt")
    private IRemoveRule removeRule;

    @Autowired
    private IJumpsCalculation jumpsCalculation;

    @Autowired
    private ISalesOrRetentions salesOrRetentions;

    @Autowired
    private ITotalRentFilter totalRentFilter;

    @Autowired
    private IFixedVelocityFilter fixedVelocityFilter;

    @Autowired
    private IMobileConsumptionFilter mobileConsumptionFilter;

    @Autowired
    private IObtainAndValidatePoFromRequest obtainAndValidatePoFromRequest;

    @Autowired
    private IOrderAllProducts orderAllProducts;
    
    @Autowired
    private IDeterminatePromo determinatePromoTemp;

    @Autowired
    @Qualifier("plantMt")
    private IEquipmentValidations equipmentValidations;

    @Autowired
    @Qualifier("plantMt")
    private IDeterminatePresentRents determinatePresentRents;

    @Autowired
    private IDeterminateAdditionalBlocksToKeep determinateAdditionalBlocksToKeep;

    @Autowired
    private IDeterminateAdditionalRentsToKeep determinateAdditionalRentsToKepp;

    @Autowired
    private IDeterminateTotalRentByOffer determinateTotalRentByOffer;

    @Autowired
    private IDeterminateActualFixedRent determinateActualFixedRent;

    @Autowired
    private ISeparatedProductsTotalCost separatedProductsTotalCost;

    @Autowired
    private IDeterminateAppsQuantityByRentMonoProduct determinateAppsQuantityByRentMonoProduct;

    @Autowired
    private IDeterminatePresentBonusAndPresentDiscounts determinatePresentBonusAndPresentDiscounts;

    @Autowired
    private IAvailableOffers availableOffers;

    @Autowired
    private IVerifyNumberOfMobiles verifyNumberOfMobiles;
    
    @Autowired
    private ParamMovTotalRepo paramMovTotalRepo;
    
    @Autowired
    private CatalogModemRepo catalogModemRepo;
    
    @Autowired
    private CatalogDecoRepo catalogDecoRepo;

    @Autowired
    private IValidatePsAndBo validatePsAndBo;
    
    @Autowired
    private IDeterminateQuantityMobile determinateQuantityMobile;

    @Autowired
    private IActualProductFilterEnabledOffers actualProductFilterEnabledOffers;

    @Autowired
    private IUpSellPriorizationsPlantMT upSellPriorizationsPlantMt;

    @Autowired
    private IObtainActualProductDisabledOffers obtainActualProductDisabledOffers;

    @Autowired
    private IUpSellPriorizationsSupposedPlantMT upSellPriorizationsSupposedPlantMt;
    
    @Autowired
    private PlanFinancingDao planFinanDao;
    
    @Autowired
    private IRiskFlag riskFlag;

    @Override
    public RespData executeLogicOperations(ReqData request) {

	List<CommercialOperationInfo> commercialInfoList = new ArrayList<>();
	List<PartialRequestDTO> partialList = new ArrayList<>();

	CommercialOperationInfo commercialOperInfo = request.getCommOperationInfo();
	Customer customer = request.getCustomer();
	List<Offer> optionalOffers = request.getOfferFilters();	

	// PARCHE
	commercialInfoList.add(commercialOperInfo);
	
	String parrilla="";
	
	List<CatalogModem> catalogModemList = catalogModemRepo.findAll();
	
	List<CatalogDeco> catalogDecoList = catalogDecoRepo.findAll();
	
	String modemEquipmentOfferCopy = null;
	
	// FIN PARCHE
	for (CommercialOperationInfo commercialInfo : commercialInfoList) {
	    /* A - Determinar ofertas segun reglas fijas */
	    
	    
	    PartialRequestDTO partial = getOffersByFixedRules(catalogDecoList, catalogModemList, commercialInfo, customer, modemEquipmentOfferCopy);
	    /* B - Validar si el cliente aplica a por lo menos una oferta de MT */
	    clientApplyToMT(partial);
	    
	    ProductFixedDTO pfd = getCurrentProductOffer(partial.getResultantProductFixedList());
	    
	    if(pfd == null) {
		
		parrilla = "";
		
	    }else {
	    
		parrilla = pfd.getMerchandisingType();
	    
	    }
	    
	    /*if(partial.getCustomerInformation().getPriorizationFlux() == 3) {
	    
		
		if(commercialInfo.getCommercialOpers().size() == 3) {
		
		    findCurrentOffer2Mobile(partial, commercialInfo, optionalOffers);
		
		}else if(commercialInfo.getCommercialOpers().size() == 2) {
		    
		    findCurrentOffer1Mobile(partial, commercialInfo, optionalOffers);
		    
		}
	    
	    }*/
	    
	    /**/
	    /*
	     * C y D - Filtrar los planes de acuerdo al score de riesgo y el interes ,
	     * internamente realiza la regla de filtrar las listas de catalog Po-Bo.
	     */
	    filterRiskScoreAndInterest(partial, commercialInfo, optionalOffers);
	    /* E - Determinar condicioenes de venta postpago facil */
	    if(activePostPayEasy()) {
		determinatePpfCondition(commercialInfo, partial);
	    }else {
		determinatePpfConditionFalse(commercialInfo, partial);
	    }
	    /* F - ¿Tiene financiamiento actual? */
	    Boolean currentFinancing = haveCurrentFinancing(commercialInfo, partial);
	    if (!currentFinancing) {
		/* G - Determinar condiciones de venta financiamiento */
		determinateSalesConditions(commercialInfo, partial);
	    }
	    
	    /* H - Determinar paquete beneficios */
	    packageBenefit(partial);

	    partialList.add(partial);
	}
	
	
	/* Construcción de el Response */
	String sfinance = "11";
	
	if(commercialOperInfo.getCommercialOpers().size() == 3) {
	
        	if(commercialOperInfo.getCommercialOpers().get(1).getCreditData().getFinanceValue()!=null && commercialOperInfo.getCommercialOpers().get(2).getCreditData().getFinanceValue()==null) {
        	    sfinance = "10";
        	}
        	
        	if(commercialOperInfo.getCommercialOpers().get(1).getCreditData().getFinanceValue()==null && commercialOperInfo.getCommercialOpers().get(2).getCreditData().getFinanceValue()!=null) {
        	    sfinance = "01";
        	}
        	
        	if(commercialOperInfo.getCommercialOpers().get(1).getCreditData().getFinanceValue()==null && commercialOperInfo.getCommercialOpers().get(2).getCreditData().getFinanceValue()==null) {
        	    sfinance = "00";
        	}
	
	}else {
    	    	if(commercialOperInfo.getCommercialOpers().get(1).getCreditData().getFinanceValue()!=null) {
        	    sfinance = "10";
        	}else {
        	    sfinance = "00";
        	}
	}
	
	int cantMoviles = 0,cantFinan = 0;
	List<String> financeValue = new ArrayList(), deviceOperation = new ArrayList();

	for(CommercialOperation op : commercialOperInfo.getCommercialOpers()) {
		if(op.getProduct().equals(Constant.MOBILE)) {
			cantMoviles++;
			if(op.getCreditData().getFinanceValue() != null && op.getDeviceOperation() != null) {
				cantFinan++;
				financeValue.add(op.getCreditData().getFinanceValue());
				deviceOperation.add(op.getDeviceOperation());
			}
		}
	}
	/*
	for(CommercialOperation op : request.getCommOperationInfo().getCommercialOpers()) {
		if(op.getProduct().equals(SuscriptionType.MOBILE.getCode())) {
			//if(op.getCreditData().getCreditScore().substring(3) != null) {
			if(op.getCreditData().getFinanceValue() != null) {
				financeValue.add(op.getCreditData().getFinanceValue());
			}
		
		}
	}
	
	for(CommercialOperation op : request.getCommOperationInfo().getCommercialOpers()) {
		if(op.getProduct().equals(SuscriptionType.MOBILE.getCode())) {
			//if(op.getCreditData().getCreditScore().substring(3) != null) {
			if(op.getDeviceOperation() != null) {
				deviceOperation.add(op.getDeviceOperation());
			}
		
		}
	}*/
	
	
	CatalogFouthDigitPPF catalogFourth = null;
	catalogFourth = riskFlag.execute(request.getCommOperationInfo().getCommercialOpers());
	
	
	//LLamando función planes de financiamiento
		List<PlanFinancingEntity> planFinanLst1 = new ArrayList<PlanFinancingEntity>();
		List<PlanFinancingEntity> planFinanLst2 = new ArrayList<PlanFinancingEntity>();
		
		if(cantFinan==1) {
			try {
				planFinanLst1 = planFinanDao.getPlanFinancing(
						request.getCustomer().getCustomerType(), this.validateString(request.getCustomer().getCustomerSubType()),catalogFourth.getFourthDigitScore().toString(), 
						financeValue.get(0), deviceOperation.get(0));
			}catch(Exception e) {
				
			}
		}
		else if(cantFinan==2) {
			try {
				planFinanLst1 = planFinanDao.getPlanFinancing(
						request.getCustomer().getCustomerType(), this.validateString(request.getCustomer().getCustomerSubType()),catalogFourth.getFourthDigitScore().toString(), 
						financeValue.get(0), deviceOperation.get(0));
			}catch(Exception e) {
				
			}
			
			try {
				planFinanLst2 = planFinanDao.getPlanFinancing(
						request.getCustomer().getCustomerType(), this.validateString(request.getCustomer().getCustomerSubType()),catalogFourth.getFourthDigitScore().toString(), 
						financeValue.get(1), deviceOperation.get(1));
			}catch(Exception e) {
				
			}
		}
		
	
	
	
	return response.sendResponseMT(partialList, commercialOperInfo, planFinanLst1, planFinanLst2, cantFinan, sfinance, parrilla, modemEquipmentOfferCopy);
    }

    /***
     * Método que se encarga de realizar todas las validaciones del documento de
     * reglas fijas del motor de Movistar Total.
     * 
     * @param commercialInfo
     * @return
     */
    private PartialRequestDTO getOffersByFixedRules(List<CatalogDeco> catalogDecoList, List<CatalogModem> catalogModemList, CommercialOperationInfo commercialInfo, Customer customer, String modemEquipmentOfferCopy) {
	/* Inputs */
	List<CommercialOperation> commercialOpeList = commercialInfo.getCommercialOpers();
	FixedDTO fixedParkDto = masterParkRules.getMasterFixed(commercialOpeList);
	
	FixedDTO fixedParkDtoActual = masterParkRules.getMasterFixed(commercialOpeList);
	
	determinateCoverage(commercialInfo, fixedParkDto);
	
	
	List<ProductFixedDTO> catalogProductFixedDto = productRules.getProductFixed();
	List<ProductFixedDTO> catalogProductFixedDtoActual = productRules.getProductFixed();
	List<ProductFixedDTO> catalogProductLMA = productRules.getProductFixedLMA();
	List<ProductFixedDTO> enabledOffers = productRules.getEnabledOffers(catalogProductFixedDto);
	List<ProductFixedDTO> offerLMA = productRules.getEnabledOffersLMA(catalogProductLMA);
	List<ParamMovTotal> listParam = paramMovTotalRepo.findAll();
	ProductFixedDTO product_current = new ProductFixedDTO();
	String coverage = commercialInfo.getCoverage() != null ? (commercialInfo.getCoverage().toLowerCase().equals("fiber")?"FTTH":commercialInfo.getCoverage()) : "";
	Character cobIntHfc = fixedParkDto.getHfcCobInternet() != null ? fixedParkDto.getHfcCobInternet() : '0';
	//Llamando funcion para validar el movil 2 si está vacío
	TotalUtil.valMobile2(enabledOffers);
	//determinateQuantityMobile.executeMt(commercialOpeList,enabledOffers);
	//Llamando funcion para validar y añadir costo y cuotas de instalación
	//TotalUtil.validateCostFeeInstall(fixedParkDto, commercialInfo, enabledOffers, listParam);
	
	List<MobileDTO> mobileDevices = masterParkRules.getMasterMobile(commercialOpeList);
	verifyNumberOfMobiles.execute(fixedParkDto, mobileDevices);
	verifyNumberOfMobiles.execute(fixedParkDtoActual, mobileDevices);
	ProductFixedDTO inhabilitatedActualOffer = null;
	boolean movilecaplOrPortability = UtilCollections.verifyCaplOrPortability(commercialInfo.getCommercialOpers(),
		SuscriptionType.MOBILE);
	/* Outputs */
	List<ProductFixedDTO> priorizationList = new ArrayList<>();
	List<ProductFixedDTO> priorizationListActual = new ArrayList<>();
	/* Parche equifax */
	equifaxPatch.execute(mobileDevices, commercialInfo);
	/* 0. Validar Ps y subcriberId */
	// boolean distinctPS =
	// fixedRules.verifyPSPackageAndSubscriberID(commercialOpeList, fixedParkDto,
	// catalogProductFixedDto);// distintos
	boolean distinctPS = verifyPsPackageAndSubscriberId.execute(commercialOpeList, fixedParkDto, catalogProductFixedDto);// distintos
	boolean distinctPSActual = verifyPsPackageAndSubscriberId.execute(commercialOpeList, fixedParkDtoActual, catalogProductFixedDtoActual);// distintos
	// -> true
	/* 1. Validaciones catalogo y cliente y ¿Tiene ofertas disponibles? */
	List<ProductFixedDTO> filteredProductList = catalogAndClientValidations.executeLMAMT(fixedParkDto, enabledOffers, offerLMA, mobileDevices,
		commercialInfo, false, product_current);
	
	/*fixedParkDto.setBoIdRequest(null);*/
	
	List<ProductFixedDTO> filteredProductListActual = catalogAndClientValidations.executeLMAMT(fixedParkDtoActual, enabledOffers, offerLMA, mobileDevices,
		commercialInfo, false, product_current);
	
	/* 2. Asignacion de Bono Duplica */
	
	assingDuplicateBonus.execute(fixedParkDto, filteredProductList, movilecaplOrPortability, commercialOpeList);
	
	// replicando
	//assingDuplicateBonus.execute(fixedParkDtoActual, filteredProductListActual, movilecaplOrPortability, commercialOpeList);
	
	//jumpsCalculation.execute(fixedParkDtoActual, filteredProductListActual, mobileDevices, commercialOpeList, customer);
	
	/* 3. Calculo de saltos */
	jumpsCalculation.execute(fixedParkDto, filteredProductList, mobileDevices, commercialOpeList, customer);
	/* 4. Ventas o Retenciones */
	salesOrRetentions.execute(commercialInfo);
	/* 5. Filtro de Netflix */
	netflixFilter.execute(fixedParkDto, filteredProductList, commercialInfo);
	
	//netflixFilter.execute(fixedParkDtoActual, filteredProductListActual, commercialInfo);
	
	//totalRentFilter.execute(filteredProductListActual);
	
	//fixedVelocityFilter.execute(fixedParkDtoActual, filteredProductListActual);
	
	//mobileConsumptionFilter.execute(filteredProductListActual, mobileDevices);
	
	//obtainAndValidatePoFromRequest.execute(fixedParkDtoActual, mobileDevices, catalogProductFixedDtoActual);
	
	//validatePsAndBo.executeMt(fixedParkDtoActual, mobileDevices, catalogProductFixedDtoActual);
	
	/* 6. Filtro Renta Total */
	totalRentFilter.execute(filteredProductList);
	/* 7. Filtro Velocidad Fija */
	fixedVelocityFilter.execute(fixedParkDto, filteredProductList);
	/* 8. Filtro Consumo Móvil */
	mobileConsumptionFilter.execute(filteredProductList, mobileDevices);
	/* 9. Flujos priorizacion y migracion de parrilas */
	obtainAndValidatePoFromRequest.execute(fixedParkDto, mobileDevices, catalogProductFixedDto);
	// verificar si las bo de cada operacion comercial son iguales y si son de
	// oferta habilitada o deshabilitada.
	validatePsAndBo.executeMt(fixedParkDto, mobileDevices, catalogProductFixedDto);
	
	//List<ProductFixedDTO> actProducts = new ArrayList<ProductFixedDTO>();
	List<ProductFixedDTO> actualProducts = new ArrayList<ProductFixedDTO>();
	
	if (fixedParkDto.getPriorizationFlux() == 1) {
	    // si son habilitadas
	    actualProductFilterEnabledOffers.executeMt(fixedParkDto, filteredProductList, commercialInfo);
	    //actualProductFilterEnabledOffers.executeMt(fixedParkDtoActual, filteredProductListActual, commercialInfo);
	    haveUbigeo(filteredProductList, fixedParkDto);
	    //haveUbigeo(filteredProductListActual, fixedParkDtoActual);
	    getCoverage(commercialInfo, fixedParkDto);
	    //getCoverage(commercialInfo, fixedParkDtoActual);
	    priorizationList.addAll(upSellPriorizationsPlantMt.executeMt(filteredProductList, null, catalogProductFixedDto, fixedParkDto, commercialInfo, actualProducts));
	    //priorizationListActual.addAll(upSellPriorizationsPlantMt.executeMt(filteredProductListActual, null, catalogProductFixedDtoActual, fixedParkDtoActual, commercialInfo, actProducts));
	} else if (fixedParkDto.getPriorizationFlux() == 2) {
	    // si son inhabilitadas
	    inhabilitatedActualOffer = obtainActualProductDisabledOffers.executeMt(fixedParkDto, catalogProductFixedDto,
		    filteredProductList, commercialInfo);
	    haveUbigeo(filteredProductList, fixedParkDto);
	    getCoverage(commercialInfo, fixedParkDto);
	    priorizationList.addAll(upSellPriorizationsPlantMt.executeMt(filteredProductList, inhabilitatedActualOffer,
		    catalogProductFixedDto, fixedParkDto, commercialInfo, actualProducts));
	} else if (fixedParkDto.getPriorizationFlux() == 3) {
	    /* 11. S2-H11 Priorizaciones Oferta Inconsistentes */
	    haveUbigeo(filteredProductList, fixedParkDto);
	    getCoverage(commercialInfo, fixedParkDto);
	    priorizationList
		    .addAll(upSellPriorizationsSupposedPlantMt.executeMt(filteredProductList, catalogProductFixedDto, fixedParkDto, commercialInfo, actualProducts));
	}
	
	//haveUbigeo(filteredProductList, fixedParkDto);
	/* 11. Remover Productos */
	removeRule.execute(priorizationList, filteredProductList);
	//removeRule.execute(priorizationListActual, filteredProductListActual);
	/* 12. Orden de productos */
	orderAllProducts.executeMT(priorizationList, filteredProductList, actualProducts, fixedParkDto);
	
	
	
	//orderAllProducts.executeMT(priorizationListActual, filteredProductListActual, actProducts, fixedParkDtoActual);
	
	onlyThreeOffersDownsell(priorizationList);
	
	showCurrentProductOfferEnabled(priorizationList);
	
	/* 13 - 17. validaciones de equipos decos y modems */
	equipmentValidations.executeMt(catalogDecoList, catalogModemList, priorizationList, fixedParkDto, commercialInfo, distinctPS, modemEquipmentOfferCopy);
	showMigraTechnologyMT(commercialInfo, fixedParkDto, priorizationList, actualProducts);
	//showMigraTechnologyMT(commercialInfo, fixedParkDto, priorizationList, actProducts);
	TotalUtil.validateCostFeeInstall(fixedParkDto, commercialInfo, priorizationList, listParam);
	/* 18. Determinar y calcular las rentas adicionales actuales */
	determinatePresentRents.executeMt(fixedParkDto, distinctPS);
	/* 19. Determinar bloques adicionales a mantener */
	determinateAdditionalBlocksToKeep.execute(priorizationList, fixedParkDto);
	/* 20. Calcular Promos Temporales */
	determinatePromoTemp.execute(commercialInfo, customer, priorizationList);
	/* 21. Determinar y calcular rentas adicionales a mantener */
	determinateAdditionalRentsToKepp.execute(priorizationList, fixedParkDto);
	/* 22. Calcular Renta Total y Renta Fija actual */
	determinateTotalRentByOffer.execute(priorizationList);
	determinateActualFixedRent.execute(fixedParkDto);
	/* 23. Calcular el Costo Total Productos separados */
	separatedProductsTotalCost.execute(priorizationList);
	/* 24 Calcular la cantidad de apps por Renta */
	determinateAppsQuantityByRentMonoProduct.execute(mobileDevices);
	/* 25. Determinar los bonos y descuentos actuales */
	determinatePresentBonusAndPresentDiscounts.execute(customer, commercialOpeList, mobileDevices);
	/* 26. Ofertas Disponibles */
	return availableOffers.execute(fixedParkDto, priorizationList, mobileDevices);

    }
    
    private String validateString(String text) {
    	if (null == text) {
    	    return StringUtils.EMPTY;
    	} else {
    	    return text;
    	}
    }
}
