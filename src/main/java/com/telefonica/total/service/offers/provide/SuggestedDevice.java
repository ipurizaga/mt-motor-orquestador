package com.telefonica.total.service.offers.provide;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Id;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.business.deviceRules.priceSimCard.IPriceSimCard;
import com.telefonica.total.business.deviceRules.riskFlag.IRiskFlag;
import com.telefonica.total.common.service.BaseMTService;
import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.ResponseConstants;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.dto.BeanMapper;
import com.telefonica.total.dto.ProductDtoMappper;
import com.telefonica.total.dto.ProductOfferDTO;
import com.telefonica.total.dto.RecentSubscMobile;
import com.telefonica.total.enums.OperationDevice;
import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.exception.BusinessException;
import com.telefonica.total.jdbc.PlanDeviceDao;
import com.telefonica.total.jdbc.PlanDeviceEntity;
import com.telefonica.total.jdbc.PlanFinancingDao;
import com.telefonica.total.jdbc.PlanFinancingEntity;
import com.telefonica.total.model.CatalogFouthDigitPPF;
import com.telefonica.total.model.ParamMovTotal;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.Customer;
import com.telefonica.total.pojo.req.Information;
import com.telefonica.total.pojo.req.ReqData;
import com.telefonica.total.pojo.req.Subscription;
import com.telefonica.total.pojo.resp.DeviceResp;
import com.telefonica.total.pojo.resp.PriceDeviceResp;
import com.telefonica.total.pojo.resp.ProductOffer;
import com.telefonica.total.pojo.resp.RespData;
import com.telefonica.total.repository.ParamMovTotalRepo;

import lombok.Data;

/**
 * 
 * @Author: jomapozo.
 * @Datecreation: 9 nov. 2018 11:56:15
 * @FileName: SuggestedDevice.java
 * @AuthorCompany: Telefonica
 * @version: 0.1
 * @Description: esta clase implementara toda la logica que refiere a la
 *               consulta de equipos.
 */
@Service
public class SuggestedDevice implements BaseMTService {

    @Autowired
    private ParamMovTotalRepo paramMovTotalRepo;
    @Autowired
    private IPriceSimCard priceSimCard;
    @Autowired
    private IRiskFlag riskFlag;
    @Autowired
    private PlanDeviceDao     planDeviceDao;
    @Autowired
    private PlanFinancingDao     planFinanDao;
    @Autowired
    private BeanMapper	      beanMap;
    @Autowired
    private ProductDtoMappper productMap;
    

    @Override
    public RespData executeLogicOperations(ReqData request) {
	RecentSubscMobile recentSubs = new RecentSubscMobile();
	CommercialOperation comOperation = request.getCommOperationInfo().getCommercialOpers().get(0);
	Subscription subscriber = request.getCommOperationInfo().getCommercialOpers().get(0).getSubscriber();
	
	String financeValue = "";
	for(CommercialOperation op : request.getCommOperationInfo().getCommercialOpers()) {
		if(op.getProduct().equals(SuscriptionType.MOBILE.getCode())) {
			//if(op.getCreditData().getCreditScore().substring(3) != null) {
			if(op.getCreditData().getFinanceValue() != null) {
				financeValue = op.getCreditData().getFinanceValue();
				break;
			}
		
		}
	}
	//operation.getCreditData().getCreditScore().substring(3);
	
	/** Se valida si es una alta reciente **/
	this.validateRecentProv(request.getCustomer(), subscriber, recentSubs, comOperation);

	List<ProductOfferDTO> products = null;
	RespData respData = null;
	
	//System.out.println("DEVICEEEEEEEEEEEE");
	
	ParamMovTotal paraMMovTotal = paramMovTotalRepo.findByParam(Constant.CAMPO_DNR_ORP_PRD_TP);
	
	/*List<ParamMovTotal> listParamMovTotal = paramMovTotalRepo.findAll();
	ParamMovTotal paraMMovTotal = new ParamMovTotal();
	for (ParamMovTotal pmt : listParamMovTotal) {
	    if(pmt.getGrupoParam().equals(Constant.CAMPO_DNR_ORP_PRD_TP)) {
		paraMMovTotal = pmt;
	    }
	}*/
	
	//System.out.println("paraMMovTotal.getCodValor()"+paraMMovTotal.getCodValor());
		
	//LLamando a stored procedure del paquete para financiamiento de equipos
	List<PlanDeviceEntity> planDeviceLst = new ArrayList<PlanDeviceEntity>();
	String deviceOperation = "";
	for(CommercialOperation op : request.getCommOperationInfo().getCommercialOpers()) {
		if(op.getProduct().equals(SuscriptionType.MOBILE.getCode())) {
			//if(op.getCreditData().getCreditScore().substring(3) != null) {
			if(op.getDeviceOperation() != null) {
				deviceOperation = op.getDeviceOperation();
				break;
			}
		
		}
	}

	String amount = "", bussiness = "";
	if(request.getCustomer().getDebts().size()>0) {
		if(request.getCustomer().getDebts().get(0).getAmount().toString() != null) {
			amount = request.getCustomer().getDebts().get(0).getAmount().toString();
		}
		if(request.getCustomer().getDebts().get(0).getBussiness() != null) {
			bussiness = request.getCustomer().getDebts().get(0).getBussiness();
		}
	}
	
	
	/*PROBANDO DATOS*/
	//System.out.println("pc_movil-> " + this.validatePhone(request.getCommOperationInfo().getCommercialOpers().get(0).getSubscriber()));
	//System.out.println("pc_operacion-> " +request.getCommOperationInfo().getCommercialOpers().get(0).getDeviceOperation());
	//System.out.println("pc_customer_type-> " + request.getCustomer().getCustomerType());
	//System.out.println("pc_customer_subtype-> " + this.validateString(request.getCustomer().getCustomerSubType()));
	//System.out.println("pc_store_province-> " + this.validateString(request.getCommOperationInfo().getProvince()));
	//System.out.println("store_brand-> " + this.validateString(request.getCommOperationInfo().getStoreBrand()));
	//System.out.println("store_id-> " + this.validateString(request.getCommOperationInfo().getStoreId()));
	//System.out.println("sale_channel-> " + request.getCommOperationInfo().getSalesChannel());
	//System.out.println("pc_movil0_estitular-> " + recentSubs.getIsOwner());
	//System.out.println("document-> " + recentSubs.getDocument());
	//System.out.println("pc_gvs-> " + recentSubs.getGvs());
	//System.out.println("pc_flag_earlycaeq-> " + recentSubs.getEarlyCaeq());
	//System.out.println("permanency-> " + recentSubs.getPermanency());
	//System.out.println("pc_flag-> " + recentSubs.getIsRecentProv());
	//System.out.println("pc_donor_operator-> " + this.validateString(bussiness));
	//System.out.println("pc_dnr_opr_prd_tp-> " + paraMMovTotal.getCodValor());
	//System.out.println("pc_snr_grp_dnr_opr-> " + amount);
	//System.out.println("cod_fina-> " + financeValue);
	//System.out.println("name_fina-> " + deviceOperation);
	//System.out.println("pc_lst_planes-> " + TotalUtil.generateArrayPlan(request.getOfferFilters()));
	//System.out.println("pc_lst_equipos-> " + TotalUtil.generateArrayDevice(request.getDeviceFilters()));
	//System.out.println("FIN PRUEBA");
	
	CatalogFouthDigitPPF catalogFourth = null;
	catalogFourth = riskFlag.execute(request.getCommOperationInfo().getCommercialOpers());
	
	
	
	
	
	try {
		planDeviceLst = planDeviceDao.getPlanDevices(
				this.validatePhone(request.getCommOperationInfo().getCommercialOpers().get(0).getSubscriber()),
				request.getCommOperationInfo().getCommercialOpers().get(0).getDeviceOperation(), 
				request.getCustomer().getCustomerType(),
				this.validateString(request.getCustomer().getCustomerSubType()),
				this.validateString(request.getCommOperationInfo().getDepartment()),
				this.validateString(request.getCommOperationInfo().getStoreBrand()),
				this.validateString(request.getCommOperationInfo().getStoreId()), 
				request.getCommOperationInfo().getSalesChannel(),
				recentSubs.getIsOwner(), 
				recentSubs.getDocument(), 
				recentSubs.getGvs(), 
				recentSubs.getEarlyCaeq(),
				recentSubs.getPermanency(), 
				recentSubs.getIsRecentProv(),
				this.validateString(bussiness),
				paraMMovTotal.getCodValor(),
				this.validateString(amount),
				//Nuevos campos para cambios de regla 14 y 18 a nivel de Equipos de Financiamiento
				catalogFourth.getFourthDigitScore().toString(),
				financeValue,
				deviceOperation,	
				
				TotalUtil.generateArrayPlan(request.getOfferFilters()),
				TotalUtil.generateArrayDevice(request.getDeviceFilters()));
		
		//System.out.println(planDeviceLst.get(0).getFinancingPlanCode());
	}catch(Exception e) {
		throw new BusinessException("2031");
	}
	
	
	
	
	
	
	
	/**HARCODEO Equipos Financiamiento
	PlanDeviceEntity planDevice = new PlanDeviceEntity();
	planDevice.setSapId("TMGPEZT0A531NE0001");
	planDevice.setSuggestedPrice(0D);
	planDevice.setDiscount(0D);
	planDevice.setPriceCommitment6(0D);
	planDevice.setPriceCommitment12(0D);
	planDevice.setPriceCommitment18(0D);
	planDevice.setPriceCommitment24(0D);
	planDevice.setPriceCommitment36(0D);
	planDevice.setFinancingPlanCode("TELEF001");
	planDevice.setAdditionalCostPpf(0D);
	planDevice.setPoCode("NUEVAPOCONTROLLMA");
	planDevice.setBoCode("PLANRE150");
	planDevice.setPoId("34794711");
	planDevice.setPoNameEs("NUEVA PO CONTROL LMA");
	planDevice.setBoId("35197511");
	planDevice.setBoNameEs("RV Plan Adic Ilimitado MiMovistar S/65.9");
	planDevice.setPostPaidFacMonthDelay("0");
	planDevice.setPosPaidFacDisDuration(0);
	planDevice.setPostPaidFacDisPercentage(0);
	planDevice.setPoProduct("Postpaid");
	planDevice.setInitialFee(250D);
	planDeviceLst.add(planDevice);**/
	

	/** Calcular precio simcard **/
	products = this.calcPrecioSimcard(request, this.generateResponse(planDeviceLst));

	/** Generate response data **/
	List<ProductOffer> prods = beanMap.dtosToProducts(products);
	respData = new RespData();
	respData.setProductOffers(prods);
	
	/** Operacion comercial es DevicechangeCAEQ **/
	OperationDevice operationDevice = OperationDevice
		.getOpeCommMobile(request.getCommOperationInfo().getCommercialOpers().get(0).getDeviceOperation());
	if (OperationDevice.CAEQ != operationDevice) {
	    return respData;
	}

	/** Determinar pago de penalidad */
	if (subscriber != null && subscriber.getIsRecentProvide() == 2) {
	    this.assignPenalty(respData, Constant.TRUE);
	} else {
	    this.calculatePenalty(products, respData);
	}
	
	

	return respData;
    }
    
    

    /**
     * 
     * @param request
     * @param products
     * @return se calcula el precio de simcard por producto o plan
     * @throws BusinessException
     */
    private List<ProductOfferDTO> calcPrecioSimcard(ReqData request, List<ProductOfferDTO> products) {
	CatalogFouthDigitPPF catalogFourth = null;
	catalogFourth = riskFlag.execute(request.getCommOperationInfo().getCommercialOpers());
	//String financeValue = request.getCommOperationInfo().getCommercialOpers().get(0).getCreditData().getFinanceValue();
	//return priceSimCard.execute(products, request, financeValue);
	return priceSimCard.execute(products, request, catalogFourth.getFlagRisk().toString());
    }

    private List<ProductOfferDTO> generateResponse(List<PlanDeviceEntity> plansDevices) {
	List<ProductOfferDTO> products = this.getProducts(plansDevices);
	List<ProductOfferDTO> productsResp = new ArrayList<>();
	DeviceResp device = null;
	PriceDeviceResp priceDevice = null;

	for (ProductOfferDTO product : products) {
	    List<DeviceResp> devices = new ArrayList<>();
	    ProductOfferDTO prodOff = null;
	    for (PlanDeviceEntity plan : plansDevices) {
		if (product.getBoCode().equals(plan.getBoCode()) && product.getPoCode().equals(plan.getPoCode())) {

		    
		    device = new DeviceResp();
		    priceDevice = new PriceDeviceResp();
		    
		    device.setSapId(plan.getSapId());
		    //device.setSapId("TMGPEZT0A531NE0001");
		    priceDevice.setPrice(plan.getSuggestedPrice());
		    //priceDevice.setPrice(0D);
		    priceDevice.setDiscount(plan.getDiscount());
		    //priceDevice.setDiscount(0D);
		    priceDevice.setPriceCommitment6(plan.getPriceCommitment6());
		    //priceDevice.setPriceCommitment6(0D);
		    priceDevice.setPriceCommitment12(plan.getPriceCommitment12());
		    //priceDevice.setPriceCommitment12(0D);
		    priceDevice.setPriceCommitment18(plan.getPriceCommitment18());
		    //priceDevice.setPriceCommitment18(0D);
		    priceDevice.setPriceCommitment24(plan.getPriceCommitment24());
		    //priceDevice.setPriceCommitment24(0D);
		    priceDevice.setPriceCommitment36(plan.getPriceCommitment36());
		    //priceDevice.setPriceCommitment36(0D);
		    //Nuevo campo para response de financiamiento de equipos con los cambios de la Regla 14 y 18
		    priceDevice.setFinancingPlanCode(plan.getFinancingPlanCode() != null ? plan.getFinancingPlanCode() : "");
		    //priceDevice.setFinancingPlanCode("TELEF001");
		    priceDevice.setInitialFee(plan.getInitialFee());
		    device.setPrice(priceDevice);
		    
		    devices.add(device);
		}
	    }
	    prodOff = productMap.dtoToProduct(product);
	    prodOff.setDevices(devices);
	    productsResp.add(prodOff);
	}
	return productsResp;
    }

    private List<ProductOfferDTO> getProducts(List<PlanDeviceEntity> plansDevices) {
	List<ProductOfferDTO> products = new ArrayList<>();
	ProductOfferDTO product = null;
	String boAux = StringUtils.EMPTY;
	String poAux = StringUtils.EMPTY;

	for (PlanDeviceEntity planDevice : plansDevices) {
	    if (!(boAux.equals(planDevice.getBoCode()) && poAux.equals(planDevice.getPoCode()))) {
		product = new ProductOfferDTO();
		product.setBoCode(planDevice.getBoCode() != null ? planDevice.getBoCode() : "");
		product.setBoId(planDevice.getBoId() != null ? planDevice.getBoId() : "");
		product.setBoNameEs(planDevice.getBoNameEs() != null ? planDevice.getBoNameEs() : "");
		product.setPoCode(planDevice.getPoCode() != null ? planDevice.getPoCode() : "");
		product.setPoId(planDevice.getPoId() != null ? planDevice.getPoId() : "");
		product.setPoNameEs(planDevice.getPoNameEs() != null ? planDevice.getPoNameEs() : "");
		product.setPoProduct(planDevice.getPoProduct() != null ? planDevice.getPoProduct() : "");
		product.setPrice(planDevice.getBoPrice() != null ? planDevice.getBoPrice() : 0D);
		product.setGvs(planDevice.getGvs() != null ? planDevice.getGvs() : "");
		product.setAdditionalCostPpf(planDevice.getAdditionalCostPpf() != null ? planDevice.getAdditionalCostPpf() : 0D);
		product.setPostPaidFacMonthDelay(planDevice.getPostPaidFacMonthDelay() != null ? planDevice.getPostPaidFacMonthDelay() : "");
		product.setPermanency(planDevice.getPermanency() != null ? planDevice.getPermanency() : 0);
		product.setFlagEarlyCaeq(planDevice.getFlagEarlyCaeq() != null ? planDevice.getFlagEarlyCaeq() : 0);
		//Nuevo Campo para equipos de financiamiento con los cambios de la regla 14 y 18
		product.setFinacingPlanCode(planDevice.getFinancingPlanCode() != null ? planDevice.getFinancingPlanCode() : "");

		Information info = new Information();
		info.setCode(ResponseConstants.POSTPAIDEASY_MOTHNSDELAY_DISCOUNT);
		info.setValue(planDevice.getPostPaidFacMonthDelay());
		product.getBillingOffersInfo().add(info);

		Information inf = new Information();
		inf.setCode(ResponseConstants.ADDITIONALCOST_PPF);
		inf.setValue(String.valueOf(planDevice.getAdditionalCostPpf()));
		product.getBillingOffersInfo().add(inf);

		Information in = new Information();
		in.setCode(ResponseConstants.POSTPAIDEASY_DISCOUNT_DURATION);
		in.setValue(String.valueOf(planDevice.getPosPaidFacDisDuration()));
		product.getBillingOffersInfo().add(in);

		Information infor = new Information();
		infor.setCode(ResponseConstants.POSTPAIDEASY_DISCOUNT_PERCENTAGE);
		infor.setValue(String.valueOf(planDevice.getPostPaidFacDisPercentage()));
		product.getBillingOffersInfo().add(infor);

		boAux = planDevice.getBoCode();
		poAux = planDevice.getPoCode();
		products.add(product);
	    }
	}
	return products;
    }

    private void calculatePenalty(List<ProductOfferDTO> products, RespData resp) {
	if (CollectionUtils.isNotEmpty(products)) {
	    Integer permanency = products.get(0).getPermanency();
	    String gvs = products.get(0).getGvs();
	    Character earlyCaeq = products.get(0).getFlagEarlyCaeq();
	    if ((null == permanency && null == earlyCaeq) || (permanency != null && Constant.YES == earlyCaeq)
		    || (permanency != null && permanency <= 6 && Constant.GVS_POS1.equals(gvs))) {
		this.assignPenalty(resp, Constant.FALSE);
	    } else {
		this.assignPenalty(resp, Constant.TRUE);
	    }
	}
    }

    private String validatePhone(Subscription subscriber) {
	if(subscriber!=null && subscriber.getServiceNumber()!=null) {
	    return subscriber.getServiceNumber();
	} else {
	    return StringUtils.EMPTY;
	}
    }

    private String validateString(String text) {
	if (null == text) {
	    return StringUtils.EMPTY;
	} else {
	    return text;
	}
    }

    private void validateRecentProv(Customer customer, Subscription subscriber, RecentSubscMobile recentSubs, CommercialOperation comOperation) {

	if (null != subscriber) {
	    recentSubs.setGvs(this.validateString(subscriber.getGvs()));
	    if (OperationDevice.CAEQ.getCodeDesc().equals(comOperation.getDeviceOperation()) 
		 && (subscriber.getIsRecentProvide() == 1 || subscriber.getIsRecentProvide() == 0)) {
		recentSubs.setIsOwner(subscriber.getIsOwner() == Boolean.TRUE ? Constant.ONE : Constant.ZERO);
		recentSubs.setDocument(customer.getDocumentNumber());
		recentSubs.setEarlyCaeq(subscriber.getIsEarlyCaeq() == Boolean.TRUE ? Constant.YES : Constant.NOT);
		recentSubs.setPermanency(subscriber.getPermanency() == null ? Constant.DEFAULT_PERMANENCY : subscriber.getPermanency());
		recentSubs.setIsRecentProv(Constant.TRUE);
	    } else {
		this.fillDefaultRecentSub(recentSubs);
	    }
	} else {
	    recentSubs.setGvs(StringUtils.EMPTY);
	    this.fillDefaultRecentSub(recentSubs);
	}
    }

    private void fillDefaultRecentSub(RecentSubscMobile recentSubs) {
	recentSubs.setIsOwner('0');
	recentSubs.setDocument(StringUtils.EMPTY);
	recentSubs.setEarlyCaeq('0');
	recentSubs.setPermanency(Constant.DEFAULT_PERMANENCY);
	recentSubs.setIsRecentProv(Constant.TRUE);
    }

    private void assignPenalty(RespData resp, String flag) {
	Information info = new Information();
	info.setCode(ResponseConstants.PENALITY);
	info.setValue(flag);
	resp.getAdditionalInfo().add(info);
    }

}
