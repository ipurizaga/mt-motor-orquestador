package com.telefonica.total.service.offers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import com.telefonica.total.business.offersRules.evaluateSalesCondition.IEvaluateSalesCondition;
import com.telefonica.total.business.offersRules.financialPercentage.IFinancialPercentage;
import com.telefonica.total.business.offersRules.pdfCondition.IPdfCondition;
import com.telefonica.total.business.offersRules.productsRules.IProductsRules;
import com.telefonica.total.common.service.BaseMTService;
import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.RulesValueProp;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.dto.BillingProductDTO;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.FourthDigitDTO;
import com.telefonica.total.dto.FourthDigitFinanceDTO;
import com.telefonica.total.dto.PartialRequestDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.enums.InternetTechnologyType;
import com.telefonica.total.enums.SaleModalityMobile;
import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.exception.BusinessException;
import com.telefonica.total.generic.BillProdCatalogCollection;
import com.telefonica.total.generic.CommOperCollection;
import com.telefonica.total.model.InternetTechnology;
import com.telefonica.total.model.MasterUbigeo;
import com.telefonica.total.model.ParamMovTotal;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.CommercialOperationInfo;
import com.telefonica.total.pojo.req.Information;
import com.telefonica.total.pojo.req.Offer;
import com.telefonica.total.repository.FinancingRepo;
import com.telefonica.total.repository.MasterUbigeoRepo;
import com.telefonica.total.repository.ParamMovTotalRepo;
import com.telefonica.total.business.offersRules.determinateTotalRentByOffer.IDeterminateTotalRentByOffer;

public abstract class ASuggestedPlan implements BaseMTService {

    @SuppressWarnings("unused")
    @Autowired
    private FinancingRepo financing;
    
    @Autowired
    private ParamMovTotalRepo paramMovTotalRepo;
    
    private ParamMovTotal pmt;

    @Autowired
    private IPdfCondition pdfCondition;
    
    @Autowired
    private IFinancialPercentage financialPercentage;
    
    @Autowired
    private IEvaluateSalesCondition evaluateSalesCondition;

    @Autowired
    private IProductsRules productRules;
    
    @Autowired
    private IDeterminateTotalRentByOffer determinateTotalRentByOffer;
    
    @Autowired
    private MasterUbigeoRepo masterUbigeoRepo;
    
    @Autowired
    private RulesValueProp rulesValue;

    /***
     * Método que valida en el plano Financiamiento si alguno de los móviles
     * ingresados desde el front tienen financiamiento actual.
     * 
     * @param commercialInfo
     * @param partial
     * @return
     */
    public Boolean haveCurrentFinancing(CommercialOperationInfo commercialInfo, PartialRequestDTO partial) {
	CommOperCollection<CommercialOperation> commOperCollection = new CommOperCollection<>(commercialInfo.getCommercialOpers());
	commOperCollection.filterByProduct(SuscriptionType.MOBILE);
	boolean hasFinancing = false;
	for (CommercialOperation operation : commOperCollection) {
	    if (operation.getSubscriber() != null && operation.getSubscriber().getHasFinancing() != null
		    && operation.getSubscriber().getHasFinancing()) {
		hasFinancing = true;
		break;
	    }
	}
	// if (!financingList.isEmpty()) {
	// partial.getCustomerInformation().setFinancingList(financingList);
	// }
	// return CollectionUtils.isNotEmpty(financingList);
	return hasFinancing;

    }

    /**
     * Método que se encarga de determinar las condiciones de venta de los productos
     * ofrecidos en MT.
     * 
     * @param commercialInfo
     * @param partial
     */
    public void determinateSalesConditions(CommercialOperationInfo commercialInfo, PartialRequestDTO partial) {
	List<FourthDigitFinanceDTO> getFinancialPercentage = null;
	CommOperCollection<CommercialOperation> commOperCollection = new CommOperCollection<>(commercialInfo.getCommercialOpers());
	commOperCollection.filterByProduct(SuscriptionType.MOBILE);
	if (CollectionUtils.isNotEmpty(commOperCollection)) {
	    getFinancialPercentage = financialPercentage.execute(commOperCollection);
	}
	if (CollectionUtils.isNotEmpty(getFinancialPercentage)) {
	    evaluateSalesCondition.execute(partial, getFinancialPercentage);
	}
    }
    
    public void determinateCoverage(CommercialOperationInfo commercialInfo, FixedDTO fixedParkDto) {
	
	if((commercialInfo.getCoverage().toLowerCase().equals("fiber")?"FTTH":commercialInfo.getCoverage()).equals(InternetTechnologyType.HFC.getCode())) {
	    if(fixedParkDto.getHfcCobInternet()!=null) {
		if(fixedParkDto.getHfcCobInternet() == '2') {
		    commercialInfo.setCoverage(InternetTechnologyType.FTTH.getCode());
		}
	    }
	}
	
    }
    
    /**
     * Método que se encarga de determinar el type según el PS.
     * 
     * @param commercialInfo
     * @param partial
     */
    public void determinateType(CommercialOperationInfo commercialInfo) {
	List<ParamMovTotal> getAllParam = paramMovTotalRepo.findAll();
	
	for (ParamMovTotal paramMovTotal : getAllParam) {
	    
	    if(paramMovTotal.getGrupoParam().equals(Constant.GROUP_PARAM_PS) && paramMovTotal.getCodValor().equals(commercialInfo.getCommercialOpers().get(0).getSubscriber().getSubscriberId())) {
		commercialInfo.getCommercialOpers().get(0).getSubscriber().setType(paramMovTotal.getValor());
		break;
	    }
	    
	}
	
	//System.out.println("llegué");
	
    }

    /***
     * Método que se encarga de asignar a cada producto ofrecido la condicion PPF
     * según los mobiles que se encuentran como operacion comercial de tipo alta
     * combo.
     * 
     * @param commercialInfo
     * @param partial
     */
    public void determinatePpfCondition(CommercialOperationInfo commercialInfo, PartialRequestDTO partial) {
	CommOperCollection<CommercialOperation> commOperCollection = new CommOperCollection<>(commercialInfo.getCommercialOpers());
	commOperCollection.filterByProduct(SuscriptionType.MOBILE);
	if (CollectionUtils.isNotEmpty(commOperCollection)) {
	    commOperCollection.filterBySalesMode(SaleModalityMobile.ALTACOMBO);
	    if (CollectionUtils.isNotEmpty(commOperCollection)) {
		List<FourthDigitDTO> conditionsPpfList = pdfCondition.execute(commOperCollection);
		for (ProductFixedDTO product : partial.getResultantProductFixedList()) {
		    int planRank = product.getBillingProductDto().getBoPlankRank();
		    for (FourthDigitDTO objDigit : conditionsPpfList) {
			Long commercialOpId = Long.parseLong(objDigit.getCommercialOperationId());
			productRules.assignPpfCondition(commercialOpId, product, productRules.evaluateCondition(planRank, objDigit));
		    }
		}
	    }
	}
    }
    public void determinatePpfConditionFalse(CommercialOperationInfo commercialInfo, PartialRequestDTO partial) {
	CommOperCollection<CommercialOperation> commOperCollection = new CommOperCollection<>(commercialInfo.getCommercialOpers());
	commOperCollection.filterByProduct(SuscriptionType.MOBILE);
	
		for (ProductFixedDTO product : partial.getResultantProductFixedList()) {
			List<FourthDigitDTO> conditionsPpfList = pdfCondition.execute(commOperCollection);
			for (FourthDigitDTO objDigit : conditionsPpfList ) {
				Long commercialOpId = Long.parseLong(objDigit.getCommercialOperationId());
				productRules.assignPpfConditionFalse(commercialOpId,product, false);
			}
		}    
    }


    /***
     * Método que se encarga de eliminar de la lista de productos los planes que
     * sean menores al score de riesgo, luego si el cliente escogio una lista de
     * offertas entonces se envian estos al cliente siempre ycuando estos cumplan
     * los criterios de escore y esten dentro de los productos fijos priorizados
     * anteriormente.
     * 
     * @param partial
     * @param commercialInfo
     * @param optionalOffers
     */
    public void filterRiskScoreAndInterest(PartialRequestDTO partial, CommercialOperationInfo commercialInfo, List<Offer> optionalOffers) {

	CommOperCollection<CommercialOperation> commOperCollection = new CommOperCollection<>(commercialInfo.getCommercialOpers());
	commOperCollection.filterByProduct(SuscriptionType.MOBILE);
	String scoreDigits = commOperCollection.obtainThreeFirtsDigitFromScore();
	String productType = commercialInfo.getProductType();
	BillProdCatalogCollection<BillingProductDTO> boPoListFromFilteredFixedProducts = productRules.obtainFilteredListBoPo(partial,
		commercialInfo, 1);
	productRules.assignBillingProductValues(partial, boPoListFromFilteredFixedProducts, productType);
	productRules.determinatePenaltyByPlanRank(partial);
	
	List<ProductFixedDTO> riskFilteredList = productRules.assignBillingOfferProductFixed(partial, scoreDigits);
	if (CollectionUtils.isNotEmpty(optionalOffers)) {
	    List<ProductFixedDTO> interestFilteredList = productRules.assignBillingOfferClientInterested(riskFilteredList, optionalOffers);
	    TotalUtil.clearAndSetFixedList(partial, interestFilteredList);
	} else {
	    TotalUtil.clearAndSetFixedList(partial, riskFilteredList);
	}
    }
    
    /***
     * Método que se encarga de eliminar de la lista de productos los planes que
     * sean menores al score de riesgo, luego si el cliente escogio una lista de
     * offertas entonces se envian estos al cliente siempre ycuando estos cumplan
     * los criterios de escore y esten dentro de los productos fijos priorizados
     * anteriormente.
     * 
     * @param partial
     * @param commercialInfo
     * @param optionalOffers
     */
    public void findCurrentOffer2Mobile(PartialRequestDTO partial, CommercialOperationInfo commercialInfo, List<Offer> optionalOffers) {

	CommOperCollection<CommercialOperation> commOperCollection = new CommOperCollection<>(commercialInfo.getCommercialOpers());
	
	List<ProductFixedDTO> catalogProductFixedDto = productRules.getProductFixed();
	//Calculando Renta Total del producto Actual que se agregará a la lista de productos
	determinateTotalRentByOffer.execute(catalogProductFixedDto);
	boolean no_existe = true;
	boolean sin_segundo_subscriber = true;
	
	if((commercialInfo.getCommercialOpers().get(2).getOperation().equals(Constant.OPERATION_REPLACEOFFER) ||
		commercialInfo.getCommercialOpers().get(2).getOperation().equals(Constant.OPERATION_PORTABILITY)
		
		&& commercialInfo.getCommercialOpers().get(1).getSubscriber().getType().equals(Constant.TYPE_MTM)))
	    
	{
	    
	    String parrilla = "";
		
            for (ProductFixedDTO productFixedDTO6 : catalogProductFixedDto) {
            		    
            	if(commOperCollection.get(1).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO6.getPackageMobilCode()))) {
            			
            		parrilla = productFixedDTO6.getMerchandisingType();
            			break;		
            	}
            		    
            }
            
            boolean existe=false;
		
		for (ProductFixedDTO productFixedDTO7 : catalogProductFixedDto) {
		
		    	if(productFixedDTO7.getPackagePs().equals(Integer.parseInt(commOperCollection.get(0).getSubscriber().getSubscriberId()))
		    		&& productFixedDTO7.getMerchandisingType().equals(parrilla)) {
			
		    	    //System.out.println("chau");		
		    	    
		    	    productFixedDTO7.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
		    	    productFixedDTO7.setMobil0QuantityData(productFixedDTO7.getMobil0QuantityData()+productFixedDTO7.getMobil1QuantityData());
		    	    partial.getResultantProductFixedList().add(productFixedDTO7);
			    //no_existe = false;
			    existe = true;
			    break;
			}
		}
		
		if(!existe) {
		    throw new BusinessException(Constant.BE_1057);
		}
	}else {
	
	for (ProductFixedDTO productFixedDTO : catalogProductFixedDto) {
	    
	    if (commOperCollection.get(2).getSubscriber()!=null) {
		if(commOperCollection.get(2).getSubscriber().getBoId()!=null) {
                	    if(productFixedDTO.getPackagePs().equals(Integer.parseInt(commOperCollection.get(0).getSubscriber().getSubscriberId()))) {
                		
                		//System.out.println("chau");
                		
                		if(commOperCollection.get(1).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO.getPackageMobilCode()))) {
                		    productFixedDTO.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
                		    partial.getResultantProductFixedList().add(productFixedDTO);        		    
                		    no_existe = false;
                		    
                		}else if (commOperCollection.get(2).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO.getPackageMobilCode()))) {
                		    productFixedDTO.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
                		    partial.getResultantProductFixedList().add(productFixedDTO);
                		    no_existe = false;
                		}
                		
                	    }
		}else {
        		    if(productFixedDTO.getPackagePs().equals(Integer.parseInt(commOperCollection.get(0).getSubscriber().getSubscriberId()))) {
                		
                		//System.out.println("chau");
                		
                		if(commOperCollection.get(1).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO.getPackageMobilCode()))) {
                		    productFixedDTO.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
                		    partial.getResultantProductFixedList().add(productFixedDTO);        		    
                		    no_existe = false;
                		    
                		}
                		
                	    }
		}
        	    
	    }else {
		if(productFixedDTO.getPackagePs().equals(Integer.parseInt(commOperCollection.get(0).getSubscriber().getSubscriberId()))) {
    		
        		//System.out.println("chau");
        		
        		if(commOperCollection.get(1).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO.getPackageMobilCode()))) {
        		    productFixedDTO.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
        		    partial.getResultantProductFixedList().add(productFixedDTO);
        		    no_existe = false;
        		    
        		}
        		
        	}
	    }
	    
	}
	
	
	
	if(no_existe) {
	    
	    if (commOperCollection.get(2).getSubscriber()!=null) {
		if(commOperCollection.get(2).getSubscriber().getBoId()!=null) {
        	    if (commOperCollection.get(1).getSubscriber().getBoId().equals(commOperCollection.get(2).getSubscriber().getBoId())) {
                	    for (ProductFixedDTO productFixedDTO2 : catalogProductFixedDto) {
                		if(commOperCollection.get(1).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO2.getPackageMobilCode()))) {
                		    productFixedDTO2.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
                		    partial.getResultantProductFixedList().add(productFixedDTO2);
                		    no_existe = false;
                		}
                	    }
        	    
        	    }else{
        		for (ProductFixedDTO productFixedDTO3 : catalogProductFixedDto) {
                		if(commOperCollection.get(1).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO3.getPackageMobilCode()))) {
                		    productFixedDTO3.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
                		    partial.getResultantProductFixedList().add(productFixedDTO3);
                		    no_existe = false;
                		    
                		}else if (commOperCollection.get(2).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO3.getPackageMobilCode()))) {
                		    productFixedDTO3.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
                		    partial.getResultantProductFixedList().add(productFixedDTO3);
                		    no_existe = false;
                		}
        		}
        	    }
		}else {
		    for (ProductFixedDTO productFixedDTO5 : catalogProductFixedDto) {
			if(commOperCollection.get(1).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO5.getPackageMobilCode()))) {
        		    productFixedDTO5.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
        		    partial.getResultantProductFixedList().add(productFixedDTO5);
        		    no_existe = false;
        		    
        		}
		    }
		}
	    
	    }else {
		String parrilla = "";
		
                for (ProductFixedDTO productFixedDTO : catalogProductFixedDto) {
                		    
                	if(commOperCollection.get(1).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO.getPackageMobilCode()))) {
                			
                		parrilla = productFixedDTO.getMerchandisingType();
                			break;		
                	}
                		    
                }
                
                boolean existe=false;
		
		for (ProductFixedDTO productFixedDTO4 : catalogProductFixedDto) {
		
		    	if(productFixedDTO4.getPackagePs().equals(Integer.parseInt(commOperCollection.get(0).getSubscriber().getSubscriberId()))
		    		&& productFixedDTO4.getMerchandisingType().equals(parrilla)) {
			
		    	    //System.out.println("chau");		
		    	    
		    	    productFixedDTO4.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
		    	    productFixedDTO4.setMobil0QuantityData(productFixedDTO4.getMobil0QuantityData()+productFixedDTO4.getMobil1QuantityData());
		    	    partial.getResultantProductFixedList().add(productFixedDTO4);
			    no_existe = false;
			    existe = true;
			    break;
			}
		}
		
		if(!existe) {
		    throw new BusinessException(Constant.BE_1057);
		}
	    }
	}
	
	if(no_existe) {
	    throw new BusinessException(Constant.BE_2023);
	}
	
	}
	
	//System.out.println("hola");
	
    }
    
    public void findCurrentOffer1Mobile(PartialRequestDTO partial, CommercialOperationInfo commercialInfo, List<Offer> optionalOffers) {

	CommOperCollection<CommercialOperation> commOperCollection = new CommOperCollection<>(commercialInfo.getCommercialOpers());
	
	List<ProductFixedDTO> catalogProductFixedDto = productRules.getProductFixed();
	//Calculando Renta Total del producto Actual que se agregará a la lista de productos
	determinateTotalRentByOffer.execute(catalogProductFixedDto);
	String parrilla = "";
	
	for (ProductFixedDTO productFixedDTO : catalogProductFixedDto) {
	    
	    if(commOperCollection.get(1).getSubscriber().getBoId().equals(Integer.parseInt(productFixedDTO.getPackageMobilCode()))) {
	    
		parrilla = productFixedDTO.getMerchandisingType();
		break;		
	    }
	    
	}
	
	boolean existe=false;
	
	for (ProductFixedDTO productFixedDTO2 : catalogProductFixedDto) {
	
	    	if(productFixedDTO2.getPackagePs().equals(Integer.parseInt(commOperCollection.get(0).getSubscriber().getSubscriberId()))
	    		&& productFixedDTO2.getMerchandisingType().equals(parrilla)) {
		
	    	    //System.out.println("chau");		
		
	    	    productFixedDTO2.setCurrentProduct(Constant.CURRENT_OFFER_TYPE);
	    	    //Integer cantidad = productFixedDTO2.getMobil0QuantityData()+productFixedDTO2.getMobil1QuantityData();
	    	    productFixedDTO2.setMobil0QuantityData(productFixedDTO2.getMobil0QuantityData()+productFixedDTO2.getMobil1QuantityData());
		    partial.getResultantProductFixedList().add(productFixedDTO2);
		    
		    existe = true;
		    break;
		}
	}
	
	if(!existe) {
	    throw new BusinessException(Constant.BE_1057);
	}
	
	//System.out.println("hola");
	
    }
    
    /***
     * Método que se encarga de eliminar de la lista de productos los planes que
     * sean menores al score de riesgo, luego si el cliente escogio una lista de
     * offertas entonces se envian estos al cliente siempre ycuando estos cumplan
     * los criterios de escore y esten dentro de los productos fijos priorizados
     * anteriormente.
     * 
     * @param partial
     * @param commercialInfo
     * @param optionalOffers
     */
    public void filterRiskScoreAndInterestLMA(PartialRequestDTO partial, CommercialOperationInfo commercialInfo, List<Offer> optionalOffers) {

	CommOperCollection<CommercialOperation> commOperCollection = new CommOperCollection<>(commercialInfo.getCommercialOpers());
	commOperCollection.filterByProduct(SuscriptionType.MOBILE);
	String scoreDigits = commOperCollection.obtainThreeFirtsDigitFromScore();
	String productType = commercialInfo.getProductType();
	BillProdCatalogCollection<BillingProductDTO> boPoListFromFilteredFixedProducts = productRules.obtainFilteredListBoPo(partial,
		commercialInfo, 2);
	productRules.assignBillingProductValues(partial, boPoListFromFilteredFixedProducts, productType);
	productRules.determinatePenaltyByPlanRank(partial);
	partial.setProductType(commercialInfo.getProductType());
	List<ProductFixedDTO> riskFilteredList = productRules.assignBillingOfferProductFixed(partial, scoreDigits);
	if (CollectionUtils.isNotEmpty(optionalOffers)) {
		//Cambio para solo LMA, validacion para filtros
		if (CollectionUtils.isNotEmpty(riskFilteredList)) {
			TotalUtil.clearAndSetFixedList(partial, riskFilteredList);
		}else {
			List<ProductFixedDTO> interestFilteredList = productRules.assignBillingOfferClientInterestedLMA(partial, riskFilteredList, optionalOffers);
		    TotalUtil.clearAndSetFixedList(partial, interestFilteredList);
		}	    
	} else {
	    TotalUtil.clearAndSetFixedList(partial, riskFilteredList);
	}
    }

    /***
     * Método que se encarga de verificar si existe algun producto a la que el
     * cliente aplique para movistar total.
     * 
     * @param partial
     */
    public void clientApplyToMT(PartialRequestDTO partial) {
	if (partial.getResultantProductFixedList().isEmpty()) {
	    throw new BusinessException(Constant.BE_1055);
	}
    }
    
    /***
     * Método que se encarga de validar el valor del campo value en el request,
     * si tiene valor 1, 0 o cualquier otro texto
     * 
     * @param commercialInfo
     */
    public void validateBeach(CommercialOperationInfo commercialInfo) {
	
	if(commercialInfo.getAdditionalOperationInformation()!=null) {
		
		for(Information info : commercialInfo.getAdditionalOperationInformation()) {
			if(!info.getCode().equals(Constant.PRODUCTO_PLAYA)) {
	        	switch(info.getValue()) {
	        	
	        	case "1":
	        	    break;
	        	case "2":
	        	    break;
	        	default:
	        	    throw new BusinessException(Constant.BE_1075);
	        	
	        	}
			}		
		}


	
	}
	
    }
    
    public boolean validateUpFront(CommercialOperationInfo commercialInfo) {
	
	boolean existeFront = false;
	
	if(commercialInfo.getAdditionalOperationInformation()!=null) {
		
		for(Information info : commercialInfo.getAdditionalOperationInformation()) {
			if(info.getCode().equals(Constant.UPFRONT)) {
	        	switch(info.getValue()) {
	        	
	        	case "1":
	        	    existeFront = true;
	        	    break;
	        	default:
	        	    throw new BusinessException(Constant.BE_1075);
	        	
	        	}
			}		
		}


	
	}
	
	return existeFront;
	
    }
    
    /**
     * 
     * @param partial
     * @param commercialInfo
     * @param optionalOffers
     */
    public void packageBenefit(PartialRequestDTO partial) {
    	List<ProductFixedDTO> productOffers = new ArrayList<ProductFixedDTO>();
    	pmt = paramMovTotalRepo.findByCode(Constant.PACK_BENEFIT);
    			if(partial.getResultantProductFixedList()!=null) {
    				if(partial.getResultantProductFixedList().size()>0) {
    					productOffers = partial.getResultantProductFixedList();
    				}
    			}
    			
    			for(ProductFixedDTO prodOffer : productOffers) {
    				if(prodOffer.getInternetDestinyTechnology().equals("FTTH")) {
    					if(pmt.getValor().equals("Habilitado")) {
        					prodOffer.setPackageBenefit(pmt.getCol2());
    					}
    				}
    			}
    }



    
    public void haveUbigeo(List<ProductFixedDTO> productList, FixedDTO fixedParkDto) {
	List<MasterUbigeo> listMasterUbigeo = this.masterUbigeoRepo.findAll();
	
	List<ProductFixedDTO> removeList = new ArrayList<>();
	
	boolean eliminar = false;
	
	boolean bdepa = true, bprov = true, bdist = true;
	
	if(fixedParkDto.getDepartmentLocation() == null) {
	    bdepa  = false;
	}
	
        if(fixedParkDto.getProvinceLocation() == null) {
            bprov  = false;
        }
        
        if(fixedParkDto.getDistrictLocation() == null) {
            bdist  = false;
        }
        
        if(bdepa && bprov && bdist) {
	
        	List<MasterUbigeo> listMasterUbigeoHabilitados = new ArrayList<MasterUbigeo>();
        	
        	for (MasterUbigeo masterUbigeo : listMasterUbigeo) {
        	    if(masterUbigeo.getHabilitado().equals("1")) {
        		    listMasterUbigeoHabilitados.add(masterUbigeo);
        	    }
        	}
        	
        	List<MasterUbigeo> listMasterUbigeo3campos = new ArrayList<MasterUbigeo>();
        	List<MasterUbigeo> listMasterUbigeo3camposNull = new ArrayList<MasterUbigeo>();
        	
        	for (MasterUbigeo masterUbigeoHabilitados : listMasterUbigeoHabilitados) {
        	    if(masterUbigeoHabilitados.getDepartamento() != null) {
        		listMasterUbigeo3campos.add(masterUbigeoHabilitados);
        	    }else {
        		listMasterUbigeo3camposNull.add(masterUbigeoHabilitados);
        	    }
        		/*else {
        		eliminar = true;
        	    }*/
        	}
        	
        	List<MasterUbigeo> listMasterUbigeo2campos = new ArrayList<MasterUbigeo>();
        	List<MasterUbigeo> listMasterUbigeo2camposNull = new ArrayList<MasterUbigeo>();
        	
        	for (MasterUbigeo masterUbigeo3campos : listMasterUbigeo3campos) {
        	    if(masterUbigeo3campos.getProvincia() != null) {
        		listMasterUbigeo2campos.add(masterUbigeo3campos);
        	    }else {
        		listMasterUbigeo2camposNull.add(masterUbigeo3campos);
        	    }
        	    /*if(masterUbigeo3campos.getProvincia() == null) {
        		eliminar = true;
        		//throw new BusinessException(Constant.BE_1056);
        	    }else {
        		listMasterUbigeo2campos.add(masterUbigeo3campos);
        	    }*/
        	}
        	
        	List<MasterUbigeo> listMasterUbigeo1campo = new ArrayList<MasterUbigeo>();
        	
        	for (MasterUbigeo masterUbigeo2campos : listMasterUbigeo2campos) {
        	    if(masterUbigeo2campos.getDistrito() != null) {
        		listMasterUbigeo1campo.add(masterUbigeo2campos);
        	    }
        	    /*if(masterUbigeo2campos.getDistrito() == null) {
        		eliminar = true;
        		//throw new BusinessException(Constant.BE_1056);
        	    }else {
        		listMasterUbigeo1campo.add(masterUbigeo2campos);
        	    }*/
        	}
        	
        	if(listMasterUbigeo1campo.size() > 0) {
        	    for (MasterUbigeo masterUbigeo1field : listMasterUbigeo1campo) {
        		if((fixedParkDto.getDistrictLocation().equals(masterUbigeo1field.getDistrito()) && 
        			fixedParkDto.getProvinceLocation().equals(masterUbigeo1field.getProvincia()) &&
        			fixedParkDto.getDepartmentLocation().equals(masterUbigeo1field.getDepartamento()))) {
        		    	eliminar = true;
        		    	break;
        		}
        	    }
        	    
        	    /*if(!eliminar) {
        		for (MasterUbigeo masterUbigeo1field : listMasterUbigeo1campo) {
        		    if(masterUbigeo1field.getProvincia() == null && masterUbigeo1field.getDistrito() !=null) {
        			eliminar = false;
        		    }
        		}
        	    }*/
        	    
        	}else {
        	    if(listMasterUbigeo2camposNull.size() > 0) {
        		for (MasterUbigeo masterUbigeo2fieldNull : listMasterUbigeo2camposNull) {
        			if((fixedParkDto.getProvinceLocation().equals(masterUbigeo2fieldNull.getProvincia()) &&
        				fixedParkDto.getDepartmentLocation().equals(masterUbigeo2fieldNull.getDepartamento()))) {
        			    	eliminar = true;
        			    	break;
        			}
        		}
        	    }else {
        		if(listMasterUbigeo3camposNull.size() > 0) {
        		    for (MasterUbigeo masterUbigeo3fieldNull : listMasterUbigeo3camposNull) {
        			if((fixedParkDto.getDepartmentLocation().equals(masterUbigeo3fieldNull.getDepartamento()))) {
        			    	eliminar = true;
        			    	break;
        			}
        		    }
        		}
        	    }
        	}
	
        }
	
	
	//fixedParkDto.getSpeedInternet() >=60D
	
	for (ProductFixedDTO produ : productList) {
	    
	    boolean prdactual = false;
	    
	    for (Information info : produ.getCharacteristics()) {
		if(info.getValue().equals(rulesValue.getPRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT())) {
		    prdactual = true;
		}
	    }
	    
	    if(!prdactual) {
	    
        	    if(produ.getInternetSpeed() >=500000) {
        		
        		if(!eliminar) {
        		    removeList.add(produ);
        		}		
        	    }
	    
	    }
	}
	
	productList.removeAll(removeList);
	//TotalUtil.availableOffersMT(productList, Constant.BE_1069);
	
    }
 

    
    public void onlyThreeOffersDownsell(List<ProductFixedDTO> productList) {
	
	int cantidadDownsell = 0;
	
	for (ProductFixedDTO produ : productList) {
	    
	    String valor_indicador = "";
	    
	    for (Information info : produ.getCharacteristics()) {
		if(info.getValue().equals(rulesValue.getPRODUCT_CHARACTERISTIC_DOWNSELL())) {
		    valor_indicador = "DOWN";
		}else if(info.getValue().equals(rulesValue.getPRODUCT_CHARACTERISTIC_UPSELL())){
		    valor_indicador = "UP";
		}
	    }
	    
	    for (Information info : produ.getCharacteristics()) {
		if(info.getCode().equals(rulesValue.getPRODUCT_IND_OFFER())) {
		    info.setValue(valor_indicador);
		}
	    } 
	    
	}
	
	
	for (ProductFixedDTO produ : productList) {
	    for (Information info : produ.getCharacteristics()) {
		if(info.getValue().equals(rulesValue.getPRODUCT_CHARACTERISTIC_DOWNSELL())) {
		    cantidadDownsell++;
		}
	    }
	}
	
	
	
	List<ParamMovTotal> listParamMov = this.paramMovTotalRepo.findAll();
	
	for (ParamMovTotal paramMovTotal : listParamMov) {
	    
	    if(paramMovTotal.getGrupoParam().equals("CANTIDAD_DOWNSELL_MAX")) {
		pmt = new ParamMovTotal();
		pmt = paramMovTotal;
		break;	
	    }
	    
	}
	
	int indice = 0, 
		conteo = 0;
	
	boolean isDownsell = false;
	
	int veces = cantidadDownsell - Integer.parseInt(pmt.getValor());
	
	if(veces > 0) {
	    
	    int contador = 0;
	    
	    while (contador < veces) {
	    
        	    for (ProductFixedDTO produ : productList) {
        		    for (Information info : produ.getCharacteristics()) {
        			if(info.getValue().equals(rulesValue.getPRODUCT_CHARACTERISTIC_DOWNSELL())) {
        			    isDownsell = true;
        			}
        		    }
        		    
        		    if(isDownsell) {
        		    
        			indice = conteo;
        		    
        		    }
        		    
        		    isDownsell = false;
        		    
        		    conteo++;
        	    }	
        	    
        	    conteo = 0;
        	    
        	    productList.remove(indice);
        	    
        	    contador++;
	    
	    }
	    
	}
	
	
	
    }
    
    public boolean activePostPayEasy() {
	
	List<ParamMovTotal> listParamMovTotal = paramMovTotalRepo.findAll();
	
	boolean activo = false;
	
	for (ParamMovTotal paramMovTotal : listParamMovTotal) {
	    if(paramMovTotal.getGrupoParam().equals("POST_PAGO_FACIL")) {
		if(paramMovTotal.getValor().equals("1")) {
		    activo = true;
		}
	    }
	}
	
	return activo;
    }
    
    
    public void showMigraTechnology(CommercialOperationInfo commercialInfo, FixedDTO fixedParkDto, List<ProductFixedDTO> productList) {
	
	List<ParamMovTotal> listParamMov = this.paramMovTotalRepo.findAll();
	
	//ParamMovTotal ppmmtt = null;
	
	for (ParamMovTotal paramMovTotal : listParamMov) {
	    
	    if(paramMovTotal.getGrupoParam().equals("MIGRACION_TECNOLOGICA")) {
		
		pmt = new ParamMovTotal();
		
		pmt = paramMovTotal;
		
		break;
		
	    }
	    
	}
	
	if(pmt.getValor().equals("0")) {
	    for (ProductFixedDTO productFixedDTO1 : productList) {
		productFixedDTO1.getCharacteristics().add(Information.builder().code(rulesValue.getPRODUCT_MIGRATION_TECHNOLOGY())
			.value("0").build());
	    }
	
	}else if(pmt.getValor().equals("1")){
	
        	for (ProductFixedDTO productFixedDTO2 : productList) {
        	    
        	    if(fixedParkDto.getPackagePs().equals(productFixedDTO2.getPackagePs())) {
        		
        		if(fixedParkDto.getTechnologyInternet().equals(productFixedDTO2.getInternetDestinyTechnology())) {
        		    
        		    productFixedDTO2.getCharacteristics().add(Information.builder().code(rulesValue.getPRODUCT_MIGRATION_TECHNOLOGY())
        				.value("1").build());
        		    
        		}else {
        		    productFixedDTO2.getCharacteristics().add(Information.builder().code(rulesValue.getPRODUCT_MIGRATION_TECHNOLOGY())
        				.value("0").build()); 
        		}
        		
        	    }else {
        		productFixedDTO2.getCharacteristics().add(Information.builder().code(rulesValue.getPRODUCT_MIGRATION_TECHNOLOGY())
        			.value("0").build());
        	    }
        	    
        	}
        	
        	boolean existe = false;
        	
        	List<ProductFixedDTO> listProdActual = new ArrayList<ProductFixedDTO>();
        	
        	ProductFixedDTO pf = new ProductFixedDTO();
	}
	
	
    }
    
    public void showMigraTechnologyMT(CommercialOperationInfo commercialInfo, FixedDTO fixedParkDto, List<ProductFixedDTO> productList, List<ProductFixedDTO> actualProduct) {
	
        List<ParamMovTotal> listParamMov = this.paramMovTotalRepo.findAll();
        	
        	//ParamMovTotal ppmmtt = null;
        	
        	for (ParamMovTotal paramMovTotal : listParamMov) {
        	    
        	    if(paramMovTotal.getGrupoParam().equals("MIGRACION_TECNOLOGICA")) {
        		
        		pmt = new ParamMovTotal();
        		
        		pmt = paramMovTotal;
        		
        		break;
        		
        	    }
        	    
        	}
	
        	if(pmt.getValor().equals("0")) {
        	    for (ProductFixedDTO productFixedDTO1 : productList) {
        		productFixedDTO1.getCharacteristics().add(Information.builder().code(rulesValue.getPRODUCT_MIGRATION_TECHNOLOGY())
        			.value("0").build());
        	    }
        	
        	}else if(pmt.getValor().equals("1")){
        	
        	    	Integer i = 0;
        	    	
        	    	ProductFixedDTO actual = new ProductFixedDTO();
        	    
                	for (ProductFixedDTO productFixedDTO : productList) {
                	    
                	    if(i==0) {
                		
                		actual = productFixedDTO;
                		
                	    }        		
                	    
                	    i++;
                	    
                	    if(fixedParkDto.getPackagePs().equals(productFixedDTO.getPackagePs())) {
                		
                		if(fixedParkDto.getTechnologyInternet().equals(productFixedDTO.getInternetDestinyTechnology())) {
                		    
                		    productFixedDTO.getCharacteristics().add(Information.builder().code(rulesValue.getPRODUCT_MIGRATION_TECHNOLOGY())
                				.value("1").build());
                		    
                		}else {
                		    productFixedDTO.getCharacteristics().add(Information.builder().code(rulesValue.getPRODUCT_MIGRATION_TECHNOLOGY())
                				.value("0").build()); 
                		}
                		
                	    }else {
                		
                		if (i == productList.size()) {
                		    
                		    if(productFixedDTO.getInternetSpeed().equals(actual.getInternetSpeed())) {
                			productFixedDTO.getCharacteristics().add(Information.builder().code(rulesValue.getPRODUCT_MIGRATION_TECHNOLOGY())
                        			.value("1").build());
                		    }else {
                			productFixedDTO.getCharacteristics().add(Information.builder().code(rulesValue.getPRODUCT_MIGRATION_TECHNOLOGY())
                        			.value("0").build());
                		    }
                		    
                		}else {
                		    /*if(productFixedDTO.getInternetSpeed().equals(actual.getInternetSpeed())) {
                			productFixedDTO.getCharacteristics().add(Information.builder().code(rulesValue.getPRODUCT_MIGRATION_TECHNOLOGY())
                        			.value("1").build());
                		    }else {
                        		productFixedDTO.getCharacteristics().add(Information.builder().code(rulesValue.getPRODUCT_MIGRATION_TECHNOLOGY())
                        			.value("0").build());
                		    }*/
                		    productFixedDTO.getCharacteristics().add(Information.builder().code(rulesValue.getPRODUCT_MIGRATION_TECHNOLOGY())
                    			.value("0").build());
                		}
                	    }
                	    
                	    
                	    
                	}
                	
                	boolean existe = false;
                	ProductFixedDTO pf = new ProductFixedDTO();
                	
                	
                	
                	//System.out.println(productList.size());
       }
    }
    
    public void getCoverage(CommercialOperationInfo commercialInfo, FixedDTO fixedParkDto) {
	
	fixedParkDto.setQuantityMobile(commercialInfo.getCommercialOpers().size() - 1);
	
	String coverage = commercialInfo.getCoverage().toLowerCase().equals("fiber")?"FTTH":commercialInfo.getCoverage();
	fixedParkDto.setCoverage(coverage);
	if(commercialInfo.getCommercialOpers().get(0).getSubscriber()!=null) {
	    if(commercialInfo.getCommercialOpers().get(0).getSubscriber().getType()!=null) {		
		fixedParkDto.setTypeF(commercialInfo.getCommercialOpers().get(0).getSubscriber().getType());
	    }
	}
	
	if(commercialInfo.getCommercialOpers().get(0).getSubscriber()!=null) {
	    if(commercialInfo.getCommercialOpers().get(0).getSubscriber().getType()!=null) {		
		fixedParkDto.setTypeF(commercialInfo.getCommercialOpers().get(0).getSubscriber().getType());
	    }
	}
	
	if(commercialInfo.getCommercialOpers().get(1).getSubscriber()!=null) {
	    if(commercialInfo.getCommercialOpers().get(1).getSubscriber().getType()!=null) {		
		fixedParkDto.setTypeM1(commercialInfo.getCommercialOpers().get(1).getSubscriber().getType());
	    }
	}
	
	if(commercialInfo.getCommercialOpers().get(1).getDeviceOperation()!=null) {		
		fixedParkDto.setDeviceOperationM1(commercialInfo.getCommercialOpers().get(1).getDeviceOperation());
	}
	
	if(commercialInfo.getCommercialOpers().size() > 2) {
	
        	if(commercialInfo.getCommercialOpers().get(2).getSubscriber()!=null) {
        	    if(commercialInfo.getCommercialOpers().get(2).getSubscriber().getType()!=null) {		
        		fixedParkDto.setTypeM2(commercialInfo.getCommercialOpers().get(2).getSubscriber().getType());
        	    }
        	}	
        	
        	if(commercialInfo.getCommercialOpers().get(2).getDeviceOperation()!=null) {		
        		fixedParkDto.setDeviceOperationM2(commercialInfo.getCommercialOpers().get(2).getDeviceOperation());
        	}	
	
	}
	
    }
    
    public void showCurrentProductOfferEnabled(List<ProductFixedDTO> priorizationList) {
	
	ProductFixedDTO offer = null;
	
	boolean existeActual = false;
	
	for (ProductFixedDTO productFixedDTO : priorizationList) {
	    
	    for(Information info: productFixedDTO.getCharacteristics()) {
		
		if(info.getValue().equals(rulesValue.PRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT)) {
		    offer = productFixedDTO;
		    existeActual = true;
		    break;
		    
		}
		
	    }
	    
	}
	
	ParamMovTotal pmt = new ParamMovTotal();
	    
	    List<ParamMovTotal> listParamMov = this.paramMovTotalRepo.findAll();
	    
	    for (ParamMovTotal paramMovTotal : listParamMov) {
		    
		    if(paramMovTotal.getGrupoParam().equals("MIGRACION_TECNOLOGICA")) {
			
			pmt = new ParamMovTotal();
			
			pmt = paramMovTotal;
			
			break;
			
		    }
		    
	    }
	
	if(!existeActual) {
	
        	for (ProductFixedDTO productFixedDTO : priorizationList) {
        	    if(!(productFixedDTO.getCurrentProduct()==null)) {
                	    if(productFixedDTO.getCurrentProduct().equals("Producto Actual")) {
                		offer = productFixedDTO;
                		break;
                	    }
        	    }
        	}
	
	}
	
	if(pmt.getValor().equals("1")) {
	
        	if(!(offer==null)) {
        	    
        	    boolean agregar = true;
        	    
        	    if(offer.getEnabled().equals('1')) {
        		for (ProductFixedDTO productFixedDTO : priorizationList) {
        		    boolean isCurrent = false;
        		    for(Information info: productFixedDTO.getCharacteristics()) {
        			
        			if(info.getValue().equals(rulesValue.PRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT)) {
        			    isCurrent = true;
        			    
        			    break;
        			    
        			}
        			
        		    }
        		    
        		    if(!isCurrent) {
        			
        			if(productFixedDTO.getPackagePs().equals(offer.getPackagePs()) &&
        				productFixedDTO.getPackageRent().equals(offer.getPackageRent()) &&
        				productFixedDTO.getInternetSpeed().equals(offer.getInternetSpeed())) {
        			    agregar = false;
        			}
        			
        		    }
        		    
        		}
        		
        		priorizationList.add(offer);
        	    }else {
        		
        		boolean existeCopia = false;
        		
        		Integer i = 0;
        		
        		for (ProductFixedDTO productFixedDTOCopy : priorizationList) {
        		    
        		    if(i > 0) {
        		    
                		    if(offer.getInternetSpeed().equals(productFixedDTOCopy.getInternetSpeed())) {
                			if((offer.getPackagePs().equals(productFixedDTOCopy.getPackagePs()))) {
                        			existeCopia = true;
                        			break;
                			}
                		    }
        		    
        		    }
        		    
        		    i++;
        		}
        		
        		if(!existeCopia) {
        		    
        		    priorizationList.add(offer);
        		    
        		}
        		
        	    }
        	    
        	    
        	    
        	}
	
	}
    }

    public ProductFixedDTO getCurrentProductOffer(List<ProductFixedDTO> priorizationList) {
	
	ProductFixedDTO offer = null;
	
	boolean existeActual = false;
	
	for (ProductFixedDTO productFixedDTO : priorizationList) {
	    
	    for(Information info: productFixedDTO.getCharacteristics()) {
		
		if(info.getValue().equals(rulesValue.PRODUCT_CHARACTERISTIC_ACTUAL_PRODUCT)) {
		    offer = productFixedDTO;
		    existeActual = false;
		    break;
		    
		}
		
	    }
	    
	}
	
	if(!existeActual) {
	    for (ProductFixedDTO productFixedDTO1 : priorizationList) {
		
		if(!(productFixedDTO1.getCurrentProduct()==null)) {
		    if(productFixedDTO1.getCurrentProduct().equals(Constant.CURRENT_OFFER_TYPE)) {
			offer = productFixedDTO1;
			break;
		    }
		}
		
	    }
	}
	
	return offer;
    }
    
    
    
    
}
