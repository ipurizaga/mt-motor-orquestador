package com.telefonica.total.service.offers.lma;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.business.GenerateOnlyPlansResponse;
import com.telefonica.total.business.IMastersParksRules;
import com.telefonica.total.business.deviceRules.riskFlag.IRiskFlag;
import com.telefonica.total.business.offersRules.assignDuplicateBonus.IAssignDuplicateBonus;
import com.telefonica.total.business.offersRules.creditLimitRule.ICreditLimitRule;
import com.telefonica.total.business.offersRules.determinateAppsQuantityByRentMonoProduct.IDeterminateAppsQuantityByRentMonoProduct;
import com.telefonica.total.business.offersRules.determinateTotalRentByOffer.IDeterminateTotalRentByOffer;
import com.telefonica.total.business.offersRules.determinatequantitymobile.IDeterminateQuantityMobile;
import com.telefonica.total.business.offersRules.equifaxPatch.IEquifaxPatch;
import com.telefonica.total.business.offersRules.orderAllProducts.IOrderAllProducts;
import com.telefonica.total.business.offersRules.salesOrRetentions.ISalesOrRetentions;
import com.telefonica.total.business.offersRules.totalRentFilter.ITotalRentFilter;
import com.telefonica.total.business.offersRules.verifyNumberOfMobiles.IVerifyNumberOfMobiles;
import com.telefonica.total.business.standaloneRules.calculateArpaSa.ICalculateArpaSa;
import com.telefonica.total.business.standaloneRules.determinatePresentBonusAndPresentDiscountsSa.IDeterminatePresentBonusAndPresentDiscountsSa;
import com.telefonica.total.business.standaloneRules.enableOffersSa.IEnableOffersSa;
import com.telefonica.total.business.standaloneRules.jumpsCalculationSa.IJumpsCalculationSa;
import com.telefonica.total.business.standaloneRules.mobileConsumptionFilterSa.IMobileConsumptionFilterSa;
import com.telefonica.total.business.standaloneRules.prioritizationSa.IPriorizationSa;
import com.telefonica.total.business.standaloneRules.productsStandaloneSa.IProductsStandaloneSa;
import com.telefonica.total.common.service.BaseMTService;
import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.common.util.UtilCollections;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.dto.MobileDTO;
import com.telefonica.total.dto.PartialRequestDTO;
import com.telefonica.total.dto.ProductFixedDTO;
import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.exception.BusinessException;
import com.telefonica.total.jdbc.PlanFinancingEntity;
import com.telefonica.total.model.CatalogFouthDigitPPF;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.CommercialOperationInfo;
import com.telefonica.total.pojo.req.Customer;
import com.telefonica.total.pojo.req.Offer;
import com.telefonica.total.pojo.req.ReqData;
import com.telefonica.total.pojo.resp.RespData;
import com.telefonica.total.service.offers.ASuggestedPlan;
import com.telefonica.total.jdbc.PlanFinancingDao;


@Service
public class SuggestedLMA extends ASuggestedPlan implements BaseMTService {

    @Autowired
    private IMastersParksRules masterParkRules;

    @Autowired
    private ICreditLimitRule haveLimitMobileCreditRule;

    @Autowired
    private IAssignDuplicateBonus assignDuplicateBonus;

    @Autowired
    private ICalculateArpaSa calculateArpa;

    @Autowired
    private IJumpsCalculationSa jumpsCalculation;

    @Autowired
    private IMobileConsumptionFilterSa mobileConsumptionFilter;

    @Autowired
    private IPriorizationSa priorization;

    @Autowired
    private IDeterminatePresentBonusAndPresentDiscountsSa determinatePresentBonusAndPresentDiscounts;

    @Autowired
    private GenerateOnlyPlansResponse response;

    @Autowired
    private IEquifaxPatch equifaxPatch;

    @Autowired
    private ISalesOrRetentions salesOrRetentions;

    @Autowired
    private ITotalRentFilter totalRentFilter;

    @Autowired
    private IOrderAllProducts orderAllProducts;

    @Autowired
    private IDeterminateTotalRentByOffer determinateTotalRentByOffer;
    
    @Autowired
    private IDeterminateQuantityMobile determinateQuantityMobile;

    @Autowired
    private IDeterminateAppsQuantityByRentMonoProduct determinateAppsQuantityByRentMonoProduct;

    @Autowired
    private IVerifyNumberOfMobiles verifyNumberOfMobiles;
    
    @Autowired
    private IProductsStandaloneSa productsStandalone;
    
    @Autowired
    private IEnableOffersSa enableOffers;
    
    @Autowired
    private PlanFinancingDao planFinanDao;
    
    @Autowired
    private IRiskFlag riskFlag;

    @Override
    public RespData executeLogicOperations(ReqData request) {
	List<CommercialOperationInfo> commercialInfoList = new ArrayList<>();
	List<PartialRequestDTO> partialList = new ArrayList<>();

	CommercialOperationInfo commercialOperInfo = request.getCommOperationInfo();
	Customer customer = request.getCustomer();
	
	List<Offer> optionalOffers = request.getOfferFilters();

	
	if(commercialOperInfo.getCommercialOpers().get(0).getSubscriber() != null) {
		
        	String type = commercialOperInfo.getCommercialOpers().get(0).getSubscriber().getType();
        	
        	validateType(type);
	
	}
	
		
	// PARCHE
	commercialInfoList.add(commercialOperInfo);
	// FIN PARCHE
	for (CommercialOperationInfo commercialInfo : commercialInfoList) {
	    /* A - Determinar ofertas segun reglas fijas */
	    PartialRequestDTO partial = getOffersByFixedRules(commercialInfo, customer, optionalOffers.get(0));
	    /* B - Validar si el cliente aplica a por lo menos una oferta de MT */
	    //clientApplyToMT(partial);
	    /*
	     * C y D - Filtrar los planes de acuerdo al score de riesgo y el interes ,
	     * internamente realiza la regla de filtrar las listas de catalog Po-Bo.
	     */
	    filterRiskScoreAndInterestLMA(partial, commercialInfo, optionalOffers);
	    /* E - Determinar condiciones de venta postpago facil */
	    if(activePostPayEasy()) {
		determinatePpfCondition(commercialInfo, partial);
	    }else {
		determinatePpfConditionFalse(commercialInfo, partial);
	    }
	    /* F - ¿Tiene financiamiento actual? */
	    Boolean currentFinancing = haveCurrentFinancing(commercialInfo, partial);
	    if (!currentFinancing) {
		/* G - Determinar condiciones de venta financiamiento */
		determinateSalesConditions(commercialInfo, partial);
	    }
	    partialList.add(partial);
	}
	
	
	/* Construcción de el Response */
	String sfinance = "11";
	
	if(commercialOperInfo.getCommercialOpers().size() == 3) {
	
        	if(commercialOperInfo.getCommercialOpers().get(1).getCreditData().getFinanceValue()!=null && commercialOperInfo.getCommercialOpers().get(2).getCreditData().getFinanceValue()==null) {
        	    sfinance = "10";
        	}
        	
        	if(commercialOperInfo.getCommercialOpers().get(1).getCreditData().getFinanceValue()==null && commercialOperInfo.getCommercialOpers().get(2).getCreditData().getFinanceValue()!=null) {
        	    sfinance = "01";
        	}
        	
        	if(commercialOperInfo.getCommercialOpers().get(1).getCreditData().getFinanceValue()==null && commercialOperInfo.getCommercialOpers().get(2).getCreditData().getFinanceValue()==null) {
        	    sfinance = "00";
        	}
	
	}else {
    	    	if(commercialOperInfo.getCommercialOpers().get(0).getCreditData().getFinanceValue()!=null) {
        	    sfinance = "10";
        	}else {
        	    sfinance = "00";
        	}
	}
	

	
	int cantMoviles = 0,cantFinan = 0;
	List<String> financeValue = new ArrayList(), deviceOperation = new ArrayList();

	for(CommercialOperation op : commercialOperInfo.getCommercialOpers()) {
		if(op.getProduct().equals(Constant.MOBILE)) {
			cantMoviles++;
			if(op.getCreditData().getFinanceValue() != null && op.getDeviceOperation() != null) {
				cantFinan++;
				financeValue.add(op.getCreditData().getFinanceValue());
				deviceOperation.add(op.getDeviceOperation());
			}
		}
	}
	
	//System.out.println(request.getCustomer().getCustomerType());
	//System.out.println(this.validateString(request.getCustomer().getCustomerSubType()));
	//System.out.println(financeValue);
	//System.out.println(deviceOperation);
	
	CatalogFouthDigitPPF catalogFourth = null;
	catalogFourth = riskFlag.execute(request.getCommOperationInfo().getCommercialOpers());
	
	
	//LLamando función planes de financiamiento
		List<PlanFinancingEntity> planFinanLst1 = new ArrayList<PlanFinancingEntity>();
		List<PlanFinancingEntity> planFinanLst2 = new ArrayList<PlanFinancingEntity>();
		
		if(cantFinan==1) {
			try {
				planFinanLst1 = planFinanDao.getPlanFinancing(
						request.getCustomer().getCustomerType(), this.validateString(request.getCustomer().getCustomerSubType()), 
						catalogFourth.getFourthDigitScore().toString(), financeValue.get(0), deviceOperation.get(0));
			}catch(Exception e) {
				
			}
		}
		else if(cantFinan==2) {
			try {
				planFinanLst1 = planFinanDao.getPlanFinancing(
						request.getCustomer().getCustomerType(), this.validateString(request.getCustomer().getCustomerSubType()), 
						catalogFourth.getFourthDigitScore().toString(), financeValue.get(0), deviceOperation.get(0));
			}catch(Exception e) {
				
			}
			
			try {
				planFinanLst2 = planFinanDao.getPlanFinancing(
						request.getCustomer().getCustomerType(), this.validateString(request.getCustomer().getCustomerSubType()), 
						catalogFourth.getFourthDigitScore().toString(), financeValue.get(1), deviceOperation.get(1));
			}catch(Exception e) {
				
			}
		}
		
	return response.sendResponseLMA(partialList, commercialOperInfo, planFinanLst1, planFinanLst2, cantFinan, sfinance, customer, optionalOffers.get(0));
    }

    private PartialRequestDTO getOffersByFixedRules(CommercialOperationInfo commercialInfo, Customer customer, Offer offer) {
	/* Inputs */
	List<CommercialOperation> commercialOpeList = commercialInfo.getCommercialOpers();
	FixedDTO fixedParkDto = new FixedDTO();
	List<ProductFixedDTO> catalogProductFixedDto = productsStandalone.executeLMA();
	//List<ProductFixedDTO> enabledOffers = enableOffers.execute(catalogProductFixedDto);
	List<ProductFixedDTO> enabledOffers = catalogProductFixedDto;
	//Validando un solo móvil
	determinateQuantityMobile.execute(commercialOpeList);
	List<MobileDTO> mobileDevices = masterParkRules.getMasterMobile(commercialOpeList);
	verifyNumberOfMobiles.execute(fixedParkDto, mobileDevices);
	boolean movilecaplOrPortability = UtilCollections.verifyCaplOrPortability(commercialInfo.getCommercialOpers(),
		SuscriptionType.MOBILE);

	/* Lista final a devolver */
	List<ProductFixedDTO> priorizationList = new ArrayList<>();

	/* Rules: */
	/* 0. Parche equifax */
	// fixedRules.determinateEquifaxPatch(mobileDevices, commercialInfo);
	equifaxPatch.execute(mobileDevices, commercialInfo);
	/* 1. validando limite de credito */
	haveLimitMobileCreditRule.executeSaLMA(enabledOffers, commercialInfo, customer, offer);
	/* 2. Asignacion de Bono Duplica a las ofertas ofrecidas */
	assignDuplicateBonus.execute(fixedParkDto, enabledOffers, movilecaplOrPortability, commercialOpeList);
	/* 3. Calculo del ARPA */
	//calculateArpa.executeSa(fixedParkDto, mobileDevices);
	/* 4. Calculo de saltos */
	//jumpsCalculation.executeSa(fixedParkDto, enabledOffers, mobileDevices, commercialOpeList, customer);
	/* 5. Ventas o Retenciones */
	salesOrRetentions.execute(commercialInfo);
	/* 6. Filtro Renta Total */
	//totalRentFilter.execute(enabledOffers);
	/* 7. Filtro Consumo Mobil */
	mobileConsumptionFilter.executeLMA(enabledOffers, mobileDevices);
	/* 8. Priorizaciones Downsell y Upsell */
	//priorizationList.addAll(priorization.executeSa(enabledOffers));
	priorizationList.addAll(enabledOffers);
	/* 9. Orden de productos */
	orderAllProducts.execute(priorizationList, enabledOffers, "", '0', fixedParkDto);
	//onlyThreeOffersDownsell(priorizationList);
	/* 10. Calcular Renta Total */
	//determinateTotalRentByOffer.execute(priorizationList);
	/* 11. Calcular la cantidad de apps por Renta */
	determinateAppsQuantityByRentMonoProduct.execute(mobileDevices);
	/* 12. Determinar los bonos y descuentos actuales */
	determinatePresentBonusAndPresentDiscounts.executeSa(customer, commercialOpeList, mobileDevices);
	/* 13. Enviar Ofertas Disponibles */
	return TotalUtil.availableOffersLma(commercialInfo, fixedParkDto, priorizationList, mobileDevices, offer);
    }
    
    public void validateType(String type) {
	
	if(!(type.equals(Constant.MOBILE))) {
	    throw new BusinessException(Constant.BE_2030); 
	}
	
    }
    
    private String validateString(String text) {
    	if (null == text) {
    	    return StringUtils.EMPTY;
    	} else {
    	    return text;
    	}
    }

}
