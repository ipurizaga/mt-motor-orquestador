package com.telefonica.total.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.telefonica.total.common.service.BaseMTService;
import com.telefonica.total.common.util.Constant;
import com.telefonica.total.enums.ProductType;
import com.telefonica.total.enums.QueryType;
import com.telefonica.total.exception.BusinessException;
import com.telefonica.total.service.filter.FilterOperations;
import com.telefonica.total.service.offers.failure.SuggestedPlanFail;
import com.telefonica.total.service.offers.lma.SuggestedLMA;
import com.telefonica.total.service.offers.plant.SuggestedPlanMt;
import com.telefonica.total.service.offers.provide.SuggestedDevice;
import com.telefonica.total.service.offers.provide.SuggestedPlan;
import com.telefonica.total.service.standalone.MobileStandAlone;

/**
 * 
 * @Author: jomapozo.
 * @Datecreation: 4 dic. 2018 18:44:06
 * @FileName: PlanDeviceFactory.java
 * @AuthorCompany: Telefonica
 * @version: 0.1
 * @Description: Implementacion de factory method
 */
@Service
public class FlowFactory {

    @Autowired
    private SuggestedPlan   suggestedPlan;
    @Autowired
    private SuggestedLMA   suggestedLma;
    @Autowired
    private SuggestedDevice suggestedDevice;
    @Autowired
    private SuggestedPlanMt suggestedPlanMt;
    @Autowired
    private SuggestedPlanFail suggestedPlanFail;
    @Autowired
    private FilterOperations filterOperations;
    @Autowired
    private MobileStandAlone standAlone;

    /***
     * Método factory que se encarga de la instanciar la clase que realizara la
     * lógica de los planes o de los dispositivos.
     * 
     * @param query
     * @return
     */
    public BaseMTService getInstance(QueryType query, int isMt, ProductType product) {

	switch (query) {
	case ONLYPLANS:
	    if (isMt == Constant.GESTION_PLANTA) {
		return suggestedPlanMt;
	    } else if(isMt == Constant.ALTA_NUEVA) {
		return suggestedPlan;
	    } else {
		switch (product) {
		case MOBILE:
		    
		    return suggestedLma;

		default:
		    return suggestedPlanFail;
		}
		
	    }	    
	/*case ONLYLMA:
	    return suggestedLma;*/
	case ONLYDEVICES:
	    return suggestedDevice;
	case STANDALONE:
	    return standAlone;
	case ONLYFILTER:
	    return filterOperations;
	default:
	    throw new BusinessException(Constant.BE_1056);
	}
    }

}
