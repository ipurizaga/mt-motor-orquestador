package com.telefonica.total.service;

import com.telefonica.total.pojo.req.ReqData;
import com.telefonica.total.pojo.resp.RespData;

public interface ISuggestedProduct {

    /***
     * Método que se encarga de decidir cual sera la implemetacion requerida por el
     * cliente, consulta de sólo planes o sólo equipos.
     * 
     * @param request
     * @return
     */
    RespData getSuggestedOffers(ReqData request);

}
