package com.telefonica.total.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BusinessException extends RuntimeException {

    private final String businessError;

    private static final long serialVersionUID = -803774844847541913L;

    public BusinessException(String businessError) {
	this.businessError = businessError;
    }

    public BusinessException(Throwable cause, String businessError) {
	super(cause);
	this.businessError = businessError;
    }

}
