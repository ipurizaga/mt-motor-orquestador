package com.telefonica.total.exception;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.exception.GenericJDBCException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.common.util.MessageProp;
import com.telefonica.total.common.util.TotalUtil;
import com.telefonica.total.common.util.UtilLog;

@RestControllerAdvice
public class ExceptionHandlerAdvice extends ResponseEntityExceptionHandler {

    private static Logger logger = LogManager.getLogger(ExceptionHandlerAdvice.class);

    @Autowired
    private MessageProp prop;

    /***
     * Método que se encarga de manejar las validaciones cuando ocurre un error de
     * binding del request o cuando falla una anotacion "@valid"
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers,
	    HttpStatus status, WebRequest request) {

	List<String> errors = new ArrayList<>();
	for (FieldError error : ex.getBindingResult().getFieldErrors()) {
	    errors.add(error.getField() + ": " + error.getDefaultMessage());
	}
	for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
	    errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
	}

	AppDetail detail = new AppDetail();
	detail.setErrorAppCode(HttpStatus.BAD_REQUEST.toString());
	detail.setErrorAppMessage(HttpStatus.BAD_REQUEST.getReasonPhrase());
	detail.setErrorAppCause(Constant.CAUSE_VALIDATION_ERROR);

	ApiFault apiError = new ApiFault();
	apiError.setErrorCode("4000");
	apiError.setErrorMsg(Constant.APP_MSG_BUSINESS_ERROR);
	apiError.setErrorDetail(errors.toString());
	apiError.setAppDetail(detail);

	this.logThrowingBusinessMethod(ex);
	return handleExceptionInternal(ex, apiError, headers, HttpStatus.BAD_REQUEST, request);
    }

    /***
     * Método que se encarga de manejar todas las excepciones que no tienen handlers
     * especificos.
     * 
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(Exception.class)
    public final ResponseEntity<ApiFault> handleAllExceptions(Exception ex, WebRequest request) {

	Map<String, Object> varArgs = new HashMap<>();
	varArgs.put("trace path", request.getDescription(true));
	varArgs.put("trace class", ex.getStackTrace()[0].getClassName());
	varArgs.put("trace method", ex.getStackTrace()[0].getMethodName());
	varArgs.put("trace line", ex.getStackTrace()[0].getLineNumber());

	AppDetail detail = new AppDetail();
	detail.setErrorAppCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
	detail.setErrorAppMessage(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
	detail.setErrorAppCause(Constant.CAUSE_SERVER_ERROR);
	detail.setVarArgs(varArgs);

	ApiFault apiError = new ApiFault();
	apiError.setErrorCode("4000");
	apiError.setErrorMsg(Constant.TECHNICAL_ERROR);
	apiError.setErrorDetail(prop.obtainDetail(Constant.GENERIC));
	apiError.setAppDetail(detail);
	this.logThrowingBusinessMethod(ex);

	return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = { JpaSystemException.class, GenericJDBCException.class, SQLException.class })
    public final ResponseEntity<ApiFault> handleDBExceptions(Exception ex, WebRequest request) {

	Map<String, Object> varArgs = new HashMap<>();
	varArgs.put("trace path", request.getDescription(false));
	varArgs.put("trace class", ex.getStackTrace()[0].getClassName());
	varArgs.put("trace method", ex.getStackTrace()[0].getMethodName());
	varArgs.put("trace line", ex.getStackTrace()[0].getLineNumber());

	AppDetail detail = new AppDetail();
	detail.setErrorAppCode(HttpStatus.NO_CONTENT.toString());
	detail.setErrorAppMessage(HttpStatus.NO_CONTENT.getReasonPhrase());
	detail.setErrorAppCause(Constant.CAUSE_BUSINESS_ERROR);
	detail.setVarArgs(varArgs);

	ApiFault apiError = new ApiFault();
	apiError.setErrorCode("4000");
	apiError.setErrorMsg(Constant.TECHNICAL_ERROR);
	apiError.setErrorDetail(prop.obtainDetail(getCodeErrorDB(ex)));
	apiError.setAppDetail(detail);
	this.logThrowingBusinessMethod(ex);

	return new ResponseEntity<>(apiError, new HttpHeaders(), HttpStatus.CONFLICT);
    }

    /***
     * Método que se encarga de manejar las excepciones que pertenezan a a la lógica
     * del negocio.
     * 
     * @param ex
     * @param request
     * @return
     */
    @ExceptionHandler(value = { BusinessException.class })
    public final ResponseEntity<ApiFault> handleBusinessException(BusinessException ex, WebRequest request) {

	Map<String, Object> varArgs = new HashMap<>();
	varArgs.put("trace path", request.getDescription(false));
	varArgs.put("trace class", ex.getStackTrace()[0].getClassName());
	varArgs.put("trace method", ex.getStackTrace()[0].getMethodName());
	varArgs.put("trace line", ex.getStackTrace()[0].getLineNumber());

	AppDetail detail = new AppDetail();
	detail.setErrorAppCode(HttpStatus.NO_CONTENT.toString());
	detail.setErrorAppMessage(HttpStatus.NO_CONTENT.getReasonPhrase());
	detail.setErrorAppCause(Constant.CAUSE_BUSINESS_ERROR);
	detail.setVarArgs(varArgs);

	ApiFault apiError = new ApiFault();
	apiError.setErrorCategory(Constant.EXCEPTION_CATEGORY);
	apiError.setErrorSeverity(Constant.EXCEPTION_SEVERITY);
	apiError.setErrorCode(ex.getBusinessError());
	apiError.setErrorMsg(Constant.BUSINESS_ERROR);
	apiError.setErrorDetail(prop.obtainDetail(ex.getBusinessError()));
	apiError.setAppDetail(detail);
	this.logThrowingBusinessMethod(ex);

	return new ResponseEntity<>(apiError, HttpStatus.CONFLICT);
    }

    private void logThrowingBusinessMethod(Exception ex) {

	StringBuilder log = new StringBuilder();
	StackTraceElement[] trace = null;
	if (ex instanceof MethodArgumentNotValidException) {
	    MethodArgumentNotValidException e = (MethodArgumentNotValidException) ex;
	    List<String> errors = new ArrayList<>();
	    for (FieldError error : e.getBindingResult().getFieldErrors()) {
		errors.add(error.getField() + ": " + error.getDefaultMessage());
	    }
	    for (ObjectError error : e.getBindingResult().getGlobalErrors()) {
		errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
	    }
	    log.append("[Message]: ");
	    log.append(errors);
	    log.append(Constant.NEW_LINE);
	    log.append("[Exception Trace]: ");
	    log.append(e.getBindingResult());
	} else if (ex instanceof JpaSystemException) {
	    JpaSystemException e = (JpaSystemException) ex;

	    SQLException eql = (SQLException) e.getRootCause();
	    if (!(eql.getErrorCode() == 20021 || eql.getErrorCode() == 20022 || eql.getErrorCode() == 20023)) {
		log.append("[Code Error]: ");
		log.append(Constant.TECHNICAL_ERROR);
		log.append(Constant.NEW_LINE);
	    }

	    Throwable esql = e.getCause();
	    log.append("[Message]: ");
	    log.append(esql.getMessage());
	    log.append(Constant.NEW_LINE);
	    log.append("[Exception Message]: ");
	    log.append(ex.toString());
	    log.append(Constant.NEW_LINE);
	    log.append("[Exception Trace]: ");
	    log.append(esql.getCause().toString());
	} else if (ex instanceof BusinessException) {
	    log.append("[Message]: ");
	    log.append(ex.getMessage());
	    log.append(Constant.NEW_LINE);
	    trace = ex.getStackTrace();
	    log.append(Constant.NEW_LINE);
	    log.append("[Exception Message]: ");
	    log.append(ex.toString());
	    log.append(Constant.NEW_LINE);
	    log.append("[Exception Trace]: ");
	    for (int i = 0; i <= trace.length - 1; i++) {
		log.append(i + " -> " + trace[i].toString());
		log.append(Constant.NEW_LINE);
		if (i == 25) {
		    break;
		}
	    }
	} else {
	    log.append("[Code Error]: ");
	    log.append(Constant.TECHNICAL_ERROR);
	    log.append(Constant.NEW_LINE);
	    log.append("[Message]: ");
	    log.append(ex.getMessage());
	    log.append(Constant.NEW_LINE);
	    trace = ex.getStackTrace();
	    log.append(Constant.NEW_LINE);
	    log.append("[Exception Message]: ");
	    log.append(ex.toString());
	    log.append(Constant.NEW_LINE);
	    log.append("[Exception Trace]: ");
	    for (int i = 0; i <= trace.length - 1; i++) {
		log.append(i + " -> " + trace[i].toString());
		log.append(Constant.NEW_LINE);
		if (i == 25) {
		    break;
		}
	    }
	}

	log.append(Constant.NEW_LINE);
	logError(log.toString(), null);
    }

    private static void logError(String log, Exception e) {

	StringBuilder finalLog = new StringBuilder();
	finalLog.append(getHeaderForLog());
	finalLog.append(log);
	finalLog.append(getFooterLog());
	if (e != null) {
	    logger.error(finalLog.toString(), e);
	} else {
	    logger.error(finalLog.toString());
	}
    }

    private static String getHeaderForLog() {

	StringBuilder headerLog = new StringBuilder();
	headerLog.append(Constant.NEW_LINE + Constant.SEPARATOR + Constant.NEW_LINE);
	headerLog.append("[TrackingID]: ");
	headerLog.append(UtilLog.getTracking());
	headerLog.append(Constant.NEW_LINE);
	headerLog.append("[Date]: ");
	headerLog.append(new Date());
	headerLog.append(Constant.NEW_LINE);
	headerLog.append("[Time]: ");
	headerLog.append(TotalUtil.getDateFormat(new Date(), Constant.TIME));
	headerLog.append(Constant.NEW_LINE + "" + Constant.NEW_LINE);
	return headerLog.toString();
    }

    private static String getFooterLog() {
	return Constant.SEPARATOR;
    }

    private static String getCodeErrorDB(Exception e) {
	JpaSystemException ex = (JpaSystemException) e;
	SQLException sql = (SQLException) ex.getRootCause();

	if (sql.getErrorCode() == 20021) {
	    return Constant.BE_2021;
	}
	if (sql.getErrorCode() == 20022) {
	    return Constant.BE_2022;
	}
	if (sql.getErrorCode() == 20023) {
	    return Constant.BE_2023;
	}

	return Constant.BE_1055;
    }
}
