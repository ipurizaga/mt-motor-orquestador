package com.telefonica.total.exception;

import java.util.HashMap;
import java.util.Map;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel(description = "Objeto que tendra el detalle del error sea tecnico o funcional")
@Data
public class AppDetail {
    private String		errorAppCode;
    private String		errorAppMessage;
    private String		errorAppCause;
    private Map<String, Object>	varArgs;

    public Map<String, Object> getVarArgs() {
	if (varArgs == null) {
	    varArgs = new HashMap<>();
	}
	return varArgs;
    }

}
