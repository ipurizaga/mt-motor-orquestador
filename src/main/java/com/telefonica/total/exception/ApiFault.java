package com.telefonica.total.exception;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel(description = "Objeto que hace referencia a los errores del api")
@Data
public class ApiFault {

    private String    errorCategory;
    private String    errorCode;
    private String    errorMsg;
    private String    errorDetail;
    private String    errorSeverity;
    private AppDetail appDetail;

}
