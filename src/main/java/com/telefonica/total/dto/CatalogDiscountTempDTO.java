package com.telefonica.total.dto;

import java.io.Serializable;

import org.apache.commons.collections4.CollectionUtils;

import com.telefonica.total.enums.SuscriptionType;
import com.telefonica.total.generic.CommOperCollection;
import com.telefonica.total.model.CatalogDiscountTemp;
import com.telefonica.total.pojo.req.CommercialOperation;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CatalogDiscountTempDTO extends CatalogDiscountTemp implements Serializable{

    private static final long serialVersionUID = 528910246397322480L;
    
    

}
