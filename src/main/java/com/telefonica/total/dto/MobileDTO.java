package com.telefonica.total.dto;

import java.util.Comparator;

import com.telefonica.total.generic.ProdOffDtoCollection;
import com.telefonica.total.model.MasterParkMobil;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Data
@EqualsAndHashCode(callSuper = true)
public class MobileDTO extends MasterParkMobil {

    private static final long serialVersionUID = 531048773727728906L;

    private Integer poId;
    private Integer boId;
    private Boolean isMtm;

    private Integer presentDuplicateBonusDataQuantity;
    private String  presentDuplicateBonusEndDate;
    private Double  presentTotalDataQuantity;

    private Double presentPromotionalDiscountRent;
    private String presentPromotionalEndDate;
    private Double presentPromotionalFinalRent;
    
    private String subscriptionType;

    @Getter(AccessLevel.NONE)
    private ProdOffDtoCollection<ProductOfferDTO> prodOffers;

    public static final Comparator<MobileDTO> majorToMinorRentComparator = new Comparator<MobileDTO>() {
	@Override
	public int compare(MobileDTO pf1, MobileDTO pf2) {
	    return pf2.getMobileRentMonoProduct().compareTo(pf1.getMobileRentMonoProduct());
	}
    };

}
