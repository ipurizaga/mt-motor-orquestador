package com.telefonica.total.dto;

import com.telefonica.total.model.CatalogFourthDigitFinance;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class FourthDigitFinanceDTO extends CatalogFourthDigitFinance{
    
    private static final long serialVersionUID = 1L;
    
    private Long commercialOperationId;

}
