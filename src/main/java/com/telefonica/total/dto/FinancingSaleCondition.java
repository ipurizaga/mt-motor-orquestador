package com.telefonica.total.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class FinancingSaleCondition implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long commercialOperationId;
    private String name;
    private Integer value;

}
