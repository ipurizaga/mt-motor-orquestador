package com.telefonica.total.dto;

import com.telefonica.total.model.CatalogError;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CatalogErrorDTO extends CatalogError {
    
    private static final long serialVersionUID = 1L;
    
    private Integer id;
    private String code;
    private String description;

}
