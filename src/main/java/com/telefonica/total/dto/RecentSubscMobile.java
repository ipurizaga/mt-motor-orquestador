package com.telefonica.total.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class RecentSubscMobile implements Serializable {

    private static final long serialVersionUID = 1L;

    private Character isOwner;
    private String    document;
    private String    gvs;
    private Character earlyCaeq;
    private Integer   permanency;
    private String    isRecentProv;
    private String    hasFinancing;

}
