package com.telefonica.total.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class PostPaidEasyDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long commercialOperationId;
    private String code;
    private String value;
    
    

}
