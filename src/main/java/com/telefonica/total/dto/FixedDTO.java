package com.telefonica.total.dto;

import java.util.Comparator;
import java.util.List;

import com.telefonica.total.model.Financing;
import com.telefonica.total.model.MasterParkFixed;
import com.telefonica.total.pojo.req.Information;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class FixedDTO extends MasterParkFixed {

    private List<Information> presentRents;
    private Double	      summatoryPresentRents;
    private List<Financing>   financingList;
    private Double	      presentARPA;
    private Boolean	      onlyOneMobileIndicator;
    private List<Integer>     commOperProvidePortaPositions;
    private Double	      actualFixedRent;

    private Integer poIdRequest;
    private Integer boIdRequest;
    private String  psRequest;
    private String  coverage;
    private String  typeF;
    private String  typeM1;
    private String  typeM2;
    private String  deviceOperationM1;
    private String  deviceOperationM2;
    private Integer  quantityMobile;
    private Integer priorizationFlux;
    private Integer equivalentCommercialOperation;
    private String modemEquipmentOfferCopy;
    private Integer contar;

    private static final long serialVersionUID = -335898104928654402L;

    public static final Comparator<FixedDTO> majorToMinorRentComparator = new Comparator<FixedDTO>() {
	@Override
	public int compare(FixedDTO pf1, FixedDTO pf2) {
	    return pf2.getActualFixedRent().compareTo(pf1.getPackageRent());
	}
    };

}
