package com.telefonica.total.dto;

import lombok.Data;

@Data
public class IdentifierProductDTO {

    private Integer   ps;
    private Integer   bo;
    private Character state;
    private String    mirror;

}
