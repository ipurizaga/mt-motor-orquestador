package com.telefonica.total.dto;

import java.util.List;

import org.mapstruct.Mapper;

import com.telefonica.total.model.CatalogBillingProduct;
import com.telefonica.total.model.CatalogDiscountTemp;
import com.telefonica.total.model.CatalogPS;
import com.telefonica.total.model.CatalogProductFixed;
import com.telefonica.total.model.MasterParkFixed;
import com.telefonica.total.model.MasterParkMobil;
import com.telefonica.total.pojo.req.CommercialOperationInfo;
import com.telefonica.total.pojo.resp.ProductOffer;

@Mapper(componentModel = "spring")
public interface BeanMapper {


    /** park mobile mapping **/
    List<MobileDTO> mobileParksToDTOs(List<MasterParkMobil> mobiles);

    MobileDTO mobileParkToDto(MasterParkMobil mobile);

    /** park fixed mapping **/
    FixedDTO fixedParkToDTO(MasterParkFixed fixed);

    /** CatalogBillingProduct mapping **/
    BillingProductDTO catalogBillingProductToBillingProductDTO(CatalogBillingProduct catalogBillingProduct);

    /** Catalog product fixed mapping **/
    List<ProductFixedDTO> productFixedToDTOs(List<CatalogProductFixed> fixedProducts);

    ProductFixedDTO productFixedToProductFixedDTO(CatalogProductFixed fixedProduct);

    /** Response products **/
    List<ProductOffer> dtosToProducts(List<ProductOfferDTO> productsDTO);

    ProductOffer dtoToProduct(ProductOfferDTO productDto);
    
    CatalogDiscountTempDTO catalogDiscountTempToDto(CatalogDiscountTemp catalogDiscountTemp);
    
    /** CatalogPs **/
    CatalogPsDTO catalogPSToDto(CatalogPS catalogPS);
    
    /** Catalog product fixed mapping **/
    CommercialOperationInfo commOperInfoToCloneCommOper(CommercialOperationInfo commOper);
}
