package com.telefonica.total.dto;

import java.io.Serializable;

import com.telefonica.total.pojo.resp.ProductOffer;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ProductOfferDTO extends ProductOffer implements Serializable {

    private static final long serialVersionUID = 1L;

    private String    poProduct;
    private Boolean   ppf;
    private String    postPaidFacMonthDelay;
    private String    gvs;
    private Double    additionalCostPpf;
    private Character flagEarlyCaeq;
    private Integer   permanency;
    private Integer   financing;
    private String	  finacingPlanCode;

}
