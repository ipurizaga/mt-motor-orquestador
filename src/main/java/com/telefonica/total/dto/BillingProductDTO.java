package com.telefonica.total.dto;

import com.telefonica.total.model.CatalogBillingProduct;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BillingProductDTO extends CatalogBillingProduct{
    
    private static final long serialVersionUID = 7882316916653315724L;
    

}
