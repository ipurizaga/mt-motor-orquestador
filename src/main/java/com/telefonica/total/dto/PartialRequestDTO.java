package com.telefonica.total.dto;

import java.util.List;

import lombok.Data;

@Data
public class PartialRequestDTO {
    
    private FixedDTO customerInformation;
    private List<MobileDTO> mobileCustomerInformation;
    private List<ProductFixedDTO> resultantProductFixedList;
    private Double actualRent;
    private String productType;
    //Campos para solo LMA
    private String po_code;
    private String saleChannel; 
    private String storeDepartment; 
    private String dealerCode; 
    private String storeId; 

}
