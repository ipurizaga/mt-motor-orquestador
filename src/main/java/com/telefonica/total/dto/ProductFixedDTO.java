package com.telefonica.total.dto;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang3.builder.CompareToBuilder;

import com.telefonica.total.common.util.Constant;
import com.telefonica.total.model.CatalogProductFixed;
import com.telefonica.total.pojo.req.Information;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ProductFixedDTO extends CatalogProductFixed {

    private Double	      jump;
    private Double	      fixedJump;
    private Double	      employeeJump;
    private Double	      employeeDiscount;
    private Double	      employeePackageRent;
    private Integer	      mobileQuantityDataSumatory;
    private String	      decoEquipment;
    private String	      internetDestinyTechnology;
    private String	      modemEquipment;
    private String	      priority;
    private List<Information> aditionalBlocksTokeep;
    private Double	      aditionalBlocksTokeepRent;
    private Integer	      EquipmentDecoQuantity;
    private Integer	      equipmentUltraWifiRepQuantity;
    private String	      currentProduct;
    private String	      coverageRquest;
    private Character	      cob_hfc;
    private String  	      typeF;
    private String            typeM1;
    private String  	      typeM2;
    private String  	      deviceOperationM1;
    private String  	      deviceOperationM2;
    private Integer  	      quantityMobile;
    private Integer	      flagMTDownsellPriority;

    private List<Information> aditionalRentsToKeep;
    private Double	      aditionalRentsToKeepSummatory;

    private Double rentByAditionals;
    private Double offerTotalRent;
    private Double totalCostAloneProducts;

    private BillingProductDTO	   billingProductDto;
    private CatalogDiscountTempDTO catalogDiscountTempDTO;
    private CatalogDiscountTempDTO catalogPromoTempDTO;
    
    private Integer   paymentModality;
    private Integer    periodByMonth;
    private Double    promotionalRent;
    private String    discount;
    private Double    amountCounted;

    private List<PostPaidEasyDTO>	 postPaidEasy;
    private List<FinancingSaleCondition> financingSaleCondition;

    private List<Information> characteristics = new ArrayList<>(3);
    private List<Information> penaltiesPlankRank = new ArrayList<>();
    private List<Information> comparativeComponents = new ArrayList<>();
    
    //Agregando campos para BO y PO del LMA
    private String lma_Bo;
    private String lma_Po;
    
    //Añadiendo gap_velocity
    private Double gap_velocity;
    
    private String packageBenefit;
    
    //Agregando campos Finaciamiento Instalacion y cuotas para parrillas FTTH
    private Double instalacionCosto;
    private Integer instalacionCuotas;
    
    private String campaign = Constant.CAMPANIA_DEFAULT1;
    private String campaignPromo;
    private String codeAutomatizer;

    private static final long serialVersionUID = 1L;

    public static final Comparator<ProductFixedDTO> actualPlanComparator = new Comparator<ProductFixedDTO>() {
	@Override
	public int compare(ProductFixedDTO pf1, ProductFixedDTO pf2) {
	    return pf1.getMobileQuantityDataSumatory() - pf2.getMobileQuantityDataSumatory();
	}
    };

    public static final Comparator<ProductFixedDTO> jumpComparator = new Comparator<ProductFixedDTO>() {
	@Override
	public int compare(ProductFixedDTO pf1, ProductFixedDTO pf2) {
	    return new CompareToBuilder().append(pf1.getJump(), pf2.getJump()).append(pf2.getInternetSpeed(), pf1.getInternetSpeed())
		    .append(pf2.getMobileQuantityDataSumatory(), pf1.getMobileQuantityDataSumatory()).toComparison();
	}
    };
    
    public static final Comparator<ProductFixedDTO> downsellJumpComparator = new Comparator<ProductFixedDTO>() {
	@Override
	public int compare(ProductFixedDTO pf1, ProductFixedDTO pf2) {
	    return pf2.getJump().compareTo(pf1.getJump());
	}
    }; 

    public static final Comparator<ProductFixedDTO> rentComparator = new Comparator<ProductFixedDTO>() {
	@Override
	public int compare(ProductFixedDTO pf1, ProductFixedDTO pf2) {
	    return pf1.getPackageRent().compareTo(pf2.getPackageRent());
	}
    };

    public static final Comparator<ProductFixedDTO> majorToMinorRentComparator = new Comparator<ProductFixedDTO>() {
	@Override
	public int compare(ProductFixedDTO pf1, ProductFixedDTO pf2) {
	    return pf2.getPackageRent().compareTo(pf1.getPackageRent());
	}
    };
    
    public static final Comparator<ProductFixedDTO> minorToMajorRentComparator = new Comparator<ProductFixedDTO>() {
	@Override
	public int compare(ProductFixedDTO pf1, ProductFixedDTO pf2) {
	    return pf1.getPackageRent().compareTo(pf2.getPackageRent());
	}
    };
    
    

    // Override Lombook - Getters
    public List<PostPaidEasyDTO> getPostPaidEasy() {
	if (postPaidEasy == null) {
	    postPaidEasy = new ArrayList<>();
	}
	return postPaidEasy;
    }

    public List<FinancingSaleCondition> getFinancingSaleCondition() {
	if (financingSaleCondition == null) {
	    financingSaleCondition = new ArrayList<>();
	}
	return financingSaleCondition;
    }
}
