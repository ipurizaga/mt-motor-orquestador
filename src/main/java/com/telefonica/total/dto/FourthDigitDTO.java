package com.telefonica.total.dto;

import com.telefonica.total.model.CatalogFouthDigitPPF;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class FourthDigitDTO extends CatalogFouthDigitPPF {

    private static final long serialVersionUID = 1L;

    private String commercialOperationId;

}
