package com.telefonica.total.dto;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductDtoMappper {

    ProductOfferDTO dtoToProduct(ProductOfferDTO productDto);

}
