package com.telefonica.total.dto;

import com.telefonica.total.model.CatalogPS;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CatalogPsDTO extends CatalogPS {
    
    private static final long serialVersionUID = 1L;
    
    private String serviceNumber;
    private String subscriberType;

}
