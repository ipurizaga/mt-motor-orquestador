package com.telefonica.total.dto;


import com.telefonica.total.model.ParamMovTotal;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class PäramMovTotalDTO extends ParamMovTotal{
    
    private static final long serialVersionUID = 1L;


}
