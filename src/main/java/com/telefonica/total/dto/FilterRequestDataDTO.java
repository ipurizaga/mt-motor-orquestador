package com.telefonica.total.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FilterRequestDataDTO implements Serializable{
    
    private static final long serialVersionUID = 528910246397322480L;
    
    private List<CatalogPsDTO> fixedOperationsList;
    private List<MobileDTO> mobileOperationsList;

    
    
}
