package com.telefonica.total.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "CATALOGO_FINANCIAMIENTO_ACTUAL")
public class Financing implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "MSISDN")
    private String msisdn;
    @Column(name = "CUOTA_MENSUAL")
    private Double monthlyFee;
    @Column(name = "CUOTA_INICIAL")
    private Double initialFee;
    @Column(name = "FECHA_INICIO")
    private String initialDate;
    @Column(name = "FECHA_FIN")
    private String endDate;
    @Column(name = "EQUIPO")
    private String equipment;
    @Column(name = "VALOR_EQUIPO")
    private Double equipmentValue;

}
