package com.telefonica.total.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "PARAM_MOV_TOTAL")
public class ParamMovTotal implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID")
    private Integer id;
    @Column(name = "GRUPO_PARAM")
    private String grupoParam;
    @Column(name = "DES_GRUPO_PARAM")
    private String desGrupoParam;
    @Column(name = "COD_VALOR")
    private String  codValor;
    @Column(name = "VALOR")
    private String valor;
    @Column(name = "COL1")
    private String col1;
    @Column(name = "COL2")
    private String col2;
    @Column(name = "COL3")
    private String col3;
    @Column(name = "COL4")
    private String col4;
    @Column(name = "COL5")
    private String col5;
    @Column(name = "COMENTARIOS")
    private String comentarios;

}
