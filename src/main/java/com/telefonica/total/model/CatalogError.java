package com.telefonica.total.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "CATALOG_ERRORS")
public class CatalogError implements Serializable {

/**Modelo de la tabla de errores**/
	
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID")
    private Integer	      id;
    @Column(name = "CODE")
    private String	      code;
    @Column(name = "DESCRIPTION")
    private String	      description;
    

}
