package com.telefonica.total.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "CATALOG_GRT_DIS")
public class CatalogGrtDis implements Serializable {
    
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    private String id;
    @Column(name = "SAP_ID")
    private String sapId;
    @Column(name = "CUSTOMER_TYPE")
    private String customerType;
    @Column(name = "PLAN_GROUP")
    private String planGroup;
    @Column(name = "OPERATION")
    private String operation;
    @Column(name = "SALE_CHANNEL")
    private String saleChannel;
    @Column(name = "STORE_PROVINCE")
    private String storeProvince;
    @Column(name = "STORE_BRAND")
    private String storeBrand;
    @Column(name = "STORE_ID")
    private String storeId;
    @Column(name = "EFFECTIVE_DATE")
    private Date   effectiveDate;
    @Column(name = "RATE")
    private Double rate;
    @Column(name = "EXPIRATION_DATE")
    private Date   expirationDate;

}
