package com.telefonica.total.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "CATALOGO_PS")
public class CatalogPS implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID")
    private Integer	      id;
    @Column(name = "PAQUETE_PS")
    private Integer	      packagePs;
    @Column(name = "CATEGORIA")
    private String	      category;
    @Column(name = "PAQUETE_NOMBRE")
    private String	      packageName;
    @Column(name = "PAQUETE_RENTA")
    private Double	      packageRent;
    @Column(name = "LINEA_PRESENTE")
    private Character	      presentLine;
    @Column(name = "LINEA_RENTAMONOPROD")
    private Double	      rentMonoProdLine;
    @Column(name = "INTERNET_PRESENTE")
    private Character	      presentInternet;
    @Column(name = "INTERNET_RENTAMONOPROD")
    private Double	      rentMonoProdInternet;
    @Column(name = "INTERNET_TECNOLOGIA")
    private String	      technologyInternet;
    @Column(name = "INTERNET_VELOCIDAD")
    private Integer	      speedInternet;
    @Column(name = "INTERNET_MODEMNOMBRE")
    private String	      modenNameInternet;
    @Column(name = "CABLE_PRESENTE")
    private Character	      cablePresent;
    @Column(name = "CABLE_RENTAMONOPROD")
    private Double	      cableRentMonoProd;
    @Column(name = "CABLE_TIPO")
    private String	      cableType;
    @Column(name = "CABLE_CANTDECOSDIGITALES")
    private Integer	      cableQuantitydigitalDeco;
    @Column(name = "CABLE_CANTDECOSHD")
    private Integer	      cableQuantityDecoHd;
    @Column(name = "CABLE_CANTDECOSSMART")
    private Integer	      cableQuantityDecoSmart;
    @Column(name = "CABLE_CANTDECOSDVR")
    private Integer	      cableQuantityDecoDvr;
//    @Column(name = "CABLE_TECNOLOGIA")
//    private String	      cableTechnology;

}
