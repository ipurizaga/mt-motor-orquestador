package com.telefonica.total.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "CATALOG_PROD_FIJO")
public class CatalogProductFixed implements Serializable{

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "IDENTIFICADOR")
    private Integer   identifier;
    @Column(name = "TIPOCOMERCIALIZACION")
    private String    merchandisingType;
    @Column(name = "PAQUETE_TIPO")
    private String    packageType;
    @Column(name = "ESPRODUCTOFIJO")
    private String    isFixedProduct;
    @Column(name = "PAQUETE_PS")
    private Integer   packagePs;
    @Column(name = "PAQUETE_PSADICIONALES")
    private Integer   packagePsAditional;
    @Column(name = "PAQUETE_ESSVA")
    private Integer   packageSva;
    @Column(name = "PAQUETE_NOMBRE")
    private String    packageName;
    @Column(name = "PAQUETE_RENTA")
    private Double    packageRent;
    @Column(name = "PAQUETE_RENTAFIJA")
    private Double    packageFixedRent;
    @Column(name = "LINEA_PRESENTE")
    private Character linePresent;
    @Column(name = "LINEA_NOMBRE")
    private String    lineName;
    @Column(name = "LINEA_NIVEL")
    private Integer   lineLevel;
    @Column(name = "INTERNET_PRESENTE")
    private Character internetPresent;
    @Column(name = "INTERNET_VELOCIDAD")
    private Integer   internetSpeed;
    @Column(name = "INTERNET_TECNOLOGIA")
    private String    internetTechnology;
    @Column(name = "INTERNET_COBERTURA_IQUITOS")
    private Character internetCoverageIquitos;
    @Column(name = "INTERNET_PSADMINISTRATIVA")
    private Integer   internetPsAdmin;
    @Column(name = "INTERNET_DESCRIPCION")
    private String    internetDescription;
    @Column(name = "CABLE_CATEGORIA")
    private String    cableCategory;
    @Column(name = "CABLE_NIVEL")
    private Integer   cableLevel;
    @Column(name = "CABLE_TIPO")
    private String    cableType;
    @Column(name = "PREMIUMNOMBRE")
    private String    premiunName;
    @Column(name = "PLAN_NIVEL")
    private Integer   planLevel;
    @Column(name = "PLAN_NOMBRE")
    private String    planName;
    @Column(name = "MOVIL0_CANTIDADDATOS")
    private Integer   mobil0QuantityData;
    @Column(name = "MOVIL1_CANTIDADDATOS")
    private Integer   mobil1QuantityData;
    @Column(name = "MOVIL0_ROAMING")
    private Double    mobil0Roaming;
    @Column(name = "MOVIL1_ROAMING")
    private Double    mobil1Roaming;
    @Column(name = "PAQUETE_CATEGORIA")
    private String    packageCategory;
    @Column(name = "MOVIL0_NOMBRE")
    private String    mobil0Name;
    @Column(name = "MOVIL0_RENTA")
    private Double    mobil0Rent;
    @Column(name = "MOVIL1_NOMBRE")
    private String    mobil1Name;
    @Column(name = "MOVIL1_RENTA")
    private Double    mobil1Rent;
    @Column(name = "PAQUETE_CODIGOMOVIL")
    private String    packageMobilCode;
    @Column(name = "PAQUETE_NOMBREMOVIL")
    private String    packageMobilName;
    @Column(name = "PAQUETE_SCORE")
    private String    packageScore;
    @Column(name = "CABLE_PRESENTE")
    private Character cablePresent;
    @Column(name = "CABLE_PAQUETIZADO")
    private Character cablePackage;
    @Column(name = "CABLE_NOMBRE")
    private String    cableName;
    @Column(name = "PRECIOREGULAR")
    private Double    regularPrice;
    @Column(name = "PAQUETE_RENTASINMODIFICAR")
    private Double    packageRentWithoutMod;
    @Column(name = "APLICADECOSMART")
    private Character applyDecoSmart;
    @Column(name = "PREMIUM_HD_PRESENTE")
    private Character premiumHdPresent;
    @Column(name = "PREMIUM_HBO_PRESENTE")
    private Character premiumHboPresent;
    @Column(name = "PREMIUM_FOX_PRESENTE")
    private Character premiumFoxPresent;
    @Column(name = "FLAG_REGLA_DECO")
    private Character flagRuleDeco;
    @Column(name = "FLAG_REGLA_MODEM")
    private Character flagRuleModem;
    @Column(name = "MOVIL0_MINUTOS")
    private Integer   mobil0Minutes;
    @Column(name = "MOVIL0_APPS")
    private Integer   mobil0Apps;
    @Column(name = "COSTOHOGAR_SOLO")
    private Double homeCostAlone;
    @Column(name = "COSTOMOVIL1_SOLO")
    private Double mobil1CostAlone;
    @Column(name = "COSTOMOVIL2_SOLO")
    private Double mobil2CostAlone;
    @Column(name = "DESCUENTO_MT")
    private Double discountMT;
    @Column(name = "PAQUETE_PO")
    private String poPackage;
    @Column(name = "MOVISTAR_PRIX")
    private String movistarPrix;
    @Column(name = "MOVISTAR_MUSICA")
    private String movistarMusic;
    @Column(name = "MOVISTAR_PLAY")
    private String movistarPlay;
    @Column(name = "LINEA_MINUTOS")
    private String minutesLine;
    @Column(name = "CABLE_CANALES")
    private String cableChannels;
    @Column(name = "MOVIL0_SMS")
    private String mobil0Sms;
    @Column(name = "MOVIL0_WAINT")
    private String mobil0WaInt;
    @Column(name = "NETFLIX")
    private String netflix;
    @Column(name = "BONO_DUPLICA")
    private String duplicateBonus;
    @Column(name = "HABILITADO")
    private Character enabled;
    @Column(name = "OFERTA_ESPEJO")
    private String mirrorOffer;
    @Column(name = "MOVIL0_ROAMINGCOBERTURA")
    private String mobil0RoamingCoverage;
    @Column(name = "MOVIL1_ROAMINGCOBERTURA")
    private String mobil1RoamingCoverage;
    @Column(name = "MOVIL0_PASAGIGAS")
    private Integer mobil0GigaPass;
    @Column(name = "MOVIL1_PASAGIGAS")
    private Integer mobil1GigaPass;
//    @Column(name = "CANTIDAD_LINEASMOVIL")
//    private Integer quantityMobilLines;
//    @Column(name = "MOVIL_ILIMITADO")
//    private Integer unlimitedMovil;

}
