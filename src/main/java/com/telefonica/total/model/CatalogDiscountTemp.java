package com.telefonica.total.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "CATALOGO_DSCTOTEMP")
public class CatalogDiscountTemp  implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID")
    private Integer id;
    @Column(name = "DESCUENTO_PS")
    private Integer discountPS;
    @Column(name = "DESCUENTO_NOMBRE")
    private String discountName;
    @Column(name = "DESCUENTO_MONTO")
    private Double discountAmount;
    @Column(name = "DESCUENTO_DURACION")
    private Integer discountduration;
    @Column(name = "UBICACION_DISTRITO")
    private String district;
    @Column(name = "UBICACION_PROVINCIA")
    private String province;
    @Column(name = "UBICACION_DEPARTAMENTO")
    private String departmemt;
    @Column(name = "CANAL_VENTAS")
    private String salesChannel;
    @Column(name = "FECHA_INICIO")
    private Date initDate;
    @Column(name = "FECHA_FIN")
    private Date endDate;
    @Column(name = "OFERTA_PS")
    private Integer offerPS;
    @Column(name = "OFERTA_PO")
    private String offerPO;
    @Column(name = "OFERTA_BO")
    private String offerBO;
    @Column(name = "ORIGEN_PRODUCTO")
    private String originProduct;
    @Column(name = "ORIGEN_TIPOPRODUCTO")
    private String originTypeProduct;
    @Column(name = "ORIGEN_OPERACIONCOMERCIAL")
    private String originOpeCommercial;
    @Column(name = "SALTO")
    private Double jump;
    @Column(name = "CAMPANIA")
    private String campain;
    @Column(name = "TIPO_DESCUENTO")
    private String discountType;
    @Column(name = "VELOCIDAD_MONTO")
    private Integer speedAmount;
    @Column(name = "TECNOLOGIA_OFERTA")
    private String offerTechnology;
    
}
