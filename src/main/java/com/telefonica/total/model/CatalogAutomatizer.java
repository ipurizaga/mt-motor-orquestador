package com.telefonica.total.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "CATALOG_AUTOMATIZADOR_ALTAS")
public class CatalogAutomatizer implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID")
    private Integer id;
    @Column(name = "OPERACION_COMERCIAL")
    private String commercialOperation;
    @Column(name = "SEGMENTO")
    private String segment;
    @Column(name = "CANAL")
    private String channel;
    @Column(name = "PROVINCIA")
    private String province;
    @Column(name = "DISTRITO")
    private String district;
    @Column(name = "CAMPANIA")
    private String campaign;
    @Column(name = "CODIGO_PS")
    private String psCode;
    @Column(name = "PAQUETE_RENTA")
    private Double packageRent;
    @Column(name = "EQUIPAMIENTO_MODEM")
    private String equipmmentModem;
    @Column(name = "EQUIPAMIENTO_DECO")
    private String equipmentDeco;
    @Column(name = "CODIGO_PRODUCTO")
    private String productCode;

}
