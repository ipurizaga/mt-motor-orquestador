package com.telefonica.total.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "CATALOG_CUARTO_DIGIT_FINAN")
public class CatalogFourthDigitFinance implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    private String id;
    @Column(name = "FV")
    private Integer risk;
    @Column(name = "FLOW")
    private String  flow;
    @Column(name = "PERCENTAGE")
    private Integer percentage;

}
