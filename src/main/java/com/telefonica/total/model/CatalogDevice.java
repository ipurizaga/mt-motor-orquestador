package com.telefonica.total.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "CATALOG_EQUIP")
public class CatalogDevice {

    private String name;
    @Id
    private String code;
    private Date   saleEffectiveDate;
    private Date   saleExpirationDate;
    private String deviceType;
    private String sapId;
    private String deviceClassification;
    
}
