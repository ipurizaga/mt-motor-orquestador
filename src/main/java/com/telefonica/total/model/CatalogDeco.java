package com.telefonica.total.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "CATALOGO_DECO")
public class CatalogDeco implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "IDENTIFICADOR")
    private String    id;
    @Column(name = "REGLA_ACTIVA")
    private String    rulerActive;
    @Column(name = "VALIDA_RENTA")
    private String   rentVaild;
    @Column(name = "CONDICIONAL_RENTA")
    private String   conditionalRent;
    @Column(name = "RENTA")
    private String   rent;
    @Column(name = "FLAG_DECO_DIGITAL")
    private String   digitalDecoFlag;
    @Column(name = "FLAG_DECO_HD")
    private String   hdDecoFlag;
    @Column(name = "FLAG_DECO_SMARTWIFI")
    private String   smartwifiDecoFlag;
    @Column(name = "CONDICIONAL_DECO_OFERTA")
    private String   offerDecoConditional;
    @Column(name = "COND_CANTIDAD_DECO_OFERTA")
    private String   offerDecoQuantityConditional;
    @Column(name = "DECO_OFERTA")
    private String   offerDeco;
    @Column(name = "CANTIDAD_DECO_OFERTA")
    private String   offerDecoQuantity;

}
