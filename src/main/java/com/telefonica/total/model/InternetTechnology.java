package com.telefonica.total.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "CATALOGO_TECNOLOGIA_DESTINO")
public class InternetTechnology implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    private Integer ID;
    @Column(name = "INTERNET_TECNOLOGIA")
    private String  internetTechnology;
    @Column(name = "COBERTURA")
    private String  coverage;
    @Column(name = "INTERNET_TECNOLOGIA_DESTINO")
    private String  technologyDestiny;

}
