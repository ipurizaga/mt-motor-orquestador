package com.telefonica.total.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "CATALOG_GRT_DEV")
public class CatalogGrtDev implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    private String id;
    @Column(name = "SAP_ID")
    private String sapId;
    @Column(name = "OPERATION")
    private String operation;
    @Column(name = "PLAN_GROUP")
    private String planGroup;
    @Column(name = "CUSTOMER_TYPE")
    private String customerType;
    @Column(name = "SUBSCRIBER_VALUE")
    private String susbcriberType;
    @Column(name = "CUSTOMER_SUBTYPE")
    private String customerSubtype;
    @Column(name = "EFFECTIVE_DATE")
    private Date   effectiveDate;
    @Column(name = "RATE")
    private Double rate;
    @Column(name = "CURRENCY_CODE")
    private String currencyCode;
    @Column(name = "COMM_6_MONTH_RATE")
    private Double comm6MonthRate;
    @Column(name = "COMM_12_MONTH_RATE")
    private Double comm12MonthRate;
    @Column(name = "COMM_18_MONTH_RATE")
    private Double comm18MonthRate;
    @Column(name = "COMM_24_MONTH_RATE")
    private Double comm24MonthRate;
    @Column(name = "COMM_36_MONTH_RATE")
    private Double comm36MonthRate;
    @Column(name = "EXPIRATION_DATE")
    private Date   expirationDate;

}
