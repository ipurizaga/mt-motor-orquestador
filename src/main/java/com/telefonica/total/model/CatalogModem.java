package com.telefonica.total.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "CATALOGO_MODEM")
public class CatalogModem implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "IDENTIFICADOR")
    private String    id;
    @Column(name = "INTERNET_MODEM_OFERTA")
    private String    offerModemInternet;
    @Column(name = "INTERNET_MODEM_TENENCIA")
    private String   tenancyModemInternet;
    @Column(name = "TECNOLOGIA_DESTINO")
    private String   destinyTechnology;
    @Column(name = "TECNOLOGIA_ORIGEN")
    private String   originTechnology;
    @Column(name = "RENTA_OFERTA")
    private String   offerRent;
    @Column(name = "RENTA_CONDICIONAL_OFERTA")
    private String   offerConditionalRent;
    @Column(name = "VALIDA_RENTA")
    private String   rentVaild;
    @Column(name = "FLAG_ACTIVO")
    private String   activeFlag;
    

}
