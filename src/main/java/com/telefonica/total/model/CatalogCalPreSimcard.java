package com.telefonica.total.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "CATALOG_CALPRE_SIMCARD")
public class CatalogCalPreSimcard implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    private String    id;
    @Column(name = "TIPO_CLIENTE")
    private String    clientType;
    @Column(name = "TIPO_PRODUCTO")
    private String    productType;
    @Column(name = "OPERACION_COMERCIAL")
    private String    commercialOperation;
    @Column(name = "MODALIDAD_VENTA_MOVIL")
    private String    mobilSaleModality;
    @Column(name = "FLAG_RIESGOSO")
    private Character flagRisk;
    @Column(name = "BO_CODE")
    private String    boCode;
    @Column(name = "BO_ID")
    private String    boId;
    @Column(name = "BO_NAME_ENG")
    private String    boNameEng;
    @Column(name = "BO_NAME_ES")
    private String    boNameEsp;
    @Column(name = "BO_PRICE")
    private Double    boPrice;
    @Column(name = "BO_CURRENCY")
    private String    boCurrency;

}
