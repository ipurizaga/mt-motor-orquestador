package com.telefonica.total.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "CATALOG_CUARTO_DIG_PPF")
public class CatalogFouthDigitPPF implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    private String    id;
    @Column(name = "DIGITO_CUARTO_SCORE")
    private String    fourthDigitScore;
    @Column(name = "PRECIO_CHIP")
    private Integer   chipPrice;
    @Column(name = "MONTO_PLAN_RANK")
    private Integer   amountPlanRank;
    @Column(name = "FLAG_PPF")
    private Character flagPpf;
    @Column(name = "FLAG_RIESGOSO")
    private Character flagRisk;

}
