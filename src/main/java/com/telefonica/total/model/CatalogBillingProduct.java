package com.telefonica.total.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "CATALOG_BILLOFF_PRODOFF")
public class CatalogBillingProduct implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    private Integer id;
    @Column(name = "PO_ID")
    private String poId;
    @Column(name = "PO_CODE")
    private String poCode;
    @Column(name = "PO_NAME_ENG")
    private String poNameEng;
    @Column(name = "PO_NAME_ES")
    private String poNameEsp;
    @Column(name = "PO_PRODUCT")
    private String poProduct;
    @Column(name = "PO_DEVICE_CLASSIFICATION")
    private String poDeviceClassification;
    @Column(name = "PO_CUSTOMER_TYPE")
    private String poCustomerType;
    @Column(name = "PO_ELIGIBILITY_RULE_RELATION")
    private String poEligibilityRuleRelation;

    @Column(name = "BO_ID")
    private String  boId;
    @Column(name = "BO_CODE")
    private String  boCode;
    @Column(name = "BO_NAME_ENG")
    private String  boNameEng;
    @Column(name = "BO_NAME_ES")
    private String  boNameEsp;
    @Column(name = "BO_PRICE")
    private Double  boPrice;
    @Column(name = "BO_CURRENCY")
    private String  boCurrency;
    @Column(name = "BO_PLAN_RANK")
    private Integer boPlankRank;
    @Column(name = "BO_PLAN_CREDIT_SCORE")
    private String  boPlanCreditScore;
    @Column(name = "BP_PLAN_BO_INDICATOR")
    private String  bpPlanBoIndicator;
    @Column(name = "BO_PLAN_GROUP")
    private String  boPlanGroup;
    @Column(name = "BO_POSTPAGOFACDISDURATION")
    private Integer boPostPaidFacDisDuration;
    @Column(name = "BO_POSTPAGOFACDISPERCENTAGE")
    private Integer boPosPaidFacDisPercentage;
    @Column(name = "BO_POSTPAGOFACMONTHSTODELAYDIS")
    private Integer boPosPaidFacMonthsToDelayDis;
    @Column(name = "SALES_CHANNEL")
    private String  salesChannel;
    @Column(name = "DEALER_CODE")
    private String  dealerCode;
    @Column(name = "STORE_ID")
    private String  storeId;
    @Column(name = "DEPARTMENT")
    private String  department;

}
