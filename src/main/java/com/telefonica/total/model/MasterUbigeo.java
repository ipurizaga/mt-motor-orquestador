package com.telefonica.total.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "MASTER_UBIGEO")
public class MasterUbigeo implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID")
    private Integer id;
    @Column(name = "DEPARTAMENTO")
    private String departamento;
    @Column(name = "PROVINCIA")
    private String provincia;
    @Column(name = "DISTRITO")
    private String distrito;
    @Column(name = "HABILITADO")
    private String habilitado;
    @Column(name = "FECHA_AUDITA")
    private Date fecha_audita;

}
