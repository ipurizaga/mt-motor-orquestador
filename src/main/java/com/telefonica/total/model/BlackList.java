package com.telefonica.total.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "CATALOGO_LISTA_NEGRA")
@Data
public class BlackList implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "DOCUMENTO")
    private String dni;
}
