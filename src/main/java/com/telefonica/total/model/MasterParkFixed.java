package com.telefonica.total.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "MASTER_PARQ_FIJO")
public class MasterParkFixed implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "TELEFONO")
    private Integer	      phoneNumber;
    @Column(name = "PAQUETE_CATEGORIA")
    private String	      packageCategory;
    @Column(name = "PAQUETE_PS")
    private Integer	      packagePs;
    @Column(name = "PAQUETE_RENTA")
    private Double	      packageRent;
    @Column(name = "DESCUENTOPERMANENTE")
    private Double	      permanentDiscount;
    @Column(name = "PAQUETE_FECHAALTA")
    private Date	      packageDateRelease;

    @Column(name = "LINEA_NOMBRE")
    private String    lineNumber;
    @Column(name = "LINEA_PS")
    private Integer   linePs;
    @Column(name = "LINEA_PRESENTE")
    private Character presentLine;
    @Column(name = "LINEA_RENTAMONOPROD")
    private Double    rentMonoProdLine;
    @Column(name = "LINEA_RENTASVASMONOPROD")
    private Double    rentVasMonoProdLine;

    @Column(name = "INTERNET_NOMBRE")
    private String    nameInternet;
    @Column(name = "INTERNET_TECNOLOGIA")
    private String    technologyInternet;
    @Column(name = "INTERNET_PS")
    private Integer   psInternet;
    @Column(name = "INTERNET_PSADMINISTRATIVA")
    private Character adminPsInternet;
    @Column(name = "INTERNET_PAQUETIZADO")
    private Character packageInternet;
    @Column(name = "INTERNET_PRESENTE")
    private Character presentInternet;
    @Column(name = "INTERNET_RENTAMONOPROD")
    private Double    rentMonoProdInternet;
    @Column(name = "INTERNET_SVA")
    private Character svaInternet;
    @Column(name = "INTERNET_SVARENTA")
    private Double    svaRentInternet;
    @Column(name = "INTERNET_VELOCIDAD")
    private Integer   speedInternet;
    @Column(name = "INTERNET_COB_HFC")
    private Character hfcCobInternet;
    @Column(name = "INTERNET_COB_IQUITOS")
    private Character iquitosCobInternet;
    @Column(name = "INTERNET_COB_RADIOENLACE")
    private Character linkRadioCobInternet;
    @Column(name = "INTERNET_COB_VELOCIDADMAXIMA")
    private Integer   maxSpeedCobInternet;
    @Column(name = "INTERNET_MODEMNOMBRE")
    private String    modenNameInternet;
    @Column(name = "INTERNET_MODEMPS")
    private Integer   modenPsInternet;
    @Column(name = "INTERNET_ULTRAWIFI")
    private Character wifiUltraInternet;
    @Column(name = "INTERNET_ULTRAWIFIRENTA")
    private Double rentWifiUltraInternet;
    @Column(name = "INTERNET_ULTRAWIFICANTIDAD")
    private Integer   quantityWifiUltraInternet;
    @Column(name = "INTERNET_NAKED")
    private Character nakedInternet;

    @Column(name = "CABLE_NOMBRE")
    private String    cableName;
    @Column(name = "CABLE_CATEGORIA")
    private String    cableCategory;
    @Column(name = "CABLE_TIPO")
    private String    cableType;
    @Column(name = "CABLE_RENTAMONOPROD")
    private Double    cableRentMonoProd;
    @Column(name = "CABLE_COBERTURA")
    private String    cableCob;
    @Column(name = "CABLE_FECHAALTA")
    private Date      cableDateRelease;
    @Column(name = "CABLE_SERVICIO")
    private Integer   cableService;
    @Column(name = "CABLE_PRESENTE")
    private Character cablePresent;
    @Column(name = "CABLE_PUNTOSDIGITALES")
    private Character cabledigitalPoint;
    @Column(name = "CABLE_CANTDECOSDIGITALES")
    private Integer   cableQuantitydigitalDeco;
    @Column(name = "CABLE_CANTDECOSHD")
    private Integer   cableQuantityDecoHd;
    @Column(name = "CABLE_CANTDECOSSMART")
    private Integer   cableQuantityDecoSmart;
    @Column(name = "CABLE_CANTDECOSDVR")
    private Integer   cableQuantityDecoDvr;
    @Column(name = "CABLE_RENTADECOENALQUILER")
    private Double    cableRentDeco;

    @Column(name = "TMP_PREMIUM_PSPAQUETE")
    private Character packagePsTmpPremium;
    @Column(name = "PREMIUM_RENTAMONOPROD")
    private Double    rentMonoProdPremium;
    @Column(name = "CLIENTE")
    private String    client;
    @Column(name = "CUENTA")
    private String    account;
    @Column(name = "INSCRIPCION")
    private String    inscription;
    @Column(name = "CLIENTECMS")
    private String    clientCms;
    @Column(name = "PLAN_PRESENTE")
    private Character presentPlan;
    @Column(name = "PLAN_RENTAMONOPROD")
    private Double    rentMonoProdPlan;
    @Column(name = "DESPOSICIONADO")
    private Character deposed;
    @Column(name = "ZONACOMPETENCIA")
    private Character competitionArea;
    @Column(name = "DIGITALIZADO")
    private Date      digital;
    @Column(name = "DIGITALIZADOPROMO")
    private Character digitalPromo;
    @Column(name = "COMPORTAMIENTOPAGOCOVERGENTE")
    private Character convergentPayBehavior;
    @Column(name = "PLAN_NOMBRE")
    private String    planName;
    @Column(name = "PLAN_PS")
    private Integer   planPs;
    @Column(name = "TMP_PREMIUM_PSMONO")
    private String    psMonoPremiumTmp;
    @Column(name = "CABLE_PREMIUM_CANALES")
    private String    cablePremiumChannels;
    @Column(name = "TMP_CABLE_DECODIFICADORATIS")
    private String    cableTmpDecoderAtis;
    @Column(name = "TMP_CABLE_DECODIFICADORCMS")
    private String    cableTmpDecoderCms;
    @Column(name = "DOCUMENTO")
    private String    document;
    @Column(name = "NOMBRE")
    private String    name;
    @Column(name = "PAQUETE_NOMBRE")
    private String    packageName;
    @Column(name = "SMARTWIFI")
    private Character smartWifi;
    @Column(name = "UBICACION_DEPARTAMENTO")
    private String    departmentLocation;
    @Column(name = "UBICACION_PROVINCIA")
    private String    provinceLocation;
    @Column(name = "UBICACION_DISTRITO")
    private String    districtLocation;
    @Column(name = "CICLO")
    private Integer   cycle;
    @Column(name = "AVERIAS")
    private Integer   fault;
    @Column(name = "RECLAMOS")
    private Integer   claim;
    @Column(name = "LLAMADAS")
    private Integer   calls;
    @Column(name = "TRAFICO")
    private Double    traffic;
    @Column(name = "FINANCIAMIENTODEUDA")
    private Double    financeDebt;
    @Column(name = "EQUIPOSFINANCIADOS")
    private Double    financeEquipment;
    @Column(name = "RENTASADICIONALES")
    private Double    aditionalRent;
    @Column(name = "MACROSEGMENTO")
    private String    macroSegment;
    @Column(name = "INCREMENTOPRECIOFECHA")
    private Date      incrementPriceDate;
    @Column(name = "DIRECCION")
    private String    address;

    @Column(name = "CABLE_PREM_HD_PRESENTE")
    private Character cablePresentPremHd;
    @Column(name = "CABLE_PREM_HBO_PRESENTE")
    private Character cablePresentPremHbo;
    @Column(name = "CABLE_PREM_FOX_PRESENTE")
    private Character cablePresentPremFox;
    @Column(name = "CABLE_ESTELAR_PRESENTE")
    private Character cablePresentEstellar;
    @Column(name = "CABLE_PREM_HD_RENTAMONOPROD")
    private Double    cableRentMonoProdPremHd;
    @Column(name = "CABLE_PREM_HBO_RENTAMONOPROD")
    private Double    cableRentMonoProdPremHbo;
    @Column(name = "CABLE_PREM_FOX_RENTAMONOPROD")
    private Double    cableRentMonoProdPremFox;
    @Column(name = "CABLE_ESTELAR_RENTAMONOPROD")
    private Double    cableRentMonoProdEstellar;
    @Column(name = "CABLE_HTPACK_PRESENTE")
    private Character cablePresentHTPack;
    @Column(name = "CABLE_HTPACK_RENTAMONOPROD")
    private Double    cableRentMonoProdHTPack;
    @Column(name = "CABLE_GOLDPREM_PRESENTE")
    private Character cablePresentGoldPrem;
    @Column(name = "CABLE_GOLDPREM_RENTAMONOPROD")
    private Double    cableRentMonoProdGoldPrem;

    @Column(name = "DECO_SMART_RENTA")
    private Double decoSmartRent;
    @Column(name = "PUNTO_ADICIONAL_HD_RENTA")
    private Double hdAditionalPointRent;
    @Column(name = "INTERNET_TIENEPAM_RENTA")
    private Double internetHavePamRent;
    @Column(name = "RENTATOTAL")
    private Double totalRent;
    
    
    @Column(name = "DSCTOEMPLEADO_PRESENTE")
    private Double presentEmployeeDiscount;
    @Column(name = "DSCTOEMPLEADO_MONTO")
    private Double presentEmployeeDiscountAmmount;
    

}
