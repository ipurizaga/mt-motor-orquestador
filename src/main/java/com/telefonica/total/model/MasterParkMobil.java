package com.telefonica.total.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "MASTER_PARQ_MOVIL")
public class MasterParkMobil implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "DOCUMENTO")
    private String    document;
    @Id
    @Column(name = "MOVIL")
    private String    mobileNumber;
    @Column(name = "MOVIL0_ESTITULAR")
    private Character isOwner;
    @Column(name = "TRESDIGITOSSCORING")
    private Integer   threeDigitScoring;
    @Column(name = "LIMITECREDITOMOVIL")
    private Double    mobileLimitCredit;
    @Column(name = "NOMBRE")
    private String    name;
    @Column(name = "MOVIL0_NOMBRE")
    private String    mobileName;
    @Column(name = "FECHAALTA")
    private Date      dateRelease;
    @Column(name = "MOVIL0_IDPLAN")
    private Integer   mobilePlanId;
    @Column(name = "MOVIL0_NOMBREPLAN")
    private String    mobilePlanName;
    @Column(name = "MOVIL0_SEGMENTO")
    private String    mobileSegment;
    @Column(name = "MOVIL0_ESTADO")
    private String    mobileStatus;
    @Column(name = "MOVIL0_EQUIPO")
    private String    mobileCellPhone;
    @Column(name = "MOVIL0_NAVEGACION")
    private String    mobileNavigation;
    @Column(name = "MOVIL0_TIPOLINEA")
    private String    mobileLineType;
    @Column(name = "MOVIL0_MONTOACTIVACION")
    private Double    mobileMonoActivation;
    @Column(name = "MOVIL0_RENTATOTAL")
    private Double    mobileRentTotal;
    @Column(name = "MOVIL0_RENTAMONOPRODUCTO")
    private Double    mobileRentMonoProduct;
    @Column(name = "MOVIL0_CANTIDADDATOS")
    private Integer   mobileQuantityData;
    @Column(name = "MOVIL0_BONODUPLICA")
    private Integer   mobileDoubleBonus;
    @Column(name = "MOVIL0_PRODUCTO")
    private String    mobileProduct;
    @Column(name = "MOVIL0_CANTIDADLINEAS")
    private Integer   mobileQuantityLines;
    @Column(name = "MOVIL0_TIPOCLIENTE")
    private String    mobileClientType;
    @Column(name = "MOVIL0_CICLO")
    private String    mobileCycle;
    @Column(name = "MOVIL0_CTDMINMOVFIJO")
    private Integer   mobileCdtMinMobFixed;
    @Column(name = "MOVIL0_CTDMINMOVMOV")
    private Integer   mobileCdtMinMob;
    @Column(name = "MOVIL0_CTDMINMOVCOMPETENCIA")
    private Integer   mobileCdtMinMobCompetition;
    @Column(name = "MOVIL0_CTDMINMOVLARGADISTANCIA")
    private Integer   mobileCdtMinMobLargeDistance;
    @Column(name = "MOVIL0_CTDSMS")
    private Integer   mobileCdtSms;
    @Column(name = "MOVIL0_CTDMBROAMING")
    private Integer   mobileCdtMbRoaming;
    @Column(name = "MOVIL0_PAGOPLAN")
    private Double    mobilePlanPay;
    @Column(name = "MOVIL0_FUENTESERVICIO")
    private String    mobileServiceSource;
    @Column(name = "MOVIL0_CONSUMOMB")
    private Double    mobileMbConsumption;
    @Column(name = "MOVIL0_IMEI")
    private String    mobileImei;
    @Column(name = "MOVIL0_DESCUENTOPERMANENTE")
    private Double    permanentDisccount;
    @Column(name = "MOVIL0_APPS")
    private Integer   mobileApps;

    @Column(name = "MOVIL0_BONODUPLICAFLAG")
    private Character duplicateBonusFlag;
    @Column(name = "MOVIL0_BONODUPLICAVENC")
    private Integer   duplicateBonusExpiration;
    @Column(name = "MOVIL0_BONODUPLICAASIG")
    private Date      duplicateBonusAsig;

    @Column(name = "MOVIL0_DESCUENTOPROM")
    private Character promotionalDiscount;
    @Column(name = "MOVIL0_DESCUENTOPROMVENC")
    private Integer   promotionalDiscountExpiration;
    @Column(name = "MOVIL0_DESCUENTOPROMASIG")
    private Date      promotionalDiscounAsig;

    @Column(name = "GVS")
    private String    gvs;
    @Column(name = "FLAG_EARLYCAEQ")
    private Character earlyCaeq;
    @Column(name = "PERMANENCIA")
    private Integer   permanence;

}
