package com.telefonica.total.pojo.resp;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonPropertyOrder({"sapId", "price"})
@JsonInclude(Include.NON_NULL)
public class DeviceResp {

    private String sapId;
    private PriceDeviceResp price;


}
