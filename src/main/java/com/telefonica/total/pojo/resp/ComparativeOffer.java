package com.telefonica.total.pojo.resp;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"origin","destiny","gap","name"})
public class ComparativeOffer {
    
    private Double origin;  
    private Double destiny;
    private Double gap;
    private String name;

}
