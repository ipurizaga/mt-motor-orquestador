package com.telefonica.total.pojo.resp;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonPropertyOrder({"initialFee", "discount", "price", "priceCommitment6", "priceCommitment12", "priceCommitment18", "priceCommitment24", "priceCommitment36", "financingPlanCode"})
@JsonInclude(Include.NON_NULL)
public class PriceDeviceResp {

	private Double initialFee;
	private Double discount;
    private Double price;
    private Double priceCommitment6;
    private Double priceCommitment12;
    private Double priceCommitment18;
    private Double priceCommitment24;
    private Double priceCommitment36;    
    private String financingPlanCode;

}
