package com.telefonica.total.pojo.resp;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.telefonica.total.pojo.req.Information;
import com.telefonica.total.pojo.req.Offer;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"poCode", "boCode", "saleConditions", "poId", "poNameEs", "boId", "boNameEs", "price", "priority", "devices", "billingOffersInfo"})
public class CurrentProductOffer extends Offer {

    private String poId;
    private String poNameEs;

    private String boId;
    private String boNameEs;

    private Double  price;
    private Integer priority;

    private List<DeviceResp>  devices;
    private List<Information> billingOffersInfo;

}
