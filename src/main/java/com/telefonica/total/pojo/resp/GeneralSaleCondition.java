package com.telefonica.total.pojo.resp;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.telefonica.total.pojo.req.Information;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

@Data
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"additionalInfo","condition","operationCommId"})
public class GeneralSaleCondition {
    
    private List<Information> additionalInfo;
    private Information	      condition;
    private Long	 operationCommId;

}
