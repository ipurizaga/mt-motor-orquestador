package com.telefonica.total.pojo.resp;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.telefonica.total.pojo.req.Information;
import com.telefonica.total.pojo.req.Offer;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"poCode", "boCode", "saleConditions", "poId", "poNameEs", "boId", "boNameEs", "price", "priority", "devices", "billingOffersInfo", "currentOffersToKeep"})
public class ProductOffer extends Offer {

    private String poId;
    private String poNameEs;

    private String boId;
    private String boNameEs;

    private Double  price;
    private Integer priority;

    private List<DeviceResp>  devices;
    private List<Information> billingOffersInfo;
 //   private List<ComparativeOffer> comparativeOffersInfo;
 //   private String quantityCommOpersMobile;
    private List<CurrentProductOffer> currentOffersToKeep;
 

    public List<Information> getBillingOffersInfo() {
	if (billingOffersInfo == null) {
	    billingOffersInfo = new ArrayList<>();
	}
	return billingOffersInfo;
    }

}
