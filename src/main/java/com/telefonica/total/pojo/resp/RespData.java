package com.telefonica.total.pojo.resp;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.telefonica.total.pojo.req.Information;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel(description = "Objeto que hace referencia a la respuesta del api, devolvera 3 listas, informacion del producto actual, informacion adicional, y ofertas sugeridas")
@Data
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({ "currentProductInfo", "additionalInfo", "generalSaleCondition", "productOffers" })
public class RespData {

    private List<Information>  currentProductInfo;
    private List<Information>  additionalInfo;
    private List<GeneralSaleCondition> generalSaleCondition;
    private List<ProductOffer> productOffers;

    public List<ProductOffer> getProductOffers() {
	if (productOffers == null) {
	    productOffers = new ArrayList<>();
	}
	return productOffers;
    }

    public List<Information> getAdditionalInfo() {
	if (additionalInfo == null) {
	    additionalInfo = new ArrayList<>();
	}
	return additionalInfo;
    }
}
