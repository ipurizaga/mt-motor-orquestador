package com.telefonica.total.pojo.req;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel(description = "Objeto que hace referencia a las condiciones de venta")
@Data
@JsonPropertyOrder({"operationCommId", "condition", "additionalInfo"})
public class SaleCondition {

    private Long	      operationCommId;
    private Information	      condition;
    private List<Information> additionalInfo;

    public List<Information> getAdditionalInfo() {
	if (additionalInfo == null) {
	    additionalInfo = new ArrayList<>();
	}
	return additionalInfo;
    }
}
