package com.telefonica.total.pojo.req;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel(description = "Objeto que tiene la informacion principal del cliente")
@Data
@JsonPropertyOrder({ "customerSubType", "customerType", "debts", "documentNumber", "documentType", "isEmployee", "district", "province",
	"department"})
public class Customer {

    @NotNull(message = "Ingrese el tipo de documento")
    private String     documentType;
    @NotNull(message = "Ingrese el numero de documento")
    private String     documentNumber;
    @NotNull(message = "Ingrese el tipo de cliente")
    private String     customerType;
    private String     customerSubType;
    private List<Debt> debts;
    private Boolean    isEmployee;
    private String     district;
    private String     province;
    private String     department;


    public List<Debt> getDebts() {
	if (debts == null) {
	    debts = new ArrayList<>();
	}
	return debts;
    }

}
