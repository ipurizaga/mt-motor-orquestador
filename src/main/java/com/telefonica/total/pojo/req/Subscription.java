package com.telefonica.total.pojo.req;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel(description = "Informacion del abonado asociado a la operacion comercial")
@Data
@JsonPropertyOrder({ "debt", "gvs", "serviceNumber", "subscriberId", "type", "isRecentProvide", "isOwner", "rentMonoProduct",
	"dataQuantity", "mbRoamingQuantity", "permanency", "isEarlyCaeq", "hasFinancing", "poId", "boId" })
public class Subscription {

    private String subscriberId;
    private String serviceNumber;
    private String type;
    private Debt   debt;
    private String gvs;

    private Integer isRecentProvide;
    private Boolean isOwner;
    private Double  rentMonoProduct;
    private Integer dataQuantity;
    private Integer mbRoamingQuantity;
    private Integer permanency;
    private Boolean isEarlyCaeq;
    private Boolean hasFinancing;
    private Integer poId;
    private Integer boId;

}
