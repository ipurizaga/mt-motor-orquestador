package com.telefonica.total.pojo.req;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel(description = "Objeto usado como filtro de dispositivos")
@Data
@JsonPropertyOrder({"deviceCategory", "sapId"})
public class Device {

    private String deviceCategory;
    private String sapId;

}
