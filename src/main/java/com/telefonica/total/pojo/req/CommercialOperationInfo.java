package com.telefonica.total.pojo.req;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@Data
@JsonPropertyOrder({ "commercialOpers", "coverage", "dealerCode", "department", "negotiationMode", "productType", "province",
	"salesChannel", "salesChannelType", "salesChannelGroup", "salesEntity", "salesPoint", "storeBrand", "storeId", "date", "additionalOperationInformation" })
public class CommercialOperationInfo {

    @NotNull(message = "Por favor ingrese el tipo de producto")
    private String		      productType;
    @NotNull(message = "Por favor ingrese los canales de venta")
    private String		      salesChannel;
    private String		      salesChannelType;
    private String		      salesChannelGroup;
    private String		      salesEntity;
    private String		      salesPoint;
    private String		      dealerCode;
    private String		      department;
    private String		      province;
    private String		      storeBrand;
    private String		      storeId;
    private String		      coverage;
    private String		      negotiationMode;
    private String		      date;
    private List<Information>	      additionalOperationInformation;
    @Valid
    @NotNull
    private List<CommercialOperation> commercialOpers;

    public List<CommercialOperation> getCommercialOpers() {
	if (commercialOpers == null) {
	    commercialOpers = new ArrayList<>();
	}
	return commercialOpers;
    }

}
