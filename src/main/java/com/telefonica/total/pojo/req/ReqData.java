package com.telefonica.total.pojo.req;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@Data
@JsonPropertyOrder({"commOperationInfo", "customer", "deviceFilters", "offerFilters", "queryType"})
public class ReqData {

    @Valid
    @NotNull(message = "Se necesita al menos una operacion comercial")
    private CommercialOperationInfo commOperationInfo;

    @Valid
    @NotNull(message = "Ingrese el cliente")
    private Customer customer;

    @NotNull(message = "Ingrese el tipo de consulta")
    private String queryType;

    private List<Offer>	 offerFilters;
    private List<Device> deviceFilters;

    /** initialize Arrays **/

    public List<Offer> getOfferFilters() {
	if (offerFilters == null) {
	    offerFilters = new ArrayList<>();
	}
	return offerFilters;
    }

    public List<Device> getDeviceFilters() {
	if (deviceFilters == null) {
	    deviceFilters = new ArrayList<>();
	}
	return deviceFilters;
    }

}
