package com.telefonica.total.pojo.req;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel(description = "Datos crediticios de la operacion comercial")
@Data
@JsonPropertyOrder({"action", "aditionalLines", "creditLimit", "creditScore", "operationComm", "financeValue", "product"})
public class Score {

    private String  action;
    private String  creditScore;
    private String  product;
    private String  operationComm;
    private Double  creditLimit;
    private String  financeValue;
    private Integer aditionalLines;    

}
