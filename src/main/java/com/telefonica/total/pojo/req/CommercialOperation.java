package com.telefonica.total.pojo.req;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel(description = "Objecto detalle de la operacion comercial")
@Data
@JsonPropertyOrder({ "creditData", "deviceOperation", "operation", "operationCommId", "product", "saleMode", "subscriber" })
public class CommercialOperation implements Comparable<CommercialOperation> {

    private Long	 operationCommId;
    @NotNull(message = "Por favor ingrese el producto")
    private String	 product;
    @NotNull(message = "Por favor ingrese la operacion")
    private String	 operation;    
    private String	 saleMode;
    private String	 deviceOperation;
    private Score	 creditData;
    private Subscription subscriber;

    @Override
    public int compareTo(CommercialOperation o) {
	return this.creditData.getCreditLimit().compareTo(o.getCreditData().getCreditLimit());
    }

}
