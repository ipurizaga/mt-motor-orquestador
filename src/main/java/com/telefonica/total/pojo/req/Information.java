package com.telefonica.total.pojo.req;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonPropertyOrder({ "code", "value" })
public class Information implements Serializable {

    private static final long serialVersionUID = 1L;

    private String code;
    private String value;

}
