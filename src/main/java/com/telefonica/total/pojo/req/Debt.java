package com.telefonica.total.pojo.req;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel(description = "Deudas relacionadas al cliente")
@Data
@JsonPropertyOrder({"amount", "bussiness", "currency"})
public class Debt {
	/*Se cambió el tipo del atributo amount a Integer, para que el valor pueda ser leido por 
	*el paquete de la base de datos de la regla 18.
	*/
    private Integer amount;
    private String currency;
    private String bussiness;

}
