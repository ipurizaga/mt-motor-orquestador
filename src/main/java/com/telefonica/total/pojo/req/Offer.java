package com.telefonica.total.pojo.req;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel(description = "Objeto usado como filtro de ofertas")
@Data
@JsonPropertyOrder({"boCode", "poCode", "saleConditions"})
public class Offer {

    private String		poCode;
    private String		boCode;
    private List<SaleCondition>	saleConditions;
}

