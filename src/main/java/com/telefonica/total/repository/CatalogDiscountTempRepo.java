package com.telefonica.total.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telefonica.total.model.CatalogDiscountTemp;

@Repository
public interface CatalogDiscountTempRepo extends JpaRepository<CatalogDiscountTemp, Serializable>  {
    
    @Cacheable(value = "catalogDiscountTemp", unless = "#result == null or #result.size() == 0")
    List<CatalogDiscountTemp> findAll();
}
