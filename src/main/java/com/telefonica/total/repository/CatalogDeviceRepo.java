package com.telefonica.total.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telefonica.total.model.CatalogDevice;

@Repository
public interface CatalogDeviceRepo extends JpaRepository<CatalogDevice, Serializable> {

    CatalogDevice findBySapId(String sapId);

}
