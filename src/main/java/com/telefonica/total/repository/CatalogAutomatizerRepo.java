package com.telefonica.total.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.telefonica.total.model.CatalogAutomatizer;

public interface CatalogAutomatizerRepo extends JpaRepository<CatalogAutomatizer, Serializable> {
    
    @Cacheable(value= "catalogAutomatizer", unless = "#result == null or #result.size() == 0")
    List<CatalogAutomatizer> findAll();

}
