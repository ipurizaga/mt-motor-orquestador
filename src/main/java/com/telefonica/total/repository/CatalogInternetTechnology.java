package com.telefonica.total.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telefonica.total.model.InternetTechnology;

@Repository
public interface CatalogInternetTechnology extends JpaRepository<InternetTechnology, Serializable>  {
    
    @Cacheable(value = "internetTechnology", unless = "#result == null or #result.size() == 0")
    List<InternetTechnology> findAll();

}
