package com.telefonica.total.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.telefonica.total.model.ParamMovTotal;


@Repository
public interface ParamMovTotalRepo extends JpaRepository<ParamMovTotal, Serializable> {

    /*ParamMovTotal findByGrupoParam(String grupoPäram);*/

    @Query("select pmt from ParamMovTotal pmt where pmt.grupoParam = ?1")
    ParamMovTotal findByParam(String grupoPäram);
    
    @Query("select pmt from ParamMovTotal pmt where pmt.codValor = ?1 and rownum < 2")
    ParamMovTotal findByCode(String codValor);
    
    @Query("select pmt from ParamMovTotal pmt where pmt.codValor = ?1")
    List<ParamMovTotal> findByCodeLst(String codValor);
    
    @Query("select pmt from ParamMovTotal pmt where pmt.grupoParam = ?1")
    List<ParamMovTotal> findByGrupoParam(String grupoParam);

    @Cacheable(value = "paramMovTotal", unless = "#result == null or #result.size() == 0")
    List<ParamMovTotal> findAll();

}
