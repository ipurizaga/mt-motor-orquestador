package com.telefonica.total.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telefonica.total.model.CatalogPS;

@Repository
public interface CatalogPSRepo extends JpaRepository<CatalogPS, Serializable> {

    CatalogPS findByPackagePs(Integer packagePs);
    
    List<CatalogPS> findAll();
}
