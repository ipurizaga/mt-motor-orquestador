package com.telefonica.total.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.telefonica.total.model.MasterUbigeo;


@Repository
public interface MasterUbigeoRepo extends JpaRepository<MasterUbigeo, Serializable> {

    /*ParamMovTotal findByGrupoParam(String grupoPäram);*/

    /*@Query("select pmt from ParamMovTotal pmt where pmt.grupoParam = ?1")
    MasterUbigeo findByParam(String grupoPäram);*/

    @Cacheable(value = "masterUbigeo", unless = "#result == null or #result.size() == 0")
    List<MasterUbigeo> findAll();

}
