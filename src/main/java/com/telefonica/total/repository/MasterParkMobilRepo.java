package com.telefonica.total.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telefonica.total.model.MasterParkMobil;

@Repository
public interface MasterParkMobilRepo extends JpaRepository<MasterParkMobil, Serializable> {

    MasterParkMobil findByMobileNumber(String mobileNumber);

    List<MasterParkMobil> findAllByDocument(String document);

}
