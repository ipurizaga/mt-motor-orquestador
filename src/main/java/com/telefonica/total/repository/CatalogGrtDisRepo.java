package com.telefonica.total.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telefonica.total.model.CatalogGrtDis;

@Repository
public interface CatalogGrtDisRepo extends JpaRepository<CatalogGrtDis, Serializable> {

}
