package com.telefonica.total.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.telefonica.total.model.CatalogCalPreSimcard;

@Repository
public interface CatalogCPreSimcardRepo extends JpaRepository<CatalogCalPreSimcard, Serializable> {

    CatalogCalPreSimcard findByBoCode(String boCode);

    @Query("select cat from CatalogCalPreSimcard cat where cat.clientType = ?1 and cat.productType = ?2 "
	    + "and cat.commercialOperation = ?3 and cat.mobilSaleModality = ?4 and cat.flagRisk = ?5")
    CatalogCalPreSimcard findByParam(String clientType, String productType, String commercialOperation, String mobilSaleModality,
	    Character flagRisk);

    @Cacheable(value = "priceSimCard", unless = "#result == null or #result.size() == 0")
    List<CatalogCalPreSimcard> findAll();

}
