package com.telefonica.total.repository;

import java.io.Serializable;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.telefonica.total.model.MasterParkFixed;

@Repository
public interface MasterParkFixedRepo extends CrudRepository<MasterParkFixed, Serializable> {
    
    MasterParkFixed findByPhoneNumber(Integer phoneNumber);

}
