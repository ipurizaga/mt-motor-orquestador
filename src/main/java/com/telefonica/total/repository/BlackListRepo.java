package com.telefonica.total.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telefonica.total.model.BlackList;

@Repository
public interface BlackListRepo extends JpaRepository<BlackList, Serializable> {
    
    BlackList findByDni(String dni);
}
