package com.telefonica.total.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telefonica.total.model.CatalogFouthDigitPPF;

@Repository
public interface Catalog4DigitPPFRepo extends JpaRepository<CatalogFouthDigitPPF, Serializable> {

    CatalogFouthDigitPPF findByFourthDigitScore(String digit);

    @Cacheable(value = "fourthDigitPpf", unless = "#result == null or #result.size() == 0")
    List<CatalogFouthDigitPPF> findAll();
}
