package com.telefonica.total.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telefonica.total.model.CatalogDeco;


@Repository
public interface CatalogDecoRepo extends JpaRepository<CatalogDeco, Serializable> {

    @Cacheable(value = "catalogDeco", unless = "#result == null or #result.size() == 0")
    List<CatalogDeco> findAll();

}
