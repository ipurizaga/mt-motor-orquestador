package com.telefonica.total.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.telefonica.total.model.CatalogGrtDev;

@Repository
public interface CatalogGrtDevRepo extends JpaRepository<CatalogGrtDev, Serializable> {

    @Query("select grtDev from CatalogGrtDev grtDev where grtDev.sapId = ?1 and grtDev.operation = ?2 and grtDev.planGroup = ?3 and grtDev.customerType = ?4 "
	    + "and grtDev.susbcriberType = ?5 and grtDev.customerSubtype = ?6 and ( CURRENT_DATE between grtDev.effectiveDate and grtDev.expirationDate)")
    CatalogGrtDev findByParam(String sapId, String operation, String planGroup, String customerType, String suscriber, String customerSubType);
}
