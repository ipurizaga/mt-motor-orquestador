package com.telefonica.total.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telefonica.total.model.CatalogProductFixed;

@Repository
public interface CatalogProdFixedRepo extends JpaRepository<CatalogProductFixed, Serializable> {
    
    @Cacheable(value = "productFixedAll", unless = "#result == null or #result.size() == 0")
    List<CatalogProductFixed> findAll();

}
