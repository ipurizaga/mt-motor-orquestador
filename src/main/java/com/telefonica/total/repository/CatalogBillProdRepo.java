package com.telefonica.total.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telefonica.total.model.CatalogBillingProduct;

@Repository
public interface CatalogBillProdRepo extends JpaRepository<CatalogBillingProduct, Serializable> {

    List<CatalogBillingProduct> findByBoIdAndPoId(String boId, String poId);

}
