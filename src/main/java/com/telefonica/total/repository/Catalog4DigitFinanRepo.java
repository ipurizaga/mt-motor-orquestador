package com.telefonica.total.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.telefonica.total.model.CatalogFourthDigitFinance;

@Repository
public interface Catalog4DigitFinanRepo extends JpaRepository<CatalogFourthDigitFinance, Serializable> {

    @Cacheable(value = "fourthDigitFinan", unless = "#result == null or #result.size() == 0")
    List<CatalogFourthDigitFinance> findAll();

}
