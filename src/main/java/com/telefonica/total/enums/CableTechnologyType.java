package com.telefonica.total.enums;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

@Getter
public enum CableTechnologyType {

    CATV("CATV"), DTH("DTH"), DEFAULT("NO APLICA");

    private String code;

    private CableTechnologyType(String code) {
	this.code = code;
    }
    
    public static CableTechnologyType getCabelTechonologyType(String code) {
	for (CableTechnologyType type : values()) {
	    if (type.getCode().equals(code)) {
		return type;
	    }
	}
	return DEFAULT;
    }

    public static List<String> getCableTechonologyList() {
	List<String> list = new ArrayList<>();
	for (CableTechnologyType type : values()) {
	    if (!type.getCode().isEmpty()) {
		list.add(type.getCode());
	    }
	}
	return list;
    }

    
}
