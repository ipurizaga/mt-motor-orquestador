package com.telefonica.total.enums;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

@Getter
public enum InternetTechnologyType {
    
    FTTH("FTTH"), HFC("HFC"), ADSL("ADSL"), NOTHING("NO APLICA");

    private String code;

    private InternetTechnologyType(String code) {
	this.code = code;
    }

    public static InternetTechnologyType getInternetTechonologyType(String code) {
	for (InternetTechnologyType type : values()) {
	    if (type.getCode().equals(code)) {
		return type;
	    }
	}
	return NOTHING;
    }

    public static List<String> getInternetTechonologyList() {
	List<String> list = new ArrayList<>();
	for (InternetTechnologyType type : values()) {
	    if (!type.getCode().isEmpty()) {
		list.add(type.getCode());
	    }
	}
	return list;
    }

}
