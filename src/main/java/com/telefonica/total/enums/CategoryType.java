package com.telefonica.total.enums;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

@Getter
public enum CategoryType {

    TRIO("Trio"), DUO_TV_AND_INTERNET("Duo TV Internet"), DUO_BA("Duo BA"), DUO_TV("Duo TV"), MONO_BA("Mono BA"), MONO_TV(
	    "Mono TV"), MONO_LINE("Mono Linea"), DEFAULT("");

    private String code;

    private CategoryType(String code) {
	this.code = code;
    }

    public static CategoryType getCategoryType(String code) {
	for (CategoryType type : values()) {
	    if (type.getCode().equals(code)) {
		return type;
	    }
	}
	return DEFAULT;
    }

    public static List<String> getCategoryTypeCodeList() {
	List<String> list = new ArrayList<>();
	for (CategoryType type : values()) {
	    if (!type.getCode().isEmpty()) {
		list.add(type.getCode());
	    }
	}
	return list;
    }

}
