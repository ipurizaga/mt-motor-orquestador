package com.telefonica.total.enums;

import lombok.Getter;

@Getter
public enum SalesChannelType {

    COUT("call center out"), O("online"), OW("online web"), OD("online delivery"), OC("online convergente"), TEX("TIENDAS EXPRESS"), TFR("TIENDAS FRANQUICIAS"), 
    MLTCNT("MULTICENTROS"), DEFAULT("");

    private String code;

    private SalesChannelType(String codeDesc) {
	this.code = codeDesc;
    }

    public static SalesChannelType getSalesChannelType(String code) {
	for (SalesChannelType channelType : values()) {
	    if (channelType.getCode().equals(code)) {
		return channelType;
	    }
	}
	return DEFAULT;
    }

}
