package com.telefonica.total.enums;

import lombok.Getter;

@Getter
public enum DocumentType {
    DNI("DNI"), RUC("RUC");

    private String type;

    private DocumentType(String type) {
	this.type = type;
    }

    public static DocumentType getDocumentType(String type) {
	for (DocumentType types : values()) {
	    if (types.getType().equals(type)) {
		return types;
	    }
	}
	return null;
    }
}
