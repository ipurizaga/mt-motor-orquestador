package com.telefonica.total.enums;

import lombok.Getter;

@Getter
public enum SuscriptionType {

    MOBILE("movil"), FIXED("fijo"), CABLE("duoCable"), INTERNET("internet"), MT("mt"), TRIO("trio"), DUO("duo"), DUOCABLE("duoCable"), 
    	DUOINTERNET("duoInternet"), VOICEFIXED("vozFija"), MTM("mtm"), MTF("mtf"), MTMV("mtmv"), MTFV("mtfv"), DEFAULT("");

    private String code;

    private SuscriptionType(String code) {
	this.code = code;
    }

    public static SuscriptionType getSuscriptionType(String code) {
	for (SuscriptionType type : values()) {
	    if (type.getCode().equals(code)) {
		return type;
	    }
	}
	return DEFAULT;
    }
}
