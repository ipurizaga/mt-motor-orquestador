package com.telefonica.total.enums;

import lombok.Getter;

@Getter
public enum SaleModalityMobile {

    ALTACOMBO("alta combo"), M4("m4"), M7("m7"), DEFAULT("nothing");

    private String code;

    private SaleModalityMobile(String codeDesc) {
	this.code = codeDesc;
    }

    public static SaleModalityMobile getSaleModMobil(String code) {
	for (SaleModalityMobile sale : values()) {
	    if (sale.getCode().equals(code)) {
		return sale;
	    }
	}
	return DEFAULT;
    }
}
