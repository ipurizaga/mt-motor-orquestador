package com.telefonica.total.enums;

import lombok.Getter;

@Getter
public enum OperationDevice {
    ACTIVATION("LineactivationProvide"), PORTABILITY("Portability"), CAEQ("DevicechangeCAEQ"), DEFAULT("nothing");

    private String codeDesc;

    private OperationDevice(String codeDesc) {
	this.codeDesc = codeDesc;
    }

    public static OperationDevice getOpeCommMobile(String code) {
	for (OperationDevice oper : values()) {
	    if (oper.getCodeDesc().equals(code)) {
		return oper;
	    }
	}
	return DEFAULT;
    }
}
