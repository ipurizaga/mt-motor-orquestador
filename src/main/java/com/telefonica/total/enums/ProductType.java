package com.telefonica.total.enums;

import lombok.Getter;

@Getter
public enum ProductType {
    MOBILE("movil"), FIXED("fijo"), DEFAULT("nothing");

    private String codeDesc;

    private ProductType(String codeDesc) {
	this.codeDesc = codeDesc;
    }

    public static ProductType getProducType(String code) {
	for (ProductType product : values()) {
	    if (product.getCodeDesc().equals(code)) {
		return product;
	    }
	}
	return DEFAULT;
    }
}
