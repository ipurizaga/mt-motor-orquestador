package com.telefonica.total.enums;

import lombok.Getter;

@Getter
public enum OperationComm {
    PROVIDE("provide"), CAPL("replaceOffer"), SIMCHANGE("simChange"), PORTABILITY("portability"), DEFAULT("");

    private String codeDesc;

    private OperationComm(String codeDesc) {
	this.codeDesc = codeDesc;
    }

    public static OperationComm getOpeCommMobile(String code) {
	for (OperationComm oper : values()) {
	    if (oper.getCodeDesc().equals(code)) {
		return oper;
	    }
	}
	return DEFAULT;
    }
}
