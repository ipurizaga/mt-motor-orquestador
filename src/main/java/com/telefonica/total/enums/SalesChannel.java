package com.telefonica.total.enums;

public enum SalesChannel {
    CC, DSA, MS, ST, DLS, DLC, DLV, SS, CEC, SMB;
}
