package com.telefonica.total.enums;

import lombok.Getter;

@Getter
public enum QueryType {
    DEVICESPLANS("planesEquipos"), ONLYPLANS("soloPlanes"), ONLYLMA("soloLMA"), ONLYDEVICES("soloEquipos"),
    ONLYFILTER("filtrarOperaciones") , STANDALONE("standAlone"), DEFAULT("");

    private String code;

    private QueryType(String code) {
	this.code = code;
    }

    public static QueryType getQueryType(String code) {
	for (QueryType query : values()) {
	    if (query.getCode().equals(code)) {
		return query;
	    }
	}
	return DEFAULT;
    }
}
