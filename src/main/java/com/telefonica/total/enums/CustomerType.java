package com.telefonica.total.enums;

import lombok.Getter;

@Getter
public enum CustomerType {
    RESIDENCIAL("R"), CORPORATIVO("C"), DEFAULT("");

    private String code;

    private CustomerType(String code) {
	this.code = code;
    }

    public static CustomerType getCustomType(String code) {
	for (CustomerType customer : values()) {
	    if (customer.getCode().equals(code)) {
		return customer;
	    }
	}
	return DEFAULT;
    }
}
