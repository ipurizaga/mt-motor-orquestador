package com.telefonica.total.enums;

import lombok.Getter;

@Getter
public enum ScoreType {
    MOBILE("movil"), FIXED("fijo"), DEFAULT("");

    private String code;

    private ScoreType(String code) {
	this.code = code;
    }

    public static ScoreType getScoreType(String code) {
	for (ScoreType type : values()) {
	    if (type.getCode().equals(code)) {
		return type;
	    }
	}
	return DEFAULT;
    }
}
