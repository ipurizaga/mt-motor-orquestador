package com.telefonica.total.enums;

import lombok.Getter;

@Getter
public enum ScoreAction {
    APPROVED("APROBAR"), REJECTED("RECHAZAR"), DEFAULT("");

    private String code;

    private ScoreAction(String code) {
	this.code = code;
    }

    public static ScoreAction getAction(String code) {
	for (ScoreAction score : values()) {
	    if (score.getCode().equals(code)) {
		return score;
	    }
	}
	return DEFAULT;
    }

}
