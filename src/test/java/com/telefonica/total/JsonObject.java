package com.telefonica.total;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telefonica.total.enums.SalesChannel;
import com.telefonica.total.pojo.req.Customer;
import com.telefonica.total.pojo.req.ReqData;
import com.telefonica.total.pojo.req.Score;

public class JsonObject {
    public static void main(String[] args) throws JsonProcessingException {

	SalesChannel sales = SalesChannel.valueOf("DSA");
	if (sales == SalesChannel.CC) {
	    System.out.println(sales + " " + true);
	} else {
	    System.out.println(sales + " " + false);
	}

	ObjectMapper obj = new ObjectMapper();
	ReqData req = new ReqData();

	Customer client = new Customer();
	client.setCustomerSubType("");
	client.setCustomerType("R");
	client.setDocumentNumber("12345678");
	client.setDocumentType("DNI");	
	Score score = new Score();
	score.setAction("APROBAR");
	score.setCreditScore("A0299");	
	score.setCreditLimit(250.3);


	System.out.println(obj.writeValueAsString(req));
    }
}
