package com.telefonica.total;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityManager;
import javax.persistence.StoredProcedureQuery;
import javax.sql.DataSource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.telefonica.total.jdbc.PlanDeviceDao;
import com.telefonica.total.jdbc.PlanDeviceEntity;
import com.telefonica.total.model.BlackList;
import com.telefonica.total.model.CatalogBillingProduct;
import com.telefonica.total.model.CatalogCalPreSimcard;
import com.telefonica.total.repository.BlackListRepo;
import com.telefonica.total.repository.CatalogCPreSimcardRepo;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MovistarTotalApplicationTests {



    @Autowired
    private PlanDeviceDao planDeviceDao;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private EntityManager em;

    @Autowired
    private CatalogCPreSimcardRepo catalgoPre;

    @Autowired
    private BlackListRepo blackListRepo;

    @Test
    public void contextLoads() {
    }

    public void structMapperTest() {

	CatalogBillingProduct catalog = new CatalogBillingProduct();
	catalog.setBoCode("1234");
	catalog.setBoId("1234");
	catalog.setPoId("abcd");
	catalog.setBoNameEng("English");
	catalog.setBoNameEsp("Espaniol");

    }

    public void callStoreEntityMan() {

	long init = System.currentTimeMillis();
	String plans = "4576PlanVuelaBPES169,ABIERTOVUELABPEMPRESASRD,true|3304PlanStaffTDPVP,ABIERTOSTAFFCORP,false";
	String devices = "TMGPEAP6S016DOS001|TMGPEAP6S016GRS001|TMGPEAP6S016ORS001|TMGPEAP6S016PLS001|TMGPEAP6S032GRS001";

	StoredProcedureQuery store = this.em.createNamedStoredProcedureQuery("listPlanDevice");
	store.setParameter(1, "");
	store.setParameter(2, "LineactivationProvide");
	store.setParameter(3, "R");
	store.setParameter(4, "");
	store.setParameter(5, "1");
	store.setParameter(6, "");
	store.setParameter(7, "");
	store.setParameter(8, "CC");
	store.setParameter(9, plans);
	store.setParameter(10, devices);

	store.execute();

	@SuppressWarnings("unchecked")
	List<PlanDeviceEntity> lstPlans = (List<PlanDeviceEntity>) store.getResultList();

	System.out.println("End time: " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - init));

	for (PlanDeviceEntity plan : lstPlans) {
	    System.out.println("Plans : " + plan.toString());
	}

    }

    public void getSpData() {
	long init = System.currentTimeMillis();

	String plans = "4576PlanVuelaBPES169,ABIERTOVUELABPEMPRESASRD,true";
	String devices = "TMGPEAP6S016DOS001";

//	List<PlanDeviceEntity> lstEntity = planDeviceDao.getPlanDevices("", "replaceOffer", "R", "NPR", "20", "1", "", "CC", ' ', "", "",
//		' ', 0, "", plans, devices);
//
//	System.out.println("End time: " + TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - init));
//
//	for (PlanDeviceEntity planDevice : lstEntity) {
//	    System.out.println("Plans : " + planDevice.toString());
//	}

    }

    public void getDataSource() {
	System.out.println("Data Source Name: " + dataSource.getClass().getName());
    }

    public void getPriceSimcard() {
	List<CatalogCalPreSimcard> pricesSim = catalgoPre.findAll();
	System.out.println(pricesSim.size());
	for (CatalogCalPreSimcard catalog : pricesSim) {
	    System.out.println(catalog.toString());
	}
    }

    public void blackList() {
	BlackList dni = blackListRepo.findByDni("99999912");
	System.out.println("DNI " + dni);
    }

}
