package com.telefonica.total;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.telefonica.total.business.IMastersParksRules;
import com.telefonica.total.business.offersRules.determinateBillingCycle.IDeterminateBillingCycle;
import com.telefonica.total.dto.FixedDTO;
import com.telefonica.total.pojo.req.CommercialOperation;
import com.telefonica.total.pojo.req.CommercialOperationInfo;
import com.telefonica.total.pojo.req.ReqData;

public class BillingCycleTest {
    
    @Autowired
    private IDeterminateBillingCycle determinateBillingCycle;
    
    @Autowired
    private IMastersParksRules masterParkRules;
    
//    @Test
//    private void testBillingCycle() {
//	ReqData request;
//	List<CommercialOperationInfo> commercialInfoList = new ArrayList<>();
//	CommercialOperationInfo commercialOperInfo = request.getCommOperationInfo();
//	commercialInfoList.add(commercialOperInfo);
//	List<CommercialOperation> commercialOpeList = commercialInfoList.getCommercialOpers();
//	FixedDTO fixedParkDto = masterParkRules.getMasterFixed(commercialOpeList);
//    }
}
